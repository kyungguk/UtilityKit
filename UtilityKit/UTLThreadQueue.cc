//
//  UTLThreadQueue.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/25/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLThreadQueue.h"
#include "UTLPrinto.h"

// MARK: Version 3
//
#include <iostream>
#include <exception> // std::terminate

// MARK: ThreadQueue
//
namespace { // helpers
    struct Terminator {};

    void worker_thread_main(UTL::__3_::BlockQueue<UTL::__3_::Mutex> *q, std::string const &label)
    {
        try {
            while (true) {
                q->dequeue()();
            }
            //pthread_exit(nullptr); // calling this at the end lead to memory leak!!
        } catch (::Terminator &) {
            return;
        } catch (std::exception &e) {
            printo(std::cerr , __PRETTY_FUNCTION__, "[\"", label, "\"] - ", e.what(), '\n');
            std::terminate();
        } catch (...) {
            printo(std::cerr , __PRETTY_FUNCTION__, "[\"", label, "\"] - ", "unknown exception", '\n');
            std::terminate();
        }
    }
}

UTL::__3_::ThreadQueue::~ThreadQueue()
{
    // signal thread exit:
    //
    for (unsigned i = 0; i < _pool.size(); ++i) {
        _q.enqueue([]() {
            throw Terminator();
        });
    }

    // join queue threads:
    //
    while (!_pool.empty()) {
        _pool.back().join();
        _pool.pop_back();
    }
}

UTL::__3_::ThreadQueue::ThreadQueue(std::string const& label)
: ThreadQueue(label, Thread::hardware_concurrency()) {
}
UTL::__3_::ThreadQueue::ThreadQueue(std::string const& label, unsigned concurrency)
: _q(), _pool(concurrency), _label(label) {
    for (Thread &t : _pool) {
        t = Thread(std::bind(&::worker_thread_main, &_q, _label));
    }
}

auto UTL::__3_::ThreadQueue::concurrency() const noexcept
-> decltype(_pool.size()) {
    return _pool.size();
}


// MARK:- Version 4
//
#include <vector>
#include <utility>

void UTL::__4_::ThreadQueue::apply(unsigned long const iterations, std::function<void(unsigned long const)> const &block)
{
    static unsigned long const n_threads = hardware_concurrency();
    std::vector<Thread> pool;
    // parallel execution
    //
    if (iterations < n_threads) {
        // dispatch the rest to worker thread
        for (unsigned long i = 1; i < iterations; ++i) {
            pool.emplace_back(std::forward<decltype(block)>(block), i);
        }
        // execute the first in calling thread
        block(0);
    } else {
        // dispatch the rest to worker thread
        for (unsigned long offset = 1; offset < n_threads; ++offset) {
            pool.emplace_back([](unsigned long const iterations, unsigned long const offset, std::function<void(unsigned long const)> const &block)->void {
                for (unsigned long i = offset; i < iterations; i += n_threads) {
                    block(i);
                }
            }, iterations, offset, std::forward<decltype(block)>(block));
        }
        // execute the first in calling thread
        for (unsigned long i = 0; i < iterations; i += n_threads) {
            block(i);
        }
    }
    // join
    //
    for (Thread &th : pool) {
        th.join();
    }
}
