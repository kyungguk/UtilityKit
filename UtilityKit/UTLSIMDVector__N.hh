//
//  UTLSIMDVector__N.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/28/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMDVector__N_hh
#define UTLSIMDVector__N_hh

// MARK:- Version 4
//
#include <UtilityKit/UTL_is_aligned.h>

// MARK: Reductions
//
template <class Type, long Size>
Type UTL::__4_::SIMDVector<Type, Size>::reduce_plus() const noexcept {
    half_vector_type lo = this->lo();
    return (lo += hi()).reduce_plus();
}
template <class Type, long Size>
Type UTL::__4_::SIMDVector<Type, Size>::reduce_prod() const noexcept {
    half_vector_type lo = this->lo();
    return (lo *= hi()).reduce_prod();
}
template <class Type, long Size>
Type UTL::__4_::SIMDVector<Type, Size>::reduce_bit_and() const noexcept {
    half_vector_type lo = this->lo();
    return (lo &= hi()).reduce_bit_and();
}
template <class Type, long Size>
Type UTL::__4_::SIMDVector<Type, Size>::reduce_bit_or() const noexcept {
    half_vector_type lo = this->lo();
    return (lo |= hi()).reduce_bit_or();
}
template <class Type, long Size>
Type UTL::__4_::SIMDVector<Type, Size>::reduce_bit_xor() const noexcept {
    half_vector_type lo = this->lo();
    return (lo ^= hi()).reduce_bit_xor();
}

// MARK: Compound Arithmetic
//
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator+=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    **this += *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator+=(vector_type const &rhs) noexcept -> SIMDVector &{
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this += static_cast<SIMDVector const&>(rhs);
    } else {
        return *this += SIMDVector{rhs};
    }
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator-=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    **this -= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator-=(vector_type const &rhs) noexcept -> SIMDVector &{
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this -= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this -= SIMDVector{rhs};
    }
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator*=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    **this *= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator*=(vector_type const &rhs) noexcept -> SIMDVector &{
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this *= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this *= SIMDVector{rhs};
    }
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator/=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    **this /= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator/=(vector_type const &rhs) noexcept -> SIMDVector &{
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this /= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this /= SIMDVector{rhs};
    }
}

// MARK: Compound Modulus
//
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator%=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this %= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator%=(vector_type const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this %= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this %= SIMDVector{rhs};
    }
}

// MARK: Compound Bit Operators
//
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator&=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this &= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator&=(vector_type const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this &= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this &= SIMDVector{rhs};
    }
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator|=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this |= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator|=(vector_type const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this |= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this |= SIMDVector{rhs};
    }
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator^=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this ^= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator^=(vector_type const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this ^= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this ^= SIMDVector{rhs};
    }
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator<<=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this <<= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator<<=(vector_type const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this <<= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this <<= SIMDVector{rhs};
    }
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator>>=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this >>= *rhs;
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator>>=(vector_type const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    if (UTL::is_aligned<std::alignment_of<SIMDVector>::value>(rhs)) {
        return *this >>= static_cast<SIMDVector const&>(rhs);
    } else {
        return *this >>= SIMDVector{rhs};
    }
}

// MARK: Comparison
//
// If true, all bits are set (i.e., -1)
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator==(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this == *rhs;
    return result;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator!=(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this != *rhs;
    return result;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator<=(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this <= *rhs;
    return result;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator>=(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this >= *rhs;
    return result;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator<(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this < *rhs;
    return result;
}
template <class Type, long Size>
auto UTL::__4_::SIMDVector<Type, Size>::operator>(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this > *rhs;
    return result;
}

#endif /* UTLSIMDVector__N_hh */
