//
//  UTLArrayND__xD.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 8/25/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayND__xD_hh
#define UTLArrayND__xD_hh

#include <algorithm>
#include <stdexcept>
#include <string>

// MARK:- UTL::__4_::Array<ND>
//
template <class Type, long ND, long PadSize>
UTL::__4_::ArrayND<Type, ND, PadSize>::ArrayND(size_vector_type const& dims)
: ArrayND() {
    if (reduce_bit_or(dims < size_vector_type{0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative dimension size");
    }
    ArrayND{PaddedArray<Type, 0>(reduce_prod(dims + 2*PadSize)).storage(), dims}._swap(*this);
}
template <class Type, long ND, long PadSize>
UTL::__4_::ArrayND<Type, ND, PadSize>::ArrayND(size_vector_type const& dims, Type const& x)
: ArrayND() {
    if (reduce_bit_or(dims < size_vector_type{0})) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative dimension size");
    }
    ArrayND{PaddedArray<Type, 0>(reduce_prod(dims + 2*PadSize), x).storage(), dims}._swap(*this);
}

template <class Type, long ND, long PadSize>
auto UTL::__4_::ArrayND<Type, ND, PadSize>::operator=(ArraySliceND<Type, ND, PadSize> const &o)
-> ArrayND &{
    {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (reduce_bit_or(o.dims() != this->dims())) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array dimension sizes inconsistency");
        }
        std::copy(o.leaf_pad_begin(), o.leaf_pad_end(), this->leaf_pad_begin());
    }
    return *this;
}
template <class Type, long ND, long PadSize>
auto UTL::__4_::ArrayND<Type, ND, PadSize>::operator=(ArraySliceND<Type, ND, PadSize> &&o)
-> ArrayND &{
    {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (reduce_bit_or(o.dims() != this->dims())) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array dimension sizes inconsistency");
        }
        std::move(o.leaf_pad_begin(), o.leaf_pad_end(), this->leaf_pad_begin());
    }
    return *this;
}

template <class Type, long ND, long PadSize>
template <long I>
auto UTL::__4_::ArrayND<Type, ND, PadSize>::size() const noexcept
-> size_type {
    static_assert(I >= 0 && I < rank(), "dimension out of bound");
    if (I) { // if constexpr
        constexpr long i = I ? I - 1 : 0;
        return _Super::operator bool() ? _Super::front().template size<i>() : size_type{0};
    } else {
        return _Super::size();
    }
}
template <class Type, long ND, long PadSize>
template <long I>
auto UTL::__4_::ArrayND<Type, ND, PadSize>::max_size() const noexcept
-> size_type {
    static_assert(I >= 0 && I < rank(), "dimension out of bound");
    if (I) { // if constexpr
        constexpr long i = I ? I - 1 : 0;
        return _Super::operator bool() ? _Super::front().template max_size<i>() : size_type{0};
    } else {
        return _Super::max_size();
    }
}

template <class Type, long ND, long PadSize>
void UTL::__4_::ArrayND<Type, ND, PadSize>::swap(ArrayND& other)
{
    if (is_subarray() || other.is_subarray()) {
        throw std::logic_error(std::string(__PRETTY_FUNCTION__) + " - both *this and other should not be an element of a higher rank array");
    }
    _swap(other);
}

#endif /* UTLArrayND__xD_hh */
