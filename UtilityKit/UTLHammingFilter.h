//
//  UTLHammingFilter.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLHammingFilter_h
#define UTLHammingFilter_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <iterator>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief 1-, 2- and 3-D hamming filter.
     @discussion The input iterator should have at least one pad in both side.
     */
    template<unsigned _ND>
    struct HammingFilter;

#pragma mark 1D specialization:
    template<>
    struct HammingFilter<1> {
        // filtration:
        template<class _In, class _Out>
        void operator()(_In i, _In e, _Out o, std::random_access_iterator_tag const & = typename std::iterator_traits<_In>::iterator_category()) const noexcept {
            __filtrate(i, e, o);
        }
        template<class _In, class _Out>
        void __filtrate(_In i, _In e, _Out o) const noexcept {
            const typename std::iterator_traits<_In>::value_type quad(0.25);

            for (; i != e; ++i) { *o++ = (*(i-1) + (*i+*i) + *(i+1) ) * quad; }
        }
    };

#pragma mark 2D specialization:
    template<>
    struct HammingFilter<2> {
        // filtration:
        template<class _In, class _Out>
        void operator()(_In i, _In e, _Out o, std::random_access_iterator_tag const & = typename std::iterator_traits<_In>::iterator_category()) const noexcept {
            __filtrate_dim1(i, e, o);
        }

        // iteration over 1st dim:
        template<class _In, class _Out>
        void __filtrate_dim1(_In i, _In e, _Out o) const noexcept {
            [[maybe_unused]] typedef typename std::iterator_traits<_In >::value_type::const_iterator _I;
            [[maybe_unused]] typedef typename std::iterator_traits<_Out>::value_type::iterator _O;
            for (; i != e; ++i) {
                __filtrate_dim2((i  )->begin(), (i  )->  end(),
                                (i-1)->begin(), (i+1)->begin(),
                                (o++)->begin());
            }
        }
        template<class _In, class _Out>
        void __filtrate_dim2(_In _1, _In e1,
                             _In _0, _In _2,
                             _Out o) const noexcept {
            const typename std::iterator_traits<_In>::value_type _1_16(1/16.), two(2.), four(4.);

            for (; _1!=e1; ++_1, ++_0, ++_2) {
                *o++ = ((*_1                                  )*four +
                        (*(_1-1) + *(_1+1) + *_0     + *_2    )*two  +
                        (*(_0-1) + *(_0+1) + *(_2-1) + *(_2+1))
                        ) * _1_16;
            }
        }
    };

#pragma mark 3D specialization:
    template<>
    struct HammingFilter<3> {
        // filtration:
        template<class _In, class _Out>
        void operator()(_In i, _In e, _Out o, std::random_access_iterator_tag const &_ = typename std::iterator_traits<_In>::iterator_category()) const noexcept {
            __filtrate_dim1(i, e, o);
        }
        // iteration over 1st dim:
        template<class _In, class _Out>
        void __filtrate_dim1(_In i, _In e, _Out o) const noexcept {
            for (; i != e; ++i) {
                __filtrate_dim2((i  )->begin(), (i  )->  end(),
                                (i-1)->begin(), (i+1)->begin(),
                                (o++)->begin());
            }
        }
        // iteration over 2nd dim:
        template<class _In, class _Out>
        void __filtrate_dim2(_In _1, _In e1,
                             _In _0, _In _2,
                             _Out _o) const noexcept {
            [[maybe_unused]] typedef typename std::iterator_traits<_In >::value_type::const_iterator _I;
            [[maybe_unused]] typedef typename std::iterator_traits<_Out>::value_type::iterator _O;
            for (; _1 != e1; ++_1, ++_0, ++_2) {
                __filtrate_dim3((_1  )->begin(), (_1  )->  end(),
                                (_1-1)->begin(), (_1+1)->begin(),
                                (_0-1)->begin(), (_2-1)->begin(),
                                (_0  )->begin(), (_2  )->begin(),
                                (_0+1)->begin(), (_2+1)->begin(),
                                (_o++)->begin());
            }
        }
        template<class _In, class _Out>
        void __filtrate_dim3(_In _11, _In e11,
                             _In _10, _In _12,
                             _In _00, _In _20,
                             _In _01, _In _21,
                             _In _02, _In _22,
                             _Out _oo) const noexcept {
            const typename std::iterator_traits<_In>::value_type _1_64(1/64.), two(2.), four(4.), eight(8.);

            for (; _11 != e11; ++_11, ++_10, ++_12, ++_00, ++_20, ++_01, ++_21, ++_02, ++_22) {
                *_oo++ = ((*_11)*eight +
                          (*(_11-1) + *(_11+1) + *_10 + *_12 + *_01 + *_21) * four +
                          (*(_10-1) + *(_10+1) + *(_12-1) + *(_12+1) + *(_01-1) + *(_01+1) + *(_21-1) + *(_21+1) + *_00 + *_02 + *_20 + *_22) * two +
                          (*(_00-1) + *(_00+1) + *(_02-1) + *(_02+1) + *(_20-1) + *(_20+1) + *(_22-1) + *(_22+1))
                          ) * _1_64;
            }
        }
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <iterator>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief 1-, 2- and 3-D hamming filter.
     @discussion The input iterator should have at least one pad in both side.
     */
    template <long ND>
    struct HammingFilter;

    // MARK: HammingFilter<1D>
    //
    template <>
    struct HammingFilter<1> {
        // filtration:
        template <class In, class Out>
        void operator()(In i, In e, Out o, std::random_access_iterator_tag const & = typename std::iterator_traits<In>::iterator_category()) const noexcept {
            _filtrate(i, e, o);
        }

    private:
        template <class In, class Out>
        inline void _filtrate(In i, In e, Out o) const noexcept;
    };

    // MARK: HammingFilter<2D>
    //
    template <>
    struct HammingFilter<2> {
        // filtration:
        template <class In, class Out>
        void operator()(In i, In e, Out o, std::random_access_iterator_tag const & = typename std::iterator_traits<In>::iterator_category()) const noexcept {
            _filtrate_dim1(i, e, o);
        }

    private:
        // iteration over 1st dim:
        template <class In, class Out>
        inline void _filtrate_dim1(In i, In e, Out o) const noexcept;
        template <class In, class Out>
        inline void _filtrate_dim2(In _1, In e1, In _0, In _2, Out o) const noexcept;
    };

    // MARK: HammingFilter<3D>
    //
    template <>
    struct HammingFilter<3> {
        // filtration:
        template <class In, class Out>
        void operator()(In i, In e, Out o, std::random_access_iterator_tag const &_ = typename std::iterator_traits<In>::iterator_category()) const noexcept {
            _filtrate_dim1(i, e, o);
        }

    private:
        // iteration over 1st dim:
        template <class In, class Out>
        inline void _filtrate_dim1(In i, In e, Out o) const noexcept;
        // iteration over 2nd dim:
        template <class In, class Out>
        inline void _filtrate_dim2(In _1, In e1, In _0, In _2, Out _o) const noexcept;
        template <class In, class Out>
        inline void _filtrate_dim3(In _11, In e11, In _10, In _12, In _00, In _20, In _01, In _21, In _02, In _22, Out _oo) const noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLHammingFilter.hh>

#endif /* UTLHammingFilter_h */
