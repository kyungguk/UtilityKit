//
//  UTLTests-ThreadKit.mm
//  UtilityKitTests
//
//  Created by KYUNGGUK MIN on 9/24/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

#define UTILITYKIT_INLINE_VERSION 4
#include <UtilityKit/UTLPrinto.h>
#include <UtilityKit/ThreadKit.h>
#include <iostream>
#include <atomic>
#include <utility>
#include <mutex>
#include <string>
#include <thread>
#include <chrono>

@interface UTLTests_ThreadKit : XCTestCase

@end

@implementation UTLTests_ThreadKit

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testThread_HardwareConcurrency {
    using UTL::Thread;

    XCTAssert([[NSProcessInfo processInfo] processorCount] == Thread::hardware_concurrency(), @"[[NSProcessInfo processInfo] processorCount] = %ld, Thread::hardware_concurrency() = %d", [[NSProcessInfo processInfo] processorCount], Thread::hardware_concurrency());
}

- (void)testThread {
    using UTL::Thread;

    // join
    try {
        Thread th;

        std::atomic<bool> tested{false};
        struct {
            bool tested{false};
            void operator()(std::atomic<bool> &flag) {
                printo(std::cout, __PRETTY_FUNCTION__, '\n');
                flag = tested = true;
            }
        } f;
        th = Thread(f, std::ref(tested));
        th.join();
        XCTAssert(true == tested && false == f.tested);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // detatch
    try {
        std::mutex m;
        m.lock();
        std::atomic<bool> tested{false};
        Thread th([&m, &tested](void)->void {
            printo(std::cout, __PRETTY_FUNCTION__, '\n');
            tested = true;
            m.unlock();
        });
        Thread(std::move(th)).detach();
        std::lock_guard<decltype(m)> UTILITYKIT_UNIQUE_NAME(_)(m);
        XCTAssert(true == tested);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // swap
    try {
        std::string const hello{"Hello"};
        Thread a, b([](std::string const &s)->void {
            printo(std::cout, __PRETTY_FUNCTION__, ", ", s, '\n');
        }, hello);
        std::swap(a, b);
        a.join();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // swap
    try {
        Thread a, b([](void)->void {
            printo(std::cout, __PRETTY_FUNCTION__, '\n');
        });
        std::swap(a, b);
        a.join();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testMutex {
    using Lock = UTL::Mutex;
    using UTL::Thread;

    try {
        Lock lk;
        {
            LOCK_GUARD(lk);
            XCTAssert(lk.try_lock() == false);
        }
        XCTAssert(lk.try_lock() == true); // locked
        lk.unlock();

        constexpr long n = 100000;
        Lock &mx = lk;
        __block unsigned long sum1{};
        dispatch_apply(n, dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^(size_t const i) {
            LOCK_GUARD(mx);
            sum1 += i + 1;
        });
        unsigned long sum2{};
        for (unsigned i = 0; i < n; ++i) {
            sum2 += i + 1;
        }
        XCTAssert(sum1 == sum2, @"sum1 = %ld, sum2 = %ld", sum1, sum2);
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSpinLock {
    using Lock = UTL::SpinLock;

    try {
        Lock lk;
        {
            LOCK_GUARD(lk);
            XCTAssert(lk.try_lock() == false);
        }
        XCTAssert(lk.try_lock() == true); // locked
        lk.unlock();

        constexpr long n = 100000;
        Lock &mx = lk;
        __block unsigned long sum1{};
        dispatch_apply(n, dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^(size_t const i) {
            LOCK_GUARD(mx);
            sum1 += i + 1;
        });
        unsigned long sum2{};
        for (unsigned i = 0; i < n; ++i) {
            sum2 += i + 1;
        }
        XCTAssert(sum1 == sum2, @"sum1 = %ld, sum2 = %ld", sum1, sum2);
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testCondV_Wait {
    using UTL::Mutex;
    using UTL::CondV;
    using UTL::Thread;

    // wait
    try {
        Mutex mx[1];
        CondV cv[1];
        long selector{0};
        long woken_up{0};
        Thread th1([&selector, &woken_up](Mutex *mx, CondV *cv)->void {
            LOCK_GUARD(*mx);
            cv[0].wait(*mx, [&selector]()->bool { return 1 == selector; });
            woken_up = 1;
        }, mx, cv);
        Thread th2([&selector, &woken_up](Mutex *mx, CondV *cv)->void {
            LOCK_GUARD(*mx);
            cv[0].wait(*mx, [&selector]()->bool { return 2 == selector; });
            woken_up = 2;
        }, mx, cv);
        std::this_thread::sleep_for(std::chrono::seconds{1});
        {
            LOCK_GUARD(*mx);
            selector = 1;
        }
        cv[0].notify_all();
        th1.join();
        XCTAssert(1 == woken_up);
        {
            LOCK_GUARD(*mx);
            selector = 2;
        }
        cv[0].notify_one();
        th2.join();
        XCTAssert(2 == woken_up);
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // wait for
    try {
        Mutex mx;
        CondV cv;
        bool flag[2]{false, true};
        Thread th1([&mx, &cv](bool *flag)->void {
            LOCK_GUARD(mx);
            flag[0] = cv.wait_for(mx, std::chrono::seconds{3}, []()->bool { return true; });
        }, flag);
        Thread th2([&mx, &cv](bool *flag)->void {
            LOCK_GUARD(mx);
            flag[1] = cv.wait_for(mx, std::chrono::seconds{3}, []()->bool { return false; });
        }, flag);
        std::this_thread::sleep_for(std::chrono::seconds{1});
        cv.notify_all();
        th1.join();
        th2.join();
        XCTAssert(flag[0] && !flag[1]);
    } catch (std::exception& e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSema_CondV {
    using UTL::Thread;
    using Sema = UTL::Sema<UTL::Mutex>;

    try {
        constexpr long n = 10;
        Sema sem(0);
        XCTAssert(0 == sem, @"sem = %ld", long(sem));
        Thread th([&sem]()->void {
            for (long i = 0; i < n; ++i) {
                sem.wait();
            }
        });
        for (long i = 0; i < n; ++i) {
            sem.post();
        }
        th.join();
        XCTAssert(0 == sem, @"sem = %ld", long(sem));

        Thread workers[n];
        for (Thread &worker : workers) {
            worker = Thread([&sem]() { sem.post(); });
        }
        for (Thread &worker : workers) {
            worker.join();
        }
        XCTAssert(n == sem, @"sem = %ld", long(sem));

        for (Thread &worker : workers) {
            worker = Thread([&sem]() { sem.wait(); sleep(1); sem.wait(); });
            sem.post();
        }
        for (Thread &worker : workers) {
            worker.join();
        }
        XCTAssert(0 == sem, @"sem = %ld", long(sem));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSema_SpinLock {
    using UTL::Thread;
    using Sema = UTL::Sema<UTL::SpinLock>;

    try {
        constexpr long n = 10;
        Sema sem(0);
        XCTAssert(0 == sem, @"sem = %ld", long(sem));
        Thread th([&sem]()->void {
            for (long i = 0; i < n; ++i) {
                sem.wait();
            }
        });
        for (long i = 0; i < n; ++i) {
            sem.post();
        }
        th.join();
        XCTAssert(0 == sem, @"sem = %ld", long(sem));

        Thread workers[n];
        for (Thread &worker : workers) {
            worker = Thread([&sem]() { sem.post(); });
        }
        for (Thread &worker : workers) {
            worker.join();
        }
        XCTAssert(n == sem, @"sem = %ld", long(sem));

        for (Thread &worker : workers) {
            worker = Thread([&sem]() { sem.wait(); sleep(1); sem.wait(); });
            sem.post();
        }
        for (Thread &worker : workers) {
            worker.join();
        }
        XCTAssert(0 == sem, @"sem = %ld", long(sem));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSemaPerformance_CondV {
    using UTL::Thread;
    using Sema = UTL::Sema<UTL::Mutex>;

    [self measureBlock:^{
        try {
            constexpr long n = 10000;
            Sema sem(0);
            Thread th([&sem]()->void {
                for (long i = 0; i < n; ++i) {
                    sem.wait();
                }
            });
            for (long i = 0; i < n; ++i) {
                sem.post();
            }
            th.join();
            XCTAssert(0 == sem, @"sem = %ld", long(sem));
        } catch (std::exception &e) {
            XCTAssert(false, @"%s", e.what());
            return;
        }
    }];
}

- (void)testSemaPerformance_SpinLock {
    using UTL::Thread;
    using Sema = UTL::Sema<UTL::SpinLock>;

    [self measureBlock:^{
        try {
            constexpr long n = 10000;
            Sema sem(0);
            Thread th([&sem]()->void {
                for (long i = 0; i < n; ++i) {
                    sem.wait();
                }
            });
            for (long i = 0; i < n; ++i) {
                sem.post();
            }
            th.join();
            XCTAssert(0 == sem, @"sem = %ld", long(sem));
        } catch (std::exception &e) {
            XCTAssert(false, @"%s", e.what());
            return;
        }
    }];
}

- (void)testBlockQueue_CondV {
    using UTL::Thread;
    using BlockQueue = UTL::BlockQueue<UTL::Mutex>;

    [self measureBlock:^{
        try {
            constexpr long n = 10000;
            BlockQueue q;
            XCTAssert(!q.try_dequeue());
            Thread worker([&q]() {
                for (long i = 0; i < n; ++i) {
                    q.dequeue()();
                }
            });

            long sum1 = 0, sum2 = 0;
            for (long i = 0; i < n; ++i) {
                q.enqueue([&sum1, i]() {
                    sum1 += i + 1;
                });
                sum2 += i + 1;
            }
            worker.join();
            XCTAssert(sum1 == sum2, @"sum1 = %ld, sum2 = %ld", sum1, sum2);
        } catch (std::exception &e) {
            XCTAssert(false, @"%s", e.what());
            return;
        }
    }];
}

- (void)testBlockQueue_SpinLock {
    using UTL::Thread;
    using BlockQueue = UTL::BlockQueue<UTL::SpinLock>;

    [self measureBlock:^{
        try {
            constexpr long n = 10000;
            BlockQueue q;
            XCTAssert(!q.try_dequeue());
            Thread worker([&q]() {
                for (long i = 0; i < n; ++i) {
                    q.dequeue()();
                }
            });

            long sum1 = 0, sum2 = 0;
            for (long i = 0; i < n; ++i) {
                q.enqueue([&sum1, i]() {
                    sum1 += i + 1;
                });
                sum2 += i + 1;
            }
            worker.join();
            XCTAssert(sum1 == sum2, @"sum1 = %ld, sum2 = %ld", sum1, sum2);
        } catch (std::exception &e) {
            XCTAssert(false, @"%s", e.what());
            return;
        }
    }];
}

- (void)testThreadQueue {
    using UTL::ThreadQueue;

    [self measureBlock:^{
        try {
            constexpr long n1 = 5;
            std::atomic_ulong sum1{};
            ThreadQueue::apply(n1, [&sum1](unsigned long i) {
                ++sum1;
            });
            XCTAssert(n1 == sum1, @"n1 = %ld, sum1 = %ld", n1, sum1.load());

            constexpr long n2 = 100000;
            std::atomic_ulong sum2{};
            ThreadQueue::apply(n2, [&sum2](unsigned long i) {
                ++sum2;
            });
            XCTAssert(n2 == sum2, @"n2 = %ld, sum2 = %ld", n2, sum2.load());
        } catch (std::exception &e) {
            XCTAssert(false, @"%s", e.what());
            return;
        }
    }];
}

@end
