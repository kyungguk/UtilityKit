//
//  UTLNewtonRootFinder.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/22/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLNewtonRootFinder_hh
#define UTLNewtonRootFinder_hh

// MARK:- Version 4
//
#include <algorithm>
#include <cmath>

template <class F_dF, class StepMonitor>
auto UTL::__4_::NewtonRootFinder<double>::operator()(F_dF&& f_df, value_type x0, StepMonitor&& monitor) const
-> optional_type {
    if (!std::isfinite(x0)) {
        return {};
    }

    value_type x1 = x0;
    for (unsigned i = 0; i < max_iterations && monitor(i, x1); ++i) {
        x1 = x0 - multiplicity*f_df(x0);
        value_type const h = x1 - x0;
        if (!std::isfinite(h)) {
            return {};
        } else if (std::abs(h) < abstol || std::abs(h) < reltol*std::max(std::abs(x1), std::abs(x0))) {
            return x1;
        } else {
            x0 = x1;
        }
    }

    return {};
}

#endif /* UTLNewtonRootFinder_hh */
