//
//  UTLTests-NumericKit.mm
//  UtilityKitTests
//
//  Created by KYUNGGUK MIN on 10/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

#define UTILITYKIT_INLINE_VERSION 4
#include <UtilityKit/NumericKit.h>
#include <UtilityKit/AuxiliaryKit.h>
#include <sstream>
#include <iostream>
#include <memory>
#include <numeric>

namespace {
    inline long irand(unsigned const max) {
        long const i = arc4random_uniform(max);
        return i <= 0 ? irand(max) : i;
    }
}

@interface UTLTests_NumericKit : XCTestCase

@end

@implementation UTLTests_NumericKit

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testVector_N {
    using UTL::Vector;
    constexpr long N = 100;

    try {
        Vector<std::unique_ptr<long>, N> v1, v2;
        for (auto &x : v2) {
            x.reset(new long);
        }
        v1 = std::move(v2);
        *v1.front() = N;
        XCTAssert(!v2.front() && N == *v1.front());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        constexpr Vector<long, N> constexpr_v{};
        XCTAssert((N == std::tuple_size<Vector<long, N>>::value) && (std::is_same<std::tuple_element<0, Vector<long, N>>::type, long>::value));
        XCTAssert(N == constexpr_v.max_size() && !std::get<0>(constexpr_v) && !std::get<N-1>(constexpr_v));
        XCTAssert(N == std::distance(constexpr_v.cbegin(), constexpr_v.cend()));
        for (auto const& x : constexpr_v) {
            XCTAssert(!x);
        }
        for (auto it = constexpr_v.crbegin(); it != constexpr_v.crend(); ++it) {
            XCTAssert(!*it);
        }

        Vector<std::string, N> v1{__PRETTY_FUNCTION__}, v2;
        v2 = std::move(v1);
        XCTAssert(std::get<0>(v1).empty() && std::get<N-1>(v1).empty());
        XCTAssert(v2[0] == __PRETTY_FUNCTION__ && v2.back() == __PRETTY_FUNCTION__);
        v2.fill("a");
        v1.front() = __PRETTY_FUNCTION__;
        std::swap(v1, v2);
        XCTAssert(v2[0] == __PRETTY_FUNCTION__);
        for (auto it = v1.cbegin() + 1; it != v1.cend(); ++it) {
            XCTAssert("a" == *it);
        }
        std::array<std::string, N> a2;
        a2 = std::move(static_cast<decltype(a2)&>(v2));
        XCTAssert(v2[0].empty() && a2[0] == __PRETTY_FUNCTION__);

        Vector<long, N> v3, v4;
        v3 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        v4.swap(v3);
        v3.fill(0);
        long sum{};
        for (long i = 0; i < N; ++i) {
            sum += v4[i];
            XCTAssert(!v3[i]);
        }
        XCTAssert(45 == sum);

        v3.lo().fill(1);
        v3.hi().fill(2);
        XCTAssert(v3.front() == 1 && v3.back() == 2);
        v3.front() = v3.back() = -1;
        XCTAssert(v3.most().front() == -1 && v3.rest().back() == -1);
        auto const& v5 = v3;
        XCTAssert(v5.most().front() == -1 && v5.rest().back() == -1);
        printo(std::cout, v3, "\n");

        constexpr long rank = decltype(v3)::rank();
        XCTAssert(rank == 1 && v3.dims()[rank-1] == v3.size() && v3.dims()[rank-1] == v3.max_dims()[rank-1]);

        printo(std::cout, v4, "\n");

        for (long i = 0; i < v3.size(); ++i) {
            v3[i] = i + 1;
        }
        auto const& v6 = v3.append(v3.size() + 1), v7 = v3.prepend(0);
        XCTAssert(v3.size() + 1 == v6.size() && v3.size() + 1 == v7.size());
        for (long i = 0; i <= v3.size(); ++i) {
            XCTAssert(v6[i] == i + 1);
            XCTAssert(v7[i] == i);
        }

        Vector<double, N> d;
        d = constexpr_v;
        printo(std::cout, d, "\n");

        Vector<std::string, N> s;
        s = Vector<char const*, N>{"a"};
        printo(std::cout, s, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        Vector<long, N> a, b, c, d;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i]));
            XCTAssert(d[i] == (a[i] != b[i]));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i]));
            XCTAssert(d[i] == (a[i] >= b[i]));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i]));
            XCTAssert(d[i] == (a[i] > b[i]));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));

        auto const &const_a = a;
        XCTAssert(UTL::reduce_bit_and(const_a.take<1>() == a.front()) && UTL::reduce_bit_and(const_a.take<-1>() == a.back()));
        XCTAssert(UTL::reduce_bit_and(a.take<1>() == const_a.front()) && UTL::reduce_bit_and(a.take<-1>() == const_a.back()));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<1>() == a.rest()) && UTL::reduce_bit_and(const_a.drop<-1>() == a.most()));
        XCTAssert(UTL::reduce_bit_and(a.drop<1>() == const_a.rest()) && UTL::reduce_bit_and(a.drop<-1>() == const_a.most()));

        XCTAssert(UTL::reduce_bit_and(const_a.take<N>() == a) && UTL::reduce_bit_and(const_a.take<-N>() == a));
        XCTAssert(UTL::reduce_bit_and(a.take<N>() == const_a) && UTL::reduce_bit_and(a.take<-N>() == const_a));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<0>() == a) && UTL::reduce_bit_and(const_a.drop<0>() == a));
        XCTAssert(UTL::reduce_bit_and(a.drop<0>() == const_a) && UTL::reduce_bit_and(a.drop<0>() == const_a));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testVector_1 {
    using UTL::Vector;
    constexpr long N = 1;

    try {
        Vector<std::unique_ptr<long>, N> v1, v2;
        for (auto &x : v2) {
            x.reset(new long);
        }
        v1 = std::move(v2);
        *v1.front() = N;
        XCTAssert(!v2.front() && N == *v1.front());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        constexpr Vector<long, N> constexpr_v{};// constexpr_v{0};
        XCTAssert(N == constexpr_v.max_size() && !std::get<0>(constexpr_v) && !std::get<N-1>(constexpr_v));
        XCTAssert(N == std::distance(constexpr_v.cbegin(), constexpr_v.cend()));
        for (auto const& x : constexpr_v) {
            XCTAssert(!x);
        }
        for (auto it = constexpr_v.crbegin(); it != constexpr_v.crend(); ++it) {
            XCTAssert(!*it);
        }

        Vector<std::string, N> v1{__PRETTY_FUNCTION__}, v2;
        v2 = std::move(v1);
        XCTAssert(std::get<0>(v1).empty() && std::get<N-1>(v1).empty());
        XCTAssert(v2[0] == __PRETTY_FUNCTION__ && v2.back() == __PRETTY_FUNCTION__);
        v2.fill("a");
        std::swap(v1, v2);
        for (auto it = v1.cbegin(); it != v1.cend(); ++it) {
            XCTAssert("a" == *it);
        }
        std::array<std::string, N> a1;
        a1 = std::move(static_cast<decltype(a1)&>(v1));
        XCTAssert(v1[0].empty() && a1[0] == "a");

        Vector<long, N> v3, v4;
        v3 = {1};
        v4.swap(v3);
        v3.fill(0);
        XCTAssert(1 == v4.front() && N == v4.back());
        for (long i = 0; i < N; ++i) {
            XCTAssert(!v3[i] && i+1 == v4[i]);
        }
        printo(std::cout, v3, "\n");

        constexpr long rank = decltype(v3)::rank();
        XCTAssert(rank == 1 && v3.dims()[rank-1] == v3.size() && v3.dims()[rank-1] == v3.max_dims()[rank-1]);

        for (long i = 0; i < v3.size(); ++i) {
            v3[i] = i + 1;
        }
        auto const& v6 = v3.append(v3.size() + 1), v7 = v3.prepend(0);
        XCTAssert(v3.size() + 1 == v6.size() && v3.size() + 1 == v7.size());
        for (long i = 0; i <= v3.size(); ++i) {
            XCTAssert(v6[i] == i + 1);
            XCTAssert(v7[i] == i);
        }

        Vector<double, N> d;
        static_cast<double &>(d) = constexpr_v.x;
        XCTAssert(static_cast<double const&>(d) == constexpr_v.x);
        printo(std::cout, d, "\n");

        Vector<std::string, N> s;
        s = Vector<char const*, N>{"a"};
        printo(std::cout, s, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        Vector<long, N> a, b, c, d;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i]));
            XCTAssert(d[i] == (a[i] != b[i]));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i]));
            XCTAssert(d[i] == (a[i] >= b[i]));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i]));
            XCTAssert(d[i] == (a[i] > b[i]));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));

        auto const &const_a = a;
        XCTAssert(UTL::reduce_bit_and(const_a.take<N>() == a) && UTL::reduce_bit_and(const_a.take<-N>() == a));
        XCTAssert(UTL::reduce_bit_and(a.take<N>() == const_a) && UTL::reduce_bit_and(a.take<-N>() == const_a));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<0>() == a) && UTL::reduce_bit_and(const_a.drop<0>() == a));
        XCTAssert(UTL::reduce_bit_and(a.drop<0>() == const_a) && UTL::reduce_bit_and(a.drop<0>() == const_a));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testVector_2 {
    using UTL::Vector;
    constexpr long N = 2;

    try {
        Vector<std::unique_ptr<long>, N> v1, v2;
        for (auto &x : v2) {
            x.reset(new long);
        }
        v1 = std::move(v2);
        *v1.front() = N;
        XCTAssert(!v2.front() && N == *v1.front());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        constexpr Vector<long, N> constexpr_v{}; // constexpr_v{0, 0};
        XCTAssert(N == constexpr_v.max_size() && !std::get<0>(constexpr_v) && !std::get<N-1>(constexpr_v));
        XCTAssert(N == std::distance(constexpr_v.cbegin(), constexpr_v.cend()));
        for (auto const& x : constexpr_v) {
            XCTAssert(!x);
        }
        for (auto it = constexpr_v.crbegin(); it != constexpr_v.crend(); ++it) {
            XCTAssert(!*it);
        }

        Vector<std::string, N> v1, v2;
        v1 = {__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        v2 = std::move(v1);
        XCTAssert(std::get<0>(v1).empty() && std::get<N-1>(v1).empty());
        XCTAssert(v2[0] == __PRETTY_FUNCTION__ && v2.back() == __PRETTY_FUNCTION__);
        v2.fill("a");
        v1.front() = __PRETTY_FUNCTION__;
        std::swap(v1, v2);
        XCTAssert(v2[0] == __PRETTY_FUNCTION__);
        for (auto it = v1.cbegin() + 1; it != v1.cend(); ++it) {
            XCTAssert("a" == *it);
        }
        std::array<std::string, N> a2;
        a2 = std::move(static_cast<decltype(a2)&>(v2));
        XCTAssert(v2[0].empty() && a2[0] == __PRETTY_FUNCTION__);

        Vector<long, N> v3, v4;
        v3 = {1, 2};
        v4.swap(v3);
        v3.fill(0);
        XCTAssert(1 == v4.front() && N == v4.back());
        for (long i = 0; i < N; ++i) {
            XCTAssert(!v3[i] && i+1 == v4[i]);
        }

        v3.lo().fill(1);
        v3.hi().fill(2);
        XCTAssert(v3.front() == 1 && v3.back() == 2);
        v3.front() = v3.back() = -1;
        XCTAssert(v3.most().front() == -1 && v3.rest().back() == -1);
        auto const& v5 = v3;
        XCTAssert(v5.most().front() == -1 && v5.rest().back() == -1);
        printo(std::cout, v3, "\n");

        constexpr long rank = decltype(v3)::rank();
        XCTAssert(rank == 1 && v3.dims()[rank-1] == v3.size() && v3.dims()[rank-1] == v3.max_dims()[rank-1]);

        for (long i = 0; i < v3.size(); ++i) {
            v3[i] = i + 1;
        }
        auto const& v6 = v3.append(v3.size() + 1), v7 = v3.prepend(0);
        XCTAssert(v3.size() + 1 == v6.size() && v3.size() + 1 == v7.size());
        for (long i = 0; i <= v3.size(); ++i) {
            XCTAssert(v6[i] == i + 1);
            XCTAssert(v7[i] == i);
        }

        Vector<double, N> d;
        d = constexpr_v;
        printo(std::cout, d, "\n");

        Vector<std::string, N> s;
        s = Vector<char const*, N>{"a", "b"};
        printo(std::cout, s, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        Vector<long, N> a, b, c, d;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i]));
            XCTAssert(d[i] == (a[i] != b[i]));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i]));
            XCTAssert(d[i] == (a[i] >= b[i]));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i]));
            XCTAssert(d[i] == (a[i] > b[i]));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));

        auto const &const_a = a;
        XCTAssert(UTL::reduce_bit_and(const_a.take<1>() == a.front()) && UTL::reduce_bit_and(const_a.take<-1>() == a.back()));
        XCTAssert(UTL::reduce_bit_and(a.take<1>() == const_a.front()) && UTL::reduce_bit_and(a.take<-1>() == const_a.back()));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<1>() == a.rest()) && UTL::reduce_bit_and(const_a.drop<-1>() == a.most()));
        XCTAssert(UTL::reduce_bit_and(a.drop<1>() == const_a.rest()) && UTL::reduce_bit_and(a.drop<-1>() == const_a.most()));

        XCTAssert(UTL::reduce_bit_and(const_a.take<N>() == a) && UTL::reduce_bit_and(const_a.take<-N>() == a));
        XCTAssert(UTL::reduce_bit_and(a.take<N>() == const_a) && UTL::reduce_bit_and(a.take<-N>() == const_a));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<0>() == a) && UTL::reduce_bit_and(const_a.drop<0>() == a));
        XCTAssert(UTL::reduce_bit_and(a.drop<0>() == const_a) && UTL::reduce_bit_and(a.drop<0>() == const_a));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testVector_3 {
    using UTL::Vector;
    constexpr long N = 3;

    try {
        Vector<std::unique_ptr<long>, N> v1, v2;
        for (auto &x : v2) {
            x.reset(new long);
        }
        v1 = std::move(v2);
        *v1.front() = N;
        XCTAssert(!v2.front() && N == *v1.front());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        constexpr Vector<long, N> constexpr_v{}; // constexpr_v{0, 0, 0};
        XCTAssert(N == constexpr_v.max_size() && !std::get<0>(constexpr_v) && !std::get<N-1>(constexpr_v));
        XCTAssert(N == std::distance(constexpr_v.cbegin(), constexpr_v.cend()));
        for (auto const& x : constexpr_v) {
            XCTAssert(!x);
        }
        for (auto it = constexpr_v.crbegin(); it != constexpr_v.crend(); ++it) {
            XCTAssert(!*it);
        }

        Vector<std::string, N> v1, v2;
        v1 = {__PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        v2 = std::move(v1);
        XCTAssert(std::get<0>(v1).empty() && std::get<N-1>(v1).empty());
        XCTAssert(v2[0] == __PRETTY_FUNCTION__ && v2.back() == __PRETTY_FUNCTION__);
        v2.fill("a");
        v1.front() = __PRETTY_FUNCTION__;
        std::swap(v1, v2);
        XCTAssert(v2[0] == __PRETTY_FUNCTION__);
        for (auto it = v1.cbegin() + 1; it != v1.cend(); ++it) {
            XCTAssert("a" == *it);
        }
        std::array<std::string, N> a2;
        a2 = std::move(static_cast<decltype(a2)&>(v2));
        XCTAssert(v2[0].empty() && a2[0] == __PRETTY_FUNCTION__);

        Vector<long, N> v3, v4;
        v3 = {1, 2, 3};
        v4.swap(v3);
        v3.fill(0);
        XCTAssert(1 == v4.front() && N == v4.back());
        for (long i = 0; i < N; ++i) {
            XCTAssert(!v3[i] && i+1 == v4[i]);
        }

        v3.lo().fill(1);
        v3.hi().fill(2);
        XCTAssert(v3.front() == 1 && v3.back() == 2);
        v3.front() = v3.back() = -1;
        XCTAssert(v3.most().front() == -1 && v3.rest().back() == -1);
        auto const& v5 = v3;
        XCTAssert(v5.most().front() == -1 && v5.rest().back() == -1);
        printo(std::cout, v3, "\n");

        constexpr long rank = decltype(v3)::rank();
        XCTAssert(rank == 1 && v3.dims()[rank-1] == v3.size() && v3.dims()[rank-1] == v3.max_dims()[rank-1]);

        for (long i = 0; i < v3.size(); ++i) {
            v3[i] = i + 1;
        }
        auto const& v6 = v3.append(v3.size() + 1), v7 = v3.prepend(0);
        XCTAssert(v3.size() + 1 == v6.size() && v3.size() + 1 == v7.size());
        for (long i = 0; i <= v3.size(); ++i) {
            XCTAssert(v6[i] == i + 1);
            XCTAssert(v7[i] == i);
        }

        Vector<double, N> d;
        d = constexpr_v;
        printo(std::cout, d, "\n");

        Vector<std::string, N> s;
        s = Vector<char const*, N>{"a", "b", "c"};
        printo(std::cout, s, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        Vector<long, N> a, b, c, d;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i]));
            XCTAssert(d[i] == (a[i] != b[i]));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i]));
            XCTAssert(d[i] == (a[i] >= b[i]));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i]));
            XCTAssert(d[i] == (a[i] > b[i]));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));

        auto const &const_a = a;
        XCTAssert(UTL::reduce_bit_and(const_a.take<1>() == a.front()) && UTL::reduce_bit_and(const_a.take<-1>() == a.back()));
        XCTAssert(UTL::reduce_bit_and(a.take<1>() == const_a.front()) && UTL::reduce_bit_and(a.take<-1>() == const_a.back()));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<1>() == a.rest()) && UTL::reduce_bit_and(const_a.drop<-1>() == a.most()));
        XCTAssert(UTL::reduce_bit_and(a.drop<1>() == const_a.rest()) && UTL::reduce_bit_and(a.drop<-1>() == const_a.most()));

        XCTAssert(UTL::reduce_bit_and(const_a.take<N>() == a) && UTL::reduce_bit_and(const_a.take<-N>() == a));
        XCTAssert(UTL::reduce_bit_and(a.take<N>() == const_a) && UTL::reduce_bit_and(a.take<-N>() == const_a));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<0>() == a) && UTL::reduce_bit_and(const_a.drop<0>() == a));
        XCTAssert(UTL::reduce_bit_and(a.drop<0>() == const_a) && UTL::reduce_bit_and(a.drop<0>() == const_a));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testVector_4 {
    using UTL::Vector;
    constexpr long N = 4;

    try {
        Vector<std::unique_ptr<long>, N> v1, v2;
        for (auto &x : v2) {
            x.reset(new long);
        }
        v1 = std::move(v2);
        *v1.front() = N;
        XCTAssert(!v2.front() && N == *v1.front());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        constexpr Vector<long, N> constexpr_v{}; // constexpr_v{0, 0, 0, 0};
        XCTAssert(N == constexpr_v.max_size() && !std::get<0>(constexpr_v) && !std::get<N-1>(constexpr_v));
        XCTAssert(N == std::distance(constexpr_v.cbegin(), constexpr_v.cend()));
        for (auto const& x : constexpr_v) {
            XCTAssert(!x);
        }
        for (auto it = constexpr_v.crbegin(); it != constexpr_v.crend(); ++it) {
            XCTAssert(!*it);
        }

        Vector<std::string, N> v1, v2;
        v1 = {__PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        v2 = std::move(v1);
        XCTAssert(std::get<0>(v1).empty() && std::get<N-1>(v1).empty());
        XCTAssert(v2[0] == __PRETTY_FUNCTION__ && v2.back() == __PRETTY_FUNCTION__);
        v2.fill("a");
        v1.front() = __PRETTY_FUNCTION__;
        std::swap(v1, v2);
        XCTAssert(v2[0] == __PRETTY_FUNCTION__);
        for (auto it = v1.cbegin() + 1; it != v1.cend(); ++it) {
            XCTAssert("a" == *it);
        }
        std::array<std::string, N> a2;
        a2 = std::move(static_cast<decltype(a2)&>(v2));
        XCTAssert(v2[0].empty() && a2[0] == __PRETTY_FUNCTION__);

        Vector<long, N> v3, v4;
        v3 = {1, 2, 3, 4};
        v4.swap(v3);
        v3.fill(0);
        XCTAssert(1 == v4.front() && N == v4.back());
        for (long i = 0; i < N; ++i) {
            XCTAssert(!v3[i] && i+1 == v4[i]);
        }

        v3.lo().fill(1);
        v3.hi().fill(2);
        XCTAssert(v3.front() == 1 && v3.back() == 2);
        v3.front() = v3.back() = -1;
        XCTAssert(v3.most().front() == -1 && v3.rest().back() == -1);
        auto const& v5 = v3;
        XCTAssert(v5.most().front() == -1 && v5.rest().back() == -1);
        printo(std::cout, v3, "\n");

        constexpr long rank = decltype(v3)::rank();
        XCTAssert(rank == 1 && v3.dims()[rank-1] == v3.size() && v3.dims()[rank-1] == v3.max_dims()[rank-1]);

        for (long i = 0; i < v3.size(); ++i) {
            v3[i] = i + 1;
        }
        auto const& v6 = v3.append(v3.size() + 1), v7 = v3.prepend(0);
        XCTAssert(v3.size() + 1 == v6.size() && v3.size() + 1 == v7.size());
        for (long i = 0; i <= v3.size(); ++i) {
            XCTAssert(v6[i] == i + 1);
            XCTAssert(v7[i] == i);
        }

        Vector<double, N> d;
        d = constexpr_v;
        printo(std::cout, d, "\n");

        Vector<std::string, N> s;
        s = Vector<char const*, N>{"a", "b", "c", "d"};
        printo(std::cout, s, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        Vector<long, N> a, b, c, d;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i]));
            XCTAssert(d[i] == (a[i] != b[i]));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i]));
            XCTAssert(d[i] == (a[i] >= b[i]));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i]));
            XCTAssert(d[i] == (a[i] > b[i]));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));

        auto const &const_a = a;
        XCTAssert(UTL::reduce_bit_and(const_a.take<1>() == a.front()) && UTL::reduce_bit_and(const_a.take<-1>() == a.back()));
        XCTAssert(UTL::reduce_bit_and(a.take<1>() == const_a.front()) && UTL::reduce_bit_and(a.take<-1>() == const_a.back()));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<1>() == a.rest()) && UTL::reduce_bit_and(const_a.drop<-1>() == a.most()));
        XCTAssert(UTL::reduce_bit_and(a.drop<1>() == const_a.rest()) && UTL::reduce_bit_and(a.drop<-1>() == const_a.most()));

        XCTAssert(UTL::reduce_bit_and(const_a.take<N>() == a) && UTL::reduce_bit_and(const_a.take<-N>() == a));
        XCTAssert(UTL::reduce_bit_and(a.take<N>() == const_a) && UTL::reduce_bit_and(a.take<-N>() == const_a));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<0>() == a) && UTL::reduce_bit_and(const_a.drop<0>() == a));
        XCTAssert(UTL::reduce_bit_and(a.drop<0>() == const_a) && UTL::reduce_bit_and(a.drop<0>() == const_a));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testVector_5 {
    using UTL::Vector;
    constexpr long N = 5;

    try {
        Vector<std::unique_ptr<long>, N> v1, v2;
        for (auto &x : v2) {
            x.reset(new long);
        }
        v1 = std::move(v2);
        *v1.front() = N;
        XCTAssert(!v2.front() && N == *v1.front());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        constexpr Vector<long, N> constexpr_v{}; // constexpr_v{0, 0, 0, 0, 0};
        XCTAssert(N == constexpr_v.max_size() && !std::get<0>(constexpr_v) && !std::get<N-1>(constexpr_v));
        XCTAssert(N == std::distance(constexpr_v.cbegin(), constexpr_v.cend()));
        for (auto const& x : constexpr_v) {
            XCTAssert(!x);
        }
        for (auto it = constexpr_v.crbegin(); it != constexpr_v.crend(); ++it) {
            XCTAssert(!*it);
        }

        Vector<std::string, N> v1, v2;
        v1 = {__PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        v2 = std::move(v1);
        XCTAssert(std::get<0>(v1).empty() && std::get<N-1>(v1).empty());
        XCTAssert(v2[0] == __PRETTY_FUNCTION__ && v2.back() == __PRETTY_FUNCTION__);
        v2.fill("a");
        v1.front() = __PRETTY_FUNCTION__;
        std::swap(v1, v2);
        XCTAssert(v2[0] == __PRETTY_FUNCTION__);
        for (auto it = v1.cbegin() + 1; it != v1.cend(); ++it) {
            XCTAssert("a" == *it);
        }
        std::array<std::string, N> a2;
        a2 = std::move(static_cast<decltype(a2)&>(v2));
        XCTAssert(v2[0].empty() && a2[0] == __PRETTY_FUNCTION__);

        Vector<long, N> v3, v4;
        v3 = {1, 2, 3, 4, 5};
        v4.swap(v3);
        v3.fill(0);
        XCTAssert(1 == v4.front() && N == v4.back());
        for (long i = 0; i < N; ++i) {
            XCTAssert(!v3[i] && i+1 == v4[i]);
        }

        v3.lo().fill(1);
        v3.hi().fill(2);
        XCTAssert(v3.front() == 1 && v3.back() == 2);
        v3.front() = v3.back() = -1;
        XCTAssert(v3.most().front() == -1 && v3.rest().back() == -1);
        auto const& v5 = v3;
        XCTAssert(v5.most().front() == -1 && v5.rest().back() == -1);
        printo(std::cout, v3, "\n");

        constexpr long rank = decltype(v3)::rank();
        XCTAssert(rank == 1 && v3.dims()[rank-1] == v3.size() && v3.dims()[rank-1] == v3.max_dims()[rank-1]);

        for (long i = 0; i < v3.size(); ++i) {
            v3[i] = i + 1;
        }
        auto const& v6 = v3.append(v3.size() + 1), v7 = v3.prepend(0);
        XCTAssert(v3.size() + 1 == v6.size() && v3.size() + 1 == v7.size());
        for (long i = 0; i <= v3.size(); ++i) {
            XCTAssert(v6[i] == i + 1);
            XCTAssert(v7[i] == i);
        }

        Vector<double, N> d;
        d = constexpr_v;
        printo(std::cout, d, "\n");

        Vector<std::string, N> s;
        s = Vector<char const*, N>{"a", "b", "c", "d", "e"};
        printo(std::cout, s, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        Vector<long, N> a, b, c, d;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i]));
            XCTAssert(d[i] == (a[i] != b[i]));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i]));
            XCTAssert(d[i] == (a[i] >= b[i]));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i]));
            XCTAssert(d[i] == (a[i] > b[i]));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));

        auto const &const_a = a;
        XCTAssert(UTL::reduce_bit_and(const_a.take<1>() == a.front()) && UTL::reduce_bit_and(const_a.take<-1>() == a.back()));
        XCTAssert(UTL::reduce_bit_and(a.take<1>() == const_a.front()) && UTL::reduce_bit_and(a.take<-1>() == const_a.back()));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<1>() == a.rest()) && UTL::reduce_bit_and(const_a.drop<-1>() == a.most()));
        XCTAssert(UTL::reduce_bit_and(a.drop<1>() == const_a.rest()) && UTL::reduce_bit_and(a.drop<-1>() == const_a.most()));

        XCTAssert(UTL::reduce_bit_and(const_a.take<N>() == a) && UTL::reduce_bit_and(const_a.take<-N>() == a));
        XCTAssert(UTL::reduce_bit_and(a.take<N>() == const_a) && UTL::reduce_bit_and(a.take<-N>() == const_a));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<0>() == a) && UTL::reduce_bit_and(const_a.drop<0>() == a));
        XCTAssert(UTL::reduce_bit_and(a.drop<0>() == const_a) && UTL::reduce_bit_and(a.drop<0>() == const_a));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testVector_6 {
    using UTL::Vector;
    constexpr long N = 6;

    try {
        Vector<std::unique_ptr<long>, N> v1, v2;
        for (auto &x : v2) {
            x.reset(new long);
        }
        v1 = std::move(v2);
        *v1.front() = N;
        XCTAssert(!v2.front() && N == *v1.front());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        constexpr Vector<long, N> constexpr_v{}; // constexpr_v{0, 0, 0, 0, 0, 0};
        XCTAssert(N == constexpr_v.max_size() && !std::get<0>(constexpr_v) && !std::get<N-1>(constexpr_v));
        XCTAssert(N == std::distance(constexpr_v.cbegin(), constexpr_v.cend()));
        for (auto const& x : constexpr_v) {
            XCTAssert(!x);
        }
        for (auto it = constexpr_v.crbegin(); it != constexpr_v.crend(); ++it) {
            XCTAssert(!*it);
        }

        Vector<std::string, N> v1, v2;
        v1 = {__PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        v2 = std::move(v1);
        XCTAssert(std::get<0>(v1).empty() && std::get<N-1>(v1).empty());
        XCTAssert(v2[0] == __PRETTY_FUNCTION__ && v2.back() == __PRETTY_FUNCTION__);
        v2.fill("a");
        v1.front() = __PRETTY_FUNCTION__;
        std::swap(v1, v2);
        XCTAssert(v2[0] == __PRETTY_FUNCTION__);
        for (auto it = v1.cbegin() + 1; it != v1.cend(); ++it) {
            XCTAssert("a" == *it);
        }
        std::array<std::string, N> a2;
        a2 = std::move(static_cast<decltype(a2)&>(v2));
        XCTAssert(v2[0].empty() && a2[0] == __PRETTY_FUNCTION__);

        Vector<long, N> v3, v4;
        v3 = {1, 2, 3, 4, 5, 6};
        v4.swap(v3);
        v3.fill(0);
        XCTAssert(1 == v4.front() && N == v4.back());
        for (long i = 0; i < N; ++i) {
            XCTAssert(!v3[i] && i+1 == v4[i]);
        }

        v3.lo().fill(1);
        v3.hi().fill(2);
        XCTAssert(v3.front() == 1 && v3.back() == 2);
        v3.front() = v3.back() = -1;
        XCTAssert(v3.most().front() == -1 && v3.rest().back() == -1);
        auto const& v5 = v3;
        XCTAssert(v5.most().front() == -1 && v5.rest().back() == -1);
        printo(std::cout, v3, "\n");

        constexpr long rank = decltype(v3)::rank();
        XCTAssert(rank == 1 && v3.dims()[rank-1] == v3.size() && v3.dims()[rank-1] == v3.max_dims()[rank-1]);

        for (long i = 0; i < v3.size(); ++i) {
            v3[i] = i + 1;
        }
        auto const& v6 = v3.append(v3.size() + 1), v7 = v3.prepend(0);
        XCTAssert(v3.size() + 1 == v6.size() && v3.size() + 1 == v7.size());
        for (long i = 0; i <= v3.size(); ++i) {
            XCTAssert(v6[i] == i + 1);
            XCTAssert(v7[i] == i);
        }

        Vector<double, N> d;
        d = constexpr_v;
        printo(std::cout, d, "\n");

        Vector<std::string, N> s;
        s = Vector<char const*, N>{"a", "b", "c", "d", "e", "f"};
        printo(std::cout, s, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        Vector<long, N> a, b, c, d;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i]));
            XCTAssert(d[i] == (a[i] != b[i]));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i]));
            XCTAssert(d[i] == (a[i] >= b[i]));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i]));
            XCTAssert(d[i] == (a[i] > b[i]));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));

        auto const &const_a = a;
        XCTAssert(UTL::reduce_bit_and(const_a.take<1>() == a.front()) && UTL::reduce_bit_and(const_a.take<-1>() == a.back()));
        XCTAssert(UTL::reduce_bit_and(a.take<1>() == const_a.front()) && UTL::reduce_bit_and(a.take<-1>() == const_a.back()));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<1>() == a.rest()) && UTL::reduce_bit_and(const_a.drop<-1>() == a.most()));
        XCTAssert(UTL::reduce_bit_and(a.drop<1>() == const_a.rest()) && UTL::reduce_bit_and(a.drop<-1>() == const_a.most()));

        XCTAssert(UTL::reduce_bit_and(const_a.take<N>() == a) && UTL::reduce_bit_and(const_a.take<-N>() == a));
        XCTAssert(UTL::reduce_bit_and(a.take<N>() == const_a) && UTL::reduce_bit_and(a.take<-N>() == const_a));
        XCTAssert(UTL::reduce_bit_and(const_a.drop<0>() == a) && UTL::reduce_bit_and(const_a.drop<0>() == a));
        XCTAssert(UTL::reduce_bit_and(a.drop<0>() == const_a) && UTL::reduce_bit_and(a.drop<0>() == const_a));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testVector_ArithematicOperator {
    using UTL::Vector;

    typedef double T;
    const long sz = 10;
    Vector<T, sz> a, b;
    for (long i = 0; i < sz; ++i) {
        b[i] = i + 2;
    }
    Vector<T, sz> const c{3};

    // compound - vector:
    a = -b;
    a += c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(-b[i] + c[i] == a[i]);
    }
    a = +b;
    a -= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b;
    a *= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b;
    a /= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }
    // compound - scalar:
    a = b;
    a += std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i]);
    }
    a = b;
    a -= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b;
    a *= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b;
    a /= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }

    // binary - expressions:
    a = b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i]);
    }
    a = b - c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b * c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b / c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }

    // binary - with a scalar:
    decltype(a) d;
    a = b + std::get<0>(c);
    d = std::get<0>(c) + b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i] && c[i] + b[i] == d[i]);
    }
    a = b - std::get<0>(c);
    d = std::get<0>(c) - b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i] && c[i] - b[i] == d[i]);
    }
    a = b * std::get<0>(c);
    d = std::get<0>(c) * b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i] && c[i] * b[i] == d[i]);
    }
    a = b / std::get<0>(c);
    d = std::get<0>(c) / b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i] && c[i] / b[i] == d[i]);
    }

    // compound - expressions:
    d.fill(10);
    a = d;
    a += b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] + (b[i] + c[i]) == a[i]);
    }
    a = d;
    a -= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] - (b[i] + c[i]) == a[i]);
    }
    a = d;
    a *= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] * (b[i] + c[i]) == a[i]);
    }
    a = d;
    a /= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] / (b[i] + c[i]) == a[i]);
    }

    // mixed:
    T s = .1;
    a = b + c - s/d;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] - s/d[i] == a[i]);
    }

    // reductions:
    {
        T sum{0}, prod{1};
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            sum += a[i];
            prod *= a[i];
        }
        XCTAssert(UTL::reduce_plus(a) == sum, @"UTL::reduce_plus(a) = %f, sum = %f", UTL::reduce_plus(a), sum);
        XCTAssert(UTL::reduce_prod(a) == prod, @"UTL::reduce_prod(a) = %f, prod = %f", UTL::reduce_prod(a), prod);
    }
    {
        long sum{0}, prod{1}; // long is used to overflow in multiplication
        Vector<Vector<long, 3>, 2> a;
        for (long i = 0; i < a.size(); ++i) {
            for (long j = 0; j < a[0].size(); ++j) {
                a[i][j] = irand(100);
                sum += a[i][j];
                prod *= a[i][j];
            }
        }
        XCTAssert(UTL::reduce_plus(UTL::reduce_plus(a)) ==  sum, @"UTL::reduce_plus(a) = %ld,  sum = %ld", UTL::reduce_plus(UTL::reduce_plus(a)),  sum);
        XCTAssert(UTL::reduce_prod(UTL::reduce_prod(a)) == prod, @"UTL::reduce_prod(a) = %ld, prod = %ld", UTL::reduce_prod(UTL::reduce_prod(a)), prod);
    }
}

- (void)testVector_BitwiseOperator {
    using UTL::Vector;

    typedef long T;
    const long sz = 10;
    Vector<T, sz> a, b;
    for (long i = 0; i < sz; ++i) {
        b[i] = i + 4;
    }
    Vector<T, sz> const c{3};

    // compound - vector:
    a = b;
    a %= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b;
    a &= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b;
    a |= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b;
    a ^= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b;
    a <<= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }a = b;
    a >>= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }
    // compound - scalar:
    a = b;
    a %= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b;
    a &= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b;
    a |= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b;
    a ^= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b;
    a <<= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }
    a = b;
    a >>= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }

    // binary - expressions:
    a = b % c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b & c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b | c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b ^ c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b << c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }
    a = b >> c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }

    // binary - with a scalar:
    decltype(a) d;
    a = b % std::get<0>(c);
    d = std::get<0>(c) % b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i] && (c[i] % b[i]) == d[i]);
    }
    a = b & std::get<0>(c);
    d = std::get<0>(c) & b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i] && (c[i] & b[i]) == d[i]);
    }
    a = b | std::get<0>(c);
    d = std::get<0>(c) | b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i] && (c[i] | b[i]) == d[i]);
    }
    a = b ^ std::get<0>(c);
    d = std::get<0>(c) ^ b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i] && (c[i] ^ b[i]) == d[i]);
    }
    a = b << std::get<0>(c);
    d = std::get<0>(c) << b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i] && (c[i] << b[i]) == d[i]);
    }
    a = b >> std::get<0>(c);
    d = std::get<0>(c) >> b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i] && (c[i] >> b[i]) == d[i]);
    }

    // reductions:
    {
        T bit_and{~T{0}}, bit_or{0}, bit_xor;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = arc4random();
            bit_and &= a[i];
            bit_or |= a[i];
            bit_xor = i ? bit_xor ^ a[i] : a[i];
        }
        XCTAssert(UTL::reduce_bit_and(a) == bit_and, @"UTL::reduce_bit_and(a) = %ld, bit_and = %ld", UTL::reduce_bit_and(a), bit_and);
        XCTAssert(UTL::reduce_bit_or(a) == bit_or, @"UTL::reduce_bit_or(a) = %ld, bit_or = %ld", UTL::reduce_bit_or(a), bit_or);
        XCTAssert(UTL::reduce_bit_xor(a) == bit_xor, @"UTL::reduce_bit_xor(a) = %ld, bit_xor = %ld", UTL::reduce_bit_xor(a), bit_xor);
    }
    {
        T bit_and{~T{0}}, bit_or{0}, bit_xor;
        Vector<Vector<T, 3>, 2> a;
        for (long i = 0; i < a.size(); ++i) {
            for (long j = 0; j < a[0].size(); ++j) {
                a[i][j] = arc4random();
                bit_and &= a[i][j];
                bit_or |= a[i][j];
                bit_xor = (i == 0 && j == 0) ? a[i][j] : bit_xor ^ a[i][j];
            }
        }
        XCTAssert(UTL::reduce_bit_and(UTL::reduce_bit_and(a)) == bit_and, @"UTL::reduce_bit_and(a) = %ld, bit_and = %ld", UTL::reduce_bit_and(UTL::reduce_bit_and(a)), bit_and);
        XCTAssert(UTL::reduce_bit_or (UTL::reduce_bit_or (a)) == bit_or , @"UTL::reduce_bit_or (a) = %ld, bit_or  = %ld", UTL::reduce_bit_or (UTL::reduce_bit_or (a)), bit_or );
        XCTAssert(UTL::reduce_bit_xor(UTL::reduce_bit_xor(a)) == bit_xor, @"UTL::reduce_bit_xor(a) = %ld, bit_xor = %ld", UTL::reduce_bit_xor(UTL::reduce_bit_xor(a)), bit_xor);
    }
}

- (void)testVector_Comparison {
    using UTL::Vector;
    using UTL::SIMDVector;

    {
        Vector<std::string, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a == b) c = a == b;
        XCTAssert(c[0] && !c[1]);
    }{
        Vector<std::string, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a != b) c = a != b;
        XCTAssert(!c[0] && c[1]);
    }{
        Vector<std::string, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a <= b) c = a <= b;
        XCTAssert(c[0] && c[1]);
    }{
        Vector<std::string, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a >= b) c = a >= b;
        XCTAssert(c[0] && !c[1]);
    }{
        Vector<std::string, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a < b) c = a < b;
        XCTAssert(!c[0] && c[1]);
    }{
        Vector<std::string, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a > b) c = a > b;
        XCTAssert(!c[0] && !c[1]);
    }
    {
        Vector<Vector<std::string, 3>, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a == b) c = a == b;
        XCTAssert(c[0][0] && !c[1][1]);
    }{
        Vector<Vector<std::string, 3>, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a != b) c = a != b;
        XCTAssert(!c[0][0] && c[1][1]);
    }{
        Vector<Vector<std::string, 3>, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a <= b) c = a <= b;
        XCTAssert(c[0][0] && c[1][1]);
    }{
        Vector<Vector<std::string, 3>, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a >= b) c = a >= b;
        XCTAssert(c[0][0] && !c[1][1]);
    }{
        Vector<Vector<std::string, 3>, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a < b) c = a < b;
        XCTAssert(!c[0][0] && c[1][1]);
    }{
        Vector<Vector<std::string, 3>, 2> const a{__PRETTY_FUNCTION__, ""}, b{__PRETTY_FUNCTION__, __PRETTY_FUNCTION__};
        decltype(a > b) c = a > b;
        XCTAssert(!c[0][0] && !c[1][1]);
    }
    {
        Vector<SIMDVector<double, 2>, 2> a, b;
        a[1] = 1;
        decltype(a == b) c = a == b;
        XCTAssert(-1 == c[0].front() && !c[1].front());
        printo(std::cout, c, '\n');
    }{
        Vector<SIMDVector<double, 2>, 2> a, b;
        a[1] = 1;
        decltype(a != b) c = a != b;
        XCTAssert(0 == c[0].front() && -1 == c[1].front());
        printo(std::cout, c, '\n');
    }{
        Vector<SIMDVector<double, 2>, 2> a, b;
        b[1] = 1;
        decltype(a <= b) c = a <= b;
        XCTAssert(-1 == c[0].front() && -1 == c[1].front());
        printo(std::cout, c, '\n');
    }{
        Vector<SIMDVector<double, 2>, 2> a, b;
        b[1] = 1;
        decltype(a >= b) c = a >= b;
        XCTAssert(-1 == c[0].front() && 0 == c[1].front());
        printo(std::cout, c, '\n');
    }{
        Vector<SIMDVector<double, 2>, 2> a, b;
        b[1] = 1;
        decltype(a < b) c = a < b;
        XCTAssert(0 == c[0].front() && -1 == c[1].front());
        printo(std::cout, c, '\n');
    }{
        Vector<SIMDVector<double, 2>, 2> a, b;
        b[1] = 1;
        decltype(a > b) c = a > b;
        XCTAssert(0 == c[0].front() && 0 == c[1].front());
        printo(std::cout, c, '\n');
    }
    {
        Vector<Vector<Vector<bool, 2>, 3>, 4> a, b;
        a[1][0][0] = true;
        auto const c = a == b;
        XCTAssert(c[0][0].front() && !c[1][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<bool, 2>, 3>, 4> a, b;
        a[1][0][0] = true;
        auto const c = a != b;
        XCTAssert(!c[0][0].front() && c[1][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<int, 2>, 3>, 4> a, b;
        b[1][0][0] = true;
        auto const c = a <= b;
        XCTAssert(c[0][0].front() && c[1][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<int, 2>, 3>, 4> a, b;
        b[1][0][0] = true;
        auto const c = a >= b;
        XCTAssert(c[0][0].front() && !c[1][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<int, 2>, 3>, 4> a, b;
        b[1][0][0] = true;
        auto const c = a < b;
        XCTAssert(!c[0][0].front() && c[1][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<int, 2>, 3>, 4> a, b;
        b[1][0][0] = true;
        auto const c = a > b;
        XCTAssert(!c[0][0].front() && !c[1][0].front());
        printo(std::cout, c, '\n');
    }
    {
        Vector<Vector<Vector<Vector<bool, 1>, 2>, 3>, 4> a, b;
        a[1][0][0][0] = true;
        auto const c = a == b;
        XCTAssert(c[0][0][0].front() && !c[1][0][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<Vector<bool, 1>, 2>, 3>, 4> a, b;
        a[1][0][0][0] = true;
        auto const c = a != b;
        XCTAssert(!c[0][0][0].front() && c[1][0][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<Vector<int, 1>, 2>, 3>, 4> a, b;
        b[1][0][0][0] = true;
        auto const c = a <= b;
        XCTAssert(c[0][0][0].front() && c[1][0][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<Vector<int, 1>, 2>, 3>, 4> a, b;
        b[1][0][0][0] = true;
        auto const c = a >= b;
        XCTAssert(c[0][0][0].front() && !c[1][0][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<Vector<int, 1>, 2>, 3>, 4> a, b;
        b[1][0][0][0] = true;
        auto const c = a < b;
        XCTAssert(!c[0][0][0].front() && c[1][0][0].front());
        printo(std::cout, c, '\n');
    }{
        Vector<Vector<Vector<Vector<int, 1>, 2>, 3>, 4> a, b;
        b[1][0][0][0] = true;
        auto const c = a > b;
        XCTAssert(!c[0][0][0].front() && !c[1][0][0].front());
        printo(std::cout, c, '\n');
    }
}

- (void)testSIMD {
    using UTL::MakeSIMD;

    try {
        using Vec = MakeSIMD<double, 2>::type;
        printo(std::cout, "size = ", sizeof(Vec), ", align = ", alignof(Vec), "\n");

        Vec a = {1, 2};
        Vec b = {3, 4};
        alignas(Vec) double c[10];
        constexpr long offset = 2;
        *reinterpret_cast<Vec*>(c + offset) = a + b;
        printo(std::cout, "c = {", c[0 + offset], ", ", c[1 + offset], "}", "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSIMDVector_N {
    using UTL::SIMDVector;
    using UTL::Vector;
    constexpr long N = 128;

    try {
        using T = double;
        SIMDVector<T, N> sv1{1}, sv2;
        Vector<long, N> lv1{2};
        for (long i = 0; i < N; ++i) {
            XCTAssert(sv1[i] == 1 && sv2[i] == 0);
        }
        sv1 = lv1;
        sv2 = SIMDVector<T, N>{1};
        for (long i = 0; i < N; ++i) {
            XCTAssert(sv1[i] == lv1[i] && sv2[i] == 1);
        }
        sv1.lo().fill(1);
        sv1.hi().fill(2);
        XCTAssert(sv1.front() == 1 && sv1.back() == 2);

        SIMDVector<T, 2> sv3{};
        XCTAssert((std::is_same<SIMDVector<T, 2>::half_vector_type, Vector<T, 1>>::value));
        sv3.lo() = 1;
        sv3.hi() = 2;
        XCTAssert(sv3.front() == 1 && sv3.back() == 2);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        SIMDVector<long, N> a, b, c, d, e;
        Vector<long, N> v;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
            v[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        (e = a) += v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
            XCTAssert(a[i] + v[i] == e[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        (e = a) -= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
            XCTAssert(a[i] - v[i] == e[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        (e = a) *= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
            XCTAssert(a[i] * v[i] == e[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        (e = a) /= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
            XCTAssert(a[i] / v[i] == e[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        (e = a) %= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
            XCTAssert(a[i] % v[i] == e[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        (e = a) &= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
            XCTAssert((a[i] & v[i]) == e[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        (e = a) |= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
            XCTAssert((a[i] | v[i]) == e[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        (e = a) ^= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
            XCTAssert((a[i] ^ v[i]) == e[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        (e = a) <<= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
            XCTAssert((a[i] << v[i]) == e[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        (e = a) >>= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
            XCTAssert((a[i] >> v[i]) == e[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i] ? -1L : 0L));
            XCTAssert(d[i] == (a[i] != b[i] ? -1L : 0L));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i] ? -1L : 0L));
            XCTAssert(d[i] == (a[i] >= b[i] ? -1L : 0L));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i] ? -1L : 0L));
            XCTAssert(d[i] == (a[i] > b[i] ? -1L : 0L));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSIMDVector_3 {
    using UTL::SIMDVector;
    using UTL::Vector;
    constexpr long N = 3;

    try {
        using T = double;
        SIMDVector<T, N> sv1{1, 2, 3}, sv2;
        Vector<long, N> lv1{4, 5, 6};
        XCTAssert(sv1.max_size() == N + 1 && sv1.max_dims().x == sv1.max_size());
        for (long i = 0; i < N; ++i) {
            XCTAssert(sv1[i] == i + 1 && sv2[i] == 0);
        }
        sv1 = lv1;
        sv2 = SIMDVector<T, N>{1};
        for (long i = 0; i < N; ++i) {
            XCTAssert(sv1[i] == lv1[i] && sv2[i] == 1);
        }

        XCTAssert(sizeof(SIMDVector<T, N>) == sizeof(T)*4 && alignof(SIMDVector<T, N>) == alignof(SIMDVector<T, 4>));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        SIMDVector<long, N> a, b, c, d, e;
        Vector<long, N> v;
        for (long i = 0; i < a.size(); ++i) {
            a[i] = irand(100);
            b[i] = irand(10);
            v[i] = irand(10);
        }
        (c = a) += b;
        (d = a) += b.front();
        (e = a) += v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] + b[i] == c[i]);
            XCTAssert(a[i] + b[0] == d[i]);
            XCTAssert(a[i] + v[i] == e[i]);
        }
        (c = a) -= b;
        (d = a) -= b.front();
        (e = a) -= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] - b[i] == c[i]);
            XCTAssert(a[i] - b[0] == d[i]);
            XCTAssert(a[i] - v[i] == e[i]);
        }
        (c = a) *= b;
        (d = a) *= b.front();
        (e = a) *= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] * b[i] == c[i]);
            XCTAssert(a[i] * b[0] == d[i]);
            XCTAssert(a[i] * v[i] == e[i]);
        }
        (c = a) /= b;
        (d = a) /= b.front();
        (e = a) /= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] / b[i] == c[i]);
            XCTAssert(a[i] / b[0] == d[i]);
            XCTAssert(a[i] / v[i] == e[i]);
        }
        (c = a) %= b;
        (d = a) %= b.front();
        (e = a) %= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(a[i] % b[i] == c[i]);
            XCTAssert(a[i] % b[0] == d[i]);
            XCTAssert(a[i] % v[i] == e[i]);
        }
        (c = a) &= b;
        (d = a) &= b.front();
        (e = a) &= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] & b[i]) == c[i]);
            XCTAssert((a[i] & b[0]) == d[i]);
            XCTAssert((a[i] & v[i]) == e[i]);
        }
        (c = a) |= b;
        (d = a) |= b.front();
        (e = a) |= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] | b[i]) == c[i]);
            XCTAssert((a[i] | b[0]) == d[i]);
            XCTAssert((a[i] | v[i]) == e[i]);
        }
        (c = a) ^= b;
        (d = a) ^= b.front();
        (e = a) ^= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] ^ b[i]) == c[i]);
            XCTAssert((a[i] ^ b[0]) == d[i]);
            XCTAssert((a[i] ^ v[i]) == e[i]);
        }
        (c = a) <<= b;
        (d = a) <<= b.front();
        (e = a) <<= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] << b[i]) == c[i]);
            XCTAssert((a[i] << b[0]) == d[i]);
            XCTAssert((a[i] << v[i]) == e[i]);
        }
        (c = a) >>= b;
        (d = a) >>= b.front();
        (e = a) >>= v;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert((a[i] >> b[i]) == c[i]);
            XCTAssert((a[i] >> b[0]) == d[i]);
            XCTAssert((a[i] >> v[i]) == e[i]);
        }
        c = +a;
        d = -b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == +a[i]);
            XCTAssert(d[i] == -b[i]);
        }
        c = a == b;
        d = a != b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] == b[i] ? -1L : 0L));
            XCTAssert(d[i] == (a[i] != b[i] ? -1L : 0L));
        }
        c = a <= b;
        d = a >= b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] <= b[i] ? -1L : 0L));
            XCTAssert(d[i] == (a[i] >= b[i] ? -1L : 0L));
        }
        c = a < b;
        d = a > b;
        for (long i = 0; i < a.size(); ++i) {
            XCTAssert(c[i] == (a[i] < b[i] ? -1L : 0L));
            XCTAssert(d[i] == (a[i] > b[i] ? -1L : 0L));
        }
        XCTAssert(UTL::reduce_plus(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::plus<long>{}));
        XCTAssert(UTL::reduce_prod(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::multiplies<long>{}));
        XCTAssert(UTL::reduce_bit_and(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_and<long>{}));
        XCTAssert(UTL::reduce_bit_or(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_or<long>{}));
        XCTAssert(UTL::reduce_bit_xor(a) == std::accumulate(a.begin() + 1, a.end(), a.front(), std::bit_xor<long>{}));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSIMDVector_ArithematicOperator {
    using UTL::SIMDVector;
    using UTL::Vector;

    typedef double T;
    const long sz = 02000;
    std::unique_ptr<SIMDVector<T, sz>> _a{new SIMDVector<T, sz>};
    SIMDVector<T, sz> &a = *_a;
    std::unique_ptr<Vector<T, sz>> _b{new Vector<T, sz>};
    Vector<T, sz> &b = *_b;
    for (long i = 0; i < sz; ++i) {
        b[i] = i + 2;
    }
    SIMDVector<T, sz> const c{3};

    // compound - vector:
    a = -b;
    a += c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(-b[i] + c[i] == a[i]);
    }
    a = +b;
    a -= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b;
    a *= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b;
    a /= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }
    // compound - scalar:
    a = b;
    a += std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i]);
    }
    a = b;
    a -= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b;
    a *= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b;
    a /= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }

    // binary - expressions:
    a = b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i]);
    }
    a = b - c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b * c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b / c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }

    // binary - with a scalar:
    SIMDVector<T, sz> d;
    a = b + std::get<0>(c);
    d = std::get<0>(c) + b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i] && c[i] + b[i] == d[i]);
    }
    a = b - std::get<0>(c);
    d = std::get<0>(c) - b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i] && c[i] - b[i] == d[i]);
    }
    a = b * std::get<0>(c);
    d = std::get<0>(c) * b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i] && c[i] * b[i] == d[i]);
    }
    a = b / std::get<0>(c);
    d = std::get<0>(c) / b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i] && c[i] / b[i] == d[i]);
    }

    // compound - expressions:
    d.fill(10);
    a = d;
    a += b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] + (b[i] + c[i]) == a[i]);
    }
    a = d;
    a -= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] - (b[i] + c[i]) == a[i]);
    }
    a = d;
    a *= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] * (b[i] + c[i]) == a[i]);
    }
    a = d;
    a /= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] / (b[i] + c[i]) == a[i]);
    }

    // mixed:
    T s = .1;
    a = b + c - s/d;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] - s/d[i] == a[i]);
    }

    // reductions:
    T sum{0}, prod{1};
    for (long i = 0; i < a.size(); ++i) {
        a[i] = arc4random();
        sum += a[i];
        prod *= a[i];
    }
    XCTAssert(UTL::reduce_plus(a) == sum, @"UTL::reduce_plus(a) = %f, sum = %f", UTL::reduce_plus(a), sum);
    XCTAssert(UTL::reduce_prod(a) == prod, @"UTL::reduce_prod(a) = %f, prod = %f", UTL::reduce_prod(a), prod);
}

- (void)testSIMDVector_BitwiseOperator {
    using UTL::SIMDVector;
    using UTL::Vector;

    typedef long T;
    const long sz = 16;
    std::unique_ptr<SIMDVector<T, sz>> _a{new SIMDVector<T, sz>};
    SIMDVector<T, sz> &a = *_a;
    std::unique_ptr<Vector<T, sz>> _b{new Vector<T, sz>};
    Vector<T, sz> &b = *_b;
    for (long i = 0; i < sz; ++i) {
        b[i] = i + 2;
    }
    SIMDVector<T, sz> const c{3};

    // compound - vector:
    a = b;
    a %= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b;
    a &= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b;
    a |= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b;
    a ^= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b;
    a <<= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }a = b;
    a >>= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }
    // compound - scalar:
    a = b;
    a %= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b;
    a &= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b;
    a |= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b;
    a ^= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b;
    a <<= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }
    a = b;
    a >>= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }

    // binary - expressions:
    a = b % c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b & c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b | c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b ^ c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b << c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }
    a = b >> c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }

    // binary - with a scalar:
    SIMDVector<T, sz> d;
    a = b % std::get<0>(c);
    d = std::get<0>(c) % b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i] && (c[i] % b[i]) == d[i]);
    }
    a = b & std::get<0>(c);
    d = std::get<0>(c) & b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i] && (c[i] & b[i]) == d[i]);
    }
    a = b | std::get<0>(c);
    d = std::get<0>(c) | b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i] && (c[i] | b[i]) == d[i]);
    }
    a = b ^ std::get<0>(c);
    d = std::get<0>(c) ^ b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i] && (c[i] ^ b[i]) == d[i]);
    }
    a = b << std::get<0>(c);
    d = std::get<0>(c) << b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i] && (c[i] << b[i]) == d[i]);
    }
    a = b >> std::get<0>(c);
    d = std::get<0>(c) >> b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i] && (c[i] >> b[i]) == d[i]);
    }

    // reductions:
    T bit_and{~T{0}}, bit_or{0}, bit_xor;
    for (long i = 0; i < a.size(); ++i) {
        a[i] = arc4random();
        bit_and &= a[i];
        bit_or |= a[i];
        bit_xor = i ? bit_xor ^ a[i] : a[i];
    }
    XCTAssert(UTL::reduce_bit_and(a) == bit_and, @"UTL::reduce_bit_and(a) = %ld, bit_and = %ld", UTL::reduce_bit_and(a), bit_and);
    XCTAssert(UTL::reduce_bit_or(a) == bit_or, @"UTL::reduce_bit_or(a) = %ld, bit_or = %ld", UTL::reduce_bit_or(a), bit_or);
    XCTAssert(UTL::reduce_bit_xor(a) == bit_xor, @"UTL::reduce_bit_xor(a) = %ld, bit_xor = %ld", UTL::reduce_bit_xor(a), bit_xor);
}

- (void)testSIMDVector_Comparison {
    using UTL::SIMDVector;

    {
        SIMDVector<double, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a == b;
        XCTAssert(c[0] == -1 && c[1] == 0);
        XCTAssert(!UTL::reduce_bit_and(c) && !!UTL::reduce_bit_or(c));
    }{
        SIMDVector<double, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a != b;
        XCTAssert(c[0] == 0 && c[1] == -1);
        XCTAssert(!UTL::reduce_bit_and(c) && !!UTL::reduce_bit_or(c));
    }{
        SIMDVector<double, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a <= b;
        XCTAssert(c[0] == -1 && c[1] == -1);
        XCTAssert(UTL::reduce_bit_and(c) && UTL::reduce_bit_or(c));
    }{
        SIMDVector<double, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a >= b;
        XCTAssert(c[0] == -1 && c[1] == 0);
        XCTAssert(!UTL::reduce_bit_and(c) && !!UTL::reduce_bit_or(c));
    }{
        SIMDVector<double, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a < b;
        XCTAssert(c[0] == 0 && c[1] == -1);
        XCTAssert(!UTL::reduce_bit_and(c) && !!UTL::reduce_bit_or(c));
    }{
        SIMDVector<double, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a > b;
        XCTAssert(c[0] == 0 && c[1] == 0);
        XCTAssert(!UTL::reduce_bit_and(c) && !UTL::reduce_bit_or(c));
    }
    {
        SIMDVector<float, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a == b;
        XCTAssert(c[0] == -1 && c[1] == 0);
    }{
        SIMDVector<float, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a != b;
        XCTAssert(c[0] == 0 && c[1] == -1);
    }{
        SIMDVector<float, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a <= b;
        XCTAssert(c[0] == -1 && c[1] == -1);
    }{
        SIMDVector<float, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a >= b;
        XCTAssert(c[0] == -1 && c[1] == 0);
    }{
        SIMDVector<float, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a < b;
        XCTAssert(c[0] == 0 && c[1] == -1);
    }{
        SIMDVector<float, 2> const a{0, 0}, b{0, 1};
        decltype(a)::comparison_result_type c = a > b;
        XCTAssert(c[0] == 0 && c[1] == 0);
    }
    {
        SIMDVector<unsigned long, 2> const a{0UL, 0UL}, b{0UL, 1UL};
        decltype(a)::comparison_result_type c = a == b;
        XCTAssert(c[0] == -1 && c[1] == 0);
    }{
        SIMDVector<unsigned long, 2> const a{0UL, 0UL}, b{0UL, 1UL};
        decltype(a)::comparison_result_type c = a != b;
        XCTAssert(c[0] == 0 && c[1] == -1);
    }{
        SIMDVector<unsigned long, 2> const a{0UL, 0UL}, b{0UL, 1UL};
        decltype(a)::comparison_result_type c = a <= b;
        XCTAssert(c[0] == -1 && c[1] == -1);
    }{
        SIMDVector<unsigned long, 2> const a{0UL, 0UL}, b{0UL, 1UL};
        decltype(a)::comparison_result_type c = a >= b;
        XCTAssert(c[0] == -1 && c[1] == 0);
    }{
        SIMDVector<unsigned long, 2> const a{0UL, 0UL}, b{0UL, 1UL};
        decltype(a)::comparison_result_type c = a < b;
        XCTAssert(c[0] == 0 && c[1] == -1);
    }{
        SIMDVector<unsigned long, 2> const a{0UL, 0UL}, b{0UL, 1UL};
        decltype(a)::comparison_result_type c = a > b;
        XCTAssert(c[0] == 0 && c[1] == 0);
    }
    {
        SIMDVector<unsigned char, 3> const a{0UL, 0UL, 0UL}, b{0UL, 1UL, 0UL};
        decltype(a)::comparison_result_type c = a == b;
        XCTAssert(c[0] == -1 && c[1] == 0 && c[2] == -1);
    }{
        SIMDVector<unsigned char, 3> const a{0UL, 0UL, 0UL}, b{0UL, 1UL, 0UL};
        decltype(a)::comparison_result_type c = a != b;
        XCTAssert(c[0] == 0 && c[1] == -1 && c[2] == 0);
    }{
        SIMDVector<unsigned char, 3> const a{0UL, 0UL, 0UL}, b{0UL, 1UL, 0UL};
        decltype(a)::comparison_result_type c = a <= b;
        XCTAssert(c[0] == -1 && c[1] == -1 && c[2] == -1);
    }{
        SIMDVector<unsigned char, 3> const a{0UL, 0UL, 0UL}, b{0UL, 1UL, 0UL};
        decltype(a)::comparison_result_type c = a >= b;
        XCTAssert(c[0] == -1 && c[1] == 0 && c[2] == -1);
    }{
        SIMDVector<unsigned char, 3> const a{0UL, 0UL, 0UL}, b{0UL, 1UL, 0UL};
        decltype(a)::comparison_result_type c = a < b;
        XCTAssert(c[0] == 0 && c[1] == -1 && c[2] == 0);
    }{
        SIMDVector<unsigned char, 3> const a{0UL, 0UL, 0UL}, b{0UL, 1UL, 0UL};
        decltype(a)::comparison_result_type c = a > b;
        XCTAssert(c[0] == 0 && c[1] == 0 && c[2] == 0);
    }
}

- (void)testSIMDVector3_ArithematicOperator {
    using UTL::SIMDVector;
    using UTL::Vector;

    typedef double T;
    const long sz = 3;
    std::unique_ptr<SIMDVector<T, sz>> _a{new SIMDVector<T, sz>};
    SIMDVector<T, sz> &a = *_a;
    std::unique_ptr<Vector<T, sz>> _b{new Vector<T, sz>};
    Vector<T, sz> &b = *_b;
    for (long i = 0; i < sz; ++i) {
        b[i] = i + 2;
    }
    SIMDVector<T, sz> const c{3};

    // compound - vector:
    a = -b;
    a += c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(-b[i] + c[i] == a[i]);
    }
    a = +b;
    a -= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b;
    a *= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b;
    a /= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }
    // compound - scalar:
    a = b;
    a += std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i]);
    }
    a = b;
    a -= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b;
    a *= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b;
    a /= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }

    // binary - expressions:
    a = b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i]);
    }
    a = b - c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i]);
    }
    a = b * c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i]);
    }
    a = b / c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i]);
    }

    // binary - with a scalar:
    SIMDVector<T, sz> d;
    a = b + std::get<0>(c);
    d = std::get<0>(c) + b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i] && c[i] + b[i] == d[i]);
    }
    a = b - std::get<0>(c);
    d = std::get<0>(c) - b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i] && c[i] - b[i] == d[i]);
    }
    a = b * std::get<0>(c);
    d = std::get<0>(c) * b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i] && c[i] * b[i] == d[i]);
    }
    a = b / std::get<0>(c);
    d = std::get<0>(c) / b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i] && c[i] / b[i] == d[i]);
    }

    // compound - expressions:
    d.fill(10);
    a = d;
    a += b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] + (b[i] + c[i]) == a[i]);
    }
    a = d;
    a -= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] - (b[i] + c[i]) == a[i]);
    }
    a = d;
    a *= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] * (b[i] + c[i]) == a[i]);
    }
    a = d;
    a /= b + c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(d[i] / (b[i] + c[i]) == a[i]);
    }

    // mixed:
    T s = .1;
    a = b + c - s/d;
    for (long i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] - s/d[i] == a[i]);
    }

    // reductions:
    T sum{0}, prod{1};
    for (long i = 0; i < a.size(); ++i) {
        a[i] = arc4random();
        sum += a[i];
        prod *= a[i];
    }
    XCTAssert(UTL::reduce_plus(a) == sum, @"UTL::reduce_plus(a) = %f, sum = %f", UTL::reduce_plus(a), sum);
    XCTAssert(UTL::reduce_prod(a) == prod, @"UTL::reduce_prod(a) = %f, prod = %f", UTL::reduce_prod(a), prod);
}

- (void)testSIMDVector3_BitwiseOperator {
    using UTL::SIMDVector;
    using UTL::Vector;

    typedef long T;
    const long sz = 3;
    std::unique_ptr<SIMDVector<T, sz>> _a{new SIMDVector<T, sz>};
    SIMDVector<T, sz> &a = *_a;
    std::unique_ptr<Vector<T, sz>> _b{new Vector<T, sz>};
    Vector<T, sz> &b = *_b;
    for (long i = 0; i < sz; ++i) {
        b[i] = i + 2;
    }
    SIMDVector<T, sz> const c{3};

    // compound - vector:
    a = b;
    a %= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b;
    a &= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b;
    a |= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b;
    a ^= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b;
    a <<= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }a = b;
    a >>= c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }
    // compound - scalar:
    a = b;
    a %= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b;
    a &= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b;
    a |= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b;
    a ^= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b;
    a <<= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }
    a = b;
    a >>= std::get<0>(c);
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }

    // binary - expressions:
    a = b % c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i]);
    }
    a = b & c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i]);
    }
    a = b | c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i]);
    }
    a = b ^ c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i]);
    }
    a = b << c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i]);
    }
    a = b >> c;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i]);
    }

    // binary - with a scalar:
    SIMDVector<T, sz> d;
    a = b % std::get<0>(c);
    d = std::get<0>(c) % b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] % c[i]) == a[i] && (c[i] % b[i]) == d[i]);
    }
    a = b & std::get<0>(c);
    d = std::get<0>(c) & b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] & c[i]) == a[i] && (c[i] & b[i]) == d[i]);
    }
    a = b | std::get<0>(c);
    d = std::get<0>(c) | b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] | c[i]) == a[i] && (c[i] | b[i]) == d[i]);
    }
    a = b ^ std::get<0>(c);
    d = std::get<0>(c) ^ b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] ^ c[i]) == a[i] && (c[i] ^ b[i]) == d[i]);
    }
    a = b << std::get<0>(c);
    d = std::get<0>(c) << b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] << c[i]) == a[i] && (c[i] << b[i]) == d[i]);
    }
    a = b >> std::get<0>(c);
    d = std::get<0>(c) >> b;
    for (long i = 0; i < sz; ++i) {
        XCTAssert((b[i] >> c[i]) == a[i] && (c[i] >> b[i]) == d[i]);
    }

    // reductions:
    T bit_and{~T{0}}, bit_or{0}, bit_xor;
    for (long i = 0; i < a.size(); ++i) {
        a[i] = arc4random();
        bit_and &= a[i];
        bit_or |= a[i];
        bit_xor = i ? bit_xor ^ a[i] : a[i];
    }
    XCTAssert(UTL::reduce_bit_and(a) == bit_and, @"UTL::reduce_bit_and(a) = %ld, bit_and = %ld", UTL::reduce_bit_and(a), bit_and);
    XCTAssert(UTL::reduce_bit_or(a) == bit_or, @"UTL::reduce_bit_or(a) = %ld, bit_or = %ld", UTL::reduce_bit_or(a), bit_or);
    XCTAssert(UTL::reduce_bit_xor(a) == bit_xor, @"UTL::reduce_bit_xor(a) = %ld, bit_xor = %ld", UTL::reduce_bit_xor(a), bit_xor);
}

- (void)testSIMDVector_MixedVectorTypesWithDifferingAlignment {
    using UTL::SIMDVector;
    using UTL::Vector;

    SIMDVector<long, 2> const v1{10, 9};
    SIMDVector<long, 3> const v3{1, 2, 3};
    Vector<long, 3> const v2{3};
    // +
    {
        auto const res = v1 + v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] + v2[i]);
        }
    }{
        auto const res = v1 + v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] + v2[i]);
        }
    }{
        auto const res = v3 + v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v3[i] + v2[i]);
        }
    }
    // -
    {
        auto const res = v1 - v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] - v2[i]);
        }
    }{
        auto const res = v1 - v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] - v2[i]);
        }
    }{
        auto const res = v3 - v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v3[i] - v2[i]);
        }
    }
    // *
    {
        auto const res = v1 * v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] * v2[i]);
        }
    }{
        auto const res = v1 * v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] * v2[i]);
        }
    }{
        auto const res = v3 * v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v3[i] * v2[i]);
        }
    }
    // /
    {
        auto const res = v1 / v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] / v2[i]);
        }
    }{
        auto const res = v1 / v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] / v2[i]);
        }
    }{
        auto const res = v3 / v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v3[i] / v2[i]);
        }
    }
    // %
    {
        auto const res = v1 % v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] % v2[i]);
        }
    }{
        auto const res = v1 % v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v1[i] % v2[i]);
        }
    }{
        auto const res = v3 % v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == v3[i] % v2[i]);
        }
    }
    // &
    {
        auto const res = v1 & v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] & v2[i]));
        }
    }{
        auto const res = v1 & v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] & v2[i]));
        }
    }{
        auto const res = v3 & v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v3[i] & v2[i]));
        }
    }
    // |
    {
        auto const res = v1 | v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] | v2[i]));
        }
    }{
        auto const res = v1 | v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] | v2[i]));
        }
    }{
        auto const res = v3 | v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v3[i] | v2[i]));
        }
    }
    // ^
    {
        auto const res = v1 ^ v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] ^ v2[i]));
        }
    }{
        auto const res = v1 ^ v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] ^ v2[i]));
        }
    }{
        auto const res = v3 ^ v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v3[i] ^ v2[i]));
        }
    }
    // <<
    {
        auto const res = v1 << v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] << v2[i]));
        }
    }{
        auto const res = v1 << v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] << v2[i]));
        }
    }{
        auto const res = v3 << v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v3[i] << v2[i]));
        }
    }
    // >>
    {
        auto const res = v1 >> v2.most();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] >> v2[i]));
        }
    }{
        auto const res = v1 >> v2.rest();
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v1[i] >> v2[i]));
        }
    }{
        auto const res = v3 >> v2;
        for (long i = 0; i < res.size(); ++i) {
            XCTAssert(res[i] == (v3[i] >> v2[i]));
        }
    }
}

- (void)testSIMDComplex {
    using UTL::SIMDComplex;

    try {
        SIMDComplex<double> const a{1, 3};
        std::complex<double> const b{4, -3};
        SIMDComplex<double> c;
        std::complex<double> d;

        // compound with complex:
        c = -a;
        d = -static_cast<std::complex<double>>(a);
        c += b;
        d += b;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c -= b;
        d -= b;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c *= b;
        d *= b;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c /= b;
        d /= b;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());

        // compound with real:
        c = +a;
        d = +static_cast<std::complex<double>>(a);
        c += b.real();
        d += b.real();
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c -= b.real();
        d -= b.real();
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c *= b.real();
        d *= b.real();
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c /= b.real();
        d /= b.real();
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());

        // simd_complex [+-*/] complex:
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = c + b;
        d = d + b;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = c - b;
        d = d - b;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = c * b;
        d = d * b;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = c / b;
        d = d / b;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());

        // complex [+-*/] simd_complex:
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = b + c;
        d = b + d;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = b - c;
        d = b - d;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = b * c;
        d = b * d;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = b / c;
        d = b / d;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());

        // simd_complex [+-*/] real:
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = c + b.real();
        d = d + b.real();
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = c - b.real();
        d = d - b.real();
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = c * b.real();
        d = d * b.real();
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = c / b.real();
        d = d / b.real();
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());

        // real [+-*/] simd_complex:
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = b.real() + c;
        d = b.real() + d;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = b.real() - c;
        d = b.real() - d;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = b.real() * c;
        d = b.real() * d;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());
        c = a;
        d = static_cast<std::complex<double>>(a);
        c = b.real() / c;
        d = b.real() / d;
        XCTAssert(c == d, @"c = %f %+f, d = %f %+f", c.real(), c.imag(), d.real(), d.imag());

        // values:
        XCTAssert(std::real(c) == std::real(d) && std::imag(c) == std::imag(d));
        XCTAssert(std::abs(c) == std::abs(d) && std::arg(c) == std::arg(d));
        XCTAssert(std::conj(c) == std::conj(d) && std::proj(c) == std::proj(d));
        XCTAssert(std::sin(c) == std::sin(d) && std::cos(c) == std::cos(d) && std::tan(c) == std::tan(d));
        XCTAssert(std::sinh(c) == std::sinh(d) && std::cosh(c) == std::cosh(d) && std::tanh(c) == std::tanh(d));
        XCTAssert(std::asin(c) == std::asin(d) && std::acos(c) == std::acos(d) && std::atan(c) == std::atan(d));
        XCTAssert(std::asinh(c) == std::asinh(d) && std::acosh(c) == std::acosh(d) && std::atanh(c) == std::atanh(d));
        XCTAssert(std::exp(c) == std::exp(d) && std::log(c) == std::log(d) && std::log10(c) == std::log10(d));
        XCTAssert(std::pow(c, 2.) == std::pow(d, 2.) && std::pow(c, b) == std::pow(d, b) && std::pow(c.real(), b) == std::pow(d.real(), b));
        XCTAssert(std::sqrt(c) == std::sqrt(d));

        // swap:
        SIMDComplex<double> const e{};
        SIMDComplex<double>{}.swap(c);
        XCTAssert(c == e && c == e.real() && c.real() == e);
        SIMDComplex<double>{1}.swap(c);
        XCTAssert(c != e && c != e.real() && c.real() != e);
        SIMDComplex<double>{1, 1}.swap(c);
        XCTAssert(c != e);

        printo(std::cout, a, '\n');
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end
