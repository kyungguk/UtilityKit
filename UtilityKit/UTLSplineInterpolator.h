//
//  UTLSplineInterpolator.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSplineInterpolator_h
#define UTLSplineInterpolator_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLOptional.h>
#include <type_traits>
#include <stdexcept>
#include <algorithm>
#include <utility>
#include <array>
#include <vector>
#include <cmath>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    // MARK: Forward Declarations
    //
    template <class RealType, long Order> class SplineCoefficient;
    template <class RealType, long Order> class SplineInterpolator;


    // MARK: SplineInterpolator
    //
    /**
     @brief Spline interpolator constructed using a SplineCoefficient instance.
     */
    template <class RealType, long Order>
    class SplineInterpolator {
        static_assert(std::is_floating_point<RealType>::value, "The template parameter should be a floating point type.");
        static_assert(Order >= 1, "The spline order should be a positive integer.");

    public:
        // types:
        using value_type = RealType;
        using optional_type = Optional<value_type>;
        using coefficient_type = std::array<value_type, Order+1>; //!< The first element is the 0th order coefficient.
        using coefficient_table_type = std::vector<std::pair<value_type, coefficient_type>>;
        using spline_coefficient_type = SplineCoefficient<RealType, Order>;

    public:
        /**
         @brief Polynomial order of the spline segments.
         */
        constexpr static unsigned spline_order() noexcept { return Order; }

        /**
         @brief Query if the receiver is valid.
         */
        explicit operator bool() const noexcept { return bool(_coef); }

        /**
         @brief Return spline coefficient object.
         Sorted in ascending order.
         */
        spline_coefficient_type const& spline_coefficient() const noexcept { return _coef; }

        /**
         @brief Minimum abscissa value.
         */
        value_type const& min_abscissa() const noexcept { return _coef.min_abscissa(); }
        /**
         @brief Maximum abscissa value.
         */
        value_type const& max_abscissa() const noexcept { return _coef.max_abscissa(); }

        /**
         @brief Interpolate at a given abscissa value, x.
         */
        optional_type interpolate(value_type const x) const;
        /**
         @brief A shorthand for interpolate(x).
         */
        optional_type operator()(value_type const x) const { return interpolate(x); }

        /**
         @brief Calculate a first order derivative at x.
         */
        optional_type derivative(value_type const x) const;

        /**
         @brief Integrate the approximating function in the entire interval.
         */
        value_type integrate() const { return _coef.integrate(); }

        /**
         @brief Default constructor.
         @discussion The state is invalid.
         */
        explicit SplineInterpolator() noexcept = default;
        /**
         @brief Construct interpolator by copying the table in a SplineCoefficient instance.
         */
        explicit SplineInterpolator(spline_coefficient_type const& coef) { _init_common(coef); } // should not use ": _coef(coef)"
        /**
         @brief Construct interpolator by moving the table in a SplineCoefficient instance.
         @discussion The SplineCoefficient instance becomes invalid after construction.
         */
        explicit SplineInterpolator(spline_coefficient_type&& coef) { _init_common(std::move(coef)); } // should not use ": _coef(std::move(coef))"

        /**
         @brief Swap the receiver's content with other's.
         */
        void swap(SplineInterpolator& other);

        // copy/move:
        SplineInterpolator(SplineInterpolator const&) = default;
        SplineInterpolator(SplineInterpolator&&) = default;
        SplineInterpolator& operator=(SplineInterpolator const&) = default;
        SplineInterpolator& operator=(SplineInterpolator&&) = default;

    private:
        spline_coefficient_type _coef{};
        unsigned long (SplineInterpolator::*_locator)(value_type const) const{nullptr};
        value_type _d{}; // either delta or correlation tolerance depending on the regularity of grid
        mutable long _j0{0}; // jsaved
        mutable bool _is_correlated{false};

        // Concrete locators
        // x bound should be checked beforehand!!
        //
        unsigned long _locator_regular(value_type const x) const {
            return x == max_abscissa() ? _coef.table().size()-2 : static_cast<unsigned long>( std::floor((x - min_abscissa())/_d) );
        }
        unsigned long _locator_irregular(value_type const x) const {
            long const i = _is_correlated ? _hunt(x) : _locate(x);
            return i >= 0 && i < long(_coef.table().size()-1) ? static_cast<unsigned long>(i) : throw std::runtime_error(__PRETTY_FUNCTION__);
        }
        long _locate(value_type const x) const;
        long _hunt(value_type const x) const;

        template <class Coef>
        void _init_common(Coef&& coef) {
            if (!coef) return;
            _coef = std::forward<Coef>(coef);

            // choose locator based on grid regularity
            //
            if (_coef.delta()) {
                _d = *_coef.delta();
                _locator = &SplineInterpolator::_locator_regular;
            } else {
                _d = std::min( value_type(1.), std::pow(value_type(_coef.table().size()), value_type(0.25)) );
                _locator = &SplineInterpolator::_locator_irregular;
            }
        }
    };

    template <class RealType, long Order>
    void SplineInterpolator<RealType, Order>::swap(SplineInterpolator& other)
    {
        _coef.swap(other._coef);
        std::swap(_locator, other._locator);
        std::swap(_d, other._d);
    }

    template <class RealType, long Order>
    auto SplineInterpolator<RealType, Order>::interpolate(value_type const x) const
    -> optional_type {
        if (x < min_abscissa() || x > max_abscissa()) return {};
        coefficient_table_type const &table = _coef.table();

        unsigned long const i = (this->*_locator)(x);
        auto const &pair = table[i];
        value_type const t = (x - pair.first) / (table[i+1].first - pair.first);
        value_type result(0.);
        for (int i = Order; i >= 0; --i) {
            result = result*t + pair.second[unsigned(i)];
        }
        return result;
    }
    template <class RealType, long Order>
    auto SplineInterpolator<RealType, Order>::derivative(value_type const x) const
    -> optional_type {
        if (x < min_abscissa() || x > max_abscissa()) return {};
        coefficient_table_type const &table = _coef.table();

        unsigned long const i = (this->*_locator)(x);
        auto const &pair = table[i];
        value_type const d = table[i+1].first - pair.first;
        value_type const t = (x - pair.first) / d;
        value_type result(0.);
        for (int i = Order; i >= 1; --i) {
            result = result*t + pair.second[unsigned(i)]*value_type(i);
        }
        return result / d;
    }

    template <class RealType, long Order>
    long SplineInterpolator<RealType, Order>::_locate(value_type const x) const
    {
        coefficient_table_type const &table = _coef.table();
        using size_type = unsigned long;
        long const n = long(table.size());
        long jl = 0;
        long ju = n - 1;
        while (ju - jl > 1) {
            long const jm = (ju + jl) >> 1; // mid point
            if (x >= table[size_type(jm)].first) {
                jl = jm;
            } else {
                ju = jm;
            }
        }
        _is_correlated = std::abs((jl - _j0)/_d) < value_type(1.);
        _j0 = jl;
        return std::max(long(0), std::min(n-2, jl));
    }

    template <class RealType, long Order>
    long SplineInterpolator<RealType, Order>::_hunt(value_type const x) const
    {
        coefficient_table_type const &table = _coef.table();
        using size_type = unsigned long;
        long const n = long(table.size());
        long jl = _j0, inc = 1, ju(-1);
        if (jl < 0 || jl > n-1) {
            jl = 0;
            ju = n - 1;
        } else {
            if (x >= table[size_type(jl)].first) { // hunt up
                for (; ; ) {
                    ju = jl + inc;
                    if (ju >= n-1) {
                        ju = n-1;
                        break;
                    } else if (x < table[size_type(ju)].first) {
                        break;
                    } else {
                        jl = ju;
                        inc += inc;
                    }
                }
            } else { // hunt down
                ju = jl;
                for (; ; ) {
                    jl = ju - inc;
                    if (jl <= 0) {
                        jl = 0;
                        break;
                    } else if (x >= table[size_type(jl)].first) {
                        break;
                    } else {
                        ju = jl;
                        inc += inc;
                    }
                }
            }
        }
        while (ju - jl > 1) { // hunt is done, so do the bisection
            long const jm = (ju + jl) >> 1;
            if (x >= table[size_type(jm)].first) {
                jl = jm;
            } else {
                ju = jm;
            }
        }
        _is_correlated = std::abs((jl - _j0)/_d) < value_type(1.);
        _j0 = jl;
        return std::max(long(0), std::min(n-2, jl));
    }

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLSplineCoefficient.h>
#include <UtilityKit/UTLOptional.h>
#include <type_traits>
#include <utility>
#include <memory>
#include <tuple> // std::tie

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class RealType, long Order> class SplineCoefficient;
    template <class RealType, long Order> class SplineInterpolator;

    /**
     @brief Spline interpolator constructed using a SplineCoefficient instance.
     */
    template <class RealType, long Order>
    class SplineInterpolator {
        static_assert(std::is_floating_point<RealType>::value, "the template parameter should be a floating point type");
        static_assert(Order >= 1, "the spline order should be a positive integer");

    public:
        // types:
        using spline_coefficient_type = SplineCoefficient<RealType, Order>;
        using value_type = typename spline_coefficient_type::value_type;
        using coefficient_type = typename spline_coefficient_type::coefficient_type;
        using coefficient_table_type = typename spline_coefficient_type::coefficient_table_type;
        using optional_type = Optional<value_type>;

    public:
        /**
         @brief Polynomial order of the spline segments.
         */
        constexpr static unsigned spline_order() noexcept { return spline_coefficient_type::spline_order(); }

        /**
         @brief Query if the receiver is valid.
         */
        explicit operator bool() const noexcept { return bool(_coef); }

        /**
         @brief Return spline coefficient object.
         */
        spline_coefficient_type const& spline_coefficient() const noexcept { return _coef; }

        /**
         @brief Minimum abscissa value.
         */
        value_type const& min_abscissa() const noexcept { return _coef.min_abscissa(); }
        /**
         @brief Maximum abscissa value.
         */
        value_type const& max_abscissa() const noexcept { return _coef.max_abscissa(); }

        /**
         @brief Definite integral over the entire interval.
         */
        value_type integrate() const noexcept { return _coef.integrate(); }

        /**
         @brief Interpolate at a given abscissa value, x.
         @discussion This call is not thread-safe if spline_coefficient().is_regular() == false.
         */
        inline optional_type interpolate(value_type const x) const noexcept;
        /**
         @brief Shorthand for interpolate(x).
         @discussion This call is not thread-safe if spline_coefficient().is_regular() == false.
         */
        optional_type operator()(value_type const x) const noexcept { return interpolate(x); }

        /**
         @brief Calculate a first order derivative at x.
         @discussion This call is not thread-safe if spline_coefficient().is_regular() == false.
         */
        inline optional_type derivative(value_type const x) const noexcept;

        /**
         @brief Swap the receiver's content with other's.
         */
        inline void swap(SplineInterpolator& other);

        /**
         @brief Default constructor.
         @discussion The state is invalid.
         */
        explicit SplineInterpolator() = default;
        /**
         @brief Construct interpolator by copying a SplineCoefficient instance.
         */
        explicit SplineInterpolator(spline_coefficient_type const& coef) : SplineInterpolator(nullptr, coef) {}
        /**
         @brief Construct interpolator by moving a SplineCoefficient instance.
         @discussion The SplineCoefficient instance becomes invalid after construction.
         */
        explicit SplineInterpolator(spline_coefficient_type&& coef) : SplineInterpolator(nullptr, std::move(coef)) {}

        // copy/move:
        SplineInterpolator(SplineInterpolator const &o) : SplineInterpolator(nullptr, o.spline_coefficient()) {}
        SplineInterpolator& operator=(SplineInterpolator const&);
        SplineInterpolator(SplineInterpolator&&) = default;
        SplineInterpolator& operator=(SplineInterpolator&&) = default;

    private:
        // member variables:
        spline_coefficient_type _coef{};
        value_type _delta_or_correlation_tolerance{}; // either delta or correlation tolerance depending on the regularity of grid
        long (SplineInterpolator::*_locator)(value_type const) const noexcept{};
        mutable struct LocatorState {
            long jsave{0}; // j_saved
            bool correlated{false};
        } _state{};

    private: // helpers
        // constructor funnel point
        //
        template <class Coef>
        explicit SplineInterpolator(decltype(nullptr), Coef&& coef); // should not use ": _coef(std::forward<Coef>(coef))"

        // accessors for delta / correlation tolerence
        //
        value_type const &_delta() const noexcept { return _delta_or_correlation_tolerance; }
        value_type const &_correlation_tolerance() const noexcept { return _delta_or_correlation_tolerance; }

        // concrete locators
        // x bound should be checked beforehand!!
        //
        long _locator_regular(value_type const x) const noexcept;
        long _locator_irregular(value_type const x) const noexcept;

        // irregular locator helpers
        //
        long _locate(value_type const x, LocatorState &s) const noexcept;
        long _hunt(value_type const x, LocatorState &s) const noexcept;

        // interpolate helper
        //
        static constexpr value_type _interpolate_eval_poly(coefficient_type const &, value_type const, std::integral_constant<long, std::tuple_size<coefficient_type>::value>) noexcept {
            return 0;
        }
        template <long I>
        static value_type _interpolate_eval_poly(coefficient_type const &coef, value_type const t, std::integral_constant<long, I>) noexcept {
            static_assert(I >= 0 && I < std::tuple_size<coefficient_type>::value, "invalid index");
            using N = std::integral_constant<long, I + 1>;
            return std::get<I>(coef) + t*_interpolate_eval_poly(coef, t, N{});
        }
        static value_type _interpolate_eval_poly(coefficient_type const &coef, value_type const t) noexcept {
            return _interpolate_eval_poly(coef, t, std::integral_constant<long, 0>{});
        }

        // derivative helper
        //
        static constexpr value_type _derivative_eval_poly(coefficient_type const &, value_type const, std::integral_constant<long, std::tuple_size<coefficient_type>::value>) noexcept {
            return 0;
        }
        template <long I>
        static value_type _derivative_eval_poly(coefficient_type const &coef, value_type const t, std::integral_constant<long, I>) noexcept {
            static_assert(I > 0 && I < std::tuple_size<coefficient_type>::value, "invalid index");
            using N = std::integral_constant<long, I + 1>;
            return std::get<I>(coef)*I + t*_derivative_eval_poly(coef, t, N{});
        }
        static value_type _derivative_eval_poly(coefficient_type const &coef, value_type const t) noexcept {
            return _derivative_eval_poly(coef, t, std::integral_constant<long, 1>{});
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLSplineInterpolator.hh>

#endif /* UTLSplineInterpolator_h */
