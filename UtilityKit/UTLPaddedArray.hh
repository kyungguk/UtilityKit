//
//  UTLPaddedArray.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/10/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLPaddedArray_hh
#define UTLPaddedArray_hh

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <stdexcept>
#include <utility>
#include <sstream>
#include <string>

template <class Type, long PadSize>
UTL::__4_::PaddedArray<Type, PadSize>::PaddedArray(size_type const sz, smart_pointer&& ptr)
: PaddedArray() {
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - size argument cannot be negative");
    }
    if (nullptr == ptr) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - ptr argument cannot be nullptr");
    }
    _storage = std::move(ptr);
    this->_beg = _storage.get() + pad_size();
    this->_end = this->_beg + sz;
}

template <class Type, long PadSize>
UTL::__4_::PaddedArray<Type, PadSize>::PaddedArray(size_type const sz)
: PaddedArray() {
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - size argument cannot be negative");
    }
    size_type const n = sz + 2*pad_size();
    if (!n) return; // no allocation
    pointer ptr = _allocate(n);
    size_type i{0};
    try {
        for (; i < n; ++i) {
            _construct(ptr + i);
        }
    } catch (...) {
        _unwind(ptr, i);
        _deallocate(ptr, n);
        throw;
    }
    PaddedArray(sz, smart_pointer{ptr, [n](pointer p) { _delete(p, n); }}).swap(*this);
}
template <class Type, long PadSize>
UTL::__4_::PaddedArray<Type, PadSize>::PaddedArray(size_type const sz, value_type const& x)
: PaddedArray() {
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - size argument cannot be negative");
    }
    size_type const n = sz + 2*pad_size();
    if (!n) return; // no allocation
    pointer ptr = _allocate(n);
    size_type i{0};
    try {
        for (; i < n; ++i) {
            _construct(ptr + i, x);
        }
    } catch (...) {
        _unwind(ptr, i);
        _deallocate(ptr, n);
        throw;
    }
    PaddedArray(sz, smart_pointer{ptr, [n](pointer p) { _delete(p, n); }}).swap(*this);
}

template <class Type, long PadSize>
template <class ForwardIt, typename std::enable_if<std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<ForwardIt>::iterator_category>::value, long>::type>
UTL::__4_::PaddedArray<Type, PadSize>::PaddedArray(ForwardIt first, ForwardIt last)
: PaddedArray() {
    size_type const sz = std::distance(first, last) - 2*pad_size();
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - size argument cannot be negative");
    }
    size_type const n = sz + 2*pad_size();
    if (!n) return; // no allocation
    pointer ptr = _allocate(n);
    size_type i{0};
    try {
        for (; first != last; ++i) {
            _construct(ptr + i, *first++);
        }
    } catch (...) {
        _unwind(ptr, i);
        _deallocate(ptr, n);
        throw;
    }
    PaddedArray(sz, smart_pointer{ptr, [n](pointer p) { _delete(p, n); }}).swap(*this);
}

template <class Type, long PadSize>
auto UTL::__4_::PaddedArray<Type, PadSize>::operator=(PaddedArray const& o)
-> PaddedArray& {
    if (this != &o) {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (o.size() != this->size()) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array size inconsistency");
        }
        std::copy(o.pad_begin(), o.pad_end(), pad_begin());
    }
    return *this;
}
template <class Type, long PadSize>
auto UTL::__4_::PaddedArray<Type, PadSize>::operator=(PaddedArray&& o)
-> PaddedArray& {
    if (this != &o) {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (o.size() != this->size()) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array size inconsistency");
        }
        std::move(o.pad_begin(), o.pad_end(), pad_begin());
    }
    return *this;
}

template <class Type, long PadSize>
template <long N>
auto UTL::__4_::PaddedArray<Type, PadSize>::operator=(PaddedArray<value_type, N> const& o)
-> PaddedArray& {
    {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (o.size() != this->size()) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array size inconsistency");
        }
        difference_type const pad = std::min(this->pad_size(), o.pad_size());
        std::copy(o.begin() - pad, o.end() + pad, this->begin() - pad);
    }
    return *this;
}
template <class Type, long PadSize>
template <long N>
auto UTL::__4_::PaddedArray<Type, PadSize>::operator=(PaddedArray<value_type, N>&& o)
-> PaddedArray& {
    {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (o.size() != this->size()) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array size inconsistency");
        }
        difference_type const pad = std::min(this->pad_size(), o.pad_size());
        std::move(o.begin() - pad, o.end() + pad, this->begin() - pad);
    }
    return *this;
}

template <class Type, long PadSize>
template <long NewPad>
auto UTL::__4_::PaddedArray<Type, PadSize>::slice(size_type loc, size_type const len)
-> PaddedArray<Type, NewPad> { // location is relative to this->begin()
    size_type const size = this->size() + 2*(pad_size() - NewPad); // this->size translated into new array space
    loc += pad_size() - NewPad; // location is now in new array space
    if (loc < 0 || loc > size) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - invalid location");
    }
    if (len < 0 || loc + len > size) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - invalid length");
    }
    // slice
    //
    if (*this) {
        return PaddedArray<Type, NewPad>{len, smart_pointer{storage(), data() + loc}};
    } else {
        return PaddedArray<Type, NewPad>{};
    }
}
template <class Type, long PadSize>
template <long NewPad>
auto UTL::__4_::PaddedArray<Type, PadSize>::slice()
-> PaddedArray<Type, NewPad> {
    size_type const size = this->size() + 2*(pad_size() - NewPad); // this->size translated into new array space
    if (size < 0) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - too large padding of the sliced array");
    }
    if (*this) {
        return PaddedArray<Type, NewPad>{size, smart_pointer{storage(), data()}};
    } else {
        return PaddedArray<Type, NewPad>{};
    }
}

template <class Type, long PadSize>
void UTL::__4_::PaddedArray<Type, PadSize>::swap(PaddedArray& o) noexcept
{
    std::swap(this->_beg, o._beg);
    std::swap(this->_end, o._end);
    std::swap(this->_storage, o._storage);
}

template <class Type, long PadSize>
auto UTL::__4_::PaddedArray<Type, PadSize>::at(size_type i)
-> reference {
    i += pad_size();
    if (i >= 0 && i < max_size()) {
        return *(pad_begin() + i);
    }
    throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - index out of range");
}
template <class Type, long PadSize>
auto UTL::__4_::PaddedArray<Type, PadSize>::at(size_type i) const
-> const_reference {
    i += pad_size();
    if (i >= 0 && i < max_size()) {
        return *(pad_begin() + i);
    }
    throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - index out of range");
}

// MARK: std::swap
//
namespace std {
    template <class Type, long PadSize>
    inline void swap(UTL::__4_::PaddedArray<Type, PadSize> &a, UTL::__4_::PaddedArray<Type, PadSize> &b) noexcept(noexcept(a.swap(b))) {
        a.swap(b);
    }
}

// MARK: Buffered Output Stream
//
template <class _CharT, class _Traits, class T, long PadSize>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::PaddedArray<T, PadSize> const& a) {
    if (!a) {
        return os << "(null)";
    }
    std::basic_ostringstream<_CharT, _Traits> ss; {
        ss.flags(os.flags());
        ss.imbue(os.getloc());
        ss.precision(os.precision());
    }
    auto it = a.pad_begin(), end = a.begin();
    // left paddings
    ss << "{" << *it++;
    while (it != end) ss << ", " << *it++;
    ss << "|";
    // contents
    end = a.end();
    if (it != end) {
        ss << *it++;
        while (it != end) ss << ", " << *it++;
    }
    // right paddings
    end = a.pad_end();
    ss << "|" << *it++;
    while (it != end) ss << ", " << *it++;
    ss << "}";
    return os << ss.str();
}
template <class _CharT, class _Traits, class T>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::PaddedArray<T, 0L> const& a) {
    return os << static_cast<UTL::__4_::Array<T> const&>(a);
}

#endif /* UTLPaddedArray_hh */
