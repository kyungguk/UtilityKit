//
//  UTLArray.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/10/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArray_hh
#define UTLArray_hh

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <algorithm>
#include <stdexcept>
#include <sstream>

// MARK: Out-of-line Implementations
//
template <class Type>
UTL::__4_::Array<Type>& UTL::__4_::Array<Type>::operator=(UTL::__4_::Array<Type> const& o) {
    if (this->size() != o.size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array size inconsistency");
    }
    std::copy(o.begin(), o.end(), this->begin());
    return *this;
}
template <class Type>
UTL::__4_::Array<Type>& UTL::__4_::Array<Type>::operator=(UTL::__4_::Array<Type>&& o) {
    if (this->size() != o.size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array size inconsistency");
    }
    std::move(o.begin(), o.end(), this->begin());
    return *this;
}

template <class Type>
auto UTL::__4_::Array<Type>::at(size_type const i)
-> reference {
    if (i >= 0 && i < size()) {
        return *(begin() + i);
    }
    throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - index out of range");
}
template <class Type>
auto UTL::__4_::Array<Type>::at(size_type const i) const
-> const_reference {
    if (i >= 0 && i < size()) {
        return *(begin() + i);
    }
    throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - index out of range");
}

template <class Type>
template <class Predicate>
auto UTL::__4_::Array<Type>::shuffle_if(Predicate&& pred) noexcept(noexcept(std::swap(*_beg, *_end)))
-> iterator {
    iterator first = begin(), last = end();
    while (first != last) {
        if (std::forward<Predicate>(pred)(static_cast<const_reference>(*first))) {
            std::swap(*first, *--last);
        } else {
            ++first;
        }
    }
    return last;
}

// MARK: Buffered Output Stream
//
template <class _CharT, class _Traits, class T>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::Array<T> const& a) {
    if (a.empty()) {
        return os << "{}";
    }
    std::basic_ostringstream<_CharT, _Traits> ss; {
        ss.flags(os.flags());
        ss.imbue(os.getloc());
        ss.precision(os.precision());
    }
    auto it = a.begin(), end = a.end();
    ss << "{" << *it++;
    while (it != end) ss << ", " << *it++;
    ss << "}";
    return os << ss.str();
}

#endif /* UTLArray_hh */
