cmake_minimum_required(VERSION 3.18)

project(UtilityKit CXX)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_EXTENSIONS OFF)

include(cmake/StandardProjectSettings.cmake)
include(cmake/PreventInSourceBuilds.cmake)
include(cmake/CompilerWarnings.cmake)
include(cmake/StaticAnalyzers.cmake)
include(cmake/Sanitizers.cmake)
include(cmake/Doxygen.cmake)
set(WARNINGS_AS_ERRORS $ENV{WARNINGS_AS_ERRORS})

enable_doxygen()
enable_testing()

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")
if (CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=lld")
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
else ()
    message(ERROR "Compiler '${CMAKE_CXX_COMPILER_ID}' not supported.")
endif ()
string(REPLACE ":" ";" LD_LIB_PATH_LIST "$ENV{LD_LIBRARY_PATH}")
foreach (LD_LIB_PATH ${LD_LIB_PATH_LIST})
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-rpath,${LD_LIB_PATH}")
endforeach ()

# UtilityKit
add_subdirectory(UtilityKit)
target_link_libraries(UtilityKit
    PUBLIC "-lpthread"
    )
