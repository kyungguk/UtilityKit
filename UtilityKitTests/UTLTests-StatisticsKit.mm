//
//  UTLTests-StatisticsKit.mm
//  UtilityKitTests
//
//  Created by KYUNGGUK MIN on 7/23/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

#define UTILITYKIT_INLINE_VERSION 4
#include <UtilityKit/NumericKit.h>
#include <UtilityKit/ArrayKit.h>
#include <UtilityKit/StatisticsKit.h>
#include <UtilityKit/AuxiliaryKit.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <random>
#include <cmath>

@interface UTLTests_StatisticsKit : XCTestCase

@end

@implementation UTLTests_StatisticsKit

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBitReversedRandomReal {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::StaticArray;

    constexpr double mean0 = .5, variance0 = 1./12, covar0 = 0;

    // exception check:
    try {
        BitReversedRandomReal{1}();
        XCTAssert(false, @"should have thrown an exception");
    } catch (std::exception&) {
        XCTAssert(true, @"");
    } catch (...) {
        XCTAssert(false, @"Oops! Unknown exception");
    }

    // move:
    try {
        BitReversedRandomReal r1{5};
        BitReversedRandomReal r2;
        XCTAssert(!!r1 && !r2);
        r2 = std::move(r1);
        XCTAssert(!r1 && !!r2);
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }

    // check algorithm consistency:
    try {
        const unsigned base = 5, seq = std::random_device{}();
        BitReversedRandomReal rng_1{base, seq};
        BitReversedRandomReal rng_2{base, seq};

        StaticArray<double, 10000> v;
        for (long i = 0; i < v.max_size(); ++i) {
            double const x1 = rng_1();
            double const x2 = rng_2();
            v.push_back(x1);
            bool const is_equal = x1 == x2;
            XCTAssert(is_equal, @"seq = %d, x1 = %e, x2 = %e", seq, x1, x2);
            if (!is_equal) {
                break;
            }
        }

        double mean{0}, variance{0};
        for (auto const &x : v) {
            mean += x;
            variance += x*x;
        }
        mean /= v.size();
        variance /= v.size();
        variance -= mean*mean;
        XCTAssert(std::abs(mean - mean0) < 2e-4 && std::abs(variance - variance0) < 1e-4, @"seq = %d, mean = %f, mean_err = %e, variance = %f, var_err = %e", seq, mean, std::abs(mean - mean0), variance, std::abs(variance - variance0));
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }

    // check coherence:
    try {
        const unsigned base1 = 5, base2 = 3, seq = std::random_device{}();
        BitReversedRandomReal rng_1{base1, seq};
        BitReversedRandomReal rng_2{base2, seq};

        StaticArray<double, 10000> v1, v2;
        for (long i = 0; i < v1.max_size(); ++i) {
            double const x1 = rng_1();
            v1.push_back(x1);
            double const x2 = rng_2();
            v2.push_back(x2);
            bool const is_equal = x1 == x2;
            XCTAssert(!is_equal, @"seq = %d, x1 = %e, x2 = %e", seq, x1, x2);
            if (is_equal) {
                break;
            }
        }

        double mean1{0}, var1{0}, mean2{0}, var2{0};
        for (long i = 0; i < v1.size(); ++i) {
            mean1 += v1.at(i);
            var1 += UTL::pow<2>(v1.at(i));
            mean2 += v2.at(i);
            var2 += UTL::pow<2>(v2.at(i));
        }
        mean1 /= v1.size();
        (var1 /= v1.size()) -= UTL::pow<2>(mean1);
        XCTAssert(std::abs(mean1 - mean0) < 2e-4 && std::abs(var1 - variance0) < 1e-4, @"seq = %d, mean = %f, mean_err = %e, variance = %f, var_err = %e", seq, mean1, std::abs(mean1 - mean0), var1, std::abs(var1 - variance0));
        mean2 /= v2.size();
        (var2 /= v2.size()) -= UTL::pow<2>(mean2);
        XCTAssert(std::abs(mean2 - mean0) < 2e-4 && std::abs(var2 - variance0) < 1e-4, @"seq = %d, mean = %f, mean_err = %e, variance = %f, var_err = %e", seq, mean2, std::abs(mean2 - mean0), var2, std::abs(var2 - variance0));

        double covar{0};
        for (long i = 0; i < v1.size(); ++i) {
            covar += (v1.at(i) - mean1)*(v2.at(i) - mean2);
        }
        covar /= v1.size() * std::sqrt(var1*var2);
        XCTAssert(std::abs(covar - covar0) < 1e-3, @"seq = %d, covar = %f, covar_err = %e", seq, covar, std::abs(covar - covar0));
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }
}

- (void)testNRRandomReal {
    using UTL::__4_::NRRandomReal;
    using UTL::__4_::StaticArray;

    constexpr double mean0 = .5, variance0 = 1./12, covar0 = 0;

    // move:
    try {
        NRRandomReal r1{5};
        NRRandomReal r2;
        XCTAssert(!!r1 && !r2);
        r2 = std::move(r1);
        XCTAssert(!r1 && !!r2);
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }

    // check algorithm consistency:
    try {
        std::random_device rng;
        const unsigned seed1 = rng(), seed2 = rng();
        NRRandomReal rng_1{seed1};
        NRRandomReal rng_2{seed2};

        StaticArray<double, 50000> v1, v2;
        for (long i = 0; i < v1.max_size(); ++i) {
            double const x1 = rng_1();
            v1.push_back(x1);
            double const x2 = rng_2();
            v2.push_back(x2);
            bool const is_equal = x1 == x2;
            XCTAssert(!is_equal, @"seed1 = %d, x1 = %e, seed2 = %d, x2 = %e", seed1, x1, seed2, x2);
            if (is_equal) {
                break;
            }
        }

        double mean1{0}, var1{0}, mean2{0}, var2{0};
        for (long i = 0; i < v1.size(); ++i) {
            mean1 += v1.at(i);
            var1 += UTL::pow<2>(v1.at(i));
            mean2 += v2.at(i);
            var2 += UTL::pow<2>(v2.at(i));
        }
        mean1 /= v1.size();
        (var1 /= v1.size()) -= UTL::pow<2>(mean1);
        XCTAssert(std::abs(mean1 - mean0) < 1e-2 && std::abs(var1 - variance0) < 1e-2, @"seed = %d, mean = %f, mean_err = %e, variance = %f, var_err = %e", seed1, mean1, std::abs(mean1 - mean0), var1, std::abs(var1 - variance0));
        mean2 /= v2.size();
        (var2 /= v2.size()) -= UTL::pow<2>(mean2);
        XCTAssert(std::abs(mean2 - mean0) < 1e-2 && std::abs(var2 - variance0) < 1e-2, @"seed = %d, mean = %f, mean_err = %e, variance = %f, var_err = %e", seed2, mean2, std::abs(mean2 - mean0), var2, std::abs(var2 - variance0));

        double covar{0};
        for (long i = 0; i < v1.size(); ++i) {
            covar += (v1.at(i) - mean1)*(v2.at(i) - mean2);
        }
        covar /= v1.size() * std::sqrt(var1*var2);
        XCTAssert(std::abs(covar - covar0) < 1e-2, @"seed1 = %d, seed2 = %d, covar = %f, covar_err = %e", seed1, seed2, covar, std::abs(covar - covar0));
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }
}

- (void)testUniformDistribution {
    using UTL::__4_::Distribution;
    using UTL::__4_::UniformDistribution;

    try {
        UniformDistribution const dist1;
        UniformDistribution dist2{dist1};
        XCTAssert(0.5 == dist1.mean() && 1./12 == dist1.variance(), @"mean = %f, var = %f", dist1.mean(), dist1.variance());
        XCTAssert(0.5 == dist2.mean() && 1./12 == dist2.variance(), @"mean = %f, var = %f", dist2.mean(), dist2.variance());

        dist2 = UniformDistribution{-1, 2};
        XCTAssert(0.5 == dist2.mean() && 3./4 == dist2.variance(), @"mean = %f, var = %f", dist2.mean(), dist2.variance());
        for (int i = -1; i <= 2; ++i) {
            XCTAssert(1./3 == dist2.pdf(i), @"pdf(%d) = %f", i, dist2.pdf(i));
        }
        XCTAssert(0. == dist2.cdf(-1) && 1./3 == dist2.cdf(0) && 2./3 == dist2.cdf(1) && 1. == dist2.cdf(2),
                  @"cdf(-1) = %f, cdf(0) = %f, cdf(1) = %f, cdf(2) = %f",
                  dist2.cdf(-1), dist2.cdf(0), dist2.cdf(1), dist2.cdf(2));
        XCTAssert(-1. == dist2.icdf(0) && 0.5 == dist2.icdf(.5) && 2. == dist2.icdf(1),
                  @"icdf(0) = %f, icdf(0.5) = %f, icdf(1) = %f",
                  dist2.icdf(0), dist2.icdf(.5), dist2.icdf(1));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testCustomDistribution {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::Distribution;
    using UTL::__4_::CustomDistribution;

    BitReversedRandomReal rng{2, 1};

    try {
        // maxwellian distribution:
        constexpr double xd = 3, scale = 2;
        constexpr double xmin = -5 + xd, xmax = 5 + xd;
        double (*pdf)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x - xd)) * M_2_SQRTPI/2. * scale;
        };
        double (*cdf)(double) = [](double x)->double {
            return .5*(1 + std::erf(x - xd));
        };

        // copy
        CustomDistribution _dist{xmin, xmax, pdf, true}, dist{_dist};
        XCTAssert(std::abs(xd - dist.mean()) < 1e-7 && std::abs(.5 - dist.variance()) < 1e-7, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(xmin == dist.min() && xmax == dist.max(), @"dist.min = %f, dist.max = %f", dist.min(), dist.max());
        {
            constexpr long n = 100;
            for (long i = 0; i <= n; ++i) {
                double const x0 = i*(xmax - xmin)/n + xmin;
                double const pdf0 = pdf(x0)/scale, pdf1 = dist.pdf(x0);
                double const cdf0 = cdf(x0), cdf1 = dist.cdf(x0);
                double const x1 = dist.icdf(cdf1);
                XCTAssert(std::abs(pdf0 - pdf1) < pdf(xd)/scale*1e-7, @"x0 = %f, pdf0 = %f, pdf1 = %f, err = %e", x0, pdf0, pdf1, std::abs(pdf0 - pdf1)/(pdf(xd)/scale));
                XCTAssert(std::abs(cdf0 - cdf1) < cdf(xmax)*1e-7, @"x0 = %f, cdf0 = %f, cdf1 = %f, err = %e", x0, pdf0, pdf1, std::abs(cdf0 - cdf1)/cdf(xmax));
                XCTAssert(std::abs(x1 - x0) < (xmax - xmin)*1e-7, @"x0 = %f, x1 = %f, err = %e", x0, x1, std::abs(x1 - x0)/(xmax - xmin));
            }
            double mean = 0, variance = 0;
            constexpr long m = 5000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-3 && std::abs(variance - dist.variance()) < dist.variance()*1e-2, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }

        // move
        dist = CustomDistribution{xmin, xmax, [pdf](double x) { return (*pdf)(x)/scale; }, cdf};
        XCTAssert(std::abs(xd - dist.mean()) < 1e-7 && std::abs(.5 - dist.variance()) < 1e-7, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(xmin == dist.min() && xmax == dist.max(), @"dist.min = %f, dist.max = %f", dist.min(), dist.max());
        {
            constexpr long n = 100;
            for (long i = 0; i <= n; ++i) {
                double const x0 = i*(xmax - xmin)/n + xmin;
                double const pdf0 = pdf(x0)/scale, pdf1 = dist.pdf(x0);
                double const cdf0 = cdf(x0), cdf1 = dist.cdf(x0);
                double const x1 = dist.icdf(cdf1);
                XCTAssert(std::abs(pdf0 - pdf1) < pdf(xd)/scale*1e-7, @"x0 = %f, pdf0 = %f, pdf1 = %f, err = %e", x0, pdf0, pdf1, std::abs(pdf0 - pdf1)/(pdf(xd)/scale));
                XCTAssert(std::abs(cdf0 - cdf1) < cdf(xmax)*1e-7, @"x0 = %f, cdf0 = %f, cdf1 = %f, err = %e", x0, pdf0, pdf1, std::abs(cdf0 - cdf1)/cdf(xmax));
                XCTAssert(std::abs(x1 - x0) < (xmax - xmin)*1e-7, @"x0 = %f, x1 = %f, err = %e", x0, x1, std::abs(x1 - x0)/(xmax - xmin));
            }
            double mean = 0, variance = 0;
            constexpr long m = 5000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-3 && std::abs(variance - dist.variance()) < dist.variance()*1e-2, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testCustomDistribution_Performance {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::CustomDistribution;

    BitReversedRandomReal rng{2, 1};
    for (long cnt = 0; cnt < 1000; ++cnt) {
        // maxwellian distribution:
        constexpr double xd = 3, scale = 2;
        constexpr double xmin = -5 + xd, xmax = 5 + xd;
        double (*pdf)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x - xd)) * M_2_SQRTPI/2. * scale;
        };
        double (*cdf)(double) = [](double x)->double {
            return .5*(1 + std::erf(x - xd));
        };

        // copy
        CustomDistribution _dist{xmin, xmax, pdf, true}, dist{_dist};
        dist = CustomDistribution{xmin, xmax, [pdf](double x) { return (*pdf)(x)/scale; }, cdf};
    }
}

- (void)testParallelMaxwellianDistribution {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::Distribution;
    using UTL::__4_::ParallelMaxwellianDistribution;

    BitReversedRandomReal rng{2, 1};

    try {
        // copy
        constexpr double vth1 = M_SQRT2, vd1 = 1.1;
        ParallelMaxwellianDistribution _dist{vth1, vd1}, dist{_dist};
        XCTAssert(std::abs(vd1 - dist.mean()) < 1e-10 && std::abs(1 - dist.variance()) < 1e-10, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(std::abs(dist.pdf(vd1) - M_2_SQRTPI/2/vth1) < 1e-10, @"pdf(vd) = %f", dist.pdf(vd1));
        XCTAssert(std::abs(dist.cdf(vd1) - .5) < 1e-10, @"cdf(vd) = %f", dist.cdf(vd1));
        XCTAssert(std::abs(vd1 - dist.icdf(.5)) < 1e-10, @"icdf(.5) = %f", dist.icdf(.5));
        XCTAssert(vth1 == dist.vth() && vd1 == dist.vd(), @"dist.vth = %f, dist.vd = %f", dist.vth(), dist.vd());
        {
            double mean = 0, variance = 0;
            constexpr long m = 5000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 2e-3 && std::abs(variance - dist.variance()) < dist.variance()*3e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }

        // move
        constexpr double vth2 = 1, vd2 = 0;
        dist = ParallelMaxwellianDistribution{vth2};
        XCTAssert(std::abs(vd2 - dist.mean()) < 1e-10 && std::abs(.5 - dist.variance()) < 1e-10, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(std::abs(dist.pdf(vd2) - M_2_SQRTPI/2/vth2) < 1e-10, @"pdf(vd) = %f", dist.pdf(vd2));
        XCTAssert(std::abs(dist.cdf(vd2) - .5) < 1e-10, @"cdf(vd) = %f", dist.cdf(vd2));
        XCTAssert(std::abs(vd2 - dist.icdf(.5)) < 1e-10, @"icdf(.5) = %f", dist.icdf(.5));
        XCTAssert(vth2 == dist.vth() && vd2 == dist.vd(), @"dist.vth = %f, dist.vd = %f", dist.vth(), dist.vd());
        {
            double mean = 0, variance = 0;
            constexpr long m = 5000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-3 && std::abs(variance - dist.variance()) < dist.variance()*1e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testPerpendicularMaxwellianDistribution {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::Distribution;
    using UTL::__4_::PerpendicularMaxwellianDistribution;

    BitReversedRandomReal rng{2, 1};

    try {
        // copy
        constexpr double vth1 = 2, mean1 = 1/M_2_SQRTPI*vth1, var1 = (1 - M_PI_4)*UTL::pow<2>(vth1);
        PerpendicularMaxwellianDistribution _dist{vth1}, dist{_dist};
        XCTAssert(std::abs(dist.mean() - mean1) < 1e-10 && std::abs(var1 - dist.variance()) < 1e-10, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(std::abs(dist.pdf(vth1) - 2/(M_E*vth1)) < 1e-10, @"pdf(vth) = %f", dist.pdf(vth1));
        XCTAssert(std::abs(dist.cdf(vth1) - (1 - 1/M_E)) < 1e-10, @"cdf(vth) = %f", dist.cdf(vth1));
        XCTAssert(std::abs(vth1 - dist.icdf(1 - 1/M_E)) < 1e-10, @"icdf(...) = %f", dist.icdf(1 - 1/M_E));
        XCTAssert(vth1 == dist.vth(), @"dist.vth = %f", dist.vth());
        {
            double mean = 0, variance = 0;
            constexpr long m = 5000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 2e-3 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }

        // move
        constexpr double vth2 = 1, mean2 = 1/M_2_SQRTPI*vth2, var2 = (1 - M_PI_4)*UTL::pow<2>(vth2);
        dist = PerpendicularMaxwellianDistribution{vth2};
        XCTAssert(std::abs(dist.mean() - mean2) < 1e-10 && std::abs(var2 - dist.variance()) < 1e-10, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(std::abs(dist.pdf(vth2) - 2/(M_E*vth2)) < 1e-10, @"pdf(vth) = %f", dist.pdf(vth2));
        XCTAssert(std::abs(dist.cdf(vth2) - (1 - 1/M_E)) < 1e-10, @"cdf(vth) = %f", dist.cdf(vth2));
        XCTAssert(std::abs(vth2 - dist.icdf(1 - 1/M_E)) < 1e-10, @"icdf(...) = %f", dist.icdf(1 - 1/M_E));
        XCTAssert(vth2 == dist.vth(), @"dist.vth = %f", dist.vth());
        {
            double mean = 0, variance = 0;
            constexpr long m = 5000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 2e-3 && std::abs(variance - dist.variance()) < dist.variance()*2e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testPerpendicularRingDistribution {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::Distribution;
    using UTL::__4_::PerpendicularRingDistribution;

    BitReversedRandomReal rng{2, 1};

    try {
        double (*A)(double) = [](double b)->double {
            return std::exp(-b*b) + 2/M_2_SQRTPI*b*std::erfc(-b);
        };
        auto const cdf = [A](double x, double b)->double {
            return (2/M_2_SQRTPI*b*std::erf(x - b) - std::exp(-UTL::pow<2>(x - b)))/A(b);
        };

        // copy
        constexpr double vth1 = .5, vr1 = 5, b1 = vr1/vth1;
        double const mean1 = (b1*std::exp(-b1*b1) + 2/M_2_SQRTPI*(b1*b1 + .5)*std::erfc(-b1))*vth1/A(b1);
        double const var1 = ((b1*b1 + 1)*std::exp(-b1*b1) + 2/M_2_SQRTPI*b1*(b1*b1 + 1.5)*std::erfc(-b1))*vth1*vth1/A(b1) - mean1*mean1;
        PerpendicularRingDistribution _dist{vth1, vr1}, dist{_dist};
        XCTAssert(std::abs(dist.mean() - mean1) < 1e-10 && std::abs(var1 - dist.variance()) < 1e-10, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(std::abs(dist.pdf(vr1) - 2*vr1/(A(b1)*vth1*vth1)) < 1e-10, @"pdf(vr) = %f", dist.pdf(vr1));
        XCTAssert(std::abs(dist.cdf(vr1 + vth1) - (cdf(1 + b1, b1) - cdf(0, b1))) < 1e-10, @"exact_cdf = %f, dist.cdf(vr + vth) = %f, err = %e", cdf(1 + b1, b1) - cdf(0, b1), dist.cdf(vr1 + vth1), std::abs(dist.cdf(vr1 + vth1) - (cdf(1 + b1, b1) - cdf(0, b1))));
        XCTAssert(std::abs((vr1 + vth1) - dist.icdf(cdf(1 + b1, b1) - cdf(0, b1))) < 1e-10, @"(vr + vth) = %f, icdf(cdf(vr + vth)) = %f", vr1 + vth1, dist.icdf(cdf(1 + b1, b1) - cdf(0, b1)));
        XCTAssert(vth1 == dist.vth() && vr1 == dist.vr(), @"dist.vth = %f, dist.vr = %f", dist.vth(), dist.vr());
        {
            double mean = 0, variance = 0;
            constexpr long m = 50000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-4 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }

        // move
        constexpr double vth2 = 2, vr2 = 0, mean2 = 1/M_2_SQRTPI*vth2, var2 = (1 - M_PI_4)*UTL::pow<2>(vth2);
        dist = PerpendicularRingDistribution{vth2};
        XCTAssert(std::abs(dist.mean() - mean2) < 1e-10 && std::abs(var2 - dist.variance()) < 1e-10, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(std::abs(dist.pdf(vth2) - 2/(M_E*vth2)) < 1e-10, @"pdf(vth) = %f", dist.pdf(vth2));
        XCTAssert(std::abs(dist.cdf(vth2) - (1 - 1/M_E)) < 1e-10, @"cdf(vth) = %f", dist.cdf(vth2));
        XCTAssert(std::abs(vth2 - dist.icdf(1 - 1/M_E)) < 1e-10, @"icdf(...) = %f, err = %e", dist.icdf(1 - 1/M_E), std::abs(vth2 - dist.icdf(1 - 1/M_E)));
        XCTAssert(vth2 == dist.vth() && vr2 == dist.vr(), @"dist.vth = %f, dist.vr = %f", dist.vth(), dist.vr());
        {
            double mean = 0, variance = 0;
            constexpr long m = 5000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 2e-3 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testShellDistribution {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::Distribution;
    using UTL::__4_::ShellDistribution;

    BitReversedRandomReal rng{2, 1};

    try {
        double (*A)(double) = [](double b)->double {
            return M_2_SQRTPI*b*std::exp(-b*b) + (2*b*b + 1)*std::erfc(-b);
        };
        auto const cdf = [A](double x, double b)->double {
            return -(M_2_SQRTPI*((x - b) + 2*b)*std::exp(-UTL::pow<2>(x - b)) - (2*b*b + 1)*std::erf(x - b))/A(b);
        };

        // copy
        constexpr double vth1 = .5, vs1 = 5, b1 = vs1/vth1;
        double const mean1 = (M_2_SQRTPI*(b1*b1 + 1)*std::exp(-b1*b1) + b1*(2*b1*b1 + 3)*std::erfc(-b1))*vth1/A(b1);
        double const var1 = (M_2_SQRTPI*b1*(b1*b1 + 2.5)*std::exp(-b1*b1) + (2*UTL::pow<4>(b1) + 6*b1*b1 + 1.5)*std::erfc(-b1))*vth1*vth1/A(b1) - mean1*mean1;
        ShellDistribution _dist{vth1, vs1}, dist{_dist};
        XCTAssert(std::abs(dist.mean() - mean1) < 1e-10 && std::abs(var1 - dist.variance()) < 1e-10, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(std::abs(dist.pdf(vs1) - 2*M_2_SQRTPI*b1*b1/(A(b1)*vth1)) < 1e-10, @"pdf(vr) = %f", dist.pdf(vs1));
        XCTAssert(std::abs(dist.cdf(vs1 + vth1) - (cdf(1 + b1, b1) - cdf(0, b1))) < 1e-10, @"exact_cdf = %f, dist.cdf(vr + vth) = %f, err = %e", cdf(1 + b1, b1) - cdf(0, b1), dist.cdf(vs1 + vth1), std::abs(dist.cdf(vs1 + vth1) - (cdf(1 + b1, b1) - cdf(0, b1))));
        XCTAssert(std::abs((vs1 + vth1) - dist.icdf(cdf(1 + b1, b1) - cdf(0, b1))) < 1e-9, @"(vr + vth) = %f, icdf(cdf(vr + vth)) = %f", vs1 + vth1, dist.icdf(cdf(1 + b1, b1) - cdf(0, b1)));
        XCTAssert(vth1 == dist.vth() && vs1 == dist.vs(), @"dist.vth = %f, dist.vs = %f", dist.vth(), dist.vs());
        {
            double mean = 0, variance = 0;
            constexpr long m = 50000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-4 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }

        // move
        constexpr double vth2 = 2, vs2 = 0, mean2 = M_2_SQRTPI*vth2, var2 = (1.5 - 1/M_PI_4)*UTL::pow<2>(vth2);
        dist = ShellDistribution{vth2};
        XCTAssert(std::abs(dist.mean() - mean2) < 1e-10 && std::abs(var2 - dist.variance()) < 1e-10, @"mean = %f, var = %f", dist.mean(), dist.variance());
        XCTAssert(std::abs(dist.pdf(vth2) - 4/(M_E*2/M_2_SQRTPI)/vth2) < 1e-10, @"pdf(vth) = %f", dist.pdf(vth2));
        XCTAssert(std::abs(dist.cdf(vth2) - (-2/(M_E*2/M_2_SQRTPI) + std::erf(1))) < 1e-10, @"cdf(vth) = %f", dist.cdf(vth2));
        XCTAssert(std::abs(vth2 - dist.icdf(dist.cdf(vth2))) < 1e-10, @"icdf(...) = %f, err = %e", dist.icdf(dist.cdf(vth2)), std::abs(vth2 - dist.icdf(dist.cdf(vth2))));
        XCTAssert(vth2 == dist.vth() && vs2 == dist.vs(), @"dist.vth = %f, dist.vs = %f", dist.vth(), dist.vs());
        {
            double mean = 0, variance = 0;
            constexpr long m = 5000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 2e-3 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testSineAlphaPitchAngleDistribution {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::Distribution;
    using UTL::__4_::SineAlphaPitchAngleDistribution;

    BitReversedRandomReal rng{2, 1};

    try {
        // copy
        constexpr double n1 = 5.1, mean1 = M_PI_2, var1 = 0.1399381400978776;
        constexpr double alpha1 = 91*M_PI/180, pdf1 = 1.02539946308462, cdf1 = 0.5179076865262811;
        SineAlphaPitchAngleDistribution _dist{n1}, dist{_dist};
        XCTAssert(std::abs(dist.mean() - mean1) < 1e-10 && std::abs(var1 - dist.variance()) < var1*2e-8, @"mean = %f, var = %f, var_err = %e", dist.mean(), dist.variance(), std::abs(var1 - dist.variance())/var1);
        XCTAssert(std::abs(dist.pdf(alpha1) - pdf1) < 1e-10, @"pdf(alpha) = %f", dist.pdf(alpha1));
        XCTAssert(std::abs(dist.cdf(alpha1) - cdf1) < 4e-7, @"cdf(alpha) = %f, err = %e", dist.cdf(alpha1), std::abs(dist.cdf(alpha1) - cdf1));
        XCTAssert(std::abs(alpha1 - dist.icdf(cdf1)) < 3e-7, @"icdf(cdf(alpha)) = %f, err = %e", dist.icdf(cdf1), std::abs(alpha1 - dist.icdf(cdf1)));
        XCTAssert(n1 == dist.n(), @"dist.n = %f", dist.n());
        {
            double mean = 0, variance = 0;
            constexpr long m = 50000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-4 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }

        // move
        constexpr double n2 = 0, mean2 = M_PI_2, var2 = (-8 + UTL::pow<2>(M_PI))/4.;
        const double alpha2 = 91*M_PI/180, pdf2 = std::sin(alpha2)/2., cdf2 = UTL::pow<2>(std::sin(alpha2/2));
        dist = SineAlphaPitchAngleDistribution{n2};
        XCTAssert(std::abs(dist.mean() - mean2) < 1e-10 && std::abs(var2 - dist.variance()) < var2*5e-9, @"mean = %f, var = %f, var_err = %e", dist.mean(), dist.variance(), std::abs(var2 - dist.variance())/var2);
        XCTAssert(std::abs(dist.pdf(alpha2) - pdf2) < 1e-10, @"pdf(alpha) = %f", dist.pdf(alpha2));
        XCTAssert(std::abs(dist.cdf(alpha2) - cdf2) < 2e-7, @"cdf(alpha) = %f, err = %e", dist.cdf(alpha2), std::abs(dist.cdf(alpha2) - cdf2));
        XCTAssert(std::abs(alpha2 - dist.icdf(cdf2)) < 3e-7, @"icdf(cdf(alpha)) = %f, err = %e", dist.icdf(cdf2), std::abs(alpha2 - dist.icdf(cdf2)));
        XCTAssert(n2 == dist.n(), @"dist.n = %f", dist.n());
        {
            double mean = 0, variance = 0;
            constexpr long m = 50000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-4 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testSubtractedMaxwellianDistribution {
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::Distribution;
    using UTL::__4_::SubtractedMaxwellianDistribution;

    BitReversedRandomReal rng{2, 1};

    try {
        // copy
        const double vth1 = std::sqrt(0.8), T2OT1_1 = 0.9, n2On1_1 = 5.3/6.3;
        const double mean1 = 1.283748148784579, var1 = 0.1879906904921638;
        const double v1 = vth1, pdf1 = 0.6918347596097822, cdf1 = 0.1900359319099438;
        SubtractedMaxwellianDistribution _dist{vth1, T2OT1_1, n2On1_1}, dist{_dist};
        XCTAssert(std::abs(dist.mean() - mean1) < 1e-10 && std::abs(var1 - dist.variance()) < var1*1e-10, @"mean = %f, var = %f, var_err = %e", dist.mean(), dist.variance(), std::abs(var1 - dist.variance())/var1);
        XCTAssert(std::abs(dist.pdf(v1) - pdf1) < pdf1*1e-10, @"pdf(v) = %f, err = %e", dist.pdf(v1), std::abs(dist.pdf(v1) - pdf1)/pdf1);
        XCTAssert(std::abs(dist.cdf(v1) - cdf1) < cdf1*1e-10, @"cdf(v) = %f, err = %e", dist.cdf(v1), std::abs(dist.cdf(v1) - cdf1)/cdf1);
        XCTAssert(std::abs(v1 - dist.icdf(cdf1)) < v1*2e-10, @"icdf(cdf(v)) = %f, err = %e", dist.icdf(cdf1), std::abs(v1 - dist.icdf(cdf1))/v1);
        XCTAssert(std::abs(vth1 - dist.vth1()) < 1e-10 && std::abs(n2On1_1 - dist.n2On1()) < 1e-10 && std::abs(T2OT1_1 - dist.T2OT1()) < 1e-10, @"dist.vth = %f, dist.n2/n1 = %f, dist.T2/T1 = %f", dist.vth1(), dist.n2On1(), dist.T2OT1());
        {
            double mean = 0, variance = 0;
            constexpr long m = 50000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-4 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }

        // move
        constexpr double vth2 = 2, T2OT1_2 = 1, n2On1_2 = 0;
        constexpr double mean2 = M_2_SQRTPI*vth2, var2 = (1.5 - 1/M_PI_4)*UTL::pow<2>(vth2);
        const double v2 = vth2, pdf2 = 2*M_2_SQRTPI/M_E/vth2, cdf2 = std::erf(1) - M_2_SQRTPI/M_E;
        dist = SubtractedMaxwellianDistribution{vth2};
        XCTAssert(std::abs(dist.mean() - mean2) < 1e-10 && std::abs(var2 - dist.variance()) < var2*1e-10, @"mean = %f, var = %f, var_err = %e", dist.mean(), dist.variance(), std::abs(var2 - dist.variance())/var2);
        XCTAssert(std::abs(dist.pdf(v2) - pdf2) < pdf2*1e-10, @"pdf(v) = %f, err = %e", dist.pdf(v2), std::abs(dist.pdf(v2) - pdf2)/pdf2);
        XCTAssert(std::abs(dist.cdf(v2) - cdf2) < cdf2*1e-10, @"cdf(v) = %f, err = %e", dist.cdf(v2), std::abs(dist.cdf(v2) - cdf2)/cdf2);
        XCTAssert(std::abs(v2 - dist.icdf(cdf2)) < v2*2e-10, @"icdf(cdf(v)) = %f, err = %e", dist.icdf(cdf2), std::abs(v2 - dist.icdf(cdf2))/v2);
        XCTAssert(std::abs(vth2 - dist.vth1()) < 1e-10 && std::abs(n2On1_2 - dist.n2On1()) < 1e-10 && std::abs(T2OT1_2 - dist.T2OT1()) < 1e-10, @"dist.vth = %f, dist.n2/n1 = %f, dist.T2/T1 = %f", dist.vth1(), dist.n2On1(), dist.T2OT1());
        {
            double mean = 0, variance = 0;
            constexpr long m = 50000;
            for (long i = 0; i <= m; ++i) {
                double const x = dist.icdf(rng());
                mean += x;
                variance += x*x;
            }
            mean /= m;
            (variance /= m) -= mean*mean;
            XCTAssert(std::abs(mean - dist.mean()) < 1e-4 && std::abs(variance - dist.variance()) < dist.variance()*6e-3, @"mean = %f, mean_err = %e, variance = %f, var_err = %e", mean, std::abs(mean - dist.mean()), variance, std::abs(variance - dist.variance())/dist.variance());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testRandomVariate_PlasmapauseDensityDistribution {
    using UTL::__4_::Vector;
    using UTL::__4_::StaticArray;
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::RandomVariate;
    using UTL::__4_::UniformDistribution;
    using UTL::__4_::CustomDistribution;

    try {
        BitReversedRandomReal rng1{2, 1};
        BitReversedRandomReal rng2{3, 1};

        constexpr long Nc = 100, Nx = 50, Np = Nc*Nx*Nx;
        constexpr double nin = 9, nout = 1, Lpp = -5, dLpp = 10, factor = 4.56, q_max = .5*Nx, q_min = -q_max;
        double (*pdf)(double) = [](double L)->double {
            return 1 - (nin - nout)/(nin + nout)*std::tanh(factor*(L - Lpp)/dLpp);
        };
        UniformDistribution const dist2{q_min, q_max};
        CustomDistribution const dist1{q_min, q_max, pdf, true};

        RandomVariate const rv1{&rng1, &dist1};
        RandomVariate const rv2{&rng2, &dist2};

        std::ostringstream os;
        os.setf(os.fixed);
        os.precision(12);

        printo(os, "Nc = ", Nc, "\n\n");
        printo(os, "Nx = ", Nx, "\n\n");
        printo(os, "Np = ", Np, "\n\n");
        printo(os, "NBoundary = {", nin, ", ", nout, "}", "\n\n");
        printo(os, "Lpp = ", Lpp, "\n\n");
        printo(os, "dLpp = ", dLpp, "\n\n");
        printo(os, "factor = ", factor, "\n\n");

        StaticArray<Vector<double, 2>, Np> pts(Np);
        for (auto &pt : pts) {
            pt.x = rv1();
            pt.y = rv2();
        }
        printo(os, "particles = ", pts, "\n\n");

        if ( (0) ) {
            NSURL *path = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"PlasmapauseDensityDistribution.m"]];
            NSError *error;
            XCTAssert([@(os.str().c_str()) writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testRandomVariate_BiMaxwellianDistribution {
    using UTL::__4_::Vector;
    using UTL::__4_::StaticArray;
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::RandomVariate;
    using UTL::__4_::ParallelMaxwellianDistribution;
    using UTL::__4_::PerpendicularMaxwellianDistribution;

    try {
        BitReversedRandomReal rng1{2, 1};
        BitReversedRandomReal rng2{3, 1};

        constexpr long Np = 100000;
        constexpr double beta1 = 0.01, T2OT1 = 5, vd = .1;
        ParallelMaxwellianDistribution const dist1{std::sqrt(beta1), vd};
        PerpendicularMaxwellianDistribution const dist2{std::sqrt(beta1*T2OT1)};

        RandomVariate const rv1{&rng1, &dist1};
        RandomVariate const rv2{&rng2, &dist2};

        std::ostringstream os;
        os.setf(os.fixed);
        os.precision(12);

        printo(os, "Np = ", Np, "\n\n");
        printo(os, "beta1 = ", beta1, "\n\n");
        printo(os, "T2OT1 = ", T2OT1, "\n\n");
        printo(os, "vd = ", vd, "\n\n");

        StaticArray<Vector<double, 2>, Np> pts(Np);
        for (auto &pt : pts) {
            pt.x = rv1();
            pt.y = rv2();
        }
        printo(os, "particles = ", pts, "\n\n");

        if ( (0) ) {
            NSURL *path = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"BiMaxwellianDistribution.m"]];
            NSError *error;
            XCTAssert([@(os.str().c_str()) writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testRandomVariate_MaxwellianRingDistribution {
    using UTL::__4_::Vector;
    using UTL::__4_::StaticArray;
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::RandomVariate;
    using UTL::__4_::ParallelMaxwellianDistribution;
    using UTL::__4_::PerpendicularRingDistribution;

    try {
        BitReversedRandomReal rng1{2, 1};
        BitReversedRandomReal rng2{3, 1};

        constexpr long Np = 100000;
        constexpr double beta1 = 0.02, vr = 2;
        ParallelMaxwellianDistribution const dist1{std::sqrt(beta1)};
        PerpendicularRingDistribution const dist2{std::sqrt(beta1), vr};

        RandomVariate const rv1{&rng1, &dist1};
        RandomVariate const rv2{&rng2, &dist2};

        std::ostringstream os;
        os.setf(os.fixed);
        os.precision(12);

        printo(os, "Np = ", Np, "\n\n");
        printo(os, "beta1 = ", beta1, "\n\n");
        printo(os, "vr = ", vr, "\n\n");

        StaticArray<Vector<double, 2>, Np> pts(Np);
        for (auto &pt : pts) {
            pt.x = rv1();
            pt.y = rv2();
        }
        printo(os, "particles = ", pts, "\n\n");

        if ( (0) ) {
            NSURL *path = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"MaxwellianRingDistribution.m"]];
            NSError *error;
            XCTAssert([@(os.str().c_str()) writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testRandomVariate_PartialShellDistribution {
    using UTL::__4_::Vector;
    using UTL::__4_::StaticArray;
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::RandomVariate;
    using UTL::__4_::ShellDistribution;
    using UTL::__4_::SineAlphaPitchAngleDistribution;

    try {
        BitReversedRandomReal rng1{2, 1};
        BitReversedRandomReal rng2{3, 1};

        constexpr long Np = 100000;
        constexpr double beta = 0.02, vs = 2, l = 5;
        ShellDistribution const dist1{std::sqrt(beta), vs};
        SineAlphaPitchAngleDistribution const dist2{l};

        RandomVariate const rv1{&rng1, &dist1};
        RandomVariate const rv2{&rng2, &dist2};

        std::ostringstream os;
        os.setf(os.fixed);
        os.precision(12);

        printo(os, "Np = ", Np, "\n\n");
        printo(os, "beta = ", beta, "\n\n");
        printo(os, "vs = ", vs, "\n\n");
        printo(os, "aniIdx = ", l, "\n\n");

        StaticArray<Vector<double, 2>, Np> pts(Np);
        for (auto &pt : pts) {
            pt.x = rv1();
            pt.y = rv2();
        }
        printo(os, "particles = ", pts, "\n\n");

        if ( (0) ) {
            NSURL *path = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"PartialShellDistribution.m"]];
            NSError *error;
            XCTAssert([@(os.str().c_str()) writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testRandomVariate_SubtractedMaxwellianDistribution {
    using UTL::__4_::Vector;
    using UTL::__4_::StaticArray;
    using UTL::__4_::BitReversedRandomReal;
    using UTL::__4_::RandomVariate;
    using UTL::__4_::SubtractedMaxwellianDistribution;

    try {
        BitReversedRandomReal rng1{2, 1};

        constexpr long Np = 200000;
        constexpr double beta1 = .8, T2OT1 = .9, n2On1 = 5.3/6.3;
        SubtractedMaxwellianDistribution const dist1{std::sqrt(beta1), T2OT1, n2On1};

        RandomVariate const rv1{&rng1, &dist1};

        std::ostringstream os;
        os.setf(os.fixed);
        os.precision(12);

        printo(os, "Np = ", Np, "\n\n");
        printo(os, "beta1 = ", beta1, "\n\n");
        printo(os, "T2OT1 = ", T2OT1, "\n\n");
        printo(os, "n2On1 = ", n2On1, "\n\n");

        StaticArray<Vector<double, 1>, Np> pts(Np);
        for (auto &pt : pts) {
            pt = rv1();
        }
        printo(os, "particles = ", pts, "\n\n");

        if ( (0) ) {
            NSURL *path = [NSURL fileURLWithPathComponents:@[NSHomeDirectory(), @"Downloads", @"SubtractedMaxwellianDistribution.m"]];
            NSError *error;
            XCTAssert([@(os.str().c_str()) writeToURL:path atomically:YES encoding:NSUTF8StringEncoding error:&error], @"%@", error);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
