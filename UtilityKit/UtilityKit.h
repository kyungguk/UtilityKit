//
//  UtilityKit.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/15/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#if defined(__APPLE__) && defined(__OBJC__)
#import <Cocoa/Cocoa.h>

//! Project version number for UtilityKit.
FOUNDATION_EXPORT double UtilityKitVersionNumber;

//! Project version string for UtilityKit.
FOUNDATION_EXPORT const unsigned char UtilityKitVersionString[];
#endif

// In this header, you should import all the public headers of your framework using statements like #import <UtilityKit/PublicHeader.h>


#if defined(__cplusplus)

#if !defined(UTILITYKIT_INLINE_VERSION)
#define UTILITYKIT_INLINE_VERSION 4
#endif

#include <UtilityKit/ThreadKit.h>
#include <UtilityKit/MemoryKit.h>
#include <UtilityKit/NumericKit.h>
#include <UtilityKit/ArrayKit.h>
#include <UtilityKit/StatisticsKit.h>
#include <UtilityKit/SIMDKit.h>
#include <UtilityKit/ContainerKit.h>
#include <UtilityKit/AlgorithmKit.h> // after arithematic operators are defined
#include <UtilityKit/AuxiliaryKit.h> // should come at the end because operator<< should be defined before printo

namespace Utility = UTL;
// TODO: Check the usage of &&

#endif
