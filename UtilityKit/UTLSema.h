//
//  UTLSema.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/24/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSema_h
#define UTLSema_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLLock.h>
#include <UtilityKit/UTLCondV.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
    /**
     @brief Generic counting semaphore.
     @discussion Instead of usual integral types, any generic type can be used for the critical value type if

     - the generic type is copy-constructable,
     - pre-decrement (called upon `wait` call) and post-increment (called upon `post` call) operators are defined for the generic type, which respectively decrements and increments the value of the generic type similar to the integral types, and
     - boolean cast is defined and returns false if a critical value is reached (equivalent to zero) else true otherwise.

     If the boolean cast returns false, any `wait` call is blocked until another thread `post`s.

     One can override the pre-decrement and post-increment operators to observe `wait` and `post` calls and to do synchronized operations.
     It is guaranteed that only one thread enters these operators.

     @note For native integral types, the initial value should be non-negative.
     */
    template <typename Crit, typename Lock = Mutex>
    class Sema {
        CondV<Lock> __cv_;
        Lock __mx_;
        Crit __ctr_;

        Sema(Sema const&) = delete;
        Sema &operator=(Sema const&) = delete;
    public:
        ~Sema() = default;

        /**
         @brief Construct object with an initial value.
         @discussion The initial value should be non-negative for an integral type.
         */
        explicit Sema(Crit __x) : __mx_(), __cv_(), __ctr_(__x) {}

        /**
         @brief Decrement the critical value by 1.
         @discussion The call is blocked if the negate of the critical variable is true.
         The block is removed if another thread `post`s.
         */
        void wait();

        /**
         @brief Increment the critical variable by 1.
         @discussion The negate of the critical variable before the increment is true, this call wakes up the blocked threads by the `wait' call.
         */
        void post();

        /**
         @brief Implicit type cast to the critical type.
         */
        operator Crit() noexcept(noexcept(LockG<Lock>(__mx_)));

        // short hand to wait:
        Sema& operator--() { return wait(), *this; }
        // short hand to post:
        Sema& operator++() { return post(), *this; }
    };

    template <typename Crit, typename Lock>
    void Sema<Crit, Lock>::wait()
    {
        LockG<Lock> l(__mx_);
        while (!__ctr_) {
            __cv_.wait(__mx_);
        }
        --__ctr_;
    }
    template <typename Crit, typename Lock>
    void Sema<Crit, Lock>::post()
    {
        LockG<Lock> l(__mx_);
        if (!__ctr_++) {
            __cv_.notify_all();
        }
    }
    template <typename Crit, typename Lock>
    Sema<Crit, Lock>::operator Crit() noexcept(noexcept(LockG<Lock>(__mx_)))
    {
        LockG<Lock> l(__mx_);
        return __ctr_;
    }
} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLLock.h>
#include <UtilityKit/UTLCondV.h>
#include <queue>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class Lock>
    class Sema;

    /**
     @brief Counting semaphore.
     @discussion Implementation based on condition variable.
     */
    template <>
    class Sema<Mutex> {
        CondV _cv;
        Mutex _mx;
        long _crit;

        Sema(Sema const&) = delete;
        Sema &operator=(Sema const&) = delete;
    public:
        using critical_type = long;

        ~Sema();

        /**
         @brief Construct object with an initial value.
         @discussion The initial value should be non-negative.
         */
        explicit Sema(critical_type const crit);

        /**
         @brief Decrement the critical value by 1.
         @discussion The call is blocked if the critical value is 0.
         The block is removed if another thread `post`s.
         */
        void wait();

        /**
         @brief Increment the critical variable by 1.
         @discussion The negate of the critical variable before the increment is true, this call wakes up the blocked threads by the `wait' call.
         */
        void post();

        /**
         @brief Implicit type cast to the critical type.
         */
        operator critical_type() noexcept { LockG<Mutex> _{_mx}; return _crit; }

        // short hand to wait:
        Sema& operator--() { wait(); return *this; }
        // short hand to post:
        Sema& operator++() { post(); return *this; }
    };

    /**
     @brief Counting semaphore.
     @discussion Implementation based on spin lock.
     */
    template <>
    class Sema<SpinLock> {
        long _crit;
        std::queue<SpinLock *> _consumer_q;
        SpinLock _key;

        Sema(Sema const&) = delete;
        Sema &operator=(Sema const&) = delete;
    public:
        using critical_type = long;

        ~Sema();

        /**
         @brief Construct object with an initial value.
         @discussion The initial value should be non-negative.
         */
        explicit Sema(critical_type const crit);

        /**
         @brief Decrement the critical value by 1.
         @discussion The call is blocked if the critical value is 0.
         The block is removed if another thread `post`s.
         */
        void wait();

        /**
         @brief Increment the critical variable by 1.
         @discussion The negate of the critical variable before the increment is true, this call wakes up the blocked threads by the `wait' call.
         */
        void post();

        /**
         @brief Implicit type cast to the critical type.
         */
        operator critical_type() noexcept { LockG<SpinLock> _{_key}; return _crit; }

        // short hand to wait:
        Sema& operator--() { wait(); return *this; }
        // short hand to post:
        Sema& operator++() { post(); return *this; }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLSema_h */
