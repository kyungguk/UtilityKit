//
//  UTLOptional.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/30/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLOptional_hh
#define UTLOptional_hh

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <exception>
#include <ostream>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    // null_value exception:
    template <typename T>
    class Optional<T>::nil_access : public std::exception {
        friend Optional<T>;
        const char *_reason;
        explicit nil_access() : _reason(__PRETTY_FUNCTION__) {}
    public:
        virtual ~nil_access() = default;
        virtual const char* what() const noexcept { return _reason; }
    };

    // assignment:
    template <typename T>
    Optional<T>& Optional<T>::operator=(Optional const& o) {
        if (this != &o) {
            if (o) *this = *o;
            else this->~Optional();
        }
        return *this;
    }
    template <typename T>
    Optional<T>& Optional<T>::operator=(Optional&& o) {
        if (this != &o) {
            if (o) *this = std::move(*o);
            else this->~Optional();
        }
        return *this;
    }

    template <typename T>
    template <typename U, typename std::enable_if<std::is_convertible<U const&, T>::value, int>::type>
    Optional<T>& Optional<T>::operator=(Optional<U> const& o) {
        if (this != &o) {
            if (o) *this = *o;
            else this->~Optional();
        }
        return *this;
    }
    template <typename T>
    template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, int>::type>
    Optional<T>& Optional<T>::operator=(Optional<U>&& o) {
        if (this != &o) {
            if (o) *this = std::move(*o);
            else this->~Optional();
        }
        return *this;
    }

    template <typename T>
    template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, int>::type>
    Optional<T>& Optional<T>::operator=(U&& u) {
        if (*this) *_v = std::forward<U>(u);
        else _v = _init(std::forward<U>(u));
        return *this;
    }

    // observers:
    template <typename T>
    T const& Optional<T>::value() const {
        if (*this) return **this;
        throw nil_access();
    }
    template <typename T>
    T      & Optional<T>::value() {
        if (*this) return **this;
        throw nil_access();
    }

    template <typename T>
    template <typename U>
    T Optional<T>::value_or(U&& default_value) const& {
        return bool(*this) ? **this : static_cast<T>(std::forward<U>(default_value));
    }
    template <typename T>
    template <typename U>
    T Optional<T>::value_or(U&& default_value) && {
        return bool(*this) ? std::move(**this) : static_cast<T>(std::forward<U>(default_value));
        }

        // modifiers:
        template <typename T>
        void Optional<T>::swap(Optional& o) {
#if 1
            std::swap(_storage, o._storage);
            std::swap(_v, o._v);
            // At this point, _v cross-references the each other's storage.
            // This must be corrected.
            if (nullptr != _v) _v = reinterpret_cast<decltype(_v)>(_storage);
            if (nullptr != o._v) o._v = reinterpret_cast<decltype(o._v)>(o._storage);
#else
            if (bool(*this) && bool(o)) {
                std::swap(**this, *o);
            } else if (*this) {
                o = std::move(**this);
                this->~Optional();
            } else if (o) {
                *this = std::move(*o);
                o.~Optional();
            } else {
                // do nothing
            }
#endif
        }

        // MARK: Output stream:
        template<class _CharT, class _Traits, class _Tp>
        std::basic_ostream<_CharT, _Traits>&
        operator<<(std::basic_ostream<_CharT, _Traits>& __os, Optional<_Tp> const& o) {
            if (o) return __os << *o;
            else return __os << "(nil)";
        }

    } // namespace __3_
    UTILITYKIT_END_NAMESPACE


    // MARK:- Version 4
    //
#include <exception>
#include <ostream>

    // MARK: nil_access
    //
    template <typename T>
    class UTL::__4_::Optional<T>::nil_access : public std::exception {
        friend Optional<T>;
        const char *_reason;
        explicit nil_access() noexcept : _reason(__PRETTY_FUNCTION__) {}
    public:
        virtual ~nil_access() = default;
        virtual const char* what() const noexcept { return _reason; }
    };

    // MARK: operator=
    //
    template <typename T>
    auto UTL::__4_::Optional<T>::operator=(Optional const& o) -> Optional<T>& {
        if (this != &o) {
            if (o) {
                *this = *o;
            } else {
                reset();
            }
        }
        return *this;
    }
    template <typename T>
    auto UTL::__4_::Optional<T>::operator=(Optional&& o) -> Optional<T>& {
        if (this != &o) {
            if (o) {
                *this = std::move(*o);
            } else {
                reset();
            }
        }
        return *this;
    }

    template <typename T>
    template <typename U, typename std::enable_if<std::is_convertible<U const&, T>::value, long>::type>
    auto UTL::__4_::Optional<T>::operator=(Optional<U> const& o) -> Optional<T>& {
        if (o) {
            *this = *o;
        } else {
            reset();
        }
        return *this;
    }
    template <typename T>
    template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, long>::type>
    auto UTL::__4_::Optional<T>::operator=(Optional<U>&& o) -> Optional<T>& {
        if (o) {
            *this = std::move(*o);
        } else {
            reset();
        }
        return *this;
    }

    template <typename T>
    template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, long>::type>
    auto UTL::__4_::Optional<T>::operator=(U&& u) -> Optional<T>& {
        if (*this) {
            **this = std::forward<U>(u);
        } else {
            _init(std::forward<U>(u));
        }
        return *this;
    }

    // MARK: value
    //
    template <typename T>
    auto UTL::__4_::Optional<T>::value() const -> T const& {
        if (*this) return **this;
        throw nil_access();
    }
    template <typename T>
    auto UTL::__4_::Optional<T>::value() -> T& {
        if (*this) return **this;
        throw nil_access();
    }

    // MARK: value_or
    //
    template <typename T>
    template <typename U>
    T UTL::__4_::Optional<T>::value_or(U&& default_value) const& {
        return *this ? **this : static_cast<T>(std::forward<U>(default_value));
    }
    template <typename T>
    template <typename U>
    T UTL::__4_::Optional<T>::value_or(U&& default_value) && {
        return *this ? std::move(**this) : static_cast<T>(std::forward<U>(default_value));
        }

        // MARK:- Output Stream
        //
        template<class CharT, class CharTraits, class T>
        auto operator<<(std::basic_ostream<CharT, CharTraits>& os, UTL::__4_::Optional<T> const& opt) -> decltype(os) {
            if (opt) return os << *opt;
                else return os << "(nil)";
                }

        // MARK: std::swap
        //
                namespace std {
                    template<class T>
                    void swap(UTL::__4_::Optional<T>& x, UTL::__4_::Optional<T>& y) noexcept(noexcept(x.swap(y))) {
                        x.swap(y);
                    }
                }

#endif /* UTLOptional_hh */
