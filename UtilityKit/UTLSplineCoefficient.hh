//
//  UTLSplineCoefficient.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/20/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSplineCoefficient_hh
#define UTLSplineCoefficient_hh

// MARK:- Version 4
//
#include <stdexcept>
#include <algorithm>
#include <limits>
#include <map>

template <class RealType, long Order>
RealType UTL::__4_::SplineCoefficient<RealType, Order>::integrate() const noexcept
{
    value_type result{};
    for (auto it = _table.begin(), end = _table.end() - 1; it != end; ++it) {
        result += _integrate_subinterval(it->second, (it + 1)->first - it->first);
    }
    return result;
}

template <class RealType, long Order>
template <class It>
auto UTL::__4_::SplineCoefficient<RealType, Order>::tabulate(It first, It last)
-> coefficient_table_type {
    // use std::map to remove duplicate points, and automatically sort
    // at least size == Order + 1 (note that last is not used, so effectively 1)
    // after this call, coefficient_type[0] is filled with ys, and coefficient_type[1] is filled with x_j+1 - x_j
    // other elements are filled with nan.
    //
    constexpr value_type quiet_nan = std::numeric_limits<RealType>::quiet_NaN();
    std::map<value_type, coefficient_type> table;
    for (; first != last; ++first) {
        coefficient_type &c = table[first->first];
        c.fill(quiet_nan);
        std::get<0>(c) = first->second;
    }
    if (table.size() < Order + 1) {
        throw std::invalid_argument(__PRETTY_FUNCTION__); // at least Order + 1 are needed
    }
    // fill coefficient_type[1] with x_j+1 - x_j
    //
    auto _1 = table.begin(), _0 = _1++;
    for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
        std::get<1>(_0->second) = _1->first - _0->first;
    }
    //
    return {table.begin(), table.end()};
}
template <class RealType, long Order>
template <class It1, class It2>
auto UTL::__4_::SplineCoefficient<RealType, Order>::tabulate(It1 first1, It1 last1, It2 first2)
-> coefficient_table_type {
    // use std::map to remove duplicate points, and automatically sort
    // at least size == Order + 1 (note that last is not used, so effectively 1)
    // after this call, coefficient_type[0] is filled with ys, and coefficient_type[1] is filled with x_j+1 - x_j
    // other elements are filled with nan.
    //
    constexpr value_type quiet_nan = std::numeric_limits<RealType>::quiet_NaN();
    std::map<value_type, coefficient_type> table;
    while (first1 != last1) {
        coefficient_type &c = table[*first1++];
        c.fill(quiet_nan);
        std::get<0>(c) = *first2++;
    }
    if (table.size() < Order + 1) {
        throw std::invalid_argument(__PRETTY_FUNCTION__); // at least Order + 1 are needed
    }
    // fill coefficient_type[1] with x_j+1 - x_j
    //
    auto _1 = table.begin(), _0 = _1++;
    for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
        std::get<1>(_0->second) = _1->first - _0->first;
    }
    //
    return {table.begin(), table.end()};
}

template <class RealType, long Order>
void UTL::__4_::SplineCoefficient<RealType, Order>::init(coefficient_table_type &&table)
{
    if (table.size() < Order + 1) {
        throw std::invalid_argument(__PRETTY_FUNCTION__); // at least Order + 1 are needed
    }
    _delta = abscissa_interval(_table = std::move(table));
}

template <class RealType, long Order>
auto UTL::__4_::SplineCoefficient<RealType, Order>::abscissa_interval(coefficient_table_type const &table) noexcept
-> Optional<value_type> {
    // table is assumed to be not empty; and
    // the abscissa values are distinct and sorted in ascending order
    //

    // 1. find min and max of adjacent abscissa value differences
    //
    value_type min = std::numeric_limits<value_type>::max(), max = -std::numeric_limits<value_type>::max();
    {
        for (auto it = table.begin(), end = table.end() - 1; it != end; ++it) {
            value_type const d = (it + 1)->first - it->first;
            min = std::min(min, d);
            max = std::max(max, d);
        }
    }

    // 2. determine whether abscissa values are regularly spaced
    //
    constexpr value_type eps = static_cast<value_type>(1e-10);
    if ((max - min) < max*eps) { // regular
        return max;
    } else { // irregular
        return {};
    }
}

#endif /* UTLSplineCoefficient_hh */
