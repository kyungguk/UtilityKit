//
//  UTLDynamicArray.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/14/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLDynamicArray_hh
#define UTLDynamicArray_hh

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <stdexcept>
#include <string>

template <class T>
UTL::__4_::DynamicArray<T>::DynamicArray(size_type const sz)
{
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    DynamicArray{nullptr, vector_type(static_cast<typename vector_type::size_type>(sz))}.swap(*this);
}
template <class T>
UTL::__4_::DynamicArray<T>::DynamicArray(size_type const sz, value_type const& x)
{
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    DynamicArray{nullptr, vector_type(static_cast<typename vector_type::size_type>(sz), x)}.swap(*this);
}

template <class T>
auto UTL::__4_::DynamicArray<T>::operator=(DynamicArray const &other)
-> DynamicArray& {
    if (this != &other) {
        DynamicArray{other}.swap(*this);
    }
    return *this;
}

template <class T>
auto UTL::__4_::DynamicArray<T>::operator=(DynamicArray&& other) noexcept(noexcept(DynamicArray(std::move(other))))
-> DynamicArray& {
    if (this != &other) {
        DynamicArray{std::move(other)}.swap(*this);
    }
    return *this;
}

template <class T>
void UTL::__4_::DynamicArray<T>::reserve(size_type const new_cap)
{
    if (new_cap < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    _vec.reserve(static_cast<typename vector_type::size_type>(new_cap));
    _invalidate();
}

template <class T>
void UTL::__4_::DynamicArray<T>::resize(size_type const sz)
{
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    _vec.resize(static_cast<typename vector_type::size_type>(sz));
    _invalidate();
}
template <class T>
void UTL::__4_::DynamicArray<T>::resize(size_type const sz, value_type const &x)
{
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    _vec.resize(static_cast<typename vector_type::size_type>(sz), x);
    _invalidate();
}

template <class T>
template <class... Args>
auto UTL::__4_::DynamicArray<T>::emplace_back(Args&&... args)
-> reference {
    _vec.emplace_back(std::forward<Args>(args)...);
    _invalidate();
    return this->back();
}

template <class T>
template <class... Args>
auto UTL::__4_::DynamicArray<T>::emplace(const_iterator const pos, Args&&... args)
-> iterator {
    if ((pos < this->cbegin()) | (pos > this->cend())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - not a valid iterator");
    }
    typename vector_type::const_iterator _pos = _vec.cbegin();
    std::advance(_pos, std::distance(this->cbegin(), pos));
    typename vector_type::iterator _it = _vec.emplace(_pos, std::forward<Args>(args)...);
    _invalidate();
    iterator it = this->begin();
    std::advance(it, std::distance(_vec.begin(), _it));
    return it;
}

template <class T>
auto UTL::__4_::DynamicArray<T>::erase(const_iterator const pos1, const_iterator const pos2)
-> iterator {
    if (pos1 > pos2 || pos1 < this->cbegin() || pos2 > this->cend()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - not a valid iterator");
    }
    typename vector_type::const_iterator _pos1 = _vec.cbegin();
    std::advance(_pos1, std::distance(this->cbegin(), pos1));
    typename vector_type::const_iterator _pos2 = _vec.cbegin();
    std::advance(_pos2, std::distance(this->cbegin(), pos2));
    typename vector_type::iterator _it = _vec.erase(_pos1, _pos2);
    _invalidate();
    iterator it = this->begin();
    std::advance(it, std::distance(_vec.begin(), _it));
    return it;
}

template <class T>
template <class Container, class Predicate>
auto UTL::__4_::DynamicArray<T>::evict_if(Container& bucket, Predicate&& pred)
-> size_type {
    iterator const first = this->shuffle_if(std::forward<Predicate>(pred));
    iterator it = first, last = this->end();
    while (it != last) {
        bucket.push_back(std::move(*it++));
    }
    erase(first, last);
    return std::distance(first, last);
}

template <class T>
void UTL::__4_::DynamicArray<T>::swap(DynamicArray& other) noexcept
{
    std::swap(this->_vec, other._vec);
    this->_invalidate();
    other._invalidate();
}

// MARK: std::swap
//
template <class T>
inline void swap(UTL::__4_::DynamicArray<T> &a, UTL::__4_::DynamicArray<T> &b) noexcept(noexcept(a.swap(b))) {
    a.swap(b);
}

#endif /* UTLDynamicArray_hh */
