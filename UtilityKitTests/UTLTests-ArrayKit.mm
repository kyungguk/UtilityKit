//
//  UTLTests-ArrayKit.mm
//  UtilityKitTests
//
//  Created by KYUNGGUK MIN on 10/12/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

#define UTILITYKIT_INLINE_VERSION 4
#include <UtilityKit/ArrayKit.h>
#include <UtilityKit/NumericKit.h>
#include <UtilityKit/AuxiliaryKit.h>
#include <sstream>
#include <iostream>
#include <iterator>
#include <forward_list>
#include <random>
#include <algorithm>

@interface UTLTests_ArrayKit : XCTestCase

@end

@implementation UTLTests_ArrayKit

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

struct Object {
    long i;
    static long counter;
    ~Object() { --counter; }
private:
    explicit Object() : i(0) { ++counter; }
public:
    explicit Object(long i) : Object() { this->i = i; }
    Object(Object const& o) noexcept : Object(o.i) {}
    Object(Object&& o) noexcept : Object(o.i) { o.i = 0; }
    Object& operator=(Object const& o) noexcept { i = o.i; return *this; }
    Object& operator=(Object&& o) noexcept { i = o.i; o.i = 0; return *this; }
    operator long const() const noexcept { return i; }
    explicit operator bool const() const noexcept { return i; }
    bool operator==(long j) const noexcept { return i == j; }
    bool operator!=(long j) const noexcept { return i != j; }
};
long Object::counter{0};
std::basic_ostream<char>&
operator<<(std::basic_ostream<char>& os, Object const& a) { return os << a.i; }

struct Throw {
    static long counter;
    ~Throw() {
        --counter;
    };
    explicit Throw() {
        if (counter > 3) throw counter;
        ++counter;
    }
};
long Throw::counter{0};

- (void)testNopadArray {
    using UTL::PaddedArray;
    constexpr long pad = 0;

    try {
        PaddedArray<Throw, pad>{10};
        XCTAssert(false);
    } catch (long cnt) {
        XCTAssert(!Throw::counter, @"Throw::counter = %ld", Throw::counter);
    }

    try {
        PaddedArray<long, pad> a1(0);
        XCTAssert(!a1 && !std::move(a1).storage() && !a1 && a1.empty());
        PaddedArray<long, pad> a2(a1);
        XCTAssert(!a2);
        PaddedArray<long, pad> a3(std::move(a2));
        XCTAssert(!a3 && !a2);
        a1.slice(0, 0).swap(a2);
        XCTAssert(!a2);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        using Array = PaddedArray<Object, pad>;
        Array a1;
        XCTAssert(pad == Array::pad_size() && !a1 && a1.empty() && !a1.size() && !a1.max_size());
        XCTAssert(nullptr == a1.data());
        try {
            a1.at(0);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }
        try {
            a1 = Array{};
            XCTAssert(false);
        } catch (std::exception &e) {
        } catch (...) {
            XCTAssert(false);
        }

        constexpr long sz = 10;
        Array{sz, Array::value_type{sz}}.swap(a1);
        XCTAssert(sz == Array::value_type::counter);
        XCTAssert(!!a1 && !a1.empty() && sz == a1.size() && sz == a1.max_size());
        try {
            a1.at(-1);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }
        try {
            a1.at(sz);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }

        XCTAssert(sz == std::distance(a1.begin(), a1.end()) && sz == std::distance(a1.cbegin(), a1.cend()));
        XCTAssert(sz == std::distance(a1.rbegin(), a1.rend()) && sz == std::distance(a1.crbegin(), a1.crend()));
        XCTAssert(a1.data() == a1.begin() && a1.data() == &a1.front() && a1.end() - 1 == &a1.back());
        for (long i = -a1.pad_size(); i < sz + a1.pad_size(); ++i) {
            XCTAssert(a1[i] == sz);
            XCTAssert(a1.cbegin() + i == &a1[i]);
        }

        XCTAssert(a1.pad_size() == std::distance(a1.pad_begin(), a1.begin()) && a1.pad_size() == std::distance(a1.end(), a1.pad_end()));
        XCTAssert(a1.pad_size() == std::distance(a1.pad_cbegin(), a1.cbegin()) && a1.pad_size() == std::distance(a1.cend(), a1.pad_cend()));
        XCTAssert(a1.pad_size() == std::distance(a1.pad_rbegin(), a1.rbegin()) && a1.pad_size() == std::distance(a1.rend(), a1.pad_rend()));
        XCTAssert(a1.pad_size() == std::distance(a1.pad_crbegin(), a1.crbegin()) && a1.pad_size() == std::distance(a1.crend(), a1.pad_crend()));
        XCTAssert(a1.data() == &a1.pad_front() && a1.pad_cend() - 1 == &a1.pad_back());

        std::forward_list<Array::value_type> l1{a1.pad_cbegin(), a1.pad_cend()};
        Array{l1.cbegin(), l1.cend()}.swap(a1);
        l1.clear();
        XCTAssert(sz == Array::value_type::counter);
        Array a2{sz, a1.data()};
        constexpr long x = 3;
        a2.fill(Object{x});
        for (long i = -a1.pad_size(); i < sz + a1.pad_size(); ++i) {
            XCTAssert(&a1[i] == &a2[i] && a1[i] == x);
        }

        Array a3{sz, Object{sz}};
        a2 = a3;
        for (long i = -a1.pad_size(); i < sz + a1.pad_size(); ++i) {
            XCTAssert(a1[i] == sz && a3[i] == sz);
        }
        a2 = std::move(a3);
        for (long i = -a1.pad_size(); i < sz + a1.pad_size(); ++i) {
            XCTAssert(a1[i] == sz && !a3[i]);
        }

        Array a4(a2);
        Array a5(std::move(a2));
        XCTAssert(!a2 && a1);
        for (long i = -a1.pad_size(); i < sz + a1.pad_size(); ++i) {
            XCTAssert(a1[i] == a4[i] && &a1[i] == &a5[i]);
        }
        try {
            a2 = a1;
            XCTAssert(false);
        } catch (std::exception &) {
        } catch (...) {
            XCTAssert(false);
        }

        long loc = 0, len = 0;
        a1.slice(loc, len).swap(a2);
        XCTAssert(a2 && a2.empty());
        loc = a1.size();
        a1.slice(loc, len).swap(a2);
        XCTAssert(a2 && a2.empty());
        loc = 0;
        len = 1;
        a1.slice(loc, len).swap(a2);
        XCTAssert(a2.size() == 1 && a1.begin() == a2.begin(), @"%ld", a2.size());
        loc = a1.size() - 1;
        len = 1;
        a1.slice(loc, len).swap(a2);
        XCTAssert(a2.size() == 1 && a1.end() == a2.end(), @"%ld", a2.size());

        PaddedArray<long, 0> a6{1, 2, 3, 4, 5};
        printo(std::cout, a6, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter);
}

- (void)testPaddedArray {
    using UTL::PaddedArray;
    constexpr long pad = 2;

    try {
        PaddedArray<Throw, pad>{10};
        XCTAssert(false);
    } catch (long cnt) {
        XCTAssert(!Throw::counter, @"Throw::counter = %ld", Throw::counter);
    }

    try {
        PaddedArray<long, pad> a1(0);
        XCTAssert(a1 && a1.max_size() == 2*pad);
        PaddedArray<long, pad> a2(a1);
        XCTAssert(a2.max_size() == a1.max_size());
        PaddedArray<long, pad> a3(std::move(a2));
        XCTAssert(a3.max_size() == a1.max_size() && !a2);
        a2.slice(0, 0).swap(a2);
        XCTAssert(!a2);
        a1.slice(0, 0).swap(a2);
        XCTAssert(a2.max_size() == a1.max_size() && a1.data() == std::move(a2).storage().get() && !a2 && a2.empty());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        using Array = PaddedArray<Object, pad>;
        Array a1;
        XCTAssert(pad == Array::pad_size() && !a1 && a1.empty() && !a1.size() && !a1.max_size());
        XCTAssert(nullptr == a1.data());
        try {
            a1.at(0);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }
        try {
            a1 = Array{};
            XCTAssert(false);
        } catch (std::exception &e) {
        } catch (...) {
            XCTAssert(false);
        }

        constexpr long sz = 10;
        Array{sz, Array::value_type{sz}}.swap(a1);
        XCTAssert(sz + 2*pad == Array::value_type::counter);
        XCTAssert(!!a1 && !a1.empty() && sz == a1.size() && sz + 2*pad == a1.max_size());
        try {
            a1.at(-pad - 1);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }
        try {
            a1.at(sz + pad);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }

        XCTAssert(sz == std::distance(a1.begin(), a1.end()) && sz == std::distance(a1.cbegin(), a1.cend()));
        XCTAssert(sz == std::distance(a1.rbegin(), a1.rend()) && sz == std::distance(a1.crbegin(), a1.crend()));
        XCTAssert(a1.data() == a1.begin() - pad && a1.begin() == &a1.front() && a1.end() - 1 == &a1.back());
        for (long i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            XCTAssert(a1[i] == sz);
            XCTAssert(a1.cbegin() + i == &a1[i]);
        }

        XCTAssert(a1.pad_size() == std::distance(a1.pad_begin(), a1.begin()) && a1.pad_size() == std::distance(a1.end(), a1.pad_end()));
        XCTAssert(a1.pad_size() == std::distance(a1.pad_cbegin(), a1.cbegin()) && a1.pad_size() == std::distance(a1.cend(), a1.pad_cend()));
        XCTAssert(a1.pad_size() == std::distance(a1.pad_rbegin(), a1.rbegin()) && a1.pad_size() == std::distance(a1.rend(), a1.pad_rend()));
        XCTAssert(a1.pad_size() == std::distance(a1.pad_crbegin(), a1.crbegin()) && a1.pad_size() == std::distance(a1.crend(), a1.pad_crend()));
        XCTAssert(a1.pad_begin() == a1.data() && a1.pad_begin() == &a1.pad_front() && a1.pad_cend() - 1 == &a1.pad_back());

        std::forward_list<Array::value_type> l1{a1.pad_cbegin(), a1.pad_cend()};
        Array{l1.cbegin(), l1.cend()}.swap(a1);
        l1.clear();
        XCTAssert(a1.max_size() == Array::value_type::counter);
        Array a2{sz, a1.data()};
        constexpr long x = 3;
        a2.fill(Object{x});
        for (long i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            XCTAssert(&a1[i] == &a2[i] && a1[i] == x);
        }

        Array a3{sz, Object{sz}};
        a2 = a3;
        for (long i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            XCTAssert(a1[i] == sz && a3[i] == sz);
        }
        a2 = std::move(a3);
        for (long i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            XCTAssert(a1[i] == sz && !a3[i]);
        }
        a3 = PaddedArray<Object, 0>{a2.begin(), a2.end()};
        XCTAssert(!a3[-1] && !a3[a3.size()]);
        for (long i = 0; i < a1.size(); ++i) {
            XCTAssert(a3[i] == a1[i]);
        }

        Array a4(a2);
        Array a5(std::move(a2));
        XCTAssert(!a2 && a1);
        for (long i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            XCTAssert(a1[i] == a4[i] && &a1[i] == &a5[i]);
        }
        try {
            a2 = a1;
            XCTAssert(false);
        } catch (std::exception &) {
        } catch (...) {
            XCTAssert(false);
        }

        long loc = 0, len = 0;
        a1.slice(loc, len).swap(a2);
        XCTAssert(a2 && a2.empty() && a1.pad_begin() == a2.pad_begin());
        loc = a1.size();
        a1.slice(loc, len).swap(a2);
        XCTAssert(a2 && a2.empty() && a1.pad_end() == a2.pad_end());
        loc = 0;
        len = 1;
        a1.slice(loc, len).swap(a2);
        XCTAssert(a2.size() == 1 && a1.begin() == a2.begin(), @"%ld", a2.size());
        loc = a1.size() - 1;
        len = 1;
        a1.slice(loc, len).swap(a2);
        XCTAssert(a2.size() == 1 && a1.end() == a2.end(), @"%ld", a2.size());

        try {
            PaddedArray<long, 2> a6{1, 2, 5};
            XCTAssert(false);
        } catch (std::exception &) {
        } catch (...) {
            XCTAssert(false);
        }
        PaddedArray<long, 2> a6{1, 2, 3, 4, 5};
        printo(std::cout, a6, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter);
}

- (void)testPaddedArraySlice {
    using UTL::PaddedArray;
    using UTL::make_slice;
    constexpr long Offset = 0;
    using Array0 = PaddedArray<double, 0 + Offset>;
    using Array1 = PaddedArray<double, 1 + Offset>;
    using Array2 = PaddedArray<double, 2 + Offset>;

    std::mt19937 rng(100);
    std::uniform_real_distribution<double> dist(0, 1);

    try {
        Array1 a1(20);
        for (long i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            a1[i] = dist(rng);
        }
        {
            Array0 const a0 = make_slice<Array0::pad_size()>(a1, -1, a1.size() + 2*1);
            XCTAssert(a1.max_size() == a0.max_size() && std::equal(a0.pad_begin(), a0.pad_end(), a1.pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            Array2 const a2 = make_slice<Array2::pad_size()>(a1, +1, a1.size() - 2*1);
            XCTAssert(a1.max_size() == a2.max_size() && std::equal(a2.pad_begin(), a2.pad_end(), a1.pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
        }
        {
            Array0 const _a0 = make_slice<Array0::pad_size()>(a1, -1, a1.size() + 2*1), a0{_a0};
            XCTAssert(a1.max_size() == a0.max_size() && std::equal(a0.pad_begin(), a0.pad_end(), a1.pad_begin()));
            Array2 const _a2 = make_slice<Array2::pad_size()>(a1, +1, a1.size() - 2*1), a2{_a2};
            XCTAssert(a1.max_size() == a2.max_size() && std::equal(a2.pad_begin(), a2.pad_end(), a1.pad_begin()));
        }
        {
            Array0 const a0 = make_slice<Array0::pad_size()>(a1, a1.size()/2, 1);
            XCTAssert(a0.size() == 1);
            make_slice<Array2::pad_size()>(a1, a1.size()/2, 1) = Array0(a0.size(), 0.0);
            for (long i = -a0.pad_size(); i < a0.size() + a0.pad_size(); ++i) {
                XCTAssert(a0[i] == 0.0);
            }
            for (auto first = a1.pad_cbegin(); first != a0.pad_begin(); ++first) {
                XCTAssert(*first != 0.0);
            }
            for (auto first = a1.pad_crbegin(); first != a0.pad_rbegin(); ++first) {
                XCTAssert(*first != 0.0);
            }
        }
        {
            Array0 const a0 = make_slice<Array0::pad_size()>(a1, 1, 0);
            Array2 const a2 = make_slice<Array2::pad_size()>(a1, 1, 0);
            XCTAssert(a0.size() == 0 && a0.size() == a2.size() && a0.begin() == a2.begin() && a1.pad_begin() == a2.pad_begin());
        }
        {
            Array0 const a0 = make_slice<Array0::pad_size()>(a1, 1, 1);
            Array2 const a2 = make_slice<Array2::pad_size()>(a1, 1, 1);
            XCTAssert(a0.size() == 1 && a0.size() == a2.size() && a0.begin() == a2.begin() && a1.pad_begin() == a2.pad_begin());
        }
        {
            Array0 const a0 = make_slice<Array0::pad_size()>(a1, a1.size() - 1, 0);
            Array2 const a2 = make_slice<Array2::pad_size()>(a1, a1.size() - 1, 0);
            XCTAssert(a0.size() == 0 && a0.size() == a2.size() && a0.end() == a2.end() && a1.pad_end() == a2.pad_end());
        }
        {
            Array0 const a0 = make_slice<Array0::pad_size()>(a1, a1.size() - 2, 1);
            Array2 const a2 = make_slice<Array2::pad_size()>(a1, a1.size() - 2, 1);
            XCTAssert(a0.size() == 1 && a0.size() == a2.size() && a0.end() == a2.end() && a1.pad_end() == a2.pad_end());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
    try {
        XCTAssert(!Array0{}.slice() && !Array1{}.slice());
        Array1 a1(2);
        Array0 const a0 = make_slice<Array0::pad_size()>(a1);
        XCTAssert(a1.max_size() == a0.max_size() && std::equal(a1.pad_begin(), a1.pad_end(), a0.pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
        Array2 const a2 = make_slice<Array2::pad_size()>(a1);
        XCTAssert(a1.max_size() == a2.max_size() && std::equal(a1.pad_begin(), a1.pad_end(), a2.pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testNopadArray1D {
    using UTL::ArrayND;
    constexpr long rank = 1, pad = 0;

    try {
        using Array = ArrayND<Object, rank, pad>;

        // default construct
        Array a1;
        XCTAssert(nullptr == a1.data());
        XCTAssert(rank == a1.rank() && a1.size() == a1.dims().x && a1.max_size() == a1.max_dims().x);
        XCTAssert(a1.size() == a1.size<0>() && a1.max_size() == a1.max_size<0>());
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());
        printo(std::cout, a1, "\n");

        // zero-size array
        Array{0, Object{rank}}.swap(a1);
        XCTAssert(nullptr == a1.data());
        XCTAssert(rank == a1.rank() && a1.size() == a1.dims().x && a1.max_size() == a1.max_dims().x);
        XCTAssert(a1.size() == a1.size<0>() && a1.max_size() == a1.max_size<0>());
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());

        // empty array copy
        Array a2(a1);
        XCTAssert(nullptr == a2.data());
        XCTAssert(rank == a2.rank() && a2.size() == a2.dims().x && a2.max_size() == a2.max_dims().x);
        XCTAssert(a2.size() == a2.size<0>() && a2.max_size() == a2.max_size<0>());
        XCTAssert(!a2.is_subarray() && a2.flat_array().empty());

        // empty array move
        Array a3(std::move(a2));
        XCTAssert(nullptr == a3.data());
        XCTAssert(rank == a3.rank() && a3.size() == a3.dims().x && a3.max_size() == a3.max_dims().x);
        XCTAssert(a3.size() == a3.size<0>() && a3.max_size() == a3.max_size<0>());
        XCTAssert(!a3.is_subarray() && a3.flat_array().empty());

        // nonzero array
        constexpr Array::size_type sz{10};
        Array{sz, Object{sz}}.swap(a1);
        XCTAssert(rank == a1.rank() && a1.size() == a1.dims().x && a1.max_size() == a1.max_dims().x);
        XCTAssert(a1.size() == a1.size<0>() && a1.max_size() == a1.max_size<0>());
        XCTAssert(!a1.is_subarray() && !a1.flat_array().empty());
        for (long i = 0; i < a1.size(); ++i) {
            XCTAssert(a1[i] == sz);
            a1[i] = Object{i + 1};
            XCTAssert(a1.flat_array()[i + a1.pad_size()] == i + 1);
        }
        Array{a1}.swap(a2);
        a2.fill(Object{sz/2});
        Array{std::move(a2)}.swap(a3);
        XCTAssert(!a2 && !!a3);
        for (Object const& x : a3.flat_array()) {
            XCTAssert(x == sz/2);
        }
        a1 = a3;
        for (Object const& x : a1.flat_array()) {
            XCTAssert(x == sz/2);
        }
        a1 = std::move(a3);
        for (Object const& x : a3.flat_array()) {
            XCTAssert(x == 0L);
        }

        a1.fill(Object{0});
        Array(1000, Object{0}).swap(a1);
        for (Object const& obj : a2.flat_array()) {
            XCTAssert(obj == 0L);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter);

    // index path subscript
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A({100});
        Array const &cA = A;
        Array::size_vector_type ipath;
        for (ipath.x = -pad; ipath.x < A.size<0>(); ++ipath.x) {
            Array::value_type v;
            v = A[ipath] = arc4random();
            XCTAssert(v == A[ipath] && v == cA[ipath]);
            v = A.at(ipath) = arc4random();
            XCTAssert(v == A.at(ipath) && v == cA.at(ipath));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // leaf iterator
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A;
        XCTAssert(A.leaf_begin() == A.leaf_end() && A.leaf_cbegin() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cbegin() && A.leaf_end() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cend() && A.leaf_begin() == A.leaf_cend());
        XCTAssert(A.leaf_rbegin() == A.leaf_rend() && A.leaf_crbegin() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crbegin() && A.leaf_rend() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crend() && A.leaf_rbegin() == A.leaf_crend());

        XCTAssert(A.leaf_pad_begin() == A.leaf_pad_end() && A.leaf_pad_cbegin() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cbegin() && A.leaf_pad_end() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cend() && A.leaf_pad_begin() == A.leaf_pad_cend());
        XCTAssert(A.leaf_pad_rbegin() == A.leaf_pad_rend() && A.leaf_pad_crbegin() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crbegin() && A.leaf_pad_rend() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crend() && A.leaf_pad_rbegin() == A.leaf_pad_crend());

        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());

        Array({3}).swap(A);
        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());
        XCTAssert(A.leaf_begin() != A.leaf_end() && A.leaf_cbegin() != A.leaf_cend() &&
                  A.leaf_begin() != A.leaf_cend() && A.leaf_begin() != A.leaf_cend());
        XCTAssert(A.leaf_rbegin() != A.leaf_rend() && A.leaf_crbegin() != A.leaf_crend() &&
                  A.leaf_rbegin() != A.leaf_crend() && A.leaf_rbegin() != A.leaf_crend());

        // exclude padding
        {
            Array::const_leaf_iterator first = A.leaf_cbegin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                auto &x = A[i];
                x = arc4random();
                XCTAssert(first != last && *first++ == x);
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_begin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                auto const &x = A[i];
                *first = arc4random();
                XCTAssert(first != last && x == *first);
                ++first;
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_crbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                auto &x = A[i];
                x = arc4random();
                XCTAssert(first != last && *first++ == x);
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_rbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                auto const &x = A[i];
                *first = arc4random();
                XCTAssert(first != last && x == *first);
                ++first;
            }
            XCTAssert(first == last);
        }

        // including padding
        {
            Array::const_leaf_iterator first = A.leaf_pad_cbegin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                auto &x = A[i];
                x = arc4random();
                XCTAssert(first != last && *first++ == x);
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_pad_begin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                auto const &x = A[i];
                *first = arc4random();
                XCTAssert(first != last && x == *first);
                ++first;
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_pad_crbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                auto &x = A[i];
                x = arc4random();
                XCTAssert(first != last && *first++ == x);
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_pad_rbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                auto const &x = A[i];
                *first = arc4random();
                XCTAssert(first != last && x == *first);
                ++first;
            }
            XCTAssert(first == last);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}
- (void)testPaddedArray1D {
    using UTL::ArrayND;
    constexpr long rank = 1, pad = 2;

    try {
        using Array = ArrayND<Object, rank, pad>;

        // default construct
        Array a1;
        XCTAssert(nullptr == a1.data());
        XCTAssert(rank == a1.rank() && a1.size() == a1.dims().x && a1.max_size() == a1.max_dims().x);
        XCTAssert(a1.size() == a1.size<0>() && a1.max_size() == a1.max_size<0>());
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());
        printo(std::cout, a1, "\n");

        // zero-size array
        Array{0, Object{rank}}.swap(a1);
        XCTAssert(!!a1);
        XCTAssert(rank == a1.rank() && a1.size() == a1.dims().x && a1.max_size() == a1.max_dims().x);
        XCTAssert(a1.size() == a1.size<0>() && a1.max_size() == a1.max_size<0>());
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == a1.max_size<0>());

        // zero-size array copy
        Array a2(a1);
        XCTAssert(nullptr != a2.data());
        XCTAssert(rank == a2.rank() && a2.dims().x == a1.size() && a2.max_dims().x == a1.max_size());
        XCTAssert(a2.size() == a2.size<0>() && a2.max_size() == a2.max_size<0>());
        XCTAssert(!a2.is_subarray() && a2.flat_array().size() == a2.max_size<0>());

        // zero-size array move
        Array a3(std::move(a2));
        XCTAssert(!!a3 && !a2);
        XCTAssert(rank == a3.rank() && a3.dims().x == a1.size() && a3.max_dims().x == a1.max_size());
        XCTAssert(a3.size() == a3.size<0>() && a3.max_size() == a3.max_size<0>());
        XCTAssert(!a3.is_subarray() && a3.flat_array().size() == a3.max_size<0>());

        // nonzero array
        constexpr Array::size_type sz{10};
        Array{sz, Object{sz}}.swap(a1);
        XCTAssert(rank == a1.rank() && a1.size() == a1.dims().x && a1.max_size() == a1.max_dims().x);
        XCTAssert(a1.size() == a1.size<0>() && a1.max_size() == a1.max_size<0>());
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == a1.max_size());
        for (long i = -a1.pad_size(); i < a1.max_size()-a1.pad_size(); ++i) {
            XCTAssert(a1[i] == sz);
            a1[i] = Object{i};
            XCTAssert(a1.flat_array()[i + a1.pad_size()] == i);
        }
        Array{a1}.swap(a2);
        a2.fill(Object{sz/2});
        Array{std::move(a2)}.swap(a3);
        XCTAssert(!a2 && !!a3);
        for (Object const& x : a3.flat_array()) {
            XCTAssert(x == sz/2);
        }
        a1 = a3;
        for (Object const& x : a1.flat_array()) {
            XCTAssert(x == sz/2);
        }
        a1 = std::move(a3);
        for (Object const& x : a3.flat_array()) {
            XCTAssert(x == 0L);
        }

        a1.fill(Object{0});
        Array(1000, Object{0}).swap(a1);
        for (Object const& obj : a2.flat_array()) {
            XCTAssert(obj == 0L);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter);

    // index path subscript
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A({100});
        Array const &cA = A;
        Array::size_vector_type ipath;
        for (ipath.x = -pad; ipath.x < A.size<0>(); ++ipath.x) {
            Array::value_type v;
            v = A[ipath] = arc4random();
            XCTAssert(v == A[ipath] && v == cA[ipath]);
            v = A.at(ipath) = arc4random();
            XCTAssert(v == A.at(ipath) && v == cA.at(ipath));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // leaf iterator
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A;
        XCTAssert(A.leaf_begin() == A.leaf_end() && A.leaf_cbegin() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cbegin() && A.leaf_end() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cend() && A.leaf_begin() == A.leaf_cend());
        XCTAssert(A.leaf_rbegin() == A.leaf_rend() && A.leaf_crbegin() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crbegin() && A.leaf_rend() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crend() && A.leaf_rbegin() == A.leaf_crend());

        XCTAssert(A.leaf_pad_begin() == A.leaf_pad_end() && A.leaf_pad_cbegin() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cbegin() && A.leaf_pad_end() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cend() && A.leaf_pad_begin() == A.leaf_pad_cend());
        XCTAssert(A.leaf_pad_rbegin() == A.leaf_pad_rend() && A.leaf_pad_crbegin() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crbegin() && A.leaf_pad_rend() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crend() && A.leaf_pad_rbegin() == A.leaf_pad_crend());

        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());

        Array({3}).swap(A);
        XCTAssert(A.leaf_begin() != A.leaf_pad_begin() && A.leaf_cbegin() != A.leaf_pad_cbegin() &&
                  A.leaf_end() != A.leaf_pad_end() && A.leaf_cend() != A.leaf_pad_cend() &&
                  A.leaf_rbegin() != A.leaf_pad_rbegin() && A.leaf_crbegin() != A.leaf_pad_crbegin() &&
                  A.leaf_rend() != A.leaf_pad_rend() && A.leaf_crend() != A.leaf_pad_crend());
        XCTAssert(A.leaf_begin() != A.leaf_end() && A.leaf_cbegin() != A.leaf_cend() &&
                  A.leaf_begin() != A.leaf_cend() && A.leaf_begin() != A.leaf_cend());
        XCTAssert(A.leaf_rbegin() != A.leaf_rend() && A.leaf_crbegin() != A.leaf_crend() &&
                  A.leaf_rbegin() != A.leaf_crend() && A.leaf_rbegin() != A.leaf_crend());

        // exclude padding
        {
            Array::const_leaf_iterator first = A.leaf_cbegin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                auto &x = A[i];
                x = arc4random();
                XCTAssert(first != last && *first++ == x);
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_begin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                auto const &x = A[i];
                *first = arc4random();
                XCTAssert(first != last && x == *first);
                ++first;
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_crbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                auto &x = A[i];
                x = arc4random();
                XCTAssert(first != last && *first++ == x);
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_rbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                auto const &x = A[i];
                *first = arc4random();
                XCTAssert(first != last && x == *first);
                ++first;
            }
            XCTAssert(first == last);
        }

        // including padding
        {
            Array::const_leaf_iterator first = A.leaf_pad_cbegin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                auto &x = A[i];
                x = arc4random();
                XCTAssert(first != last && *first++ == x);
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_pad_begin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                auto const &x = A[i];
                *first = arc4random();
                XCTAssert(first != last && x == *first);
                ++first;
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_pad_crbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                auto &x = A[i];
                x = arc4random();
                XCTAssert(first != last && *first++ == x);
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_pad_rbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                auto const &x = A[i];
                *first = arc4random();
                XCTAssert(first != last && x == *first);
                ++first;
            }
            XCTAssert(first == last);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testArraySlice1DEqualPad {
    using UTL::ArrayND;
    using UTL::ArraySliceND;
    using UTL::make_slice;
    constexpr long ND = 1;

    // no pad
    try {
        constexpr long pad = 0;
        using Array = ArrayND<double, ND, pad>;
        using Slice = ArraySliceND<double, ND, pad>;

        Array a1;
        Slice s1;
        XCTAssert(!s1 && s1.empty() && a1.rank() == s1.rank() && a1.pad_size() == s1.pad_size());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cbegin() && s1.leaf_end() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend());
        XCTAssert(s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crbegin() && s1.leaf_rend() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        XCTAssert(s1.leaf_pad_begin() == s1.leaf_pad_end() && s1.leaf_pad_cbegin() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cbegin() && s1.leaf_pad_end() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cend() && s1.leaf_pad_begin() == s1.leaf_pad_cend());
        XCTAssert(s1.leaf_pad_rbegin() == s1.leaf_pad_rend() && s1.leaf_pad_crbegin() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crbegin() && s1.leaf_pad_rend() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crend() && s1.leaf_pad_rbegin() == s1.leaf_pad_crend());

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());

        Array({0}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(!s1 && s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend() &&
                  s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        Array({10}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && !s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() != s1.leaf_end() && s1.leaf_cbegin() != s1.leaf_cend() &&
                  s1.leaf_begin() != s1.leaf_cend() && s1.leaf_begin() != s1.leaf_cend() &&
                  s1.leaf_rbegin() != s1.leaf_rend() && s1.leaf_crbegin() != s1.leaf_crend() &&
                  s1.leaf_rbegin() != s1.leaf_crend() && s1.leaf_rbegin() != s1.leaf_crend());

        XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), s1.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_cbegin(), s1.leaf_cend(), a1.leaf_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_crbegin(), s1.leaf_crend(), a1.leaf_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_crbegin(), a1.leaf_crend(), s1.leaf_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        XCTAssert(std::equal(a1.leaf_pad_cbegin(), a1.leaf_pad_cend(), s1.leaf_pad_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_cbegin(), s1.leaf_pad_cend(), a1.leaf_pad_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_crbegin(), s1.leaf_pad_crend(), a1.leaf_pad_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_pad_crbegin(), a1.leaf_pad_crend(), s1.leaf_pad_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        {
            for (auto &x : a1.flat_array()) {
                x = arc4random();
            }
            Array a2{s1};
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            a2 = make_slice<Array::pad_size()>(a1);
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = a1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = s1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
        }
        {
            s1.fill(1);
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 1., @"x = %e", x);
            }
        }
        {
            Array tmp(a1.dims());
            s1 = Slice{tmp, {0}, a1.dims()};
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 0., @"x = %e", x);
            }
        }

        {
            Slice::size_vector_type loc{}, len{};
            Slice s2 = make_slice<Slice::pad_size()>(a1, loc, len);
            XCTAssert(s2 && s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            len += 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
            loc = a1.dims() - 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // with pad
    try {
        constexpr long pad = 2;
        using Array = ArrayND<double, ND, pad>;
        using Slice = ArraySliceND<double, ND, pad>;

        Array a1;
        Slice s1;
        XCTAssert(!s1 && s1.empty() && a1.rank() == s1.rank() && a1.pad_size() == s1.pad_size());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cbegin() && s1.leaf_end() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend());
        XCTAssert(s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crbegin() && s1.leaf_rend() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        XCTAssert(s1.leaf_pad_begin() == s1.leaf_pad_end() && s1.leaf_pad_cbegin() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cbegin() && s1.leaf_pad_end() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cend() && s1.leaf_pad_begin() == s1.leaf_pad_cend());
        XCTAssert(s1.leaf_pad_rbegin() == s1.leaf_pad_rend() && s1.leaf_pad_crbegin() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crbegin() && s1.leaf_pad_rend() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crend() && s1.leaf_pad_rbegin() == s1.leaf_pad_crend());

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());

        Array({0}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() != s1.leaf_pad_begin() && s1.leaf_cbegin() != s1.leaf_pad_cbegin() &&
                  s1.leaf_end() != s1.leaf_pad_end() && s1.leaf_cend() != s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() != s1.leaf_pad_rbegin() && s1.leaf_crbegin() != s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() != s1.leaf_pad_rend() && s1.leaf_crend() != s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend() &&
                  s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        Array({10}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && !s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() != s1.leaf_pad_begin() && s1.leaf_cbegin() != s1.leaf_pad_cbegin() &&
                  s1.leaf_end() != s1.leaf_pad_end() && s1.leaf_cend() != s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() != s1.leaf_pad_rbegin() && s1.leaf_crbegin() != s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() != s1.leaf_pad_rend() && s1.leaf_crend() != s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() != s1.leaf_end() && s1.leaf_cbegin() != s1.leaf_cend() &&
                  s1.leaf_begin() != s1.leaf_cend() && s1.leaf_begin() != s1.leaf_cend() &&
                  s1.leaf_rbegin() != s1.leaf_rend() && s1.leaf_crbegin() != s1.leaf_crend() &&
                  s1.leaf_rbegin() != s1.leaf_crend() && s1.leaf_rbegin() != s1.leaf_crend());

        XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), s1.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_cbegin(), s1.leaf_cend(), a1.leaf_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_crbegin(), s1.leaf_crend(), a1.leaf_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_crbegin(), a1.leaf_crend(), s1.leaf_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        XCTAssert(std::equal(a1.leaf_pad_cbegin(), a1.leaf_pad_cend(), s1.leaf_pad_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_cbegin(), s1.leaf_pad_cend(), a1.leaf_pad_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_crbegin(), s1.leaf_pad_crend(), a1.leaf_pad_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_pad_crbegin(), a1.leaf_pad_crend(), s1.leaf_pad_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        {
            for (auto &x : a1.flat_array()) {
                x = arc4random();
            }
            Array a2{s1};
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            a2 = make_slice<Array::pad_size()>(a1);
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = a1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = s1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
        }
        {
            s1.fill(1);
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 1., @"x = %e", x);
            }
        }
        {
            Array tmp(a1.dims());
            s1 = Slice{tmp, {0}, a1.dims()};
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 0., @"x = %e", x);
            }
        }

        {
            Slice::size_vector_type loc{}, len{};
            Slice s2 = make_slice<Slice::pad_size()>(a1, loc, len);
            XCTAssert(s2 && s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            len += 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
            loc = a1.dims() - 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testArraySlice1DCrossPad {
    using UTL::ArrayND;
    using UTL::ArraySliceND;
    using UTL::make_slice;
    constexpr long ND = 1, Offset = 1;
    using Array0 = ArrayND<double, ND, 0 + Offset>;
    using Array1 = ArrayND<double, ND, 1 + Offset>;
    using Array2 = ArrayND<double, ND, 2 + Offset>;
    using Slice0 = ArraySliceND<double, ND, 0 + Offset>;
    using Slice1 = ArraySliceND<double, ND, 1 + Offset>;
    using Slice2 = ArraySliceND<double, ND, 2 + Offset>;
    using SV = Array0::size_vector_type;

    std::mt19937 rng(100);
    std::uniform_real_distribution<double> dist(0, 1);

    try {
        Array1 a1(5);
        for (auto &x : a1.flat_array()) {
            x = dist(rng);
        }
        {
            Slice0 const s0 = make_slice<0 + Offset>(a1);
            Array0 const a0{s0};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s0.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a0.max_dims()));
            XCTAssert(std::equal(a1.pad_begin(), a1.pad_end(), s0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.pad_begin(), a1.pad_end(), a0.pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
            Slice2 const s2 = make_slice<2 + Offset>(a1);
            Array2 const a2{s2};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s2.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a2.max_dims()));
            XCTAssert(std::equal(a1.pad_begin(), a1.pad_end(), s2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.pad_begin(), a1.pad_end(), a2.pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
        }
        {
            Slice0 const s0 = make_slice<0 + Offset>(a1, {-1}, a1.dims() + 2*1L);
            Array0 const a0{s0};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s0.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a0.max_dims()));
            XCTAssert(std::equal(a1.pad_begin(), a1.pad_end(), s0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.pad_begin(), a1.pad_end(), a0.pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
            Slice2 const s2 = make_slice<2 + Offset>(a1, {+1}, a1.dims() - 2*1L);
            Array2 const a2{s2};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s2.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a2.max_dims()));
            XCTAssert(std::equal(a1.pad_begin(), a1.pad_end(), s2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.pad_begin(), a1.pad_end(), a2.pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, SV{1}, SV{0});
            Slice2 const a2 = make_slice<2 + Offset>(a1, SV{1}, SV{0});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{0}) && UTL::reduce_bit_and(a2.dims() == SV{0}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{0 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{0 + 2*a2.pad_size()}));
            XCTAssert(a1.pad_begin() == &*a2.leaf_pad_begin());
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, SV{1}, SV{1});
            Slice2 const a2 = make_slice<2 + Offset>(a1, SV{1}, SV{1});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{1}) && UTL::reduce_bit_and(a2.dims() == SV{1}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{1 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{1 + 2*a2.pad_size()}));
            XCTAssert(a1.pad_begin() == &*a2.leaf_pad_begin() && &*a0.leaf_begin() == &*a2.leaf_begin());
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, a1.dims() - 1L, {0});
            Slice2 const a2 = make_slice<2 + Offset>(a1, a1.dims() - 1L, {0});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{0}) && UTL::reduce_bit_and(a2.dims() == SV{0}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{0 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{0 + 2*a2.pad_size()}));
            XCTAssert(&*a1.pad_rbegin() == &*a2.leaf_pad_rbegin());
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, a1.dims() - 2L, {1});
            Slice2 const a2 = make_slice<2 + Offset>(a1, a1.dims() - 2L, {1});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{1}) && UTL::reduce_bit_and(a2.dims() == SV{1}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{1 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{1 + 2*a2.pad_size()}));
            XCTAssert(&*a1.pad_rbegin() == &*a2.leaf_pad_rbegin() && &*a0.leaf_rbegin() == &*a2.leaf_rbegin());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testNopadArray2D {
    using UTL::ArrayND;
    constexpr long rank = 2, pad = 0;

    try {
        using Array = ArrayND<Object, rank, pad>;
        Array::size_vector_type dims, max_dims;
        constexpr long sz = 10;

        // default construct
        Array a1, a2, a3;
        XCTAssert(nullptr == a1.data() && !a1);
        XCTAssert(rank == a1.rank() && 0 == a1.size<0>() && 0 == a1.max_size<0>() && 0 == a1.size<1>() && 0 == a1.max_size<1>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(0 == dims[0] && 0 == max_dims[0] && 0 == dims[1] && 0 == max_dims[1]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());
        a1.fill(Object{sz});
        printo(std::cout, a1, "\n");

        // zero first dimension empty array
        Array{{0, 1}, Object{rank}}.swap(a1);
        XCTAssert(nullptr == a1.data() && !a1);
        XCTAssert(0 == a1.size<0>() && 0 == a1.max_size<0>() && 0 == a1.size<1>() && 0 == a1.max_size<1>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(0 == dims[0] && 0 == max_dims[0] && 0 == dims[1] && 0 == max_dims[1]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());
        a1.fill(Object{sz});

        // copy
        Array{a1}.swap(a2);
        XCTAssert(nullptr == a2.data() && !a2);
        XCTAssert(0 == a2.size<0>() && 0 == a2.max_size<0>() && 0 == a2.size<1>() && 0 == a2.max_size<1>());
        dims = a2.dims();
        max_dims = a2.max_dims();
        XCTAssert(0 == dims[0] && 0 == max_dims[0] && 0 == dims[1] && 0 == max_dims[1]);
        XCTAssert(!a2.is_subarray() && a2.flat_array().empty());
        a1.fill(Object{sz});

        // move
        Array{std::move(a2)}.swap(a3);
        XCTAssert(nullptr == a3.data() && !a3);
        XCTAssert(0 == a3.size<0>() && 0 == a3.max_size<0>() && 0 == a3.size<1>() && 0 == a3.max_size<1>());
        dims = a3.dims();
        max_dims = a3.max_dims();
        XCTAssert(0 == dims[0] && 0 == max_dims[0] && 0 == dims[1] && 0 == max_dims[1]);
        XCTAssert(!a3.is_subarray() && a3.flat_array().empty());
        a1.fill(Object{sz});

        // zero second dimension empty array
        Array{{sz, 0}, Object{rank}}.swap(a1);
        XCTAssert(nullptr == a1.data() && !a1);
        XCTAssert(sz == a1.size<0>() && sz == a1.max_size<0>() && 0 == a1.size<1>() && 0 == a1.max_size<1>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(sz == dims[0] && sz == max_dims[0] && 0 == dims[1] && 0 == max_dims[1]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());
        for (long i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            XCTAssert(a1[i].is_subarray() && a1[i].flat_array().empty());
        }
        a1.fill(Object{sz});
        printo(std::cout, a1, "\n");

        // move
        Array::size_vector_type const length = a1.dims();
        Array{std::move(a1)}.swap(a3);
        XCTAssert(!a1 && a1.empty() && nullptr == a3.data() && !a3);
        XCTAssert(length.x == a3.size<0>() && length.x + 2*a3.pad_size() == a3.max_size<0>() && length.y == a3.size<1>() && length.y + 2*a3.pad_size() == a3.max_size<1>());
        dims = a3.dims();
        max_dims = a3.max_dims();
        XCTAssert(a3.size<0>() == dims[0] && a3.max_size<0>() == max_dims[0] && a3.size<1>() == dims[1] && a3.max_size<1>() == max_dims[1]);
        XCTAssert(!a3.is_subarray() && a3.flat_array().empty());
        for (long i = -a3.pad_size(); i < a3.size() + a3.pad_size(); ++i) {
            XCTAssert(a3[i].is_subarray() && a3[i].flat_array().empty());
        }
        a3.fill(Object{sz});

        // nonzero array
        dims = {3, 5};
        max_dims = dims + 2*Array::pad_size();
        Array{dims, Object{0}}.swap(a1);
        XCTAssert(nullptr != a1.data() && !!a1);
        XCTAssert(a1.size<0>() == dims.x && a1.max_size<0>() == max_dims.x && a1.size<1>() == dims.y && a1.max_size<1>() == max_dims.y);
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0] && a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == UTL::reduce_prod(max_dims));
        for (long i = 0; i < a1.flat_array().size(); ++i) {
            a1.flat_array()[i] = Object{i};
        }
        for (long idx = 0, i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            XCTAssert(a1[i].is_subarray() && a1[i].flat_array().size() == max_dims.y);
            for (long j = -Array::pad_size(); j < a1[i].size() + Array::pad_size(); ++j, ++idx) {
                XCTAssert(a1[i][j] == idx);
                XCTAssert(a1[i].flat_array()[j + Array::pad_size()] == idx);
            }
        }

        // copy
        Array{a1.dims(), Object{0}}.swap(a2);
        long count = 0;
        for (auto &x : a2.flat_array()) {
            x = Object{count++};
        }
        a1 = a2;
        for (long idx = 0, i = -a1.pad_size(); i < a2.size() + a1.pad_size(); ++i) {
            for (long j = -Array::pad_size(); j < a2[i].size() + Array::pad_size(); ++j, ++idx) {
                XCTAssert(a1[i][j] == idx, @"idx = %ld, a1[%ld][%ld] = %ld", idx, i, j, a1[i][j].i);
                XCTAssert(a1[i].flat_array()[j + Array::pad_size()] == idx, @"idx = %ld, a1[%ld][%ld] = %ld", idx, i, j, a1[i].flat_array()[j + Array::pad_size()].i);
            }
        }

        // move
        Array{std::move(a2)}.swap(a1);
        XCTAssert(nullptr != a1.data() && !!a1 && !a2);
        XCTAssert(a1.size<0>() == dims.x && a1.max_size<0>() == max_dims.x && a1.size<1>() == dims.y && a1.max_size<1>() == max_dims.y);
        XCTAssert(!a1.is_subarray() && a1.data() == a1.flat_array().begin());
        for (long idx = 0, i = -a1.pad_size(); i < a1.size() + a1.pad_size(); ++i) {
            XCTAssert(a1[i].is_subarray() && a1[i].flat_array().size() == max_dims.y);
            for (long j = -Array::pad_size(); j < a1[i].size() + Array::pad_size(); ++j, ++idx) {
                XCTAssert(a1[i][j] == idx);
                XCTAssert(a1[i].flat_array()[j + Array::pad_size()] == idx);
            }
        }

        printo(std::cout, a1, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter);

    // index path subscript
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A({100, 99});
        Array const &cA = A;
        Array::size_vector_type ipath;
        for (ipath.x = -pad; ipath.x < A.size<0>(); ++ipath.x) {
            for (ipath.y = -pad; ipath.y < A.size<1>(); ++ipath.y) {
                Array::value_type v;
                v = A[ipath] = arc4random();
                XCTAssert(v == A[ipath] && v == cA[ipath]);
                v = A.at(ipath) = arc4random();
                XCTAssert(v == A.at(ipath) && v == cA.at(ipath));
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // leaf iterator
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A;
        XCTAssert(A.leaf_begin() == A.leaf_end() && A.leaf_cbegin() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cbegin() && A.leaf_end() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cend() && A.leaf_begin() == A.leaf_cend());
        XCTAssert(A.leaf_rbegin() == A.leaf_rend() && A.leaf_crbegin() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crbegin() && A.leaf_rend() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crend() && A.leaf_rbegin() == A.leaf_crend());

        XCTAssert(A.leaf_pad_begin() == A.leaf_pad_end() && A.leaf_pad_cbegin() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cbegin() && A.leaf_pad_end() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cend() && A.leaf_pad_begin() == A.leaf_pad_cend());
        XCTAssert(A.leaf_pad_rbegin() == A.leaf_pad_rend() && A.leaf_pad_crbegin() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crbegin() && A.leaf_pad_rend() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crend() && A.leaf_pad_rbegin() == A.leaf_pad_crend());

        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());

        Array({2, 3}).swap(A);
        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());
        XCTAssert(A.leaf_begin() != A.leaf_end() && A.leaf_cbegin() != A.leaf_cend() &&
                  A.leaf_begin() != A.leaf_cend() && A.leaf_begin() != A.leaf_cend());
        XCTAssert(A.leaf_rbegin() != A.leaf_rend() && A.leaf_crbegin() != A.leaf_crend() &&
                  A.leaf_rbegin() != A.leaf_crend() && A.leaf_rbegin() != A.leaf_crend());

        // exclude padding
        {
            Array::const_leaf_iterator first = A.leaf_cbegin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                for (long j = 0; j < A.size<1>(); ++j) {
                    auto &x = A[i][j];
                    x = arc4random();
                    XCTAssert(first != last && *first++ == x);
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_begin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                for (long j = 0; j < A.size<1>(); ++j) {
                    auto const &x = A[i][j];
                    *first = arc4random();
                    XCTAssert(first != last && x == *first);
                    ++first;
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_crbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                for (long j = A.size<1>() - 1; j >= 0; --j) {
                    auto &x = A[i][j];
                    x = arc4random();
                    XCTAssert(first != last && *first++ == x);
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_rbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                for (long j = A.size<1>() - 1; j >= 0; --j) {
                    auto const &x = A[i][j];
                    *first = arc4random();
                    XCTAssert(first != last && x == *first);
                    ++first;
                }
            }
            XCTAssert(first == last);
        }

        // including padding
        {
            Array::const_leaf_iterator first = A.leaf_pad_cbegin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                for (long j = -A.pad_size(); j < A.size<1>() + A.pad_size(); ++j) {
                    auto &x = A[i][j];
                    x = arc4random();
                    XCTAssert(first != last && *first++ == x);
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_pad_begin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                for (long j = -A.pad_size(); j < A.size<1>() + A.pad_size(); ++j) {
                    auto const &x = A[i][j];
                    *first = arc4random();
                    XCTAssert(first != last && x == *first);
                    ++first;
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_pad_crbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                for (long j = A.size<1>() + A.pad_size() - 1; j >= -A.pad_size(); --j) {
                    auto &x = A[i][j];
                    x = arc4random();
                    XCTAssert(first != last && *first++ == x);
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_pad_rbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                for (long j = A.size<1>() + A.pad_size() - 1; j >= -A.pad_size(); --j) {
                    auto const &x = A[i][j];
                    *first = arc4random();
                    XCTAssert(first != last && x == *first);
                    ++first;
                }
            }
            XCTAssert(first == last);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}
- (void)testPaddedArray2D {
    using UTL::ArrayND;
    constexpr long rank = 2, pad = 2;

    try {
        using Array = ArrayND<Object, rank, pad>;
        Array::size_vector_type dims, max_dims;

        // default construct
        Array a1, a2, a3;
        XCTAssert(nullptr == a1.data() && !a1);
        XCTAssert(rank == a1.rank());
        XCTAssert(0 == a1.size<0>() && 0 == a1.max_size<0>());
        XCTAssert(0 == a1.size<1>() && 0 == a1.max_size<1>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());
        a1.fill(Object{rank});
        printo(std::cout, a1, "\n");

        // zero-size array
        Array{{0, 0}, Object{rank}}.swap(a1);
        XCTAssert(nullptr != a1.data() && !!a1);
        XCTAssert(0 == a1.size<0>() && 2*pad == a1.max_size<0>());
        XCTAssert(0 == a1.size<1>() && 2*pad == a1.max_size<1>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == UTL::reduce_prod(max_dims));
        a1.fill(Object{rank});
        printo(std::cout, a1, "\n");

        // copy
        Array{a1}.swap(a2);
        XCTAssert(nullptr != a2.data() && !!a2);
        XCTAssert(0 == a2.size<0>() && 2*pad == a2.max_size<0>());
        XCTAssert(0 == a2.size<1>() && 2*pad == a2.max_size<1>());
        dims = a2.dims();
        max_dims = a2.max_dims();
        XCTAssert(a2.size<0>() == dims[0] && a2.max_size<0>() == max_dims[0]);
        XCTAssert(a2.size<1>() == dims[1] && a2.max_size<1>() == max_dims[1]);
        XCTAssert(!a2.is_subarray() && a2.flat_array().size() == UTL::reduce_prod(max_dims));
        for (long i = -a2.pad_size(); i < a2.size() + a2.pad_size(); ++i) {
            XCTAssert(a2[i].is_subarray() && a2[i].flat_array().size() == max_dims.y);
            XCTAssert(a2[i].pad_front() == rank && a2[i].pad_back() == rank);
        }
        printo(std::cout, a2, "\n");

        // move
        Array{std::move(a1)}.swap(a3);
        XCTAssert(nullptr != a3.data() && !!a3 && !a1);
        XCTAssert(0 == a3.size<0>() && 2*pad == a3.max_size<0>());
        XCTAssert(0 == a3.size<1>() && 2*pad == a3.max_size<1>());
        dims = a3.dims();
        max_dims = a3.max_dims();
        XCTAssert(a3.size<0>() == dims[0] && a3.max_size<0>() == max_dims[0]);
        XCTAssert(a3.size<1>() == dims[1] && a3.max_size<1>() == max_dims[1]);
        XCTAssert(!a3.is_subarray() && a3.data() == a3.flat_array().begin());
        for (long i = -a3.pad_size(); i < a3.size() + a3.pad_size(); ++i) {
            XCTAssert(a3[i].is_subarray() && a3[i].flat_array().size() == max_dims.y);
            XCTAssert(a3[i].pad_front() == rank && a3[i].pad_back() == rank);
        }
        printo(std::cout, a3, "\n");

        // nonzero array
        dims = {3, 5};
        max_dims = dims + 2*Array::pad_size();
        Array{dims, Object{0}}.swap(a1);
        XCTAssert(nullptr != a1.data() && !!a1);
        XCTAssert(dims.x == a1.size<0>() && max_dims.x == a1.max_size<0>());
        XCTAssert(dims.y == a1.size<1>() && max_dims.y == a1.max_size<1>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == UTL::reduce_prod(max_dims));
        for (long i = 0; i < a1.flat_array().size(); ++i) {
            a1.flat_array()[i] = Object{i};
        }
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            XCTAssert(a1[i].is_subarray() && a1[i].flat_array().size() == max_dims.y);
            for (long j = -pad; j < a1[i].size() + pad; ++j, ++idx) {
                XCTAssert(a1[i][j] == idx);
                XCTAssert(a1[i].flat_array()[j + Array::pad_size()] == idx);
            }
        }

        // copy assign
        Array{a1.dims(), Object{0}}.swap(a2);
        long count = 0;
        for (auto &x : a2.flat_array()) {
            x = Object{count++};
        }
        a1 = a2;
        a2.fill(Object{0});
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            for (long j = -pad; j < a1[i].size() + pad; ++j, ++idx) {
                XCTAssert(a1[i][j] == idx);
                XCTAssert(a1[i].flat_array()[j + Array::pad_size()] == idx);
                XCTAssert(a2[i][j] == 0L);
                XCTAssert(a2[i].flat_array()[j + Array::pad_size()] == 0L);
            }
        }

        // move assign
        a2 = std::move(a1);
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            for (long j = -pad; j < a1[i].size() + pad; ++j, ++idx) {
                XCTAssert(a2[i][j] == idx);
                XCTAssert(a2[i].flat_array()[j + Array::pad_size()] == idx);
                XCTAssert(a1[i][j] == 0L);
                XCTAssert(a1[i].flat_array()[j + Array::pad_size()] == 0L);
            }
        }

        printo(std::cout, a1, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter, @"Object::counter = %ld", Object::counter);

    // index path subscript
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A({100, 99});
        Array const &cA = A;
        Array::size_vector_type ipath;
        for (ipath.x = -pad; ipath.x < A.size<0>(); ++ipath.x) {
            for (ipath.y = -pad; ipath.y < A.size<1>(); ++ipath.y) {
                Array::value_type v;
                v = A[ipath] = arc4random();
                XCTAssert(v == A[ipath] && v == cA[ipath]);
                v = A.at(ipath) = arc4random();
                XCTAssert(v == A.at(ipath) && v == cA.at(ipath));
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // leaf iterator
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A;
        XCTAssert(A.leaf_begin() == A.leaf_end() && A.leaf_cbegin() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cbegin() && A.leaf_end() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cend() && A.leaf_begin() == A.leaf_cend());
        XCTAssert(A.leaf_rbegin() == A.leaf_rend() && A.leaf_crbegin() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crbegin() && A.leaf_rend() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crend() && A.leaf_rbegin() == A.leaf_crend());

        XCTAssert(A.leaf_pad_begin() == A.leaf_pad_end() && A.leaf_pad_cbegin() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cbegin() && A.leaf_pad_end() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cend() && A.leaf_pad_begin() == A.leaf_pad_cend());
        XCTAssert(A.leaf_pad_rbegin() == A.leaf_pad_rend() && A.leaf_pad_crbegin() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crbegin() && A.leaf_pad_rend() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crend() && A.leaf_pad_rbegin() == A.leaf_pad_crend());

        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());

        Array({2, 3}).swap(A);
        XCTAssert(A.leaf_begin() != A.leaf_pad_begin() && A.leaf_cbegin() != A.leaf_pad_cbegin() &&
                  A.leaf_end() != A.leaf_pad_end() && A.leaf_cend() != A.leaf_pad_cend() &&
                  A.leaf_rbegin() != A.leaf_pad_rbegin() && A.leaf_crbegin() != A.leaf_pad_crbegin() &&
                  A.leaf_rend() != A.leaf_pad_rend() && A.leaf_crend() != A.leaf_pad_crend());
        XCTAssert(A.leaf_begin() != A.leaf_end() && A.leaf_cbegin() != A.leaf_cend() &&
                  A.leaf_begin() != A.leaf_cend() && A.leaf_begin() != A.leaf_cend());
        XCTAssert(A.leaf_rbegin() != A.leaf_rend() && A.leaf_crbegin() != A.leaf_crend() &&
                  A.leaf_rbegin() != A.leaf_crend() && A.leaf_rbegin() != A.leaf_crend());

        // exclude padding
        {
            Array::const_leaf_iterator first = A.leaf_cbegin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                for (long j = 0; j < A.size<1>(); ++j) {
                    auto &x = A[i][j];
                    x = arc4random();
                    XCTAssert(first != last && *first++ == x);
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_begin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                for (long j = 0; j < A.size<1>(); ++j) {
                    auto const &x = A[i][j];
                    *first = arc4random();
                    XCTAssert(first != last && x == *first);
                    ++first;
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_crbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                for (long j = A.size<1>() - 1; j >= 0; --j) {
                    auto &x = A[i][j];
                    x = arc4random();
                    XCTAssert(first != last && *first++ == x);
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_rbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                for (long j = A.size<1>() - 1; j >= 0; --j) {
                    auto const &x = A[i][j];
                    *first = arc4random();
                    XCTAssert(first != last && x == *first);
                    ++first;
                }
            }
            XCTAssert(first == last);
        }

        // including padding
        {
            Array::const_leaf_iterator first = A.leaf_pad_cbegin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                for (long j = -A.pad_size(); j < A.size<1>() + A.pad_size(); ++j) {
                    auto &x = A[i][j];
                    x = arc4random();
                    XCTAssert(first != last && *first++ == x);
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_pad_begin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                for (long j = -A.pad_size(); j < A.size<1>() + A.pad_size(); ++j) {
                    auto const &x = A[i][j];
                    *first = arc4random();
                    XCTAssert(first != last && x == *first);
                    ++first;
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_pad_crbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                for (long j = A.size<1>() + A.pad_size() - 1; j >= -A.pad_size(); --j) {
                    auto &x = A[i][j];
                    x = arc4random();
                    XCTAssert(first != last && *first++ == x);
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_pad_rbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                for (long j = A.size<1>() + A.pad_size() - 1; j >= -A.pad_size(); --j) {
                    auto const &x = A[i][j];
                    *first = arc4random();
                    XCTAssert(first != last && x == *first);
                    ++first;
                }
            }
            XCTAssert(first == last);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testArraySlice2DEqualPad {
    using UTL::ArrayND;
    using UTL::ArraySliceND;
    using UTL::make_slice;
    constexpr long ND = 2;

    // no pad
    try {
        constexpr long pad = 0;
        using Array = ArrayND<double, ND, pad>;
        using Slice = ArraySliceND<double, ND, pad>;

        Array a1;
        Slice s1;
        XCTAssert(!s1 && s1.empty() && a1.rank() == s1.rank() && a1.pad_size() == s1.pad_size());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cbegin() && s1.leaf_end() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend());
        XCTAssert(s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crbegin() && s1.leaf_rend() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        XCTAssert(s1.leaf_pad_begin() == s1.leaf_pad_end() && s1.leaf_pad_cbegin() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cbegin() && s1.leaf_pad_end() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cend() && s1.leaf_pad_begin() == s1.leaf_pad_cend());
        XCTAssert(s1.leaf_pad_rbegin() == s1.leaf_pad_rend() && s1.leaf_pad_crbegin() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crbegin() && s1.leaf_pad_rend() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crend() && s1.leaf_pad_rbegin() == s1.leaf_pad_crend());

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());

        Array({0, 0}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(!s1 && s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend() &&
                  s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        Array({3, 5}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && !s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() != s1.leaf_end() && s1.leaf_cbegin() != s1.leaf_cend() &&
                  s1.leaf_begin() != s1.leaf_cend() && s1.leaf_begin() != s1.leaf_cend() &&
                  s1.leaf_rbegin() != s1.leaf_rend() && s1.leaf_crbegin() != s1.leaf_crend() &&
                  s1.leaf_rbegin() != s1.leaf_crend() && s1.leaf_rbegin() != s1.leaf_crend());

        XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), s1.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_cbegin(), s1.leaf_cend(), a1.leaf_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_crbegin(), s1.leaf_crend(), a1.leaf_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_crbegin(), a1.leaf_crend(), s1.leaf_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        XCTAssert(std::equal(a1.leaf_pad_cbegin(), a1.leaf_pad_cend(), s1.leaf_pad_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_cbegin(), s1.leaf_pad_cend(), a1.leaf_pad_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_crbegin(), s1.leaf_pad_crend(), a1.leaf_pad_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_pad_crbegin(), a1.leaf_pad_crend(), s1.leaf_pad_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        {
            for (auto &x : a1.flat_array()) {
                x = arc4random();
            }
            Array a2{s1};
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            a2 = make_slice<Array::pad_size()>(a1);
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = a1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = s1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
        }
        {
            s1.fill(1);
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 1., @"x = %e", x);
            }
        }
        {
            Array tmp(a1.dims());
            s1 = Slice{tmp, {0, 0}, a1.dims()};
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 0., @"x = %e", x);
            }
        }

        {
            Slice::size_vector_type loc{}, len{};
            Slice s2 = make_slice<Slice::pad_size()>(a1, loc, len);
            XCTAssert(s2 && s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            len += 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
            loc = a1.dims() - 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // with pad
    try {
        constexpr long pad = 2;
        using Array = ArrayND<double, ND, pad>;
        using Slice = ArraySliceND<double, ND, pad>;

        Array a1;
        Slice s1;
        XCTAssert(!s1 && s1.empty() && a1.rank() == s1.rank() && a1.pad_size() == s1.pad_size());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cbegin() && s1.leaf_end() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend());
        XCTAssert(s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crbegin() && s1.leaf_rend() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        XCTAssert(s1.leaf_pad_begin() == s1.leaf_pad_end() && s1.leaf_pad_cbegin() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cbegin() && s1.leaf_pad_end() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cend() && s1.leaf_pad_begin() == s1.leaf_pad_cend());
        XCTAssert(s1.leaf_pad_rbegin() == s1.leaf_pad_rend() && s1.leaf_pad_crbegin() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crbegin() && s1.leaf_pad_rend() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crend() && s1.leaf_pad_rbegin() == s1.leaf_pad_crend());

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());

        Array({0, 0}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() != s1.leaf_pad_begin() && s1.leaf_cbegin() != s1.leaf_pad_cbegin() &&
                  s1.leaf_end() != s1.leaf_pad_end() && s1.leaf_cend() != s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() != s1.leaf_pad_rbegin() && s1.leaf_crbegin() != s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() != s1.leaf_pad_rend() && s1.leaf_crend() != s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend() &&
                  s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        Array({2, 4}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && !s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() != s1.leaf_pad_begin() && s1.leaf_cbegin() != s1.leaf_pad_cbegin() &&
                  s1.leaf_end() != s1.leaf_pad_end() && s1.leaf_cend() != s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() != s1.leaf_pad_rbegin() && s1.leaf_crbegin() != s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() != s1.leaf_pad_rend() && s1.leaf_crend() != s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() != s1.leaf_end() && s1.leaf_cbegin() != s1.leaf_cend() &&
                  s1.leaf_begin() != s1.leaf_cend() && s1.leaf_begin() != s1.leaf_cend() &&
                  s1.leaf_rbegin() != s1.leaf_rend() && s1.leaf_crbegin() != s1.leaf_crend() &&
                  s1.leaf_rbegin() != s1.leaf_crend() && s1.leaf_rbegin() != s1.leaf_crend());

        XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), s1.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_cbegin(), s1.leaf_cend(), a1.leaf_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_crbegin(), s1.leaf_crend(), a1.leaf_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_crbegin(), a1.leaf_crend(), s1.leaf_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        XCTAssert(std::equal(a1.leaf_pad_cbegin(), a1.leaf_pad_cend(), s1.leaf_pad_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_cbegin(), s1.leaf_pad_cend(), a1.leaf_pad_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_crbegin(), s1.leaf_pad_crend(), a1.leaf_pad_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_pad_crbegin(), a1.leaf_pad_crend(), s1.leaf_pad_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        {
            for (auto &x : a1.flat_array()) {
                x = arc4random();
            }
            Array a2{s1};
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            a2 = make_slice<Array::pad_size()>(a1);
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = a1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = s1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
        }
        {
            s1.fill(1);
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 1., @"x = %e", x);
            }
        }
        {
            Array tmp(a1.dims());
            s1 = Slice{tmp, {0, 0}, a1.dims()};
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 0., @"x = %e", x);
            }
        }

        {
            Slice::size_vector_type loc{}, len{};
            Slice s2 = make_slice<Slice::pad_size()>(a1, loc, len);
            XCTAssert(s2 && s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            len += 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
            loc = a1.dims() - 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testArraySlice2DCrossPad {
    using UTL::ArrayND;
    using UTL::ArraySliceND;
    using UTL::make_slice;
    constexpr long ND = 2, Offset = 1;
    using Array0 = ArrayND<double, ND, 0 + Offset>;
    using Array1 = ArrayND<double, ND, 1 + Offset>;
    using Array2 = ArrayND<double, ND, 2 + Offset>;
    using Slice0 = ArraySliceND<double, ND, 0 + Offset>;
    using Slice1 = ArraySliceND<double, ND, 1 + Offset>;
    using Slice2 = ArraySliceND<double, ND, 2 + Offset>;
    using SV = Array0::size_vector_type;

    std::mt19937 rng(100);
    std::uniform_real_distribution<double> dist(0, 1);

    try {
        Array1 a1({3, 5});
        for (auto &x : a1.flat_array()) {
            x = dist(rng);
        }
        {
            Slice0 const s0 = make_slice<0 + Offset>(a1);
            Array0 const a0{s0};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s0.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a0.max_dims()));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), a0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
            Slice2 const s2 = make_slice<2 + Offset>(a1);
            Array2 const a2{s2};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s2.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a2.max_dims()));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), a2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
        }
        {
            Slice0 const s0 = make_slice<0 + Offset>(a1, SV{-1}, a1.dims() + 2*1L);
            Array0 const a0{s0};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s0.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a0.max_dims()));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), a0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
            Slice2 const s2 = make_slice<2 + Offset>(a1, SV{+1}, a1.dims() - 2*1L);
            Array2 const a2{s2};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s2.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a2.max_dims()));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), a2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, SV{1}, SV{0});
            Slice2 const a2 = make_slice<2 + Offset>(a1, SV{1}, SV{0});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{0}) && UTL::reduce_bit_and(a2.dims() == SV{0}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{0 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{0 + 2*a2.pad_size()}));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, SV{1}, SV{1});
            Slice2 const a2 = make_slice<2 + Offset>(a1, SV{1}, SV{1});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{1}) && UTL::reduce_bit_and(a2.dims() == SV{1}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{1 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{1 + 2*a2.pad_size()}));
            XCTAssert(std::equal(a2.leaf_begin(), a2.leaf_end(), a0.leaf_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, a1.dims() - 1L, SV{0});
            Slice2 const a2 = make_slice<2 + Offset>(a1, a1.dims() - 1L, SV{0});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{0}) && UTL::reduce_bit_and(a2.dims() == SV{0}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{0 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{0 + 2*a2.pad_size()}));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, a1.dims() - 2L, SV{1});
            Slice2 const a2 = make_slice<2 + Offset>(a1, a1.dims() - 2L, SV{1});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{1}) && UTL::reduce_bit_and(a2.dims() == SV{1}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{1 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{1 + 2*a2.pad_size()}));
            XCTAssert(std::equal(a2.leaf_begin(), a2.leaf_end(), a0.leaf_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testNopadArray3D {
    using UTL::ArrayND;
    constexpr long rank = 3, pad = 0;

    try {
        using Array = ArrayND<Object, rank, pad>;
        Array::size_vector_type dims, max_dims;

        // default construct
        Array a1, a2, a3;
        XCTAssert(nullptr == a1.data() && !a1);
        XCTAssert(rank == a1.rank());
        XCTAssert(0 == a1.size<0>() && 0 == a1.max_size<0>());
        XCTAssert(0 == a1.size<1>() && 0 == a1.max_size<1>());
        XCTAssert(0 == a1.size<2>() && 0 == a1.max_size<2>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(a1.size<2>() == dims[2] && a1.max_size<2>() == max_dims[2]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());
        a1.fill(Object{rank});
        printo(std::cout, a1, "\n");

        // zero-size array
        Array{{0, 0, 0}, Object{rank}}.swap(a1);
        XCTAssert(nullptr == a1.data() && !a1);
        XCTAssert(0 == a1.size<0>() && 2*pad == a1.max_size<0>());
        XCTAssert(0 == a1.size<1>() && 2*pad == a1.max_size<1>());
        XCTAssert(0 == a1.size<2>() && 2*pad == a1.max_size<2>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(a1.size<2>() == dims[2] && a1.max_size<2>() == max_dims[2]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == UTL::reduce_prod(max_dims));
        a1.fill(Object{rank});
        printo(std::cout, a1, "\n");

        // copy
        Array{a1}.swap(a2);
        XCTAssert(nullptr == a2.data() && !a2);
        XCTAssert(0 == a2.size<0>() && 2*pad == a2.max_size<0>());
        XCTAssert(0 == a2.size<1>() && 2*pad == a2.max_size<1>());
        XCTAssert(0 == a2.size<2>() && 2*pad == a2.max_size<2>());
        dims = a2.dims();
        max_dims = a2.max_dims();
        XCTAssert(a2.size<0>() == dims[0] && a2.max_size<0>() == max_dims[0]);
        XCTAssert(a2.size<1>() == dims[1] && a2.max_size<1>() == max_dims[1]);
        XCTAssert(a2.size<2>() == dims[2] && a2.max_size<2>() == max_dims[2]);
        XCTAssert(!a2.is_subarray() && a2.flat_array().size() == UTL::reduce_prod(max_dims));
        for (long i = -pad; i < a2.size() + pad; ++i) {
            XCTAssert(a2[i].is_subarray() && a2[i].flat_array().size() == UTL::reduce_prod(max_dims.rest()));
            for (long j = -pad; j < a2[i].size() + pad; ++j) {
                XCTAssert(a2[i][j].is_subarray() && a2[i][j].flat_array().size() == max_dims.z);
                XCTAssert(a2[i][j].pad_front() == rank && a2[i][j].pad_back() == rank);
            }
        }
        printo(std::cout, a2, "\n");

        // move
        Array{std::move(a1)}.swap(a3);
        XCTAssert(nullptr == a3.data() && !a3 && !a1);
        XCTAssert(0 == a3.size<0>() && 2*pad == a3.max_size<0>());
        XCTAssert(0 == a3.size<1>() && 2*pad == a3.max_size<1>());
        XCTAssert(0 == a3.size<2>() && 2*pad == a3.max_size<2>());
        dims = a3.dims();
        max_dims = a3.max_dims();
        XCTAssert(a3.size<0>() == dims[0] && a3.max_size<0>() == max_dims[0]);
        XCTAssert(a3.size<1>() == dims[1] && a3.max_size<1>() == max_dims[1]);
        XCTAssert(a3.size<2>() == dims[2] && a3.max_size<2>() == max_dims[2]);
        XCTAssert(!a3.is_subarray() && a3.flat_array().empty());
        for (long i = -pad; i < a3.size() + pad; ++i) {
            XCTAssert(a3[i].is_subarray() && a3[i].flat_array().empty());
            for (long j = -pad; j < a3[i].size() + pad; ++j) {
                XCTAssert(a3[i][j].is_subarray() && a3[i][j].flat_array().size() == max_dims.z);
                XCTAssert(a3[i][j].pad_front() == rank && a3[i][j].pad_back() == rank);
            }
        }
        printo(std::cout, a3, "\n");

        // nonzero array
        dims = {3, 4, 5};
        max_dims = dims + 2*pad;
        Array{dims, Object{0}}.swap(a1);
        XCTAssert(nullptr != a1.data() && !!a1);
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(a1.size<2>() == dims[2] && a1.max_size<2>() == max_dims[2]);
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(a1.size<2>() == dims[2] && a1.max_size<2>() == max_dims[2]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == UTL::reduce_prod(max_dims));
        for (long i = 0; i < a1.flat_array().size(); ++i) {
            a1.flat_array()[i] = Object{i};
        }
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            XCTAssert(a1[i].is_subarray() && a1[i].flat_array().size() == UTL::reduce_prod(max_dims.rest()));
            for (long j = -pad; j < a1[i].size() + pad; ++j) {
                XCTAssert(a1[i][j].is_subarray() && a1[i][j].flat_array().size() == max_dims.z);
                for (long k = -pad; k < a1[i][j].size() + pad; ++k, ++idx) {
                    XCTAssert(a1[i][j][k] == idx);
                    XCTAssert(a1[i][j].flat_array()[k + pad] == idx);
                    XCTAssert(a1[i].flat_array()[(j + pad)*max_dims.z + (k + pad)] == idx);
                }
            }
        }

        // copy assign
        Array{a1.dims(), Object{0}}.swap(a2);
        long count = 0;
        for (auto &x : a2.flat_array()) {
            x = Object{count++};
        }
        a1 = a2;
        a2.fill(Object{0});
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            for (long j = -pad; j < a1[i].size() + pad; ++j) {
                for (long k = -pad; k < a1[i][j].size() + pad; ++k, ++idx) {
                    XCTAssert(a1[i][j][k] == idx);
                    XCTAssert(a1[i][j].flat_array()[k + pad] == idx);
                    XCTAssert(a2[i][j][k] == 0L);
                    XCTAssert(a2[i][j].flat_array()[k + pad] == 0L);
                }
            }
        }

        // move assign
        a2 = std::move(a1);
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            for (long j = -pad; j < a1[i].size() + pad; ++j) {
                for (long k = -pad; k < a1[i][j].size() + pad; ++k, ++idx) {
                    XCTAssert(a2[i][j][k] == idx);
                    XCTAssert(a2[i][j].flat_array()[k + pad] == idx);
                    XCTAssert(a1[i][j][k] == 0L);
                    XCTAssert(a1[i][j].flat_array()[k + pad] == 0L);
                }
            }
        }

        printo(std::cout, a2, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter, @"Object::counter = %ld", Object::counter);

    // index path subscript
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A({10, 9, 8});
        Array const &cA = A;
        Array::size_vector_type ipath;
        for (ipath.x = -pad; ipath.x < A.size<0>(); ++ipath.x) {
            for (ipath.y = -pad; ipath.y < A.size<1>(); ++ipath.y) {
                for (ipath.z = -pad; ipath.z < A.size<2>(); ++ipath.z) {
                    Array::value_type v;
                    v = A[ipath] = arc4random();
                    XCTAssert(v == A[ipath] && v == cA[ipath]);
                    v = A.at(ipath) = arc4random();
                    XCTAssert(v == A.at(ipath) && v == cA.at(ipath));
                }
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // leaf iterator
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A;
        XCTAssert(A.leaf_begin() == A.leaf_end() && A.leaf_cbegin() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cbegin() && A.leaf_end() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cend() && A.leaf_begin() == A.leaf_cend());
        XCTAssert(A.leaf_rbegin() == A.leaf_rend() && A.leaf_crbegin() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crbegin() && A.leaf_rend() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crend() && A.leaf_rbegin() == A.leaf_crend());

        XCTAssert(A.leaf_pad_begin() == A.leaf_pad_end() && A.leaf_pad_cbegin() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cbegin() && A.leaf_pad_end() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cend() && A.leaf_pad_begin() == A.leaf_pad_cend());
        XCTAssert(A.leaf_pad_rbegin() == A.leaf_pad_rend() && A.leaf_pad_crbegin() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crbegin() && A.leaf_pad_rend() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crend() && A.leaf_pad_rbegin() == A.leaf_pad_crend());

        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());

        Array({2, 3, 5}).swap(A);
        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());
        XCTAssert(A.leaf_begin() != A.leaf_end() && A.leaf_cbegin() != A.leaf_cend() &&
                  A.leaf_begin() != A.leaf_cend() && A.leaf_begin() != A.leaf_cend());
        XCTAssert(A.leaf_rbegin() != A.leaf_rend() && A.leaf_crbegin() != A.leaf_crend() &&
                  A.leaf_rbegin() != A.leaf_crend() && A.leaf_rbegin() != A.leaf_crend());

        // exclude padding
        {
            Array::const_leaf_iterator first = A.leaf_cbegin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                for (long j = 0; j < A.size<1>(); ++j) {
                    for (long k = 0; k < A.size<2>(); ++k) {
                        auto &x = A[i][j][k];
                        x = arc4random();
                        XCTAssert(first != last && *first++ == x);
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_begin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                for (long j = 0; j < A.size<1>(); ++j) {
                    for (long k = 0; k < A.size<2>(); ++k) {
                        auto const &x = A[i][j][k];
                        *first = arc4random();
                        XCTAssert(first != last && x == *first);
                        ++first;
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_crbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                for (long j = A.size<1>() - 1; j >= 0; --j) {
                    for (long k = A.size<2>() - 1; k >= 0; --k) {
                        auto &x = A[i][j][k];
                        x = arc4random();
                        XCTAssert(first != last && *first++ == x);
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_rbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                for (long j = A.size<1>() - 1; j >= 0; --j) {
                    for (long k = A.size<2>() - 1; k >= 0; --k) {
                        auto const &x = A[i][j][k];
                        *first = arc4random();
                        XCTAssert(first != last && x == *first);
                        ++first;
                    }
                }
            }
            XCTAssert(first == last);
        }

        // including padding
        {
            Array::const_leaf_iterator first = A.leaf_pad_cbegin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                for (long j = -A.pad_size(); j < A.size<1>() + A.pad_size(); ++j) {
                    for (long k = -A.pad_size(); k < A.size<2>() + A.pad_size(); ++k) {
                        auto &x = A[i][j][k];
                        x = arc4random();
                        XCTAssert(first != last && *first++ == x);
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_pad_begin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                for (long j = -A.pad_size(); j < A.size<1>() + A.pad_size(); ++j) {
                    for (long k = -A.pad_size(); k < A.size<2>() + A.pad_size(); ++k) {
                        auto const &x = A[i][j][k];
                        *first = arc4random();
                        XCTAssert(first != last && x == *first);
                        ++first;
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_pad_crbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                for (long j = A.size<1>() + A.pad_size() - 1; j >= -A.pad_size(); --j) {
                    for (long k = A.size<2>() + A.pad_size() - 1; k >= -A.pad_size(); --k) {
                        auto &x = A[i][j][k];
                        x = arc4random();
                        XCTAssert(first != last && *first++ == x);
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_pad_rbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                for (long j = A.size<1>() + A.pad_size() - 1; j >= -A.pad_size(); --j) {
                    for (long k = A.size<2>() + A.pad_size() - 1; k >= -A.pad_size(); --k) {
                        auto const &x = A[i][j][k];
                        *first = arc4random();
                        XCTAssert(first != last && x == *first);
                        ++first;
                    }
                }
            }
            XCTAssert(first == last);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}
- (void)testPaddedArray3D {
    using UTL::ArrayND;
    constexpr long rank = 3, pad = 2;

    try {
        using Array = ArrayND<Object, rank, pad>;
        Array::size_vector_type dims, max_dims;

        // default construct
        Array a1, a2, a3;
        XCTAssert(nullptr == a1.data() && !a1);
        XCTAssert(rank == a1.rank());
        XCTAssert(0 == a1.size<0>() && 0 == a1.max_size<0>());
        XCTAssert(0 == a1.size<1>() && 0 == a1.max_size<1>());
        XCTAssert(0 == a1.size<2>() && 0 == a1.max_size<2>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(a1.size<2>() == dims[2] && a1.max_size<2>() == max_dims[2]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().empty());
        a1.fill(Object{rank});
        printo(std::cout, a1, "\n");

        // zero-size array
        Array{{0, 0, 0}, Object{rank}}.swap(a1);
        XCTAssert(nullptr != a1.data() && !!a1);
        XCTAssert(0 == a1.size<0>() && 2*pad == a1.max_size<0>());
        XCTAssert(0 == a1.size<1>() && 2*pad == a1.max_size<1>());
        XCTAssert(0 == a1.size<2>() && 2*pad == a1.max_size<2>());
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(a1.size<2>() == dims[2] && a1.max_size<2>() == max_dims[2]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == UTL::reduce_prod(max_dims));
        a1.fill(Object{rank});
        printo(std::cout, a1, "\n");

        // copy
        Array{a1}.swap(a2);
        XCTAssert(nullptr != a2.data() && !!a2);
        XCTAssert(0 == a2.size<0>() && 2*pad == a2.max_size<0>());
        XCTAssert(0 == a2.size<1>() && 2*pad == a2.max_size<1>());
        XCTAssert(0 == a2.size<2>() && 2*pad == a2.max_size<2>());
        dims = a2.dims();
        max_dims = a2.max_dims();
        XCTAssert(a2.size<0>() == dims[0] && a2.max_size<0>() == max_dims[0]);
        XCTAssert(a2.size<1>() == dims[1] && a2.max_size<1>() == max_dims[1]);
        XCTAssert(a2.size<2>() == dims[2] && a2.max_size<2>() == max_dims[2]);
        XCTAssert(!a2.is_subarray() && a2.flat_array().size() == UTL::reduce_prod(max_dims));
        for (long i = -pad; i < a2.size() + pad; ++i) {
            XCTAssert(a2[i].is_subarray() && a2[i].flat_array().size() == UTL::reduce_prod(max_dims.rest()));
            for (long j = -pad; j < a2[i].size() + pad; ++j) {
                XCTAssert(a2[i][j].is_subarray() && a2[i][j].flat_array().size() == max_dims.z);
                XCTAssert(a2[i][j].pad_front() == rank && a2[i][j].pad_back() == rank);
            }
        }
        printo(std::cout, a2, "\n");

        // move
        Array{std::move(a1)}.swap(a3);
        XCTAssert(nullptr != a3.data() && !!a3 && !a1);
        XCTAssert(0 == a3.size<0>() && 2*pad == a3.max_size<0>());
        XCTAssert(0 == a3.size<1>() && 2*pad == a3.max_size<1>());
        XCTAssert(0 == a3.size<2>() && 2*pad == a3.max_size<2>());
        dims = a3.dims();
        max_dims = a3.max_dims();
        XCTAssert(a3.size<0>() == dims[0] && a3.max_size<0>() == max_dims[0]);
        XCTAssert(a3.size<1>() == dims[1] && a3.max_size<1>() == max_dims[1]);
        XCTAssert(a3.size<2>() == dims[2] && a3.max_size<2>() == max_dims[2]);
        XCTAssert(!a3.is_subarray() && a3.data() == a3.flat_array().begin());
        for (long i = -pad; i < a3.size() + pad; ++i) {
            XCTAssert(a3[i].is_subarray() && a3[i].data() == a3[i].flat_array().begin());
            for (long j = -pad; j < a3[i].size() + pad; ++j) {
                XCTAssert(a3[i][j].is_subarray() && a3[i][j].flat_array().size() == max_dims.z);
                XCTAssert(a3[i][j].pad_front() == rank && a3[i][j].pad_back() == rank);
            }
        }
        printo(std::cout, a3, "\n");

        // nonzero array
        dims = {3, 4, 5};
        max_dims = dims + 2*pad;
        Array{dims, Object{0}}.swap(a1);
        XCTAssert(nullptr != a1.data() && !!a1);
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(a1.size<2>() == dims[2] && a1.max_size<2>() == max_dims[2]);
        dims = a1.dims();
        max_dims = a1.max_dims();
        XCTAssert(a1.size<0>() == dims[0] && a1.max_size<0>() == max_dims[0]);
        XCTAssert(a1.size<1>() == dims[1] && a1.max_size<1>() == max_dims[1]);
        XCTAssert(a1.size<2>() == dims[2] && a1.max_size<2>() == max_dims[2]);
        XCTAssert(!a1.is_subarray() && a1.flat_array().size() == UTL::reduce_prod(max_dims));
        for (long i = 0; i < a1.flat_array().size(); ++i) {
            a1.flat_array()[i] = Object{i};
        }
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            XCTAssert(a1[i].is_subarray() && a1[i].flat_array().size() == UTL::reduce_prod(max_dims.rest()));
            for (long j = -pad; j < a1[i].size() + pad; ++j) {
                XCTAssert(a1[i][j].is_subarray() && a1[i][j].flat_array().size() == max_dims.z);
                for (long k = -pad; k < a1[i][j].size() + pad; ++k, ++idx) {
                    XCTAssert(a1[i][j][k] == idx);
                    XCTAssert(a1[i][j].flat_array()[k + pad] == idx);
                    XCTAssert(a1[i].flat_array()[(j + pad)*max_dims.z + (k + pad)] == idx);
                }
            }
        }

        // copy assign
        Array{a1.dims(), Object{0}}.swap(a2);
        long count = 0;
        for (auto &x : a2.flat_array()) {
            x = Object{count++};
        }
        a1 = a2;
        a2.fill(Object{0});
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            for (long j = -pad; j < a1[i].size() + pad; ++j) {
                for (long k = -pad; k < a1[i][j].size() + pad; ++k, ++idx) {
                    XCTAssert(a1[i][j][k] == idx);
                    XCTAssert(a1[i][j].flat_array()[k + pad] == idx);
                    XCTAssert(a2[i][j][k] == 0L);
                    XCTAssert(a2[i][j].flat_array()[k + pad] == 0L);
                }
            }
        }

        // move assign
        a2 = std::move(a1);
        for (long idx = 0, i = -pad; i < a1.size() + pad; ++i) {
            for (long j = -pad; j < a1[i].size() + pad; ++j) {
                for (long k = -pad; k < a1[i][j].size() + pad; ++k, ++idx) {
                    XCTAssert(a2[i][j][k] == idx);
                    XCTAssert(a2[i][j].flat_array()[k + pad] == idx);
                    XCTAssert(a1[i][j][k] == 0L);
                    XCTAssert(a1[i][j].flat_array()[k + pad] == 0L);
                }
            }
        }

        printo(std::cout, a2, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter, @"Object::counter = %ld", Object::counter);

    // index path subscript
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A({10, 9, 8});
        Array const &cA = A;
        Array::size_vector_type ipath;
        for (ipath.x = -pad; ipath.x < A.size<0>(); ++ipath.x) {
            for (ipath.y = -pad; ipath.y < A.size<1>(); ++ipath.y) {
                for (ipath.z = -pad; ipath.z < A.size<2>(); ++ipath.z) {
                    Array::value_type v;
                    v = A[ipath] = arc4random();
                    XCTAssert(v == A[ipath] && v == cA[ipath]);
                    v = A.at(ipath) = arc4random();
                    XCTAssert(v == A.at(ipath) && v == cA.at(ipath));
                }
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // leaf iterator
    try {
        using Array = ArrayND<double, rank, pad>;
        Array A;
        XCTAssert(A.leaf_begin() == A.leaf_end() && A.leaf_cbegin() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cbegin() && A.leaf_end() == A.leaf_cend() &&
                  A.leaf_begin() == A.leaf_cend() && A.leaf_begin() == A.leaf_cend());
        XCTAssert(A.leaf_rbegin() == A.leaf_rend() && A.leaf_crbegin() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crbegin() && A.leaf_rend() == A.leaf_crend() &&
                  A.leaf_rbegin() == A.leaf_crend() && A.leaf_rbegin() == A.leaf_crend());

        XCTAssert(A.leaf_pad_begin() == A.leaf_pad_end() && A.leaf_pad_cbegin() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cbegin() && A.leaf_pad_end() == A.leaf_pad_cend() &&
                  A.leaf_pad_begin() == A.leaf_pad_cend() && A.leaf_pad_begin() == A.leaf_pad_cend());
        XCTAssert(A.leaf_pad_rbegin() == A.leaf_pad_rend() && A.leaf_pad_crbegin() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crbegin() && A.leaf_pad_rend() == A.leaf_pad_crend() &&
                  A.leaf_pad_rbegin() == A.leaf_pad_crend() && A.leaf_pad_rbegin() == A.leaf_pad_crend());

        XCTAssert(A.leaf_begin() == A.leaf_pad_begin() && A.leaf_cbegin() == A.leaf_pad_cbegin() &&
                  A.leaf_end() == A.leaf_pad_end() && A.leaf_cend() == A.leaf_pad_cend() &&
                  A.leaf_rbegin() == A.leaf_pad_rbegin() && A.leaf_crbegin() == A.leaf_pad_crbegin() &&
                  A.leaf_rend() == A.leaf_pad_rend() && A.leaf_crend() == A.leaf_pad_crend());

        Array({2, 3, 5}).swap(A);
        XCTAssert(A.leaf_begin() != A.leaf_pad_begin() && A.leaf_cbegin() != A.leaf_pad_cbegin() &&
                  A.leaf_end() != A.leaf_pad_end() && A.leaf_cend() != A.leaf_pad_cend() &&
                  A.leaf_rbegin() != A.leaf_pad_rbegin() && A.leaf_crbegin() != A.leaf_pad_crbegin() &&
                  A.leaf_rend() != A.leaf_pad_rend() && A.leaf_crend() != A.leaf_pad_crend());
        XCTAssert(A.leaf_begin() != A.leaf_end() && A.leaf_cbegin() != A.leaf_cend() &&
                  A.leaf_begin() != A.leaf_cend() && A.leaf_begin() != A.leaf_cend());
        XCTAssert(A.leaf_rbegin() != A.leaf_rend() && A.leaf_crbegin() != A.leaf_crend() &&
                  A.leaf_rbegin() != A.leaf_crend() && A.leaf_rbegin() != A.leaf_crend());

        // exclude padding
        {
            Array::const_leaf_iterator first = A.leaf_cbegin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                for (long j = 0; j < A.size<1>(); ++j) {
                    for (long k = 0; k < A.size<2>(); ++k) {
                        auto &x = A[i][j][k];
                        x = arc4random();
                        XCTAssert(first != last && *first++ == x);
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_begin(), last = A.leaf_end();
            for (long i = 0; i < A.size<0>(); ++i) {
                for (long j = 0; j < A.size<1>(); ++j) {
                    for (long k = 0; k < A.size<2>(); ++k) {
                        auto const &x = A[i][j][k];
                        *first = arc4random();
                        XCTAssert(first != last && x == *first);
                        ++first;
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_crbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                for (long j = A.size<1>() - 1; j >= 0; --j) {
                    for (long k = A.size<2>() - 1; k >= 0; --k) {
                        auto &x = A[i][j][k];
                        x = arc4random();
                        XCTAssert(first != last && *first++ == x);
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_rbegin(), last = A.leaf_rend();
            for (long i = A.size<0>() - 1; i >= 0; --i) {
                for (long j = A.size<1>() - 1; j >= 0; --j) {
                    for (long k = A.size<2>() - 1; k >= 0; --k) {
                        auto const &x = A[i][j][k];
                        *first = arc4random();
                        XCTAssert(first != last && x == *first);
                        ++first;
                    }
                }
            }
            XCTAssert(first == last);
        }

        // including padding
        {
            Array::const_leaf_iterator first = A.leaf_pad_cbegin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                for (long j = -A.pad_size(); j < A.size<1>() + A.pad_size(); ++j) {
                    for (long k = -A.pad_size(); k < A.size<2>() + A.pad_size(); ++k) {
                        auto &x = A[i][j][k];
                        x = arc4random();
                        XCTAssert(first != last && *first++ == x);
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::leaf_iterator first = A.leaf_pad_begin(), last = A.leaf_pad_end();
            for (long i = -A.pad_size(); i < A.size<0>() + A.pad_size(); ++i) {
                for (long j = -A.pad_size(); j < A.size<1>() + A.pad_size(); ++j) {
                    for (long k = -A.pad_size(); k < A.size<2>() + A.pad_size(); ++k) {
                        auto const &x = A[i][j][k];
                        *first = arc4random();
                        XCTAssert(first != last && x == *first);
                        ++first;
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::const_reverse_leaf_iterator first = A.leaf_pad_crbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                for (long j = A.size<1>() + A.pad_size() - 1; j >= -A.pad_size(); --j) {
                    for (long k = A.size<2>() + A.pad_size() - 1; k >= -A.pad_size(); --k) {
                        auto &x = A[i][j][k];
                        x = arc4random();
                        XCTAssert(first != last && *first++ == x);
                    }
                }
            }
            XCTAssert(first == last);
        }
        {
            Array::reverse_leaf_iterator first = A.leaf_pad_rbegin(), last = A.leaf_pad_rend();
            for (long i = A.size<0>() + A.pad_size() - 1; i >= -A.pad_size(); --i) {
                for (long j = A.size<1>() + A.pad_size() - 1; j >= -A.pad_size(); --j) {
                    for (long k = A.size<2>() + A.pad_size() - 1; k >= -A.pad_size(); --k) {
                        auto const &x = A[i][j][k];
                        *first = arc4random();
                        XCTAssert(first != last && x == *first);
                        ++first;
                    }
                }
            }
            XCTAssert(first == last);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testArraySlice3DEqualPad {
    using UTL::ArrayND;
    using UTL::ArraySliceND;
    using UTL::make_slice;
    constexpr long ND = 3;

    // no pad
    try {
        constexpr long pad = 0;
        using Array = ArrayND<double, ND, pad>;
        using Slice = ArraySliceND<double, ND, pad>;

        Array a1;
        Slice s1;
        XCTAssert(!s1 && s1.empty() && a1.rank() == s1.rank() && a1.pad_size() == s1.pad_size());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cbegin() && s1.leaf_end() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend());
        XCTAssert(s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crbegin() && s1.leaf_rend() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        XCTAssert(s1.leaf_pad_begin() == s1.leaf_pad_end() && s1.leaf_pad_cbegin() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cbegin() && s1.leaf_pad_end() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cend() && s1.leaf_pad_begin() == s1.leaf_pad_cend());
        XCTAssert(s1.leaf_pad_rbegin() == s1.leaf_pad_rend() && s1.leaf_pad_crbegin() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crbegin() && s1.leaf_pad_rend() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crend() && s1.leaf_pad_rbegin() == s1.leaf_pad_crend());

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());

        Array({0, 0, 0}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(!s1 && s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend() &&
                  s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        Array({3, 4, 5}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && !s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() != s1.leaf_end() && s1.leaf_cbegin() != s1.leaf_cend() &&
                  s1.leaf_begin() != s1.leaf_cend() && s1.leaf_begin() != s1.leaf_cend() &&
                  s1.leaf_rbegin() != s1.leaf_rend() && s1.leaf_crbegin() != s1.leaf_crend() &&
                  s1.leaf_rbegin() != s1.leaf_crend() && s1.leaf_rbegin() != s1.leaf_crend());

        XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), s1.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_cbegin(), s1.leaf_cend(), a1.leaf_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_crbegin(), s1.leaf_crend(), a1.leaf_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_crbegin(), a1.leaf_crend(), s1.leaf_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        XCTAssert(std::equal(a1.leaf_pad_cbegin(), a1.leaf_pad_cend(), s1.leaf_pad_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_cbegin(), s1.leaf_pad_cend(), a1.leaf_pad_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_crbegin(), s1.leaf_pad_crend(), a1.leaf_pad_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_pad_crbegin(), a1.leaf_pad_crend(), s1.leaf_pad_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        {
            for (auto &x : a1.flat_array()) {
                x = arc4random();
            }
            Array a2{s1};
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            a2 = make_slice<Array::pad_size()>(a1);
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = a1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = s1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
        }
        {
            s1.fill(1);
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 1., @"x = %e", x);
            }
        }
        {
            Array tmp(a1.dims());
            s1 = Slice{tmp, {0, 0, 0}, a1.dims()};
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 0., @"x = %e", x);
            }
        }

        {
            Slice::size_vector_type loc{}, len{};
            Slice s2 = make_slice<Slice::pad_size()>(a1, loc, len);
            XCTAssert(s2 && s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            len += 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
            loc = a1.dims() - 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // with pad
    try {
        constexpr long pad = 2;
        using Array = ArrayND<double, ND, pad>;
        using Slice = ArraySliceND<double, ND, pad>;

        Array a1;
        Slice s1;
        XCTAssert(!s1 && s1.empty() && a1.rank() == s1.rank() && a1.pad_size() == s1.pad_size());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cbegin() && s1.leaf_end() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend());
        XCTAssert(s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crbegin() && s1.leaf_rend() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        XCTAssert(s1.leaf_pad_begin() == s1.leaf_pad_end() && s1.leaf_pad_cbegin() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cbegin() && s1.leaf_pad_end() == s1.leaf_pad_cend() &&
                  s1.leaf_pad_begin() == s1.leaf_pad_cend() && s1.leaf_pad_begin() == s1.leaf_pad_cend());
        XCTAssert(s1.leaf_pad_rbegin() == s1.leaf_pad_rend() && s1.leaf_pad_crbegin() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crbegin() && s1.leaf_pad_rend() == s1.leaf_pad_crend() &&
                  s1.leaf_pad_rbegin() == s1.leaf_pad_crend() && s1.leaf_pad_rbegin() == s1.leaf_pad_crend());

        XCTAssert(s1.leaf_begin() == s1.leaf_pad_begin() && s1.leaf_cbegin() == s1.leaf_pad_cbegin() &&
                  s1.leaf_end() == s1.leaf_pad_end() && s1.leaf_cend() == s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() == s1.leaf_pad_rbegin() && s1.leaf_crbegin() == s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() == s1.leaf_pad_rend() && s1.leaf_crend() == s1.leaf_pad_crend());

        Array({0, 0, 0}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() != s1.leaf_pad_begin() && s1.leaf_cbegin() != s1.leaf_pad_cbegin() &&
                  s1.leaf_end() != s1.leaf_pad_end() && s1.leaf_cend() != s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() != s1.leaf_pad_rbegin() && s1.leaf_crbegin() != s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() != s1.leaf_pad_rend() && s1.leaf_crend() != s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() == s1.leaf_end() && s1.leaf_cbegin() == s1.leaf_cend() &&
                  s1.leaf_begin() == s1.leaf_cend() && s1.leaf_begin() == s1.leaf_cend() &&
                  s1.leaf_rbegin() == s1.leaf_rend() && s1.leaf_crbegin() == s1.leaf_crend() &&
                  s1.leaf_rbegin() == s1.leaf_crend() && s1.leaf_rbegin() == s1.leaf_crend());

        Array({2, 4, 3}).swap(a1);
        make_slice<Array::pad_size()>(a1).swap(s1); // Slice{a1}.swap(s1);
        XCTAssert(s1 && !s1.empty());
        XCTAssert(UTL::reduce_bit_and(s1.dims() == a1.dims()) && UTL::reduce_bit_and(s1.max_dims() == a1.max_dims()));
        XCTAssert(s1.size<0>() == a1.size<0>() && s1.max_size<0>() == a1.max_size<0>());
        XCTAssert(s1.size() == a1.size() && s1.max_size() == a1.max_size());
        println(std::cout, s1);

        XCTAssert(s1.leaf_begin() != s1.leaf_pad_begin() && s1.leaf_cbegin() != s1.leaf_pad_cbegin() &&
                  s1.leaf_end() != s1.leaf_pad_end() && s1.leaf_cend() != s1.leaf_pad_cend() &&
                  s1.leaf_rbegin() != s1.leaf_pad_rbegin() && s1.leaf_crbegin() != s1.leaf_pad_crbegin() &&
                  s1.leaf_rend() != s1.leaf_pad_rend() && s1.leaf_crend() != s1.leaf_pad_crend());
        XCTAssert(s1.leaf_begin() != s1.leaf_end() && s1.leaf_cbegin() != s1.leaf_cend() &&
                  s1.leaf_begin() != s1.leaf_cend() && s1.leaf_begin() != s1.leaf_cend() &&
                  s1.leaf_rbegin() != s1.leaf_rend() && s1.leaf_crbegin() != s1.leaf_crend() &&
                  s1.leaf_rbegin() != s1.leaf_crend() && s1.leaf_rbegin() != s1.leaf_crend());

        XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), s1.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_cbegin(), s1.leaf_cend(), a1.leaf_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_crbegin(), s1.leaf_crend(), a1.leaf_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_crbegin(), a1.leaf_crend(), s1.leaf_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        XCTAssert(std::equal(a1.leaf_pad_cbegin(), a1.leaf_pad_cend(), s1.leaf_pad_begin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_cbegin(), s1.leaf_pad_cend(), a1.leaf_pad_cbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(s1.leaf_pad_crbegin(), s1.leaf_pad_crend(), a1.leaf_pad_rbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));
        XCTAssert(std::equal(a1.leaf_pad_crbegin(), a1.leaf_pad_crend(), s1.leaf_pad_crbegin(), [](Array::value_type const &a, Array::value_type const &b) { return &a == &b; }));

        {
            for (auto &x : a1.flat_array()) {
                x = arc4random();
            }
            Array a2{s1};
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            a2 = make_slice<Array::pad_size()>(a1);
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = a1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
            a2.fill(0);
            make_slice<Array::pad_size()>(a2) = s1;
            XCTAssert(std::equal(a1.leaf_cbegin(), a1.leaf_cend(), a2.leaf_begin(), [](Array::value_type const &a, Array::value_type const &b) { return a == b; }));
        }
        {
            s1.fill(1);
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 1., @"x = %e", x);
            }
        }
        {
            Array tmp(a1.dims());
            s1 = Slice{tmp, {0, 0, 0}, a1.dims()};
            for (auto const &x : a1.flat_array()) {
                XCTAssert(x == 0., @"x = %e", x);
            }
        }

        {
            Slice::size_vector_type loc{}, len{};
            Slice s2 = make_slice<Slice::pad_size()>(a1, loc, len);
            XCTAssert(s2 && s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            len += 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
            loc = a1.dims() - 1L;
            make_slice<Slice::pad_size()>(a1, loc, len).swap(s2);
            XCTAssert(s2 && !s2.empty() && UTL::reduce_bit_and(s2.dims() == len));
            XCTAssert(&s1[loc] == &*s2.leaf_begin());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testArraySlice3DCrossPad {
    using UTL::ArrayND;
    using UTL::ArraySliceND;
    using UTL::make_slice;
    constexpr long ND = 3, Offset = 0;
    using Array0 = ArrayND<double, ND, 0 + Offset>;
    using Array1 = ArrayND<double, ND, 1 + Offset>;
    using Array2 = ArrayND<double, ND, 2 + Offset>;
    using Slice0 = ArraySliceND<double, ND, 0 + Offset>;
    using Slice1 = ArraySliceND<double, ND, 1 + Offset>;
    using Slice2 = ArraySliceND<double, ND, 2 + Offset>;
    using SV = Array0::size_vector_type;

    std::mt19937 rng(100);
    std::uniform_real_distribution<double> dist(0, 1);

    try {
        Array1 a1({3, 4, 5});
        for (auto &x : a1.flat_array()) {
            x = dist(rng);
        }
        {
            Slice0 const s0 = make_slice<0 + Offset>(a1);
            Array0 const a0{s0};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s0.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a0.max_dims()));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), a0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
            Slice2 const s2 = make_slice<2 + Offset>(a1);
            Array2 const a2{s2};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s2.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a2.max_dims()));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), a2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
        }
        {
            Slice0 const s0 = make_slice<0 + Offset>(a1, SV{-1}, a1.dims() + 2*1L);
            Array0 const a0{s0};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s0.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a0.max_dims()));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), a0.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
            Slice2 const s2 = make_slice<2 + Offset>(a1, SV{+1}, a1.dims() - 2*1L);
            Array2 const a2{s2};
            XCTAssert(UTL::reduce_bit_and(a1.max_dims() == s2.max_dims()) && UTL::reduce_bit_and(a1.max_dims() == a2.max_dims()));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), a2.leaf_pad_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return a == b; }));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, SV{1}, SV{0});
            Slice2 const a2 = make_slice<2 + Offset>(a1, SV{1}, SV{0});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{0}) && UTL::reduce_bit_and(a2.dims() == SV{0}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{0 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{0 + 2*a2.pad_size()}));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, SV{1}, SV{1});
            Slice2 const a2 = make_slice<2 + Offset>(a1, SV{1}, SV{1});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{1}) && UTL::reduce_bit_and(a2.dims() == SV{1}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{1 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{1 + 2*a2.pad_size()}));
            XCTAssert(std::equal(a2.leaf_begin(), a2.leaf_end(), a0.leaf_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, a1.dims() - 1L, SV{0});
            Slice2 const a2 = make_slice<2 + Offset>(a1, a1.dims() - 1L, SV{0});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{0}) && UTL::reduce_bit_and(a2.dims() == SV{0}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{0 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{0 + 2*a2.pad_size()}));
        }
        {
            Slice0 const a0 = make_slice<0 + Offset>(a1, a1.dims() - 2L, SV{1});
            Slice2 const a2 = make_slice<2 + Offset>(a1, a1.dims() - 2L, SV{1});
            XCTAssert(UTL::reduce_bit_and(a0.dims() == SV{1}) && UTL::reduce_bit_and(a2.dims() == SV{1}));
            XCTAssert(UTL::reduce_bit_and(a0.max_dims() == SV{1 + 2*a0.pad_size()}) && UTL::reduce_bit_and(a2.max_dims() == SV{1 + 2*a2.pad_size()}));
            XCTAssert(std::equal(a2.leaf_begin(), a2.leaf_end(), a0.leaf_begin(), [](Array0::value_type const &a, Array0::value_type const &b) { return &a == &b; }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testDynamicArray {
    using UTL::DynamicArray;

    try {
        DynamicArray<Throw>{10};
        XCTAssert(false);
    } catch (long cnt) {
        XCTAssert(!Throw::counter, @"Throw::counter = %ld", Throw::counter);
    }

    try {
        DynamicArray<long> a1(0);
        XCTAssert(a1.empty());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    try {
        using Array = DynamicArray<Object>;
        Array a1;
        XCTAssert(a1.empty() && !a1.size());
        XCTAssert(nullptr == a1.data());
        try {
            a1.at(0);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }

        constexpr long sz = 10;
        Array{sz, Array::value_type{sz}}.swap(a1);
        XCTAssert(sz == Array::value_type::counter);
        XCTAssert(!a1.empty() && sz == a1.size());
        try {
            a1.at(-1);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }
        try {
            a1.at(sz);
            XCTAssert(false);
        } catch (std::out_of_range &e) {
        } catch (...) {
            XCTAssert(false);
        }

        XCTAssert(sz == std::distance(a1.begin(), a1.end()) && sz == std::distance(a1.cbegin(), a1.cend()));
        XCTAssert(sz == std::distance(a1.rbegin(), a1.rend()) && sz == std::distance(a1.crbegin(), a1.crend()));
        XCTAssert(a1.data() == a1.begin() && a1.data() == &a1.front() && a1.end() - 1 == &a1.back());
        for (long i = 0; i < sz; ++i) {
            XCTAssert(a1[i] == sz);
            XCTAssert(a1.cbegin() + i == &a1[i]);
        }

        std::forward_list<Array::value_type> l1{a1.cbegin(), a1.cend()};
        Array{l1.cbegin(), l1.cend()}.swap(a1);
        l1.clear();
        XCTAssert(sz == Array::value_type::counter);
        for (long i = 0; i < sz; ++i) {
            XCTAssert(a1[i] == sz);
            XCTAssert(a1.cbegin() + i == &a1[i]);
        }

        Array a3{sz, Object{sz}};
        a1 = a3;
        for (long i = 0; i < sz; ++i) {
            XCTAssert(a1[i] == sz && a3[i] == sz);
        }
        a1 = std::move(a3);
        XCTAssert(a3.empty());
        for (long i = 0; i < sz; ++i) {
            XCTAssert(a1[i] == sz);
        }

        a1.clear();
        XCTAssert(a1.empty());
        a1.push_back(Object{sz});
        a1.reserve(100000);
        a1.shrink_to_fit();
        XCTAssert(1 == a1.size() && a1.front() == sz);
        a1.resize(sz, Object{sz});
        XCTAssert(sz == a1.size());
        for (Object const& x : static_cast<std::vector<Array::value_type>>(a1)) {
            XCTAssert(x == sz);
        }

        a1.insert(a1.end(), Object{1});
        XCTAssert(sz + 1 == a1.size() && a1.back() == 1L);
        a1.pop_back();
        XCTAssert(sz == a1.size());
        a1.erase(a1.begin());
        XCTAssert(sz - 1 == a1.size());
        a1.erase(a1.cbegin(), a1.cend());
        a1.shrink_to_fit();
        XCTAssert(a1.empty());

        DynamicArray<long> a6{1, 2, 3, 4, 5};
        a6 = {1, 2, 3, 4, 5};
        printo(std::cout, a6, "\n");

        a1.clear();
        Array::iterator it = a1.shuffle_if([](Object const& o)->bool { return true; });
        XCTAssert(it == a1.end() && it == a1.begin());
        for (long i = 1; i <= 10; ++i) {
            a1.emplace_back(i);
        }
        it = a1.shuffle_if([](Object const& o)->bool { return o.i > 10; });
        XCTAssert(it == a1.end());
        it = a1.shuffle_if([](Object const& o)->bool { return o.i > 4; });
        XCTAssert(it == a1.begin() + 4);
        a3.clear();
        long const n = a1.evict_if(a3, [](Object const& o)->bool { return o.i == 4 || o.i == 10; });
        XCTAssert(2 == n && a1.size() == 8 && a3.size() == 2);
        for (Object const& o : a3) {
            XCTAssert(o == 4L || o == 10L);
        }
        for (Object const& o : a1) {
            XCTAssert(o != 4L && o != 10L);
        }
        printo(std::cout, "a1 = ", a1, ", a3 = ", a3, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
    XCTAssert(0 == Object::counter);
}

- (void)testStaticArray {
    using UTL::StaticArray;
    constexpr long MaxSize = 8;

    // unwind test
    try {
        StaticArray<Throw, MaxSize>{MaxSize};
        XCTAssert(false);
        return;
    } catch (long cnt) {
        XCTAssert(!Throw::counter, @"Throw::counter = %ld", Throw::counter);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // size limit test
    try {
        StaticArray<long, MaxSize>(MaxSize + 1);
        XCTAssert(false);
        return;
    } catch (std::exception &e) {
    }
    try {
        StaticArray<long, MaxSize>(MaxSize + 1, 3);
        XCTAssert(false);
        return;
    } catch (std::exception &e) {
    }
    try {
        StaticArray<long, MaxSize>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        XCTAssert(false);
        return;
    } catch (std::exception &e) {
    }

    try {
        using Array = StaticArray<Object, MaxSize>;
        Array a1;
        XCTAssert(!!a1 && a1.empty() && !a1.size() && a1.max_size() == MaxSize && a1.capacity() == MaxSize);
        XCTAssert(nullptr != a1.data() && 0 == Object::counter);

        Array{MaxSize - 1, Object{MaxSize}}.swap(a1);
        XCTAssert(!a1.empty() && a1.size() == a1.max_size() - 1 && a1.size() == Object::counter);
        for (Object const& x : a1) {
            XCTAssert(x == MaxSize);
        }
        Array::pointer const data = a1.data();
        a1.push_back(Object{1});
        XCTAssert(data == a1.data() && a1.size() == a1.max_size() && a1.back() == 1L && a1.size() == Object::counter);
        try {
            a1.push_back(Object{1});
            XCTAssert(false);
            return;
        } catch (std::exception &e) {
            XCTAssert(a1.size() == Object::counter);
        }
        a1.clear();
        XCTAssert(data == a1.data() && a1.empty() && a1.size() == Object::counter);
        a1.resize(MaxSize, Object{1});
        XCTAssert(data == a1.data() && a1.size() == MaxSize && a1.size() == Object::counter);
        for (Object const& x : a1) {
            XCTAssert(x == 1L);
        }
        a1.pop_back();
        XCTAssert(a1.size() == a1.max_size() - 1 && a1.size() == Object::counter);
        a1.resize(0, Object{1});
        a1.pop_back();
        XCTAssert(data == a1.data() && a1.empty() && 0 == Object::counter);
        a1.resize(MaxSize, Object{1});

        Array a2(std::move(a1));
        XCTAssert(!a1 && a1.empty() && data == a2.data() && a2.size() == MaxSize && a2.size() == Object::counter);
        a1 = a2;
        XCTAssert(!!a1 && a2.size() == MaxSize);
        for (Object const& x : a1) {
            XCTAssert(x == 1L);
        }

        Array::pointer data2 = a2.erase(data + 1);
        XCTAssert(a2.data() + 1 == data2 && a2.size() + 1 == a1.size());
        data2 = a2.erase(a2.begin(), a2.end());
        XCTAssert(a2.data() == data2 && a2.empty());
        data2 = a2.insert(a2.begin(), Object{MaxSize});
        XCTAssert(*data2 == MaxSize);
        a1 = a2;
        a2.erase(a2.end(), a2.end());
        XCTAssert(a1.size() == a2.size());
        for (long i = 0; i < a1.size(); ++i) {
            XCTAssert(a1[i] == a2[i]);
        }

        a1.clear();
        Array::iterator it = a1.shuffle_if([](Object const& o)->bool { return true; });
        XCTAssert(it == a1.end() && it == a1.begin());
        for (long i = 1; i <= MaxSize; ++i) {
            a1.emplace_back(i);
        }
        it = a1.shuffle_if([](Object const& o)->bool { return o.i > 10; });
        XCTAssert(it == a1.end());
        it = a1.shuffle_if([](Object const& o)->bool { return o.i > 4; });
        XCTAssert(it == a1.begin() + 4);
        a2.clear();
        long const n = a1.evict_if(a2, [](Object const& o)->bool { return o.i == 4 || o.i == 10; });
        XCTAssert(1 == n && a1.size() == MaxSize - n && a2.size() == 1);
        for (Object const& o : a2) {
            XCTAssert(o == 4L || o == 10L);
        }
        for (Object const& o : a1) {
            XCTAssert(o != 4L && o != 10L);
        }
        printo(std::cout, "a1 = ", a1, ", a2 = ", a2, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        //return;
    }
    XCTAssert(0 == Object::counter, @"Object::counter = %ld", Object::counter);

    try {
        using Scalar = double;
        using Vector = UTL::SIMDVector<Scalar, 4>;
        using Array = StaticArray<Scalar, Vector::size()*2 - 1, Vector>;
        XCTAssert(alignof(Scalar) == alignof(Array::value_type));
        XCTAssert(alignof(Vector) == alignof(Array::overlay_type));
        Array a1;
        Vector *vec = reinterpret_cast<Vector*>(a1.data());

        constexpr Scalar s = 1;
        a1.resize(Array::max_size() - 1, s);
        vec[0] += vec[0];
        vec[1] += vec[1];
        for (Scalar const& x: a1) {
            XCTAssert(s + s == x, @"s = %f, x = %f", s, x);
        }
        a1.resize(a1.max_size());
        XCTAssert(Scalar{} == a1.back());
        printo(std::cout, *vec, "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testBlockedArray {
    using UTL::BlockedArray;

    BlockedArray<double, 1024>::iterator a, b;
}

//- (void)testLeakCheck {
//    try {
//        constexpr long n = 50000;
//        for (long i = 0; i < n; ++i) {
//            @autoreleasepool {
//                [self testNopadArray];
//                [self testPaddedArray];
//                [self testPaddedArraySlice];
//                [self testNopadArray1D];
//                [self testPaddedArray1D];
//                [self testArraySlice1DEqualPad];
//                [self testArraySlice1DCrossPad];
//                [self testNopadArray2D];
//                [self testPaddedArray2D];
//                [self testArraySlice2DEqualPad];
//                [self testArraySlice2DCrossPad];
//                [self testNopadArray3D];
//                [self testPaddedArray3D];
//                [self testArraySlice3DEqualPad];
//                [self testArraySlice3DCrossPad];
//                [self testDynamicArray];
//                [self testStaticArray];
//            }
//        }
//    } catch (std::exception &e) {
//        XCTAssert(false, @"%s", e.what());
//    }
//}

@end
