//
//  UTLCustomDistribution.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/16/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLCustomDistribution_hh
#define UTLCustomDistribution_hh

#include <stdexcept>
#include <utility>
#include <string>

// MARK:- UTL::__4_::CustomDistribution
//
template <class PDF>
UTL::__4_::CustomDistribution::CustomDistribution(value_type const min, value_type const max, PDF &&pdf, bool should_normalize_pdf)
: Distribution(), _min(min), _max(max), _mean(), _var(), _pdf(), _cdf(), _icdf() {
    // sanity check:
    if (min >= max) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - min >= max");
    }

    // helpers:
    integrator_type const integrator = default_integrator();
    sampler_type const sampler = default_sampler();

    // moments:
    value_type mom_1st = integrator([&pdf](value_type x)->value_type {
        return pdf(x) * x;
    }, min, max).first;
    value_type mom_2nd = integrator([&pdf](value_type x)->value_type {
        return pdf(x) * (x*x);
    }, min, max).first;

    // pdf:
    sampler_type::point_list_type
    pdf_pts = sampler(pdf, min, max, 1);

    // cdf:
    sampler_type::point_list_type
    cdf_pts = sampler([&pdf, &integrator, min](value_type const x)->value_type {
        return integrator(pdf, min, x).first;
    }, min, max, 1);

    // optional normalization:
    if (should_normalize_pdf) {
        // normalize cdf:
        cdf_pts.reverse();
        value_type const denom = cdf_pts.front().second;
        for (auto &pt : cdf_pts) {
            pt.second /= denom;
        }
        cdf_pts.reverse();
        // normalize pdf:
        for (auto &pt : pdf_pts) {
            pt.second /= denom;
        }
        // moments:
        mom_1st /= denom;
        mom_2nd /= denom;
    }

    // save:
    this->_pdf = spline_coefficient_type{pdf_pts.begin(), pdf_pts.end()}.interpolator();
    this->_cdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
    _mean = mom_1st;
    _var  = mom_2nd - _mean*_mean;

    // inverse cdf:
    for (auto &pt : cdf_pts) {
        std::swap(pt.first, pt.second);
    }
    this->_icdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
}
template <class PDF, class CDF, typename std::enable_if<!std::is_fundamental<CDF>::value, long>::type>
UTL::__4_::CustomDistribution::CustomDistribution(value_type const min, value_type const max, PDF &&pdf, CDF &&cdf)
: Distribution(), _min(min), _max(max), _mean(), _var(), _pdf(), _cdf(), _icdf() {
    // sanity check:
    if (min >= max) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - min >= max");
    }

    // helpers:
    integrator_type const integrator = default_integrator();
    sampler_type const sampler = default_sampler();

    // mean & variance:
    _mean = integrator([&pdf](value_type x)->value_type {
        return pdf(x) * x;
    }, min, max).first;
    _var  = integrator([&pdf](value_type x)->value_type {
        return pdf(x) * (x*x);
    }, min, max).first - _mean*_mean;

    // pdf:
    sampler_type::point_list_type const
    pdf_pts = sampler(pdf, min, max, 1);
    this->_pdf = spline_coefficient_type{pdf_pts.begin(), pdf_pts.end()}.interpolator();

    // cdf:
    sampler_type::point_list_type
    cdf_pts = sampler(cdf, min, max, 1);
    this->_cdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();

    // inverse cdf:
    for (auto &pt : cdf_pts) {
        std::swap(pt.first, pt.second);
    }
    this->_icdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
}

#endif /* UTLCustomDistribution_hh */
