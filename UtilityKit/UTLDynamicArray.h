//
//  UTLDynamicArray.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/22/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLDynamicArray_h
#define UTLDynamicArray_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLArray.h>
#include <initializer_list>
#include <type_traits>
#include <iterator>
#include <utility>
#include <vector>
#include <limits>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Variable-length contiguous array.
     @discussion A simplified version of std::vector.
     It removes the use of custom allocator.

     Internally, it wraps an instance of std::vector that does all the heavy lifting of memory management (reserving, resizing, insertion, etc.).
     */
    template <class Type>
    class DynamicArray : public Array<Type> {
        using vector_type = std::vector<Type>;
        vector_type _vec{};

        explicit DynamicArray(decltype(nullptr), vector_type const& vec)
        : Array<Type>(), _vec(vec) { _invalidate(); }
        inline explicit DynamicArray(decltype(nullptr), vector_type&& vec) noexcept(std::is_nothrow_move_constructible<vector_type>::value)
        : Array<Type>(), _vec(std::move(vec)) { _invalidate(); }

    public:
        // types:
        using value_type             = typename Array<Type>::value_type;
        using reference              = typename Array<Type>::reference;
        using const_reference        = typename Array<Type>::const_reference;
        using pointer                = typename Array<Type>::pointer;
        using const_pointer          = typename Array<Type>::const_pointer;
        using iterator               = typename Array<Type>::iterator;
        using const_iterator         = typename Array<Type>::const_iterator;
        using reverse_iterator       = typename Array<Type>::reverse_iterator;
        using const_reverse_iterator = typename Array<Type>::const_reverse_iterator;
        using size_type              = typename Array<Type>::size_type;
        using difference_type        = typename Array<Type>::difference_type;

        // destructor:
        ~DynamicArray() {}

        // Constructors:
        /**
         @brief Construct an empty array.
         */
        explicit DynamicArray() : DynamicArray(nullptr, {}) {}

        /**
         @brief Construct an array with the given size.
         */
        explicit DynamicArray(size_type const sz);
        explicit DynamicArray(size_type const sz, value_type const& x);

        /**
         @brief Construct an array from the contents of the range [first, last).
         @discussion The iterators should be well defined.
         */
        template <class ForwardIt, typename std::enable_if<std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<ForwardIt>::iterator_category>::value, long>::type = 0L>
        DynamicArray(ForwardIt first, ForwardIt last) : DynamicArray(nullptr, {first, last}) {}

        /**
         @brief Constructs an array with the contents of the initializer list.
         */
        DynamicArray(std::initializer_list<value_type> const il) : DynamicArray(il.begin(), il.end()) {}

        /**
         @brief Copy/move constructor.
         */
        DynamicArray(DynamicArray const &other) : DynamicArray(nullptr, other._vec) {}
        DynamicArray(DynamicArray &&other) noexcept(noexcept(DynamicArray(nullptr, std::move(_vec)))) : DynamicArray(nullptr, std::move(other._vec)) { other._invalidate(); }

        // Assignments:
        /**
         @brief Copy/move assignment.
         */
        inline DynamicArray& operator=(DynamicArray const &other);
        inline DynamicArray& operator=(DynamicArray &&other) noexcept(noexcept(DynamicArray(std::move(other))));

        /**
         @brief Replaces the contents with those identified by initializer list.
         */
        DynamicArray& operator=(std::initializer_list<value_type> const il) { return *this = DynamicArray{il.begin(), il.end()}; }

        // Capacity:
        /**
         @brief Returns the maximum possible number of elements.
         @discussion It simply returns the maximum value that size_type can hold.
         */
        static constexpr size_type max_size() noexcept { return std::numeric_limits<size_type>::max(); }
        /**
         @brief The capacity of the allocated storage.
         */
        size_type capacity() const noexcept { return static_cast<size_type>(_vec.capacity()); }

        /**
         @brief Reserve a storage to hold at least new_cap elements.
         @discussion See std::vector::reserve implementation.
         */
        void reserve(size_type const new_cap);

        /**
         @brief Reduces memory usage by freeing unused memory.
         @discussion See std::vector::shrink_to_fit implementation.
         */
        void shrink_to_fit() { _vec.shrink_to_fit(), _invalidate(); }

        // Element Access:
        pointer       data()       noexcept { return _vec.data(); }
        const_pointer data() const noexcept { return _vec.data(); }

        /**
         @brief Implicit cast to std::vector<Type> const&.
         */
        operator std::vector<Type> const&() const& noexcept { return _vec; }

        // Modifiers:
        /**
         @brief Clear the contents of *this.
         */
        void clear() noexcept { _vec.clear(), _invalidate(); }

        /**
         @brief Resize the container with optionally filling with a default value.
         */
        void resize(size_type const sz);
        void resize(size_type const sz, value_type const &x);

        /**
         @brief Append an element to *this.
         */
        template <class... Args>
        reference emplace_back(Args&&... args);
        reference push_back(value_type const &x) { return emplace_back(x); }
        reference push_back(value_type &&x) { return emplace_back(std::move(x)); }

        /**
         @brief Inserts an element into *this before pos.
         @return Iterator pointing to the inserted value.
         */
        template <class... Args>
        iterator emplace(const_iterator const pos, Args&&... args);
        iterator insert(const_iterator const pos, const value_type &x) { return emplace(pos, x); }
        iterator insert(const_iterator const pos, value_type &&x) { return emplace(pos, std::move(x)); }

        /**
         @brief Removes specified elements from the container.
         @discussion The first version removes the element at pos.
         The iterator pos must be valid and dereferenceable.
         Thus the end() iterator (which is valid, but is not dereferencable) cannot be used as a value for pos.

         The second version removes the elements in the range [first; last).
         The iterator first does not need to be dereferenceable if first==last: erasing an empty range is a no-op.
         @return Iterator following the last removed element.
         If the iterator pos refers to the last element, the end() iterator is returned.
         */
        iterator erase(const_iterator const pos) { return erase(pos, pos + 1); }
        iterator erase(const_iterator const first, const_iterator const last);

        /**
         @brief Removes the last element of the container.
         @discussion Calling it when *this is empty is no-op.
         */
        void pop_back() noexcept { if (!this->empty()) _vec.pop_back(), _invalidate(); }

        /**
         @brief Picks out the elements for which the predicate returns true.
         @discussion The evicted elements are pushed back to the passed container by invoking push_back(std::move(...)).

         The order may not be preserved after this operation.
         @return The number of elements evicted.
         */
        template <class Container, class Predicate/*bool(const_reference)*/>
        size_type evict_if(Container& bucket, Predicate&& pred);

        /**
         @brief Storage swap.
         */
        inline void swap(DynamicArray &other) noexcept;

    private:
        void _invalidate() noexcept {
            this->_end = this->_beg = _vec.empty() ? nullptr : _vec.data();
            this->_end += _vec.size();
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLDynamicArray.hh>

#endif /* UTLDynamicArray_h */
