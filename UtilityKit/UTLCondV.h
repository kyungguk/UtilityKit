//
//  UTLCondV.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/23/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLCondV_h
#define UTLCondV_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLLock.h>
#include <vector>
#include <pthread.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
    // forward decls
    //
    class Mutex;
    class SpinLock;
    template <typename Lock> class CondV;

    /**
     @brief pthread condition variable wrapper.
     @discussion Conceptually similar to c++11 std::condition_variable.
     Consult documentation of std::condition_variable.
     */
    template <>
    class CondV<Mutex> {
        pthread_cond_t __c_;

        CondV(CondV const&) = delete;
        CondV& operator=(CondV const&) = delete;
    public:
        typedef Mutex lock_type;
        enum timed_status {timeout = 0, no_timeout};

        ~CondV(); // terminate when error occurs while destorying the native handle.
        explicit CondV();

        // notification:
        void notify_one() noexcept;
        void notify_all() noexcept;

        // waiting:
        void wait(lock_type& __mx);
        /**
         @brief Wake the waiting thread up by checking the predicate.
         @discussion Spurious wake up can be prevented.
         The predicate object should be callable with no argument and return value that can be convertible to bool.
         If the return value is `true`, the waiting thread wakes up.

         Note that before enter to this method lock must be acquired, after wait(lock) exits it is also reacquired, i.e. lock can be used as a guard to pred() access.
         */
        template<typename Predicate>
        void wait(lock_type& mx, Predicate pred) { while (!pred()) wait(mx); }

        /**
         @brief Blocks the current thread until the condition variable is woken up or after the specified timeout duration.
         */
        timed_status wait_for(lock_type& __mx, unsigned __dur_in_sec);

        /**
         @brief Same as wait_for, but can be used to filter out spurious wakeups.
         */
        template<typename Predicate>
        bool wait_for(lock_type& __mx, unsigned __dur_in_sec, Predicate __p);

        // native handle:
        typedef pthread_cond_t* native_handle_type;
        native_handle_type native_handle() noexcept { return &__c_; }
    };
    template<class _Predicate>
    bool CondV<Mutex>::wait_for(lock_type& mx, unsigned __dur_in_sec, _Predicate pred)
    {
        while (!pred()) {
            if (timeout == wait_for(mx, __dur_in_sec)) {
                return pred();
            }
        }
        return true;
    }

    /**
     @brief lock-free condition variable.
     */
    template <>
    class CondV<SpinLock> {
        std::vector<SpinLock*> _wait_list;
        SpinLock _master_key;

        CondV(CondV const&) = delete;
        CondV& operator=(CondV const&) = delete;
    public:
        typedef SpinLock lock_type;
        enum timed_status {timeout = 0, no_timeout};

        ~CondV(); // terminate when error occurs while destorying the native handle.
        explicit CondV();

        // notification:
        void notify_one() noexcept;
        void notify_all() noexcept;

        // waiting:
        void wait(lock_type& mx);
        /**
         @brief Wake the waiting thread up by checking the predicate.
         @discussion Spurious wake up can be prevented.
         The predicate object should be callable with no argument and return value that can be convertible to bool.
         If the return value is `true`, the waiting thread wakes up.

         Note that before enter to this method lock must be acquired, after wait(lock) exits it is also reacquired, i.e. lock can be used as a guard to pred() access.
         */
        template<typename Predicate>
        void wait(lock_type& mx, Predicate pred) { while (!pred()) wait(mx); }
    };
} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLLock.h>
#include <queue>
#include <stdexcept>
#include <chrono>
#include <utility> // std::move
#include <pthread.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief pthread condition variable wrapper.
     @discussion Conceptually similar to c++11 std::condition_variable.
     Consult documentation of std::condition_variable.
     */
    class CondV {
        using lock_type = Mutex;

        pthread_cond_t __c_;

        CondV(CondV const&) = delete;
        CondV& operator=(CondV const&) = delete;
    public:
        enum timed_status {timeout = 0, no_timeout};

        ~CondV(); // terminate when error occurs while destorying the native handle.
        explicit CondV();

        // notification:
        void notify_one() noexcept { pthread_cond_signal(&__c_); }
        void notify_all() noexcept { pthread_cond_broadcast(&__c_); }

        // waiting:
        void wait(lock_type& mx) { if (pthread_cond_wait(&__c_, mx.native_handle())) throw std::runtime_error(__PRETTY_FUNCTION__); }

        /**
         @brief Wake the waiting thread up by checking the predicate.
         @discussion Spurious wake up can be prevented.
         The predicate object should be callable with no argument and return value that can be convertible to bool.
         If the return value is `true`, the waiting thread wakes up (i.e., return ​false if the waiting should be continued).

         Note that before entering to this method lock must be acquired; after wait(lock) exits it is also reacquired, i.e. lock can be used as a guard to pred() access.
         */
        template <typename Predicate>
        void wait(lock_type&mx, Predicate&& pred) { while (!std::forward<Predicate>(pred)()) wait(mx); }

        /**
         @brief Blocks the current thread until the condition variable is woken up or after the specified timeout duration.
         */
        timed_status wait_for(lock_type& mx, std::chrono::seconds const &duration) { return wait_until(mx, std::chrono::system_clock::now() + duration); }

        /**
         @brief Same as wait_for, but can be used to filter out spurious wakeups.
         */
        template <typename Predicate>
        bool wait_for(lock_type& mx, std::chrono::seconds const &duration, Predicate&& pred) { return wait_until(mx, std::chrono::system_clock::now() + duration, std::forward<Predicate>(pred)); }

        /**
         @brief Blocks the current thread until the condition variable is woken up or until the specified time.
         */
        timed_status wait_until(lock_type& mx, std::chrono::system_clock::time_point const &future);

        /**
         @brief Same as wait_until, but can be used to filter out spurious wakeups.
         */
        template <typename Predicate>
        bool wait_until(lock_type& mx, std::chrono::system_clock::time_point const &future, Predicate&& pred);

        // native handle:
        using native_handle_type = pthread_cond_t*;
        native_handle_type native_handle() noexcept { return &__c_; }
    };
    template <class Predicate>
    bool CondV::wait_until(lock_type& mx, std::chrono::system_clock::time_point const &future, Predicate&& pred)
    {
        while (!std::forward<Predicate>(pred)()) {
            if (timeout == wait_until(mx, future)) {
                return std::forward<Predicate>(pred)();
            }
        }
        return true;
    }
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLCondV_h */
