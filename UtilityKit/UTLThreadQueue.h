//
//  UTLThreadQueue.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/25/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLThreadQueue_h
#define UTLThreadQueue_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLThread.h>
#include <UtilityKit/UTLLock.h>
#include <UtilityKit/UTLCondV.h>
#include <UtilityKit/UTLSema.h>
#include <UtilityKit/UTLBlockQueue.h>
#include <type_traits>
#include <functional>
#include <utility>
#include <string>
#include <vector>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
    /**
     @brief FIFO queue of tasks waiting to be executed by concurrent threads.
     @discussion Conceptually similar to the GCD queueing model.

     On construction, `*this` creates a pool of worker threads that are initially blocked.
     When tasks or jobs are submitted by either `async`, `sync` or `apply`, they are queued for execution.
     After an indeterminate time, one of the worker thead picks up and executes a task. It happens in a FIFO order.

     On destruction, `*this` waits for all the worker threads to finish their job and then terminate themselves before destructing itself.
     */
    class ThreadQueue {
        BlockQueue<Mutex> _q;
        std::vector<Thread> _pool;
        std::string _label;

    public:
        ~ThreadQueue();

        /**
         @brief Construct a thread queue with Thread::hardware_concurrency() worker threads.
         */
        explicit ThreadQueue(std::string const& label);
        /**
         @brief Construct a thread queue with concurrency worker threads.
         */
        explicit ThreadQueue(std::string const& label, unsigned concurrency);

        /**
         @brief Thread Q label.
         */
        const char* label() const noexcept { return _label.c_str(); }

        /**
         @brief Number of threads associated *this.
         */
        auto concurrency() const noexcept -> decltype(_pool.size());

        // block submission:
        /**
         @brief Executes the job asynchronously.
         @discussion This call immediately exits upon queueing the job.
         It is executed after an indeterminate time.

         Since the job should be alive until the end of the execution, the _Block type should be either copy- or move-constructible depending on how the _Block variable passed.

         @param b A function object which should be callable with no argument. The return value will be ignored.
         */
        template<typename Block/*void(void)*/>
        void async(Block&& b);

        /**
         @brief Executes the job synchronously.
         @discussion This call is blocked until the job is executed and finished by a worker thread.

         Since the job is guarrentied to be alive until the end of the execution, no copy/move is performed.

         @param b A function object which should be callable with no argument. The return value will be ignored.
         */
        template<typename Block/*void(void)*/>
        void sync(Block&& b);

        /**
         @brief Applies the passed block of code repeatedly by `iterations` times with the current iteration as its sole argument.
         @discussion This call is blocked until all iterations are finished.

         This can be used for a parallel execution of a `for` loop. For example, the following code
         \code
         int sum = 0;
         for (int i = 0; i < 10; sum += ++i) {}
         \endcode
         can be run in parallel with
         \code
         Queue q("sum");
         atomic<int> sum{0};
         q.apply([&sum](size_t i)->void {
         sum += int(i) + 1;
         });
         \endcode
         Note that the iteration count passed to the apply block runs from 0 to `iterations`-1.

         Since the job is guarrentied to be alive until the end of the execution, no copy/move is performed.

         @param a A function object which should be callable with the current iteration count as its sole argument. The return value will be ignored.
         */
        template<typename Apply/*void(unsigned long)*/>
        void apply(unsigned long const iterations, Apply&& a);
    };
    template<typename Block/*void(void)*/>
    void ThreadQueue::async(Block&& _b)
    {
        typedef typename std::decay<Block>::type B;
        B *b = new B(std::forward<Block>(_b));
        _q.enqueue([b]() {
            (*b)();
            delete b;
        });
    }
    template<typename Block/*void(void)*/>
    void ThreadQueue::sync(Block&& _b)
    {
        Block b = std::forward<Block>(_b);
        Sema<int> sem(0);
        _q.enqueue([&sem, &b]() {
            b();
            sem.post();
        });
        sem.wait();
    }
    template<typename Apply/*void(unsigned long)*/>
    void ThreadQueue::apply(unsigned long const iterations, Apply&& _a)
    {
        Apply a = std::forward<Apply>(_a);
#if 1
        Sema<long> sem(0);
        unsigned long concurrency = this->concurrency();

        // distribute concurrency-1 works
        //
        for (unsigned long offset = 1; offset < concurrency; ++offset) {
            _q.enqueue([&sem, &a, offset, concurrency, iterations]() {
                for (unsigned long i = offset; i < iterations; i += concurrency) {
                    a(i);
                }
                sem.post();
            });
        }

        // perform work for calling thread
        //
        for (unsigned long i = 0; i < iterations; i += concurrency) {
            a(i);
        }

        // wait
        //
        for (unsigned long i = 1; i < concurrency; ++i) {
            sem.wait();
        }
#else
        long concurrency = long(this->concurrency());
        Sema<long> sem(concurrency);

        // enqueue
        //
        for (unsigned long i = 0; i < iterations; ++i) {
            sem.wait();
            _q.enqueue([&sem, &a, i]() {
                a(i);
                sem.post();
            });
        }

        // wait
        //
        while (concurrency--) {
            sem.wait();
        }
#endif
    }
} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLThread.h>
#include <functional>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief FIFO queue of tasks waiting to be executed synchronously or asynchronously.
     @discussion Initial purpose was to model GCD-like multi-thread programming, but test showed that it is inefficient.
     Current implementation only provides static function apply.
     */
    class ThreadQueue {
    public:
        explicit ThreadQueue() = delete;

        /**
         @brief Returns the number of processors.
         @discussion Simply calls Thread::hardware_concurrency().
         */
        static unsigned hardware_concurrency() noexcept { return Thread::hardware_concurrency(); }

        /**
         @brief Applies the passed block of code repeatedly by `iterations` times with the current iteration as its sole argument.
         @discussion This call is blocked until all iterations are finished.

         This can be used for a parallel execution of a `for` loop. For example, the following code
         \code
         int sum = 0;
         for (int i = 0; i < 10; sum += ++i) {}
         \endcode
         can be run in parallel with
         \code
         Queue q("sum");
         atomic<int> sum{0};
         q.apply([&sum](size_t i)->void {
         sum += int(i) + 1;
         });
         \endcode
         Note that the iteration count passed to the apply block runs from 0 to `iterations`-1.

         Since the job is guarrentied to be alive until the end of the execution, no copy/move is performed.

         @param iterations Number of iterations.
         @param block A callable object with signature `void(unsigned long const i)'.
         */
        static void apply(unsigned long const iterations, std::function<void(unsigned long const)> const &block);
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLThreadQueue_h */
