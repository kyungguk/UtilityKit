//
//  UTLMatrix.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLMatrix_h
#define UTLMatrix_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLVector.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // TODO: make_matrix
    /**
     */
    template <class Type, long M, long N = M>
    struct Matrix {
        // types:
        using folded_vector_type     = Vector<Vector<Type, N>, M>;
        using value_type             = Type;
        using reference              = Type&;
        using const_reference        = Type const&;
        using pointer                = Type*;
        using const_pointer          = Type const*;
        using iterator               = pointer;
        using const_iterator         = const_pointer;
        using reverse_iterator       = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;
        using size_type              = long;
        using difference_type        = long;
        using size_vector_type       = Vector<size_type, 2L>;

        // rank()
        // size()
        // dims()
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLMatrix.hh>

#endif /* UTLMatrix_h */
