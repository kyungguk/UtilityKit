//
//  UTLPerpendicularMaxwellianDistribution.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLPerpendicularMaxwellianDistribution.h"

// MARK: Version 3
//
#include <utility>
#include <cmath>
#include <stdexcept>

#pragma mark helpers
namespace {
    auto gaussian_pdf(UTL::__3_::PerpendicularMaxwellianDistribution::value_type x)
    -> UTL::__3_::PerpendicularMaxwellianDistribution::value_type {
        // note that x is not multiplied!
        return std::exp(-x*x)*2;
    }
    auto gaussian_cdf(UTL::__3_::PerpendicularMaxwellianDistribution::value_type x)
    -> UTL::__3_::PerpendicularMaxwellianDistribution::value_type {
        return 1 - std::exp(-x*x);
    }
}

#pragma mark class implementation
UTL::__3_::PerpendicularMaxwellianDistribution::PerpendicularMaxwellianDistribution(value_type vth, bool should_normalize_cdf)
: Distribution(), _icdf(), _vth(vth) {
    if (_vth <= 0) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }

    // sampler:
    sampler_type sampler = this->_sampler();

    // sample cdf:
    constexpr value_type x_max = 5;
    sampler_type::point_list_type
    cdf_pts = sampler(&::gaussian_cdf, 0, x_max, 1);

    // optional normalization:
    if (should_normalize_cdf) {
        // normalize cdf:
        cdf_pts.reverse();
        value_type const denom = cdf_pts.front().second;
        sampler_type::point_list_type::iterator
        first = cdf_pts.begin(), last = cdf_pts.end();
        for (; first != last; ++first) {
            first->second /= denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    sampler_type::point_list_type::iterator
    first = cdf_pts.begin(), last = cdf_pts.end();
    for (; first != last; ++first) {
        std::swap(first->second, first->first);
    }
    _icdf = interp_type(cdf_pts.begin(), cdf_pts.end());
}

UTL::__3_::PerpendicularMaxwellianDistribution::PerpendicularMaxwellianDistribution(PerpendicularMaxwellianDistribution const &o) noexcept
: Distribution(), _icdf(o._icdf), _vth(o._vth) {
}
auto UTL::__3_::PerpendicularMaxwellianDistribution::operator=(PerpendicularMaxwellianDistribution const &o) noexcept
-> PerpendicularMaxwellianDistribution &{
    if (this != &o) {
        _icdf = o._icdf;
        _vth = o._vth;
    }
    return *this;
}
UTL::__3_::PerpendicularMaxwellianDistribution::PerpendicularMaxwellianDistribution(PerpendicularMaxwellianDistribution &&o) noexcept
: Distribution(), _icdf(std::move(o._icdf)), _vth(std::move(o._vth)) {
}
auto UTL::__3_::PerpendicularMaxwellianDistribution::operator=(PerpendicularMaxwellianDistribution &&o) noexcept
-> PerpendicularMaxwellianDistribution &{
    if (this != &o) {
        _icdf = std::move(o._icdf);
        _vth = std::move(o._vth);
    }
    return *this;
}

auto UTL::__3_::PerpendicularMaxwellianDistribution::mean() const noexcept
-> value_type {
    return _vth/M_2_SQRTPI;
}
auto UTL::__3_::PerpendicularMaxwellianDistribution::variance() const noexcept
-> value_type {
    return _vth*_vth - this->mean()*this->mean();
}

auto UTL::__3_::PerpendicularMaxwellianDistribution::pdf(value_type v2) const
-> value_type {
    value_type const x = v2 / _vth;
    return v2*::gaussian_pdf(x) / (_vth*_vth);
}
auto UTL::__3_::PerpendicularMaxwellianDistribution::cdf(value_type v2) const
-> value_type {
    value_type const x = v2 / _vth;
    return ::gaussian_cdf(x);
}
auto UTL::__3_::PerpendicularMaxwellianDistribution::icdf(value_type cdf) const
-> value_type {
    return _icdf(cdf)*_vth;
}

auto UTL::__3_::PerpendicularMaxwellianDistribution::_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.maxRecursion = 20;
        sampler.accuracyGoal = 7;
        sampler.initialPoints = 100;
        sampler.yScaleAbsoluteTolerance = 1e-10;
    }
    return sampler;
}


// MARK:- Version 4
//
#include <UtilityKit/AuxiliaryKit.h>
#include <stdexcept>
#include <utility>
#include <string>
#include <cmath>

UTL::__4_::PerpendicularMaxwellianDistribution::PerpendicularMaxwellianDistribution(PerpendicularMaxwellianDistribution const &o)
: Distribution(), _vth(o._vth), _icdf(o._icdf) {
}
UTL::__4_::PerpendicularMaxwellianDistribution::PerpendicularMaxwellianDistribution(PerpendicularMaxwellianDistribution &&o)
: Distribution(), _vth(o._vth), _icdf() {
    _icdf = std::move(o._icdf);
}
auto UTL::__4_::PerpendicularMaxwellianDistribution::operator=(PerpendicularMaxwellianDistribution const &o)
-> PerpendicularMaxwellianDistribution &{
    if (this != &o) {
        PerpendicularMaxwellianDistribution{o}.swap(*this);
    }
    return *this;
}
auto UTL::__4_::PerpendicularMaxwellianDistribution::operator=(PerpendicularMaxwellianDistribution &&o)
-> PerpendicularMaxwellianDistribution &{
    if (this != &o) {
        PerpendicularMaxwellianDistribution{std::move(o)}.swap(*this);
    }
    return *this;
}

void UTL::__4_::PerpendicularMaxwellianDistribution::swap(PerpendicularMaxwellianDistribution &o)
{
    _icdf.swap(o._icdf);
    std::swap(_vth, o._vth);
}

auto UTL::__4_::PerpendicularMaxwellianDistribution::mean() const noexcept
-> value_type {
    return _vth/M_2_SQRTPI;
}
auto UTL::__4_::PerpendicularMaxwellianDistribution::variance() const noexcept
-> value_type {
    return _vth*_vth - pow<2>(mean());
}

auto UTL::__4_::PerpendicularMaxwellianDistribution::pdf(value_type const v2) const noexcept
-> value_type {
    value_type const x = v2 / _vth;
    return v2*gaussian_pdf(x) / (_vth*_vth);
}
auto UTL::__4_::PerpendicularMaxwellianDistribution::cdf(value_type const v2) const noexcept
-> value_type {
    value_type const x = v2 / _vth;
    return gaussian_cdf(x);
}
auto UTL::__4_::PerpendicularMaxwellianDistribution::icdf(value_type const cdf) const
-> value_type {
    try {
        return _vth*_icdf(cdf)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

auto UTL::__4_::PerpendicularMaxwellianDistribution::default_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.set_max_recursion(20);
        sampler.set_accuracy_goal(7);
        sampler.set_initial_points(100);
        sampler.set_y_scale_absolute_tolerance(1e-10);
    }
    return sampler;
}

UTL::__4_::PerpendicularMaxwellianDistribution::PerpendicularMaxwellianDistribution(value_type const vth)
: Distribution(), _vth(vth), _icdf() {
    if (_vth <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative vth");
    }

    // sampler:
    sampler_type const sampler = default_sampler();

    // sample cdf:
    constexpr value_type x_max = 5;
    sampler_type::point_list_type
    cdf_pts = sampler(&gaussian_cdf, 0, x_max, 1);

    // re-normalization:
    {
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        for (auto &pt : cdf_pts) {
            pt.second = (pt.second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    for (auto &pt : cdf_pts) {
        std::swap(pt.first, pt.second);
    }
    this->_icdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
}

auto UTL::__4_::PerpendicularMaxwellianDistribution::gaussian_pdf(value_type const x) noexcept
-> value_type {
    // note that x is not multiplied!
    return std::exp(-x*x)*2;
}
auto UTL::__4_::PerpendicularMaxwellianDistribution::gaussian_cdf(value_type const x) noexcept
-> value_type {
    return 1 - std::exp(-x*x);
}
