//
//  UtilityKitTests-SIMDKit.mm
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#include <UtilityKit/UTLPrinto.h>
#include <iostream>
#include <type_traits>
//#define VECTOR_KIT_NO_SIMD
#include <UtilityKit/SIMDKit.h>
//#include "__ct_sqrt.h"

@interface UtilityKitTests_SIMDKit__3_ : XCTestCase

@end

@implementation UtilityKitTests_SIMDKit__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testVectorAlignment {
    using UTL::__3_::SIMD::__make_vector;

    constexpr long N = 010;
    printo(std::cout, "char[", N, "] = ",
           std::alignment_of<char[N]>::value,
           ", <char> = ",
           std::alignment_of<__make_vector<char, N>::type>::value, '\n');
    printo(std::cout, "unsigned char[", N, "] = ",
           std::alignment_of<unsigned char[N]>::value,
           ", <unsigned char> = ",
           std::alignment_of<__make_vector<unsigned char, N>::type>::value, '\n');
    printo(std::cout, "short[", N, "] = ",
           std::alignment_of<short[N]>::value,
           ", <short> = ",
           std::alignment_of<__make_vector<short, N>::type>::value, '\n');
    printo(std::cout, "unsigned short[", N, "] = ",
           std::alignment_of<unsigned short[N]>::value,
           ", <unsigned short> = ",
           std::alignment_of<__make_vector<unsigned short, N>::type>::value, '\n');
    printo(std::cout, "int[", N, "] = ",
           std::alignment_of<int[N]>::value,
           ", <int> = ",
           std::alignment_of<__make_vector<int, N>::type>::value, '\n');
    printo(std::cout, "unsigned int[", N, "] = ",
           std::alignment_of<unsigned int[N]>::value,
           ", <unsigned int> = ",
           std::alignment_of<__make_vector<unsigned int, N>::type>::value, '\n');
    printo(std::cout, "long[", N, "] = ",
           std::alignment_of<long[N]>::value,
           ", <long> = ",
           std::alignment_of<__make_vector<long, N>::type>::value, '\n');
    printo(std::cout, "unsigned long[", N, "] = ",
           std::alignment_of<unsigned long[N]>::value,
           ", <unsigned long> = ",
           std::alignment_of<__make_vector<unsigned long, N>::type>::value, '\n');
    printo(std::cout, "float[", N, "] = ",
           std::alignment_of<float[N]>::value,
           ", <float> = ",
           std::alignment_of<__make_vector<float, N>::type>::value, '\n');
    printo(std::cout, "double[", N, "] = ",
           std::alignment_of<double[N]>::value,
           ", <double> = ",
           std::alignment_of<__make_vector<double, N>::type>::value, '\n');
}

- (void)testVector {
    using UTL::__3_::SIMD::Vector;

    @autoreleasepool { // size check:
        using type = double;
        constexpr int sz{8};
        Vector<type, sz> vec;
        type d[sz];
        XCTAssert(sizeof(d) == sizeof(vec) && sz == vec.size(),
                  @"sizeof(d) = %lu, sizeof(vec) = %lu", sizeof(d), sizeof(vec));
        XCTAssert(sizeof(decltype(vec)::comparison_result_type) == sizeof(vec) && sizeof(decltype(vec)::shuffle_mask_type) == sizeof(vec),
                  @"comparison_result_type = %lu, shuffle_mask_type = %lu", sizeof(decltype(vec)::comparison_result_type), sizeof(decltype(vec)::shuffle_mask_type));
    }

    @autoreleasepool { // initialization check:
        using type = long;
        Vector<type, 8>  v8(10);
        printo(std::cout, v8, '\n');

        for (unsigned i = 0; i < v8.size(); ++i) XCTAssert(10 == v8[i], @"v8[%d] = %ld", i, v8[i]);

        Vector<type, 4> v4{{1, 2, 3, 4}};
        for (unsigned i = 0; i < v4.size(); ++i) XCTAssert(i + 1 == v4[i], @"v4[%d] = %ld", i, v4[i]);

        const type a[2] = {1, 2};
        Vector<type, 2> v2(a);
        for (unsigned i = 0; i < v2.size(); ++i) {
            XCTAssert(a[i] == v2[i], @"");
        }
    }

    @autoreleasepool { // compound operators check:
        using type = int;
        constexpr int sz{8};
        Vector<type, sz> v1(10), v2(5), v3 = v1;

        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v1[i] == v3[i], @"");

        // artithematic with a vector:
        v3 = v1;
        v3 += v2;
        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v3[i] == v1[i] + v2[i], @"");

        v3 = v1;
        v3 -= v2;
        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v3[i] == v1[i] - v2[i], @"");

        v3 = v1;
        v3 *= v2;
        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v3[i] == v1[i] * v2[i], @"");

        v3 = v1;
        v3 /= v2;
        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v3[i] == v1[i] / v2[i], @"");

        v3 = v1;
        v3 %= v2;
        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v3[i] == v1[i] % v2[i], @"");

        // bitwise with a vector:
        v3 = v1;
        v3 |= v2;
        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v3[i] == (v1[i] | v2[i]), @"");

        v3 = v1;
        v3 &= v2;
        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v3[i] == (v1[i] & v2[i]), @"");

        v3 = v1;
        v3 ^= v2;
        for (unsigned i = 0; i < v1.size(); ++i) XCTAssert(v3[i] == (v1[i] ^ v2[i]), @"");
    }
}

- (void)testVector_operators_arithmetic {
    using UTL::__3_::SIMD::Vector;

    @autoreleasepool {
        using type = float;
        constexpr int sz{2};
        typedef Vector<type, sz> _Vec;
        std::unique_ptr<_Vec> _a(new _Vec), _b(new _Vec), _c(new _Vec);
        _Vec &a = *_a, &b = *_b, &c = *_c;

        // unary
        a = _Vec(1); b = _Vec(2);
        c = +a;
        XCTAssert(1 == c[0] && 1 == c[1], @"");
        c = -b;
        XCTAssert(-2 == c[0] && -2 == c[1], @"");

        // binary +
        c = a + b;
        XCTAssert(3 == c[0] && 3 == c[1], @"");
        c = a + _Vec(2);
        XCTAssert(3 == c[0] && 3 == c[1], @"");
        c = _Vec(1) + b;
        XCTAssert(3 == c[0] && 3 == c[1], @"");

        // binary -
        c = a - b;
        XCTAssert(-1 == c[0] && -1 == c[1], @"");
        c = a - _Vec(2);
        XCTAssert(-1 == c[0] && -1 == c[1], @"");
        c = _Vec(1) - b;
        XCTAssert(-1 == c[0] && -1 == c[1], @"");

        // binary *
        c = a * b;
        XCTAssert(2 == c[0] && 2 == c[1], @"");
        c = a * _Vec(2);
        XCTAssert(2 == c[0] && 2 == c[1], @"");
        c = _Vec(1) * b;
        XCTAssert(2 == c[0] && 2 == c[1], @"");

        // binary /
        b = {1, 2};
        c = a / b;
        XCTAssert(1 == c[0] && 0.5 == c[1], @"");
        c = a / _Vec(2);
        XCTAssert(0.5 == c[0] && 0.5 == c[1], @"");
        c = _Vec(1) / b;
        XCTAssert(1 == c[0] && 0.5 == c[1], @"");
    }
}

- (void)testVector_operators_comparison {
    using UTL::__3_::SIMD::Vector;

    @autoreleasepool {
        using type = float;
        constexpr int sz{2};
        typedef Vector<type, sz> _Vec;
        _Vec a, b;

        // ==
        _Vec::comparison_result_type r;
        a = _Vec(1); b = {1, 2};
        r = a == b;
        XCTAssert(-1 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = _Vec(1) == b;
        XCTAssert(-1 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = a == _Vec(2);
        XCTAssert(0 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);

        // !=
        r = a != b;
        XCTAssert(0 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = _Vec(1) != b;
        XCTAssert(0 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = a != _Vec(2);
        XCTAssert(-1 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);

        // <
        r = a < b;
        XCTAssert(0 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = _Vec(1) < b;
        XCTAssert(0 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = a < _Vec(2);
        XCTAssert(-1 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);

        // <=
        r = a <= b;
        XCTAssert(-1 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = _Vec(1) <= b;
        XCTAssert(-1 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = a <= _Vec(2);
        XCTAssert(-1 == r[0] && -1 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);

        // >
        r = a > b;
        XCTAssert(0 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = _Vec(1) > b;
        XCTAssert(0 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = a > _Vec(2);
        XCTAssert(0 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);

        // >=
        r = a >= b;
        XCTAssert(-1 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = _Vec(1) >= b;
        XCTAssert(-1 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);
        r = a >= _Vec(2);
        XCTAssert(0 == r[0] && 0 == r[1], @"r[0] = %d, r[1] = %d", r[0], r[1]);

        // combination
        r = (a>=b) == (a<b);
        XCTAssert(!r[0] && !r[1], @"");
        r = (a>=b) != (a<b);
        XCTAssert(r[0] && r[1], @"");
    }
}

- (void)testVector_operators_integrals {
    using UTL::__3_::SIMD::Vector;

    @autoreleasepool {
        using type = int;
        constexpr int sz{2};
        typedef Vector<type, sz> _Vec;
        _Vec a, b, c;

        // negate
        c = _Vec(0);
        c = ~c;
        XCTAssert(-1 == c[0] && -1 == c[1], @"");

        // binary %
        a = _Vec(1); b = _Vec(2);
        c = a % b;
        XCTAssert(1 == c[0] && 1 == c[1], @"");
        c = a % _Vec(2);
        XCTAssert(1 == c[0] && 1 == c[1], @"");
        c = _Vec(1) % b;
        XCTAssert(1 == c[0] && 1 == c[1], @"");

        // binary &
        b = {2, 3};
        c = a & b;
        XCTAssert(0 == c[0] && 1 == c[1], @"");
        c = a & _Vec(3);
        XCTAssert(1 == c[0] && 1 == c[1], @"");
        c = _Vec(2) & b;
        XCTAssert(2 == c[0] && 2 == c[1], @"");

        // binary |
        b = {2, 3};
        c = a | b;
        XCTAssert(3 == c[0] && 3 == c[1], @"");
        c = a | _Vec(3);
        XCTAssert(3 == c[0] && 3 == c[1], @"");
        c = _Vec(2) | b;
        XCTAssert(2 == c[0] && 3 == c[1], @"");

        // binary ^
        b = {2, 3};
        c = a ^ b;
        XCTAssert(3 == c[0] && 2 == c[1], @"");
        c = a ^ _Vec(3);
        XCTAssert(2 == c[0] && 2 == c[1], @"");
        c = _Vec(2) ^ b;
        XCTAssert(0 == c[0] && 1 == c[1], @"");

        // shift <<, >>
        a = _Vec(1); b = {1, 2};
        (c = a) <<= b;
        XCTAssert(2 == c[0] && 4 == c[1], @"");
        c = a << b;
        XCTAssert(2 == c[0] && 4 == c[1], @"");

        a = _Vec(8);
        (c = a) >>= b;
        XCTAssert(4 == c[0] && 2 == c[1], @"");
        c = a >> b;
        XCTAssert(4 == c[0] && 2 == c[1], @"");
    }
}

- (void)testVectorPerformance {
    using UTL::__3_::SIMD::Vector;

    typedef double _base;
    constexpr int dividend = 8;
    constexpr int sz[2] = {1024*8/dividend, dividend};
    typedef Vector<_base, sz[1]> _vec;
    _vec a[sz[0]], b[sz[0]], c[sz[0]];
    _vec result[sz[0]];
    _base front[sz[0]];

    // initialize
    _vec::value_type buffer_a[sz[1]], buffer_b[sz[1]], buffer_c[sz[1]];
    for (unsigned i = 0; i < sz[0]; ++i) {
        for (unsigned j = 0; j < sz[1]; ++j) {
            buffer_a[j] = arc4random() * _base(1) / RAND_MAX;
            buffer_b[j] = arc4random() * _base(1) / RAND_MAX;
            buffer_c[j] = arc4random() * _base(1) / RAND_MAX;
        }
        a[i] = _vec(buffer_a);
        b[i] = _vec(buffer_b);
        c[i] = _vec(buffer_c);
        //        printf(", %f", a[i].array.front());
    }
    //    printf("\n");

    [self measureBlock:[a,b,c,sz,&result,&front]() {
        @autoreleasepool {
            for (int i = 0; i < sz[0]; ++i) {
                for (int j = 0; j < sz[0]; ++j) {
                    result[j] = a[j] * b[j] * c[j] / (a[j] + b[j] + c[j]);
                    front[j] = a[j][0] * b[j][0] * c[j][0] / (a[j][0] + b[j][0] + c[j][0]);
                }
            }
        }
    }];

    int incorrect_count = 0;
    bool eq;
    for (int i = 0; i < sz[0] && incorrect_count < 10; ++i) {
        incorrect_count += !(eq = result[i][0] == front[i]);
        XCTAssert(eq, @"result[%d] = %f, front[%d] = %f", i, result[i][0], i, front[i]);
    }
}

@end
