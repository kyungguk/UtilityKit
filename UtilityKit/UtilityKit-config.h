//
//  UtilityKit-config.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#ifndef UtilityKit_config_h
#define UtilityKit_config_h


// root namespace
//
#ifndef UTILITYKIT_NAMESPACE
#define UTILITYKIT_NAMESPACE UTL
#define UTILITYKIT_BEGIN_NAMESPACE namespace UTILITYKIT_NAMESPACE {
#define UTILITYKIT_END_NAMESPACE }
#endif


// unique variable name
//
#ifndef UTILITYKIT_UNIQUE_NAME
#define _UTILITYKIT_UNIQUE_NAME_(x, y) x##y
#define _UTILITYKIT_UNIQUE_NAME(x, y) _UTILITYKIT_UNIQUE_NAME_(x, y)
#define UTILITYKIT_UNIQUE_NAME(base) _UTILITYKIT_UNIQUE_NAME(base, __LINE__)
#endif


// expression-based Vector operations (deprecated)
// TODO: Remove this when version 3 is removed.
//
#ifndef ARRAYKIT_USE_VECTOR_EXPRESSION
#define ARRAYKIT_USE_VECTOR_EXPRESSION 0
#endif


// GCC Deprecated attribute
//
#ifndef UTILITYKIT_DEPRECATED_ATTRIBUTE
#define UTILITYKIT_DEPRECATED_ATTRIBUTE __attribute__ ((deprecated))
#endif


#endif /* UtilityKit_config_h */
