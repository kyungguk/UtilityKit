//
//  UTLArrayNDLeafWalker.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 8/30/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayNDLeafWalker_hh
#define UTLArrayNDLeafWalker_hh

// MARK:- Version 4
//
template <class ArrayND>
UTL::__4_::_ArrayNDLeafWalker<ArrayND const>::_ArrayNDLeafWalker(ArrayND const *A, bool const include_paddings, std::false_type/*begin*/) noexcept
: _A(A), _sizes(A->dims()), _ipath(0) {
    _sizes.front() = 0;
    if (*A && include_paddings) {
        _sizes += ArrayND::pad_size();
        _ipath -= ArrayND::pad_size();
    }
}
template <class ArrayND>
UTL::__4_::_ArrayNDLeafWalker<ArrayND const>::_ArrayNDLeafWalker(ArrayND const *A, bool const include_paddings, std::true_type/*end*/) noexcept
: _A(A), _sizes(A->dims()), _ipath(0) {
    _ipath.front() = _sizes.front();
    _sizes.front() = 0;
    if (*A && include_paddings) {
        _sizes += ArrayND::pad_size();
        _ipath -= ArrayND::pad_size();
        _ipath.front() += 2*ArrayND::pad_size(); // _ipath.rest() is not available when ND == 1
    }
}

template <class ArrayND>
template <long S>
void UTL::__4_::_ArrayNDLeafWalker<ArrayND const>::_incr(Vector<long, S> &ipath) const noexcept
{
    _incr(ipath.rest());
    if (std::get<1>(ipath) >= std::get<ArrayND::rank() - S>(_sizes.rest())) {
        std::get<1>(ipath) = -_sizes.front();
        ++ipath.front();
    }
}
template <class ArrayND>
template <long S>
void UTL::__4_::_ArrayNDLeafWalker<ArrayND const>::_decr(Vector<long, S> &ipath) const noexcept {
    _decr(ipath.rest());
    if (std::get<1>(ipath) < -_sizes.front()) {
        std::get<1>(ipath) = std::get<ArrayND::rank() - S>(_sizes.rest()) - 1;
        --ipath.front();
    }
}

#endif /* UTLArrayNDLeafWalker_hh */
