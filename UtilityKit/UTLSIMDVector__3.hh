//
//  UTLSIMDVector__3.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/28/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMDVector__3_hh
#define UTLSIMDVector__3_hh

// MARK:- Version 4
//

// MARK: Compound Arithmetic
//
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator+=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    **this += *rhs;
    _dummy = one;
    return *this;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator-=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    **this -= *rhs;
    _dummy = one;
    return *this;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator*=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    **this *= *rhs;
    _dummy = one;
    return *this;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator/=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    **this /= *rhs;
    _dummy = one;
    return *this;
}

// MARK: Compound Modulus
//
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator%=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this %= *rhs;
    _dummy = one;
    return *this;
}

// MARK: Compound Bit Operators
//
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator&=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this &= *rhs;
    _dummy = one;
    return *this;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator|=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this |= *rhs;
    _dummy = one;
    return *this;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator^=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this ^= *rhs;
    _dummy = one;
    return *this;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator<<=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this <<= *rhs;
    _dummy = one;
    return *this;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator>>=(SIMDVector const &rhs) noexcept -> SIMDVector &{
    static_assert(std::is_integral<Type>::value, "not an integral type");
    **this >>= *rhs;
    _dummy = one;
    return *this;
}

// MARK: Comparison
//
// If true, all bits are set (i.e., -1)
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator==(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this == *rhs;
    result._dummy = 1;
    return result;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator!=(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this != *rhs;
    result._dummy = 1;
    return result;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator<=(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this <= *rhs;
    result._dummy = 1;
    return result;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator>=(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this >= *rhs;
    result._dummy = 1;
    return result;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator<(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this < *rhs;
    result._dummy = 1;
    return result;
}
template <class Type>
auto UTL::__4_::SIMDVector<Type, 3L>::operator>(SIMDVector const &rhs) const noexcept -> comparison_result_type {
    comparison_result_type result;
    *result = **this > *rhs;
    result._dummy = 1;
    return result;
}

#endif /* UTLSIMDVector__3_hh */
