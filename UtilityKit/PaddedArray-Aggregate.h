//
//  PaddedArray-Aggregate.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/11/15.
//
//

#ifndef ArrayKit__PaddedArray_Aggregate_h
#define ArrayKit__PaddedArray_Aggregate_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <type_traits>
#include <utility>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include <sstream>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
namespace Aggregate {

    // PaddedArray forward declation:
    template <class _Tp, long _Size, long _Padding>
    struct PaddedArray;

    // ND array aliases:
    template <class _Tp, long _Padding, long _Size1>
    using Array_1D = PaddedArray<_Tp, _Size1, _Padding>;
    template <class _Tp, long _Padding, long _Size1, long _Size2>
    using Array_2D = Array_1D<Array_1D<_Tp, _Padding, _Size2>, _Padding, _Size1>;
    template <class _Tp, long _Padding, long _Size1, long _Size2, long _Size3>
    using Array_3D = Array_1D<Array_2D<_Tp, _Padding, _Size2, _Size3>, _Padding, _Size1>;

    // PaddedArray definition:
    template <class _Tp, long _Size, long _Padding>
    struct PaddedArray {
        static_assert(_Size > 0 && _Padding >= 0, "PaddedArray - _Size + 2*_Padding == 0");

        // types:
        //typedef typename std::enable_if<std::is_pod<_Tp>::value, _Tp>::type value_type;
        typedef _Tp     value_type;
        typedef value_type&                           reference;
        typedef const value_type&                     const_reference;
        typedef value_type*                           iterator;
        typedef const value_type*                     const_iterator;
        typedef value_type*                           pointer;
        typedef const value_type*                     const_pointer;
        typedef std::size_t                           size_type;
        typedef std::ptrdiff_t                        difference_type;
        typedef std::reverse_iterator<iterator>       reverse_iterator;
        typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

        value_type __elems_[_Padding + _Size + _Padding];

        // No explicit construct/copy/destroy for aggregate type
        // AGGREGATE INITIALIZATION SHOULD TAKE INTO ACCOUNT PADDINGS AT THE BEGINNING

        // member functions:
        void fill(const value_type& __u) // including the paddings
        { std::fill(__elems_, __elems_ + _Padding+_Size+_Padding, __u); }
        void swap(PaddedArray& o) noexcept(noexcept(std::swap(*__elems_, *__elems_))) // including paddings
        { std::swap_ranges(__elems_, __elems_ + _Padding+_Size+_Padding, o.__elems_); }

        // iterators (excluding padding):
        iterator begin() noexcept // excluding the paddings
        { return iterator(__elems_ + _Padding); }
        const_iterator begin() const noexcept // excluding the paddings
        { return const_iterator(__elems_ + _Padding); }
        iterator end() noexcept // excluding the paddings
        { return iterator(__elems_ + _Padding+_Size); }
        const_iterator end() const noexcept // excluding the paddings
        { return const_iterator(__elems_ + _Padding+_Size); }

        reverse_iterator rbegin() noexcept // excluding the paddings
        { return reverse_iterator(end()); }
        const_reverse_iterator rbegin() const noexcept // excluding the paddings
        { return const_reverse_iterator(end()); }
        reverse_iterator rend() noexcept // excluding the paddings
        { return reverse_iterator(begin()); }
        const_reverse_iterator rend() const noexcept // excluding the paddings
        { return const_reverse_iterator(begin()); }

        // iterators (including padding):
        iterator pad_begin() noexcept // including the paddings
        { return iterator(__elems_); }
        const_iterator pad_begin() const noexcept // including the paddings
        { return const_iterator(__elems_); }
        iterator pad_end() noexcept // including the paddings
        { return iterator(__elems_ + _Padding+_Size+_Padding); }
        const_iterator pad_end() const noexcept // including the paddings
        { return const_iterator(__elems_ + _Padding+_Size+_Padding); }

        reverse_iterator pad_rbegin() noexcept // including the paddings
        { return reverse_iterator(pad_end()); }
        const_reverse_iterator pad_rbegin() const noexcept // including the paddings
        { return const_reverse_iterator(pad_end()); }
        reverse_iterator pad_rend() noexcept // including the paddings
        { return reverse_iterator(pad_begin()); }
        const_reverse_iterator pad_rend() const noexcept // including the paddings
        { return const_reverse_iterator(pad_begin()); }

        // capacity:
        constexpr static size_type pad_size() noexcept // one-side padding size
        { return _Padding; }
        constexpr size_type size() const noexcept // excluding the paddings
        { return _Size; }
        constexpr size_type max_size() const noexcept // including the paddings
        { return _Padding+_Size+_Padding; }
        constexpr bool empty() const noexcept
        { return 0 == _Size; }

        // element access:
#if defined(DEBUG)
        reference operator[](difference_type __n) noexcept { return at(__n); }
        const_reference operator[](difference_type __n) const noexcept { return at(__n); }
#else
        reference operator[](difference_type __n) noexcept // offset by padding
        { return __elems_[_Padding+__n]; }
        const_reference operator[](difference_type __n) const noexcept // offset by padding
        { return __elems_[_Padding+__n]; }
#endif

        reference at(difference_type __n); // offset by padding
        const_reference at(difference_type __n) const; // offset by padding

        reference front() noexcept // offset by padding
        { return __elems_[_Padding]; }
        const_reference front() const noexcept // offset by padding
        { return __elems_[_Padding]; }
        reference back() noexcept // offset by padding
        { return __elems_[_Padding+_Size-1]; }
        const_reference back() const noexcept // offset by padding
        { return __elems_[_Padding+_Size-1]; }

        reference pad_front() noexcept // first padding
        { return *__elems_; }
        const_reference pad_front() const noexcept // first padding
        { return *__elems_; }
        reference pad_back() noexcept // last padding
        { return __elems_[_Padding+_Size+_Padding-1]; }
        const_reference pad_back() const noexcept // last padding
        { return __elems_[_Padding+_Size+_Padding-1]; }

        value_type* data() noexcept // the first padding
        { return __elems_; }
        const value_type* data() const noexcept // the first padding
        { return __elems_; }
    };

    // out-of-line definitions:
    template <class _Tp, long _Size, long _Padding>
    auto PaddedArray<_Tp, _Size, _Padding>::at(difference_type __n)
    -> reference
    {
        if ( (difference_type(_Padding)+__n < 0) || (__n >= _Size+_Padding) )
        { throw std::out_of_range(__PRETTY_FUNCTION__); }
        return __elems_[_Padding+__n];
    }
    template <class _Tp, long _Size, long _Padding>
    auto PaddedArray<_Tp, _Size, _Padding>::at(difference_type __n) const
    -> const_reference
    {
        if ( (difference_type(_Padding)+__n < 0) || (__n >= _Size+_Padding) )
        { throw std::out_of_range(__PRETTY_FUNCTION__); }
        return __elems_[_Padding+__n];
    }

    // global operators:
    // COMPARISONS INCLUDE PADDING
    template <class _Tp, long _Size, long _Padding>
    inline bool operator==(const PaddedArray<_Tp, _Size, _Padding>& x, const PaddedArray<_Tp, _Size, _Padding>& y)
    {
        return std::equal(x.__elems_, x.__elems_ + _Padding+_Size+_Padding, y.__elems_);
    }
    template <class _Tp, long _Size, long _Padding>
    inline bool operator!=(const PaddedArray<_Tp, _Size, _Padding>& x, const PaddedArray<_Tp, _Size, _Padding>& y)
    {
        return !(x == y);
    }
    template <class _Tp, long _Size, long _Padding>
    inline bool operator<(const PaddedArray<_Tp, _Size, _Padding>& x, const PaddedArray<_Tp, _Size, _Padding>& y)
    {
        return std::lexicographical_compare(x.__elems_, x.__elems_ + _Padding+_Size+_Padding, y.__elems_, y.__elems_ + _Padding+_Size+_Padding);
    }
    template <class _Tp, long _Size, long _Padding>
    inline bool operator>(const PaddedArray<_Tp, _Size, _Padding>& x, const PaddedArray<_Tp, _Size, _Padding>& y)
    {
        return y < x;
    }
    template <class _Tp, long _Size, long _Padding>
    inline bool operator<=(const PaddedArray<_Tp, _Size, _Padding>& x, const PaddedArray<_Tp, _Size, _Padding>& y)
    {
        return !(y < x);
    }
    template <class _Tp, long _Size, long _Padding>
    inline bool operator>=(const PaddedArray<_Tp, _Size, _Padding>& x, const PaddedArray<_Tp, _Size, _Padding>& y)
    {
        return !(x < y);
    }

    // output stream:
    template<class _CharT, class _Traits, class _Tp, long _Size, long _Pad>
    std::basic_ostream<_CharT, _Traits>&
    operator<<(std::basic_ostream<_CharT, _Traits>& __os, PaddedArray<_Tp, _Size, _Pad> const& __a)
    {
        std::basic_ostringstream<_CharT, _Traits> __s; {
            __s.flags(__os.flags());
            __s.imbue(__os.getloc());
            __s.precision(__os.precision());
        }
        long i = -_Pad;
        // left paddings:
        { __s << '{' << __a[i++]; while (i < 0         ) __s << ", " << __a[i++]; __s << '|'; }
        // content:
        { __s <<        __a[i++]; while (i < _Size     ) __s << ", " << __a[i++]; }
        // right paddings:
        { __s << '|' << __a[i++]; while (i < _Size+_Pad) __s << ", " << __a[i++]; __s << '}'; }
        return __os << __s.str();
    }
    template<class _CharT, class _Traits, class _Tp, long _Size>
    std::basic_ostream<_CharT, _Traits>&
    operator<<(std::basic_ostream<_CharT, _Traits>& __os, PaddedArray<_Tp, _Size, 0> const& __a)
    {
        std::basic_ostringstream<_CharT, _Traits> __s; {
            __s.flags(__os.flags());
            __s.imbue(__os.getloc());
            __s.precision(__os.precision());
        }
        long i = 0;
        { __s << '{' << __a[i++]; while (i < _Size) __s << ", " << __a[i++]; __s << '}'; }
        return __os << __s.str();
    }

} // namespace Aggregate
} // namespace __3_
UTILITYKIT_END_NAMESPACE

// std::swap:
namespace std {
    template <class _Tp, long _Size, long _Padding> inline
    void swap(UTILITYKIT_NAMESPACE::__3_::Aggregate::PaddedArray<_Tp, _Size, _Padding>& x,
              UTILITYKIT_NAMESPACE::__3_::Aggregate::PaddedArray<_Tp, _Size, _Padding>& y) noexcept(noexcept(x.swap(y))) {
        x.swap(y);
    }
}

#endif /* ArrayKit__PaddedArray_Aggregate_h */
