//
//  UTLParallelMaxwellianDistribution.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLParallelMaxwellianDistribution.h"

// MARK: Version 3
//
#include <stdexcept>
#include <utility>
#include <cmath>

#pragma mark helpers
namespace {
    auto gaussian_pdf(UTL::__3_::ParallelMaxwellianDistribution::value_type x)
    -> UTL::__3_::ParallelMaxwellianDistribution::value_type {
        return std::exp(-x*x) * M_2_SQRTPI/2;
    }
    auto gaussian_cdf(UTL::__3_::ParallelMaxwellianDistribution::value_type x)
    -> UTL::__3_::ParallelMaxwellianDistribution::value_type {
        return std::erfc(-x) / 2;
    }
}

#pragma mark class implementation
UTL::__3_::ParallelMaxwellianDistribution::ParallelMaxwellianDistribution(value_type vth, value_type vd, bool should_normalize_cdf)
: Distribution(), _icdf(), _vth(vth), _vd(vd) {
    if (_vth <= 0) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }

    // sampler:
    sampler_type sampler = this->_sampler();

    // sample cdf:
    constexpr value_type x_max = 5;
    sampler_type::point_list_type
    cdf_pts = sampler(&::gaussian_cdf, -x_max, x_max, 1);

    // optional normalization:
    if (should_normalize_cdf) {
        // normalize cdf:
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        sampler_type::point_list_type::iterator
        first = cdf_pts.begin(), last = cdf_pts.end();
        for (; first != last; ++first) {
            first->second = (first->second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    sampler_type::point_list_type::iterator
    first = cdf_pts.begin(), last = cdf_pts.end();
    for (; first != last; ++first) {
        std::swap(first->second, first->first);
    }
    _icdf = interp_type(cdf_pts.begin(), cdf_pts.end());
}

UTL::__3_::ParallelMaxwellianDistribution::ParallelMaxwellianDistribution(ParallelMaxwellianDistribution const &o) noexcept
: Distribution(), _icdf(o._icdf), _vth(o._vth), _vd(o._vd) {
}
auto UTL::__3_::ParallelMaxwellianDistribution::operator=(ParallelMaxwellianDistribution const &o) noexcept
-> ParallelMaxwellianDistribution &{
    if (this != &o) {
        _icdf = o._icdf;
        _vth = o._vth;
        _vd = o._vd;
    }
    return *this;
}
UTL::__3_::ParallelMaxwellianDistribution::ParallelMaxwellianDistribution(ParallelMaxwellianDistribution &&o) noexcept
: Distribution(), _icdf(std::move(o._icdf)), _vth(std::move(o._vth)), _vd(std::move(o._vd)) {
}
auto UTL::__3_::ParallelMaxwellianDistribution::operator=(ParallelMaxwellianDistribution &&o) noexcept
-> ParallelMaxwellianDistribution &{
    if (this != &o) {
        _icdf = std::move(o._icdf);
        _vth = std::move(o._vth);
        _vd = std::move(o._vd);
    }
    return *this;
}

auto UTL::__3_::ParallelMaxwellianDistribution::pdf(value_type v1) const
-> value_type {
    value_type const x = (v1 - _vd) / _vth;
    return ::gaussian_pdf(x) / _vth;
}
auto UTL::__3_::ParallelMaxwellianDistribution::cdf(value_type v1) const
-> value_type {
    value_type const x = (v1 - _vd) / _vth;
    return ::gaussian_cdf(x);
}
auto UTL::__3_::ParallelMaxwellianDistribution::icdf(value_type cdf) const
-> value_type {
    return _icdf(cdf)*_vth + _vd;
}

auto UTL::__3_::ParallelMaxwellianDistribution::_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.maxRecursion = 20;
        sampler.accuracyGoal = 7;
        sampler.initialPoints = 100;
        sampler.yScaleAbsoluteTolerance = 1e-10;
    }
    return sampler;
}


// MARK:- Version 4
//
#include <stdexcept>
#include <utility>
#include <string>
#include <cmath>

UTL::__4_::ParallelMaxwellianDistribution::ParallelMaxwellianDistribution(ParallelMaxwellianDistribution const &o)
: Distribution(), _vth(o._vth), _vd(o._vd), _icdf(o._icdf) {
}
UTL::__4_::ParallelMaxwellianDistribution::ParallelMaxwellianDistribution(ParallelMaxwellianDistribution &&o)
: Distribution(), _vth(o._vth), _vd(o._vd), _icdf() {
    _icdf = std::move(o._icdf);
}
auto UTL::__4_::ParallelMaxwellianDistribution::operator=(ParallelMaxwellianDistribution const &o)
-> ParallelMaxwellianDistribution &{
    if (this != &o) {
        ParallelMaxwellianDistribution{o}.swap(*this);
    }
    return *this;
}
auto UTL::__4_::ParallelMaxwellianDistribution::operator=(ParallelMaxwellianDistribution &&o)
-> ParallelMaxwellianDistribution &{
    if (this != &o) {
        ParallelMaxwellianDistribution{std::move(o)}.swap(*this);
    }
    return *this;
}

void UTL::__4_::ParallelMaxwellianDistribution::swap(ParallelMaxwellianDistribution &o)
{
    _icdf.swap(o._icdf);
    std::swap(_vth, o._vth);
    std::swap(_vd, o._vd);
}

auto UTL::__4_::ParallelMaxwellianDistribution::pdf(value_type const v1) const noexcept
-> value_type {
    value_type const x = (v1 - _vd) / _vth;
    return gaussian_pdf(x) / _vth;
}
auto UTL::__4_::ParallelMaxwellianDistribution::cdf(value_type const v1) const noexcept
-> value_type {
    value_type const x = (v1 - _vd) / _vth;
    return gaussian_cdf(x);
}
auto UTL::__4_::ParallelMaxwellianDistribution::icdf(value_type const cdf) const
-> value_type {
    try {
        return _vd + _vth*_icdf(cdf)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

auto UTL::__4_::ParallelMaxwellianDistribution::default_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.set_max_recursion(20);
        sampler.set_accuracy_goal(7);
        sampler.set_initial_points(100);
        sampler.set_y_scale_absolute_tolerance(1e-10);
    }
    return sampler;
}

UTL::__4_::ParallelMaxwellianDistribution::ParallelMaxwellianDistribution(value_type const vth, value_type const vd)
: Distribution(), _vth(vth), _vd(vd), _icdf() {
    if (_vth <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative vth");
    }

    // sampler:
    sampler_type const sampler = default_sampler();

    // sample cdf:
    constexpr value_type x_max = 5;
    sampler_type::point_list_type
    cdf_pts = sampler(&gaussian_cdf, -x_max, x_max, 1);

    // re-normalization:
    {
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        for (auto &pt : cdf_pts) {
            pt.second = (pt.second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    for (auto &pt : cdf_pts) {
        std::swap(pt.first, pt.second);
    }
    this->_icdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
}

auto UTL::__4_::ParallelMaxwellianDistribution::gaussian_pdf(value_type const x) noexcept
-> value_type {
    return std::exp(-x*x) * M_2_SQRTPI/2;
}
auto UTL::__4_::ParallelMaxwellianDistribution::gaussian_cdf(value_type const x) noexcept
-> value_type {
    return std::erfc(-x) / 2;
}
