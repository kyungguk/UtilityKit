//
//  PaddedArray-Dynamic.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/4/15.
//
//

#ifndef ArrayKit__PaddedArray_Dynamic_h
#define ArrayKit__PaddedArray_Dynamic_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/PaddedArray-Allocator.h>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include <initializer_list>
#include <sstream>
#include <string>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
inline namespace Dynamic {

#pragma mark - struct PaddedArray
    /**
     @brief Fixed-length array with dynamic storage allocation.
     @discussion Note that the array is not an aggregate of values as the static array does.

     Unlike std::vector, the allocator can have state.
     */
    template <class _Tp, long _Padding, class _Alloc = OndemandAllocator<_Tp> >
    struct PaddedArray {
        static_assert(_Padding >= 0, "PaddedArray - _Padding == 0");

        // types:
        typedef _Alloc allocator_type;
        typedef _Tp                                   value_type;
        typedef value_type&                           reference;
        typedef const value_type&                     const_reference;
        typedef value_type*                           iterator;
        typedef const value_type*                     const_iterator;
        typedef value_type*                           pointer;
        typedef const value_type*                     const_pointer;
        typedef std::size_t                           size_type;
        typedef std::ptrdiff_t                        difference_type;
        typedef std::reverse_iterator<iterator>       reverse_iterator;
        typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    private:
        pointer __beg_, __end_; // excluding padding
        allocator_type __alloc_;

    public:
        // destructor:
        ~PaddedArray();

        /**
         @brief Construct an invalid array.
         @discussion It's the only instance constructed this way that bool cast returns false.
         */
        explicit PaddedArray(allocator_type a=_Alloc()) noexcept(noexcept(_Alloc(a))) : __beg_(), __end_(), __alloc_(a) {}

        /**
         @brief Construct a fixed-sized array whose elements including the paddings are initialized with the default value.
         */
        explicit PaddedArray(size_type sz, allocator_type a=_Alloc());
        /**
         @brief Construct a fixed-sized array whose elements including the paddings are initialized with the given value.
         */
        explicit PaddedArray(size_type sz, const value_type &x, allocator_type a=_Alloc());

        /**
         @brief Construct a fixed-sized array from an iterator.
         @discussion The paddings are excluded from the iterator, and are default value initialized.
         */
        template <class _It>
        PaddedArray(_It first, _It last, allocator_type a=_Alloc());
        /**
         @brief Construct a fixed-sized array from an initializer list.
         @discussion Effectively calls the constructor with iterator as its argument.
         */
        PaddedArray(std::initializer_list<value_type> il, allocator_type a=_Alloc()) : PaddedArray(il.begin(), il.end(), a) {}

        // copy/move construct:
        PaddedArray(PaddedArray const& o) : PaddedArray(o, o.__alloc_) {}
        PaddedArray(PaddedArray const& o, allocator_type a);
        PaddedArray(PaddedArray&& o) noexcept(noexcept(_Alloc(std::move(__alloc_))));

        /**
         @brief Element-wise copy assignment.
         @discussion *this and other should be a valid array and of the same size.
         */
        PaddedArray& operator=(PaddedArray const& o);
        /**
         @brief Element-wise move assignment.
         @discussion If bool(*this) is false, effectively swap is called with the other array as the arguemnt.
         Otherwise, the other array should be a valid array and of the same size.
         */
        PaddedArray& operator=(PaddedArray&& o);

        /**
         @brief Element-wise copy assignment of different type of array.
         @discussion *this and other should be a valid array and of the same size.
         The paddings will be copied by the lesser number of two paddings.
         */
        template <class T, class Alloc, long Padding>
        PaddedArray& operator=(PaddedArray<T, Padding, Alloc> const& o);
        /**
         @brief Element-wise move assignment of different type of array.
         @discussion *this and other should be a valid array and of the same size.
         The paddings will be moved by the lesser number of two paddings.
         */
        template <class T, class Alloc, long Padding>
        PaddedArray& operator=(PaddedArray<T, Padding, Alloc>&& o);

        // observers:
        /**
         @brief Returns false if *this has been constructed with PaddedArray(allocator_type).
         */
        explicit operator bool() const noexcept { return __beg_; }
        /**
         @brief Returns the allocator.
         */
        allocator_type get_allocator() const noexcept { return __alloc_; }

        // modifiers:
        /**
         Filling includes paddings.
         */
        void fill(const value_type& __u) { std::fill(__beg_-_Padding, __end_+_Padding, __u); }
        void swap(PaddedArray& o) noexcept(noexcept(std::swap(__alloc_, o.__alloc_)));

        // iterators (excluding padding):
        iterator begin() noexcept // excluding the paddings
        { return iterator(__beg_); }
        const_iterator begin() const noexcept // excluding the paddings
        { return const_iterator(__beg_); }
        iterator end() noexcept // excluding the paddings
        { return iterator(__end_); }
        const_iterator end() const noexcept // excluding the paddings
        { return const_iterator(__end_); }

        reverse_iterator rbegin() noexcept // excluding the paddings
        { return reverse_iterator(end()); }
        const_reverse_iterator rbegin() const noexcept // excluding the paddings
        { return const_reverse_iterator(end()); }
        reverse_iterator rend() noexcept // excluding the paddings
        { return reverse_iterator(begin()); }
        const_reverse_iterator rend() const noexcept // excluding the paddings
        { return const_reverse_iterator(begin()); }

        // iterators (including padding):
        iterator pad_begin() noexcept // excluding the paddings
        { return iterator(__beg_-_Padding); }
        const_iterator pad_begin() const noexcept // excluding the paddings
        { return const_iterator(__beg_-_Padding); }
        iterator pad_end() noexcept // excluding the paddings
        { return iterator(__end_+_Padding); }
        const_iterator pad_end() const noexcept // excluding the paddings
        { return const_iterator(__end_+_Padding); }

        reverse_iterator pad_rbegin() noexcept // excluding the paddings
        { return reverse_iterator(pad_end()); }
        const_reverse_iterator pad_rbegin() const noexcept // excluding the paddings
        { return const_reverse_iterator(pad_end()); }
        reverse_iterator pad_rend() noexcept // excluding the paddings
        { return reverse_iterator(pad_begin()); }
        const_reverse_iterator pad_rend() const noexcept // excluding the paddings
        { return const_reverse_iterator(pad_begin()); }

        // capacity:
        constexpr static size_type pad_size() noexcept // one-side padding size
        { return _Padding; }
        size_type size() const noexcept // excluding the paddings
        { return size_type(__end_ - __beg_); }
        size_type max_size() const noexcept // including the paddings
        { return _Padding+size()+_Padding; }
        bool empty() const noexcept
        { return __beg_ == __end_; }

        // element access:
#if defined(DEBUG)
        reference operator[](difference_type __n) noexcept { return at(__n); }
        const_reference operator[](difference_type __n) const noexcept { return at(__n); }
#else
        reference operator[](difference_type __n) noexcept // offset by padding
        { return *(__beg_+__n); }
        const_reference operator[](difference_type __n) const noexcept // offset by padding
        { return *(__beg_+__n); }
#endif

        reference at(difference_type __n); // offset by padding
        const_reference at(difference_type __n) const; // offset by padding

        reference front() noexcept // offset by padding
        { return *__beg_; }
        const_reference front() const noexcept // offset by padding
        { return *__beg_; }
        reference back() noexcept // offset by padding
        { return *(__end_-1); }
        const_reference back() const noexcept // offset by padding
        { return *(__end_-1); }

        reference pad_front() noexcept // offset by padding
        { return *(__beg_-_Padding); }
        const_reference pad_front() const noexcept // offset by padding
        { return *(__beg_-_Padding); }
        reference pad_back() noexcept // offset by padding
        { return *(__end_+_Padding-1); }
        const_reference pad_back() const noexcept // offset by padding
        { return *(__end_+_Padding-1); }

        value_type* data() noexcept // the first padding
        { return __beg_-_Padding; }
        const value_type* data() const noexcept // the first padding
        { return __beg_-_Padding; }
    };

#pragma mark - Out-of-line Implementation
#pragma mark ~PaddedArray()
    template <class _Tp, long _Padding, class _Alloc>
    PaddedArray<_Tp, _Padding, _Alloc>::~PaddedArray() {
        if (!__beg_) { return; }
        __beg_ -= _Padding, __end_ += _Padding;
        for (pointer p = __beg_; p != __end_; ) { __alloc_.destroy(p++); }
        __alloc_.deallocate(__beg_, max_size());
    }

#pragma mark PaddedArray(size_type, allocator_type)
    template <class _Tp, long _Padding, class _Alloc>
    PaddedArray<_Tp, _Padding, _Alloc>::PaddedArray(size_type sz, allocator_type a)
    : __beg_(), __end_(), __alloc_(a) {
        __end_ = __beg_ = __alloc_.allocate(_Padding + sz + _Padding);
        if (!__beg_) { throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - nil buffer pointer"); }
        try {
            __end_ += _Padding + sz + _Padding;
            for (pointer p = __beg_; p != __end_; ) { __alloc_.construct(p++, value_type()); }
        } catch (...) {
            __alloc_.deallocate(__beg_, _Padding + sz + _Padding);
            throw;
        }
        __beg_ += _Padding, __end_ -= _Padding;
    }
#pragma mark PaddedArray(size_type, const value_type &, allocator_type)
    template <class _Tp, long _Padding, class _Alloc>
    PaddedArray<_Tp, _Padding, _Alloc>::PaddedArray(size_type sz, const value_type &x, allocator_type a)
    : __beg_(), __end_(), __alloc_(a) {
        __end_ = __beg_ = __alloc_.allocate(_Padding + sz + _Padding);
        if (!__beg_) { throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - nil buffer pointer"); }
        try {
            __end_ += _Padding + sz + _Padding;
            for (pointer p = __beg_; p != __end_; ) { __alloc_.construct(p++, x); }
        } catch (...) {
            __alloc_.deallocate(__beg_, _Padding + sz + _Padding);
            throw;
        }
        __beg_ += _Padding, __end_ -= _Padding;
    }
#pragma mark PaddedArray(first, last, allocator_type)
    template <class _Tp, long _Padding, class _Alloc>
    template <class _It>
    PaddedArray<_Tp, _Padding, _Alloc>::PaddedArray(_It first, _It last, allocator_type a)
    : __beg_(), __end_(), __alloc_(a) {
        const difference_type _sz = std::distance(first, last);
        if (_sz < 0) { throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - distance < 0"); }
        const size_type sz = size_type(_sz);
        __end_ = __beg_ = __alloc_.allocate(_Padding + sz + _Padding);
        if (!__beg_) { throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - nil buffer pointer"); }
        try {
            pointer p = __beg_;
            // left padding:
            __end_ += _Padding;
            for (; p != __end_; ) { __alloc_.construct(p++, value_type()); }
            // content:
            __end_ += sz;
            for (; p != __end_; ) { __alloc_.construct(p++, *first++); }
            // right padding:
            __end_ += _Padding;
            for (; p != __end_; ) { __alloc_.construct(p++, value_type()); }
        } catch (...) {
            __alloc_.deallocate(__beg_, _Padding + sz + _Padding);
            throw;
        }
        __beg_ += _Padding, __end_ -= _Padding;
    }
#pragma mark PaddedArray(PaddedArray const&, allocator_type)
    template <class _Tp, long _Padding, class _Alloc>
    PaddedArray<_Tp, _Padding, _Alloc>::PaddedArray(PaddedArray const& o, allocator_type a)
    : __beg_(), __end_(), __alloc_(a) {
        if (!o) { return; } // instead of throw, because allocator uses it.
        const size_type sz = o.size();
        __end_ = __beg_ = __alloc_.allocate(_Padding + sz + _Padding);
        if (!__beg_) { throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - nil buffer pointer"); }
        try {
            __end_ += _Padding + sz + _Padding;
            for (pointer p = __beg_, q = o.__beg_-_Padding; p != q && p != __end_; ) { __alloc_.construct(p++, *q++); }
        } catch (...) {
            __alloc_.deallocate(__beg_, _Padding + sz + _Padding);
            throw;
        }
        __beg_ += _Padding, __end_ -= _Padding;
    }
#pragma mark PaddedArray(PaddedArray&&)
    template <class _Tp, long _Padding, class _Alloc>
    PaddedArray<_Tp, _Padding, _Alloc>::PaddedArray(PaddedArray&& o) noexcept(noexcept(_Alloc(std::move(__alloc_))))
    : __beg_(o.__beg_), __end_(o.__end_), __alloc_(std::move(o.__alloc_)) {
        o.__end_ = o.__beg_ = nullptr;
    }

#pragma mark operator=(PaddedArray const&)
    template <class _Tp, long _Padding, class _Alloc>
    auto PaddedArray<_Tp, _Padding, _Alloc>::operator=(PaddedArray const& o)
    -> PaddedArray& {
        if (&o != this) {
            if (!*this) { throw std::domain_error(__PRETTY_FUNCTION__); }
            if (!o || o.size() != size()) { throw std::invalid_argument(__PRETTY_FUNCTION__); }
            std::copy(o.__beg_-_Padding, o.__end_+_Padding, __beg_-_Padding);
        }
        return *this;
    }
#pragma mark operator=(PaddedArray&&)
    template <class _Tp, long _Padding, class _Alloc>
    auto PaddedArray<_Tp, _Padding, _Alloc>::operator=(PaddedArray&& o)
    -> PaddedArray& {
        if (&o != this) {
            if (!*this) { swap(o); }
            else if (!o || o.size() != size()) { throw std::invalid_argument(__PRETTY_FUNCTION__); }
            else { std::swap_ranges(o.__beg_-_Padding, o.__end_+_Padding, __beg_-_Padding); }
        }
        return *this;
    }
#pragma mark operator=(PaddedArray<T, Padding, Alloc> const& o)
    template <class _Tp, long _Padding, class _Alloc>
    template <class T, class Alloc, long Padding>
    auto PaddedArray<_Tp, _Padding, _Alloc>::operator=(PaddedArray<T, Padding, Alloc> const& o)
    -> PaddedArray& {
        if (!*this) { throw std::domain_error(__PRETTY_FUNCTION__); }
        if (!o || o.size() != size()) { throw std::invalid_argument(__PRETTY_FUNCTION__); }
        // copy content:
        std::copy(o.begin(), o.end(), begin());
        // copy paddings:
        difference_type const pad = std::min(_Padding, Padding);
        if (pad) {
            std::copy(o.rend(), o.rend()+pad, rend()); // left padding
            std::copy(o. end(), o. end()+pad,  end()); // right padding
        }
        return *this;
    }
#pragma mark operator=(PaddedArray<T, Padding, Alloc>&&)
    template <class _Tp, long _Padding, class _Alloc>
    template <class T, class Alloc, long Padding>
    auto PaddedArray<_Tp, _Padding, _Alloc>::operator=(PaddedArray<T, Padding, Alloc>&& o)
    -> PaddedArray& {
        if (!*this) { throw std::domain_error(__PRETTY_FUNCTION__); }
        if (!o || o.size() != size()) { throw std::invalid_argument(__PRETTY_FUNCTION__); }
        // move content:
        std::swap_ranges(o.begin(), o.end(), begin());
        // move paddings:
        difference_type const pad = std::min(_Padding, Padding);
        if (pad) {
            std::swap_ranges(o.rend(), o.rend()+pad, rend()); // left padding
            std::swap_ranges(o. end(), o. end()+pad,  end()); // right padding
        }
        return *this;
    }

#pragma mark swap(PaddedArray&)
    template <class _Tp, long _Padding, class _Alloc>
    void PaddedArray<_Tp, _Padding, _Alloc>::swap(PaddedArray& o) noexcept(noexcept(std::swap(__alloc_, o.__alloc_)))
    {
        std::swap(__beg_, o.__beg_);
        std::swap(__end_, o.__end_);
        std::swap(__alloc_, o.__alloc_);
    }

#pragma mark at(difference_type)
    template <class _Tp, long _Padding, class _Alloc>
    auto PaddedArray<_Tp, _Padding, _Alloc>::at(difference_type __n)
    -> reference {
        if (!*this) { throw std::invalid_argument(__PRETTY_FUNCTION__); }
        pointer p = __beg_ + __n;
        if (difference_type(_Padding)+__n < 0 || p >= __end_+_Padding) { throw std::out_of_range(__PRETTY_FUNCTION__); }
        return *p;
    }
#pragma mark at(difference_type) const
    template <class _Tp, long _Padding, class _Alloc>
    auto PaddedArray<_Tp, _Padding, _Alloc>::at(difference_type __n) const
    -> const_reference {
        if (!*this) { throw std::invalid_argument(__PRETTY_FUNCTION__); }
        pointer p = __beg_ + __n;
        if (difference_type(_Padding)+__n < 0 || p >= __end_+_Padding) { throw std::out_of_range(__PRETTY_FUNCTION__); }
        return *p;
    }

#pragma mark - Array Comparison
    // COMPARISONS INCLUDE PADDING
    template <class _Tp, long _Padding, class _Alloc> inline
    bool operator==(const PaddedArray<_Tp, _Padding, _Alloc>& x, const PaddedArray<_Tp, _Padding, _Alloc>& y)
    {
        if (!x ^ !y) { return false; }
        return !x ? true :
        x.size() == y.size() && std::equal(x.begin()-_Padding, x.end()+_Padding, y.begin()-_Padding);
    }
    template <class _Tp, long _Padding, class _Alloc> inline
    bool operator!=(const PaddedArray<_Tp, _Padding, _Alloc>& x, const PaddedArray<_Tp, _Padding, _Alloc>& y)
    {
        return !(x == y);
    }
    template <class _Tp, long _Padding, class _Alloc> inline
    bool operator<(const PaddedArray<_Tp, _Padding, _Alloc>& x, const PaddedArray<_Tp, _Padding, _Alloc>& y)
    {
        if (!x ^ !y) { return false; }
        return !x ? false :
        std::lexicographical_compare(x.begin()-_Padding, x.end()+_Padding, y.begin()-_Padding, y.end()+_Padding);
    }
    template <class _Tp, long _Padding, class _Alloc> inline
    bool operator>(const PaddedArray<_Tp, _Padding, _Alloc>& x, const PaddedArray<_Tp, _Padding, _Alloc>& y)
    {
        return y < x;
    }
    template <class _Tp, long _Padding, class _Alloc> inline
    bool operator<=(const PaddedArray<_Tp, _Padding, _Alloc>& x, const PaddedArray<_Tp, _Padding, _Alloc>& y)
    {
        return !(y < x);
    }
    template <class _Tp, long _Padding, class _Alloc> inline
    bool operator>=(const PaddedArray<_Tp, _Padding, _Alloc>& x, const PaddedArray<_Tp, _Padding, _Alloc>& y)
    {
        return !(x < y);
    }

#pragma mark - Formatted Output
    template<class _CharT, class _Traits, class _Tp, long _Pad, class _Alloc>
    std::basic_ostream<_CharT, _Traits>&
    operator<<(std::basic_ostream<_CharT, _Traits>& __os, PaddedArray<_Tp, _Pad, _Alloc> const& __a)
    {
        std::basic_ostringstream<_CharT, _Traits> __s; {
            __s.flags(__os.flags());
            __s.imbue(__os.getloc());
            __s.precision(__os.precision());
        }
        if (!__a) {
            return __os << "NULL";
        }
        long i = -_Pad, sz = long(__a.size());
        // left paddings:
        { __s << '{' << __a[i++]; while (i < 0) __s << ", " << __a[i++]; __s << '|'; }
        // content:
        if (sz) { __s << __a[i++]; while (i < sz) __s << ", " << __a[i++]; }
        // right paddings:
        { __s << '|' << __a[i++]; while (i < sz+_Pad) __s << ", " << __a[i++]; __s << '}'; }
        return __os << __s.str();
    }
    template<class _CharT, class _Traits, class _Tp, class _Alloc>
    std::basic_ostream<_CharT, _Traits>&
    operator<<(std::basic_ostream<_CharT, _Traits>& __os, PaddedArray<_Tp, 0, _Alloc> const& __a)
    {
        std::basic_ostringstream<_CharT, _Traits> __s; {
            __s.flags(__os.flags());
            __s.imbue(__os.getloc());
            __s.precision(__os.precision());
        }
        if (!__a) {
            return __os << "NULL";
        }
        long i = 0, sz = long(__a.size());
        __s << '{';
        if (sz) { __s << __a[i++]; while (i < sz) __s << ", " << __a[i++]; }
        __s << '}';
        return __os << __s.str();
    }

} // namespace Dynamic
} // namespace __3_
UTILITYKIT_END_NAMESPACE

#pragma mark - std::swap
namespace std {
    template <class _Tp, long _Padding, class _Alloc> inline
    void swap(UTILITYKIT_NAMESPACE::__3_::Dynamic::PaddedArray<_Tp, _Padding, _Alloc>& x,
              UTILITYKIT_NAMESPACE::__3_::Dynamic::PaddedArray<_Tp, _Padding, _Alloc>& y) noexcept(noexcept(x.swap(y))) {
        x.swap(y);
    }
}

#endif /* ArrayKit__PaddedArray_Dynamic_h */
