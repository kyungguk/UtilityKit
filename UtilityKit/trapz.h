//
//  trapz.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/21/14.
//
//

#ifndef AlgorithmKit__trapz_h
#define AlgorithmKit__trapz_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <cmath>
#include <utility>
#include <stdexcept>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
namespace trapz {
    namespace {

        using index_type = unsigned; //!< Index integer type.
        constexpr index_type maximum_iterations = 20; //!< Maximum iterations.


        // helpers
        //
        template<class F, class T>
        inline auto _trapzd(F&& f, std::pair<T, T> const& ab) -> std::pair<T, index_type>
        {
            return {T(.5)*(ab.second-ab.first)*(f(ab.first)+f(ab.second)), 1};
        }
        template<class F, class T>
        inline auto _trapzd(F&& f, std::pair<T, T> const& ab, T const& sold, index_type const& n) -> std::pair<T, index_type>
        {
            T sumf = T();
            T const dx = (ab.second-ab.first)/T(n);
            for (index_type i=0; i<n; i++) {
                T const x = ab.first + (T(i)+T(.5))*dx;
                sumf += f(x);
            }

            return {T(.5)*(sold + dx*sumf), 2*n};
        }


        /*!
         @brief Trapezoidal sum.
         @param _f Function pointer or functor that implements operator()(double x)->double.
         @param ab A pair of integral interval.
         @param minIter Minimum number of iteration beyond which integration terminates by the convergence test.
         @param reltol Relative tolerance.
         @param abstol Absolute tolerance.
         @return A pair of integrated value and number of iterations.
         */
        template<class F, class T>
        inline auto qtrap(F&& _f, std::pair<T, T> const& ab, unsigned const minIter, T const reltol=T(1e-7), T const abstol=T(1e-10)) -> std::pair<T, index_type>
        {
            if (reltol<=T() || abstol<=T()) {
                throw std::invalid_argument(__PRETTY_FUNCTION__);
            }
            F f = std::forward<F>(_f); // safeguard: If F is rvalue ref, move construction occur.

            auto sn = _trapzd(f, ab);
            index_type it;
            for (it=1; it<minIter; it++) {
                sn = _trapzd(f, ab, sn.first, sn.second);
            }
            for (; it<=maximum_iterations; it++) {
                auto sold = sn.first;
                sn = _trapzd(f, ab, sn.first, sn.second);

                if (!std::isfinite(sn.first) ||
                    std::abs(sold)<abstol ||
                    reltol*std::abs(sold)>std::abs(sold-sn.first)
                    ) break;
            }
            //if (maximum_iterations<it) {
            //throw Conv();
            //}

            return sn;
        }

        /*!
         @brief Trapezoidal sum by the Simpson rule.
         @param _f Function pointer or functor that implements operator()(double x)->double.
         @param ab A pair of integral interval.
         @param minIter Minimum number of iteration beyond which integration terminates by the convergence test.
         @param reltol Relative tolerance.
         @param abstol Absolute tolerance.
         @return A pair of integrated value and number of iterations.
         */
        template<class F, class T>
        inline auto qsimp(F&& _f, std::pair<T, T> const& ab, unsigned const minIter, T const reltol=T(1e-7), T const abstol=T(1e-10)) -> std::pair<T, index_type>
        {
            if (reltol<=T() || abstol<=T()) {
                throw std::invalid_argument(__PRETTY_FUNCTION__);
            }
            F f = std::forward<F>(_f); // safeguard: If F is rvalue ref, move construction occur.

            auto stn = _trapzd(f, ab);
            auto s = stn.first;
            index_type it;
            // FIXME: Implementation is erroneous. See UTL::__4_::TrapzIntergrator::qsimp.
            for (it=1; it<minIter; it++) {
                stn = _trapzd(f, ab, stn.first, stn.second);
                s = (T(4.)*stn.first - s)/T(3.);
            }
            for (; it<=maximum_iterations; it++) {
                auto stold = stn.first;
                stn = _trapzd(f, ab, stn.first, stn.second);

                auto sold = s;
                s = (T(4.)*stn.first - stold)/T(3.);

                if (!std::isfinite(s) ||
                    std::abs(sold)<abstol ||
                    reltol*std::abs(sold)>std::abs(sold-s)
                    ) break;
            }
            //if (maximum_iterations<it) {
            //throw Conv();
            //}

            return {s, stn.second};
        }

    } // anonymous namespace
} // trapz
} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* defined(AlgorithmKit__trapz_h) */
