//
//  UTLOptional.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/29/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLOptional_h
#define UTLOptional_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <type_traits>
#include <utility>
#include <exception>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Optional wrapper simplified from c++17 std::optional.
     */
    template <typename T>
    class Optional {
        T *_v;
        char _storage[sizeof(T)];

    public:
        //types:
        using value_type = T; //!< Type of the associated value.
        class nil_access; //!< nil value access exception.

        // destructor:
        ~Optional() { if (*this) _v->T::~T(), _v = nullptr; }

        // constructor:
        /**
         @brief Nil optional.
         */
        Optional() noexcept : _v(nullptr) {}

        /**
         @brief Copy construct.
         */
        Optional(Optional const& o) : Optional() { if (o) _v = _init(*o); }
        /**
         @brief Move construct.
         */
        Optional(Optional&& o) : Optional() { if (o) _v = _init(std::move(*o)); /* leave other unaffected */ }

        /**
         @brief Copy construct by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U const&, T>::value, int>::type = 0>
        Optional(Optional<U> const& o) : Optional() { if (o) _v = _init(*o); }
        /**
         @brief Move construct by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, int>::type = 0>
        Optional(Optional<U>&& o) : Optional() { if (o) _v = _init(std::move(*o)); /* leave other unaffected */ }

        /**
         @brief Copy/move construct by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, int>::type = 0>
        Optional(U&& u) : Optional() { _v = _init(std::forward<U>(u)); }

        // assignment:
        /**
         @brief Copy assignment.
         */
        Optional& operator=(Optional const& o);
        /**
         @brief Move assignment.
         */
        Optional& operator=(Optional&& o);

        /**
         @brief Copy assignment by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U const&, T>::value, int>::type = 0>
        Optional& operator=(Optional<U> const& o);
        /**
         @brief Move assignment by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, int>::type = 0>
        Optional& operator=(Optional<U>&& o);

        /**
         @brief Copy/move assignment by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, int>::type = 0>
        Optional& operator=(U&& u);

        // observers:
        /**
         @brief Returns a pointer to the contained value.
         */
        T const* operator->() const noexcept { return _v; }
        /**
         @brief Returns a pointer to the contained value.
         */
        T      * operator->()       noexcept { return _v; }

        /**
         @brief Returns a reference to the contained value (unchecked).
         */
        T const& operator*() const noexcept { return *_v; }
        /**
         @brief Returns a reference to the contained value (unchecked).
         */
        T      & operator*()       noexcept { return *_v; }

        /**
         @brief Checks whether *this contains a value.
         */
        explicit operator bool() const noexcept { return has_value(); }
        /**
         @brief Checks whether *this contains a value.
         */
        bool has_value() const noexcept { return nullptr != _v; }

        /**
         @brief Checked value access.
         @discussion Throws Optional::nil_access when there is no value.
         */
        T const& value() const;
        /**
         @brief Checked value access.
         @discussion Throws Optional::nil_access when there is no value.
         */
        T      & value();

        /**
         @brief Checked value access.
         @discussion No exception is thrown unless T constructor throws an exception.
         */
        template <typename U>
        T value_or(U&& default_value) const&;
        /**
         @brief Checked value access.
         @discussion No exception is thrown unless T constructor throws an exception.
         */
        template <typename U>
        T value_or(U&& default_value) &&;

        /**
         @brief Shorthand for value().
         */
        T const& operator()() const { return value(); }
        /**
         @brief Shorthand for value().
         */
        T      & operator()() { return value(); }

        /**
         @brief Shorthand for value_or(...).
         */
        template <typename U>
        T operator()(U&& default_value) const& { return value_or(std::forward<U>(default_value)); }
        /**
         @brief Shorthand for value_or(...).
         */
        template <typename U>
        T operator()(U&& default_value) && { return value_or(std::forward<U>(default_value)); }

        // modifiers:
        /**
         @brief Swaps the contents with those of other.
         */
        void swap(Optional& o);
        /**
         @brief If *this contains a value, destroy that value.
         */
        void reset() noexcept { this->~Optional(); }
        /**
         @brief Constructs the contained value in-place.
         */
        template <typename... Args>
        void emplace(Args&&... args) { this->~Optional(); _v = _init(std::forward<Args>(args)...); }

    private:
        template <typename... Args>
        T* _init(Args&&... args) { return ::new(_storage) T(std::forward<Args>(args)...); }
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE

// std::swap:
namespace std {
    template<class T> inline
    void swap(UTILITYKIT_NAMESPACE::__3_::Optional<T>& x, UTILITYKIT_NAMESPACE::__3_::Optional<T>& y) noexcept(noexcept(x.swap(y))) {
        x.swap(y);
    }
}


// MARK:- Version 4
//
#include <type_traits>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Optional wrapper simplified from c++17 std::optional.
     */
    template <typename T>
    class Optional {
        typename std::aligned_storage<sizeof(T), std::alignment_of<T>::value>::type _storage;
        bool _has_value{false};

        // value pointer conversion
        //
        T const *_value_ptr() const noexcept { return reinterpret_cast<T const *>(&_storage); }
        T       *_value_ptr()       noexcept { return reinterpret_cast<T       *>(&_storage); }

    public:
        //types:
        using value_type = T; //!< Type of the associated value.
        class nil_access; //!< nil value access exception.

        // destructor:
        ~Optional() { if (*this) _value_ptr()->T::~T(); }

        // constructor:
        /**
         @brief Nil optional.
         */
        Optional() noexcept {}

        /**
         @brief Copy construct.
         */
        Optional(Optional const& o) { if (o) _init(*o); }
        /**
         @brief Move construct.
         @discussion When the other contains a value, it does not make the other empty; a moved-from optional still contains a value, but the value itself is moved from.
         */
        Optional(Optional&& o) { if (o) _init(std::move(*o)); }

        /**
         @brief Copy construct by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U const&, T>::value, long>::type = 0L>
        Optional(Optional<U> const& o) { if (o) _init(*o); }
        /**
         @brief Move construct by conversion.
         @discussion When the other contains a value, it does not make the other empty; a moved-from optional still contains a value, but the value itself is moved from.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, long>::type = 0L>
        Optional(Optional<U>&& o) { if (o) _init(std::move(*o)); }

        /**
         @brief Copy/move construct by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, long>::type = 0L>
        Optional(U&& u) { _init(std::forward<U>(u)); }

        /**
         @brief Construct by direct initialization.
         */
        template <typename... Args>
        explicit Optional(decltype(nullptr), Args&&... args) { _init(std::forward<Args>(args)...); }

        // assignment:
        /**
         @brief Copy assignment.
         */
        inline Optional& operator=(Optional const& o);
        /**
         @brief Move assignment.
         */
        inline Optional& operator=(Optional&& o);

        /**
         @brief Copy assignment by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U const&, T>::value, long>::type = 0L>
        inline Optional& operator=(Optional<U> const& o);
        /**
         @brief Move assignment by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, long>::type = 0L>
        inline Optional& operator=(Optional<U>&& o);

        /**
         @brief Copy/move assignment by conversion.
         */
        template <typename U, typename std::enable_if<std::is_convertible<U&&, T>::value, long>::type = 0L>
        inline Optional& operator=(U&& u);

        // observers:
        /**
         @brief Returns a pointer to the contained value.
         */
        T const* operator->() const noexcept { return _value_ptr(); }
        /**
         @brief Returns a pointer to the contained value.
         */
        T      * operator->()       noexcept { return _value_ptr(); }

        /**
         @brief Returns a reference to the contained value (unchecked).
         */
        T const& operator*() const noexcept { return *_value_ptr(); }
        /**
         @brief Returns a reference to the contained value (unchecked).
         */
        T      & operator*()       noexcept { return *_value_ptr(); }

        /**
         @brief Checks whether *this contains a value.
         */
        explicit operator bool() const noexcept { return has_value(); }
        /**
         @brief Checks whether *this contains a value.
         */
        bool has_value() const noexcept { return _has_value; }

        /**
         @brief Checked value access.
         @discussion Throws Optional::nil_access when there is no value.
         */
        inline T const& value() const;
        /**
         @brief Checked value access.
         @discussion Throws Optional::nil_access when there is no value.
         */
        inline T      & value();

        /**
         @brief Checked value access.
         @discussion No exception is thrown unless T constructor throws an exception.
         */
        template <typename U>
        inline T value_or(U&& default_value) const&;
        /**
         @brief Checked value access.
         @discussion No exception is thrown unless T constructor throws an exception.
         */
        template <typename U>
        inline T value_or(U&& default_value) &&;

        /**
         @brief Shorthand for value().
         */
        T const& operator()() const { return value(); }
        /**
         @brief Shorthand for value().
         */
        T      & operator()() { return value(); }

        /**
         @brief Shorthand for value_or(...).
         */
        template <typename U>
        T operator()(U&& default_value) const& { return value_or(std::forward<U>(default_value)); }
        /**
         @brief Shorthand for value_or(...).
         */
        template <typename U>
        T operator()(U&& default_value) && { return value_or(std::forward<U>(default_value)); }

        // modifiers:
        /**
         @brief Swaps the contents with those of other.
         */
        void swap(Optional& o) noexcept { std::swap(_storage, o._storage); std::swap(_has_value, o._has_value); }
        /**
         @brief If *this contains a value, destroy that value.
         */
        void reset() noexcept { this->~Optional(); _has_value = false; }
        /**
         @brief Constructs the contained value in-place.
         */
        template <typename... Args>
        void emplace(Args&&... args) { Optional{nullptr, std::forward<Args>(args)...}.swap(*this); }

    private:
        template <typename... Args>
        void _init(Args&&... args) {
            ::new(&_storage) T(std::forward<Args>(args)...);
            _has_value = true;
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLOptional.hh>

#endif /* UTLOptional_h */
