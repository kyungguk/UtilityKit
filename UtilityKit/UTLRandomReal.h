//
//  UTLRandomReal.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLRandomReal_h
#define UTLRandomReal_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Abstract class for a psuedo random number generator uniformly distributed between 0.0 and 1.0.
     */
    class RandomReal {
        RandomReal(RandomReal const&) = delete;
        RandomReal &operator=(RandomReal const&) = delete;

    protected:
        explicit RandomReal() noexcept = default;
    public:
        virtual ~RandomReal() = default;

        // types
        //
        using value_type = double; //!< real value type.
        using size_type = unsigned long; //!< integer type.

        // generate
        //
        virtual value_type operator()() noexcept = 0;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Abstract class for a psuedo random number generator uniformly distributed between 0.0 and 1.0.
     */
    class RandomReal {
        RandomReal(RandomReal const&) = delete;
        RandomReal &operator=(RandomReal const&) = delete;

    protected:
        explicit RandomReal() noexcept = default;
    public:
        virtual ~RandomReal() = default;

        // types
        //
        using value_type = double; //!< real value type.
        using size_type = unsigned long; //!< integer type.

        // generate
        //
        virtual value_type operator()() noexcept = 0;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLRandomReal_h */
