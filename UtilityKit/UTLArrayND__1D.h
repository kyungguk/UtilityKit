//
//  UTLArrayND__1D.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 8/25/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayND__1D_h
#define UTLArrayND__1D_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLPaddedArray.h>
#include <UtilityKit/UTLVector.h>
#include <utility>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class Type, long ND, long PadSize>
    class ArrayND;
    template <class Type, long ND, long PadSize>
    class ArraySliceND;

    // MARK: Array<1D>
    //
    /**
     @brief One-dimensional specialization.
     */
    template <class Type, long PadSize>
    class ArrayND<Type, 1L, PadSize> final : private PaddedArray<Type, PadSize> {
        using _Super = PaddedArray<Type, PadSize>; // _Super owns the storage
        static_assert(PadSize >= 0, "invalid PadSize");

        class _Flat : public Array<Type> {
        public:
            explicit _Flat() noexcept {}
            using Array<Type>::Array;
            void swap(_Flat &o) noexcept { // storage swap
                std::swap(this->_beg, o._beg);
                std::swap(this->_end, o._end);
            }
        } _flat{};
        bool _is_subarray{};

    public:
        // types:
        using value_type             = Type;
        using iterator               = typename _Super::iterator;
        using const_iterator         = typename _Super::const_iterator;
        using reverse_iterator       = typename _Super::reverse_iterator;
        using const_reverse_iterator = typename _Super::const_reverse_iterator;
        using size_type              = typename _Super::size_type;
        using difference_type        = typename _Super::difference_type;
        using size_vector_type       = Vector<size_type, 1L>;

        using leaf_iterator                 = iterator;
        using const_leaf_iterator           = const_iterator;
        using reverse_leaf_iterator         = reverse_iterator;
        using const_reverse_leaf_iterator   = const_reverse_iterator;

        // destructor:
        ~ArrayND() {}

        // constructors:
        /**
         @brief Construct an array whose state is invalid.
         @discussion No storage allocation occurs.
         */
        explicit ArrayND() noexcept {}

        //@{
        /**
         @brief Construct an array of the given dimension sizes (not including paddings) optionally with default value.
         */
        explicit ArrayND(size_vector_type const& dims) : ArrayND(_Super(dims.front())) {}
        explicit ArrayND(size_vector_type const& dims, Type const& fill) : ArrayND(_Super(dims.front(), fill)) {}
        //@}

        //@{
        /**
         @brief Copy/move construct from another array of the same type.
         @discussion When move constructing, other should not be an element of a higher rank array.

         Moving swaps the internal storages including flags, while copying is equivalent to creating a new array with the contents of other.
         */
        ArrayND(ArrayND const& other) : ArrayND(static_cast<_Super const&>(other)) {}
        ArrayND(ArrayND&& other) : ArrayND() { swap(other); }
        //@}

        //@{
        /**
         @brief Copy/move construct from a padded array.
         */
        explicit ArrayND(PaddedArray<Type, PadSize> const& other) : ArrayND(_Super{other}) {}
        explicit ArrayND(PaddedArray<Type, PadSize>&& other) noexcept : _Super(std::move(other)) {
            if (_Super::operator bool()) _Flat{_Super::pad_begin(), _Super::pad_end()}.swap(_flat);
        }
        //@}

        /**
         @brief Copy construct from an array slice.
         */
        explicit ArrayND(ArraySliceND<Type, 1L, PadSize> const &slice) : ArrayND() {
            if (slice) ArrayND({slice.leaf_pad_begin(), slice.leaf_pad_end()})._swap(*this);
        }

        // assignment:
        //@{
        /**
         @brief Element-wise copy/move assignment.
         @discussion Be careful when copy assigning array returned from a function, which will trigger move assignment.
         */
        ArrayND& operator=(ArrayND const& other) { _Super::operator=(other); return *this; }
        ArrayND& operator=(ArrayND&& other) { _Super::operator=(std::move(other)); return *this; }
        ArrayND& operator=(PaddedArray<Type, PadSize> const& other) { _Super::operator=(other); return *this; }
        ArrayND& operator=(PaddedArray<Type, PadSize>&& other) { _Super::operator=(std::move(other)); return *this; }
        ArrayND& operator=(ArraySliceND<Type, 1L, PadSize> const &slice);
        ArrayND& operator=(ArraySliceND<Type, 1L, PadSize> &&slice);
        //@}

        // observers:
        /**
         @brief Check if the status of *this is valid.
         @discussion The validity is defined by whether the underlying storage is allocated or not.
         A default-constructed array is invalid, so is an array with reduce_prod(max_dims()) == 0.
         */
        using _Super::operator bool;

        /**
         @brief Returns true if *this is an element of a higher rank array.
         @discussion If true, storage move semantic does not make sense and doing so will throw an exception.
         */
        bool is_subarray() const noexcept { return _is_subarray; }

        // capacity:
        //@{
        /**
         Properties of the leading dimension.
         */
        using _Super::empty;
        using _Super::size;
        using _Super::pad_size;
        using _Super::max_size;
        //@}

        /**
         @brief Array rank, i.e., ND template parameter.
         */
        static constexpr size_type rank() noexcept { return size_vector_type::size(); }

        /**
         @brief Dimension sizes (excluding paddings).
         */
        size_vector_type dims() const noexcept { return _Super::size(); }

        /**
         @brief Maximum dimension sizes (i.e., dims() + pad_size()).
         */
        size_vector_type max_dims() const noexcept { return _Super::max_size(); }

        /**
         @brief Size at a given dimension.
         */
        template <long I>
        size_type size() const noexcept {
            static_assert(I >= 0 && I < rank(), "dimension out of bound");
            return _Super::size();
        }

        /**
         @brief Max size at a given dimension.
         */
        template <long I>
        size_type max_size() const noexcept {
            static_assert(I >= 0 && I < rank(), "dimension out of bound");
            return _Super::max_size();
        }

        // modifiers:
        using _Super::fill;

        /**
         @brief Storage swap.
         @discussion Both *this and other should not be an element of higher rank array.
         @exception When *this and/or other is an element of higher rank array (i.e., if is_subarray() returns true).
         */
        inline void swap(ArrayND& other);

        // iterators:
        using _Super::begin;
        using _Super::cbegin;
        using _Super::end;
        using _Super::cend;

        using _Super::rbegin;
        using _Super::crbegin;
        using _Super::rend;
        using _Super::crend;

        using _Super::pad_begin;
        using _Super::pad_cbegin;
        using _Super::pad_end;
        using _Super::pad_cend;

        using _Super::pad_rbegin;
        using _Super::pad_crbegin;
        using _Super::pad_rend;
        using _Super::pad_crend;

        //@{
        /**
         @brief Walk through the elements in the slice (excluding paddings).
         */
        leaf_iterator        leaf_begin()       noexcept { return _Super::begin(); }
        const_leaf_iterator  leaf_begin() const noexcept { return _Super::begin(); }
        const_leaf_iterator leaf_cbegin() const noexcept { return _Super::begin(); }
        leaf_iterator          leaf_end()       noexcept { return _Super::end(); }
        const_leaf_iterator    leaf_end() const noexcept { return _Super::end(); }
        const_leaf_iterator   leaf_cend() const noexcept { return _Super::end(); }

        reverse_leaf_iterator        leaf_rbegin()       noexcept { return _Super::rbegin(); }
        const_reverse_leaf_iterator  leaf_rbegin() const noexcept { return _Super::rbegin(); }
        const_reverse_leaf_iterator leaf_crbegin() const noexcept { return _Super::rbegin(); }
        reverse_leaf_iterator          leaf_rend()       noexcept { return _Super::rend(); }
        const_reverse_leaf_iterator    leaf_rend() const noexcept { return _Super::rend(); }
        const_reverse_leaf_iterator   leaf_crend() const noexcept { return _Super::rend(); }
        //@}

        //@{
        /**
         @brief Walk through the elements in the slice (including paddings).
         */
        leaf_iterator        leaf_pad_begin()       noexcept { return _Super::pad_begin(); }
        const_leaf_iterator  leaf_pad_begin() const noexcept { return _Super::pad_begin(); }
        const_leaf_iterator leaf_pad_cbegin() const noexcept { return _Super::pad_begin(); }
        leaf_iterator          leaf_pad_end()       noexcept { return _Super::pad_end(); }
        const_leaf_iterator    leaf_pad_end() const noexcept { return _Super::pad_end(); }
        const_leaf_iterator   leaf_pad_cend() const noexcept { return _Super::pad_end(); }

        reverse_leaf_iterator        leaf_pad_rbegin()       noexcept { return _Super::pad_rbegin(); }
        const_reverse_leaf_iterator  leaf_pad_rbegin() const noexcept { return _Super::pad_rbegin(); }
        const_reverse_leaf_iterator leaf_pad_crbegin() const noexcept { return _Super::pad_rbegin(); }
        reverse_leaf_iterator          leaf_pad_rend()       noexcept { return _Super::pad_rend(); }
        const_reverse_leaf_iterator    leaf_pad_rend() const noexcept { return _Super::pad_rend(); }
        const_reverse_leaf_iterator   leaf_pad_crend() const noexcept { return _Super::pad_rend(); }
        //@}

        // element access:
        using _Super::at;
        using _Super::operator[];
        using _Super::front;
        using _Super::back;
        using _Super::pad_front;
        using _Super::pad_back;

        //@{
        /**
         @brief Index path subscript.
         */
        Type       &operator[](size_vector_type const &ipath)       noexcept(noexcept(std::declval<_Super>()[0])) { return _Super::operator[](ipath.front()); }
        Type const &operator[](size_vector_type const &ipath) const noexcept(noexcept(std::declval<_Super>()[0])) { return _Super::operator[](ipath.front()); }

        Type       &at(size_vector_type const &ipath)       { return this->at(ipath.front()); }
        Type const &at(size_vector_type const &ipath) const { return this->at(ipath.front()); }
        //@}

        /**
         @brief Returns reference to the flattened array of *this.
         @discussion If this->dims() is {N0, N1, ...}, flat_array() will contain (N0+2*pad_size()) * (N1+2*pad_size()) * ... elements.

         It contains no elements if *this is invalid.
         */
        Array<Type>       &flat_array()       noexcept { return _flat; }
        Array<Type> const &flat_array() const noexcept { return _flat; }

        //@{
        /**
         @brief Returns the first element of the underlying storage.
         @discussion This returns nullptr if bool(*this) is false.
         */
        Type       *data()       noexcept { return _flat.begin(); }
        Type const *data() const noexcept { return _flat.begin(); }
        //@}

        /**
         @brief Returns the pointer to the storage of the underlying array and its maximum extents.
         @discussion Used for slicing.
         */
        std::pair<Type *, size_vector_type> storage() noexcept { return std::make_pair(data(), max_dims()); }

    private:
        friend ArrayND<Type, 2, PadSize>; // grants access to enclosing ArrayND with rank() + 1
        explicit ArrayND(decltype(nullptr), std::shared_ptr<Type>&& storage, size_vector_type const& dims) : ArrayND() {
            // only called by enclosing ArrayND with rank() + 1
            // storage can be nullptr, indicating empty array (even without padding)
            // otherwise storage should be large enough that elements [_Super::pad_begin(), _Super::pad_end()) are valid
            //
            if (storage) {
                ArrayND{_Super{dims.front(), std::move(storage)}}._swap(*this);
            }
            _is_subarray = true;
        }

        void _swap(ArrayND& other) noexcept {
            _Super::swap(other);
            _flat.swap(other._flat);
            std::swap(_is_subarray, other._is_subarray);
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLArrayND__1D.hh>

#endif /* UTLArrayND__1D_h */
