//
//  UTLLinearSplineCoefficient.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/15/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLLinearSplineCoefficient_hh
#define UTLLinearSplineCoefficient_hh

// MARK:- UTL::__4_::LinearSplineCoefficient<T>
//
template <class T>
void UTL::__4_::LinearSplineCoefficient<T>::_construct_coefs(typename Base::coefficient_table_type &table)
{
    // S_j(x) = y_j + (y_j+1 - y_j)*t, where t = (x - x_j)/(x_j+1 - x_j).
    //
    for (auto it = table.begin(), end = table.end() - 1; it != end; ++it) {
        std::get<1>(it->second) = std::get<0>((it + 1)->second) - std::get<0>(it->second);
    }
}

#endif /* UTLLinearSplineCoefficient_hh */
