//
//  SIMDVector.hpp
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#ifndef SIMDVector_hpp
#define SIMDVector_hpp

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/SIMDVector.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
namespace SIMD {

// Currently, GCC allows using the following operators on these types: +, -, *, /, unary minus, ^, |, &, ~, %.
//
// It is possible to use shifting operators <<, >> on integer-type vectors. The operation is defined as following: {a0, a1, ..., an} >> {b0, b1, ..., bn} == {a0 >> b0, a1 >> b1, ..., an >> bn}. Vector operands must have the same number of elements.
//
// Vector comparison is supported with standard comparison operators: ==, !=, <, <=, >, >=.
// Comparison operands can be vector expressions of integer-type or real-type. Comparison between integer-type vectors and real-type vectors are not supported.
// The result of the comparison is a vector of the same width and number of elements as the comparison operands with a signed integral element type.
// Vectors are compared element-wise producing 0 when comparison is false and -1 (constant of the appropriate type where all bits are set) otherwise.
#pragma mark Unary
#if !defined(VECTOR_KIT_NO_SIMD)
// +:
template<class T, long S> inline Vector<T, S> operator+(const Vector<T, S>& a) { return +a.__v_; }
// -:
template<class T, long S> inline Vector<T, S> operator-(const Vector<T, S>& a) { return -a.__v_; }
// ~:
template<class T, long S> inline Vector<T, S> operator~(const Vector<T, S>& a) { return ~a.__v_; }
#else
// +:
template<class T, long S> inline Vector<T, S> const& operator+(const Vector<T, S>& a) { return a; }
// -:
template<class T, long S> inline Vector<T, S> operator-(Vector<T, S> a) { for (unsigned i = 0; i < S; ++i) a.__v_[i] = -a.__v_[i]; return a; }
// ~:
template<class T, long S> inline Vector<T, S> operator~(Vector<T, S> a) { for (unsigned i = 0; i < S; ++i) a.__v_[i] = ~a.__v_[i]; return a; }
#endif

#pragma mark Binary - Arithematic
#if !defined(VECTOR_KIT_NO_SIMD)
template<class T, long S> inline Vector<T, S> operator+(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ + b.__v_; }
template<class T, long S> inline Vector<T, S> operator-(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ - b.__v_; }
template<class T, long S> inline Vector<T, S> operator*(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ * b.__v_; }
template<class T, long S> inline Vector<T, S> operator/(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ / b.__v_; }
#else
// +:
template<class T, long S> inline Vector<T, S> operator+(Vector<T, S> a, const Vector<T, S>& b) { return a += b; }
// -:
template<class T, long S> inline Vector<T, S> operator-(Vector<T, S> a, const Vector<T, S>& b) { return a -= b; }
// *:
template<class T, long S> inline Vector<T, S> operator*(Vector<T, S> a, const Vector<T, S>& b) { return a *= b; }
// /:
template<class T, long S> inline Vector<T, S> operator/(Vector<T, S> a, const Vector<T, S>& b) { return a /= b; }
#endif

#pragma mark Binary - Integer Modulus
#if !defined(VECTOR_KIT_NO_SIMD)
// %:
template<class T, long S> inline Vector<T, S> operator%(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ % b.__v_; }
#else
// %:
template<class T, long S> inline Vector<T, S> operator%(Vector<T, S> a, const Vector<T, S>& b) { return a %= b; }
#endif

#pragma mark Binary - Bitwise
#if !defined(VECTOR_KIT_NO_SIMD)
// &:
template<class T, long S> inline Vector<T, S> operator&(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ & b.__v_; }
// |:
template<class T, long S> inline Vector<T, S> operator|(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ | b.__v_; }
// ^:
template<class T, long S> inline Vector<T, S> operator^(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ ^ b.__v_; }
#else
// &:
template<class T, long S> inline Vector<T, S> operator&(Vector<T, S> a, const Vector<T, S>& b) { return a &= b; }
// |:
template<class T, long S> inline Vector<T, S> operator|(Vector<T, S> a, const Vector<T, S>& b) { return a |= b; }
// ^:
template<class T, long S> inline Vector<T, S> operator^(Vector<T, S> a, const Vector<T, S>& b) { return a ^= b; }
#endif

#pragma mark Binary - Bit Shift
#if !defined(VECTOR_KIT_NO_SIMD)
// <<:
template<class T, long S> inline Vector<T, S> operator<<(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ <<  b.__v_; }
// >>:
template<class T, long S> inline Vector<T, S> operator>>(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ >>  b.__v_; }
#else
// <<:
template<class T, long S> inline Vector<T, S> operator<<(Vector<T, S> a, const Vector<T, S>& b) { return a <<= b; }
// >>:
template<class T, long S> inline Vector<T, S> operator>>(Vector<T, S> a, const Vector<T, S>& b) { return a >>= b; }
#endif

#pragma mark Binary - Comparison
#if !defined(VECTOR_KIT_NO_SIMD)
// ==:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator==(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ == b.__v_; }
// !=:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator!=(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ != b.__v_; }
// <:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator< (const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ <  b.__v_; }
// <=:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator<=(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ <= b.__v_; }
// >:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator> (const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ >  b.__v_; }
// >=:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator>=(const Vector<T, S>& a, const Vector<T, S>& b) { return a.__v_ >= b.__v_; }
#else
// ==:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator==(const Vector<T, S>& a, const Vector<T, S>& b) {
    typename Vector<T, S>::comparison_result_type c;
    for (unsigned i = 0; i < S; ++i) c.__v_[i] = -(a[i] == b[i]);
    return c;
}
// !=:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator!=(const Vector<T, S>& a, const Vector<T, S>& b) {
    typename Vector<T, S>::comparison_result_type c;
    for (unsigned i = 0; i < S; ++i) c.__v_[i] = -(a[i] != b[i]);
    return c;
}
// <:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator< (const Vector<T, S>& a, const Vector<T, S>& b) {
    typename Vector<T, S>::comparison_result_type c;
    for (unsigned i = 0; i < S; ++i) c.__v_[i] = -(a[i] < b[i]);
    return c;
}
// <=:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator<=(const Vector<T, S>& a, const Vector<T, S>& b) {
    typename Vector<T, S>::comparison_result_type c;
    for (unsigned i = 0; i < S; ++i) c.__v_[i] = -(a[i] <= b[i]);
    return c;
}
// >:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator> (const Vector<T, S>& a, const Vector<T, S>& b) {
    typename Vector<T, S>::comparison_result_type c;
    for (unsigned i = 0; i < S; ++i) c.__v_[i] = -(a[i] > b[i]);
    return c;
}
// >=:
template<class T, long S> inline typename Vector<T, S>::comparison_result_type operator>=(const Vector<T, S>& a, const Vector<T, S>& b) {
    typename Vector<T, S>::comparison_result_type c;
    for (unsigned i = 0; i < S; ++i) c.__v_[i] = -(a[i] >= b[i]);
    return c;
}
#endif


#pragma mark Binary - Ternary
// In C++, the ternary operator ?: is available. a?b:c, where b and c are vectors of the same type and a is an integer vector with the same number of elements of the same size as b and c, computes all three arguments and creates a vector {a[0]?b[0]:c[0], a[1]?b[1]:c[1], ...}.
// Note that unlike in OpenCL, a is thus interpreted as a != 0 and not a < 0.
// As in the case of binary operations, this syntax is also accepted when one of b or c is a scalar that is then transformed into a vector. If both b and c are scalars and the type of true?b:c has the same size as the element type of a, then b and c are converted to a vector type whose elements have this type and with the same number of elements as a.


// In C++, the logic operators !, &&, || are available for vectors. !v is equivalent to v == 0, a && b is equivalent to a!=0 & b!=0 and a || b is equivalent to a!=0 | b!=0.
// For mixed operations between a scalar s and a vector v, s && v is equivalent to s?v!=0:0 (the evaluation is short-circuit) and v && s is equivalent to v!=0 & (s?-1:0).
#pragma mark Binary - Logic
//template<class T, long S> inline auto operator! (const Vector<T, S>& a                       ) -> decltype(a == 0)      { return a == 0; }
//template<class T, long S> inline auto operator&&(const Vector<T, S>& a, const Vector<T, S>& b) -> decltype(a!=0 & b!=0) { return a!=0 & b!=0; }
//template<class T, long S> inline auto operator||(const Vector<T, S>& a, const Vector<T, S>& b) -> decltype(a!=0 | b!=0) { return a!=0 | b!=0; }
//
//template<class T, long S> inline auto operator&&(const T&            s, const Vector<T, S>& v) -> decltype(v && v) { return Vector<T, S>(s ? v.__v_!=0 : 0)   ; }
//template<class T, long S> inline auto operator&&(const Vector<T, S>& v, const T&            s) -> decltype(v && v) { return Vector<T, S>(v.__v_!=0 & (s?-1:0)); }

// Vector shuffling is available using functions __builtin_shuffle (vec, mask) and __builtin_shuffle (vec0, vec1, mask). Both functions construct a permutation of elements from one or two vectors and return a vector of the same type as the input vector(s).
// The mask is an integral vector with the same width (W) and element count (N) as the output vector.
// The elements of the input vectors are numbered in memory ordering of vec0 beginning at 0 and vec1 beginning at N. The elements of mask are considered modulo N in the single-operand case and modulo 2*N in the two-operand case.
#pragma mark Binary - Vector Shuffle
//template<class T, long S> inline auto shuffle(const Vector<T, S>& a, const typename Vector<T, S>::shuffle_mask_type& mask)
//-> Vector<T, S> { return Vector<T, S>( __builtin_shuffle(a.__v_,         mask) ); }
//template<class T, long S> inline auto shuffle(const Vector<T, S>& a, const Vector<T, S>& b, const typename Vector<T, S>::shuffle_mask_type& mask)
//-> Vector<T, S> { return Vector<T, S>( __builtin_shuffle(a.__v_, b.__v_, mask) ); }

// You cannot operate between vectors of different lengths or different signedness without a cast.

} // namespace SIMD
} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* SIMDVector_hpp */
