//
//  UTLNRRandomReal.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLNRRandomReal.h"

// MARK: Version 3
//
#include <utility>

void UTL::__3_::NRRandomReal::swap(NRRandomReal &o) noexcept
{
    __u_.swap(o.__u_);
    __v_.swap(o.__v_);
    __w_.swap(o.__w_);
}
UTL::__3_::NRRandomReal::NRRandomReal(NRRandomReal &&o) noexcept
: RandomReal(), __u_(), __v_(), __w_() {
    this->swap(o);
}
auto UTL::__3_::NRRandomReal::operator=(NRRandomReal &&o) noexcept
-> NRRandomReal &{
    if (this != &o) this->swap(o);
        return *this;
}

UTL::__3_::NRRandomReal::NRRandomReal(size_type s) noexcept
: RandomReal(), __u_(new size_type), __v_(new size_type(4101842887655102017UL)), __w_(new size_type(1)) {
    *__u_ = s ^ *__v_; _variate_int();
    *__v_ =     *__u_; _variate_int();
    *__w_ =     *__v_; _variate_int();
}

auto UTL::__3_::NRRandomReal::_variate_int() noexcept
-> size_type {
    *__u_  = *__u_ * 2862933555777941757UL + 7046029254386353087UL;
    *__v_ ^= *__v_ >> 17; *__v_ ^= *__v_ << 31; *__v_ ^= *__v_ >> 8;
    *__w_  = 4294957665UL*(*__w_ & 0xffffffff) + (*__w_ >> 32);
    size_type x = *__u_ ^ (*__u_ << 21); x ^= x >> 35; x ^= x << 4;
    return (x + *__v_) ^ *__w_;
}


// MARK:- Version 4
//
#include <utility>

void UTL::__4_::NRRandomReal::swap(NRRandomReal &o) noexcept
{
    __u_.swap(o.__u_);
    __v_.swap(o.__v_);
    __w_.swap(o.__w_);
}
UTL::__4_::NRRandomReal::NRRandomReal(NRRandomReal &&o) noexcept
: RandomReal(), __u_(std::move(o.__u_)), __v_(std::move(o.__v_)), __w_(std::move(o.__w_)) {
}
auto UTL::__4_::NRRandomReal::operator=(NRRandomReal &&o) noexcept
-> NRRandomReal &{
    if (this != &o) {
        NRRandomReal{std::move(o)}.swap(*this);
    }
    return *this;
}

UTL::__4_::NRRandomReal::NRRandomReal(size_type s) noexcept
: RandomReal(), __u_(new size_type), __v_(new size_type(4101842887655102017UL)), __w_(new size_type(1)) {
    *__u_ = s ^ *__v_; _variate_int();
    *__v_ =     *__u_; _variate_int();
    *__w_ =     *__v_; _variate_int();
}

auto UTL::__4_::NRRandomReal::_variate_int() noexcept
-> size_type {
    *__u_  = *__u_ * 2862933555777941757UL + 7046029254386353087UL;
    *__v_ ^= *__v_ >> 17; *__v_ ^= *__v_ << 31; *__v_ ^= *__v_ >> 8;
    *__w_  = 4294957665UL*(*__w_ & 0xffffffff) + (*__w_ >> 32);
    size_type x = *__u_ ^ (*__u_ << 21); x ^= x >> 35; x ^= x << 4;
    return (x + *__v_) ^ *__w_;
}
