//
//  UTLMullerRootFinder.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/22/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLMullerRootFinder_hh
#define UTLMullerRootFinder_hh

// MARK:- Version 4
//
#include <algorithm>
#include <array>
#include <cmath>

template <class F, class StepMonitor>
auto UTL::__4_::MullerRootFinder<double>::operator()(F&& f, complex_type x0, complex_type x1, complex_type x2, StepMonitor&& monitor) const
-> optional_type {
    // check for initial guesses
    //
    if (!(isfinite( x0) && isfinite( x1) && isfinite( x2)) ) {
        return {};
    }
    complex_type fx0 = f(x0), fx1 = f(x1), fx2 = f(x2);
    if (!(isfinite(fx0) && isfinite(fx1) && isfinite(fx2)) ) {
        return {};
    }

    // iteration
    //
    constexpr complex_type two{2, 0};
    for (unsigned i = 0; i < max_iterations && monitor(i, x2); ++i) {
        auto const h1 = x1 - x0;
        auto const h2 = x2 - x1;
        auto const h3 = x2 - x0;
        auto const d1 = (fx1 - fx0)/h1;
        auto const d2 = (fx2 - fx1)/h2;
        auto const d3 = (fx2 - fx0)/h3;
        auto const b = d3 + d2 - d1;
        auto const _2sqrtFx2d = two * std::sqrt(fx2*(fx2*h1 + fx0*h2 - fx1*h3) / (h1*h2*h3));
        auto const D = std::sqrt((b + _2sqrtFx2d)*(b - _2sqrtFx2d));
        complex_type const E = (std::abs(b - D) > std::abs(b + D) ? b - D : b + D);

        auto const h = -two * fx2/E;
        if (!isfinite(h)) {
            return {};
        } else if (/*std::abs(h) < abstol || */std::abs(h) < reltol*abs_max(x0, x1, x2)) {
            return x2 + h;
        } else {
            x0 = x1; fx0 = fx1;
            x1 = x2; fx1 = fx2;
            x2 = x2 + h, fx2 = f(x2);
        }
    }

    return {};
}

bool UTL::__4_::MullerRootFinder<double>::isfinite(complex_type const &x) noexcept
{
    return std::isfinite(x.real()) && std::isfinite(x.imag());
}
auto UTL::__4_::MullerRootFinder<double>::abs_max(complex_type const &x0, complex_type const &x1, complex_type const &x2) noexcept
-> real_type {
    std::array<real_type, 3> const arr{std::abs(x0), std::abs(x1), std::abs(x2)};
    return *std::max_element(arr.begin(), arr.end());
}

#endif /* UTLMullerRootFinder_hh */
