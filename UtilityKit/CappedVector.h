//
//  CappedVector.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/12/15.
//
//

#ifndef ArrayKit__CappedVector_h
#define ArrayKit__CappedVector_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <type_traits>
#include <utility> // move
#include <iterator>
#include <initializer_list>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
    // class definition:
    template<class _Tp, long _MaxSize>
    struct CappedVector {
        static_assert(_MaxSize > 0, "CappedVector - _MaxSize == 0");

        // types:
        //typedef typename std::enable_if<std::is_pod<_Tp>::value, _Tp>::type value_type;
        typedef _Tp                                   value_type;
        typedef value_type&                           reference;
        typedef const value_type&                     const_reference;
        typedef value_type*                           iterator;
        typedef const value_type*                     const_iterator;
        typedef value_type*                           pointer;
        typedef const value_type*                     const_pointer;
        typedef std::size_t                           size_type;
        typedef std::ptrdiff_t                        difference_type;
        typedef std::reverse_iterator<iterator>       reverse_iterator;
        typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

        size_type __size_;
        value_type __elems_[_MaxSize];

        // construct/copy/destory:
        ~CappedVector() = default;
        constexpr CappedVector() noexcept : __size_(0) {}

        explicit CappedVector(size_type __sz); // Don't initialize the elements
        explicit CappedVector(size_type __sz, const value_type& __x);
        template<class _It>
        CappedVector(_It first, _It last);
        CappedVector(std::initializer_list<value_type> __il) : CappedVector(__il.begin(), __il.end()) {}

        CappedVector(CappedVector const&) noexcept(noexcept(*__elems_=_Tp())) = default;
        CappedVector(CappedVector&&) noexcept(noexcept(*__elems_=std::move(*__elems_))) = default;

        // assignment:
        CappedVector& operator=(CappedVector const&) noexcept(noexcept(*__elems_=_Tp())) = default;
        CappedVector& operator=(CappedVector&&) noexcept(noexcept(*__elems_=std::move(*__elems_))) = default;
        CappedVector& operator=(std::initializer_list<value_type> __il)
        { return *this = CappedVector(__il); }

        // iterators:
        iterator begin() noexcept
        { return iterator(__elems_); }
        const_iterator begin() const noexcept
        { return const_iterator(__elems_); }
        iterator end() noexcept
        { return iterator(__elems_ + __size_); }
        const_iterator end() const noexcept
        { return const_iterator(__elems_ + __size_); }

        reverse_iterator rbegin() noexcept
        { return reverse_iterator(end()); }
        const_reverse_iterator rbegin() const noexcept
        { return const_reverse_iterator(end()); }
        reverse_iterator rend() noexcept
        { return reverse_iterator(begin()); }
        const_reverse_iterator rend() const noexcept
        { return const_reverse_iterator(begin()); }

        // capacity:
        size_type size() const noexcept
        { return __size_; }
        static size_type max_size() noexcept
        { return _MaxSize; }
        static size_type capacity() noexcept
        { return _MaxSize; }
        bool empty() const noexcept
        { return 0 == __size_; }

        // element access:
#if defined(DEBUG)
        reference operator[](size_type __n) noexcept { return at(__n); }
        const_reference operator[](size_type __n) const noexcept { return at(__n); }
#else
        reference operator[](size_type __n) noexcept
        { return __elems_[__n]; }
        const_reference operator[](size_type __n) const noexcept
        { return __elems_[__n]; }
#endif

        reference at(size_type __n);
        const_reference at(size_type __n) const;

        reference front(); // check if size is zero, then throw
        const_reference front() const;
        reference back();
        const_reference back() const;

        value_type* data() noexcept
        { return __elems_; }
        const value_type* data() const noexcept
        { return __elems_; }

        // modifiers:
        void push_back(const value_type& __x);
        void push_back(value_type&& __x);
        value_type&& pop_back();

        void clear() noexcept
        { __size_ = 0; }

        void resize(size_type __sz); // Don't initialize the elements
        void resize(size_type __sz, const value_type& __x);

        void swap(CappedVector& __v);
    };
} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* ArrayKit__CappedVector_h */
