//
//  UTLUniformDistribution.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLUniformDistribution.h"

// MARK: Version 3
//
#include <cmath>

UTL::__3_::UniformDistribution::UniformDistribution() noexcept
: Distribution(), _min(0), _max(1) {
}
UTL::__3_::UniformDistribution::UniformDistribution(value_type min, value_type max) noexcept
: UniformDistribution() {
    _min = min;
    _max = max;
}

UTL::__3_::UniformDistribution::UniformDistribution(UniformDistribution const &o) noexcept
: Distribution(), _min(o._min), _max(o._max) {
}
auto UTL::__3_::UniformDistribution::operator=(const UniformDistribution &o) noexcept
-> UniformDistribution &{
    if (this != &o) {
        _min = o._min;
        _max = o._max;
    }
    return *this;
}

auto UTL::__3_::UniformDistribution::mean() const noexcept
-> value_type {
    return 0.5 * (_min + _max);
}
auto UTL::__3_::UniformDistribution::variance() const noexcept
-> value_type {
    return (_min - _max)*(_min - _max) / 12.0;
}

auto UTL::__3_::UniformDistribution::pdf(value_type x) const
-> value_type {
    return ((x >= _min) & (x <= _max)) ? 1.0/(_max - _min) : 0.0;
}
auto UTL::__3_::UniformDistribution::cdf(value_type x) const
-> value_type {
    return ((x >= _min) & (x <= _max)) ? (x - _min)/(_max - _min) : (x < _min ? 0.0 : 1.0);
}
auto UTL::__3_::UniformDistribution::icdf(value_type cdf) const
-> value_type {
    return cdf*(_max - _min) + _min;
}


// MARK:- Version 4
//
#include <cmath>

UTL::__4_::UniformDistribution::UniformDistribution() noexcept
: Distribution(), _min(0), _max(1) {
}
UTL::__4_::UniformDistribution::UniformDistribution(value_type const min, value_type const max) noexcept
: UniformDistribution() {
    _min = min;
    _max = max;
}

UTL::__4_::UniformDistribution::UniformDistribution(UniformDistribution const &o) noexcept
: Distribution(), _min(o._min), _max(o._max) {
}
auto UTL::__4_::UniformDistribution::operator=(const UniformDistribution &o) noexcept
-> UniformDistribution &{
    if (this != &o) {
        _min = o._min;
        _max = o._max;
    }
    return *this;
}

auto UTL::__4_::UniformDistribution::mean() const noexcept
-> value_type {
    return 0.5 * (_min + _max);
}
auto UTL::__4_::UniformDistribution::variance() const noexcept
-> value_type {
    return (_min - _max)*(_min - _max) / 12.0;
}

auto UTL::__4_::UniformDistribution::pdf(value_type const x) const
-> value_type {
    return ((x >= _min) & (x <= _max)) ? 1.0/(_max - _min) : 0.0;
}
auto UTL::__4_::UniformDistribution::cdf(value_type const x) const
-> value_type {
    return ((x >= _min) & (x <= _max)) ? (x - _min)/(_max - _min) : (x < _min ? 0.0 : 1.0);
}
auto UTL::__4_::UniformDistribution::icdf(value_type const cdf) const
-> value_type {
    return cdf*(_max - _min) + _min;
}
