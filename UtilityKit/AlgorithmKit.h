//
//  AlgorithmKit.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 1/14/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#ifndef UtilityKit__AlgorithmKit_h
#define UtilityKit__AlgorithmKit_h


#if defined(__cplusplus)

#include <UtilityKit/UTLAdaptiveSampling1D.h>
#include <UtilityKit/UTLSplineCoefficient.h>
#include <UtilityKit/UTLLinearSplineCoefficient.h>
#include <UtilityKit/UTLCubicSplineCoefficient.h>
#include <UtilityKit/UTLMonotoneCubicCoefficient.h>
#include <UtilityKit/UTLSplineInterpolator.h>
#include <UtilityKit/UTLTrapzIntegrator.h>
#include <UtilityKit/UTLGaussLegendreIntegrator.h>
#include <UtilityKit/UTLNewtonRootFinder.h>
#include <UtilityKit/UTLMullerRootFinder.h>
#include <UtilityKit/UTLHammingFilter.h>
#include <UtilityKit/UTLShapeFunction.h>
#include <UtilityKit/UTLZeroCrossings.h>

#include <UtilityKit/LinearInterpolation.h>
#include <UtilityKit/MonotoneCubicInterpolation.h>
#include <UtilityKit/CubicSplineInterpolation.h>
#include <UtilityKit/trapz.h>
#include <UtilityKit/Decimal.h>
#include <UtilityKit/Expression.h>

#endif


#endif /* UtilityKit__AlgorithmKit_h */


// TODO: fast pow: http://martin.ankerl.com/2012/01/25/optimized-approximative-pow-in-c-and-cpp/
//inline double fastPow(double a, double b) {
//    union {
//        double d;
//        int x[2];
//    } u = { a };
//    u.x[1] = (int)(b * (u.x[1] - 1072632447) + 1072632447);
//    u.x[0] = 0;
//    return u.d;
//}
