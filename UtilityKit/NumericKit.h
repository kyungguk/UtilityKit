//
//  NumericKit.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/28/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef NumericKit_h
#define NumericKit_h


#if defined(__cplusplus)

// TODO: Common math functions
// TODO: Shuffle
// TODO: make_vector and make_simd_vector
#include <UtilityKit/UTLVector.h>
#include <UtilityKit/UTLSIMD.h>
#include <UtilityKit/UTLSIMDVector.h>
#include <UtilityKit/UTLSIMDComplex.h>

#endif


#endif /* NumericKit_h */
