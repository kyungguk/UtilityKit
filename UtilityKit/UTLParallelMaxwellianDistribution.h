//
//  UTLParallelMaxwellianDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLParallelMaxwellianDistribution_h
#define UTLParallelMaxwellianDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     Parallel component of bi-Maxwellian distribution.

     pdf = exp( -(v1 - vd)^2/vth^2 ) / √pi vth.
     cdf = ( 1 + erf((v1 - vd)/vth) )/2.
     mean = vd and variance = vth^2/2.
     */
    class ParallelMaxwellianDistribution final : public Distribution {
        using interp_type = MonotoneCubicInterpolation<value_type>;

        interp_type _icdf;
        value_type _vth;
        value_type _vd;

    public:
        // constructors
        //
        /**
         Construct distribution with the thermal speed and drift speed.
         @param vth Thermal speed defined as sqrt of 2*variance.
         @param vd Drift speed. Default is 0.
         @param should_normalize_cdf If true, normalize the sampled cdf points. Default is true.
         */
        explicit ParallelMaxwellianDistribution(value_type vth, value_type vd = 0, bool should_normalize_cdf = true);

        // copy/move
        //
        ParallelMaxwellianDistribution(ParallelMaxwellianDistribution const &) noexcept;
        ParallelMaxwellianDistribution &operator=(ParallelMaxwellianDistribution const &) noexcept;
        ParallelMaxwellianDistribution(ParallelMaxwellianDistribution &&) noexcept;
        ParallelMaxwellianDistribution &operator=(ParallelMaxwellianDistribution &&) noexcept;

        // properties
        //
        /**
         Thermal speed (note the definition).
         */
        value_type vth() const noexcept { return _vth; }
        /**
         Drift speed.
         */
        value_type vd() const noexcept { return _vd; }

        // inherited
        //
        value_type mean() const noexcept override { return _vd; }
        value_type variance() const noexcept override { return _vth*_vth/2; }

        /**
         Probability distribution function.
         */
        value_type pdf(value_type v1) const override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type v1) const override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type cdf) const override;

    private: // helpers:
        using sampler_type = AdaptiveSampling1D<value_type>;
        static sampler_type _sampler() noexcept;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     Parallel component of bi-Maxwellian distribution.

     pdf = exp( -(v1 - vd)^2/vth^2 ) / √pi vth.
     cdf = ( 1 + erf((v1 - vd)/vth) )/2.
     mean = vd and variance = vth^2/2.
     */
    class ParallelMaxwellianDistribution final : public Distribution {
        using spline_coefficient_type = MonotoneCubicCoefficient<value_type>;
        using interpolator_type = spline_coefficient_type::spline_interpolator_type;
        using sampler_type = AdaptiveSampling1D<value_type>;

        value_type _vth;
        value_type _vd;
        interpolator_type _icdf;

    public:
        // constructors
        //
        /**
         Construct distribution with the thermal speed and drift speed.
         @param vth Thermal speed defined as sqrt of 2*variance.
         @param vd Drift speed (default is 0).
         */
        explicit ParallelMaxwellianDistribution(value_type const vth, value_type const vd = 0);

        // copy/move
        //
        ParallelMaxwellianDistribution(ParallelMaxwellianDistribution const &);
        ParallelMaxwellianDistribution &operator=(ParallelMaxwellianDistribution const &);
        ParallelMaxwellianDistribution(ParallelMaxwellianDistribution &&);
        ParallelMaxwellianDistribution &operator=(ParallelMaxwellianDistribution &&);

        // swap
        //
        void swap(ParallelMaxwellianDistribution &o);

        // properties
        //
        /**
         Thermal speed (note the definition).
         */
        value_type vth() const noexcept { return _vth; }
        /**
         Drift speed.
         */
        value_type vd() const noexcept { return _vd; }

        // inherited
        //
        value_type mean() const noexcept override { return _vd; }
        value_type variance() const noexcept override { return _vth*_vth/2; }

        /**
         Probability distribution function.
         */
        value_type pdf(value_type const v1) const noexcept override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type const v1) const noexcept override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type const cdf) const override;

    private: // helpers:
        static sampler_type default_sampler() noexcept;

        inline static value_type gaussian_pdf(value_type const x) noexcept;
        inline static value_type gaussian_cdf(value_type const x) noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLParallelMaxwellianDistribution_h */
