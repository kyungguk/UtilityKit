//
//  UTL_pow.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 12/21/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTL_pow_h
#define UTL_pow_h

#include <UtilityKit/UtilityKit-config.h>
#include <type_traits>
#include <stdexcept>

UTILITYKIT_BEGIN_NAMESPACE
// power of integer n
//
namespace {
    // helpers
    //
    template <class T>
    constexpr T _pow(T  , std::integral_constant<long, 0>) noexcept {
        return T{1};
    }
    template <class T>
    constexpr T _pow(T x, std::integral_constant<long, 1>) noexcept {
        return x;
    }
    template <class T>
    constexpr T _pow(T x, std::integral_constant<long, 2>) noexcept {
        return x*x;
    }
    template <class T>
    constexpr T _pow(T x, std::integral_constant<long, 3>) noexcept {
        return x*x*x;
    }
    template <class T>
    constexpr T _pow(T x, std::integral_constant<long, 4>) noexcept {
        return (x*x)*(x*x);
    }
    template <class T, long N>
    constexpr T _pow(T x, std::integral_constant<long, N>) noexcept {
        using Chunk = std::integral_constant<long, 4>;
        using Quotient = std::integral_constant<long, N/Chunk::value>;
        using Remainder = std::integral_constant<long, N%Chunk::value>;
        //
        return _pow(_pow(x, Chunk{}), Quotient{})*_pow(x, Remainder{});
    }

    /**
     @brief Power of positive integer exponent.
     */
    template <long Exponent, class T>
    constexpr typename std::enable_if<std::is_arithmetic<T>::value, T>::type pow(T x) noexcept {
        static_assert(Exponent >= 0, "negative exponent");
        static_assert(std::is_arithmetic<T>::value, "argument is not an arithmatic type");
        return _pow(x, std::integral_constant<long, Exponent>{});
    }

    // synthetic sugar (for backward compatibility)
    // TODO: Remove this.
    //
    template <class T> UTILITYKIT_DEPRECATED_ATTRIBUTE
    constexpr T pow2(T x) noexcept {
        return pow<2>(x);
    }
    template <class T> UTILITYKIT_DEPRECATED_ATTRIBUTE
    constexpr T pow3(T x) noexcept {
        return pow<3>(x);
    }
    template <class T> UTILITYKIT_DEPRECATED_ATTRIBUTE
    constexpr T pow4(T x) noexcept {
        return pow<4>(x);
    }
}
UTILITYKIT_END_NAMESPACE

#endif /* UTL_pow_h */
