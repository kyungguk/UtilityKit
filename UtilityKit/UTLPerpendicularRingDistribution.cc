//
//  UTLPerpendicularRingDistribution.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLPerpendicularRingDistribution.h"

// MARK: Version 3
//
#include <utility>
#include <cmath>
#include <stdexcept>

#pragma mark class implementation
UTL::__3_::PerpendicularRingDistribution::PerpendicularRingDistribution(value_type vth, value_type vr, bool should_normalize_cdf)
: Distribution(), _icdf(), _vth(vth), _vr(vr) {
    if (_vth <= 0) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }

    // sampler:
    sampler_type sampler = this->_sampler();

    // sample cdf:
    value_type const xr = _vr/_vth, x_max = 5;
    sampler_type::point_list_type
    cdf_pts = sampler([this](value_type const v2Ovth)->value_type {
        return this->_helper_cdf(v2Ovth) - this->_helper_cdf(0);
    }, xr > x_max ? xr-x_max : 0, x_max + xr, 1);

    // optional normalization:
    if (should_normalize_cdf) {
        // normalize cdf:
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        sampler_type::point_list_type::iterator
        first = cdf_pts.begin(), last = cdf_pts.end();
        for (; first != last; ++first) {
            first->second = (first->second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    sampler_type::point_list_type::iterator
    first = cdf_pts.begin(), last = cdf_pts.end();
    for (; first != last; ++first) {
        std::swap(first->second, first->first);
    }
    _icdf = interp_type(cdf_pts.begin(), cdf_pts.end());
}

UTL::__3_::PerpendicularRingDistribution::PerpendicularRingDistribution(PerpendicularRingDistribution const &o) noexcept
: Distribution(), _icdf(o._icdf), _vth(o._vth), _vr(o._vr) {
}
auto UTL::__3_::PerpendicularRingDistribution::operator=(PerpendicularRingDistribution const &o) noexcept
-> PerpendicularRingDistribution &{
    if (this != &o) {
        _icdf = o._icdf;
        _vth = o._vth;
        _vr = o._vr;
    }
    return *this;
}
UTL::__3_::PerpendicularRingDistribution::PerpendicularRingDistribution(PerpendicularRingDistribution &&o) noexcept
: Distribution(), _icdf(std::move(o._icdf)), _vth(std::move(o._vth)), _vr(std::move(o._vr)) {
}
auto UTL::__3_::PerpendicularRingDistribution::operator=(PerpendicularRingDistribution &&o) noexcept
-> PerpendicularRingDistribution &{
    if (this != &o) {
        _icdf = std::move(o._icdf);
        _vth = std::move(o._vth);
        _vr = std::move(o._vr);
    }
    return *this;
}

auto UTL::__3_::PerpendicularRingDistribution::A() const noexcept
-> value_type {
    value_type const xr = _vr / _vth;
    return std::exp(-xr*xr) + 2/M_2_SQRTPI*xr*std::erfc(-xr);
}

auto UTL::__3_::PerpendicularRingDistribution::mean() const noexcept
-> value_type {
    value_type const xr = _vr / _vth;
    return (xr*std::exp(-xr*xr) + 2/M_2_SQRTPI*(xr*xr + 0.5)*std::erfc(-xr)) * _vth/this->A();
}
auto UTL::__3_::PerpendicularRingDistribution::variance() const noexcept
-> value_type {
    value_type const xr = _vr / _vth;
    value_type var = ((xr*xr + 1)*std::exp(-xr*xr) + 2/M_2_SQRTPI*xr*(xr*xr + 1.5)*std::erfc(-xr))*_vth*_vth/this->A();
    return var -= this->mean()*this->mean();
}

auto UTL::__3_::PerpendicularRingDistribution::pdf(value_type v2) const
-> value_type {
    value_type const x = v2 / _vth;
    return v2*this->_helper_pdf(x) / (_vth*_vth);
}
auto UTL::__3_::PerpendicularRingDistribution::cdf(value_type v2) const
-> value_type {
    value_type const x = v2 / _vth;
    return this->_helper_cdf(x) - this->_helper_cdf(0);
}
auto UTL::__3_::PerpendicularRingDistribution::icdf(value_type cdf) const
-> value_type {
    return _icdf(cdf)*_vth;
}

auto UTL::__3_::PerpendicularRingDistribution::_helper_pdf(value_type x) const noexcept
-> value_type {
    // note that x is not multiplied!
    x -= _vr / _vth;
    return std::exp(-x*x) * 2/this->A();
}
auto UTL::__3_::PerpendicularRingDistribution::_helper_cdf(value_type x) const noexcept
-> value_type {
    // note that this is indefinite integral of pdf!
    x -= _vr / _vth;
    return (2/M_2_SQRTPI*_vr/_vth*std::erf(x) - std::exp(-x*x)) / this->A();
}

auto UTL::__3_::PerpendicularRingDistribution::_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.maxRecursion = 20;
        sampler.accuracyGoal = 7;
        sampler.initialPoints = 100;
        sampler.yScaleAbsoluteTolerance = 1e-10;
    }
    return sampler;
}


// MARK:- Version 4
//
#include <UtilityKit/AuxiliaryKit.h>
#include <functional>
#include <stdexcept>
#include <utility>
#include <string>
#include <cmath>

UTL::__4_::PerpendicularRingDistribution::PerpendicularRingDistribution(PerpendicularRingDistribution const &o)
: Distribution(), _vth(o._vth), _vr(o._vr), _icdf(o._icdf) {
}
UTL::__4_::PerpendicularRingDistribution::PerpendicularRingDistribution(PerpendicularRingDistribution &&o)
: Distribution(), _vth(o._vth), _vr(o._vr), _icdf() {
    _icdf = std::move(o._icdf);
}
auto UTL::__4_::PerpendicularRingDistribution::operator=(PerpendicularRingDistribution const &o)
-> PerpendicularRingDistribution &{
    if (this != &o) {
        PerpendicularRingDistribution{o}.swap(*this);
    }
    return *this;
}
auto UTL::__4_::PerpendicularRingDistribution::operator=(PerpendicularRingDistribution &&o)
-> PerpendicularRingDistribution &{
    if (this != &o) {
        PerpendicularRingDistribution{std::move(o)}.swap(*this);
    }
    return *this;
}

void UTL::__4_::PerpendicularRingDistribution::swap(PerpendicularRingDistribution &o)
{
    _icdf.swap(o._icdf);
    std::swap(_vth, o._vth);
    std::swap(_vr, o._vr);
}

auto UTL::__4_::PerpendicularRingDistribution::mean() const noexcept
-> value_type {
    value_type const b = _vr/_vth, b2 = b*b;
    return (b*std::exp(-b2) + 2/M_2_SQRTPI*(b2 + 0.5)*std::erfc(-b))/A(b) * _vth;
}
auto UTL::__4_::PerpendicularRingDistribution::variance() const noexcept
-> value_type {
    value_type const b = _vr/_vth, b2 = b*b;
    value_type var = ((b2 + 1)*std::exp(-b2) + 2/M_2_SQRTPI*b*(b2 + 1.5)*std::erfc(-b))/A(b) * _vth*_vth;
    return var -= pow<2>(mean());
}

auto UTL::__4_::PerpendicularRingDistribution::pdf(value_type const v2) const noexcept
-> value_type {
    value_type const x = v2/_vth, b = _vr/_vth;
    return ring_pdf(x, b) * x/_vth;
}
auto UTL::__4_::PerpendicularRingDistribution::cdf(value_type const v2) const noexcept
-> value_type {
    value_type const x = v2/_vth, b = _vr/_vth;
    return ring_cdf(x, b) - ring_cdf(0, b);
}
auto UTL::__4_::PerpendicularRingDistribution::icdf(value_type const cdf) const
-> value_type {
    try {
        return _vth*_icdf(cdf)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

auto UTL::__4_::PerpendicularRingDistribution::default_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.set_max_recursion(20);
        sampler.set_accuracy_goal(7);
        sampler.set_initial_points(150);
        sampler.set_y_scale_absolute_tolerance(1e-10);
    }
    return sampler;
}

UTL::__4_::PerpendicularRingDistribution::PerpendicularRingDistribution(value_type const vth, value_type const vr)
: Distribution(), _vth(vth), _vr(vr), _icdf() {
    if (_vth <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative vth");
    }

    // sampler:
    sampler_type const sampler = default_sampler();

    // sample cdf:
    value_type const b = _vr/_vth, x_max = 6;
    sampler_type::point_list_type
    cdf_pts = sampler(std::bind<value_type>(&ring_cdf, std::placeholders::_1, b), b > x_max ? b - x_max : 0, x_max + b, 1);

    // re-normalization:
    {
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        for (auto &pt : cdf_pts) {
            pt.second = (pt.second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    for (auto &pt : cdf_pts) {
        std::swap(pt.first, pt.second);
    }
    this->_icdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
}

auto UTL::__4_::PerpendicularRingDistribution::ring_pdf(value_type const x/*v2/vth*/, value_type const b/*vr/vth*/) noexcept
-> value_type {
    // note that x is not multiplied!
    return std::exp(-pow<2>(x - b))*2/A(b);
}
auto UTL::__4_::PerpendicularRingDistribution::ring_cdf(value_type const x/*v2/vth*/, value_type const b/*vr/vth*/) noexcept
-> value_type {
    // note that this is indefinite integral of pdf!
    return 2/M_2_SQRTPI*b*std::erf(x - b)/A(b) - .5*ring_pdf(x, b);
}
auto UTL::__4_::PerpendicularRingDistribution::A(value_type const b/*vr/vth*/) noexcept
-> value_type {
    return std::exp(-b*b) + 2/M_2_SQRTPI*b*std::erfc(-b);
}
