//
//  UTLBlockedArray.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/23/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLBlockedArray_h
#define UTLBlockedArray_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLDynamicArray.h>
#include <UtilityKit/UTLStaticArray.h>
#include <initializer_list>
#include <type_traits>
#include <iterator>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     */
    template <class Type, long BlockSize, class OverlayT = Type>
    class BlockedArray {
    public:
        // types:
        using subarray_type = StaticArray<Type, BlockSize, OverlayT>;
        using bucket_type = DynamicArray<subarray_type>;
        using overlay_type           = typename subarray_type::overlay_type;
        using value_type             = typename subarray_type::value_type;
        using reference              = typename subarray_type::reference;
        using const_reference        = typename subarray_type::const_reference;
        using pointer                = typename subarray_type::pointer;
        using const_pointer          = typename subarray_type::const_pointer;
        class iterator;
        class const_iterator;
        using reverse_iterator       = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;
        using size_type              = typename subarray_type::size_type;
        using difference_type        = typename subarray_type::difference_type;

    private:
        bucket_type _bucket{};

    public:
        // destructor:
        ~BlockedArray() = default;

        /**
         @brief Construct an empty array.
         */
        explicit BlockedArray() = default;

        /**
         @brief Construct an array with the given size.
         */
        explicit BlockedArray(size_type sz);
        explicit BlockedArray(size_type const sz, value_type const& x);

        /**
         @brief Construct an array from the contents of the range [first, last).
         @discussion The iterators should be well defined.
         */
        template <class ForwardIt, typename std::enable_if<std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<ForwardIt>::iterator_category>::value, long>::type = 0L>
        BlockedArray(ForwardIt first, ForwardIt last);

        /**
         @brief Constructs an array with the contents of the initializer list.
         */
        BlockedArray(std::initializer_list<value_type> const il) : BlockedArray(il.begin(), il.end()) {}

        /**
         @brief Copy/move constructor.
         */
        BlockedArray(BlockedArray const &other) noexcept(std::is_nothrow_copy_constructible<bucket_type>::value) : _bucket(other._bucket) {}
        BlockedArray(BlockedArray &&other) noexcept(std::is_nothrow_move_constructible<bucket_type>::value) : _bucket(std::move(other._bucket)) {}

        // Assignments:
        /**
         @brief Copy/move assignment.
         */
        BlockedArray& operator=(BlockedArray const &other);
        BlockedArray& operator=(BlockedArray &&other) noexcept(noexcept(BlockedArray(std::move(other))));

        /**
         @brief Replaces the contents with those identified by initializer list.
         */
        BlockedArray& operator=(std::initializer_list<value_type> const il) { return *this = BlockedArray{il.begin(), il.end()}; }

        // Capacity:
        bool     empty() const noexcept;
        size_type size() const noexcept;

        /**
         @brief Returns the unit memory block size.
         */
        static constexpr size_type block_size() noexcept { return BlockSize; }

        /**
         @brief Returns the maximum possible number of elements.
         @discussion It simply returns the maximum value that size_type can hold.
         */
        static constexpr size_type max_size() noexcept { return std::numeric_limits<size_type>::max(); }

        /**
         @brief Reduces memory usage by filling the middle memory blocks from the last elements.
         @discussion This operation does not preserve the element order.
         */
        void squeeze();

        // element access:
        reference       front()       noexcept { return _bucket.begin()->front(); }
        const_reference front() const noexcept { return _bucket.begin()->front(); }
        reference        back()       noexcept;
        const_reference  back() const noexcept;

        template <class UnaryFunc>
        void for_each(UnaryFunc&& f)       noexcept(noexcept(f(std::declval<value_type>())));
        template <class UnaryFunc>
        void for_each(UnaryFunc&& f) const noexcept(noexcept(f(std::declval<value_type const>())));

        // Modifiers:
        void swap(BlockedArray& other) noexcept { _bucket.swap(other._bucket); }

        /**
         @brief Clear the contents of *this.
         */
        void clear() noexcept { _bucket.clear(); }

        /**
         @brief Append an element to *this.
         @exception An exception will be thrown if size() == max_size().
         */
        template <class... Args>
        reference emplace_back(Args&&... args);
        reference push_back(value_type const &x) { return emplace_back(x); }
        reference push_back(value_type &&x) { return emplace_back(std::move(x)); }

        /**
         @brief Removes the last element from *this.
         @discussion Calling it when *this is empty is no-op.
         */
        void pop_back() noexcept;

        template <class Container, class Predicate/*bool(const_reference)*/>
        size_type evict_if(Container& bucket, Predicate&& pred);
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLBlockedArray.hh>

#endif /* UTLBlockedArray_h */
