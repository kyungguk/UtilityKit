//
//  UTLPerpendicularRingDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLPerpendicularRingDistribution_h
#define UTLPerpendicularRingDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Perpendicular component of ring distribution.
     @discussion Gyro-averaged perpendicular velocity component.

     pdf = v2*exp(-(v2-vr)^2/vth^2) * 2/(vth^2*A), where
     A = exp(-vr^2/vth^2) + √pi vr/vth erfc(-vr/vth).
     Integrate[pdf(v2), v2] = (√pi vr/vth erf((v2-vr)/vth) - exp(-(v2-vr)^2/vth^2)) / A.
     mean = (vr/vth exp(-vr^2/vth^2) + √pi (vr^2/vth^2 + 1/2)erfc(-vr/vth)) vth/A.
     variance = ((vr^2/vth^2 + 1) exp(-vr^2/vth^2) + √pi vr/vth (vr^2/vth^2 + 3/2) erfc(-vr/vth)) vth^2/A - mean^2.
     */
    class PerpendicularRingDistribution final : public Distribution {
        using interp_type = MonotoneCubicInterpolation<value_type>;

        interp_type _icdf;
        value_type _vth;
        value_type _vr;

    public:
        // constructors
        //
        /**
         Construct ring distribution with the thermal speed.
         @param vth Thermal speed.
         @param vr Ring speed.
         @param should_normalize_cdf If true, normalize the sampled cdf points. Default is true.
         */
        explicit PerpendicularRingDistribution(value_type vth, value_type vr, bool should_normalize_cdf = true);

        // copy/move
        //
        PerpendicularRingDistribution(PerpendicularRingDistribution const &) noexcept;
        PerpendicularRingDistribution &operator=(PerpendicularRingDistribution const &) noexcept;
        PerpendicularRingDistribution(PerpendicularRingDistribution &&) noexcept;
        PerpendicularRingDistribution &operator=(PerpendicularRingDistribution &&) noexcept;

        // properties
        //
        /**
         Thermal speed (note the definition).
         */
        value_type vth() const noexcept { return _vth; }
        /**
         Ring speed.
         */
        value_type vr() const noexcept { return _vr; }
        /**
         Normalization constant.
         */
        value_type A() const noexcept;

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type v2) const override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type v2) const override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type cdf) const override;

    private: // helpers:
        value_type _helper_pdf(value_type x) const noexcept;
        value_type _helper_cdf(value_type x) const noexcept;

        using sampler_type = AdaptiveSampling1D<value_type>;
        static sampler_type _sampler() noexcept;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Perpendicular component of ring distribution.
     @discussion Gyro-averaged perpendicular velocity component.

     pdf = v2*exp(-(v2-vr)^2/vth^2) * 2/(vth^2*A), where
     A = exp(-vr^2/vth^2) + √pi vr/vth erfc(-vr/vth).
     Integrate[pdf(v2), v2] = (√pi vr/vth erf((v2-vr)/vth) - exp(-(v2-vr)^2/vth^2)) / A.
     mean = (vr/vth exp(-vr^2/vth^2) + √pi (vr^2/vth^2 + 1/2)erfc(-vr/vth)) vth/A.
     variance = ((vr^2/vth^2 + 1) exp(-vr^2/vth^2) + √pi vr/vth (vr^2/vth^2 + 3/2) erfc(-vr/vth)) vth^2/A - mean^2.
     */
    class PerpendicularRingDistribution final : public Distribution {
        using spline_coefficient_type = MonotoneCubicCoefficient<value_type>;
        using interpolator_type = spline_coefficient_type::spline_interpolator_type;
        using sampler_type = AdaptiveSampling1D<value_type>;

        value_type _vth;
        value_type _vr;
        interpolator_type _icdf;

    public:
        // constructors
        //
        /**
         Construct ring distribution with the thermal speed.
         @param vth Thermal speed.
         @param vr Ring speed (default is 0).
         */
        explicit PerpendicularRingDistribution(value_type const vth, value_type const vr = 0);

        // copy/move
        //
        PerpendicularRingDistribution(PerpendicularRingDistribution const &);
        PerpendicularRingDistribution &operator=(PerpendicularRingDistribution const &);
        PerpendicularRingDistribution(PerpendicularRingDistribution &&);
        PerpendicularRingDistribution &operator=(PerpendicularRingDistribution &&);

        // swap
        //
        void swap(PerpendicularRingDistribution &o);

        // properties
        //
        /**
         Thermal speed (note the definition).
         */
        value_type vth() const noexcept { return _vth; }
        /**
         Ring speed.
         */
        value_type vr() const noexcept { return _vr; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type const v2) const noexcept override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type const v2) const noexcept override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type const cdf) const override;

    private: // helpers:
        static sampler_type default_sampler() noexcept;

        static inline value_type ring_pdf(value_type const x/*v2/vth*/, value_type const b/*vr/vth*/) noexcept;
        static inline value_type ring_cdf(value_type const x/*v2/vth*/, value_type const b/*vr/vth*/) noexcept;
        static inline value_type A(value_type const b/*vr/vth*/) noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLPerpendicularRingDistribution_h */
