//
//  UTLSIMDVector__0.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/28/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMDVector__0_h
#define UTLSIMDVector__0_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLVector.h>
#include <UtilityKit/UTLSIMD.h>
#include <type_traits>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class Type, long Size>
    struct alignas(typename MakeSIMD<Type, Size>::type) SIMDVector;

    // MARK: Helper to Construct Comparison Result Type of Nested Vector
    //
    template <class T, long S>
    struct _comparison_result<SIMDVector<T, S>> { using type = SIMDVector<typename _comparison_result<T>::type, S>; };

    // MARK: Reductions
    //
    template <class T, long S> inline
    T reduce_plus(SIMDVector<T, S> const& v) noexcept(noexcept(v.reduce_plus())) {
        return v.reduce_plus();
    }
    template <class T, long S> inline
    T reduce_prod(SIMDVector<T, S> const& v) noexcept(noexcept(v.reduce_prod())) {
        return v.reduce_prod();
    }

    template <class T, long S> inline
    T reduce_bit_and(SIMDVector<T, S> const& v) noexcept(noexcept(v.reduce_bit_and())) {
        return v.reduce_bit_and();
    }
    template <class T, long S> inline
    T reduce_bit_or(SIMDVector<T, S> const& v) noexcept(noexcept(v.reduce_bit_or())) {
        return v.reduce_bit_or();
    }
    template <class T, long S> inline
    T reduce_bit_xor(SIMDVector<T, S> const& v) noexcept(noexcept(v.reduce_bit_xor())) {
        return v.reduce_bit_xor();
    }
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLSIMDVector__0.hh>

#endif /* UTLSIMDVector__0_h */
