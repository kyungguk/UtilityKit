//
//  UTLVector__N.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/27/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLVector__N_hh
#define UTLVector__N_hh

// MARK:- Version 4
//

// MARK: Assignment
//
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator=(Vector const& o) noexcept(std::is_nothrow_copy_assignable<_Array>::value) -> Vector &{
    if (this != &o) {
        this->_a = o._a;
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator=(Vector&& o) noexcept(std::is_nothrow_move_assignable<_Array>::value) -> Vector &{
    if (this != &o) {
        this->_a = std::move(o._a);
    }
    return *this;
}

// MARK: at
//
template <class Type, long Size>
Type       &UTL::__4_::Vector<Type, Size>::at(size_type const i) {
    if (i >= 0 && i < size()) return data()[i];
    throw std::out_of_range(__PRETTY_FUNCTION__);
}
template <class Type, long Size>
Type const &UTL::__4_::Vector<Type, Size>::at(size_type const i) const {
    if (i >= 0 && i < size()) return data()[i];
    throw std::out_of_range(__PRETTY_FUNCTION__);
}

// MARK: Reductions
//
template <class Type, long Size>
Type UTL::__4_::Vector<Type, Size>::reduce_plus() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{lo().reduce_plus()};
    return r += hi().reduce_plus();
}
template <class Type, long Size>
Type UTL::__4_::Vector<Type, Size>::reduce_prod() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{lo().reduce_prod()};
    return r *= hi().reduce_prod();
}
template <class Type, long Size>
Type UTL::__4_::Vector<Type, Size>::reduce_bit_and() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{lo().reduce_bit_and()};
    return r &= hi().reduce_bit_and();
}
template <class Type, long Size>
Type UTL::__4_::Vector<Type, Size>::reduce_bit_or() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{lo().reduce_bit_or()};
    return r |= hi().reduce_bit_or();
}
template <class Type, long Size>
Type UTL::__4_::Vector<Type, Size>::reduce_bit_xor() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{lo().reduce_bit_xor()};
    return r ^= hi().reduce_bit_xor();
}

// MARK: Compound Arithmetic
//
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator+=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] += rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator+=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] += rhs;
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator-=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] -= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator-=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] -= rhs;
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator*=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] *= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator*=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] *= rhs;
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator/=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] /= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator/=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] /= rhs;
    }
    return *this;
}

// MARK: Compound Modulus
//
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator%=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] %= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator%=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] %= rhs;
    }
    return *this;
}

// MARK: Compound Bit Operators
//
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator&=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] &= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator&=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] &= rhs;
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator|=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] |= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator|=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] |= rhs;
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator^=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] ^= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator^=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] ^= rhs;
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator<<=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] <<= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator<<=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] <<= rhs;
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator>>=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] >>= rhs._a[i];
    }
    return *this;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator>>=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    for (unsigned i = 0; i < Size; ++i) {
        this->_a[i] >>= rhs;
    }
    return *this;
}

// MARK: Comparison
//
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator==(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
    comparison_result_type result;
    for (unsigned i = 0; i < Size; ++i) {
        result.data()[i] = this->_a[i] == rhs._a[i];
    }
    return result;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator!=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
    comparison_result_type result;
    for (unsigned i = 0; i < Size; ++i) {
        result.data()[i] = this->_a[i] != rhs._a[i];
    }
    return result;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator<=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
    comparison_result_type result;
    for (unsigned i = 0; i < Size; ++i) {
        result.data()[i] = this->_a[i] <= rhs._a[i];
    }
    return result;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator>=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
    comparison_result_type result;
    for (unsigned i = 0; i < Size; ++i) {
        result.data()[i] = this->_a[i] >= rhs._a[i];
    }
    return result;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator<(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
    comparison_result_type result;
    for (unsigned i = 0; i < Size; ++i) {
        result.data()[i] = this->_a[i] < rhs._a[i];
    }
    return result;
}
template <class Type, long Size>
auto UTL::__4_::Vector<Type, Size>::operator>(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
    comparison_result_type result;
    for (unsigned i = 0; i < Size; ++i) {
        result.data()[i] = this->_a[i] > rhs._a[i];
    }
    return result;
}

#endif /* UTLVector__N_hh */
