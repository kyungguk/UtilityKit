//
//  UTLPerpendicularMaxwellianDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLPerpendicularMaxwellianDistribution_h
#define UTLPerpendicularMaxwellianDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Perpendicular component of bi-Maxwellian distribution.
     @discussion Gyro-averaged perpendicular velocity component.

     pdf = v2*exp(-v2^2/vth^2) * 2/vth^2.
     cdf = 1 - exp(-v2^2/vth^2).
     mean = √pi vth/2 and variance = vth^2 - mean^2.
     */
    class PerpendicularMaxwellianDistribution final : public Distribution {
        using interp_type = MonotoneCubicInterpolation<value_type>;

        interp_type _icdf;
        value_type _vth;

    public:
        // constructors
        //
        /**
         Construct distribution with the thermal speed.
         @param vth Thermal speed defined as sqrt of 2T/m.
         @param should_normalize_cdf If true, normalize the sampled cdf points. Default is true.
         */
        explicit PerpendicularMaxwellianDistribution(value_type vth, bool should_normalize_cdf = true);

        // copy/move
        //
        PerpendicularMaxwellianDistribution(PerpendicularMaxwellianDistribution const &) noexcept;
        PerpendicularMaxwellianDistribution &operator=(PerpendicularMaxwellianDistribution const &) noexcept;
        PerpendicularMaxwellianDistribution(PerpendicularMaxwellianDistribution &&) noexcept;
        PerpendicularMaxwellianDistribution &operator=(PerpendicularMaxwellianDistribution &&) noexcept;

        // properties
        //
        /**
         Thermal speed (note the definition).
         */
        value_type vth() const noexcept { return _vth; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type v2) const override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type v2) const override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type cdf) const override;

    private: // helpers:
        using sampler_type = AdaptiveSampling1D<value_type>;
        static sampler_type _sampler() noexcept;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Perpendicular component of bi-Maxwellian distribution.
     @discussion Gyro-averaged perpendicular velocity component.

     pdf = v2*exp(-v2^2/vth^2) * 2/vth^2.
     cdf = 1 - exp(-v2^2/vth^2).
     mean = √pi vth/2 and variance = vth^2 - mean^2.
     */
    class PerpendicularMaxwellianDistribution final : public Distribution {
        using spline_coefficient_type = MonotoneCubicCoefficient<value_type>;
        using interpolator_type = spline_coefficient_type::spline_interpolator_type;
        using sampler_type = AdaptiveSampling1D<value_type>;

        value_type _vth;
        interpolator_type _icdf;

    public:
        // constructors
        //
        /**
         Construct distribution with the thermal speed.
         @param vth Thermal speed defined as sqrt of 2T/m.
         */
        explicit PerpendicularMaxwellianDistribution(value_type const vth);

        // copy/move
        //
        PerpendicularMaxwellianDistribution(PerpendicularMaxwellianDistribution const &);
        PerpendicularMaxwellianDistribution &operator=(PerpendicularMaxwellianDistribution const &);
        PerpendicularMaxwellianDistribution(PerpendicularMaxwellianDistribution &&);
        PerpendicularMaxwellianDistribution &operator=(PerpendicularMaxwellianDistribution &&);

        // swap
        //
        void swap(PerpendicularMaxwellianDistribution &o);

        // properties
        //
        /**
         Thermal speed (note the definition).
         */
        value_type vth() const noexcept { return _vth; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type const v2) const noexcept override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type const v2) const noexcept override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type const cdf) const override;

    private: // helpers:
        static sampler_type default_sampler() noexcept;

        inline static value_type gaussian_pdf(value_type const x) noexcept;
        inline static value_type gaussian_cdf(value_type const x) noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLPerpendicularMaxwellianDistribution_h */
