//
//  UTLSIMDComplex.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMDComplex_h
#define UTLSIMDComplex_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLSIMDVector.h>
#include <type_traits>
#include <algorithm>
#include <complex>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class RealType>
    class SIMDComplex {
        static_assert(std::is_floating_point<RealType>::value, "not a floating point type");

        SIMDVector<RealType, 2L> _v;

    public:
        // types:
        using value_type   = RealType;
        using complex_type = std::complex<value_type>;

        // constructors:
        constexpr SIMDComplex() noexcept = default;
        SIMDComplex(SIMDComplex const &) noexcept = default;
        SIMDComplex(RealType const& re, RealType const& im) noexcept : _v(re, im) {}
        SIMDComplex(RealType const& re) noexcept : SIMDComplex(re, RealType{}) {}
        SIMDComplex(std::complex<RealType> const& z) noexcept : SIMDComplex(z.real(), z.imag()) {}

        // std::complex cast:
        explicit operator complex_type      &()      & noexcept { return *reinterpret_cast<complex_type      *>(this); }
        explicit operator complex_type const&() const& noexcept { return *reinterpret_cast<complex_type const*>(this); }

        // element access:
        RealType real() const noexcept { return _v.x; }
        RealType imag() const noexcept { return _v.y; }

        // modifiers:
        void real(RealType const& re) noexcept { _v.x = re; }
        void imag(RealType const& im) noexcept { _v.y = im; }

        void swap(SIMDComplex &o) noexcept { _v.swap(o._v); }

        // operators with RealType:
        SIMDComplex& operator =(RealType const& x) noexcept { return *this = SIMDComplex{x}; }
        SIMDComplex& operator+=(RealType const& x) noexcept { _v.x += x; return *this; }
        SIMDComplex& operator-=(RealType const& x) noexcept { _v.x -= x; return *this; }
        SIMDComplex& operator*=(RealType const& x) noexcept { _v   *= x; return *this; }
        SIMDComplex& operator/=(RealType const& x) noexcept { _v   /= x; return *this; }

        // operators with std::complex:
        SIMDComplex& operator =(std::complex<RealType> const& z) noexcept { return *this = SIMDComplex{z}; }

        // operators with SIMDComplex:
        SIMDComplex& operator =(SIMDComplex const& z) noexcept = default;
        SIMDComplex& operator+=(SIMDComplex const& z) noexcept { _v += z._v; return *this; }
        SIMDComplex& operator-=(SIMDComplex const& z) noexcept { _v -= z._v; return *this; }
        SIMDComplex& operator*=(SIMDComplex const& z) noexcept {
            decltype(_v) tmp{_v.y, -_v.x};
            _v *= z._v;
            tmp *= z._v;
            std::swap(tmp.x, _v.y);
            _v -= tmp;
            return *this;
        }
        SIMDComplex& operator/=(SIMDComplex const& z) noexcept {
            decltype(_v) tmp{z._v};
            tmp *= tmp;
            _v.y = -_v.y;
            *this *= z;
            _v.y = -_v.y;
            return *this /= tmp.x + tmp.y;
        }

        // comparison:
        friend inline bool operator==(SIMDComplex const& a, SIMDComplex const& b) noexcept { return a.real() == b.real() && a.imag() == b.imag(); }
        friend inline bool operator==(SIMDComplex const& z,    RealType const& x) noexcept { return z.real() == x && z.imag() == RealType{}; }
        friend inline bool operator==(   RealType const& x, SIMDComplex const& z) noexcept { return z == x; }
        friend inline bool operator!=(SIMDComplex const& a, SIMDComplex const& b) noexcept { return !(a == b); }
        friend inline bool operator!=(SIMDComplex const& z,    RealType const& x) noexcept { return !(z == x); }
        friend inline bool operator!=(   RealType const& x, SIMDComplex const& z) noexcept { return !(x == z); }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLSIMDComplex.hh>

#endif /* UTLSIMDComplex_h */
