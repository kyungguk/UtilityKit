//
//  UTLUniformDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLUniformDistribution_h
#define UTLUniformDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLDistribution.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief A uniform distribution.
     */
    class UniformDistribution final : public Distribution {
        value_type _min, _max;

    public:
        // constructors
        //
        explicit UniformDistribution() noexcept;
        explicit UniformDistribution(value_type min, value_type max) noexcept;

        // copy
        //
        UniformDistribution(UniformDistribution const &) noexcept;
        UniformDistribution &operator=(UniformDistribution const &) noexcept;

        // properties
        //
        value_type min() const noexcept { return _min; }
        value_type max() const noexcept { return _max; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        value_type pdf(value_type x) const override;
        value_type cdf(value_type x) const override;
        value_type icdf(value_type cdf) const override;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief A uniform distribution.
     */
    class UniformDistribution final : public Distribution {
        value_type _min, _max;

    public:
        // constructors
        //
        explicit UniformDistribution() noexcept;
        explicit UniformDistribution(value_type const min, value_type const max) noexcept;

        // copy
        //
        UniformDistribution(UniformDistribution const &) noexcept;
        UniformDistribution &operator=(UniformDistribution const &) noexcept;

        // properties
        //
        value_type min() const noexcept { return _min; }
        value_type max() const noexcept { return _max; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        value_type pdf(value_type const x) const override;
        value_type cdf(value_type const x) const override;
        value_type icdf(value_type const cdf) const override;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLUniformDistribution_h */
