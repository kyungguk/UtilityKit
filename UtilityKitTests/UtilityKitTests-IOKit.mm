//
//  UtilityKitTests-IOKit.mm
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 1/7/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#import <UtilityKit/UTLPrinto.h>
#import <UtilityKit/UTLOptional.h>
#include <iostream>
#include <string>

@interface UtilityKitTests_IOKit__3_ : XCTestCase

@end

@implementation UtilityKitTests_IOKit__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPrintO {
    printo(std::cout, "single argument test\n");
    printo(std::cout, "multiple argument test", '\n');
    printo(std::cout, 1, "+", 1, "=", 1+1, '\n');
}

- (void)testOptionalConstructor {
    using UTL::__3_::Optional;

    try {
        Optional<int> o1, o2(1), o3(o2);
        XCTAssert(!bool(o1), @"");
        XCTAssert(bool(o2), @"");
        XCTAssert(bool(o3), @"");
        try {
            printo(std::cout, "o1=", o1(), '\n');
            XCTAssert(false, @"");
        } catch (decltype(o1)::nil_access const& e) {
            printo(std::cout, e.what(), "\n");
        }
        XCTAssert(o2() == o3(), @"");
        XCTAssert(o2() == o1(*o2), @"");

        Optional<long> o5(o1), o6(*o2);
        XCTAssert(!bool(o5), @"");
        XCTAssert(bool(o6), @"");
        XCTAssert(o2() == o6(), @"");
    } catch (...) {
        XCTAssert(false, @"");
    }

    try {
        int x = 10;
        Optional<long> o1(std::move(x));
        XCTAssert(o1 && x == *o1, @"");
        decltype(o1) o2(std::move(o1));
        XCTAssert(o2 && x == *o2, @"");
    } catch (...) {
        XCTAssert(false, @"");
    }
}

- (void)testOptionalAssignment {
    using UTL::__3_::Optional;

    try {
        Optional<const char*> o1 = "abc", o2;
        o2 = o1;
        o1 = "def";
        XCTAssert(o1 && o2, @"");
        if (o1 && o2) printo(std::cout, *o2, *o1, '\n');
    } catch (...) {
        XCTAssert(false, @"");
    }

    try {
        const char *orig = "abc";
        std::string s = orig;
        Optional<decltype(s)> o1;
        o1 = std::move(s);
        XCTAssert(o1 && *o1 == orig, @"");
        decltype(o1) o2;
        o2 = std::move(o1);
        XCTAssert(o2 && *o2 == orig, @"");
    } catch (...) {
        XCTAssert(false, @"");
    }

    try {
        int orig = 10;
        Optional<long> o1;
        o1 = orig;
        XCTAssert(o1 && *o1 == orig, @"");
        decltype(o1) o2;
        o2 = std::move(orig);
        XCTAssert(o2 && *o2 == *o1, @"");
        decltype(o1) o3;
        o3 = std::move(o1);
        XCTAssert(o3, @"");
    } catch (...) {
        XCTAssert(false, @"");
    }
}

- (void)testOptionalSwap {
    using UTL::__3_::Optional;

    try {
        std::string s1 = "abc", s2 = "def";
        Optional<std::string> o1 = s1.c_str(), o2;
        o2.emplace(s2.begin(), s2.end());
        o1.swap(o2);
        XCTAssert(o1 && *o1 == s2, @"*o1 = %s", o1->c_str());
        XCTAssert(o2 && *o2 == s1, @"*o2 = %s", o2->c_str());

        o1.reset();
        o1.swap(o2);
        XCTAssert(!o2 && o1 && *o1 == s1, @"*o1 = %s", o1->c_str());
        o1.swap(o2);
        XCTAssert(!o1 && o2 && *o2 == s1, @"*o2 = %s", o2->c_str());
    } catch (...) {
        XCTAssert(false, @"");
    }
}

- (void)testOptionalOutputStream {
    using UTL::__3_::Optional;

    try {
        printo(std::cout, __PRETTY_FUNCTION__, " - ", Optional<int>(), "\n");
        printo(std::cout, __PRETTY_FUNCTION__, " - ", Optional<int>(1), "\n");
    } catch (...) {
        XCTAssert(false, @"");
    }
}

@end
