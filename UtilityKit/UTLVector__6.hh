//
//  UTLVector__6.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/27/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLVector__6_hh
#define UTLVector__6_hh

// MARK:- Version 4
//

// MARK: Assignment
//
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator=(Vector const& o) noexcept(std::is_nothrow_copy_assignable<Type>::value) -> Vector &{
    if (this != &o) {
        this->x = o.x;
        this->y = o.y;
        this->z = o.z;
        this->u = o.u;
        this->v = o.v;
        this->w = o.w;
    }
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator=(Vector&& o) noexcept(std::is_nothrow_move_assignable<Type>::value) -> Vector &{
    if (this != &o) {
        this->x = std::move(o.x);
        this->y = std::move(o.y);
        this->z = std::move(o.z);
        this->u = std::move(o.u);
        this->v = std::move(o.v);
        this->w = std::move(o.w);
    }
    return *this;
}

// MARK: at
//
template <class Type>
Type       &UTL::__4_::Vector<Type, 6L>::at(size_type const i) {
    if (i >= 0 && i < size()) return data()[i];
    throw std::out_of_range(__PRETTY_FUNCTION__);
}
template <class Type>
Type const &UTL::__4_::Vector<Type, 6L>::at(size_type const i) const {
    if (i >= 0 && i < size()) return data()[i];
    throw std::out_of_range(__PRETTY_FUNCTION__);
}

// MARK: Reductions
//
template <class Type>
Type UTL::__4_::Vector<Type, 6L>::reduce_plus() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{this->x};
    r += this->y;
    r += this->z;
    r += this->u;
    r += this->v;
    r += this->w;
    return r;
}
template <class Type>
Type UTL::__4_::Vector<Type, 6L>::reduce_prod() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{this->x};
    r *= this->y;
    r *= this->z;
    r *= this->u;
    r *= this->v;
    r *= this->w;
    return r;
}
template <class Type>
Type UTL::__4_::Vector<Type, 6L>::reduce_bit_and() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{this->x};
    r &= this->y;
    r &= this->z;
    r &= this->u;
    r &= this->v;
    r &= this->w;
    return r;
}
template <class Type>
Type UTL::__4_::Vector<Type, 6L>::reduce_bit_or() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{this->x};
    r |= this->y;
    r |= this->z;
    r |= this->u;
    r |= this->v;
    r |= this->w;
    return r;
}
template <class Type>
Type UTL::__4_::Vector<Type, 6L>::reduce_bit_xor() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
    Type r{this->x};
    r ^= this->y;
    r ^= this->z;
    r ^= this->u;
    r ^= this->v;
    r ^= this->w;
    return r;
}

// MARK: Compound Arithmetic
//
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator+=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x += rhs.x;
    this->y += rhs.y;
    this->z += rhs.z;
    this->u += rhs.u;
    this->v += rhs.v;
    this->w += rhs.w;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator+=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x += rhs;
    this->y += rhs;
    this->z += rhs;
    this->u += rhs;
    this->v += rhs;
    this->w += rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator-=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x -= rhs.x;
    this->y -= rhs.y;
    this->z -= rhs.z;
    this->u -= rhs.u;
    this->v -= rhs.v;
    this->w -= rhs.w;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator-=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x -= rhs;
    this->y -= rhs;
    this->z -= rhs;
    this->u -= rhs;
    this->v -= rhs;
    this->w -= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator*=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x *= rhs.x;
    this->y *= rhs.y;
    this->z *= rhs.z;
    this->u *= rhs.u;
    this->v *= rhs.v;
    this->w *= rhs.w;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator*=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x *= rhs;
    this->y *= rhs;
    this->z *= rhs;
    this->u *= rhs;
    this->v *= rhs;
    this->w *= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator/=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x /= rhs.x;
    this->y /= rhs.y;
    this->z /= rhs.z;
    this->u /= rhs.u;
    this->v /= rhs.v;
    this->w /= rhs.w;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator/=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x /= rhs;
    this->y /= rhs;
    this->z /= rhs;
    this->u /= rhs;
    this->v /= rhs;
    this->w /= rhs;
    return *this;
}

// MARK: Compound Modulus
//
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator%=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x %= rhs.x;
    this->y %= rhs.y;
    this->z %= rhs.z;
    this->u %= rhs.u;
    this->v %= rhs.v;
    this->w %= rhs.w;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator%=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x %= rhs;
    this->y %= rhs;
    this->z %= rhs;
    this->u %= rhs;
    this->v %= rhs;
    this->w %= rhs;
    return *this;
}

// MARK: Compound Bit Operators
//
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator&=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x &= rhs.x;
    this->y &= rhs.y;
    this->z &= rhs.z;
    this->u &= rhs.u;
    this->v &= rhs.v;
    this->w &= rhs.w;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator&=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x &= rhs;
    this->y &= rhs;
    this->z &= rhs;
    this->u &= rhs;
    this->v &= rhs;
    this->w &= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator|=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x |= rhs.x;
    this->y |= rhs.y;
    this->z |= rhs.z;
    this->u |= rhs.u;
    this->v |= rhs.v;
    this->w |= rhs.w;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator|=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x |= rhs;
    this->y |= rhs;
    this->z |= rhs;
    this->u |= rhs;
    this->v |= rhs;
    this->w |= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator^=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x ^= rhs.x;
    this->y ^= rhs.y;
    this->z ^= rhs.z;
    this->u ^= rhs.u;
    this->v ^= rhs.v;
    this->w ^= rhs.w;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator^=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x ^= rhs;
    this->y ^= rhs;
    this->z ^= rhs;
    this->u ^= rhs;
    this->v ^= rhs;
    this->w ^= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 6L>::operator<<=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x <<= rhs.x;
    this->y <<= rhs.y;
    this->z <<= rhs.z;
    this->u <<= rhs.u;
    this->v <<= rhs.v;
    this->w <<= rhs.w;
    return *this;
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator<<=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
        this->x <<= rhs;
        this->y <<= rhs;
        this->z <<= rhs;
        this->u <<= rhs;
        this->v <<= rhs;
        this->w <<= rhs;
        return *this;
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator>>=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
        this->x >>= rhs.x;
        this->y >>= rhs.y;
        this->z >>= rhs.z;
        this->u >>= rhs.u;
        this->v >>= rhs.v;
        this->w >>= rhs.w;
        return *this;
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator>>=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
        this->x >>= rhs;
        this->y >>= rhs;
        this->z >>= rhs;
        this->u >>= rhs;
        this->v >>= rhs;
        this->w >>= rhs;
        return *this;
    }

    // MARK: Comparison
    //
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator==(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x == rhs.x,
            this->y == rhs.y,
            this->z == rhs.z,
            this->u == rhs.u,
            this->v == rhs.v,
            this->w == rhs.w
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator!=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x != rhs.x,
            this->y != rhs.y,
            this->z != rhs.z,
            this->u != rhs.u,
            this->v != rhs.v,
            this->w != rhs.w
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator<=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x <= rhs.x,
            this->y <= rhs.y,
            this->z <= rhs.z,
            this->u <= rhs.u,
            this->v <= rhs.v,
            this->w <= rhs.w
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator>=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x >= rhs.x,
            this->y >= rhs.y,
            this->z >= rhs.z,
            this->u >= rhs.u,
            this->v >= rhs.v,
            this->w >= rhs.w
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator<(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x < rhs.x,
            this->y < rhs.y,
            this->z < rhs.z,
            this->u < rhs.u,
            this->v < rhs.v,
            this->w < rhs.w
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 6L>::operator>(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x > rhs.x,
            this->y > rhs.y,
            this->z > rhs.z,
            this->u > rhs.u,
            this->v > rhs.v,
            this->w > rhs.w
        };
    }

#endif /* UTLVector__6_hh */
