//
//  UTLArrayNDLeafWalker.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 8/30/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayNDLeafWalker_h
#define UTLArrayNDLeafWalker_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLVector.h>
#include <type_traits>
#include <iterator>
#include <utility> // std::declval

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class ArrayND>
    class _ArrayNDLeafWalker;

    /**
     @brief Immutable iterator.
     */
    template <class ArrayND>
    class _ArrayNDLeafWalker<ArrayND const> {
        friend ArrayND;
    protected:
        ArrayND const *_A;
        Vector<long, ArrayND::rank()> _sizes; //!< 1st element contains PadSize if it includes paddings; 0 otherwise
        Vector<long, ArrayND::rank()> _ipath; //!< current index path

        _ArrayNDLeafWalker(ArrayND const *A, bool const include_paddings, std::false_type/*begin*/) noexcept;
        _ArrayNDLeafWalker(ArrayND const *A, bool const include_paddings, std::true_type/*end*/) noexcept;

    public:
        using iterator_category = std::bidirectional_iterator_tag;
        using difference_type   = long;
        using value_type        = typename ArrayND::value_type const;
        using reference         = value_type &;
        using pointer           = value_type *;

        explicit _ArrayNDLeafWalker() noexcept {}
        reference operator*() const noexcept(noexcept((*_A)[_ipath])) { return (*_A)[_ipath]; }

        // comparison
        bool operator==(_ArrayNDLeafWalker const &o) const noexcept { return reduce_bit_and(_ipath == o._ipath); }
        bool operator!=(_ArrayNDLeafWalker const &o) const noexcept { return reduce_bit_or (_ipath != o._ipath); }

        // walk forward
        _ArrayNDLeafWalker operator++(int) noexcept {
            _ArrayNDLeafWalker retval = *this;
            ++(*this);
            return retval;
        }
        _ArrayNDLeafWalker& operator++() noexcept {
            _incr(_ipath);
            return *this;
        }

        // walk backward
        _ArrayNDLeafWalker operator--(int) noexcept {
            _ArrayNDLeafWalker retval = *this;
            --(*this);
            return retval;
        }
        _ArrayNDLeafWalker& operator--() noexcept {
            _decr(_ipath);
            return *this;
        }

    protected:
        void _incr(Vector<long, 1> &ipath) const noexcept { ++ipath.front(); }
        template <long S>
        inline void _incr(Vector<long, S> &ipath) const noexcept;

        void _decr(Vector<long, 1> &ipath) const noexcept { --ipath.front(); }
        template <long S>
        inline void _decr(Vector<long, S> &ipath) const noexcept;
    };

    /**
     @brief Mutable iterator.
     */
    template <class ArrayND>
    class _ArrayNDLeafWalker : public _ArrayNDLeafWalker<ArrayND const> {
        using Super = _ArrayNDLeafWalker<ArrayND const>;
        friend ArrayND;

        template <bool TF>
        _ArrayNDLeafWalker(ArrayND *A, bool const include_paddings, std::integral_constant<bool, TF> tf) noexcept : Super(A, include_paddings, tf) {}

    public:
        using value_type        = typename ArrayND::value_type;
        using reference         = value_type &;
        using pointer           = value_type *;

        explicit _ArrayNDLeafWalker() noexcept {}
        reference operator*() const noexcept(noexcept(*std::declval<Super>())) {
            return const_cast<ArrayND&>(*this->_A)[this->_ipath];
        }

        // walk forward
        _ArrayNDLeafWalker operator++(int) noexcept {
            _ArrayNDLeafWalker retval = *this;
            ++(*this);
            return retval;
        }
        _ArrayNDLeafWalker& operator++() noexcept {
            this->_incr(this->_ipath);
            return *this;
        }

        // walk backward
        _ArrayNDLeafWalker operator--(int) noexcept {
            _ArrayNDLeafWalker retval = *this;
            --(*this);
            return retval;
        }
        _ArrayNDLeafWalker& operator--() noexcept {
            this->_decr(this->_ipath);
            return *this;
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLArrayNDLeafWalker.hh>

#endif /* UTLArrayNDLeafWalker_h */
