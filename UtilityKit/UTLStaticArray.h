//
//  UTLStaticArray.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/22/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLStaticArray_h
#define UTLStaticArray_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLArray.h>
#include <initializer_list>
#include <type_traits>
#include <iterator>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Variable-length contiguous array with a maximum capacity.
     @discussion A simplified version of std::vector with fixed capacity of MaxSize.
     An block of memory of at least MaxSize is allocated on construction, and no reallocation should occur afterward.
     This means that any elements in the range of [data(), data() + max_size()) can be always accessed, although those in the range of [data() + size(), data() + max_size()) may be in an invalid state.
     MaxSize can be zero.

     When given, OverlayT must have a stricter alignment and has size multiples of the size of Type.
     A storage large enough to hold n OverlayT elements is allocated, where
     for (first = reinterpret_cast<char *>(data()); first < data() + max_size(); first += sizeof(OverlayT)) ++n;
     .

     One exception is when the contents of an instance is moved to another.
     In this case, any operations on the former may result in an undefined behavior.
     */
    template <class Type, long MaxSize, class OverlayT = Type>
    class StaticArray : public Array<Type> {
        static_assert(MaxSize >= 0, "MaxSize must be non-negative");
        static_assert(alignof(OverlayT) >= alignof(Type), "the alignment of OverlayT must be stricter");
        static_assert(sizeof(OverlayT) >= sizeof(Type) && sizeof(OverlayT)%sizeof(Type) == 0, "sizeof(OverlayT) must be multiples of sizeof(Type)");

    public:
        // types:
        using overlay_type           = OverlayT;
        using value_type             = typename Array<Type>::value_type;
        using reference              = typename Array<Type>::reference;
        using const_reference        = typename Array<Type>::const_reference;
        using pointer                = typename Array<Type>::pointer;
        using const_pointer          = typename Array<Type>::const_pointer;
        using iterator               = typename Array<Type>::iterator;
        using const_iterator         = typename Array<Type>::const_iterator;
        using reverse_iterator       = typename Array<Type>::reverse_iterator;
        using const_reverse_iterator = typename Array<Type>::const_reverse_iterator;
        using size_type              = typename Array<Type>::size_type;
        using difference_type        = typename Array<Type>::difference_type;

        // destructor:
        ~StaticArray() { clear(), _deallocate(data()); }

        // Constructors:
        /**
         @brief Construct an empty array.
         @discussion The storage for max_size() elements is allocated.
         */
        explicit StaticArray() : StaticArray(nullptr, _allocate(), size_type{0}) {}

        /**
         @brief Construct an array with the given size.
         @discussion Storage for max_size() elements will be allocated regardless of the given size.
         @exception In addition to any exceptions thrown during construction, an exception will be thrown if sz > max_size().
         */
        explicit StaticArray(size_type const sz);
        explicit StaticArray(size_type const sz, value_type const& x);

        /**
         @brief Construct an array from the contents in the range of [first, last).
         @discussion Storage for max_size() elements will be allocated regardless of the given size.
         @exception In addition to any exceptions thrown during construction, an exception will be thrown if the number of elements that the iterators identify is greater than max_size().
         */
        template <class ForwardIt, typename std::enable_if<std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<ForwardIt>::iterator_category>::value, long>::type = 0L>
        StaticArray(ForwardIt first, ForwardIt last);

        /**
         @brief Constructs an array with the contents of the initializer list.
         @discussion Internally, delegate to the iterator constructor.
         */
        StaticArray(std::initializer_list<value_type> const il) : StaticArray(il.begin(), il.end()) {}

        /**
         @brief Copy/move constructor.
         @discussion After move, the state of other becomes invalid.
         */
        StaticArray(StaticArray const &other) : StaticArray(other.begin(), other.end()) {}
        StaticArray(StaticArray &&other) noexcept : StaticArray(nullptr, nullptr, size_type{0}) { swap(other); }

        // Assignments:
        /**
         @brief Copy/move assignment.
         @discussion After move, the state of other becomes invalid.
         */
        inline StaticArray& operator=(StaticArray const &other);
        inline StaticArray& operator=(StaticArray &&other) noexcept(noexcept(StaticArray(std::move(other))));

        /**
         @brief Replaces the contents with those identified by initializer list.
         @exception Additionally, an exception will be thrown if the number of elements in the initializer list is greater than max_size().
         */
        StaticArray& operator=(std::initializer_list<value_type> const il);

        // Capacity:
        /**
         @brief The maximum number of elements that *this can hold.
         */
        static constexpr size_type max_size() noexcept { return MaxSize; }
        /**
         @brief The maximum number of elements that *this can hold.
         */
        static constexpr size_type capacity() noexcept { return MaxSize; }

        // Observers:
        /**
         @brief Returns true if *this is in a valid state.
         @discussion It returns false if and only if *this has been used for move construction/assignment.
         */
        explicit operator bool() const noexcept { return nullptr != this->begin(); }

        // Element Access:
        pointer       data()       noexcept { return this->begin(); }
        const_pointer data() const noexcept { return this->begin(); }

        // Modifiers:
        /**
         @brief Clear the contents of *this.
         */
        void clear() noexcept { this->_end = _destory(data(), this->size()); }

        /**
         @brief Resize the container with optionally filling with a default value.
         @discussion If an exception is thrown during expansion, this->size() < sz after recovery.
         @exception An exception will be thrown if sz > max_size().
         */
        void resize(size_type const sz);
        void resize(size_type const sz, value_type const &x);

        /**
         @brief Append an element to *this.
         @exception An exception will be thrown if size() == max_size().
         */
        template <class... Args>
        reference emplace_back(Args&&... args);
        reference push_back(value_type const &x) { return emplace_back(x); }
        reference push_back(value_type &&x) { return emplace_back(std::move(x)); }

        /**
         @brief Inserts an element into *this before pos.
         @discussion Internally, a temporary array is used to move the [begin(), pos) elements, emplace the new element, move the rest, and then swap storage.
         So if an exception is thrown during this process, *this will have the size same as before this call, but contents of some elements may have already been moved.
         @exception An exception will be thrown if size() == max_size().
         @return Iterator pointing to the inserted value.
         */
        template <class... Args>
        iterator emplace(const_iterator pos, Args&&... args);
        iterator insert(const_iterator const pos, const value_type &x) { return emplace(pos, x); }
        iterator insert(const_iterator const pos, value_type &&x) { return emplace(pos, std::move(x)); }

        /**
         @brief Removes specified elements from *this.
         @discussion The first version removes the element at pos.
         The iterator pos must be valid and dereferenceable.
         Thus, the end() iterator (which is valid, but is not dereferencable) cannot be used as a value for pos.

         The second version removes the elements in the range [first; last).
         The iterator first does not need to be dereferenceable if first==last: erasing an empty range is a no-op.

         Internally, a temporary array is used to move the [begin(), pos1) elements and [pos2, end()), and then swap storage.
         So if an exception is thrown during this process, *this will have the size same as before this call, but contents of some elements may have already been moved.
         @return Iterator following the last removed element.
         If the iterator pos refers to the last element, the end() iterator is returned.
         */
        iterator erase(const_iterator const pos) { return erase(pos, pos + 1); }
        iterator erase(const_iterator const first, const_iterator last);

        /**
         @brief Removes the last element from *this.
         @discussion Calling it when *this is empty is no-op.
         */
        void pop_back() noexcept { if (!this->empty()) _destory(--this->_end); }

        /**
         @brief Picks out the elements for which the predicate returns true.
         @discussion The evicted elements are pushed back to the passed container by invoking push_back(std::move(...)).

         The order may not be preserved after this operation.
         @return The number of elements evicted.
         */
        template <class Container, class Predicate/*bool(const_reference)*/>
        size_type evict_if(Container& bucket, Predicate&& pred);

        /**
         @brief Storage swap.
         */
        void swap(StaticArray &other) noexcept { std::swap(this->_beg, other._beg), std::swap(this->_end, other._end); }

    private:
        // constructor funnel point:
        explicit StaticArray(decltype(nullptr), pointer const p, size_type const n) noexcept : Array<Type>(p, n) {}

        // aligned memory allocator:
        class _Alloc {
            using POD = typename std::aligned_storage<sizeof(OverlayT), alignof(OverlayT)>::type;
            static constexpr unsigned long _Size = MaxSize*sizeof(Type)/sizeof(OverlayT) + 1; // note that 1 is always added, so MaxSize == 0 is OK
        public:
            template <class> struct rebind { using other = _Alloc; };
            using value_type = Type;
            using is_always_equal = std::true_type;
            constexpr explicit _Alloc() noexcept = default;
            static constexpr unsigned long max_size() noexcept { return sizeof(POD)*_Size/sizeof(Type); }
            template <class I> Type* allocate(I) const { return reinterpret_cast<Type*>(new POD[_Size]); }
            template <class I> void deallocate(Type* p, I) const noexcept { delete [] reinterpret_cast<POD*>(p); }
            constexpr bool operator==(const _Alloc&) const noexcept { return true; }
            constexpr bool operator!=(const _Alloc&) const noexcept { return false; }
        };

        // memory management:
        static pointer _allocate() {
            static _Alloc alloc{};
            return std::allocator_traits<_Alloc>::allocate(alloc, max_size());
        }
        template <class... Args>
        static void _construct(pointer const p, Args&&... args) {
            static _Alloc alloc{};
            std::allocator_traits<_Alloc>::construct(alloc, p, std::forward<Args>(args)...);
        }

        static void _deallocate(pointer const p) noexcept {
            if (p) {
                static _Alloc alloc{};
                std::allocator_traits<_Alloc>::deallocate(alloc, p, max_size());
            }
        }
        static void _destory(pointer const p) noexcept {
            static _Alloc alloc{};
            std::allocator_traits<_Alloc>::destroy(alloc, p);
        }
        static pointer _destory(pointer const p, size_type n) noexcept {
            while (n--) _destory(p + n);
            return p;
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLStaticArray.hh>

#endif /* UTLStaticArray_h */
