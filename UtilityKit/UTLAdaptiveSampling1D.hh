//
//  UTLAdaptiveSampling1D.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/15/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLAdaptiveSampling1D_hh
#define UTLAdaptiveSampling1D_hh

// MARK:- UTL::__4_::AdaptiveSampling1D<T>
//
#include <UtilityKit/UTLSIMDVector.h>
#include <algorithm>
#include <stdexcept>
#include <iterator>
#include <string>
#include <cmath>

template <class T>
void UTL::__4_::AdaptiveSampling1D<T>::set_y_scale_absolute_tolerance(value_type const y_scale_tol)
{
    if (y_scale_tol <= 0) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
    _y_scale_absolute_tolerance = y_scale_tol;
}

// MARK: Preparation Version 1
//
template <class T>
template <class F>
auto UTL::__4_::AdaptiveSampling1D<T>::_sample(F &&f, value_type const& x0, value_type const& x1) const
-> point_list_type {
    // abscissa interval
    //
    value_type const dx = (x1 - x0)/_initial_points;
    if (y_scale_absolute_tolerance() >= std::abs(dx)) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - x0 == x1");
    }

    // initial points
    //
    point_list_type points;
    for (long i = _initial_points; i >= 0; --i) {
        value_type const x = dx*i + x0;
        points.push_front(point_type{x, f(x)});
    }

    // ordinate interval
    //
    typename point_list_type::const_iterator it_min, it_max;
    std::tie(it_min, it_max) = std::minmax_element(points.cbegin(), points.cend(), [](point_type const& a, point_type const& b)->bool {
        return std::get<1>(a) < std::get<1>(b);
    });
    value_type abs_dy = std::get<1>(*it_max) - std::get<1>(*it_min);
    if (y_scale_absolute_tolerance() >= abs_dy) {
        abs_dy = y_scale_absolute_tolerance();
    }

    // scale factor
    //
    point_type const scale{1/std::abs(dx), 1/abs_dy};

    // adaptive sampling
    //
    { // Iterative algorithm:
        if (1 == _initial_points) { // If there are two points, halve.
            for (typename point_list_type::const_iterator it1 = points.cbegin(), it0 = it1++; points.cend() != it1; it0 = it1++) {
                points.splice_after(it0, _sample(f, *it0, *it1, scale, _tolerance, unsigned{1}));
            }
        }

        // Iteration until convergence:
        // NOTE: Maybe scale can be updated at every iteration
        long previous_count = 0, current_count = std::distance(points.cbegin(), points.cend());
        for (long i = 0; previous_count != current_count && i < _max_recursion; ++i) {
            // First three points in reverse order
            // This step is necessary because the sampling algorithm returns only one halved point at the second interval.
            // Three points are passed in a reversed order because the halved point at the first interval is needed.
            //
            typename point_list_type::const_iterator it2 = points.cbegin();
            typename point_list_type::const_iterator it0 = it2++;
            typename point_list_type::const_iterator it1 = it2++;
            points.splice_after(it0, _sample(f, *it2, *it1, *it0, scale, _tolerance));

            // The rest in normal order
            // Note that '++it0' is abscent before for iteration. This means that I will not use the halved point because empty list may be returned in the previous step.
            //
            for (/*++it0*/; points.cend() != it2; it0 = it1/*I am not using the halved point because empty list may be returned*/, it1 = it2++) {
                points.splice_after(it1, _sample(f, *it0, *it1, *it2, scale, _tolerance));
            }

            // update counters
            //
            previous_count = current_count;
            current_count = std::distance(points.cbegin(), points.cend());
        }
    }

    // Return:
    return points;
}

// MARK: Preparation Version 2
//
template <class T>
template <class F>
auto UTL::__4_::AdaptiveSampling1D<T>::_sample(F &&f, value_type const& x0, value_type const& x1, value_type const& y_interval) const -> point_list_type
{
    // abscissa interval
    //
    value_type const dx = (x1 - x0)/_initial_points;
    if (y_scale_absolute_tolerance() >= std::abs(dx)) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - x0 == x1");
    }

    // initial points
    //
    point_list_type points;
    for (long i = _initial_points; i >= 0; --i) {
        value_type const x = dx*i + x0;
        points.push_front(point_type{x, f(x)});
    }

    // ordinate interval
    //
    value_type abs_dy = std::abs(y_interval);
    if (y_scale_absolute_tolerance() >= abs_dy) {
        abs_dy = y_scale_absolute_tolerance();
    }

    // scale factor
    //
    point_type const scale{1/std::abs(dx), 1/abs_dy};

    // adaptive sampling
    //
    { // Iterative algorithm:
        if (1 == _initial_points) { // If there are two points, halve.
            for (typename point_list_type::const_iterator it1 = points.cbegin(), it0 = it1++; points.cend() != it1; it0 = it1++) {
                points.splice_after(it0, _sample(f, *it0, *it1, scale, _tolerance, unsigned{1}));
            }
        }

        // Iteration until convergence:
        long previous_count = 0, current_count = std::distance(points.cbegin(), points.cend());
        for (long i = 0; previous_count != current_count && i < _max_recursion; ++i) {
            // First three points in reverse order
            // This step is necessary because the sampling algorithm returns only one halved point at the second interval.
            // Three points are passed in a reversed order because the halved point at the first interval is needed.
            //
            typename point_list_type::const_iterator it2 = points.cbegin();
            typename point_list_type::const_iterator it0 = it2++;
            typename point_list_type::const_iterator it1 = it2++;
            points.splice_after(it0, _sample(f, *it2, *it1, *it0, scale, _tolerance));

            // The rest in normal order
            // Note that '++it0' is abscent before for iteration. This means that I will not use the halved point because empty list may be returned in the previous step.
            //
            for (/*++it0*/; points.cend() != it2; it0 = it1/*I am not using the halved point because empty list may be returned*/, it1 = it2++) {
                points.splice_after(it1, _sample(f, *it0, *it1, *it2, scale, _tolerance));
            }

            // update counters
            //
            previous_count = current_count;
            current_count = std::distance(points.cbegin(), points.cend());
        }
    }

    // Return:
    return points;
}

// MARK: Recursion-based sampling algorithm
//
template <class T>
template <class F>
auto UTL::__4_::AdaptiveSampling1D<T>::_sample(F &f, point_type const& pt0, point_type const& pt2, point_type const& scale, value_type const& tolerance, unsigned const max_recursion) -> point_list_type
{
    if (0 == max_recursion) { // Terminate recursion
        return point_list_type{};
    }
    using Vector = SIMDVector<value_type, 2>; // be careful with vector alignment
    Vector const v_pt0{std::get<0>(pt0), std::get<1>(pt0)};
    Vector const v_pt2{std::get<0>(pt2), std::get<1>(pt2)};
    Vector const v_scale{std::get<0>(scale), std::get<1>(scale)};

    // sub-intervals
    //
    constexpr value_type _1_2 = value_type{1}/2;
    value_type const x0 = std::get<0>(pt0);
    value_type const x2 = std::get<0>(pt2);
    value_type const x1 = _1_2*(x0 + x2);
    value_type const x12 = _1_2*(x1 + x2);
    value_type const x01 = _1_2*(x0 + x1);
    Vector const v_pt1{x1, f(x1)};
    Vector const v_pt12{x12, f(x12)};
    Vector const v_pt01{x01, f(x01)};
    point_type const& pt1 = *reinterpret_cast<point_type const*>(&v_pt1);

    // scaled vectors among points
    //
    Vector const A = (v_pt1 - v_pt0) * v_scale;
    Vector const B = (v_pt2 - v_pt1) * v_scale;
    Vector const C = (v_pt12 - v_pt01) * v_scale;
    value_type const AB = std::sqrt(reduce_plus(A*A) * reduce_plus(B*B));
    value_type const BC = std::sqrt(reduce_plus(B*B) * reduce_plus(C*C));

    // convergence test
    //
    if (std::abs(AB - reduce_plus(A*B)) < tolerance/*angle test*/ && std::abs(BC - reduce_plus(B*C)) < tolerance/*slope test*/) { // Satisfied, terminate recursion
        return point_list_type{pt1};
    } else { // More division needed, recursive sampling
        point_list_type points = _sample(f, pt1, pt2, scale, tolerance, max_recursion - 1);
        points.push_front(pt1);
        points.splice_after(points.before_begin(), _sample(f, pt0, pt1, scale, tolerance, max_recursion - 1));
        return points;
    }
}

// MARK: One-pass sampling algorithm
//
template <class T>
template <class F>
auto UTL::__4_::AdaptiveSampling1D<T>::_sample(F &f, point_type const& pt0, point_type const& pt1, point_type const& pt2, point_type const& scale, value_type const& tolerance) -> point_list_type
{
    using Vector = SIMDVector<value_type, 2>; // be careful with vector alignment
    Vector const v_pt0{std::get<0>(pt0), std::get<1>(pt0)};
    Vector const v_pt1{std::get<0>(pt1), std::get<1>(pt1)};
    Vector const v_pt2{std::get<0>(pt2), std::get<1>(pt2)};
    Vector const v_scale{std::get<0>(scale), std::get<1>(scale)};

    // mid points
    //
    constexpr value_type _1_2 = value_type{1}/2;
    value_type const x01 = _1_2*(std::get<0>(pt0) + std::get<0>(pt1));
    value_type const x12 = _1_2*(std::get<0>(pt1) + std::get<0>(pt2));
    Vector const v_pt01{x01, f(x01)};
    Vector const v_pt12{x12, f(x12)};
    point_type const& pt12 = *reinterpret_cast<point_type const*>(&v_pt12);

    // scaled vectors among points
    //
    Vector const A = (v_pt1 - v_pt0) * v_scale;
    Vector const B = (v_pt2 - v_pt1) * v_scale;
    Vector const C = A + B;
    Vector const D = (v_pt12 - v_pt01) * v_scale;

    value_type const AB = std::sqrt(reduce_plus(A*A) * reduce_plus(B*B));
    value_type const CD = std::sqrt(reduce_plus(C*C) * reduce_plus(D*D));

    // convergence test
    //
    if (std::abs(AB - reduce_plus(A*B)) < tolerance/*angle test*/ && std::abs(CD - reduce_plus(C*D)) < tolerance/*slope test*/) { // Converged, return no point.
        return point_list_type{};
    } else { // Not converged, return the halved point at the second interval.
        return point_list_type{pt12};
    }
}

#endif /* UTLAdaptiveSampling1D_hh */
