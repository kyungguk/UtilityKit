//
//  UTLTests-AuxiliaryKit.mm
//  UtilityKitTests
//
//  Created by KYUNGGUK MIN on 9/30/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

#define UTILITYKIT_INLINE_VERSION 4
#include <UtilityKit/AuxiliaryKit.h>
#include <sstream>
#include <string>
#include <memory>
#include <iostream>
#include <array>
#include <vector>

@interface UTLTests_AuxiliaryKit : XCTestCase

@end

@implementation UTLTests_AuxiliaryKit

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPrinto {
    std::string const in_s{__PRETTY_FUNCTION__};
    std::ostringstream os;
    printo(os, in_s);
    auto const out_s = os.str();
    XCTAssert(in_s == out_s, @"in = \"%s\", out = \"%s\"", in_s.c_str(), out_s.c_str());
}

struct RefCounter {
    static long count;
    RefCounter() { ++count; }
    ~RefCounter() { --count; }
    RefCounter(RefCounter const &) : RefCounter() {}
    RefCounter &operator=(RefCounter const &) = default;
};
long RefCounter::count;
- (void)testOptional {
    using UTL::Optional;

    try {
        Optional<bool> const opt1;
        XCTAssert(!opt1 && !opt1(false));
        try {
            opt1();
            XCTAssert(false, @"Exception not thrown");
        } catch (decltype(opt1)::nil_access) {
        } catch (...) {
            XCTAssert(false, @"nil_access is not thrown");
        }

        int i = 5;
        Optional<int> const opt2(i);
        XCTAssert(opt2 && i == *opt2);
        Optional<long> opt3(opt2);
        XCTAssert(opt3 && i == *opt3);
        opt3 = opt2;
        XCTAssert(opt3 && i == *opt3);
        opt3.reset();
        XCTAssert(!opt3);
        opt3 = i;
        XCTAssert(!!opt3 && *opt3 == i);
        Optional<std::unique_ptr<long>> opt4(std::unique_ptr<long>{new long(i)});
        XCTAssert(opt4 && i == **opt4);
        Optional<std::unique_ptr<long>> opt5(std::move(opt4));
        XCTAssert(opt4 && !*opt4);
        XCTAssert(opt5 && i == **opt5);
        i = 10;
        opt3 = i;
        XCTAssert(opt3 && i == *opt3);
        opt4 = decltype(opt4)::value_type{new long(i)};
        XCTAssert(opt4 && i == **opt4);
        std::swap(opt4, opt5);
        XCTAssert(opt5 && i == **opt5);

        std::string str{__PRETTY_FUNCTION__};
        Optional<std::string> opt_str{str};
        XCTAssert(opt_str && str.size() == opt_str->size());
        std::string str2 = std::move(opt_str).value_or(str);
        XCTAssert(str2 == __PRETTY_FUNCTION__, @"str2 = %s", str2.c_str());
        XCTAssert(opt_str && opt_str->empty());
        opt_str.emplace(std::move(str));
        XCTAssert(str.empty());
        {
            std::ostringstream os;
            printo(os, opt_str);
            str = os.str();
        }
        XCTAssert(str == str2, @"str = %s, str2 = %s", str.c_str(), str2.c_str());

        RefCounter::count = 0;
        std::vector<Optional<RefCounter>> opts(10);
        XCTAssert(0 == RefCounter::count);
        for (auto &opt : opts) {
            opt = RefCounter{};
        }
        XCTAssert(long(opts.size()) == RefCounter::count, @"count = %ld", RefCounter::count);
        for (auto &opt : opts) {
            opt = Optional<RefCounter>{};
        }
        XCTAssert(0 == RefCounter::count, @"count = %ld", RefCounter::count);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testAny {
    using UTL::Any;

    try {
        Any a;
        XCTAssert(!a);

        std::string str{__PRETTY_FUNCTION__};
        a = Any(std::move(str));
        XCTAssert(str.empty() && a.type() == typeid(str));
        str = Any::cast<decltype(str)>(std::move(a));
        XCTAssert(str == __PRETTY_FUNCTION__ && a.type() == typeid(str) && Any::cast<decltype(str)>(a).empty());
        {
            std::string& str2 = Any::cast<decltype(str) &>(a);
            str2 = str;
        }
        Any b;
        b = a;
        a.reset();
        XCTAssert(!a && b);
        auto const str1 = Any::cast<decltype(str) const&>(b);
        XCTAssert(str1 == __PRETTY_FUNCTION__);

        long i = 10;
        a = i;
        try {
        Any::cast<int>(a);
            XCTAssert(false, @"bad_cast exception not thrown");
        } catch (Any::bad_cast) {
        }
        XCTAssert(a.type() == typeid(i) && Any::cast<long>(a) == i);

        Any const c{std::string{__PRETTY_FUNCTION__}};
        str = *Any::cast<std::string>(&c);
        XCTAssert(str == __PRETTY_FUNCTION__, "*c = %s", str.c_str());
        str.clear();
        str = Any::cast<std::string>(c);
        XCTAssert(str == __PRETTY_FUNCTION__, "*c = %s", str.c_str());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

struct Callable {
    static std::string str;
    static long num[2];
    void operator()() const {
        str.clear();
        num[0] = num[1] = 0;
    }
    void operator()(std::string&& s) const {
        str = std::move(s);
    }
    long operator()(long const i, long const j) const {
        num[0] = i;
        num[1] = j;
        return i;
    }
};
std::string Callable::str;
long Callable::num[2];
- (void)testSplatTuple {
    try {
        UTL::splat_tuple(Callable{}, std::make_tuple());
        XCTAssert(Callable::str.empty() && !Callable::num[0] && !Callable::num[1]);

        std::string str{__PRETTY_FUNCTION__};
        UTL::splat_tuple(Callable{}, std::make_tuple(std::move(str)));
        XCTAssert(str.empty() && Callable::str == __PRETTY_FUNCTION__, @"str = %s, Callable::str = %s", str.c_str(), Callable::str.c_str());

        long const num = 10;
        long const ret = UTL::splat_tuple<long>(Callable{}, std::make_pair(num, num));
        XCTAssert(num == ret && num == Callable::num[0] && num == Callable::num[1]);

        UTL::splat_tuple(Callable{}, std::array<long, 2>{0, 0});
        XCTAssert(!Callable::num[0] && !Callable::num[1]);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)test_power_of_2 {
    XCTAssert(!UTL::is_power_of_2(0) && !UTL::is_power_of_2(3) && UTL::is_power_of_2(4));
    XCTAssert(2 == UTL::next_power_of_2(1U));
    XCTAssert(2 == UTL::next_power_of_2(2U));
    XCTAssert(4 == UTL::next_power_of_2(3U));
    XCTAssert(4 == UTL::next_power_of_2(4U));
    XCTAssert(8 == UTL::next_power_of_2(5U));
    XCTAssert(8 == UTL::next_power_of_2(6U));
    XCTAssert(8 == UTL::next_power_of_2(7U));
    XCTAssert(128 == UTL::next_power_of_2(128U - 1));
    XCTAssert(128 == UTL::next_power_of_2(128U));
    XCTAssert(128*2 == UTL::next_power_of_2(128U + 1));
}

- (void)test_is_aligned {
    using T = double;
    alignas(T) char a[100];
    XCTAssert(UTL::is_aligned<alignof(T)>(a[0]) && !UTL::is_aligned<alignof(T)>(a[1]));
}

- (void)testPowN {
    try {
        constexpr double x = 2.1,
        x0 = UTL::pow<0>(x),
        x1 = UTL::pow<1>(x),
        x2 = UTL::pow<2>(x),
        x3 = UTL::pow<3>(x),
        x4 = UTL::pow<4>(x),
        x5 = UTL::pow<5>(x),
        x12 = UTL::pow<12>(x),
        x13 = UTL::pow<13>(x);

        XCTAssert(1 == x0);
        XCTAssert(x == x1);
        XCTAssert(x*x == x2);
        XCTAssert(x2*x == x3);
        XCTAssert(x2*x2 == x4);
        XCTAssert(x3*x2 == x5);
        XCTAssert(x4*x4*x4 == x12);
        XCTAssert(x12*x == x13);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
