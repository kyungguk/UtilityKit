//
//  UTLVector__0.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/27/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLVector__0_h
#define UTLVector__0_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <type_traits>
#include <algorithm>
#include <stdexcept>
#include <iterator>
#include <utility>
#include <array>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK: Forward Decls
    //
    template <class Type, long Size> struct Vector;
    template <class Type> struct Vector<Type, 1L>;
    template <class Type> struct Vector<Type, 2L>;
    template <class Type> struct Vector<Type, 3L>;
    template <class Type> struct Vector<Type, 4L>;
    template <class Type> struct Vector<Type, 5L>;
    template <class Type> struct Vector<Type, 6L>;

    // MARK: Scalar Comparison Result Type
    //
    template <class> struct _scalar_comparison_result                     { typedef bool      type; };
    template <     > struct _scalar_comparison_result<         char     > { typedef char      type; };
    template <     > struct _scalar_comparison_result<         short    > { typedef short     type; };
    template <     > struct _scalar_comparison_result<         int      > { typedef int       type; };
    template <     > struct _scalar_comparison_result<         long     > { typedef long      type; };
    template <     > struct _scalar_comparison_result<         long long> { typedef long long type; };
    template <     > struct _scalar_comparison_result<unsigned char     > { typedef char      type; };
    template <     > struct _scalar_comparison_result<unsigned short    > { typedef short     type; };
    template <     > struct _scalar_comparison_result<unsigned int      > { typedef int       type; };
    template <     > struct _scalar_comparison_result<unsigned long     > { typedef long      type; };
    template <     > struct _scalar_comparison_result<unsigned long long> { typedef long long type; };
    template <     > struct _scalar_comparison_result<         float    > { typedef int       type; };
    template <     > struct _scalar_comparison_result<         double   > { typedef long      type; };

    // MARK: Helper to Construct Comparison Result Type of Nested Vector
    //
    template <class T        > struct _comparison_result               { using type = typename _scalar_comparison_result<T>::type; };
    template <class T, long S> struct _comparison_result<Vector<T, S>> { using type = Vector<typename _comparison_result<T>::type, S>; };

    // MARK: Helper to Extract the Element Type of Nested Vector
    //
    template <class T        > struct _Innermost               { using type = T; };
    template <class T, long S> struct _Innermost<Vector<T, S>> { using type = typename _Innermost<T>::type; };

    // MARK: Reductions
    //
    template <class T, long S> inline
    T reduce_plus(Vector<T, S> const& v) noexcept(noexcept(v.reduce_plus())) {
        return v.reduce_plus();
    }
    template <class T, long S> inline
    T reduce_prod(Vector<T, S> const& v) noexcept(noexcept(v.reduce_prod())) {
        return v.reduce_prod();
    }

    template <class T, long S> inline
    T reduce_bit_and(Vector<T, S> const& v) noexcept(noexcept(v.reduce_bit_and())) {
        return v.reduce_bit_and();
    }
    template <class T, long S> inline
    T reduce_bit_or(Vector<T, S> const& v) noexcept(noexcept(v.reduce_bit_or())) {
        return v.reduce_bit_or();
    }
    template <class T, long S> inline
    T reduce_bit_xor(Vector<T, S> const& v) noexcept(noexcept(v.reduce_bit_xor())) {
        return v.reduce_bit_xor();
    }
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLVector__0.hh>

#endif /* UTLVector__0_h */
