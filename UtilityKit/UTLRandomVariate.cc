//
//  UTLRandomVariate.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLRandomVariate.h"

// MARK: Version 3
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/UTLRandomReal.h>


UTL::__3_::RandomVariate::RandomVariate(RandomReal *rng, Distribution const *dist) noexcept
: _rng(rng), _dist(dist) {
}

auto UTL::__3_::RandomVariate::operator()() const
-> value_type {
    return _dist->icdf( (*_rng)() );
}


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/UTLRandomReal.h>

UTL::__4_::RandomVariate::RandomVariate(RandomReal *rng, Distribution const *dist) noexcept
: _rng(rng), _dist(dist) {
}

auto UTL::__4_::RandomVariate::operator()() const
-> value_type {
    return _dist->icdf( (*_rng)() );
}
