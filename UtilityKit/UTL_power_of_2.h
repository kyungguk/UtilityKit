//
//  UTL_power_of_2.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 12/21/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTL_power_of_2_h
#define UTL_power_of_2_h

#include <UtilityKit/UtilityKit-config.h>
#include <type_traits>
#include <limits>

UTILITYKIT_BEGIN_NAMESPACE
namespace {
    /**
     Check whether integer is a power of 2.
     */
    template <class Integral>
    constexpr bool is_power_of_2(Integral const i) noexcept {
        static_assert(std::is_integral<Integral>::value, "argument type is not an integral type");
        return (i > 0) && !(i & (i-1));
    }

    /**
     Get the next power of 2.
     */
    template <class UIntegral>
    inline UIntegral next_power_of_2(UIntegral const _v) noexcept {
        static_assert(std::is_unsigned<UIntegral>::value, "argument type is not an unsigned integral type");
        if (_v <= 2) return UIntegral{2};
        UIntegral v{_v};
        --v;
        for (long i = 1; i < std::numeric_limits<UIntegral>::digits; i *= 2) {
            v |= v >> i;
        }
        return ++v;
    }
}
UTILITYKIT_END_NAMESPACE

#endif /* UTL_power_of_2_h */
