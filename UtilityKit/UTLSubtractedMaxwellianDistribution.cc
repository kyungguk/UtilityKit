//
//  UTLSubtractedMaxwellianDistribution.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLSubtractedMaxwellianDistribution.h"

// MARK: Version 3
//
#include <utility>
#include <cmath>
#include <stdexcept>
#include <string>

#pragma mark class implementation
UTL::__3_::SubtractedMaxwellianDistribution::SubtractedMaxwellianDistribution(std::pair<value_type, value_type> n, std::pair<value_type, value_type> vth, bool should_normalize_cdf)
: Distribution(), _icdf(), _n(n), _vth(vth) {
    if (_n.first < 0 || _n.second < 0 || std::abs(_n.first - (_n.second+1)) > 1e-5) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid concentration(s)");
    }
    if (_vth.first <= 0 || _vth.second <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid thermal speed(s)");
    }
    if (_n.second/_n.first >= std::pow(_vth.second/_vth.first, 3)) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative phase space density");
    }

    // sampler:
    sampler_type sampler = this->_sampler();

    // sample cdf:
    constexpr value_type x_max = 5;
    sampler_type::point_list_type
    cdf_pts = sampler([this](value_type const vOvth1)->value_type {
        return this->_helper_cdf(vOvth1);
    }, 0, x_max, 1);

    // optional normalization:
    if (should_normalize_cdf) {
        // normalize cdf:
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        sampler_type::point_list_type::iterator
        first = cdf_pts.begin(), last = cdf_pts.end();
        for (; first != last; ++first) {
            first->second = (first->second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    sampler_type::point_list_type::iterator
    first = cdf_pts.begin(), last = cdf_pts.end();
    for (; first != last; ++first) {
        std::swap(first->second, first->first);
    }
    _icdf = interp_type(cdf_pts.begin(), cdf_pts.end());
}

UTL::__3_::SubtractedMaxwellianDistribution::SubtractedMaxwellianDistribution(SubtractedMaxwellianDistribution const &o) noexcept
: Distribution(), _icdf(o._icdf), _n(o._n), _vth(o._vth) {
}
auto UTL::__3_::SubtractedMaxwellianDistribution::operator=(SubtractedMaxwellianDistribution const &o) noexcept
-> SubtractedMaxwellianDistribution &{
    if (this != &o) {
        _icdf = o._icdf;
        _n = o._n;
        _vth = o._vth;
    }
    return *this;
}
UTL::__3_::SubtractedMaxwellianDistribution::SubtractedMaxwellianDistribution(SubtractedMaxwellianDistribution &&o) noexcept
: Distribution(), _icdf(std::move(o._icdf)), _n(std::move(o._n)), _vth(std::move(o._vth)) {
}
auto UTL::__3_::SubtractedMaxwellianDistribution::operator=(SubtractedMaxwellianDistribution &&o) noexcept
-> SubtractedMaxwellianDistribution &{
    if (this != &o) {
        _icdf = std::move(o._icdf);
        _n = std::move(o._n);
        _vth = std::move(o._vth);
    }
    return *this;
}

auto UTL::__3_::SubtractedMaxwellianDistribution::mean() const noexcept
-> value_type {
    return M_2_SQRTPI * (_n.first*_vth.first - _n.second*_vth.second);
}
auto UTL::__3_::SubtractedMaxwellianDistribution::variance() const noexcept
-> value_type {
    return 1.5*(_n.first*_vth.first*_vth.first - _n.second*_vth.second*_vth.second) - mean()*mean();
}

auto UTL::__3_::SubtractedMaxwellianDistribution::pdf(value_type v) const
-> value_type {
    value_type const x1 = v/_vth.first, x2 = v/_vth.second;
    value_type const f1 = v*v*std::exp(-x1*x1) * 2*M_2_SQRTPI/(_vth.first *_vth.first *_vth.first );
    value_type const f2 = v*v*std::exp(-x2*x2) * 2*M_2_SQRTPI/(_vth.second*_vth.second*_vth.second);
    return _n.first*f1 - _n.second*f2;
}
auto UTL::__3_::SubtractedMaxwellianDistribution::cdf(value_type v) const
-> value_type {
    return _helper_cdf(v/_vth.first);
}
auto UTL::__3_::SubtractedMaxwellianDistribution::icdf(value_type cdf) const
-> value_type {
    return _icdf(cdf)*_vth.first;
}

auto UTL::__3_::SubtractedMaxwellianDistribution::_helper_cdf(value_type x/*v/vth1*/) const noexcept
-> value_type {
    // note that the definition of CDF in the comment!!
    value_type const cdf1 = std::erf(x) - M_2_SQRTPI*x*std::exp(-x*x);
    x *= _vth.first/_vth.second;
    value_type const cdf2 = std::erf(x) - M_2_SQRTPI*x*std::exp(-x*x);
    return _n.first*cdf1 - _n.second*cdf2;
}

auto UTL::__3_::SubtractedMaxwellianDistribution::_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.maxRecursion = 20;
        sampler.accuracyGoal = 7;
        sampler.initialPoints = 100;
        sampler.yScaleAbsoluteTolerance = 1e-10;
    }
    return sampler;
}


// MARK:- Version 4
//
#include <UtilityKit/AuxiliaryKit.h>
#include <stdexcept>
#include <utility>
#include <string>
#include <cmath>

UTL::__4_::SubtractedMaxwellianDistribution::SubtractedMaxwellianDistribution(SubtractedMaxwellianDistribution const &o)
: Distribution(), _vth(o._vth), _n(o._n), _icdf(o._icdf) {
}
UTL::__4_::SubtractedMaxwellianDistribution::SubtractedMaxwellianDistribution(SubtractedMaxwellianDistribution &&o)
: Distribution(), _vth(o._vth), _n(o._n), _icdf() {
    _icdf = std::move(o._icdf);
}
auto UTL::__4_::SubtractedMaxwellianDistribution::operator=(SubtractedMaxwellianDistribution const &o)
-> SubtractedMaxwellianDistribution &{
    if (this != &o) {
        SubtractedMaxwellianDistribution{o}.swap(*this);
    }
    return *this;
}
auto UTL::__4_::SubtractedMaxwellianDistribution::operator=(SubtractedMaxwellianDistribution &&o)
-> SubtractedMaxwellianDistribution &{
    if (this != &o) {
        SubtractedMaxwellianDistribution{std::move(o)}.swap(*this);
    }
    return *this;
}

void UTL::__4_::SubtractedMaxwellianDistribution::swap(SubtractedMaxwellianDistribution &o)
{
    _icdf.swap(o._icdf);
    std::swap(_vth, o._vth);
    std::swap(_n, o._n);
}

auto UTL::__4_::SubtractedMaxwellianDistribution::mean() const noexcept
-> value_type {
    return M_2_SQRTPI * reduce_plus(_n*_vth*vector_type{1, -1});
}
auto UTL::__4_::SubtractedMaxwellianDistribution::variance() const noexcept
-> value_type {
    return 1.5*reduce_plus(_n*_vth*_vth*vector_type{1, -1}) - pow<2>(mean());
}

auto UTL::__4_::SubtractedMaxwellianDistribution::pdf(value_type const v) const noexcept
-> value_type {
    vector_type const x = v/_vth;
    return reduce_plus(_n*vector_type{gaussian_pdf(x.x), -gaussian_pdf(x.y)} * x*x/_vth);
}
auto UTL::__4_::SubtractedMaxwellianDistribution::cdf(value_type const v) const noexcept
-> value_type {
    vector_type const x = v/_vth;
    return reduce_plus(_n*vector_type{gaussian_cdf(x.x), -gaussian_cdf(x.y)});
}
auto UTL::__4_::SubtractedMaxwellianDistribution::icdf(value_type const cdf) const
-> value_type {
    try {
        return vth<1>()*_icdf(cdf)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

auto UTL::__4_::SubtractedMaxwellianDistribution::default_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.set_max_recursion(20);
        sampler.set_accuracy_goal(7);
        sampler.set_initial_points(100);
        sampler.set_y_scale_absolute_tolerance(1e-10);
    }
    return sampler;
}

UTL::__4_::SubtractedMaxwellianDistribution::SubtractedMaxwellianDistribution(value_type const vth1, value_type const T2OT1, value_type const n2On1)
: Distribution(), _vth(1, std::sqrt(T2OT1)), _n(1, n2On1), _icdf() {
    // argument checks:
    if (vth1 <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid vth1");
    }
    if (T2OT1 <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid T2/T1 ratio");
    }
    if (n2On1 < 0 || n2On1 >= 1) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid n2/n1 ratio");
    }
    if (n2On1 >= pow<3>(vth<2>()/vth<1>())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative phase space density");
    }
    _vth *= vth1;
    _n.x /= 1 - n2On1;
    _n.y *= _n.x;

    // sampler:
    sampler_type const sampler = default_sampler();

    // sample cdf:
    constexpr value_type x_max = 5; // normalized to vth1
    sampler_type::point_list_type
    cdf_pts = sampler([this](value_type const vOvth1)->value_type {
        return this->cdf(vOvth1*this->vth1());
    }, 0, x_max, 1);

    // re-normalization:
    {
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        for (auto &pt : cdf_pts) {
            pt.second = (pt.second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    for (auto &pt : cdf_pts) {
        std::swap(pt.first, pt.second);
    }
    this->_icdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
}

auto UTL::__4_::SubtractedMaxwellianDistribution::gaussian_pdf(value_type const x) noexcept
-> value_type {
    // note that x is not multiplied!
    return std::exp(-x*x)*(2*M_2_SQRTPI);
}
auto UTL::__4_::SubtractedMaxwellianDistribution::gaussian_cdf(value_type const x) noexcept
-> value_type {
    return std::erf(x) - .5*x*gaussian_pdf(x);
}
