//
//  UTLVector__6.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/27/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLVector__6_h
#define UTLVector__6_h

#include <UtilityKit/UTLVector__0.h>

// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK: Vector<6>
    //
    /**
     Three element vector specialization.
     */
    template <class Type>
    struct Vector<Type, 6L> {
        // types:
        using value_type             = Type;
        using reference              = Type&;
        using const_reference        = Type const&;
        using pointer                = Type*;
        using const_pointer          = Type const*;
        using iterator               = pointer;
        using const_iterator         = const_pointer;
        using reverse_iterator       = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;
        using size_type              = long;
        using difference_type        = long;
        using size_vector_type       = Vector<size_type, 1L>;
        using comparison_result_type = Vector<typename _comparison_result<Type>::type, 6L>;
        using innermost_element_type = typename _Innermost<Type>::type; // the innermost element type if Type is a vector (whose value_type may be vector as well)

        // elements:
        Type x{};
        Type y{};
        Type z{};
        Type u{};
        Type v{};
        Type w{};

        // capacity:
        static constexpr size_type size() noexcept { return size_type{6}; }
        static constexpr size_type max_size() noexcept { return size(); }
        static constexpr size_type rank() noexcept { return size_vector_type::size(); }
        static constexpr size_vector_type dims() noexcept { return {size()}; }
        static constexpr size_vector_type max_dims() noexcept { return dims(); }

        // constructors:
        constexpr explicit Vector() noexcept(std::is_nothrow_default_constructible<Type>::value) {}
        constexpr explicit Vector(Type const& s) noexcept(std::is_nothrow_copy_constructible<Type>::value) : x(s), y(s), z(s), u(s), v(s), w(s) {}
        template <class X, class Y, class Z, class U, class V, class W>
        constexpr Vector(X&& _x, Y&& _y, Z&& _z, U&& _u, V&& _v, W&& _w)
        noexcept(std::is_nothrow_constructible<Type, X&&>::value &&
                 std::is_nothrow_constructible<Type, Y&&>::value &&
                 std::is_nothrow_constructible<Type, Z&&>::value &&
                 std::is_nothrow_constructible<Type, U&&>::value &&
                 std::is_nothrow_constructible<Type, V&&>::value &&
                 std::is_nothrow_constructible<Type, W&&>::value) :
        x(std::forward<X>(_x)),
        y(std::forward<Y>(_y)),
        z(std::forward<Z>(_z)),
        u(std::forward<U>(_u)),
        v(std::forward<V>(_v)),
        w(std::forward<V>(_w)) {}

        // copy/move:
        Vector(Vector const& o) noexcept(std::is_nothrow_copy_constructible<Type>::value) : Vector(o.x, o.y, o.z, o.u, o.v, o.w) {}
        inline Vector& operator=(Vector const& o) noexcept(std::is_nothrow_copy_assignable<Type>::value);
        Vector(Vector&& o) noexcept(std::is_nothrow_move_constructible<Type>::value) : Vector(std::move(o.x), std::move(o.y), std::move(o.z), std::move(o.u), std::move(o.v), std::move(o.w)) {}
        inline Vector& operator=(Vector&& o) noexcept(std::is_nothrow_move_assignable<Type>::value);
        template <class U, typename std::enable_if<std::is_convertible<U, Type>::value, long>::type = 0L>
        explicit Vector(Vector<U, size()> const& o) noexcept(std::is_nothrow_constructible<Type, U const&>::value) : Vector(o.x, o.y, o.z, o.u, o.v, o.w) {}
        template <class U>
        auto operator=(Vector<U, size()> const& o) -> typename std::enable_if<std::is_convertible<U, Type>::value, Vector>::type &{ return *this = Vector{o}; }

        // modifiers:
        void fill(Type const& s) noexcept(std::is_nothrow_copy_assignable<Type>::value) { w = v = u = z = y = x = s; }
        void swap(Vector& o) noexcept(noexcept(std::swap(x, o.x))) {
            std::swap(x, o.x);
            std::swap(y, o.y);
            std::swap(z, o.z);
            std::swap(u, o.u);
            std::swap(v, o.v);
            std::swap(w, o.w);
        }

        // cast:
        operator std::array<Type, Vector::size()> const&() const& noexcept { return *reinterpret_cast<std::array<Type, Vector::size()> const*>(this); }
        operator std::array<Type, Vector::size()>      &()      & noexcept { return *reinterpret_cast<std::array<Type, Vector::size()>      *>(this); }

        // element access:
        reference       back ()       noexcept { return w; }
        const_reference back () const noexcept { return w; }
        reference       front()       noexcept { return x; }
        const_reference front() const noexcept { return x; }

        pointer       data()       noexcept { return reinterpret_cast<      pointer>(this); }
        const_pointer data() const noexcept { return reinterpret_cast<const_pointer>(this); }

#if defined(DEBUG)
        reference       operator[](size_type const i)       { return this->at(i); }
        const_reference operator[](size_type const i) const { return this->at(i); }
#else
        reference       operator[](size_type const i)       noexcept { return data()[i]; }
        const_reference operator[](size_type const i) const noexcept { return data()[i]; }
#endif

        inline reference       at(size_type const i);
        inline const_reference at(size_type const i) const;

        // iterators:
        iterator        begin()       noexcept { return       iterator{data()}; }
        const_iterator  begin() const noexcept { return const_iterator{data()}; }
        const_iterator cbegin() const noexcept { return begin(); }
        iterator        end  ()       noexcept { return       iterator{data() + Vector::size()}; }
        const_iterator  end  () const noexcept { return const_iterator{data() + Vector::size()}; }
        const_iterator cend  () const noexcept { return end(); }

        reverse_iterator        rbegin()       noexcept { return       reverse_iterator{end()}; }
        const_reverse_iterator  rbegin() const noexcept { return const_reverse_iterator{end()}; }
        const_reverse_iterator crbegin() const noexcept { return rbegin(); }
        reverse_iterator        rend  ()       noexcept { return       reverse_iterator{begin()}; }
        const_reverse_iterator  rend  () const noexcept { return const_reverse_iterator{begin()}; }
        const_reverse_iterator crend  () const noexcept { return rend(); }

        // half vectors:
        using lo_vector_type = Vector<Type, Vector::size()/2>;
        lo_vector_type      & lo()       noexcept { return reinterpret_cast<lo_vector_type      *>(this)[0]; }
        lo_vector_type const& lo() const noexcept { return reinterpret_cast<lo_vector_type const*>(this)[0]; }
        using hi_vector_type = lo_vector_type;
        hi_vector_type      & hi()       noexcept { return reinterpret_cast<hi_vector_type      *>(this)[1]; }
        hi_vector_type const& hi() const noexcept { return reinterpret_cast<hi_vector_type const*>(this)[1]; }

        // symmetric/anti-symmetric tensor elements:
        reference xx() noexcept { return x; }
        reference yy() noexcept { return y; }
        reference zz() noexcept { return z; }
        reference xy() noexcept { return u; }
        reference yz() noexcept { return v; }
        reference zx() noexcept { return w; }

        const_reference xx() const noexcept { return x; }
        const_reference yy() const noexcept { return y; }
        const_reference zz() const noexcept { return z; }
        const_reference xy() const noexcept { return u; }
        const_reference yz() const noexcept { return v; }
        const_reference zx() const noexcept { return w; }

        // transformations:
        // ** take const **
        template <long N> typename std::enable_if<(N > 0), Vector<Type, N>
        >::type const &take() const noexcept { static_assert(N <= Vector::size(), "cannot take more than already have");
            return *reinterpret_cast<Vector<Type, N> const*>(this);
        }
        template <long M> typename std::enable_if<(M < 0), Vector<Type,-M>
        >::type const &take() const noexcept { static_assert(M >=-Vector::size(), "cannot take more than already have");
            return *(reinterpret_cast<Vector<Type,-M> const*>(this + 1) - 1);
        }
        // ** take **
        template <long N> typename std::enable_if<(N > 0), Vector<Type, N>
        >::type &take() noexcept { static_assert(N <= Vector::size(), "cannot take more than already have");
            return *reinterpret_cast<Vector<Type, N> *>(this);
        }
        template <long M> typename std::enable_if<(M < 0), Vector<Type,-M>
        >::type &take() noexcept { static_assert(M >=-Vector::size(), "cannot take more than already have");
            return *(reinterpret_cast<Vector<Type,-M> *>(this + 1) - 1);
        }
        // ** drop const **
        template <long N> typename std::enable_if<(N >= 0), Vector<Type, Vector::size() - N>
        >::type const &drop() const noexcept { static_assert(N < Vector::size(), "cannot drop all");
            return this->template take<N - Vector::size()>();
        }
        template <long M> typename std::enable_if<(M < 0), Vector<Type, Vector::size() + M>
        >::type const &drop() const noexcept { static_assert(M >-Vector::size(), "cannot drop all");
            return this->template take<M + Vector::size()>();
        }
        // ** drop **
        template <long N> typename std::enable_if<(N >= 0), Vector<Type, Vector::size() - N>
        >::type &drop() noexcept { static_assert(N < Vector::size(), "cannot drop all");
            return this->template take<N - Vector::size()>();
        }
        template <long M> typename std::enable_if<(M < 0), Vector<Type, Vector::size() + M>
        >::type &drop() noexcept { static_assert(M >-Vector::size(), "cannot drop all");
            return this->template take<M + Vector::size()>();
        }

        Vector<Type, Vector::size() - 1>       &most()       noexcept { return drop<-1>(); }
        Vector<Type, Vector::size() - 1> const &most() const noexcept { return drop<-1>(); }
        Vector<Type, Vector::size() - 1>       &rest()       noexcept { return drop<+1>(); }
        Vector<Type, Vector::size() - 1> const &rest() const noexcept { return drop<+1>(); }

        Vector<Type, Vector::size() + 1>  append(Type const& x) const { decltype(append(x)) v{}; v.most() = *this; v.back () = x; return v; }
        Vector<Type, Vector::size() + 1> prepend(Type const& x) const { decltype(append(x)) v{}; v.rest() = *this; v.front() = x; return v; }

        // reductions:
        inline Type reduce_plus   () const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Type reduce_prod   () const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Type reduce_bit_and() const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Type reduce_bit_or () const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Type reduce_bit_xor() const noexcept(std::is_arithmetic<innermost_element_type>::value);

        // compound arithmetic:
        inline Vector &operator+=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator+=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator-=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator-=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator*=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator*=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator/=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator/=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // compound modulus:
        inline Vector &operator%=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator%=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // compound bit operators:
        inline Vector &operator&=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator&=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator|=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator|=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator^=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator^=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator<<=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator<<=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator>>=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator>>=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // unary:
        Vector const &operator+() const noexcept { return *this; }
        Vector        operator-() const noexcept(std::is_arithmetic<innermost_element_type>::value) { return Vector(-x, -y, -z, -u, -v, -w); }
        // comparison (element-wise):
        inline comparison_result_type operator==(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator!=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator<=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator>=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator<(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator>(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLVector__6.hh>

#endif /* UTLVector__6_h */
