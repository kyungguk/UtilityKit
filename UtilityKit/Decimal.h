//
//  Decimal.h
//  ParticleInCell
//
//  Created by Kyungguk Min on 5/29/16.
//
//

#ifndef Decimal_h
#define Decimal_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <type_traits>
#include <limits>
#include <cmath>
#include <stdexcept>
#include <sstream>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

/**
 Floating-point number represented by an integer part and a fractional part.
 */
template <class _Int, class _Frac>
struct Decimal {
    static_assert(std::is_integral<_Int>::value, "integer part should be of integral type");
    static_assert(std::is_floating_point<_Frac>::value, "fractional part should be of floating-point type");

    using integer_type = _Int;
    using fraction_type = _Frac;

    fraction_type f; //!< Fractional part.
    integer_type i; //!< Integer part.

    // type casts:
    explicit operator double() const noexcept { return double(i) + f; }

    // constructors:
    /**
     Default constructor.
     */
    constexpr Decimal() noexcept : f(0), i(0) {}
    /**
     Construct with an integer part and set the fractional part a default value.
     */
    constexpr Decimal(integer_type integer_part) noexcept : f(0), i(integer_part) {}
    /**
     @brief Construct with both integer and fractional parts.
     The fractional part is first floored and then its integer part is added to the given integer part.
     @note It does not check the integer overflow.
     */
    Decimal(integer_type integer_part, fraction_type fractional_part) noexcept;
    /**
     Construct with a floating-point number.
     
     It raises an invalid_argument exception when integer overflow.
     */
    Decimal(double x);
    constexpr Decimal(unsigned long u) noexcept : Decimal(integer_type(u)) {}
};

// out-of-line definitions:
template <class _Int, class _Frac>
Decimal<_Int, _Frac>::Decimal(integer_type integer_part, fraction_type fractional_part) noexcept
{
    i = integer_type( std::floor(fractional_part) );
    f = fraction_type( fractional_part - i );
    i += integer_part;
}
template <class _Int, class _Frac>
Decimal<_Int, _Frac>::Decimal(double x)
{
    if (x <= std::numeric_limits<_Int>::min() || x > std::numeric_limits<_Int>::max()) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
    i = integer_type(std::floor(x));
    f = fraction_type(x - i);
}

#pragma mark - Arithematic Compounds
// decimal += decimal:
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator+=(Decimal<Int, Frac>& a, Decimal<Int, Frac> const& b) {
    return a = Decimal<Int, Frac>(a.i+b.i, a.f+b.f);
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator-=(Decimal<Int, Frac>& a, Decimal<Int, Frac> const& b) {
    return a = Decimal<Int, Frac>(a.i-b.i, a.f-b.f);
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator*=(Decimal<Int, Frac>& a, Decimal<Int, Frac> const& b) {
    return a = Decimal<Int, Frac>(double(a)*double(b));
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator/=(Decimal<Int, Frac>& a, Decimal<Int, Frac> const& b) {
    return a = Decimal<Int, Frac>(double(a)/double(b));
}
// decimal += double:
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator+=(Decimal<Int, Frac>& a, double const& b) {
    return a += Decimal<Int, Frac>(b);
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator-=(Decimal<Int, Frac>& a, double const& b) {
    return a -= Decimal<Int, Frac>(b);
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator*=(Decimal<Int, Frac>& a, double const& b) {
    return a *= Decimal<Int, Frac>(b);
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator/=(Decimal<Int, Frac>& a, double const& b) {
    return a /= Decimal<Int, Frac>(b);
}
// decimal += int:
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator+=(Decimal<Int, Frac>& a, Int const& b) {
    return a += Decimal<Int, Frac>(b);
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator-=(Decimal<Int, Frac>& a, Int const& b) {
    return a -= Decimal<Int, Frac>(b);
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator*=(Decimal<Int, Frac>& a, Int const& b) {
    return a *= Decimal<Int, Frac>(b);
}
template <class Int, class Frac> inline
Decimal<Int, Frac>& operator/=(Decimal<Int, Frac>& a, Int const& b) {
    return a /= Decimal<Int, Frac>(b);
}

#pragma mark Arithematic Unary
template <class Int, class Frac> inline
Decimal<Int, Frac> const& operator+(Decimal<Int, Frac> const& a) {
    return a;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator-(Decimal<Int, Frac> const& b) {
    return {-b.i, -b.f};
}

#pragma mark Arithematic Binary
// +:
template <class Int, class Frac> inline
Decimal<Int, Frac> operator+(Decimal<Int, Frac> a, Decimal<Int, Frac> const& b) {
    return a += b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator+(Decimal<Int, Frac> a, double const& b) {
    return a += b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator+(double const& s, Decimal<Int, Frac> a) {
    return a += s;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator+(Decimal<Int, Frac> a, Int const& b) {
    return a += b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator+(Int const& s, Decimal<Int, Frac> a) {
    return a += s;
}
// -:
template <class Int, class Frac> inline
Decimal<Int, Frac> operator-(Decimal<Int, Frac> a, Decimal<Int, Frac> const& b) {
    return a -= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator-(Decimal<Int, Frac> a, double const& b) {
    return a -= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator-(double const& s, Decimal<Int, Frac> const& b) {
    Decimal<Int, Frac> a(s);
    return a -= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator-(Decimal<Int, Frac> a, Int const& b) {
    return a -= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator-(Int const& s, Decimal<Int, Frac> const& b) {
    Decimal<Int, Frac> a(s);
    return a -= b;
}
// *:
template <class Int, class Frac> inline
Decimal<Int, Frac> operator*(Decimal<Int, Frac> a, Decimal<Int, Frac> const& b) {
    return a *= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator*(Decimal<Int, Frac> a, double const& b) {
    return a *= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator*(double const& s, Decimal<Int, Frac> a) {
    return a *= s;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator*(Decimal<Int, Frac> a, Int const& b) {
    return a *= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator*(Int const& s, Decimal<Int, Frac> a) {
    return a *= s;
}
// /:
template <class Int, class Frac> inline
Decimal<Int, Frac> operator/(Decimal<Int, Frac> a, Decimal<Int, Frac> const& b) {
    return a /= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator/(Decimal<Int, Frac> a, double const& b) {
    return a /= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator/(double const& s, Decimal<Int, Frac> const& b) {
    Decimal<Int, Frac> a(s);
    return a /= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator/(Decimal<Int, Frac> a, Int const& b) {
    return a /= b;
}
template <class Int, class Frac> inline
Decimal<Int, Frac> operator/(Int const& s, Decimal<Int, Frac> const& b) {
    Decimal<Int, Frac> a(s);
    return a /= b;
}


#pragma mark output stream operator:
template<class _CharT, class _Traits, class _Int, class _Frac>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, Decimal<_Int, _Frac> const& dc)
{
    std::basic_ostringstream<_CharT, _Traits> ss; {
        ss.flags(os.flags() | os.fixed);
        ss.imbue(os.getloc());
        ss.precision(os.precision());
    }
    ss << dc.f;
    std::string const frac = ss.str();
    return os << dc.i << frac.substr(1);
}

} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* Decimal_h */
