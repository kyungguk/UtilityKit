//
//  UTLSplineInterpolator.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/22/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSplineInterpolator_hh
#define UTLSplineInterpolator_hh

// MARK:- Version 4
//
#include <algorithm>
#include <cmath>

template <class RealType, long Order>
void UTL::__4_::SplineInterpolator<RealType, Order>::swap(SplineInterpolator& other)
{
    _coef.swap(other._coef);
    std::swap(_delta_or_correlation_tolerance, other._delta_or_correlation_tolerance);
    std::swap(_locator, other._locator);
    std::swap(_state, other._state);
}

template <class RealType, long Order>
UTL::__4_::SplineInterpolator<RealType, Order>& UTL::__4_::SplineInterpolator<RealType, Order>::operator=(SplineInterpolator const &o)
{
    if (this != &o) {
        SplineInterpolator{o}.swap(*this);
    }
    return *this;
}

template <class RealType, long Order>
template <class Coef>
UTL::__4_::SplineInterpolator<RealType, Order>::SplineInterpolator(decltype(nullptr), Coef&& coef)
{
    if (!coef) return;
    _coef = std::forward<Coef>(coef);

    // choose locator based on grid regularity
    //
    if (_coef.delta()) { // regular abscissa
        _delta_or_correlation_tolerance = *_coef.delta();
        _locator = &SplineInterpolator::_locator_regular;
    } else { // irregular abscissa
        _delta_or_correlation_tolerance = 1; //std::min(value_type{1}, std::pow(_coef.table().size(), value_type(0.25)));
        _locator = &SplineInterpolator::_locator_irregular;
    }
}

template <class RealType, long Order>
auto UTL::__4_::SplineInterpolator<RealType, Order>::interpolate(value_type const x) const noexcept
-> optional_type {
    if (x < _coef.min_abscissa() || x > _coef.max_abscissa()) {
        return {};
    }

    using size_type = unsigned long;
    size_type const i = size_type((this->*_locator)(x));
    auto const &pair = _coef.table()[i];
    value_type const d = _coef.table()[i+1].first - pair.first;
    value_type const t = (x - pair.first) / d;
    return _interpolate_eval_poly(pair.second, t);
}
template <class RealType, long Order>
auto UTL::__4_::SplineInterpolator<RealType, Order>::derivative(value_type const x) const noexcept
-> optional_type {
    if (x < _coef.min_abscissa() || x > _coef.max_abscissa()) {
        return {};
    }

    using size_type = unsigned long;
    size_type const i = size_type((this->*_locator)(x));
    auto const &pair = _coef.table()[i];
    value_type const d = _coef.table()[i+1].first - pair.first;
    value_type const t = (x - pair.first) / d;
    return _derivative_eval_poly(pair.second, t) / d;
}

template <class RealType, long Order>
long UTL::__4_::SplineInterpolator<RealType, Order>::_locator_regular(value_type const x) const noexcept
{
    // x is assumed to be within the abscissa range
    //
    if (x == _coef.max_abscissa()) {
        return static_cast<long>(_coef.table().size() - 2);
    } else {
        return static_cast<long>((x - _coef.min_abscissa())/_delta());
    }
}
template <class RealType, long Order>
long UTL::__4_::SplineInterpolator<RealType, Order>::_locator_irregular(value_type const x) const noexcept
{
    // x is assumed to be within the abscissa range
    //
    LocatorState s = _state; // copy of previous state; local scope
    long const loc = s.correlated ? _hunt(x, s) : _locate(x, s);
    _state = s; // keep current state; this should be done atomically for multi-thread
    return loc;
}

template <class RealType, long Order>
long UTL::__4_::SplineInterpolator<RealType, Order>::_locate(value_type const x, LocatorState &s) const noexcept
{
    coefficient_table_type const &table = _coef.table();
    using size_type = unsigned long;
    long const n = long(table.size());
    long jl = 0;
    long ju = n - 1;
    while (ju - jl > 1) {
        long const jm = (ju + jl) >> 1; // mid point
        if (x >= table[size_type(jm)].first) {
            jl = jm;
        } else {
            ju = jm;
        }
    }
    s.correlated = std::abs((jl - s.jsave)/_correlation_tolerance()) < value_type{1};
    s.jsave = jl;
    return std::max(long{0}, std::min(n - 2, jl)); // 0 <= loc < size() - 1
}
template <class RealType, long Order>
long UTL::__4_::SplineInterpolator<RealType, Order>::_hunt(value_type const x, LocatorState &s) const noexcept
{
    coefficient_table_type const &table = _coef.table();
    using size_type = unsigned long;
    long const n = long(table.size());
    long jl = s.jsave, inc = 1, ju = -1;
    if (jl < 0 || jl > n - 1) {
        jl = 0;
        ju = n - 1;
    } else {
        if (x >= table[size_type(jl)].first) { // hunt up
            for (; ; ) {
                ju = jl + inc;
                if (ju >= n - 1) {
                    ju = n - 1;
                    break;
                } else if (x < table[size_type(ju)].first) {
                    break;
                } else {
                    jl = ju;
                    inc += inc;
                }
            }
        } else { // hunt down
            ju = jl;
            for (; ; ) {
                jl = ju - inc;
                if (jl <= 0) {
                    jl = 0;
                    break;
                } else if (x >= table[size_type(jl)].first) {
                    break;
                } else {
                    ju = jl;
                    inc += inc;
                }
            }
        }
    }
    while (ju - jl > 1) { // hunt is done, so do the bisection
        long const jm = (ju + jl) >> 1;
        if (x >= table[size_type(jm)].first) {
            jl = jm;
        } else {
            ju = jm;
        }
    }
    s.correlated = std::abs((jl - s.jsave)/_correlation_tolerance()) < value_type{1};
    s.jsave = jl;
    return std::max(long{0}, std::min(n - 2, jl)); // 0 <= loc < size() - 1
}

#endif /* UTLSplineInterpolator_hh */
