//
//  UTLStaticArray.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/15/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLStaticArray_hh
#define UTLStaticArray_hh

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <stdexcept>
#include <string>

template <class Type, long MaxSize, class OverlayT>
UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::StaticArray(size_type const sz)
{
    if (sz < 0 || sz > max_size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    pointer const ptr = _allocate();
    size_type i{0};
    try {
        for (; i < sz; ++i) {
            _construct(ptr + i);
        }
    } catch (...) {
        _destory(ptr, i);
        _deallocate(ptr);
        throw;
    }
    StaticArray{nullptr, ptr, sz}.swap(*this);
}
template <class Type, long MaxSize, class OverlayT>
UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::StaticArray(size_type const sz, value_type const& x)
{
    if (sz < 0 || sz > max_size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    pointer const ptr = _allocate();
    size_type i{0};
    try {
        for (; i < sz; ++i) {
            _construct(ptr + i, x);
        }
    } catch (...) {
        _destory(ptr, i);
        _deallocate(ptr);
        throw;
    }
    StaticArray{nullptr, ptr, sz}.swap(*this);
}

template <class Type, long MaxSize, class OverlayT>
template <class ForwardIt, typename std::enable_if<std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<ForwardIt>::iterator_category>::value, long>::type>
UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::StaticArray(ForwardIt first, ForwardIt last)
{
    size_type const sz = std::distance(first, last);
    if (sz < 0 || sz > max_size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - the number of elements is greater than the capacity");
    }
    pointer const ptr = _allocate();
    size_type i{0};
    try {
        for (; first != last; ++i) {
            _construct(ptr + i, *first++);
        }
    } catch (...) {
        _destory(ptr, i);
        _deallocate(ptr);
        throw;
    }
    StaticArray{nullptr, ptr, sz}.swap(*this);
}

template <class Type, long MaxSize, class OverlayT>
auto UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::operator=(StaticArray const &other)
-> StaticArray& {
    if (this != &other) {
        StaticArray{other}.swap(*this);
    }
    return *this;
}

template <class Type, long MaxSize, class OverlayT>
auto UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::operator=(StaticArray&& other) noexcept(noexcept(StaticArray(std::move(other))))
-> StaticArray& {
    if (this != &other) {
        StaticArray{std::move(other)}.swap(*this);
    }
    return *this;
}

template <class Type, long MaxSize, class OverlayT>
auto UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::operator=(std::initializer_list<value_type> const il)
-> StaticArray& {
    if (il.size() > max_size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - the number of elements is greater than the capacity");
    }
    return *this = StaticArray{il.begin(), il.end()};
}

template <class Type, long MaxSize, class OverlayT>
void UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::resize(size_type const sz)
{
    //if (!*this) throw
    if (sz < 0 || sz > max_size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    size_type delta = sz - this->size();
    for (; delta > 0; --delta) { // expansion
        _construct(this->end());
        ++this->_end; // increment after construction due to exception
    }
    for (; delta < 0; ++delta) { // shrink
        _destory(--this->_end);
    }
}
template <class Type, long MaxSize, class OverlayT>
void UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::resize(size_type const sz, value_type const &x)
{
    //if (!*this) throw
    if (sz < 0 || sz > max_size()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    size_type delta = sz - this->size();
    for (; delta > 0; --delta) { // expansion
        _construct(this->end(), x);
        ++this->_end; // increment after construction due to exception
    }
    for (; delta < 0; ++delta) { // shrink
        _destory(--this->_end);
    }
}

template <class Type, long MaxSize, class OverlayT>
template <class... Args>
auto UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::emplace_back(Args&&... args)
-> reference {
    //if (!*this) throw
    if (this->size() == max_size()) {
        throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - no space left");
    }
    _construct(this->end(), std::forward<Args>(args)...);
    return *this->_end++; // increment after construction due to exception
}

template <class Type, long MaxSize, class OverlayT>
template <class... Args>
auto UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::emplace(const_iterator pos, Args&&... args)
-> iterator {
    //if (!*this) throw
    if (this->size() == max_size()) {
        throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - no space left");
    }
    if ((pos < this->cbegin()) || (pos > this->cend())) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - not a valid iterator");
    }
    StaticArray tmp{};
    // move [begin(), pos)
    for (const_iterator first = this->cbegin(); first != pos; ++first) {
        tmp.emplace_back(std::move(*first));
    }
    // keep insertion point
    iterator const last = tmp.end();
    // insertion
    tmp.emplace_back(std::forward<Args>(args)...);
    // move [pos, end())
    while (pos != this->cend()) {
        tmp.emplace_back(std::move(*pos++));
    }
    // swap storage
    tmp.swap(*this);
    return last;
}

template <class Type, long MaxSize, class OverlayT>
auto UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::erase(const_iterator const pos1, const_iterator pos2)
-> iterator {
    //if (!*this) throw
    //if (pos1 == pos2) return pos2; // no-op
    if (pos1 > pos2 || pos1 < this->cbegin() || pos2 > this->cend()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - not a valid iterator");
    }
    StaticArray tmp{};
    // move[begin(), pos1)
    for (const_iterator first = this->cbegin(); first != pos1; ++first) {
        tmp.emplace_back(std::move(*first));
    }
    // keep iterator following the last removed element, i.e., pos2
    iterator const last = tmp.end();
    // move[pos2, end())
    while (pos2 != this->cend()) {
        tmp.emplace_back(std::move(*pos2++));
    }
    // swap storage
    tmp.swap(*this);
    return last;
}

template <class Type, long MaxSize, class OverlayT>
template <class Container, class Predicate>
auto UTL::__4_::StaticArray<Type, MaxSize, OverlayT>::evict_if(Container& bucket, Predicate&& pred)
-> size_type {
    iterator const first = this->shuffle_if(std::forward<Predicate>(pred));
    iterator it = first, last = this->end();
    while (it != last) {
        bucket.push_back(std::move(*it++));
    }
    erase(first, last);
    return std::distance(first, last);
}

// MARK: std::swap
//
namespace std {
    template <class Type, long MaxSize, class OverlayT>
    inline void swap(UTL::__4_::StaticArray<Type, MaxSize, OverlayT> &a, UTL::__4_::StaticArray<Type, MaxSize, OverlayT> &b) noexcept(noexcept(a.swap(b))) {
        a.swap(b);
    }
}

#endif /* UTLStaticArray_hh */
