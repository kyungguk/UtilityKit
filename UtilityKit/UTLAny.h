//
//  UTLAny.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/30/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLAny_h
#define UTLAny_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <type_traits>
#include <typeinfo>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief A type-safe container for single values of any type; simplified version of c++17 std::any.
     @discussion Implementation is based on "https://gist.github.com/Jefffrey/9202235", which itself is a simplified version of Boost's any.
     */
    class Any {
        class _PlaceHolder;

        std::unique_ptr<_PlaceHolder> _ptr;

    public:
        // bad cast exception:
        class bad_cast;

        ~Any();

        Any() noexcept : _ptr(nullptr) {}
        Any(Any const& o);
        Any(Any&& o) noexcept;
        template <class ValueType, typename std::enable_if<!std::is_same<typename std::decay<ValueType>::type, Any>::value, long>::type = 0L>
        inline Any(ValueType&& value);

        Any& operator=(Any const& rhs);
        Any& operator=(Any&& rhs) noexcept;
        template <class ValueType, typename std::enable_if<!std::is_same<typename std::decay<ValueType>::type, Any>::value, long>::type = 0L>
        inline Any& operator=(ValueType&& rhs);

        // modifiers:
        template <class ValueType, class... Args>
        inline auto emplace(Args&&... args) -> typename std::decay<ValueType>::type&;
        void reset() noexcept;
        void swap(Any& o) noexcept { _ptr.swap(o._ptr); }

        // observers:
        explicit operator bool() const noexcept { return has_value(); }
        bool has_value() const noexcept { return bool(_ptr); }
        const std::type_info& type() const noexcept;

        // casts:
        template <class ValueType>
        inline static ValueType cast(Any const& any);
        template <class ValueType>
        inline static ValueType cast(Any& any);
        template <class ValueType>
        inline static ValueType cast(Any&& any);
        template <class ValueType>
        inline static ValueType const* cast(Any const* any) noexcept;
        template <class ValueType>
        inline static ValueType* cast(Any* any) noexcept;

    private:
        template <class T>
        class _ValueWrapper;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLAny.hh>

#endif /* UTLAny_h */
