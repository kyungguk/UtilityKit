//
//  UTLBitReversedRandomReal.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLBitReversedRandomReal_h
#define UTLBitReversedRandomReal_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLRandomReal.h>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief From bit reversed algorithm from Birdsall and Langdon (1985); taken from Kaijun's PIC code.
     */
    class BitReversedRandomReal final : public RandomReal {
        std::unique_ptr<size_type> __base_, __seq_;

    public:
        // move/swap
        //
        void swap(BitReversedRandomReal &) noexcept;
        BitReversedRandomReal(BitReversedRandomReal &&) noexcept;
        BitReversedRandomReal &operator=(BitReversedRandomReal &&) noexcept;

        /**
         @brief Default constructor.
         @discussion The state is invalid (i.e., bool(*this) == false).
         */
        explicit BitReversedRandomReal() noexcept = default;
        /**
         @brief Initialize with a base integer number greater than or equal to 2, with an optional starting integer sequence (default is 0).
         @discussion A std::invalid_argument exception is thrown when base < 2.
         */
        explicit BitReversedRandomReal(size_type base, size_type sequence = 0);

        /**
         @brief State.
         @discussion If true, the state is valid and can be used.
         */
        operator bool() const noexcept { return bool(__base_); }

        /**
         @brief Return the current sequence.
         */
        size_type sequence() const noexcept { return *__seq_; }

        /**
         @brief Returns a random real variate uniformly distributed between 0 and 1.
         @discussion After this call, the integer sequence returned by the `sequence()` call will be incremented by 1.

         No check is made of the integer overflow.
         If the number overflows, the random number sequence will repeat.
         */
        value_type operator()() noexcept override { return _revers((*__seq_)++, *__base_); }
    private:
        value_type _revers(size_type seq, size_type base) noexcept;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLRandomReal.h>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief From bit reversed algorithm from Birdsall and Langdon (1985); taken from Kaijun's PIC code.
     @note BitReversedRandomReal's with `base' integers having common denominator(s) will result in correlated distributions.
     */
    class BitReversedRandomReal final : public RandomReal {
        std::unique_ptr<size_type> __base_, __seq_;

    public:
        // move/swap
        //
        void swap(BitReversedRandomReal &) noexcept;
        BitReversedRandomReal(BitReversedRandomReal &&) noexcept;
        BitReversedRandomReal &operator=(BitReversedRandomReal &&) noexcept;

        /**
         @brief Default constructor.
         @discussion The state is invalid (i.e., bool(*this) == false).
         */
        explicit BitReversedRandomReal() noexcept {}
        /**
         @brief Initialize with a base integer number greater than or equal to 2, with an optional starting integer sequence (default is 0).
         @discussion A std::invalid_argument exception is thrown when base < 2.
         */
        explicit BitReversedRandomReal(size_type base, size_type sequence = 0);

        /**
         @brief State.
         @discussion If true, the state is valid and can be used.
         */
        explicit operator bool() const noexcept { return bool(__base_); }

        /**
         @brief Return the current sequence.
         */
        size_type sequence() const noexcept { return *__seq_; }

        /**
         @brief Returns a random real variate uniformly distributed between 0 and 1.
         @discussion After this call, the integer sequence returned by the `sequence()` call will be incremented by 1.

         No check is made of the integer overflow.
         If the number overflows, the random number sequence will repeat.
         */
        value_type operator()() noexcept override { return _revers((*__seq_)++, *__base_); }
    private:
        value_type _revers(size_type seq, size_type base) noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLBitReversedRandomReal_h */
