//
//  UTLVector__1.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/27/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLVector__1_h
#define UTLVector__1_h

#include <UtilityKit/UTLVector__0.h>

// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK: Vector<1>
    //
    /**
     One element vector specialization.
     */
    template <class Type>
    struct Vector<Type, 1L> {
        // types:
        using value_type             = Type;
        using reference              = Type&;
        using const_reference        = Type const&;
        using pointer                = Type*;
        using const_pointer          = Type const*;
        using iterator               = pointer;
        using const_iterator         = const_pointer;
        using reverse_iterator       = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;
        using size_type              = long;
        using difference_type        = long;
        using size_vector_type       = Vector<size_type, 1L>;
        using comparison_result_type = Vector<typename _comparison_result<Type>::type, 1L>;
        using innermost_element_type = typename _Innermost<Type>::type; // the innermost element type if Type is a vector (whose value_type may be vector as well)

        // elements:
        Type x{};

        // capacity:
        static constexpr size_type size() noexcept { return size_type{1}; }
        static constexpr size_type max_size() noexcept { return size(); }
        static constexpr size_type rank() noexcept { return size_vector_type::size(); }
        static constexpr size_vector_type dims() noexcept { return {size()}; }
        static constexpr size_vector_type max_dims() noexcept { return dims(); }

        // constructors:
        constexpr explicit Vector() noexcept(std::is_nothrow_default_constructible<Type>::value) {}
        constexpr Vector(Type const &s) noexcept(std::is_nothrow_copy_constructible<Type>::value) : x(s) {}
        constexpr Vector(Type &&s) noexcept(std::is_nothrow_move_constructible<Type>::value) : x(std::move(s)) {}

        // copy/move:
        Vector(Vector const& o) noexcept(std::is_nothrow_copy_constructible<Type>::value) : Vector(o.x) {}
        inline Vector& operator=(Vector const& o) noexcept(std::is_nothrow_copy_assignable<Type>::value);
        Vector(Vector&& o) noexcept(std::is_nothrow_move_constructible<Type>::value) : Vector(std::move(o.x)) {}
        inline Vector& operator=(Vector&& o) noexcept(std::is_nothrow_move_assignable<Type>::value);
        template <class U, typename std::enable_if<std::is_convertible<U, Type>::value, long>::type = 0L>
        explicit Vector(Vector<U, size()> const& o) noexcept(std::is_nothrow_constructible<Type, U const&>::value) : Vector(o.x) {}
        template <class U>
        auto operator=(Vector<U, size()> const& o) -> typename std::enable_if<std::is_convertible<U, Type>::value, Vector>::type &{ return *this = Vector{o}; }

        // modifiers:
        void fill(Type const& s) noexcept(std::is_nothrow_copy_assignable<Type>::value) { x = s; }
        void swap(Vector& o) noexcept(noexcept(std::swap(x, o.x))) { std::swap(x, o.x); }

        // cast:
        operator std::array<Type, Vector::size()> const&() const& noexcept { return *reinterpret_cast<std::array<Type, Vector::size()> const*>(this); }
        operator std::array<Type, Vector::size()>      &()      & noexcept { return *reinterpret_cast<std::array<Type, Vector::size()>      *>(this); }

        explicit operator Type const &() const& noexcept { return x; }
        explicit operator Type       &()      & noexcept { return x; }

        // element access:
        reference       back ()       noexcept { return x; }
        const_reference back () const noexcept { return x; }
        reference       front()       noexcept { return x; }
        const_reference front() const noexcept { return x; }

        pointer       data()       noexcept { return reinterpret_cast<      pointer>(this); }
        const_pointer data() const noexcept { return reinterpret_cast<const_pointer>(this); }

#if defined(DEBUG)
        reference       operator[](size_type const i)       { return this->at(i); }
        const_reference operator[](size_type const i) const { return this->at(i); }
#else
        reference       operator[](size_type const i)       noexcept { return data()[i]; }
        const_reference operator[](size_type const i) const noexcept { return data()[i]; }
#endif

        inline reference       at(size_type const i);
        inline const_reference at(size_type const i) const;

        // iterators:
        iterator        begin()       noexcept { return       iterator{data()}; }
        const_iterator  begin() const noexcept { return const_iterator{data()}; }
        const_iterator cbegin() const noexcept { return begin(); }
        iterator        end  ()       noexcept { return       iterator{data() + Vector::size()}; }
        const_iterator  end  () const noexcept { return const_iterator{data() + Vector::size()}; }
        const_iterator cend  () const noexcept { return end(); }

        reverse_iterator        rbegin()       noexcept { return       reverse_iterator{end()}; }
        const_reverse_iterator  rbegin() const noexcept { return const_reverse_iterator{end()}; }
        const_reverse_iterator crbegin() const noexcept { return rbegin(); }
        reverse_iterator        rend  ()       noexcept { return       reverse_iterator{begin()}; }
        const_reverse_iterator  rend  () const noexcept { return const_reverse_iterator{begin()}; }
        const_reverse_iterator crend  () const noexcept { return rend(); }

        // transformations:
        template <long N>
        Vector const &take() const noexcept { static_assert(N == Vector::size() || N == -Vector::size(), "can take only one"); return *this; }
        template <long N>
        Vector       &take()       noexcept { static_assert(N == Vector::size() || N == -Vector::size(), "can take only one"); return *this; }

        template <long N>
        Vector const &drop() const noexcept { static_assert(N == 0, "cannot drop any"); return *this; }
        template <long N>
        Vector       &drop()       noexcept { static_assert(N == 0, "cannot drop any"); return *this; }

        Vector<Type, Vector::size() + 1>  append(Type const& x) const { decltype(append(x)) v{}; v.most() = *this; v.back () = x; return v; }
        Vector<Type, Vector::size() + 1> prepend(Type const& x) const { decltype(append(x)) v{}; v.rest() = *this; v.front() = x; return v; }

        // reductions:
        Type reduce_plus   () const noexcept { return front(); }
        Type reduce_prod   () const noexcept { return front(); }
        Type reduce_bit_and() const noexcept { return front(); }
        Type reduce_bit_or () const noexcept { return front(); }
        Type reduce_bit_xor() const noexcept { return front(); }

        // compound arithmetic:
        inline Vector &operator+=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator+=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator-=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator-=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator*=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator*=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator/=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator/=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // compound modulus:
        inline Vector &operator%=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator%=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // compound bit operators:
        inline Vector &operator&=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator&=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator|=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator|=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator^=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator^=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator<<=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator<<=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator>>=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator>>=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // unary:
        Vector const &operator+() const noexcept { return *this; }
        Vector        operator-() const noexcept(std::is_arithmetic<innermost_element_type>::value) { return Vector(-x); }
        // comparison (element-wise):
        inline comparison_result_type operator==(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator!=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator<=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator>=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator<(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator>(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLVector__1.hh>

#endif /* UTLVector__1_h */
