//
//  UTLMonotoneCubicCoefficient.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/15/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLMonotoneCubicCoefficient_hh
#define UTLMonotoneCubicCoefficient_hh

#include <stdexcept>

// MARK:- UTL::__4_::MonotoneCubicCoefficient<T>
//
template <class T>
void UTL::__4_::MonotoneCubicCoefficient<T>::_construct_coefs(typename Base::coefficient_table_type &table)
{
    constexpr typename Base::value_type zero{0}/*, one{1}*/, three{3};
    if (table.size() < Base::spline_order()) {
        throw std::invalid_argument(__PRETTY_FUNCTION__); // at least three entries are needed
    }

    // get consecutive differences and slopes
    // initial coefficient array, {y_j, dx_j, _, _}
    // upon this operation, {y_j, dy_j, dx_j, m}
    //
    {
        typename Base::coefficient_table_type::iterator _1 = table.begin(), _0 = _1++;
        for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
            typename Base::value_type const d1 = std::get<1>(_0->second);
            typename Base::value_type const y1 = std::get<0>(_0->second), y2 = std::get<0>(_1->second);
            typename Base::coefficient_type &c = _0->second;
            std::get<1>(c) = y2 - y1; // dy
            std::get<2>(c) = d1; // dx
            std::get<3>(c) = std::get<1>(c) / std::get<2>(c); // m=dy/dx
        }
    }

    // get degree-1 coefficients
    // upon this operation, {y, c1, dx, m}
    //
    {
        typename Base::coefficient_table_type::iterator _1 = table.begin(), _0 = _1++;
        typename Base::coefficient_table_type::const_iterator last = table.end();
        std::get<1>(_0->second) = std::get<3>(_0->second); // boundary
        for (--last; _1 != last; ++_0, ++_1) {
            typename Base::value_type const m = std::get<3>(_0->second), mNext = std::get<3>(_1->second);
            if (m * mNext <= zero) { // FIXME: Need to include some tolerance.
                std::get<1>(_1->second) = zero;
            } else {
                typename Base::value_type const dx = std::get<2>(_0->second), dxNext = std::get<2>(_1->second), common = dx + dxNext;
                std::get<1>(_1->second) = (common*m*mNext*three)/(dx*m + dxNext*mNext + common*(m + mNext)); // three*common / ((common + dxNext)/m + (common + dx)/mNext);
            }
        }
        std::get<1>(_1->second) = std::get<3>(_0->second);
    }

    // get degree-2 and degree-3 coefficients
    //
    {
        typename Base::coefficient_table_type::iterator _1 = table.begin(), _0 = _1++;
        for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
            typename Base::coefficient_type &c = _0->second;
            typename Base::value_type const c1 = std::get<1>(c), m = std::get<3>(c), dx = std::get<2>(c);
            typename Base::value_type const common = c1 + std::get<1>(_1->second) - (m + m);
#if 1 // normalized; t = (x - x_j)/(x_j + 1-x_j)
            std::get<2>(c) = (m - c1 - common)*dx;
            std::get<3>(c) = common*dx;
            std::get<1>(c) *= dx;
#else // non-normalized; t = (x - x_j)
            std::get<2>(c) = (m - c1 - common)/dx;
            std::get<3>(c) = common/(dx*dx);
            //std::get<1>(c) *= one;
#endif
        }
    }
}

#endif /* UTLMonotoneCubicCoefficient_hh */
