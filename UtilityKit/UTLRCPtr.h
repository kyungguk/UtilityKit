//
//  UTLRCPtr.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/25/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLRCPtr_h
#define UTLRCPtr_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/UTLLock.h>
#include <type_traits>
#include <typeinfo> // for std::bad_cast
#include <stdexcept>
#include <utility>
#include <algorithm>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    class _RefCounter;

    /**
     @brief Reference counted pointer, similar concept to std::shared_ptr.
     */
    template <class _T>
    class RCPtr {
        _RefCounter* _rc;
        _T* _obj;
    public:
        typedef _T  element_type;
        typedef _T* pointer;
        typedef _T& reference;

        /**
         @brief Release the managing object when the reference count reaches 0.
         */
        ~RCPtr();

        /**
         @brief Null-object constructor.
         */
        explicit RCPtr() noexcept;
        /**
         @brief Construct the managing object of element_type (_T) by forwarding the passed parameters to the new operator.
         @discussion The first argument should be nullptr.
         @exception Throws whatever exception thrown by the new operator.
         */
        template <class... Args>
        explicit RCPtr(std::nullptr_t, Args&&... args);

        // copy/move:
        RCPtr(RCPtr const &o);
        RCPtr(RCPtr &&o) noexcept;

        RCPtr& operator=(RCPtr const& o);
        RCPtr& operator=(RCPtr&& o) noexcept;

        // copy/move by up and down casts:
        template <class U> friend class RCPtr;
        template <class U>
        explicit RCPtr(RCPtr<U> const& o);
        template <class U>
        explicit RCPtr(RCPtr<U>&& o);

        template <class U>
        RCPtr& operator=(RCPtr<U> const& o);
        template <class U>
        RCPtr& operator=(RCPtr<U>&& o) noexcept;

        // modifiers:
        void swap(RCPtr &o) noexcept;

        // observers:
        explicit operator bool() const noexcept;
        long reference_count() const noexcept;
        element_type* get() const noexcept { return  _obj; }

        // managed object access (no check):
        element_type* operator->() const noexcept { return  _obj; }
        element_type& operator *() const noexcept { return *_obj; }

        // comparisons:
        template <class T, class U> // equal if pointer to the managed object is equal
        friend inline bool operator==(RCPtr<T> const& x, RCPtr<U> const& y) noexcept { return x.get() == y.get(); }
        template <class T, class U>
        friend inline bool operator!=(RCPtr<T> const& x, RCPtr<U> const& y) noexcept { return !(x == y); }
    };

    // Internal atomic reference counter:
    //
    class _RefCounter {
        typedef SpinLock lock_type;
        typedef long counter_type;
        counter_type _rc;
        lock_type __mx_;

        _RefCounter() noexcept(noexcept(lock_type()));
        operator counter_type() const noexcept;
        counter_type operator--();
        _RefCounter& operator++();

        template <class T> friend class RCPtr;
    };

    // MARK: -- Out-of-line definitions --
    //
    template <class T>
    RCPtr<T>::~RCPtr() {
        if (!*this || --*_rc > 0) return;
        delete _obj;
        delete _rc;
    }

    template <class T>
    RCPtr<T>::RCPtr() noexcept
    : _rc(), _obj() { // should not use ' = default' as the optimization may not nil-initialize
    }
    template <class T>
    template <class... Args>
    RCPtr<T>::RCPtr(std::nullptr_t, Args&&... args)
    : _rc(new _RefCounter()), _obj(new T(std::forward<Args>(args)...)) {
    }

    template <class T>
    RCPtr<T>::RCPtr(RCPtr const &o)
    : _rc(o ? &++*o._rc : nullptr), _obj(o._obj) {
    }
    template <class T>
    RCPtr<T>::RCPtr(RCPtr &&o) noexcept
    : _rc(o._rc), _obj(o._obj) {
        o._obj = nullptr;
        o._rc = nullptr;
    }

    template <class T>
    auto RCPtr<T>::operator=(RCPtr const &o)
    -> RCPtr &{
        if (&o != this) {
            RCPtr tmp(o);
            swap(tmp);
        }
        return *this;
    }
    template <class T>
    auto RCPtr<T>::operator=(RCPtr&& o) noexcept
    -> RCPtr &{
        if (&o != this) {
            swap(o);
        }
        return *this;
    }

    template <class T>
    template <class U>
    RCPtr<T>::RCPtr(RCPtr<U> const &o)
    : _rc(o ? &++*o._rc : nullptr), _obj(o ? dynamic_cast<T*>(o._obj) : nullptr) {
        static_assert(std::is_polymorphic<T>::value && std::is_polymorphic<U>::value, "T and U must be a polymorphic type.");
        if (o && !_obj) {
            --*_rc;
            _rc = nullptr;
            throw std::bad_cast();
        }
    }
    template <class T>
    template <class U>
    RCPtr<T>::RCPtr(RCPtr<U> &&o)
    : _rc(o._rc), _obj(o ? dynamic_cast<T*>(o._obj) : nullptr) {
        static_assert(std::is_polymorphic<T>::value && std::is_polymorphic<U>::value, "T and U must be a polymorphic type.");
        if (o && !_obj) {
            _rc = nullptr;
            throw std::bad_cast();
        } else {
            o._obj = nullptr;
            o._rc = nullptr;
        }
    }

    template <class T>
    template <class U>
    auto RCPtr<T>::operator=(RCPtr<U> const& o)
    -> RCPtr<T> &{
        {
            RCPtr tmp(o);
            swap(tmp);
        }
        return *this;
    }
    template <class T>
    template <class U>
    auto RCPtr<T>::operator=(RCPtr<U>&& o) noexcept
    -> RCPtr<T> &{
        {
            RCPtr tmp(std::move(o));
            swap(tmp);
        }
        return *this;
    }


    template <class T>
    void RCPtr<T>::swap(RCPtr &o) noexcept
    {
        std::swap(_rc, o._rc);
        std::swap(_obj, o._obj);
    }

    template <class T>
    RCPtr<T>::operator bool() const noexcept
    {
        return _obj;
    }
    template <class T>
    auto RCPtr<T>::reference_count() const noexcept
    -> long {
        return *_rc;
    }

} // namespace __3_
UTILITYKIT_END_NAMESPACE

// std::swap:
namespace std {
    template<class T> inline
    void swap(UTILITYKIT_NAMESPACE::__3_::RCPtr<T>& x, UTILITYKIT_NAMESPACE::__3_::RCPtr<T>& y) noexcept(noexcept(x.swap(y))) {
        x.swap(y);
    }
}

#endif /* UTLRCPtr_h */
