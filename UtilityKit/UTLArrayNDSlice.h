//
//  UTLArrayNDSlice.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 8/26/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayNDSlice_h
#define UTLArrayNDSlice_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLArrayNDLeafWalker.h>
#include <UtilityKit/UTLArray.h>
#include <UtilityKit/UTLVector.h>
#include <type_traits>
#include <algorithm>
#include <iterator>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class Type, long ND, long PadSize>
    class ArrayND;
    template <class Type, long ND, long PadSize>
    class ArraySliceND;

    //@{
    /**
     @brief Create a slice of ArrayND.
     */
    template <long NewPad, class Type, long ND, long OldPad>
    ArraySliceND<Type, ND, NewPad> make_slice(ArrayND<Type, ND, OldPad> &A, Vector<long, ND> const &locs, Vector<long, ND> const &lens) {
        return {A, locs, lens};
    }
    template <long NewPad, class Type, long ND, long OldPad>
    ArraySliceND<Type, ND, NewPad> make_slice(ArrayND<Type, ND, OldPad> &A) {
        return ArraySliceND<Type, ND, NewPad>{A};
    }
    template <long NewPad, class Type, long ND, long OldPad>
    ArraySliceND<Type, ND, NewPad> make_slice(ArraySliceND<Type, ND, OldPad> &A, Vector<long, ND> const &locs, Vector<long, ND> const &lens) {
        return {A, locs, lens};
    }
    template <long NewPad, class Type, long ND, long OldPad>
    ArraySliceND<Type, ND, NewPad> make_slice(ArraySliceND<Type, ND, OldPad> &A) {
        return ArraySliceND<Type, ND, NewPad>{A};
    }
    //@}

    /**
     @brief Light-weight slice of ArrayND.
     @discussion Storage is owned by ArrayND.
     */
    template <class Type, long ND, long PadSize>
    class ArraySliceND {
        static_assert(ND >= 1, "invalid ND");
        static_assert(PadSize >= 0, "invalid PadSize");

    public: // types:
        using value_type                    = Type;
        using size_type                     = long;
        using difference_type               = long;
        using size_vector_type              = Vector<size_type, ND>;

        using leaf_iterator                 = _ArrayNDLeafWalker<ArraySliceND>;
        using const_leaf_iterator           = _ArrayNDLeafWalker<ArraySliceND const>;
        using reverse_leaf_iterator         = std::reverse_iterator<leaf_iterator>;
        using const_reverse_leaf_iterator   = std::reverse_iterator<const_leaf_iterator>;

    private: // member variables:
        class Backend : public Array<Type> {
            size_vector_type _strides{}; //!< index path strides; for example, if max_dims = {Nx, Ny, Nz}, then strides = {Ny*Nz, Nz, 1}
            static Vector<size_type, 1> _get_strides([[maybe_unused]] Vector<size_type, 1> const &max_dims) noexcept {
                return {1};
            }
            template <long S>
            static Vector<size_type, S> _get_strides(Vector<size_type, S> const &max_dims) noexcept {
                return _get_strides(max_dims.rest()).prepend(reduce_prod(max_dims.rest()));
            }

            Vector<size_type, 1> _get_max_dims(Vector<size_type, 1> &&max_dims) const noexcept {
                return max_dims /= _strides;
            }
            template <long S>
            Vector<size_type, S> _get_max_dims(Vector<size_type, S> &&max_dims) const noexcept {
                max_dims.rest() = _strides.most();
                return max_dims /= _strides; // be careful with division by 0
            }
        public:
            size_vector_type const &strides() const noexcept { return _strides; }
            size_vector_type max_dims() const noexcept { return this->empty() ? size_vector_type{} : _get_max_dims(size_vector_type{this->size()}); }

            explicit Backend() noexcept {}
            explicit Backend(std::pair<Type *, size_vector_type/*max_dims*/> const &storage) noexcept
            : Array<Type>(storage.first, storage.first), _strides(_get_strides(storage.second)) {
                if (this->_end) this->_end += storage.second.front()*strides().front();
            }
            void swap(Backend &o) noexcept { // storage swap
                std::swap(this->_beg, o._beg);
                std::swap(this->_end, o._end);
                std::swap(this->_strides, o._strides);
            }
            Type       &operator[](size_vector_type &&ipath/*excluding paddings*/)       noexcept(noexcept(std::declval<Array<Type>>()[0])) {
                return Array<Type>::operator[](reduce_plus((ipath += PadSize) *= _strides));
            }
            Type const &operator[](size_vector_type &&ipath/*excluding paddings*/) const noexcept(noexcept(std::declval<Array<Type>>()[0])) {
                return Array<Type>::operator[](reduce_plus((ipath += PadSize) *= _strides));
            }
        } _backend{};
        size_vector_type _slice_begs{}; //!< beginning indices of slice (excluding paddings)
        size_vector_type _slice_dims{}; //!< dimension sizes (excluding paddings) of sliced array

    public:
        // destructor:
        ~ArraySliceND() {}

        // constructors:
        /**
         @brief Construct an array slice whose state is invalid.
         @discussion No storage allocation occurs.
         */
        explicit ArraySliceND() noexcept {}

        //@{
        /**
         @brief Move/copy construct from another array slice of the same type.
         */
        ArraySliceND(ArraySliceND &&other) noexcept : ArraySliceND() { swap(other); }
        ArraySliceND(ArraySliceND &o) noexcept : ArraySliceND(o.storage(), o._slice_begs, o._slice_dims) {}
        //@}

        /**
         @brief Create an array slice mirroring other.
         @discussion Implicit cast makes assignment and slicing of ArrayND work.
         */
        ArraySliceND(ArrayND<Type, ND, PadSize> &A) noexcept : ArraySliceND() {
            // this guards against copying dimension sizes that contains 0, e.g., dims = {3, 4, 0}
            if (A) ArraySliceND{A.storage(), size_vector_type{}, A.dims()}.swap(*this);
        }

        /**
         @brief Construct an array slice from an another array.
         */
        template <long OldPad>
        ArraySliceND(ArraySliceND<Type, ND, OldPad> &A, size_vector_type locs, size_vector_type const &lens);
        template <long OldPad>
        ArraySliceND(ArraySliceND<Type, ND, OldPad> &&A, size_vector_type locs, size_vector_type const &lens) : ArraySliceND(A, locs, lens) {
            ArraySliceND<Type, ND, OldPad>{std::move(A)};
        }
        template <long OldPad>
        ArraySliceND(ArrayND<Type, ND, OldPad> &A, size_vector_type const &locs, size_vector_type const &lens) : ArraySliceND(ArraySliceND<Type, ND, OldPad>{A}, locs, lens) {}

        /**
         @brief Construct an array mirroring an another array.
         @discussion Explicit to prevent accidental assignment.
         */
        template <long OldPad>
        explicit ArraySliceND(ArraySliceND<Type, ND, OldPad> &A);
        template <long OldPad>
        explicit ArraySliceND(ArraySliceND<Type, ND, OldPad> &&A) : ArraySliceND(A) { ArraySliceND<Type, ND, OldPad>{std::move(A)}; }
        template <long OldPad>
        explicit ArraySliceND(ArrayND<Type, ND, OldPad> &A) : ArraySliceND(ArraySliceND<Type, ND, OldPad>{A}) {}

        // assignment:
        //@{
        /**
         @brief Element-wise copy/move assignment.
         @discussion Be careful when copy assigning array returned from a function, which will trigger move assignment.
         */
        ArraySliceND &operator=(ArraySliceND const &other);
        ArraySliceND &operator=(ArraySliceND &&other);
        ArraySliceND &operator=(ArrayND<Type, ND, PadSize> const &other);
        ArraySliceND &operator=(ArrayND<Type, ND, PadSize> &&other);
        //@}

        // modifiers:
        /**
         @brief Storage swap.
         @discussion Both *this and other should not be an element of higher rank array.
         @exception When *this and/or other is an element of higher rank array.
         */
        void swap(ArraySliceND &o) {
            _backend.swap(o._backend);
            std::swap(_slice_begs, o._slice_begs);
            std::swap(_slice_dims, o._slice_dims);
        }

        /**
         @brief Fill the elements with the given value.
         */
        void fill(Type const &x) noexcept(std::is_nothrow_copy_assignable<Type>::value) {
            if (*this) std::fill(leaf_pad_begin(), leaf_pad_end(), x);
        }

        // observers:
        /**
         @brief Check if the status of *this is valid.
         @discussion The validity is defined by whether the pointer to the underlying storage is nullptr or not.

         A default-constructed array is invalid, so is an array with reduce_prod(max_dims()) == 0.
         */
        explicit operator bool() const noexcept { return _backend.begin(); }

        // capacity:
        /**
         @brief Array rank.
         */
        static constexpr size_type rank() noexcept { return size_vector_type::size(); }
        /**
         @brief Returns one-side pad size.
         */
        static constexpr size_type pad_size() noexcept { return PadSize; }

        /**
         @brief Dimension sizes (excluding paddings).
         */
        size_vector_type const &dims() const noexcept { return _slice_dims; }

        /**
         @brief Maximum dimension sizes (i.e., dims() + pad_size()).
         */
        size_vector_type max_dims() const noexcept { return *this ? dims() + 2*pad_size() : size_vector_type{}; }

        /**
         @brief Size at a given dimension.
         */
        template <long I>
        size_type const &size() const noexcept {
            static_assert(I >= 0 && I < rank(), "dimension out of bound");
            return std::get<I>(_slice_dims);
        }

        /**
         @brief Max size at a given dimension.
         */
        template <long I>
        size_type max_size() const noexcept {
            static_assert(I >= 0 && I < rank(), "dimension out of bound");
            return *this ? this->template size<I>() + 2*pad_size() : size_type{};
        }

        //@{
        /**
         Properties of the leading dimension (for consistency with ArrayND interface).
         */
        size_type const &size() const noexcept { return size<0>(); }
        size_type max_size() const noexcept { return max_size<0>(); }
        bool empty() const noexcept { return !size(); }
        //@}

        // element access:
        //@{
        /**
         @brief Index path subscript.
         */
        Type       &at(size_vector_type const &ipath)      ;
        Type const &at(size_vector_type const &ipath) const;

#if defined(DEBUG)
        Type       &operator[](size_vector_type const &ipath)       { return at(ipath); }
        Type const &operator[](size_vector_type const &ipath) const { return at(ipath); }
#else
        Type       &operator[](size_vector_type const &ipath)       noexcept { return _backend[ipath + _slice_begs]; }
        Type const &operator[](size_vector_type const &ipath) const noexcept { return _backend[ipath + _slice_begs]; }
#endif
        //@}

        /**
         @brief Returns the pointer to the storage of the underlying array and its maximum extents.
         @discussion Used for slicing.
         */
        std::pair<Type *, size_vector_type> storage() noexcept { return std::make_pair(_backend.begin(), _backend.max_dims()); }

        /**
         @brief Offset of the slicing indices.
         */
        size_vector_type const &slice_offsets() const noexcept { return _slice_begs; } // excluding padding

        // iterator:
        //@{
        /**
         @brief Walk through the elements in the slice (excluding paddings).
         */
        leaf_iterator        leaf_begin()       noexcept { return leaf_iterator{this, false, std::false_type{}}; }
        const_leaf_iterator  leaf_begin() const noexcept { return const_leaf_iterator{this, false, std::false_type{}}; }
        const_leaf_iterator leaf_cbegin() const noexcept { return leaf_begin(); }
        leaf_iterator          leaf_end()       noexcept { return leaf_iterator{this, false, std::true_type{}}; }
        const_leaf_iterator    leaf_end() const noexcept { return const_leaf_iterator{this, false, std::true_type{}}; }
        const_leaf_iterator   leaf_cend() const noexcept { return leaf_end(); }

        reverse_leaf_iterator        leaf_rbegin()       noexcept { return reverse_leaf_iterator{leaf_end()}; }
        const_reverse_leaf_iterator  leaf_rbegin() const noexcept { return const_reverse_leaf_iterator{leaf_end()}; }
        const_reverse_leaf_iterator leaf_crbegin() const noexcept { return leaf_rbegin(); }
        reverse_leaf_iterator          leaf_rend()       noexcept { return reverse_leaf_iterator{leaf_begin()}; }
        const_reverse_leaf_iterator    leaf_rend() const noexcept { return const_reverse_leaf_iterator{leaf_begin()}; }
        const_reverse_leaf_iterator   leaf_crend() const noexcept { return leaf_rend(); }
        //@}

        //@{
        /**
         @brief Walk through the elements in the slice (including paddings).
         */
        leaf_iterator        leaf_pad_begin()       noexcept { return leaf_iterator{this, true, std::false_type{}}; }
        const_leaf_iterator  leaf_pad_begin() const noexcept { return const_leaf_iterator{this, true, std::false_type{}}; }
        const_leaf_iterator leaf_pad_cbegin() const noexcept { return leaf_pad_begin(); }
        leaf_iterator          leaf_pad_end()       noexcept { return leaf_iterator{this, true, std::true_type{}}; }
        const_leaf_iterator    leaf_pad_end() const noexcept { return const_leaf_iterator{this, true, std::true_type{}}; }
        const_leaf_iterator   leaf_pad_cend() const noexcept { return leaf_pad_end(); }

        reverse_leaf_iterator        leaf_pad_rbegin()       noexcept { return reverse_leaf_iterator{leaf_pad_end()}; }
        const_reverse_leaf_iterator  leaf_pad_rbegin() const noexcept { return const_reverse_leaf_iterator{leaf_pad_end()}; }
        const_reverse_leaf_iterator leaf_pad_crbegin() const noexcept { return leaf_pad_rbegin(); }
        reverse_leaf_iterator          leaf_pad_rend()       noexcept { return reverse_leaf_iterator{leaf_pad_begin()}; }
        const_reverse_leaf_iterator    leaf_pad_rend() const noexcept { return const_reverse_leaf_iterator{leaf_pad_begin()}; }
        const_reverse_leaf_iterator   leaf_pad_crend() const noexcept { return leaf_pad_rend(); }
        //@}

    private:
        explicit ArraySliceND(std::pair<Type *, size_vector_type> const &storage, size_vector_type const &slice_begs, size_vector_type const &slice_dims) noexcept : _backend(storage), _slice_begs(slice_begs), _slice_dims(slice_dims) {}
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLArrayNDSlice.hh>

#endif /* UTLArrayNDSlice_h */
