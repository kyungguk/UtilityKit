//
//  UTLLock.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/23/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLLock_h
#define UTLLock_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <atomic>
#include <pthread.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
    /**
     @brief pthread mutex wrapper.
     @discussion Conceptually similar c++11 std::mutex.
     Consult documentation of std::mutex.
     */
    class Mutex {
        pthread_mutex_t __m_;

        Mutex(const Mutex&) = delete;
        Mutex& operator=(const Mutex&) = delete;
    public:
        ~Mutex(); // terminate when error occurs while destorying the native handle.
        explicit Mutex();

        // lock/unlock:
        void lock();
        bool try_lock() noexcept;
        void unlock() noexcept;

        // native handle:
        typedef pthread_mutex_t* native_handle_type;
        native_handle_type native_handle() noexcept { return &__m_; }
    };

    /**
     @brief Simple spin lock implementation using std::atomic_flag.
     */
    class SpinLock {
        std::atomic_flag __s_;

        SpinLock(const SpinLock&) = delete;
        SpinLock& operator=(const SpinLock&) = delete;
    public:
        ~SpinLock() = default;
        explicit SpinLock() noexcept : __s_(false) {}

        enum Status { Unlocked = 0, Locked };
        explicit SpinLock(Status status) noexcept : __s_(status) {}

        // lock/unlock:
        void lock() noexcept;
        bool try_lock() noexcept;
        void unlock() noexcept;
    };

    /**
     @brief Lock guard.
     @discussion Conceptually equivalent to c++11 std::lock_guard.
     Consult documentation of std::lock_guard.
     */
    template <typename Lock>
    class LockG {
        Lock& __m_;

        LockG(LockG const&) = delete;
        LockG& operator=(LockG const&) = delete;
    public:
        // unlock:
        ~LockG() { __m_.unlock(); }
        // guarded lock:
        explicit LockG(Lock& mx) noexcept(noexcept(__m_.lock())) : __m_(mx) { __m_.lock(); }
    };
} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <atomic>
#include <stdexcept>
#include <type_traits> // for std::decay
#include <pthread.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief pthread mutex wrapper.
     @discussion Conceptually similar c++11 std::mutex.
     Consult documentation of std::mutex.
     */
    class Mutex {
        pthread_mutex_t __m_;

        Mutex(const Mutex&) = delete;
        Mutex& operator=(const Mutex&) = delete;
    public:
        ~Mutex(); // terminate when error occurs while destorying the native handle.
        explicit Mutex();

        // lock/unlock:
        void lock() { if (pthread_mutex_lock(&__m_)) throw std::runtime_error(__PRETTY_FUNCTION__); }
        bool try_lock() noexcept { return 0 == pthread_mutex_trylock(&__m_); }
        void unlock() noexcept { pthread_mutex_unlock(&__m_); }

        // native handle:
        using native_handle_type = pthread_mutex_t*;
        native_handle_type native_handle() noexcept { return &__m_; }
    };

    /**
     @brief Simple spin lock implementation using std::atomic_flag.
     */
    class SpinLock {
        std::atomic_flag __s_;

        SpinLock(const SpinLock&) = delete;
        SpinLock& operator=(const SpinLock&) = delete;
    public:
        ~SpinLock() = default;
        explicit SpinLock() noexcept : __s_(false) {}

        enum Status { Unlocked = 0, Locked };
        explicit SpinLock(Status status) noexcept : __s_(status) {}

        // lock/unlock:
        // test_and_set returns the previous value, so if it returns false, then lock has been acquired. otherwise the lock has not been acquired.
        // relocking is a valid operation.
        void lock() noexcept { while (__s_.test_and_set(std::memory_order_acquire)) {} }
        void lock(bool const yield) noexcept;
        bool try_lock() noexcept { return !__s_.test_and_set(std::memory_order_acquire); }
        void unlock() noexcept { __s_.clear(std::memory_order_release); }
    };

    /**
     @brief Lock guard.
     @discussion Conceptually equivalent to c++11 std::lock_guard.
     Consult documentation of std::lock_guard.
     */
    template <typename Lock>
    class LockG {
        Lock& __m_;

        LockG(LockG const&) = delete;
        LockG& operator=(LockG const&) = delete;
    public:
        // unlock:
        ~LockG() { __m_.unlock(); }
        // guarded lock:
        explicit LockG(Lock& mx) noexcept(noexcept(__m_.lock())) : __m_(mx) { __m_.lock(); }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- LOCK_GUARD
//
/**
 Convenience shorthand for LockG<LK> _UNIQUENAME{mx}.

 It requires that UTILITYKIT_INLINE_VERSION be set.
 It cannot be used more than once in the same line.
 */
#ifndef LOCK_GUARD
#define LOCK_GUARD(mx) UTILITYKIT_NAMESPACE::LockG<std::decay<decltype(mx)>::type> UTILITYKIT_UNIQUE_NAME(_lock_guard_){mx}
#endif

#endif /* UTLLock_h */
