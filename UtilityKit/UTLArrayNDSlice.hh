//
//  UTLArrayNDSlice.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 8/26/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayNDSlice_hh
#define UTLArrayNDSlice_hh

#include <algorithm>
#include <stdexcept>
#include <string>

// MARK:- UTL::__4_::ArraySlice<ND>
//
template <class Type, long ND, long PadSize>
template <long OldPad>
UTL::__4_::ArraySliceND<Type, ND, PadSize>::ArraySliceND(ArraySliceND<Type, ND, OldPad> &A, size_vector_type locs, size_vector_type const &lens)
: ArraySliceND() { // location is relative to the array to be sliced
    size_vector_type const dims = A.dims() + 2*(A.pad_size() - this->pad_size()); // A.dims() translated into new array space
    locs += A.pad_size() - this->pad_size(); // location is now in new array space
    if (reduce_bit_or(locs < size_vector_type{}) || reduce_bit_or(locs > dims)) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - invalid slicing location");
    }
    if (reduce_bit_or(lens < size_vector_type{}) || reduce_bit_or(locs + lens > dims)) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - invalid slicing length");
    }
    // slice
    //
    ArraySliceND{A.storage(), A.slice_offsets() + locs, lens}.swap(*this);
}
template <class Type, long ND, long PadSize>
template <long OldPad>
UTL::__4_::ArraySliceND<Type, ND, PadSize>::ArraySliceND(ArraySliceND<Type, ND, OldPad> &A)
: ArraySliceND() {
    size_vector_type const dims = A.dims() + 2*(A.pad_size() - this->pad_size()); // A.dims() translated into new array space
    if (reduce_bit_or(dims < size_vector_type{})) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - too large padding of the sliced array");
    }
    // slice
    //
    ArraySliceND{A.storage(), A.slice_offsets(), dims}.swap(*this);
}

template <class Type, long ND, long PadSize>
auto UTL::__4_::ArraySliceND<Type, ND, PadSize>::operator=(ArraySliceND const &o)
-> ArraySliceND &{
    if (this != &o) {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (reduce_bit_or(o.dims() != this->dims())) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array dimension sizes inconsistency");
        }
        std::copy(o.leaf_pad_begin(), o.leaf_pad_end(), this->leaf_pad_begin());
    }
    return *this;
}
template <class Type, long ND, long PadSize>
auto UTL::__4_::ArraySliceND<Type, ND, PadSize>::operator=(ArraySliceND &&o)
-> ArraySliceND &{
    if (this != &o) {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (reduce_bit_or(o.dims() != this->dims())) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array dimension sizes inconsistency");
        }
        std::move(o.leaf_pad_begin(), o.leaf_pad_end(), this->leaf_pad_begin());
    }
    return *this;
}

template <class Type, long ND, long PadSize>
auto UTL::__4_::ArraySliceND<Type, ND, PadSize>::operator=(ArrayND<Type, ND, PadSize> const &o)
-> ArraySliceND &{
    {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (reduce_bit_or(o.dims() != this->dims())) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array dimension sizes inconsistency");
        }
        std::copy(o.leaf_pad_begin(), o.leaf_pad_end(), this->leaf_pad_begin());
    }
    return *this;
}
template <class Type, long ND, long PadSize>
auto UTL::__4_::ArraySliceND<Type, ND, PadSize>::operator=(ArrayND<Type, ND, PadSize> &&o)
-> ArraySliceND &{
    {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (reduce_bit_or(o.dims() != this->dims())) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array dimension sizes inconsistency");
        }
        std::move(o.leaf_pad_begin(), o.leaf_pad_end(), this->leaf_pad_begin());
    }
    return *this;
}

template <class Type, long ND, long PadSize>
Type       &UTL::__4_::ArraySliceND<Type, ND, PadSize>::at(size_vector_type const &ipath)
{
    if (*this &&
        reduce_bit_and(ipath >= size_vector_type{-PadSize}) &&
        reduce_bit_and(ipath < dims() + PadSize)) {
        return _backend[ipath + _slice_begs];
    }
    throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - index out of range");
}
template <class Type, long ND, long PadSize>
Type const &UTL::__4_::ArraySliceND<Type, ND, PadSize>::at(size_vector_type const &ipath) const
{
    if (*this &&
        reduce_bit_and(ipath >= size_vector_type{-PadSize}) &&
        reduce_bit_and(ipath < dims() + PadSize)) {
        return _backend[ipath + _slice_begs];
    }
    throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - index out of range");
}

#endif /* UTLArrayNDSlice_hh */
