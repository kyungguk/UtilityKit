//
//  PaddedField.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/17/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#ifndef PaddedField_h
#define PaddedField_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
// Originated from HybridKit Version 3.
//
#include <UtilityKit/ArrayKit.h>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    template <class T, long ND, long Padding> struct PaddedField;

    // MARK: PaddedField<1D>
    //
    template <class _T, long Padding>
    struct PaddedField<_T, 1, Padding> final : public Dynamic::PaddedArray<_T, Padding> {
        static constexpr long ND = 1;

        using padded_array_type = Dynamic::PaddedArray<_T, Padding>;
        using nopad_array_type  = Dynamic::PaddedArray<_T, 0      >;
        using __base            = padded_array_type;
        using size_type         = typename __base::size_type;
        using difference_type   = typename __base::difference_type;
        using value_type        = typename __base::value_type;
        using size_vector       = Numeric::Vector<size_type, ND>;

    private:
        value_type *_p;
        size_type _len; // _len*sizeof(value_type) is the total length of memory allocated.

        PaddedField(PaddedField const &) = delete;
        PaddedField &operator=(PaddedField const &) = delete;

    public:
        ~PaddedField() { delete [] _p; }

        PaddedField(PaddedField &&o) : __base(std::move(o)), _p(o._p), _len(o._len) {
            o._p = nullptr, o._len = 0;
        }
        explicit PaddedField(size_vector N) : __base(), _p(nullptr), _len() {
            size_type const mx = N.x + 2*__base::pad_size();
            value_type *p = _p = new value_type[_len = mx];
            __base::operator=(__base(N.x, p));
            reset();
        }

        std::pair<_T const *, size_type> memory() const noexcept { return {_p, _len}; }
        std::pair<_T       *, size_type> memory()       noexcept { return {_p, _len}; }
        size_vector N() const noexcept { return { this->size() }; } //!< This is size without padding.

        using __base::fill;
        void reset() { __base::fill(value_type()); }

        static constexpr difference_type pad() noexcept { return __base::pad_size(); }
        difference_type Nx() const noexcept { return difference_type(__base::size()); }
        difference_type padNx() const noexcept { return pad() + Nx(); }
        difference_type padNxPad() const noexcept { return padNx() + pad(); }
    };

    // MARK: PaddedField<2D>
    //
    template <class _T, long Padding>
    struct PaddedField<_T, 2, Padding> final : public Dynamic::PaddedArray<typename PaddedField<_T, 1, Padding>::padded_array_type, Padding> {
        static constexpr long ND = 2;

        using padded_array_type = Dynamic::PaddedArray<typename PaddedField<_T, 1, Padding>::padded_array_type, Padding>;
        using nopad_array_type  = Dynamic::PaddedArray<typename PaddedField<_T, 1, Padding>::nopad_array_type , 0      >;
        using __base            = padded_array_type;
        using __inner_array     = typename __base::value_type;
        using size_type         = typename __base::size_type;
        using difference_type   = typename __base::difference_type;
        using value_type        = typename __inner_array::value_type;
        using size_vector       = Numeric::Vector<size_type, ND>;

    private:
        value_type *_p;
        size_type _len; // _len*sizeof(value_type) is the total length of memory allocated.

        PaddedField(PaddedField const &) = delete;
        PaddedField &operator=(PaddedField const &) = delete;

    public:
        ~PaddedField() { delete [] _p; }

        PaddedField(PaddedField &&o) : __base(std::move(o)), _p(o._p), _len(o._len) {
            o._p = nullptr, o._len = 0;
        }
        explicit PaddedField(size_vector N) : __base(), _p(nullptr), _len() {
            size_type const mx = N.x + 2*__base::pad_size();
            size_type const my = N.y + 2*__base::pad_size();
            value_type *p = _p = new value_type[_len = mx*my];
            __base::operator=(__base(N.x));
            for (size_type i = 0; i < mx; ++i, p += my) {
                __base::data()[i] = __inner_array(N.y, p);
            }
            reset();
        }

        std::pair<_T const *, size_type> memory() const noexcept { return {_p, _len}; }
        std::pair<_T       *, size_type> memory()       noexcept { return {_p, _len}; }
        size_vector N() const noexcept { return { this->size(), _len ? this->begin()->size() : 0 }; } //!< This is size without padding.

        void fill(value_type const &f) {
            for (auto first = __base::pad_begin(), last = __base::pad_end(); first != last; ++first) {
                first->fill(f);
            }
        }
        void reset() { this->fill(value_type()); }

        static constexpr difference_type pad() noexcept { return __base::pad_size(); }
        difference_type Nx() const noexcept { return difference_type(__base::size()); }
        difference_type padNx() const noexcept { return pad() + Nx(); }
        difference_type padNxPad() const noexcept { return padNx() + pad(); }
        difference_type Ny() const noexcept { return difference_type(__base::operator[](0).size()); }
        difference_type padNy() const noexcept { return pad() + Ny(); }
        difference_type padNyPad() const noexcept { return padNy() + pad(); }
    };

    // MARK: PaddedField<3D>
    //
    template <class _T, long Padding>
    struct PaddedField<_T, 3, Padding> final : public Dynamic::PaddedArray<typename PaddedField<_T, 2, Padding>::padded_array_type, Padding> {
        static constexpr long ND = 3;

        using padded_array_type = typename Dynamic::PaddedArray<typename PaddedField<_T, 2, Padding>::padded_array_type, Padding>;
        using nopad_array_type  = typename Dynamic::PaddedArray<typename PaddedField<_T, 2, Padding>::nopad_array_type , 0      >;
        using __base            = padded_array_type;
        using __outer_array     = typename __base::value_type;
        using __inner_array     = typename __outer_array::value_type;
        using size_type         = typename __base::size_type;
        using difference_type   = typename __base::difference_type;
        using value_type        = typename __inner_array::value_type;
        using size_vector       = Numeric::Vector<size_type, ND>;

    private:
        value_type *_p;
        size_type _len; // _len*sizeof(value_type) is the total length of memory allocated.

        PaddedField(PaddedField const &) = delete;
        PaddedField &operator=(PaddedField const &) = delete;

    public:
        ~PaddedField() { delete [] _p; }

        PaddedField(PaddedField &&o) : __base(std::move(o)), _p(o._p), _len(o._len) {
            o._p = nullptr, o._len = 0;
        }
        explicit PaddedField(size_vector N) : __base(), _p(nullptr), _len() {
            size_type const mx = N.x + 2*__base::pad_size();
            size_type const my = N.y + 2*__base::pad_size();
            size_type const mz = N.z + 2*__base::pad_size();
            value_type *p = _p = new value_type[_len = mx*my*mz];
            __base::operator=(__base(N.x));
            for (size_type i = 0; i < mx; ++i) {
                __base::data()[i] = __outer_array(N.y);
                for (size_type j = 0; j < my; ++j, p += mz) {
                    __base::data()[i].data()[j] = __inner_array(N.z, p);
                }
            }
            reset();
        }

        std::pair<_T const *, size_type> memory() const noexcept { return {_p, _len}; }
        std::pair<_T       *, size_type> memory()       noexcept { return {_p, _len}; }
        size_vector N() const noexcept { return { this->size(), _len ? this->begin()->size() : 0, _len ? this->begin()->begin()->size() : 0 }; } //!< This is size without padding.

        void fill(value_type const &f) {
            for (auto first = __base::pad_begin(), last = __base::pad_end(); first != last; ++first) {
                __helper_fill(*first, f);
            }
        }
        void reset() { this->fill(value_type()); }

        static constexpr difference_type pad() noexcept { return __base::pad_size(); }
        difference_type Nx() const noexcept { return difference_type(__base::size()); }
        difference_type padNx() const noexcept { return pad() + Nx(); }
        difference_type padNxPad() const noexcept { return padNx() + pad(); }
        difference_type Ny() const noexcept { return difference_type(__base::operator[](0).size()); }
        difference_type padNy() const noexcept { return pad() + Ny(); }
        difference_type padNyPad() const noexcept { return padNy() + pad(); }
        difference_type Nz() const noexcept { return difference_type(__base::operator[](0)[0].size()); }
        difference_type padNz() const noexcept { return pad() + Nz(); }
        difference_type padNzPad() const noexcept { return padNz() + pad(); }

    private:
        static void __helper_fill(__outer_array &a, value_type const &f) {
            for (auto first = a.pad_begin(), last = a.pad_end(); first != last; ++first) {
                first->fill(f);
            }
        }
    };

    // MARK: Aliases
    //
    template <class T, long ND, long Pad>
    using PaddedScalarField = PaddedField<Numeric::Vector<T, 1>, ND, Pad>;
    template <class T, long ND, long Pad>
    using PaddedVectorField = PaddedField<Numeric::Vector<T, 3>, ND, Pad>;
    template <class T, long ND, long Pad>
    using PaddedTensorField = PaddedField<Numeric::Vector<T, 6>, ND, Pad>;

} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* PaddedField_h */
