//
//  UTLSIMD.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMD_h
#define UTLSIMD_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <type_traits>
#include <memory>

// TODO: Fallback when GCC vector_size attribute is not supported.

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class T, long N>
    struct MakeSIMD;

#define _UTL_MAKE_SIMD(T) \
template <> struct MakeSIMD<T,    02L> { using type = T __attribute__ (( vector_size (sizeof(T)*   02) )); }; \
template <> struct MakeSIMD<T,    04L> { using type = T __attribute__ (( vector_size (sizeof(T)*   04) )); }; \
template <> struct MakeSIMD<T,   010L> { using type = T __attribute__ (( vector_size (sizeof(T)*  010) )); }; \
template <> struct MakeSIMD<T,   020L> { using type = T __attribute__ (( vector_size (sizeof(T)*  020) )); }; \
template <> struct MakeSIMD<T,   040L> { using type = T __attribute__ (( vector_size (sizeof(T)*  040) )); }; \
template <> struct MakeSIMD<T,  0100L> { using type = T __attribute__ (( vector_size (sizeof(T)* 0100) )); }; \
template <> struct MakeSIMD<T,  0200L> { using type = T __attribute__ (( vector_size (sizeof(T)* 0200) )); }; \
template <> struct MakeSIMD<T,  0400L> { using type = T __attribute__ (( vector_size (sizeof(T)* 0400) )); }; \
template <> struct MakeSIMD<T, 01000L> { using type = T __attribute__ (( vector_size (sizeof(T)*01000) )); }; \
template <> struct MakeSIMD<T, 02000L> { using type = T __attribute__ (( vector_size (sizeof(T)*02000) )); };

    // integral types:
    _UTL_MAKE_SIMD(char);
    _UTL_MAKE_SIMD(unsigned char);

    _UTL_MAKE_SIMD(short);
    _UTL_MAKE_SIMD(unsigned short);

    _UTL_MAKE_SIMD(int);
    _UTL_MAKE_SIMD(unsigned int);

    _UTL_MAKE_SIMD(long);
    _UTL_MAKE_SIMD(unsigned long);

    _UTL_MAKE_SIMD(long long);
    _UTL_MAKE_SIMD(unsigned long long);

    // floating-point types:
    _UTL_MAKE_SIMD(float);
    _UTL_MAKE_SIMD(double);
#undef _UTL_MAKE_SIMD
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLSIMD_h */
