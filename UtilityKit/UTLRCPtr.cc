//
//  UTLRCPtr.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/25/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLRCPtr.h"

// MARK: Version 3
//

UTL::__3_::_RefCounter::_RefCounter() noexcept(noexcept(lock_type()))
: _rc(1), __mx_() {
}

UTL::__3_::_RefCounter::operator counter_type() const noexcept
{
    return _rc;
}

auto UTL::__3_::_RefCounter::operator--()
-> counter_type {
    LockG<lock_type> l(__mx_);
    return --_rc;
}

auto UTL::__3_::_RefCounter::operator++()
-> _RefCounter& {
    LockG<lock_type> l(__mx_);
    return static_cast<void>(++_rc), *this;
}
