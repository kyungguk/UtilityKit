//
//  UTLAny.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/30/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLAny_hh
#define UTLAny_hh

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <exception>
#include <utility>

// MARK: Any::bas_cast
//
class UTL::__4_::Any::bad_cast : public std::exception {
    friend Any;
    const char *_reason;
    explicit bad_cast() noexcept : _reason(__PRETTY_FUNCTION__) {}
public:
    virtual ~bad_cast() = default;
    virtual const char* what() const noexcept { return _reason; }
};

// MARK: Value Place Holder
//
class UTL::__4_::Any::_PlaceHolder {
public:
    virtual ~_PlaceHolder() = default;
    virtual const std::type_info& type() const noexcept = 0;
    virtual Any any() const& = 0;
    virtual Any any() && = 0;
};

// MARK: Value Wrapper
//
template <class ValueType>
class UTL::__4_::Any::_ValueWrapper : public _PlaceHolder {
    ValueType _v;

    explicit _ValueWrapper(ValueType const& v) : _v(v) {}
    explicit _ValueWrapper(ValueType&& v) : _v(std::move(v)) {}

    ValueType& operator*() noexcept { return _v; }
    ValueType* operator&() noexcept { return &_v; }
    ValueType const& operator*() const noexcept { return _v; }
    ValueType const* operator&() const noexcept { return &_v; }

    friend Any;
public:
    const std::type_info& type() const noexcept override { return typeid(ValueType); }
    Any any() const& override { return Any{_v}; }
    Any any() && override { return Any{std::move(_v)}; }
};

// MARK: Any Impl
template <class ValueType, typename std::enable_if<!std::is_same<typename std::decay<ValueType>::type, UTL::__4_::Any>::value, long>::type>
UTL::__4_::Any::Any(ValueType&& value)
: Any() {
    this->template emplace<ValueType>(std::forward<ValueType>(value));
}

template <class ValueType, typename std::enable_if<!std::is_same<typename std::decay<ValueType>::type, UTL::__4_::Any>::value, long>::type>
auto UTL::__4_::Any::operator=(ValueType&& rhs)
-> Any& {
    Any(std::forward<ValueType>(rhs)).swap(*this);
    return *this;
}

template <class ValueType, class... Args>
auto UTL::__4_::Any::emplace(Args&&... args)
-> typename std::decay<ValueType>::type& {
    using _Wrapper = _ValueWrapper<typename std::remove_cv<typename std::remove_reference<ValueType>::type>::type>;
    _Wrapper *wrapper = new _Wrapper(std::forward<Args>(args)...);
    _ptr.reset(wrapper);
    return **wrapper;
}

template <class ValueType>
ValueType UTL::__4_::Any::cast(Any const& any) {
    if (typeid(ValueType) != any.type()) throw bad_cast{};
    return static_cast<ValueType>(*cast<typename std::remove_cv<typename std::remove_reference<ValueType>::type>::type>(&any));
}
template <class ValueType>
ValueType UTL::__4_::Any::cast(Any& any) {
    if (typeid(ValueType) != any.type()) throw bad_cast{};
    return static_cast<ValueType>(*cast<typename std::remove_cv<typename std::remove_reference<ValueType>::type>::type>(&any));
}
template <class ValueType>
ValueType UTL::__4_::Any::cast(Any&& any) {
    if (typeid(ValueType) != any.type()) throw bad_cast{};
    return static_cast<ValueType>(std::move(*cast<typename std::remove_cv<typename std::remove_reference<ValueType>::type>::type>(&any)));
}
template <class ValueType>
ValueType const* UTL::__4_::Any::cast(Any const* any) noexcept {
    if (!any || typeid(ValueType) != any->type()) return nullptr;
    return &dynamic_cast<_ValueWrapper<typename std::remove_cv<typename std::remove_reference<ValueType>::type>::type> const &>(*any->_ptr.get());
}
template <class ValueType>
ValueType* UTL::__4_::Any::cast(Any* any) noexcept {
    if (!any || typeid(ValueType) != any->type()) return nullptr;
    return &dynamic_cast<_ValueWrapper<typename std::remove_cv<typename std::remove_reference<ValueType>::type>::type> &>(*any->_ptr.get());
}

// MARK: std::swap
//
namespace std {
    inline void swap(UTL::__4_::Any &a, UTL::__4_::Any &b) noexcept(noexcept(a.swap(b))) {
        a.swap(b);
    }
}

#endif /* UTLAny_hh */
