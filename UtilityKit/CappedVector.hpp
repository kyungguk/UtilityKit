//
//  CappedVector.hpp
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 6/18/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef CappedVector_hpp
#define CappedVector_hpp

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/CappedVector.h>
#include <algorithm>
#include <stdexcept>
#include <sstream>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
    template<class _Tp, long _MaxSize>
    CappedVector<_Tp, _MaxSize>::CappedVector(size_type __sz)
    : __size_(__sz)
    {
        if (_MaxSize < __sz)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        //std::fill_n(__elems_, __size_, _Tp());
    }
    template<class _Tp, long _MaxSize>
    CappedVector<_Tp, _MaxSize>::CappedVector(size_type __sz, const value_type& __x)
    : __size_(__sz)
    {
        if (_MaxSize < __sz)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        std::fill_n(__elems_, __size_, __x);
    }
    template<class _Tp, long _MaxSize>
    template<class _It>
    CappedVector<_Tp, _MaxSize>::CappedVector(_It first, _It last)
    : __size_(0)
    {
        for (; first!=last && __size_<_MaxSize; ++first, ++__size_)
        { __elems_[__size_] = *first; }
        if (first != last)
        { throw std::length_error(__PRETTY_FUNCTION__); }
    }

    template<class _Tp, long _MaxSize>
    auto CappedVector<_Tp, _MaxSize>::at(size_type __n)
    -> reference
    {
        if (__n >= __size_)
        { throw std::out_of_range(__PRETTY_FUNCTION__); }
        return __elems_[__n];
    }
    template<class _Tp, long _MaxSize>
    auto CappedVector<_Tp, _MaxSize>::at(size_type __n) const
    -> const_reference
    {
        if (__n >= __size_)
        { throw std::out_of_range(__PRETTY_FUNCTION__); }
        return __elems_[__n];
    }

    template<class _Tp, long _MaxSize>
    auto CappedVector<_Tp, _MaxSize>::front()
    -> reference
    {
        if (0 == __size_)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        return __elems_[0];
    }
    template<class _Tp, long _MaxSize>
    auto CappedVector<_Tp, _MaxSize>::front() const
    -> const_reference
    {
        if (0 == __size_)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        return __elems_[0];
    }

    template<class _Tp, long _MaxSize>
    auto CappedVector<_Tp, _MaxSize>::back()
    -> reference
    {
        if (0 == __size_)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        return __elems_[__size_-1];
    }
    template<class _Tp, long _MaxSize>
    auto CappedVector<_Tp, _MaxSize>::back() const
    -> const_reference
    {
        if (0 == __size_)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        return __elems_[__size_-1];
    }

    template<class _Tp, long _MaxSize>
    void CappedVector<_Tp, _MaxSize>::push_back(const value_type& __x)
    {
        if (_MaxSize == __size_)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        __elems_[__size_++] = __x;
    }
    template<class _Tp, long _MaxSize>
    void CappedVector<_Tp, _MaxSize>::push_back(value_type&& __x)
    {
        if (_MaxSize == __size_)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        __elems_[__size_++] = std::move(__x);
    }

    template<class _Tp, long _MaxSize>
    auto CappedVector<_Tp, _MaxSize>::pop_back()
    -> value_type&&
    {
        if (0 == __size_)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        return std::move(__elems_[--__size_]);
    }

    template<class _Tp, long _MaxSize>
    void CappedVector<_Tp, _MaxSize>::resize(size_type __sz)
    {
        if (_MaxSize < __sz)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        //else if (__size_ < __sz)
        //{ std::fill_n(__elems_+__size_, __sz-__size_, _Tp()); }
        __size_ = __sz;
    }
    template<class _Tp, long _MaxSize>
    void CappedVector<_Tp, _MaxSize>::resize(size_type __sz, const value_type& __x)
    {
        if (_MaxSize < __sz)
        { throw std::length_error(__PRETTY_FUNCTION__); }
        else if (__size_ < __sz)
        { std::fill_n(__elems_+__size_, __sz-__size_, __x); }
        __size_ = __sz;
    }

    template<class _Tp, long _MaxSize>
    void CappedVector<_Tp, _MaxSize>::swap(CappedVector& __v)
    {
        std::swap_ranges(__elems_, __elems_+_MaxSize, __v.__elems_);
        std::swap(__size_, __v.__size_);
    }

    // MARK: Global operators:
    template<class _Tp, long _MaxSize>
    inline bool operator==(const CappedVector<_Tp, _MaxSize>& x, const CappedVector<_Tp, _MaxSize>& y)
    {
        return std::equal(x.__elems_, x.__elems_ + x.__size_, y.__elems_);
    }
    template<class _Tp, long _MaxSize>
    inline bool operator!=(const CappedVector<_Tp, _MaxSize>& x, const CappedVector<_Tp, _MaxSize>& y)
    {
        return !(x == y);
    }
    template<class _Tp, long _MaxSize>
    inline bool operator<(const CappedVector<_Tp, _MaxSize>& x, const CappedVector<_Tp, _MaxSize>& y)
    {
        return std::lexicographical_compare(x.__elems_, x.__elems_ + x.__size_, y.__elems_, y.__elems_ + y.__size_);
    }
    template<class _Tp, long _MaxSize>
    inline bool operator>(const CappedVector<_Tp, _MaxSize>& x, const CappedVector<_Tp, _MaxSize>& y)
    {
        return y < x;
    }
    template<class _Tp, long _MaxSize>
    inline bool operator<=(const CappedVector<_Tp, _MaxSize>& x, const CappedVector<_Tp, _MaxSize>& y)
    {
        return !(y < x);
    }
    template<class _Tp, long _MaxSize>
    inline bool operator>=(const CappedVector<_Tp, _MaxSize>& x, const CappedVector<_Tp, _MaxSize>& y)
    {
        return !(x < y);
    }

    // MARK: Output stream:
    template<class _CharT, class _Traits, class _Tp, long _MaxSize>
    std::basic_ostream<_CharT, _Traits>&
    operator<<(std::basic_ostream<_CharT, _Traits>& __os, CappedVector<_Tp, _MaxSize> const& __a)
    {
        std::basic_ostringstream<_CharT, _Traits> __s; {
            __s.flags(__os.flags());
            __s.imbue(__os.getloc());
            __s.precision(__os.precision());
        }
        typename CappedVector<_Tp, _MaxSize>::size_type const sz = __a.size();
        if (0 == sz) {
            __s << "{}";
        }
        else {
            typename CappedVector<_Tp, _MaxSize>::size_type i = 0;
            __s << "{", __a[i++]; while (i < sz) __s << ", " << __a[i++]; __s << "}";
        }
        return __os << __s.str();
    }
} // namespace __3_
UTILITYKIT_END_NAMESPACE

// std::swap:
namespace std {
    template<class _Tp, long _MaxSize> inline
    void swap(UTILITYKIT_NAMESPACE::__3_::CappedVector<_Tp, _MaxSize>& x,
              UTILITYKIT_NAMESPACE::__3_::CappedVector<_Tp, _MaxSize>& y) noexcept(noexcept(x.swap(y))) {
        x.swap(y);
    }
}

#endif /* CappedVector_hpp */
