//
//  UTL_is_aligned.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 12/21/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTL_is_aligned_h
#define UTL_is_aligned_h

#include <UtilityKit/UtilityKit-config.h>
#include <type_traits>

UTILITYKIT_BEGIN_NAMESPACE
namespace {
    /**
     Check for memory alignment.
     */
    template <long Alignment, typename T>
    inline bool is_aligned(T &x) noexcept {
        static_assert(Alignment > 0 && 0 == Alignment % alignof(T), "Alignment is not multiples of alignof(T)");
        return !(reinterpret_cast<long const>(static_cast<void const*>(std::addressof(x))) & (Alignment - 1));
    }
}
UTILITYKIT_END_NAMESPACE

#endif /* UTL_is_aligned_h */
