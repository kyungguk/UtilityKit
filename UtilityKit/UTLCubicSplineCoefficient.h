//
//  UTLCubicSplineCoefficient.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/20/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLCubicSplineCoefficient_h
#define UTLCubicSplineCoefficient_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLSplineCoefficient.h>
#include <type_traits>
#include <iterator>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class RealType = double>
    class CubicSplineCoefficient;

    /**
     @brief Construct cubic (3rd order) spline coefficients.
     Implementation from Numerical Recipe.
     */
    template <class RealType>
    class CubicSplineCoefficient : public SplineCoefficient<RealType, 3> {
        using Base = SplineCoefficient<RealType, 3>;

    public:
        template <class It, typename std::enable_if< // It::value_type = std::pair<value_type, value_type>
        std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<It>::iterator_category>::value,
        long>::type = 0L>
        CubicSplineCoefficient(It first, It last) : CubicSplineCoefficient(Base::tabulate(first, last)) {}

        template <class It1, class It2, typename std::enable_if< // It1::value_type == It2::value_type == value_type
        std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<It1>::iterator_category>::value &&
        std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<It2>::iterator_category>::value,
        long>::type = 0L>
        CubicSplineCoefficient(It1 first1, It1 last1, It2 first2) : CubicSplineCoefficient(Base::tabulate(first1, last1, first2)) {}

    private:
        explicit CubicSplineCoefficient(typename Base::coefficient_table_type &&table) { _construct_coefs(table); Base::init(std::move(table)); }

        inline static void _construct_coefs(typename Base::coefficient_table_type &table);

        // tridiagonal linear solver (in-place)
        //
        inline static void _solve_tridiag(typename Base::coefficient_table_type &table);
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLCubicSplineCoefficient.hh>

#endif /* UTLCubicSplineCoefficient_h */
