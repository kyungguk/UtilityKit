//
//  UTLShapeFunction.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/16/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLShapeFunction_hh
#define UTLShapeFunction_hh

// MARK:- UTL::__4_::Shape<T, Order>
//
template <class T>
void UTL::__4_::Shape<T, 1L>::operator()(const value_type x) noexcept {
    constexpr index_type ione{1};
    constexpr value_type fone{1};

    _i.fill(this->floor(x)); // i0 = floor(x), where x = x/dx
    std::get<1>(_i) += ione;
    std::get<1>(_w) = x - std::get<0>(_i); // w1 = x - i0, i becomes i1
    std::get<0>(_w) = fone - std::get<1>(_w); // at i = i0, w = w0 = 1 - (x - i0)
}

template <class T>
void UTL::__4_::Shape<T, 2L>::operator()(value_type x) noexcept {
    constexpr index_type ione{1};
    constexpr value_type fone{1}, half = T{1}/2, _3_4 = T{3}/4;

    _i.fill(this->round(x));
    std::get<0>(_i) -= ione;
    // i1 = round(x), where x = x/dx
    std::get<2>(_i) += ione;
    // i = i1:
    x = std::get<1>(_i) - x; // (i - x)
    std::get<1>(_w) = _3_4 - (x*x); // i = i1, w = w1 = 3/4 - (x - i)^2
    // i = i0:
    x += half; // (i - x) + 1/2
    std::get<0>(_w) = half * (x*x); // i = i0, w = w0 = 1/2 * (1/2 - (x - i))^2
    // i = i2:
    std::get<2>(_w) = fone - (std::get<0>(_w) + std::get<1>(_w));
}

void UTL::__4_::Shape<double, 3L>::operator()(value_type x) noexcept {
    _m1 = this->ceil(x) - 2;
    _w.fill(1./6);

    // for -2 <= x < -1
    //
    _w[0] *= UTL::pow<3>(2 + (this->i<0>() - x));

    // for 1 <= x < 2
    //
    _w[3] *= UTL::pow<3>(2 - (this->i<3>() - x));

    // for -1 <= x < 0
    //
    x -= this->i<1>(); // x - i0
    _w[1] *= 4 + 3*x*x*(x - 2);

    // for 0 <= x < 1
    //
    _w[2] = 1 - (std::get<0>(_w) + std::get<1>(_w) + std::get<3>(_w));
}

#endif /* UTLShapeFunction_hh */
