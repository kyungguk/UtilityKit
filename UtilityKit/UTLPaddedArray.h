//
//  UTLPaddedArray.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/22/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLPaddedArray_h
#define UTLPaddedArray_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLArray.h>
#include <initializer_list>
#include <type_traits>
#include <algorithm>
#include <iterator>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class Type, long PadSize> class PaddedArray;

    //@{
    /**
     @brief Create a storage-shared slice of PaddedArray.
     */
    template <long NewPad, class Type, long OldPad>
    PaddedArray<Type, NewPad> make_slice(PaddedArray<Type, OldPad> &A, long const loc, long const len) {
        return A.template slice<NewPad>(loc, len);
    }
    template <long NewPad, class Type, long OldPad>
    PaddedArray<Type, NewPad> make_slice(PaddedArray<Type, OldPad> &A) {
        return A.template slice<NewPad>();
    }
    //@}

    /**
     @brief Fixed-length array with non-zero padding at both sides.
     */
    template <class Type, long PadSize>
    class PaddedArray : public Array<Type> {
        static_assert(PadSize >= 0, "PadSize is negative");

    public:
        // types:
        using value_type             = typename Array<Type>::value_type;
        using reference              = typename Array<Type>::reference;
        using const_reference        = typename Array<Type>::const_reference;
        using pointer                = typename Array<Type>::pointer;
        using const_pointer          = typename Array<Type>::const_pointer;
        using iterator               = typename Array<Type>::iterator;
        using const_iterator         = typename Array<Type>::const_iterator;
        using reverse_iterator       = typename Array<Type>::reverse_iterator;
        using const_reverse_iterator = typename Array<Type>::const_reverse_iterator;
        using size_type              = typename Array<Type>::size_type;
        using difference_type        = typename Array<Type>::difference_type;

    private:
        using smart_pointer = std::shared_ptr<value_type>;
        smart_pointer _storage{nullptr};

    protected:
        smart_pointer const &storage() & noexcept { return _storage; } // intentional const ref return

    public:
        // destructor:
        ~PaddedArray() = default;

        /**
         @brief Construct an array whose state is invalid.
         @discussion No storage allocation occurs.
         */
        explicit PaddedArray() noexcept {}

        /**
         @brief Construct a fixed-size array whose storage is provided.
         @discussion The pointer must be valid and must hold at least max_size() elements.

         The second version does not hold owership of the storage.
         @exception When p == nullptr.
         */
        explicit PaddedArray(size_type const sz, smart_pointer&& p);
        explicit PaddedArray(size_type const sz, pointer const p) : PaddedArray(sz, smart_pointer{p, [](pointer) {}}) {}

        /**
         @brief Construct a fixed-size array whose elements including the paddings are initialized with either the default value (first version) or the given value (second version).
         @discussion If both pad_size() and sz are 0, this call is equivalent to the default constructor.
         */
        explicit PaddedArray(size_type const sz);
        explicit PaddedArray(size_type const sz, value_type const& x);

        /**
         @brief Construct a fixed-size array from an iterator.
         @discussion The paddings are also constructed from the iterator.
         This means that the iterator distance must be at least 2*pad_size().
         */
        template <class ForwardIt, typename std::enable_if<std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<ForwardIt>::iterator_category>::value, long>::type = 0L>
        PaddedArray(ForwardIt first, ForwardIt last);
        /**
         @brief Construct a fixed-size array from an initializer list.
         @discussion The paddings are excluded from the iterator, and are default initialized.
         */
        explicit PaddedArray(std::initializer_list<value_type> il) : PaddedArray(il.begin(), il.end()) {}

        /**
         @biref Copy constructor.
         */
        PaddedArray(PaddedArray const& o) : PaddedArray(o.pad_begin(), o.pad_end()) {}
        /**
         @biref Move constructor.
         @discussion The underlying storage is moved.
         */
        PaddedArray(PaddedArray&& o) noexcept : Array<Type>(std::move(o)), _storage(std::move(o._storage)) {}

        /**
         @brief Element-wise copy assignment (including paddings).
         @discussion *this and other must be a valid array and of the same size.
         */
        PaddedArray& operator=(PaddedArray const& other);
        /**
         @brief Element-wise move assignment (including paddings).
         @discussion *this and other must be a valid array and of the same size.

         Be careful when copy assigning array returned from a function, which will trigger move assignment.
         */
        PaddedArray& operator=(PaddedArray&& other);

        /**
         @brief Element-wise copy assignment from an array with a different padding size.
         @discussion *this and other should be a valid array and of the same size.

         The call is equivalent to std::copy(other.begin()-pad, other.end()+pad, this->begin()-pad),
         where pad = std::min(this->pad_size(), other->pad_size()).
         */
        template <long N>
        PaddedArray& operator=(PaddedArray<value_type, N> const& other);
        /**
         @brief Element-wise move assignment from an array with a different padding size.
         @discussion *this and other should be a valid array and of the same size.

         The call is equivalent to std::copy(other.begin()-pad, other.end()+pad, this->begin()-pad),
         where pad = std::min(this->pad_size(), other->pad_size()).

         Be careful when copy assigning array returned from a function, which will trigger move assignment.
         */
        template <long N>
        PaddedArray& operator=(PaddedArray<value_type, N>&& other);

        // observers:
        /**
         @brief Returns false if *this has been constructed with PaddedArray().
         */
        explicit operator bool() const noexcept { return bool(_storage); }

        /**
         @brief Returns the underlying storage.
         @discussion Upon return, *this becomes as if it has been default-constructed.
         */
        std::shared_ptr<Type> storage() && noexcept { return PaddedArray{std::move(*this)}._storage; }

        // transformations:
        /**
         @brief Returns a storage-shared slice of *this.
         @discussion The new array shares storage with *this, so it remains valid even if *this goes away.

         The location is given relative to this->begin(), and the length will be the size of the new array.

         @note For NewPad == 0, operator bool() on the new array can return true even though length == 0, if bool(*this) is true.
         */
        template <long NewPad>
        PaddedArray<Type, NewPad> slice(size_type location, size_type const length);
        PaddedArray slice(size_type const location, size_type const length) { return slice<PaddedArray::pad_size()>(location, length); }

        /**
         @brief Semantically equivalent to slice<NewPad>(NewPad - pad_size(), max_dims() - 2*NewPad).
         @discussion Creates an array that essentially mirrors *this.
         */
        template <long NewPad>
        PaddedArray<Type, NewPad> slice();
        PaddedArray slice() { return slice<PaddedArray::pad_size()>(); }

        // modifiers:
        /**
         @brief Fill the contents of *this (including paddings) with the fill value.
         */
        void fill(value_type const& v) noexcept(std::is_nothrow_copy_assignable<value_type>::value) { std::fill(pad_begin(), pad_end(), v); }

        /**
         @brief Swap the underlying storage of *this with other.
         */
        inline void swap(PaddedArray& other) noexcept;

        // capacity:
        /**
         @brief Returns one-side pad size.
         */
        constexpr static size_type pad_size() noexcept { return size_type{PadSize}; }
        /**
         @brief Returns the maximum number of elements, i.e., size() + 2*pad_size(), or 0 if bool(*this) is false.
         */
        size_type max_size() const noexcept { return this->size() + 2*pad_size()*bool(*this); }

        // iterators with paddings:
        iterator        pad_begin()       noexcept { return iterator{this->begin() - pad_size()*bool(*this)}; }
        const_iterator  pad_begin() const noexcept { return const_iterator{this->begin() - pad_size()*bool(*this)}; }
        const_iterator pad_cbegin() const noexcept { return pad_begin(); }
        iterator          pad_end()       noexcept { return iterator{this->end() + pad_size()*bool(*this)}; }
        const_iterator    pad_end() const noexcept { return const_iterator{this->end() + pad_size()*bool(*this)}; }
        const_iterator   pad_cend() const noexcept { return pad_end(); }

        reverse_iterator        pad_rbegin()       noexcept { return reverse_iterator{pad_end()}; }
        const_reverse_iterator  pad_rbegin() const noexcept { return const_reverse_iterator{pad_end()}; }
        const_reverse_iterator pad_crbegin() const noexcept { return pad_rbegin(); }
        reverse_iterator          pad_rend()       noexcept { return reverse_iterator{pad_begin()}; }
        const_reverse_iterator    pad_rend() const noexcept { return const_reverse_iterator{pad_begin()}; }
        const_reverse_iterator   pad_crend() const noexcept { return pad_rend(); }

        // element access:
        pointer       data()       noexcept { return _storage.get(); }
        const_pointer data() const noexcept { return _storage.get(); }

        reference       at(size_type i)       override;
        const_reference at(size_type i) const override;

        reference       pad_front()       noexcept { return *pad_begin(); }
        const_reference pad_front() const noexcept { return *pad_begin(); }
        reference       pad_back ()       noexcept { return *(pad_end() - 1); }
        const_reference pad_back () const noexcept { return *(pad_end() - 1); }

    private:
        using _Alloc = std::allocator<value_type>;

        static pointer _allocate(size_type const n) {
            if (n <= 0) {
                return nullptr;
            }
            static _Alloc alloc{};
            return std::allocator_traits<_Alloc>::allocate(alloc, static_cast<unsigned long>(n));
        }
        static void _deallocate(pointer const p, size_type const n) noexcept {
            static _Alloc alloc{};
            if (p) {
                std::allocator_traits<_Alloc>::deallocate(alloc, p, static_cast<unsigned long>(n));
            }
        }

        template <class... Args>
        static void _construct(pointer const p, Args&&... args) {
            static _Alloc alloc{};
            std::allocator_traits<_Alloc>::construct(alloc, p, std::forward<Args>(args)...);
        }
        static void _unwind(pointer const p, size_type n) noexcept {
            static _Alloc alloc{};
            while (n--) {
                std::allocator_traits<_Alloc>::destroy(alloc, p + n);
            }
        }

        static void _delete(pointer const p, size_type const n) noexcept {
            if (p) {
                _unwind(p, n);
                _deallocate(p, n);
            }
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLPaddedArray.hh>

#endif /* UTLPaddedArray_h */
