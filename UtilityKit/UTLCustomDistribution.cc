//
//  UTLCustomDistribution.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLCustomDistribution.h"

// MARK: Version 3
//
#include <utility>

UTL::__3_::CustomDistribution::CustomDistribution(CustomDistribution const &o) noexcept
: Distribution(), _pdf(o._pdf), _cdf(o._cdf), _icdf(o._icdf),
_min(o._min), _max(o._max), _mean(o._mean), _var(o._var) {
}
auto UTL::__3_::CustomDistribution::operator=(CustomDistribution const &o) noexcept
-> CustomDistribution &{
    if (this != &o) {
        _pdf = o._pdf;
        _cdf = o._cdf;
        _icdf = o._icdf;
        _min = o._min;
        _max = o._max;
        _mean = o._mean;
        _var = o._var;
    }
    return *this;
}
UTL::__3_::CustomDistribution::CustomDistribution(CustomDistribution &&o) noexcept
: Distribution(), _pdf(std::move(o._pdf)), _cdf(std::move(o._cdf)), _icdf(std::move(o._icdf)), _min(std::move(o._min)), _max(std::move(o._max)), _mean(std::move(o._mean)), _var(std::move(o._var)) {
}
auto UTL::__3_::CustomDistribution::operator=(CustomDistribution &&o) noexcept
-> CustomDistribution &{
    if (this != &o) {
        _pdf = std::move(o._pdf);
        _cdf = std::move(o._cdf);
        _icdf = std::move(o._icdf);
        _min = std::move(o._min);
        _max = std::move(o._max);
        _mean = std::move(o._mean);
        _var = std::move(o._var);
    }
    return *this;
}

auto UTL::__3_::CustomDistribution::pdf(value_type x) const
-> value_type {
    return _pdf(x);
}
auto UTL::__3_::CustomDistribution::cdf(value_type x) const
-> value_type {
    return _cdf(x);
}
auto UTL::__3_::CustomDistribution::icdf(value_type cdf) const
-> value_type {
    return _icdf(cdf);
}

auto UTL::__3_::CustomDistribution::_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.maxRecursion = 20;
        sampler.accuracyGoal = 7;
        sampler.initialPoints = 100;
        sampler.yScaleAbsoluteTolerance = 1e-10;
    }
    return sampler;
}


// MARK:- Version 4
//
#include <stdexcept>
#include <string>

UTL::__4_::CustomDistribution::CustomDistribution(CustomDistribution const &o)
: Distribution(),
_min(o._min), _max(o._max), _mean(o._mean), _var(o._var),
_pdf(o._pdf), _cdf(o._cdf), _icdf(o._icdf) {
}
UTL::__4_::CustomDistribution::CustomDistribution(CustomDistribution &&o)
: Distribution(),
_min(o._min), _max(o._max), _mean(o._mean), _var(o._var),
_pdf(), _cdf(), _icdf() {
    _pdf = std::move(o._pdf);
    _cdf = std::move(o._cdf);
    _icdf = std::move(o._icdf);
}
auto UTL::__4_::CustomDistribution::operator=(CustomDistribution const &o)
-> CustomDistribution &{
    if (this != &o) {
        CustomDistribution{o}.swap(*this);
    }
    return *this;
}
auto UTL::__4_::CustomDistribution::operator=(CustomDistribution &&o)
-> CustomDistribution &{
    if (this != &o) {
        CustomDistribution{std::move(o)}.swap(*this);
    }
    return *this;
}

void UTL::__4_::CustomDistribution::swap(CustomDistribution &o)
{
    _pdf.swap(o._pdf);
    _cdf.swap(o._cdf);
    _icdf.swap(o._icdf);
    std::swap(_min, o._min);
    std::swap(_max, o._max);
    std::swap(_mean, o._mean);
    std::swap(_var, o._var);
}

auto UTL::__4_::CustomDistribution::pdf(value_type const x) const
-> value_type {
    try {
        return _pdf(x)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}
auto UTL::__4_::CustomDistribution::cdf(value_type const x) const
-> value_type {
    try {
        return _cdf(x)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}
auto UTL::__4_::CustomDistribution::icdf(value_type const cdf) const
-> value_type {
    try {
        return _icdf(cdf)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

auto UTL::__4_::CustomDistribution::default_integrator() noexcept
-> integrator_type {
    integrator_type integrator; {
        integrator.set_accuracy_goal(7);
        integrator.set_max_recursion(20);
        integrator.set_min_recursion(5);
    }
    return integrator;
}
auto UTL::__4_::CustomDistribution::default_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.set_max_recursion(20);
        sampler.set_accuracy_goal(7);
        sampler.set_initial_points(100);
        sampler.set_y_scale_absolute_tolerance(1e-10);
    }
    return sampler;
}
