//
//  UTLSubtractedMaxwellianDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSubtractedMaxwellianDistribution_h
#define UTLSubtractedMaxwellianDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Subtracted Maxwellian speed distribution.
     @discussion Isotropic subtracted Maxwellian speed.

     For Maxwellian:
     pdf(vth; v) = v^2*exp(-v^2/vth^2) * 4/(√pi vth^3).
     cdf(vth; v) = Integrate[pdf(v'), {v', 0, v}] = erf(v/vth) - 2/√pi*(v/vth)*exp(-v^2/vth^2).
     <v>(vth) = 2*vth/√pi.
     <v^2>(vth) = 3/2*vth^2.

     Subtracted Maxwellian (n2/n1 < (vth2/vth1)^3):
     pdf = n1*pdf(vth1; v) - n2*pdf(vth2; v).
     cdf = n1*cdf(vth1; v) - n2*cdf(vth2; v).
     mean <v> = n1*<v>(vth1) - n2*<v>(vth2).
     var <v^2> - <v>^2 = (n1*<v^2>(vth1) - n2*<v^2>(vth2)) - (n1*<v>(vth1) - n2*<v>(vth2))^2.
     */
    class SubtractedMaxwellianDistribution final : public Distribution {
        using interp_type = MonotoneCubicInterpolation<value_type>;

        interp_type _icdf;
        std::pair<value_type, value_type> _n;
        std::pair<value_type, value_type> _vth;

    public:
        // constructors
        //
        /**
         Construct subtracted Maxwellian distribution with the relative ratio and thermal speed.
         @discussion It is required to have n2/n1 < (vth2/vth1)^3. Otherwide, an invalid_argument exception will be thrown.

         @param n A pair<n1,n2> (all positive).
         @param vth A pair<vth1,vth2> (all positive).
         @param should_normalize_cdf If true, normalize the sampled cdf points. Default is true.
         */
        explicit SubtractedMaxwellianDistribution(std::pair<value_type, value_type> n, std::pair<value_type, value_type> vth, bool should_normalize_cdf = true);

        // copy/move
        //
        SubtractedMaxwellianDistribution(SubtractedMaxwellianDistribution const &) noexcept;
        SubtractedMaxwellianDistribution &operator=(SubtractedMaxwellianDistribution const &) noexcept;
        SubtractedMaxwellianDistribution(SubtractedMaxwellianDistribution &&) noexcept;
        SubtractedMaxwellianDistribution &operator=(SubtractedMaxwellianDistribution &&) noexcept;

        // properties
        //
        /**
         Two Maxwellian concentrations.
         */
        std::pair<value_type, value_type> n() const noexcept { return _n; }
        /**
         Two Maxwellian thermal speeds (note the definition).
         */
        std::pair<value_type, value_type> vth() const noexcept { return _vth; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type v) const override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type v) const override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type cdf) const override;

    private: // helpers:
        value_type _helper_cdf(value_type x) const noexcept;

        using sampler_type = AdaptiveSampling1D<value_type>;
        static sampler_type _sampler() noexcept;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>
#include <UtilityKit/NumericKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Subtracted Maxwellian speed distribution.
     @discussion Isotropic subtracted Maxwellian speed.

     For Maxwellian:
     pdf(vth; v) = v^2*exp(-v^2/vth^2) * 4/(√pi vth^3).
     cdf(vth; v) = Integrate[pdf(v'), {v', 0, v}] = erf(v/vth) - 2/√pi*(v/vth)*exp(-v^2/vth^2).
     <v>(vth) = 2*vth/√pi.
     <v^2>(vth) = 3/2*vth^2.

     Subtracted Maxwellian (n2/n1 < (vth2/vth1)^3):
     pdf = n1*pdf(vth1; v) - n2*pdf(vth2; v).
     cdf = n1*cdf(vth1; v) - n2*cdf(vth2; v).
     mean <v> = n1*<v>(vth1) - n2*<v>(vth2).
     var <v^2> - <v>^2 = (n1*<v^2>(vth1) - n2*<v^2>(vth2)) - (n1*<v>(vth1) - n2*<v>(vth2))^2.
     */
    class SubtractedMaxwellianDistribution final : public Distribution {
        using spline_coefficient_type = MonotoneCubicCoefficient<value_type>;
        using interpolator_type = spline_coefficient_type::spline_interpolator_type;
        using sampler_type = AdaptiveSampling1D<value_type>;
        using vector_type = SIMDVector<value_type, 2>;

        vector_type _vth; //!< {vth1, vth2}.
        vector_type _n; //!< {n1, n2}.
        interpolator_type _icdf;

    public:
        // constructors
        //
        /**
         Construct subtracted Maxwellian distribution with the relative ratio and thermal speed.
         @discussion It is required to have n2/n1 < (vth2/vth1)^3. Otherwide, an invalid_argument exception will be thrown.

         @param vth1 Thermal speed of the first maxwellain (positive).
         @param T2OT1 The ratio vth2^2/vth1^2 (positive).
         @param n2On1 The ratio n2/n1 (0 <= n2/n1 < 1).
         */
        explicit SubtractedMaxwellianDistribution(value_type const vth1, value_type const T2OT1 = 1, value_type const n2On1 = 0);

        // copy/move
        //
        SubtractedMaxwellianDistribution(SubtractedMaxwellianDistribution const &);
        SubtractedMaxwellianDistribution &operator=(SubtractedMaxwellianDistribution const &);
        SubtractedMaxwellianDistribution(SubtractedMaxwellianDistribution &&);
        SubtractedMaxwellianDistribution &operator=(SubtractedMaxwellianDistribution &&);

        // swap
        //
        void swap(SubtractedMaxwellianDistribution &o);

        // properties
        //
        /**
         Thermal speed of the first maxwellain.
         */
        value_type vth1() const noexcept { return vth<1>(); }
        /**
         @brief The ratio n2/n1.
         */
        value_type n2On1() const noexcept { return n<2>()/n<1>(); }
        /**
         @brief The ratio vth2/vth1.
         */
        value_type T2OT1() const noexcept { value_type tmp = vth<2>()/vth<1>(); return tmp *= tmp; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type const v) const noexcept override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type const v) const noexcept override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type const cdf) const override;

    private: // helpers:
        static sampler_type default_sampler() noexcept;

        template <long i>
        value_type n() const noexcept { return std::get<i - 1>(_n); }
        template <long i>
        value_type vth() const noexcept { return std::get<i - 1>(_vth); }

        inline static value_type gaussian_pdf(value_type const x) noexcept;
        inline static value_type gaussian_cdf(value_type const x) noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLSubtractedMaxwellianDistribution_h */
