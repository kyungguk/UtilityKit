//
//  UTLPrinto.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/29/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLPrinto_h
#define UTLPrinto_h

#include <UtilityKit/UtilityKit-config.h>
#include <utility>
#include <ostream>

// MARK: printo
//
namespace {
    template <class CharT, class Traits, class Arg>
    std::basic_ostream<CharT, Traits> &print(std::basic_ostream<CharT, Traits> &os, Arg&& arg) {
        return os << arg;
    }
    template <class CharT, class Traits, class First, class... Rest>
    std::basic_ostream<CharT, Traits> &print(std::basic_ostream<CharT, Traits> &os, First&& _1, Rest&&... _n) {
        return print(os << _1, std::forward<Rest>(_n)...);
    }
    template <class CharT, class Traits, class... Args>
    std::basic_ostream<CharT, Traits> &println(std::basic_ostream<CharT, Traits> &os, Args&&... args) {
        return print(os, std::forward<Args>(args)...) << std::endl;
    }

    template <class CharT, class Traits, class... Args> //UTILITYKIT_DEPRECATED_ATTRIBUTE
    std::basic_ostream<CharT, Traits> &printo(std::basic_ostream<CharT, Traits> &os, Args&&... args) {
        return print(os, std::forward<Args>(args)...);
    }
}

#endif /* UTLPrinto_h */
