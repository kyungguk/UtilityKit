//
//  MonotoneCubicInterpolation.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 6/10/15.
//
//

#ifndef AlgorithmKit__MonotoneCubicInterpolation_h
#define AlgorithmKit__MonotoneCubicInterpolation_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/UTLSplineCoefficient.h>
#include <UtilityKit/UTLSplineInterpolator.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

/**
 @brief Monotone cubic interpolation algorithm (source from wikipedia).
 @discussion Sorted points are maintained by the default std::map class, and therefore, there may be a performance issue related to that.
 */
template <class RealType>
class MonotoneCubicInterpolation {
public:
    using spline_coefficient_type = MonotoneCubicCoefficient<RealType>;
    using spline_interpolator_type = typename spline_coefficient_type::spline_interpolator_type;
    using value_type = RealType;
    using point_type = std::pair<RealType, RealType>; // we don't use map::value_type, because first of the pair has const qualifier.
    using point_container_type = std::map<RealType, RealType>;
private:
    spline_interpolator_type _interpolator{};

public:
    /**
     @brief Get the underlying spline interpolator.
     */
    spline_interpolator_type const &spline_interpolator() const noexcept { return _interpolator; }

    /**
     @brief Query if the receiver is valid.
     @return true if so.
     */
    explicit operator bool() const noexcept { return bool(_interpolator); }

    /**
     @brief Return interpolated value with linear extrapolation by default.
     */
    value_type operator()(value_type const& x) const;

    /**
     @brief Returns integral of the interpolant between the range with which the interpolation was constructed.
     */
    value_type integral() const noexcept(noexcept(_interpolator.integrate())) { return _interpolator.integrate(); }

    /**
     @brief Returns integral of the interpolant between the range with which the interpolation was constructed.
     @discussion Nil optional is returned when `x` is out of range.
     */
    Optional<value_type> derivative(value_type const& x) const noexcept(noexcept(_interpolator.derivative(x))) { return _interpolator.derivative(x); }

    /**
     @brief Default empty constructor which is invalid.
     */
    explicit MonotoneCubicInterpolation() noexcept = default;

    /**
     @brief Construct with iterator.
     @discussion At least two distinct points should be given.
     */
    template <typename InputIterator>
    explicit MonotoneCubicInterpolation(InputIterator first, InputIterator last);

    /**
     @brief Swap receiver's content with other's content.
     */
    void swap(MonotoneCubicInterpolation &o) { _interpolator.swap(o._interpolator); }

    // copy/move:
    MonotoneCubicInterpolation(MonotoneCubicInterpolation const &) = default;
    MonotoneCubicInterpolation(MonotoneCubicInterpolation &&) = default;
    MonotoneCubicInterpolation& operator=(MonotoneCubicInterpolation const &) = default;
    MonotoneCubicInterpolation& operator=(MonotoneCubicInterpolation &&) = default;
}; //TODO: UTILITYKIT_DEPRECATED_ATTRIBUTE

template <class RealType>
template <typename InputIterator>
MonotoneCubicInterpolation<RealType>::MonotoneCubicInterpolation(InputIterator first, InputIterator last)
{ // Should not use ": _interpolator(spline_coefficient_type(first, last).interpolator())" due to memory corrupt
    _interpolator = spline_coefficient_type(first, last).interpolator();
}

template <class RealType>
auto MonotoneCubicInterpolation<RealType>::operator()(value_type const& x) const
-> value_type {
    if (x >= _interpolator.min_abscissa() && x <= _interpolator.max_abscissa()) return (*_interpolator(x));

    typename spline_interpolator_type::coefficient_table_type const& table = _interpolator.spline_coefficient().table();
    decltype(table.size()) i = x < _interpolator.min_abscissa() ? 0 : table.size() - 2;
    value_type const dx = (table[i+1].first - table[i].first);
    value_type const dy = (std::get<0>(table[i+1].second) - std::get<0>(table[i].second));
    return std::get<0>(table[i].second) + (x - table[i].first)*dy/dx;
}

} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* defined(AlgorithmKit__MonotoneCubicInterpolation_h) */
