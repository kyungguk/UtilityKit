//
//  UTLTests-AlgorithmKit.mm
//  UtilityKitTests
//
//  Created by KYUNGGUK MIN on 10/31/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

#define UTILITYKIT_INLINE_VERSION 4
#include <UtilityKit/NumericKit.h>
#include <UtilityKit/ArrayKit.h>
#include <UtilityKit/AlgorithmKit.h>
#include <UtilityKit/AuxiliaryKit.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <random>
#include <array>

@interface UTLTests_AlgorithmKit : XCTestCase

@end

@implementation UTLTests_AlgorithmKit

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAdaptiveSampling1D {
    // Maxwellian:
    try {
        UTL::__4_::AdaptiveSampling1D<double> as__4_;
        as__4_.set_max_recursion(10);
        as__4_.set_accuracy_goal(4);
        as__4_.set_initial_points(10);
        as__4_.set_y_scale_absolute_tolerance(1e-5);
        UTL::__3_::AdaptiveSampling1D<double> as__3_;
        as__3_.maxRecursion = static_cast<unsigned>(as__4_.max_recursion());
        as__3_.accuracyGoal = static_cast<int>(as__4_.accuracy_goal());
        as__3_.initialPoints = static_cast<unsigned>(as__4_.initial_points());
        as__3_.yScaleAbsoluteTolerance = as__4_.y_scale_absolute_tolerance();

        auto f = [](double v)->double {
            auto v2 = v*v;
            return 2.*M_2_SQRTPI * v2*std::exp(-v2);
        };

        auto const pts__4_ = as__4_(f, 0, 5);
        auto const pts__3_ = as__3_(f, 0, 5);
        XCTAssert(!pts__4_.empty() && std::distance(pts__3_.cbegin(), pts__3_.cend()) == std::distance(pts__4_.cbegin(), pts__4_.cend()));
        for (auto const& pt : pts__4_) {
            XCTAssert(std::abs(std::get<1>(pt) - f(std::get<0>(pt))) < 1e-15);
        }
        auto it__3_ = pts__3_.cbegin();
        for (auto it__4_ = pts__4_.cbegin(); it__4_ != pts__4_.cend(); ++it__4_, ++it__3_) {
            XCTAssert(std::abs(std::get<1>(*it__4_) - std::get<1>(*it__3_)) < 1e-15);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Ring:
    try {
        UTL::__4_::AdaptiveSampling1D<double> as__4_;
        as__4_.set_max_recursion(10);
        as__4_.set_accuracy_goal(4);
        as__4_.set_initial_points(20);
        as__4_.set_y_scale_absolute_tolerance(1e-5);
        UTL::__3_::AdaptiveSampling1D<double> as__3_;
        as__3_.maxRecursion = static_cast<unsigned>(as__4_.max_recursion());
        as__3_.accuracyGoal = static_cast<int>(as__4_.accuracy_goal());
        as__3_.initialPoints = static_cast<unsigned>(as__4_.initial_points());
        as__3_.yScaleAbsoluteTolerance = as__4_.y_scale_absolute_tolerance();

        constexpr double vr = 1.;
        auto f = [](double v)->double {
            auto v_ = v - vr;
            return 2. * v*std::exp(-(v_*v_)) /
            (std::exp(-vr*vr) + 2./M_2_SQRTPI*vr*(1+std::erf(vr)));
        };

        auto const pts__4_ = as__4_(f, 0, vr+4);
        auto const pts__3_ = as__3_(f, 0, vr+4);
        XCTAssert(!pts__4_.empty() && std::distance(pts__3_.cbegin(), pts__3_.cend()) == std::distance(pts__4_.cbegin(), pts__4_.cend()));
        for (auto const& pt : pts__4_) {
            XCTAssert(std::abs(std::get<1>(pt) - f(std::get<0>(pt))) < 1e-15);
        }
        auto it__3_ = pts__3_.cbegin();
        for (auto it__4_ = pts__4_.cbegin(); it__4_ != pts__4_.cend(); ++it__4_, ++it__3_) {
            XCTAssert(std::abs(std::get<1>(*it__4_) - std::get<1>(*it__3_)) < 1e-15);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Sine:
    try {
        UTL::__4_::AdaptiveSampling1D<double> as__4_;
        as__4_.set_max_recursion(20);
        as__4_.set_accuracy_goal(7);
        as__4_.set_initial_points(100);
        as__4_.set_y_scale_absolute_tolerance(1e-10);
        UTL::__3_::AdaptiveSampling1D<double> as__3_;
        as__3_.maxRecursion = static_cast<unsigned>(as__4_.max_recursion());
        as__3_.accuracyGoal = static_cast<int>(as__4_.accuracy_goal());
        as__3_.initialPoints = static_cast<unsigned>(as__4_.initial_points());
        as__3_.yScaleAbsoluteTolerance = as__4_.y_scale_absolute_tolerance();

        constexpr double n = 1.5, n_2 = n/2;
        auto f = [](double alpha)->double {
            double const A = std::tgamma(1 + n_2)/std::tgamma(1.5 + n_2) / M_2_SQRTPI;
            if (std::abs(std::sin(alpha)) < 1e-10) return 0;
            double y = std::pow(std::sin(alpha), n + 1) * 0.5/A;
            return y;
        };

        auto const pts__4_ = as__4_([&f](double x)->double {
            return UTL::__3_::trapz::qsimp(f, std::make_pair(double(0), x), 4).first;
        }, 0, M_PI, 1);
        auto const pts__3_ = as__3_([&f](double x)->double {
            return UTL::__3_::trapz::qsimp(f, std::make_pair(double(0), x), 4).first;
        }, 0, M_PI, 1);
        XCTAssert(!pts__4_.empty() && std::distance(pts__3_.cbegin(), pts__3_.cend()) == std::distance(pts__4_.cbegin(), pts__4_.cend()));
        for (auto const& pt : pts__4_) {
            double const y = UTL::__3_::trapz::qsimp(f, std::make_pair(0., std::get<0>(pt)), 4).first;
            XCTAssert(std::abs(std::get<1>(pt) - y) < 1e-15);
        }
        auto it__3_ = pts__3_.cbegin();
        for (auto it__4_ = pts__4_.cbegin(); it__4_ != pts__4_.cend(); ++it__4_, ++it__3_) {
            XCTAssert(std::abs(std::get<1>(*it__4_) - std::get<1>(*it__3_)) < 1e-15);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testSplineInterpolation_Empty {
    constexpr long Order = 10;
    using SplineCoefficient = UTL::__4_::SplineCoefficient<float, Order>;
    using SplineInterpolator = SplineCoefficient::spline_interpolator_type;
    try {
        SplineCoefficient coef1{}, coef2(coef1), coef3(std::move(coef1)), coef4, coef5;
        coef4 = coef2;
        coef5 = std::move(coef2);
        XCTAssert(Order == coef1.spline_order() && !coef1.is_regular_grid());
        XCTAssert(!coef1 && !coef2 && !coef3 && !coef4 && !coef5);

        SplineInterpolator interp1{}, interp2{}, interp3{std::move(interp1)}, interp4, interp5;
        interp4 = interp2;
        interp5 = std::move(interp2);
        XCTAssert(Order == interp1.spline_order());
        XCTAssert(!interp1 && !interp2 && !interp3 && !interp4 && !interp5);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testSplineInterpolation_LinearSpline {
    using UTL::__4_::AdaptiveSampling1D;
    using LinearCoefficient = UTL::__4_::LinearSplineCoefficient<double>;
    using SplineInterpolator = LinearCoefficient::spline_interpolator_type;
    XCTAssert(SplineInterpolator::spline_order() == 1, @"");

    // single point exception test
    //
    try {
        std::vector<double> xs = {0}, ys{0};
        LinearCoefficient l(xs.begin(), xs.end(), ys.begin());
        XCTAssert(false, @"exception should have been thrown");
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }
    try {
        std::vector<std::pair<double, double>> pts(1);
        LinearCoefficient l(pts.begin(), pts.end());
        XCTAssert(false, @"exception should have been thrown");
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }

    // regular grid
    //
    try {
        double (*f)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        std::vector<double> xs, ys;
        std::vector<std::pair<double, double>> pts;
        for (double x = 0.1; x <= 3; x += .01) {
            double const y = f(x);
            xs.push_back(x);
            ys.push_back(y);
            pts.emplace_back(x, y);
        }
        LinearCoefficient const coef1{xs.begin(), xs.end(), ys.begin()};
        LinearCoefficient _coef2{pts.begin(), pts.end()};
        SplineInterpolator::spline_coefficient_type coef2;
        coef2.swap(_coef2);
        XCTAssert(!!coef1 && !!coef2);
        XCTAssert(coef1.is_regular_grid() && coef2.is_regular_grid() && coef1.delta()() == coef2.delta()());
        XCTAssert(coef1.table().size() == xs.size() && coef2.table().size() == xs.size());
        XCTAssert(coef1.min_abscissa() == xs.front() && coef2.min_abscissa() == xs.front());
        XCTAssert(coef1.max_abscissa() == xs.back() && coef2.max_abscissa() == xs.back());
        for (unsigned i = 0; i < xs.size() - 1; ++i) {
            XCTAssert(coef1.table()[i].first == xs[i] && coef2.table()[i].first == xs[i] && coef1.table()[i] == coef2.table()[i]);
        }
        XCTAssert(coef1.table().back().first == xs.back() && coef2.table().back().first == xs.back());
        XCTAssert(coef1.table().back().second[0] == coef2.table().back().second[0]);
        double const integral = F(coef1.max_abscissa()) - F(coef1.min_abscissa());
        XCTAssert(std::abs(coef1.integrate() - integral)/std::abs(integral) < 1e-4, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef1.integrate(), std::abs(coef1.integrate() - integral)/std::abs(integral));

        SplineInterpolator const &interp1 = coef1.interpolator();
        SplineInterpolator &&_interp2 = std::move(coef2).interpolator(), interp2;
        interp2 = _interp2;
        XCTAssert(!!coef1 && !coef2);
        XCTAssert(!!interp1 && !!interp2);
        XCTAssert(interp1.min_abscissa() == xs.front() && interp2.min_abscissa() == xs.front());
        XCTAssert(interp1.max_abscissa() == xs.back() && interp2.max_abscissa() == xs.back());
        XCTAssert(coef1.integrate() == interp1.integrate() && coef1.integrate() == interp2.integrate());
        for (unsigned i = 0; i < xs.size(); ++i) {
            double const x = xs[i];
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30 && y1 == y2, @"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < xs.size() - 1; ++i) {
            double const x = .5*(xs.at(i) + xs.at(i + 1));
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*5e-4 && y1 == y2, @"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp1(interp1.min_abscissa() - 1) && !interp2(interp2.max_abscissa() + 1));

        dispatch_apply(interp1.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp1, &interp2](unsigned long const i) {
            double const x = interp1.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30 && y1 == y2) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // irregular grid: adaptive sampling
    //
    try {
        double (*f)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        AdaptiveSampling1D<double> sampler; {
            sampler.set_max_recursion(20);
            sampler.set_accuracy_goal(10);
            sampler.set_initial_points(20);
            sampler.set_y_scale_absolute_tolerance(1e-15);
        }
        auto const &_pts = sampler.operator()(f, .1, 2, 1);
        std::vector<std::pair<double, double>> pts{_pts.begin(), _pts.end()};
        LinearCoefficient coef{pts.begin(), pts.end()};
        XCTAssert(!!coef && !coef.is_regular_grid() && !coef.delta());
        XCTAssert(coef.table().size() == pts.size());
        XCTAssert(coef.min_abscissa() == pts.begin()->first && coef.max_abscissa() == pts.rbegin()->first);
        for (unsigned i = 0; i < coef.table().size(); ++i) {
            XCTAssert(coef.table()[i].first == pts[i].first);
        }
        double const integral = F(coef.max_abscissa()) - F(coef.min_abscissa());
        XCTAssert(std::abs(coef.integrate() - integral)/std::abs(integral) < 1e-5, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef.integrate(), std::abs(coef.integrate() - integral)/std::abs(integral));

        SplineInterpolator const interp = coef.interpolator();
        XCTAssert(!!coef && !!interp);
        XCTAssert(interp.min_abscissa() == pts.begin()->first && interp.max_abscissa() == pts.rbegin()->first);
        XCTAssert(coef.integrate() == interp.integrate());
        for (auto const &pt : pts) {
            double const x = pt.first;
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < interp.spline_coefficient().table().size() - 1; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-3, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp(interp.min_abscissa() - 1) && !interp(interp.max_abscissa() + 1));

        dispatch_apply(interp.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp](unsigned long const i) {
            double const x = interp.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // irregular grid: random samples
    //
    try {
        double (*f)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        std::mt19937 rng;
        std::uniform_real_distribution<> dist{.1, 2.5};
        std::map<double, double> pts;
        constexpr long N = 1000;
        for (long i = 0; i <= N; ++i) {
            double const x = dist(rng);
            pts[x] = f(x);
        }
        LinearCoefficient coef{pts.begin(), pts.end()};
        XCTAssert(!!coef && !coef.is_regular_grid() && !coef.delta());
        XCTAssert(coef.table().size() == pts.size());
        XCTAssert(coef.min_abscissa() == pts.begin()->first && coef.max_abscissa() == pts.rbegin()->first);
        for (unsigned i = 0; i < coef.table().size(); ++i) {
            XCTAssert(pts.count(coef.table()[i].first));
        }
        double const integral = F(coef.max_abscissa()) - F(coef.min_abscissa());
        XCTAssert(std::abs(coef.integrate() - integral)/std::abs(integral) < 1e-5, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef.integrate(), std::abs(coef.integrate() - integral)/std::abs(integral));

        SplineInterpolator const interp = coef.interpolator();
        XCTAssert(!!coef && !!interp);
        XCTAssert(interp.min_abscissa() == pts.begin()->first && interp.max_abscissa() == pts.rbegin()->first);
        XCTAssert(coef.integrate() == interp.integrate());
        for (auto const &pt : pts) {
            double const x = pt.first;
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < interp.spline_coefficient().table().size() - 1; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*6e-4, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp(interp.min_abscissa() - 1) && !interp(interp.max_abscissa() + 1));

        dispatch_apply(interp.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp](unsigned long const i) {
            double const x = interp.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testSplineInterpolation_CubicSpline {
    using UTL::__4_::AdaptiveSampling1D;
    using CubicSplineCoefficient = UTL::__4_::CubicSplineCoefficient<double>;
    using SplineInterpolator = CubicSplineCoefficient::spline_interpolator_type;
    XCTAssert(SplineInterpolator::spline_order() == 3, @"");

    // single point exception test
    //
    try {
        std::vector<double> xs = {0, 1, 2}, ys{0, 0, 0};
        CubicSplineCoefficient l(xs.begin(), xs.end(), ys.begin());
        XCTAssert(false, @"exception should have been thrown");
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }
    try {
        std::vector<std::pair<double, double>> pts(3);
        CubicSplineCoefficient l(pts.begin(), pts.end());
        XCTAssert(false, @"exception should have been thrown");
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }

    // regular grid
    //
    try {
        double (*df)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*f)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return 1/M_2_SQRTPI*std::erf(x);
        };
        std::vector<double> xs, ys;
        std::vector<std::pair<double, double>> pts;
        for (double x = 0.1; x <= 3; x += .01) {
            double const y = f(x);
            xs.push_back(x);
            ys.push_back(y);
            pts.emplace_back(x, y);
        }
        CubicSplineCoefficient const coef1{xs.begin(), xs.end(), ys.begin()};
        CubicSplineCoefficient _coef2{pts.begin(), pts.end()};
        SplineInterpolator::spline_coefficient_type coef2;
        coef2.swap(_coef2);
        XCTAssert(!!coef1 && !!coef2);
        XCTAssert(coef1.is_regular_grid() && coef2.is_regular_grid() && coef1.delta()() == coef2.delta()());
        XCTAssert(coef1.table().size() == xs.size() && coef2.table().size() == xs.size());
        XCTAssert(coef1.min_abscissa() == xs.front() && coef2.min_abscissa() == xs.front());
        XCTAssert(coef1.max_abscissa() == xs.back() && coef2.max_abscissa() == xs.back());
        for (unsigned i = 0; i < xs.size() - 1; ++i) {
            XCTAssert(coef1.table()[i].first == xs[i] && coef2.table()[i].first == xs[i] && coef1.table()[i] == coef2.table()[i]);
        }
        XCTAssert(coef1.table().back().first == xs.back() && coef2.table().back().first == xs.back());
        XCTAssert(coef1.table().back().second[0] == coef2.table().back().second[0]);
        double const integral = F(coef1.max_abscissa()) - F(coef1.min_abscissa());
        XCTAssert(std::abs(coef1.integrate() - integral)/std::abs(integral) < 1e-7, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef1.integrate(), std::abs(coef1.integrate() - integral)/std::abs(integral));

        SplineInterpolator const &interp1 = coef1.interpolator();
        SplineInterpolator &&_interp2 = std::move(coef2).interpolator(), interp2;
        interp2 = _interp2;
        XCTAssert(!!coef1 && !coef2);
        XCTAssert(!!interp1 && !!interp2);
        XCTAssert(interp1.min_abscissa() == xs.front() && interp2.min_abscissa() == xs.front());
        XCTAssert(interp1.max_abscissa() == xs.back() && interp2.max_abscissa() == xs.back());
        XCTAssert(coef1.integrate() == interp1.integrate() && coef1.integrate() == interp2.integrate());
        for (unsigned i = 0; i < xs.size(); ++i) {
            double const x = xs[i];
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30 && y1 == y2, @"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < xs.size() - 1; ++i) {
            double const x = .5*(xs.at(i) + xs.at(i + 1));
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*2e-4 && y1 == y2, @"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp1(interp1.min_abscissa() - 1) && !interp2(interp2.max_abscissa() + 1));
        for (unsigned i = 5; i < xs.size() - 5; ++i) {
            double const x = xs[i];
            double const dy0 = df(x), dy1 = interp1.derivative(x)(), dy2 = interp2.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*1e-4 && dy1 == dy2, @"x = %f, dy0 = %f, dy1 = %f, dy2 = %f, err = %e", x, dy0, dy1, dy2, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        for (unsigned i = 5; i < xs.size() - 5; ++i) {
            double const x = .5*(xs.at(i) + xs.at(i + 1));
            double const dy0 = df(x), dy1 = interp1.derivative(x)(), dy2 = interp2.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*1e-4 && dy1 == dy2, @"x = %f, dy0 = %f, dy1 = %f, dy2 = %f, err = %e", x, dy0, dy1, dy2, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        XCTAssert(!interp1.derivative(interp1.min_abscissa() - 1) && !interp2.derivative(interp2.max_abscissa() + 1));

        dispatch_apply(interp1.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp1, &interp2](unsigned long const i) {
            double const x = interp1.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30 && y1 == y2) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // irregular grid: adaptive sampling
    //
    try {
        double (*df)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*f)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return 1/M_2_SQRTPI*std::erf(x);
        };
        AdaptiveSampling1D<double> sampler; {
            sampler.set_max_recursion(20);
            sampler.set_accuracy_goal(10);
            sampler.set_initial_points(20);
            sampler.set_y_scale_absolute_tolerance(1e-15);
        }
        auto const &_pts = sampler.operator()(f, .1, 2.5, 1);
        std::vector<std::pair<double, double>> pts{_pts.begin(), _pts.end()};
        CubicSplineCoefficient coef{pts.begin(), pts.end()};
        XCTAssert(!!coef && !coef.is_regular_grid() && !coef.delta());
        XCTAssert(coef.table().size() == pts.size());
        XCTAssert(coef.min_abscissa() == pts.begin()->first && coef.max_abscissa() == pts.rbegin()->first);
        for (unsigned i = 0; i < coef.table().size(); ++i) {
            XCTAssert(coef.table()[i].first == pts[i].first);
        }
        double const integral = F(coef.max_abscissa()) - F(coef.min_abscissa());
        XCTAssert(std::abs(coef.integrate() - integral)/std::abs(integral) < 1e-8, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef.integrate(), std::abs(coef.integrate() - integral)/std::abs(integral));

        SplineInterpolator const interp = coef.interpolator();
        XCTAssert(!!coef && !!interp);
        XCTAssert(interp.min_abscissa() == pts.begin()->first && interp.max_abscissa() == pts.rbegin()->first);
        XCTAssert(coef.integrate() == interp.integrate());
        for (auto const &pt : pts) {
            double const x = pt.first;
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < interp.spline_coefficient().table().size() - 1; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*3e-4, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp(interp.min_abscissa() - 1) && !interp(interp.max_abscissa() + 1));
        for (unsigned i = 5; i < interp.spline_coefficient().table().size() - 5; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = table.at(i).first;
            double const dy0 = df(x), dy1 = interp.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*1e-4, @"x = %f, dy0 = %f, dy1 = %f, err = %e", x, dy0, dy1, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        for (unsigned i = 5; i < interp.spline_coefficient().table().size() - 5; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const dy0 = df(x), dy1 = interp.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*1e-4, @"x = %f, dy0 = %f, dy1 = %f, err = %e", x, dy0, dy1, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        XCTAssert(!interp.derivative(interp.min_abscissa() - 1) && !interp.derivative(interp.max_abscissa() + 1));

        dispatch_apply(interp.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp](unsigned long const i) {
            double const x = interp.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // irregular grid: random samples
    //
    try {
        double (*df)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*f)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return 1/M_2_SQRTPI*std::erf(x);
        };
        std::mt19937 rng;
        std::uniform_real_distribution<> dist{.1, 3};
        std::map<double, double> pts;
        constexpr long N = 1000;
        for (long i = 0; i <= N; ++i) {
            double const x = dist(rng);
            pts[x] = f(x);
        }
        CubicSplineCoefficient coef{pts.begin(), pts.end()};
        XCTAssert(!!coef && !coef.is_regular_grid() && !coef.delta());
        XCTAssert(coef.table().size() == pts.size());
        XCTAssert(coef.min_abscissa() == pts.begin()->first && coef.max_abscissa() == pts.rbegin()->first);
        for (unsigned i = 0; i < coef.table().size(); ++i) {
            XCTAssert(pts.count(coef.table()[i].first));
        }
        double const integral = F(coef.max_abscissa()) - F(coef.min_abscissa());
        XCTAssert(std::abs(coef.integrate() - integral)/std::abs(integral) < 1e-9, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef.integrate(), std::abs(coef.integrate() - integral)/std::abs(integral));

        SplineInterpolator const interp = coef.interpolator();
        XCTAssert(!!coef && !!interp);
        XCTAssert(interp.min_abscissa() == pts.begin()->first && interp.max_abscissa() == pts.rbegin()->first);
        XCTAssert(coef.integrate() == interp.integrate());
        for (auto const &pt : pts) {
            double const x = pt.first;
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < interp.spline_coefficient().table().size() - 1; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*2e-6, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp(interp.min_abscissa() - 1) && !interp(interp.max_abscissa() + 1));
        for (unsigned i = 5; i < interp.spline_coefficient().table().size() - 5; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = table.at(i).first;
            double const dy0 = df(x), dy1 = interp.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*2e-6, @"x = %f, dy0 = %f, dy1 = %f, err = %e", x, dy0, dy1, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        for (unsigned i = 5; i < interp.spline_coefficient().table().size() - 5; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const dy0 = df(x), dy1 = interp.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*1e-6, @"x = %f, dy0 = %f, dy1 = %f, err = %e", x, dy0, dy1, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        XCTAssert(!interp.derivative(interp.min_abscissa() - 1) && !interp.derivative(interp.max_abscissa() + 1));

        dispatch_apply(interp.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp](unsigned long const i) {
            double const x = interp.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testSplineInterpolation_MonotoneCubic {
    using UTL::__4_::AdaptiveSampling1D;
    using MonotoneCubicCoefficient = UTL::__4_::MonotoneCubicCoefficient<double>;
    using SplineInterpolator = MonotoneCubicCoefficient::spline_interpolator_type;
    XCTAssert(SplineInterpolator::spline_order() == 3, @"");

    // single point exception test
    //
    try {
        std::vector<double> xs = {0, 1, 2}, ys{0, 0, 0};
        MonotoneCubicCoefficient l(xs.begin(), xs.end(), ys.begin());
        XCTAssert(false, @"exception should have been thrown");
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }
    try {
        std::vector<std::pair<double, double>> pts(3);
        MonotoneCubicCoefficient l(pts.begin(), pts.end());
        XCTAssert(false, @"exception should have been thrown");
    } catch (std::exception &e) {
        XCTAssert(true, @"%s", e.what());
    }

    // regular grid
    //
    try {
        double (*df)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*f)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return 1/M_2_SQRTPI*std::erf(x);
        };
        std::vector<double> xs, ys;
        std::vector<std::pair<double, double>> pts;
        for (double x = 0.1; x <= 3; x += .01) {
            double const y = f(x);
            xs.push_back(x);
            ys.push_back(y);
            pts.emplace_back(x, y);
        }
        MonotoneCubicCoefficient const coef1{xs.begin(), xs.end(), ys.begin()};
        MonotoneCubicCoefficient _coef2{pts.begin(), pts.end()};
        SplineInterpolator::spline_coefficient_type coef2;
        coef2.swap(_coef2);
        XCTAssert(!!coef1 && !!coef2);
        XCTAssert(coef1.is_regular_grid() && coef2.is_regular_grid() && coef1.delta()() == coef2.delta()());
        XCTAssert(coef1.table().size() == xs.size() && coef2.table().size() == xs.size());
        XCTAssert(coef1.min_abscissa() == xs.front() && coef2.min_abscissa() == xs.front());
        XCTAssert(coef1.max_abscissa() == xs.back() && coef2.max_abscissa() == xs.back());
        for (unsigned i = 0; i < xs.size() - 1; ++i) {
            XCTAssert(coef1.table()[i].first == xs[i] && coef2.table()[i].first == xs[i] && coef1.table()[i] == coef2.table()[i]);
        }
        XCTAssert(coef1.table().back().first == xs.back() && coef2.table().back().first == xs.back());
        XCTAssert(coef1.table().back().second[0] == coef2.table().back().second[0]);
        double const integral = F(coef1.max_abscissa()) - F(coef1.min_abscissa());
        XCTAssert(std::abs(coef1.integrate() - integral)/std::abs(integral) < 2e-7, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef1.integrate(), std::abs(coef1.integrate() - integral)/std::abs(integral));

        SplineInterpolator const &interp1 = coef1.interpolator();
        SplineInterpolator &&_interp2 = std::move(coef2).interpolator(), interp2;
        interp2 = _interp2;
        XCTAssert(!!coef1 && !coef2);
        XCTAssert(!!interp1 && !!interp2);
        XCTAssert(interp1.min_abscissa() == xs.front() && interp2.min_abscissa() == xs.front());
        XCTAssert(interp1.max_abscissa() == xs.back() && interp2.max_abscissa() == xs.back());
        XCTAssert(coef1.integrate() == interp1.integrate() && coef1.integrate() == interp2.integrate());
        for (unsigned i = 0; i < xs.size(); ++i) {
            double const x = xs[i];
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30 && y1 == y2, @"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < xs.size() - 1; ++i) {
            double const x = .5*(xs.at(i) + xs.at(i + 1));
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*3e-4 && y1 == y2, @"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp1(interp1.min_abscissa() - 1) && !interp2(interp2.max_abscissa() + 1));
        for (unsigned i = 5; i < xs.size() - 5; ++i) {
            double const x = xs[i];
            double const dy0 = df(x), dy1 = interp1.derivative(x)(), dy2 = interp2.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*2e-3 && dy1 == dy2, @"x = %f, dy0 = %f, dy1 = %f, dy2 = %f, err = %e", x, dy0, dy1, dy2, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        for (unsigned i = 5; i < xs.size() - 5; ++i) {
            double const x = .5*(xs.at(i) + xs.at(i + 1));
            double const dy0 = df(x), dy1 = interp1.derivative(x)(), dy2 = interp2.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*6e-4 && dy1 == dy2, @"x = %f, dy0 = %f, dy1 = %f, dy2 = %f, err = %e", x, dy0, dy1, dy2, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        XCTAssert(!interp1.derivative(interp1.min_abscissa() - 1) && !interp2.derivative(interp2.max_abscissa() + 1));

        dispatch_apply(interp1.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp1, &interp2](unsigned long const i) {
            double const x = interp1.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp1(x)(), y2 = interp2(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30 && y1 == y2) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, y2 = %f, err = %e", x, y0, y1, y2, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // irregular grid: adaptive sampling
    //
    try {
        double (*df)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*f)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return 1/M_2_SQRTPI*std::erf(x);
        };
        AdaptiveSampling1D<double> sampler; {
            sampler.set_max_recursion(20);
            sampler.set_accuracy_goal(10);
            sampler.set_initial_points(20);
            sampler.set_y_scale_absolute_tolerance(1e-15);
        }
        auto const &_pts = sampler.operator()(f, .1, 2.5, 1);
        std::vector<std::pair<double, double>> pts{_pts.begin(), _pts.end()};
        MonotoneCubicCoefficient coef{pts.begin(), pts.end()};
        XCTAssert(!!coef && !coef.is_regular_grid() && !coef.delta());
        XCTAssert(coef.table().size() == pts.size());
        XCTAssert(coef.min_abscissa() == pts.begin()->first && coef.max_abscissa() == pts.rbegin()->first);
        for (unsigned i = 0; i < coef.table().size(); ++i) {
            XCTAssert(coef.table()[i].first == pts[i].first);
        }
        double const integral = F(coef.max_abscissa()) - F(coef.min_abscissa());
        XCTAssert(std::abs(coef.integrate() - integral)/std::abs(integral) < 2e-8, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef.integrate(), std::abs(coef.integrate() - integral)/std::abs(integral));

        SplineInterpolator const interp = coef.interpolator();
        XCTAssert(!!coef && !!interp);
        XCTAssert(interp.min_abscissa() == pts.begin()->first && interp.max_abscissa() == pts.rbegin()->first);
        XCTAssert(coef.integrate() == interp.integrate());
        for (auto const &pt : pts) {
            double const x = pt.first;
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < interp.spline_coefficient().table().size() - 1; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*4e-4, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp(interp.min_abscissa() - 1) && !interp(interp.max_abscissa() + 1));
        for (unsigned i = 5; i < interp.spline_coefficient().table().size() - 5; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = table.at(i).first;
            double const dy0 = df(x), dy1 = interp.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*6e-3, @"x = %f, dy0 = %f, dy1 = %f, err = %e", x, dy0, dy1, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        for (unsigned i = 5; i < interp.spline_coefficient().table().size() - 5; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const dy0 = df(x), dy1 = interp.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*2e-3, @"x = %f, dy0 = %f, dy1 = %f, err = %e", x, dy0, dy1, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        XCTAssert(!interp.derivative(interp.min_abscissa() - 1) && !interp.derivative(interp.max_abscissa() + 1));

        dispatch_apply(interp.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp](unsigned long const i) {
            double const x = interp.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // irregular grid: random samples
    //
    try {
        double (*df)(double) = [](double x)->double {
            return -2*x*std::exp(-UTL::pow<2>(x));
        };
        double (*f)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x));
        };
        double (*F)(double) = [](double x)->double {
            return 1/M_2_SQRTPI*std::erf(x);
        };
        std::mt19937 rng;
        std::uniform_real_distribution<> dist{.1, 3};
        std::map<double, double> pts;
        constexpr long N = 1000;
        for (long i = 0; i <= N; ++i) {
            double const x = dist(rng);
            pts[x] = f(x);
        }
        MonotoneCubicCoefficient coef{pts.begin(), pts.end()};
        XCTAssert(!!coef && !coef.is_regular_grid() && !coef.delta());
        XCTAssert(coef.table().size() == pts.size());
        XCTAssert(coef.min_abscissa() == pts.begin()->first && coef.max_abscissa() == pts.rbegin()->first);
        for (unsigned i = 0; i < coef.table().size(); ++i) {
            XCTAssert(pts.count(coef.table()[i].first));
        }
        double const integral = F(coef.max_abscissa()) - F(coef.min_abscissa());
        XCTAssert(std::abs(coef.integrate() - integral)/std::abs(integral) < 4e-7, @"integral_exact = %f, integral_spline = %f, err = %e", integral, coef.integrate(), std::abs(coef.integrate() - integral)/std::abs(integral));

        SplineInterpolator const interp = coef.interpolator();
        XCTAssert(!!coef && !!interp);
        XCTAssert(interp.min_abscissa() == pts.begin()->first && interp.max_abscissa() == pts.rbegin()->first);
        XCTAssert(coef.integrate() == interp.integrate());
        for (auto const &pt : pts) {
            double const x = pt.first;
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*1e-30, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        for (unsigned i = 0; i < interp.spline_coefficient().table().size() - 1; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const y0 = f(x), y1 = interp(x)();
            XCTAssert(std::abs(y0 - y1) < std::abs(y0)*5e-4, @"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
        }
        XCTAssert(!interp(interp.min_abscissa() - 1) && !interp(interp.max_abscissa() + 1));
        for (unsigned i = 5; i < interp.spline_coefficient().table().size() - 5; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = table.at(i).first;
            double const dy0 = df(x), dy1 = interp.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*2e-2, @"x = %f, dy0 = %f, dy1 = %f, err = %e", x, dy0, dy1, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        for (unsigned i = 5; i < interp.spline_coefficient().table().size() - 5; ++i) {
            auto const &table = interp.spline_coefficient().table();
            double const x = .5*(table.at(i).first + table.at(i + 1).first);
            double const dy0 = df(x), dy1 = interp.derivative(x)();
            XCTAssert(std::abs(dy0 - dy1) < std::abs(dy0)*1e-2, @"x = %f, dy0 = %f, dy1 = %f, err = %e", x, dy0, dy1, std::abs(dy0 - dy1)/std::abs(dy0));
        }
        XCTAssert(!interp.derivative(interp.min_abscissa() - 1) && !interp.derivative(interp.max_abscissa() + 1));

        dispatch_apply(interp.spline_coefficient().table().size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), [f, &interp](unsigned long const i) {
            double const x = interp.spline_coefficient().table()[i].first;
            double const y0 = f(x), y1 = interp(x)();
            if (std::abs(y0 - y1) < std::abs(y0)*1e-30) {
            } else {
                NSLog(@"x = %f, y0 = %f, y1 = %f, err = %e", x, y0, y1, std::abs(y0 - y1)/std::abs(y0));
            }
        });
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testTrapzIntegrator {
    using UTL::__4_::TrapzIntegrator;
    using UTL::__4_::Vector;
    using UTL::__4_::SIMDComplex;

    std::ostringstream os;
    os.setf(os.fixed);
    os.precision(15);

    // scalar
    //
    try {
        constexpr double xd = 3;
        constexpr double xmin = -5 + xd, xmax = 5 + xd;
        double (*pdf)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x - xd)) * M_2_SQRTPI/2;
        };
        double (*cdf)(double) = [](double x)->double {
            return .5*(1 + std::erf(x - xd));
        };
        struct cvPDF {
            double (*pdf)(double);
            cvPDF(double (*pdf)(double)) { this->pdf = pdf; }
            cvPDF(cvPDF const&) = delete;
            cvPDF &operator=(cvPDF const&) = delete;
            double operator()(double x) const { return this->pdf(x); }
        };
        struct rvPDF {
            double (*pdf)(double);
            rvPDF(double (*pdf)(double)) { this->pdf = pdf; }
            rvPDF(rvPDF const&) = delete;
            rvPDF &operator=(rvPDF const&) = delete;
            double operator()(double x) { return this->pdf(x); }
        };

        TrapzIntegrator<double> integrator;
        constexpr long n = 100;
        for (long i = 0; i <= n; ++i) {
            double const x = i*(xmax - 0)/n + 0;
            double const y0 = cdf(x);
            cvPDF const cv_pdf{pdf};
            rvPDF rv_pdf{pdf};
            auto const y1 = integrator.qsimp(cv_pdf, xmin, x);
            auto const y2 = integrator.qtrap(std::move(rv_pdf), xmin, x);
            XCTAssert(y1.second <= integrator.max_recursion() && y2.second <= integrator.max_recursion() && y1.second <= y1.second);
            XCTAssert(std::abs(y0 - y1.first)/y0 < 1e-7 && std::abs(y0 - y2.first)/y0 < 1e-7, @"y0 = %f, y1 = %f, err1 = %e, y2 = %f, err2 = %e", y0, y1.first, std::abs(y0 - y1.first)/y0, y2.first, std::abs(y0 - y2.first)/y0);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // complex
    //
    try {
        using Cx = SIMDComplex<double>;
        constexpr double xmin = 0, xmax = 2*M_PI;
        Cx (*f)(double) = [](double x)->Cx {
            Cx const i{0, 1};
            return std::exp(i*x) + Cx{1};
        };
        Cx (*F)(double) = [](double x)->Cx {
            Cx const i{0, 1};
            return x + i - i*std::exp(i*x);
        };

        TrapzIntegrator<double> integrator;
        constexpr long n = 100;
        for (long i = 1; i <= n; ++i) {
            double const x = i*(xmax - xmin)/n + xmin;
            Cx const y0 = F(x);
            auto const y1 = integrator.qsimp(f, xmin, x, [](Cx z)->double { return std::abs(z); });
            auto const y2 = integrator.qtrap(f, xmin, x, [](Cx z)->double { return std::abs(z); });
            XCTAssert(y1.second <= integrator.max_recursion() && y2.second <= integrator.max_recursion() && y1.second <= y1.second);
            XCTAssert(std::abs(y0 - y1.first) < xmax*1e-7 && std::abs(y0 - y2.first) < xmax*1e-7,
                      @"y0 = %f %+fi, y1 = %f %+fi, err1 = %e, y2 = %f %+fi, err2 = %e",
                      y0.real(), y0.imag(), y1.first.real(), y1.first.imag(), std::abs(y0 - y1.first)/xmax, y2.first.real(), y2.first.imag(), std::abs(y0 - y2.first)/xmax);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Vector
    //
    try {
        using Vec = Vector<double, 2>;
        constexpr double xmin = 0, xmax = 10;
        Vec (*f)(double) = [](double x)->Vec {
            Vec v;
            v.x = -j1(x); // j0
            v.y = .5*(j0(x) - jn(2, x)); // j1
            return v;
        };
        Vec (*F)(double) = [](double x)->Vec {
            return {j0(x), j1(x)};
        };
        double (*norm)(Vec v) = [](Vec v)->double {
            return std::sqrt(UTL::reduce_plus(v*v));
        };

        TrapzIntegrator<double> integrator;
        constexpr long n = 100;
        for (long i = 1; i <= n; ++i) {
            double const x = i*(xmax - xmin)/n + xmin;
            Vec const y0 = F(x) - F(xmin);
            auto const y1 = integrator.qsimp(f, xmin, x, norm);
            auto const y2 = integrator.qtrap(f, xmin, x, norm);
            XCTAssert(y1.second <= integrator.max_recursion() && y2.second <= integrator.max_recursion() && y1.second <= y1.second);
            XCTAssert(norm(y0 - y1.first) < 1e-7 && norm(y0 - y2.first) < 1e-7,
                      @"y0 = {%f, %f}, y1 = {%f, %f}, err1 = %e, y2 = {%f, %f}, err2 = %e",
                      y0.x, y0.y, y1.first.x, y1.first.y, norm(y0 - y1.first), y2.first.x, y2.first.y, norm(y0 - y2.first));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Trapzd sum
    //
    try {
        std::vector<double> xs;
        std::vector<Vector<double, 2>> ys;
        constexpr long nx = 501;
        for (long i = 0; i <= nx; ++i) {
            double const x = double(i)/nx;
            xs.push_back(x);
            ys.push_back({x*x, x});
        }
        Vector<double, 2> const y{1./3., 1./2.};

        TrapzIntegrator<double> integrator;
        Vector<double, 2> const y1 = integrator.trapzd(xs.begin(), xs.end(), ys.begin());
        XCTAssert(std::abs(UTL::reduce_plus(y*y) - UTL::reduce_plus(y1*y1)) < 1e-6, @"y = {%f, %f}", y1[0], y1[1]);
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testGaussLegendreIntegrator {
    using UTL::__4_::GaussLegendreIntegrator;
    using UTL::__4_::Vector;
    using UTL::__4_::SIMDComplex;

    std::ostringstream os;
    os.setf(os.fixed);
    os.precision(15);

    // check abscissas and weights for N = 10; see Numerical Recipe, pp. 180.
    //
    try {
        constexpr long order = 10;
        constexpr std::array<double, order> xs{
            -0.9739065285171717, -0.8650633666889845, -0.6794095682990244, -0.4333953941292472, -0.1488743389816312,
            0.1488743389816312, 0.4333953941292472, 0.6794095682990244, 0.8650633666889845, 0.9739065285171717,
        };
        constexpr std::array<double, order> ws{
            0.0666713443086881, 0.1494513491505806, 0.2190863625159821, 0.2692667193099963, 0.2955242247147529,
            0.2955242247147529, 0.2692667193099963, 0.2190863625159821, 0.1494513491505806, 0.0666713443086881,
        };
        GaussLegendreIntegrator<double, order> integrator;
        for (unsigned i = 0; i < order; ++i) {
            double const x0 = xs.at(i), x1 = integrator.abscissas().at(i);
            double const w0 = ws.at(i), w1 = integrator.weights().at(i);
            XCTAssert(std::abs(x0 - x1) < std::abs(x0)*1e-7 && std::abs(w0 - w1) < std::abs(w0)*1e-7,
                      @"i = %d, x0 = %f, x1 = %f, xerr = %e, w0 = %f, w1 = %f, werr = %e",
                      i, x0, x1, std::abs(x0 - x1)/std::abs(x0), w0, w1, std::abs(w0 - w1)/std::abs(w0));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // scalar
    //
    try {
        constexpr double xd = 2;
        constexpr double xmin = -5 + xd, xmax = 5 + xd;
        double (*pdf)(double) = [](double x)->double {
            return std::exp(-UTL::pow<2>(x - xd)) * M_2_SQRTPI/2;
        };
        double (*cdf)(double) = [](double x)->double {
            return .5*(1 + std::erf(x - xd));
        };
        struct cvPDF {
            double (*pdf)(double);
            cvPDF(double (*pdf)(double)) { this->pdf = pdf; }
            cvPDF(cvPDF const&) = delete;
            cvPDF &operator=(cvPDF const&) = delete;
            double operator()(double x) const { return this->pdf(x); }
        };
        struct rvPDF {
            double (*pdf)(double);
            rvPDF(double (*pdf)(double)) { this->pdf = pdf; }
            rvPDF(rvPDF const&) = delete;
            rvPDF &operator=(rvPDF const&) = delete;
            double operator()(double x) { return this->pdf(x); }
        };

        GaussLegendreIntegrator<double, 30> integrator;
        constexpr long n = 100;
        for (long i = 0; i <= n; ++i) {
            double const x = i*(xmax - 0)/n + 0;
            double const y0 = cdf(x);
            cvPDF const cv_pdf{pdf};
            rvPDF rv_pdf{pdf};
            double const y1 = integrator(cv_pdf, xmin, x);
            double const d1 = integrator(std::move(rv_pdf), xmin, x, -y0);
            XCTAssert(std::abs(y0 - y1) < y0*1e-7 && std::abs(d1) < y0*1e-7, @"i = %ld, x = %f, y0 = %f, y1 = %f, err = %e, d1 = %e", i, x, y0, y1, std::abs(y0 - y1)/y0, d1);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // complex
    //
    try {
        using Cx = SIMDComplex<double>;
        constexpr double xmin = 0, xmax = 2*M_PI;
        Cx (*f)(double) = [](double x)->Cx {
            Cx const i{0, 1};
            return std::exp(i*x) + Cx{1};
        };
        Cx (*F)(double) = [](double x)->Cx {
            Cx const i{0, 1};
            return x + i - i*std::exp(i*x);
        };

        GaussLegendreIntegrator<double, 10> integrator;
        constexpr long n = 100;
        for (long i = 1; i <= n; ++i) {
            double const x = i*(xmax - xmin)/n + xmin;
            Cx const y0 = F(x);
            Cx const y1 = integrator(f, xmin, x);
            Cx const d1 = integrator(f, xmin, x, -F(x));
            XCTAssert(std::abs(y0 - y1) < xmax*1e-7 && std::abs(d1) < xmax*1e-7, @"i = %ld, x = %f, y0 = %f %+fi, y1 = %f %+fi, err = %e, d1 = %e", i, x, y0.real(), y0.imag(), y1.real(), y1.imag(), std::abs(y0 - y1)/xmax, std::abs(d1));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }

    // Vector
    //
    try {
        using Vec = Vector<double, 2>;
        constexpr double xmin = 0, xmax = 10;
        Vec (*f)(double) = [](double x)->Vec {
            Vec v;
            v.x = -j1(x); // j0
            v.y = .5*(j0(x) - jn(2, x)); // j1
            return v;
        };
        Vec (*F)(double) = [](double x)->Vec {
            return {j0(x), j1(x)};
        };
        double (*norm)(Vec v) = [](Vec v)->double {
            return std::sqrt(UTL::reduce_plus(v*v));
        };

        GaussLegendreIntegrator<double, 10> integrator;
        constexpr long n = 100;
        for (long i = 1; i <= n; ++i) {
            double const x = i*(xmax - xmin)/n + xmin;
            Vec const y0 = F(x) - F(xmin);
            Vec const y1 = integrator(f, xmin, x);
            Vec const d1 = integrator(f, xmin, x, -y0);
            XCTAssert(norm(y0 - y1) < 1e-7 && norm(d1) < 1e-7, @"i = %ld, x = %f, y0 = {%f, %f}, y1 = {%f, %f}, err = %e, d1 = %e", i, x, y0.x, y0.y, y1.x, y1.y, norm(y0 - y1), norm(d1));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"c++ exception: %s", e.what());
    }
}

- (void)testNewtonRootFinder {
    using UTL::__4_::NewtonRootFinder;

    try {
        @autoreleasepool {
            struct _fOdf {
                double (^fOdf)(double);
                _fOdf(double (^fOdf)(double)) { this->fOdf = fOdf; }
                _fOdf(_fOdf const &) = delete;
                _fOdf &operator=(_fOdf const &) = delete;
                double operator()(double x) const { return fOdf(x); }
            } const fOdf(^(double x) {
                //return double(INFINITY);
                return (x*x - 1.) / (2.*x);
            });

            NewtonRootFinder<double> root_finder;
            double const x_exact = 1;
            double const x0 = 1.1;
            auto const x_approx = root_finder(fOdf, x0, ^(unsigned i, double x) {
                printo(std::cout, "i = ", i, ", x = ", x, "\n");
                return true;
            });
            if (x_approx) {
                XCTAssert(std::abs(*x_approx - x_exact) < x_exact*root_finder.reltol, @"x_exact = %f, x_approx = %f, err = %e", x_exact, *x_approx, std::abs(*x_approx - x_exact)/x_exact);
            } else {
                XCTAssert(x_approx.has_value(), @"null optional");
            }
        }

        @autoreleasepool {
            struct _fOdf {
                double (^fOdf)(double);
                _fOdf(double (^fOdf)(double)) { this->fOdf = fOdf; }
                _fOdf(_fOdf const &) = delete;
                _fOdf &operator=(_fOdf const &) = delete;
                double operator()(double x) { return fOdf(x); }
            } fOdf(^(double x) {
                return -j0(x)/j1(x); // f = j0(x); df = -j1(x)
            });

            constexpr std::array<double, 3> j0_zeros{2.404825557695773, 5.520078110286311, 8.65372791291101};
            NewtonRootFinder<double> root_finder;
            for (double const &x_exact : j0_zeros) {
                double const x_try = x_exact - 1;
                auto const x_approx = root_finder(std::move(fOdf), x_try);
                if (x_approx) {
                    XCTAssert(std::abs(*x_approx - x_exact) < x_exact*root_finder.reltol, @"x_exact = %f, x_approx = %f, err = %e", x_exact, *x_approx, std::abs(*x_approx - x_exact)/x_exact);
                } else {
                    XCTAssert(x_approx.has_value(), @"null optional");
                }
            }
        }

        @autoreleasepool {
            double c = 0;
            auto const fOdf = [&c](double x)->double {
                double f = x*(x - c);
                double df = 2*x - c;
                return f/df;
            };

            NewtonRootFinder<double> root_finder; {
                root_finder.multiplicity = 1;
                root_finder.max_iterations = 10;
            }

            c = 1 + 1e-2;
            for (double x0 = c + .1; c >= 0.; c -= .1) {
                auto const x_approx = root_finder(fOdf, x0);
                if (x_approx) {
                    x0 = x_approx();
                    XCTAssert(std::abs(c - x0) < 1e-7, @"x_exact = %f, x_approx = %f, err = %e", c, x0, std::abs(c - x0));
                } else {
                    XCTAssert(x_approx.has_value(), @"c = %f, x0 = %f", c, x0);
                    break;
                }
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testMullerRootFinder {
    using UTL::__4_::MullerRootFinder;

    try {
        MullerRootFinder<double> root_finder;
        using Cx = decltype(root_finder)::complex_type;

        @autoreleasepool {
            __block double b = -1;
            struct F {
                Cx (^f)(Cx);
                F(Cx (^f)(Cx)) { this->f = f; }
                F(F const &) = delete;
                F &operator=(F const &) = delete;
                Cx operator()(Cx z) const { return f(z); }
            } const f(^(Cx z) {
                return (z - 1.)*(z - 1.) + b;
            });

            Cx x0 = 2.1, x1 = 2.07, x2 = 2.03;
            for (b = -1; b < 1; b += .1) {
                Cx const exact = 1. + std::sqrt(Cx{-b});
                auto const approx = root_finder(f, x0, x1, x2, ^(unsigned i, Cx z) {
                    printo(std::cout, "b = ", b, ", i = ", i, ", z = ", z, "\n");
                    return true;
                });
                if (approx) {
                    XCTAssert(std::abs(*approx - exact) < std::abs(exact)*root_finder.reltol, @"exact = %f %+fi, approx = %f %+fi, err = %e", exact.real(), exact.imag(), approx->real(), approx->imag(), std::abs(*approx - exact)/std::abs(exact));
                    x0 = x1; x1 = x2; x2 = *approx;
                } else {
                    XCTAssert(approx.has_value(), @"null optional");
                }
            }
        }

        @autoreleasepool {
            __block double y = .1;
            struct F {
                Cx (^f)(Cx);
                F(Cx (^f)(Cx)) { this->f = f; }
                F(F const &) = delete;
                F &operator=(F const &) = delete;
                Cx operator()(Cx z) { return f(z); }
            } f(^(Cx z) {
                return std::cos(z) - std::cosh(y);
            });

            Cx x0{.1, y}, x1{-.1, y}, x2{.2, y};
            for (; y < 2; y += .1) {
                Cx const exact{0, y};
                auto const approx = root_finder(f, x0, x1, x2, ^(unsigned i, Cx z) {
                    printo(std::cout, "y = ", y, ", i = ", i, ", z = ", z, "\n");
                    return true;
                });
                if (approx) {
                    XCTAssert(std::abs(*approx - exact) < std::abs(exact)*root_finder.reltol, @"exact = %f %+fi, approx = %f %+fi, err = %e", exact.real(), exact.imag(), approx->real(), approx->imag(), std::abs(*approx - exact)/std::abs(exact));
                    x0 = x1; x1 = x2; x2 = *approx;
                } else {
                    XCTAssert(approx.has_value(), @"null optional");
                }
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}


template <typename T>
static void _kaijun_smoothing(T *data, T *buffer, long Mx, long My);
template <typename T>
static void _kaijun_smoothing(T **_data, T **_buffer, long Mx, long My);

- (void)testHammingFilter_1D {
    using UTL::__4_::ArrayND;
    using UTL::__4_::HammingFilter;
    using UTL::__4_::SIMDVector;
    using usize_type = unsigned long;
    constexpr long ND = 1;

    std::stringstream os;

    // scalar
    try {
        ArrayND<double, ND, 1> km_i({5});
        ArrayND<double, ND, 0> km_o(km_i.dims());
        km_i[2] = 1.;
        km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

        double const total_0 = 1;

        std::unique_ptr<double[]> kl_i(new double[3*km_i.max_size()]);
        std::unique_ptr<double[]> kl_o(new double[3*km_i.max_size()]);
        std::fill_n(kl_i.get(), 3*km_i.max_size(), 0.0);
        kl_i[usize_type(0*km_i.max_size() + 2+km_i.pad_size())] = km_i[2];
        kl_i[usize_type(1*km_i.max_size() + 2+km_i.pad_size())] = km_i[2];
        kl_i[usize_type(2*km_i.max_size() + 2+km_i.pad_size())] = km_i[2];

        if ( (0) ) {
            std::cout << "km_i=" << km_i << std::endl;
            std::cout << "kl_i={";
            for (unsigned i = 0; i < 3; ++i) {
                for (unsigned j = 0; j < km_i.max_size(); ++j) {
                    std::cout << kl_i[usize_type(i*km_i.max_size() + j)] << ", ";
                }
            }
            std::cout << "}\n";
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            constexpr HammingFilter<ND> hf{};
            hf(km_i.begin(), km_i.end(), km_o.begin());
            std::copy(km_o.begin(), km_o.end(), km_i.begin());
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

            double const total_1 = std::accumulate(km_o.begin(), km_o.end(), 0.);
            XCTAssert(std::abs(total_0 - total_1) < total_0*1e-15, @"total_0 = %f, total_1 = %f, err = %e", total_0, total_1, std::abs(total_0 - total_1)/total_0);

            _kaijun_smoothing(kl_i.get(), kl_o.get(), 1, km_i.size());

            os.str(""); os
            << "i=" << i << std::endl
            << "km_i=" << km_i << std::endl;
            {
                std::cout << "kl_i={";
                for (unsigned i = 1, j = 0; j < km_i.max_size(); ++j) {
                    std::cout << kl_i[usize_type(i*km_i.max_size() + j)] << ", ";
                }
                std::cout << "}\n";
            }
            double const* beg = kl_o.get() + km_i.max_size() + km_i.pad_size();
            bool is_equal = std::equal(km_o.begin(), km_o.end(), beg);
            XCTAssert(is_equal, @"%s", os.str().c_str());
            //std::cout << os.str() << std::endl;
        }
        std::cout << os.str() << std::endl;
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // vector
    try {
        using Vector = SIMDVector<double, 2>;
        ArrayND<double, ND, 1> km_is({5});
        ArrayND<double, ND, 0> km_os(km_is.dims());
        ArrayND<Vector, ND, 1> km_iv({5});
        ArrayND<Vector, ND, 0> km_ov(km_iv.dims());
        km_is[2] = 1.;
        km_is[-1] = km_is.back(); *km_is.end() = km_is.front();
        km_iv[2].fill(1.);
        km_iv[-1] = km_iv.back(); *km_iv.end() = km_iv.front();

        if ( (0) ) {
            std::cout << "km_is = " << km_is << std::endl;
            std::cout << "km_iv = " << km_iv << std::endl;
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            constexpr HammingFilter<ND> hf{};
            hf(km_is.begin(), km_is.end(), km_os.begin());
            std::copy(km_os.begin(), km_os.end(), km_is.begin());
            km_is[-1] = km_is.back(); *km_is.end() = km_is.front();

            hf(km_iv.begin(), km_iv.end(), km_ov.begin());
            std::copy(km_ov.begin(), km_ov.end(), km_iv.begin());
            km_iv[-1] = km_iv.back(); *km_iv.end() = km_iv.front();

            os.str(""); os
            << "i = " << i << std::endl
            << "km_is = " << km_is << std::endl
            << "km_iv = " << km_iv << std::endl;
            bool is_equal = std::equal(km_os.begin(), km_os.end(), km_ov.begin(), [](double s, Vector v)->bool {
                return s == v.x && s == v.y;
            });
            XCTAssert(is_equal, @"%s", os.str().c_str());
            //std::cout << os.str() << std::endl;
        }
        std::cout << os.str() << std::endl;
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testHammingFilter_2D {
    using UTL::__4_::ArrayND;
    using UTL::__4_::HammingFilter;
    using UTL::__4_::SIMDVector;
    using usize_type = unsigned long;
    constexpr long ND = 2;

    std::stringstream os;

    // scalar
    try {
        ArrayND<double, ND, 1> km_i({3, 5});
        ArrayND<double, ND, 0> km_o(km_i.dims());
        km_i[1][2] = km_i[0][0] = 1.;
        for (auto &km_i : km_i) {
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
        }
        km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

        double const total_0 = 2;

        ArrayND<double, ND, 1> kl_i(km_i);

        if ( (0) ) {
            std::cout << "km_i=" << km_i << std::endl;
            std::cout << "kl_i=" << kl_i << std::endl;
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            constexpr HammingFilter<ND> hf{};
            hf(km_i.begin(), km_i.end(), km_o.begin());
            for (long i = 0; i < km_i.size<0>(); ++i) {
                std::copy(km_o[i].begin(), km_o[i].end(), km_i[i].begin());
            }
            for (auto& km_i : km_i) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

            double const total_1 = std::accumulate(km_o.flat_array().begin(), km_o.flat_array().end(), 0.);
            XCTAssert(std::abs(total_0 - total_1) < total_0*1e-15, @"total_0 = %f, total_1 = %f, err = %e", total_0, total_1, std::abs(total_0 - total_1)/total_0);

            std::unique_ptr<double[]> ip(new double[kl_i.max_size() * kl_i[0].max_size()]);
            std::unique_ptr<double[]> op(new double[kl_i.max_size() * kl_i[0].max_size()]);
            for (long n = 0, i = -kl_i.pad_size(); i < kl_i.size() + kl_i.pad_size(); ++i) {
                for (long j = -kl_i[i].pad_size(); j < kl_i[i].size() + kl_i[i].pad_size(); ++j, ++n) {
                    ip[usize_type(n)] = kl_i[i][j];
                }
            }
            _kaijun_smoothing(ip.get(), op.get(), kl_i.size(), kl_i[0].size());
            for (long n = 0, i = -kl_i.pad_size(); i < kl_i.size() + kl_i.pad_size(); ++i) {
                for (long j = -kl_i[i].pad_size(); j < kl_i[i].size() + kl_i[i].pad_size(); ++j, ++n) {
                    kl_i[i][j] = ip[usize_type(n)];
                }
            }

            os.str(""); os
            << "i=" << i << std::endl
            << "km_i=" << km_i << std::endl
            << "kl_i=" << kl_i << std::endl;
            //std::cout << os.str() << std::endl;
            for (unsigned i = 0; i < km_i.size<0>(); ++i) {
                bool tf = std::equal(km_i[i].begin(), km_i[i].end(), kl_i[i].begin());
                XCTAssert(tf, @"%s", os.str().c_str());
                if (!tf) {
                    break;
                }
            }
        }
        std::cout << os.str() << std::endl;
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // vector
    try {
        using Vector = SIMDVector<double, 2>;
        ArrayND<double, ND, 1> km_is({3, 5});
        ArrayND<double, ND, 0> km_os(km_is.dims());
        km_is[1][2] = km_is[0][0] = 1.;
        for (auto &km_i : km_is) {
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
        }
        km_is[-1] = km_is.back(); *km_is.end() = km_is.front();

        ArrayND<Vector, ND, 1> km_iv({3, 5});
        ArrayND<Vector, ND, 0> km_ov(km_iv.dims());
        km_iv[1][2].fill(1.);
        km_iv[0][0].fill(1.);
        for (auto &km_i : km_iv) {
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
        }
        km_iv[-1] = km_iv.back(); *km_iv.end() = km_iv.front();

        if ( (0) ) {
            std::cout << "km_is = " << km_is << std::endl;
            std::cout << "km_iv = " << km_iv << std::endl;
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            constexpr HammingFilter<ND> hf{};
            hf(km_is.begin(), km_is.end(), km_os.begin());
            for (long i = 0; i < km_is.size<0>(); ++i) {
                std::copy(km_os[i].begin(), km_os[i].end(), km_is[i].begin());
            }
            for (auto& km_i : km_is) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_is[-1] = km_is.back(); *km_is.end() = km_is.front();

            hf(km_iv.begin(), km_iv.end(), km_ov.begin());
            for (long i = 0; i < km_iv.size<0>(); ++i) {
                std::copy(km_ov[i].begin(), km_ov[i].end(), km_iv[i].begin());
            }
            for (auto& km_i : km_iv) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_iv[-1] = km_iv.back(); *km_iv.end() = km_iv.front();

            os.str(""); os
            << "i = " << i << std::endl
            << "km_is = " << km_is << std::endl
            << "km_iv = " << km_iv << std::endl;
            for (unsigned i = 0; i < km_is.size<0>(); ++i) {
                bool tf = std::equal(km_is[i].begin(), km_is[i].end(), km_iv[i].begin(), [](double s, Vector v)->bool {
                    return s == v.x && s == v.y;
                });
                XCTAssert(tf, @"%s", os.str().c_str());
                if (!tf) {
                    break;
                }
            }
        }
        std::cout << os.str() << std::endl;
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}
- (void)testHammingFilter_3D {
    using UTL::__4_::ArrayND;
    using UTL::__4_::HammingFilter;
    using UTL::__4_::SIMDVector;
    using usize_type = unsigned long;
    constexpr long ND = 3;

    std::stringstream os;

    // scalar
    try {
        ArrayND<double, ND, 1> km_i({1, 3, 5});
        ArrayND<double, ND, 0> km_o(km_i.dims());
        km_i[0][2][2] = km_i[0][0][0] = 1.;
        for (auto& km_i : km_i) {
            for (auto& km_i : km_i) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
        }
        km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

        double const total_0 = 2;

        ArrayND<double, ND - 1, 1> kl_i(km_i[0]);

        if ( (0) ) {
            std::cout << "km_i=" << km_i[0] << std::endl;
            std::cout << "kl_i=" << kl_i << std::endl;
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            constexpr HammingFilter<3> hf{};
            hf(km_i.begin(), km_i.end(), km_o.begin());
            for (long i = 0; i < km_i.size<0>(); ++i) {
                for (long j = 0; j < km_i.size<1>(); ++j) {
                    std::copy(km_o[i][j].begin(), km_o[i][j].end(), km_i[i][j].begin());
                }
            }
            for (auto& km_i : km_i) {
                for (auto& km_i : km_i) {
                    km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
                }
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();

            double const total_1 = std::accumulate(km_o.flat_array().begin(), km_o.flat_array().end(), 0.);
            XCTAssert(std::abs(total_0 - total_1) < total_0*1e-15, @"total_0 = %f, total_1 = %f, err = %e", total_0, total_1, std::abs(total_0 - total_1)/total_0);

            std::unique_ptr<double[]> ip(new double[kl_i.max_size() * kl_i[0].max_size()]);
            std::unique_ptr<double[]> op(new double[kl_i.max_size() * kl_i[0].max_size()]);
            for (long n = 0, i = -kl_i.pad_size(); i < kl_i.size() + kl_i.pad_size(); ++i) {
                for (long j = -kl_i[i].pad_size(); j < kl_i[i].size() + kl_i[i].pad_size(); ++j, ++n) {
                    ip[usize_type(n)] = kl_i[i][j];
                }
            }
            _kaijun_smoothing(ip.get(), op.get(), kl_i.size(), kl_i[0].size());
            for (long n = 0, i = -kl_i.pad_size(); i < kl_i.size() + kl_i.pad_size(); ++i) {
                for (long j = -kl_i[i].pad_size(); j < kl_i[i].size() + kl_i[i].pad_size(); ++j, ++n) {
                    kl_i[i][j] = ip[usize_type(n)];
                }
            }

            os.str(""); os
            << "i=" << i << std::endl
            << "km_i=" << km_i[0] << std::endl
            << "kl_i=" << kl_i << std::endl;
            //std::cout << os.str() << std::endl;
            for (unsigned i = 0; i < km_i.size<1>(); ++i) {
                bool tf = std::equal(km_i[0][i].begin(), km_i[0][i].end(), kl_i[i].begin());
                XCTAssert(tf, @"%s", os.str().c_str());
                if (!tf) {
                    break;
                }
            }
        }
        std::cout << os.str() << std::endl;
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // vector
    try {
        using Vector = SIMDVector<double, 2>;
        ArrayND<double, ND, 1> km_is({4, 3, 5});
        ArrayND<double, ND, 0> km_os(km_is.dims());
        km_is[2][1][2] = km_is[0][0][0] = 1.;
        for (auto &km_i : km_is) {
            for (auto& km_i : km_i) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
        }
        km_is[-1] = km_is.back(); *km_is.end() = km_is.front();

        ArrayND<Vector, ND, 1> km_iv({4, 3, 5});
        ArrayND<Vector, ND, 0> km_ov(km_iv.dims());
        km_iv[2][1][2].fill(1.);
        km_iv[0][0][0].fill(1.);
        for (auto &km_i : km_iv) {
            for (auto& km_i : km_i) {
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
        }
        km_iv[-1] = km_iv.back(); *km_iv.end() = km_iv.front();

        if ( (0) ) {
            std::cout << "km_is = " << km_is << std::endl;
            std::cout << "km_iv = " << km_iv << std::endl;
            return;
        }

        // filtration
        for (int i = 0; i < 4; ++i) {
            constexpr HammingFilter<ND> hf{};
            hf(km_is.begin(), km_is.end(), km_os.begin());
            for (long i = 0; i < km_is.size<0>(); ++i) {
                for (long j = 0; j < km_is.size<1>(); ++j) {
                    std::copy(km_os[i][j].begin(), km_os[i][j].end(), km_is[i][j].begin());
                }
            }
            for (auto& km_i : km_is) {
                for (auto& km_i : km_i) {
                    km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
                }
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_is[-1] = km_is.back(); *km_is.end() = km_is.front();

            hf(km_iv.begin(), km_iv.end(), km_ov.begin());
            for (long i = 0; i < km_iv.size<0>(); ++i) {
                for (long j = 0; j < km_iv.size<1>(); ++j) {
                    std::copy(km_ov[i][j].begin(), km_ov[i][j].end(), km_iv[i][j].begin());
                }
            }
            for (auto& km_i : km_iv) {
                for (auto& km_i : km_i) {
                    km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
                }
                km_i[-1] = km_i.back(); *km_i.end() = km_i.front();
            }
            km_iv[-1] = km_iv.back(); *km_iv.end() = km_iv.front();

            os.str(""); os
            << "i = " << i << std::endl
            << "km_is = " << km_is << std::endl
            << "km_iv = " << km_iv << std::endl;
            for (unsigned i = 0; i < km_is.size<0>(); ++i) {
                for (unsigned j = 0; j < km_is.size<1>(); ++j) {
                    bool tf = std::equal(km_is[i][j].begin(), km_is[i][j].end(), km_iv[i][j].begin(), [](double s, Vector v)->bool {
                        return s == v.x && s == v.y;
                    });
                    XCTAssert(tf, @"%s", os.str().c_str());
                    if (!tf) {
                        break;
                    }
                }
            }
        }
        std::cout << os.str() << std::endl;
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testShapeFunction {
    using T = double;
    // order 1
    //
    try {
        using Shape = UTL::Shape<T, 1>;
        Shape sh;
        for (Shape::value_type x = 0.; x < 3.; x += .1) {
            sh = x;
            Shape::index_type const i = long(std::floor(x));
            XCTAssert(i == sh.i<0>() && i + 1 == sh.i<1>(), @"x = %f, i(x) = %ld, sh.i[0] = %ld, sh.i[1] = %ld", x, i, sh.i<0>(), sh.i<1>());
            XCTAssert(std::abs((sh.w<0>() + sh.w<1>()) - 1.) < 1e-10, @"w[0]=%f, w[1]=%f", sh.w<0>(), sh.w<1>());
            XCTAssert(std::abs((x - i) - sh.w<1>()) < 1e-10, @"w[0]=%f, w[1]=%f", sh.w<0>(), sh.w<1>());
            //printo(std::cout, "x = ", x, ", i[0] = ", sh.i<0>(), ", w[0] = ", sh.w<0>(), ", w[1] = ", sh.w<1>(), "\n");
        }
    } catch(std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // order 2
    //
    try {
        using Shape = UTL::Shape<T, 2>;
        Shape sh;
        for (Shape::value_type x = 10.; x < 11.; x += .1) {
            sh = x;
            Shape::index_type const i = long(std::round(x));
            XCTAssert(i - 1 == sh.i<0>() && i + 0 == sh.i<1>() && i + 1 == sh.i<2>(), @"x = %f, i(round(x)) = %ld, sh.i[0] = %ld, sh.i[1] = %ld, sh.i[2] = %ld", x, i, sh.i<0>(), sh.i<1>(), sh.i<2>());
            XCTAssert(std::abs((sh.w<0>() + sh.w<1>() + sh.w<2>()) - 1.) < 1e-10, @"w[0]=%f, w[1]=%f, w[2]=%f", sh.w<0>(), sh.w<1>(), sh.w<2>());
            printo(std::cout, "x = ", x, ", i[1] = ", sh.i<1>(), ", w[0] = ", sh.w<0>(), ", w[1] = ", sh.w<1>(), ", w[2] = ", sh.w<2>(), "\n");
        }
    } catch(std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // order 3
    //
    try {
        using Shape = UTL::Shape<double, 3>;
        Shape sh;
        for (Shape::value_type x = 10.; x < 11.; x += .1) {
            sh = x;
            Shape::index_type const i = long(std::ceil(x));
            XCTAssert(i - 2 == sh.i<0>() && i - 1 == sh.i<1>() && i + 0 == sh.i<2>() && i + 1 == sh.i<3>(), @"x = %f, i(ceil(x)) = %ld, sh.i[0] = %ld, sh.i[1] = %ld, sh.i[2] = %ld, sh.i[3] = %ld", x, i, sh.i<0>(), sh.i<1>(), sh.i<2>(), sh.i<3>());
            XCTAssert(std::abs((sh.w<0>() + sh.w<1>() + sh.w<2>() + sh.w<3>()) - 1.) < 1e-10, @"w[0]=%f, w[1]=%f, w[2]=%f, w[3]=%f", sh.w<0>(), sh.w<1>(), sh.w<2>(), sh.w<3>());
            printo(std::cout, "x = ", x, ", i[1] = ", sh.i<1>(), ", w[0] = ", sh.w<0>(), ", w[1] = ", sh.w<1>(), ", w[2] = ", sh.w<2>(), ", w[3] = ", sh.w<3>(), "\n");
        }
    } catch(std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end


template <typename T>
void _kaijun_smoothing(T *data, T *buffer, long Mx, long My) {
    long My2 = My + 2;
    long i,j,ix0,ix1,iy0,iy1;
    for (i=1; i<=Mx; i++) { // Filtration
        ix0=i-1;
        ix1=i+1;
        for (j=1; j<=My; j++) {
            iy0=j-1;
            iy1=j+1;
            *(buffer+i*My2+j)=(4.*(*(data+i*My2+j))+2.*(*(data+ix0*My2+j)+*(data+ix1*My2+j)+*(data+i*My2+iy0)+*(data+i*My2+iy1))
                               +(*(data+ix0*My2+iy0)+*(data+ix0*My2+iy1)+*(data+ix1*My2+iy0)+*(data+ix1*My2+iy1)))/16.;
        }
    }
    for (i=1; i<=Mx; i++) { // Copy
        for (j=1; j<=My; j++) {
            *(data+i*My2+j) = *(buffer+i*My2+j);
        }
        *(data+i*My2+My+1) = *(data+i*My2+ 1);
        *(data+i*My2+   0) = *(data+i*My2+My);
    }
    for (j=0; j<My2; j++) {
        *(data+(0   )*My2+j) = *(data+Mx*My2+j);
        *(data+(Mx+1)*My2+j) = *(data+ 1*My2+j);
    }
}
template <typename T>
void _kaijun_smoothing(T **_data, T **_buffer, long Mx, long My) {
    T *data = *_data, *buffer = *_buffer;
    long My2 = My + 2;
    long i,j,ix0,ix1,iy0,iy1;
    for (i=1; i<=Mx; i++) { // Filtration
        ix0=i-1;
        ix1=i+1;
        for (j=1; j<=My; j++) {
            iy0=j-1;
            iy1=j+1;
            *(buffer+i*My2+j)=(4.*(*(data+i*My2+j))+2.*(*(data+ix0*My2+j)+*(data+ix1*My2+j)+*(data+i*My2+iy0)+*(data+i*My2+iy1))
                               +(*(data+ix0*My2+iy0)+*(data+ix0*My2+iy1)+*(data+ix1*My2+iy0)+*(data+ix1*My2+iy1)))/16.;
        }
    }
    { // Swap storage
        *_data = buffer, *_buffer = data;
        data = *_data, buffer = *_buffer;
    }
    // Fill ghost cells
    for (i=1; i<=Mx; i++) {
        *(data+i*My2+My+1) = *(data+i*My2+ 1);
        *(data+i*My2+   0) = *(data+i*My2+My);
    }
    for (j=0; j<My2; j++) {
        *(data+(0   )*My2+j) = *(data+Mx*My2+j);
        *(data+(Mx+1)*My2+j) = *(data+ 1*My2+j);
    }
}
