//
//  UTLHammingFilter.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/22/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLHammingFilter_hh
#define UTLHammingFilter_hh

// MARK:- Version 4
//

// MARK: HammingFilter<1D>
//
template <class In, class Out>
void UTL::__4_::HammingFilter<1>::_filtrate(In i, In e, Out o) const noexcept
{
    const typename std::iterator_traits<In>::value_type quad(0.25);
    for (; i != e; ++i) {
        *o++ = (*(i-1) + (*i + *i) + *(i+1) ) * quad;
    }
}

// MARK: HammingFilter<2D>
//
template <class In, class Out>
void UTL::__4_::HammingFilter<2>::_filtrate_dim1(In i, In e, Out o) const noexcept
{
    [[maybe_unused]] typedef typename std::iterator_traits<In >::value_type::const_iterator _I;
    [[maybe_unused]] typedef typename std::iterator_traits<Out>::value_type::iterator _O;
    for (; i != e; ++i) {
        _filtrate_dim2((i  )->begin(), (i  )->  end(),
                       (i-1)->begin(), (i+1)->begin(),
                       (o++)->begin());
    }
}
template <class In, class Out>
void UTL::__4_::HammingFilter<2>::_filtrate_dim2(In _1, In e1, In _0, In _2, Out o) const noexcept
{
    const typename std::iterator_traits<In>::value_type _1_16(1/16.), two(2.), four(4.);
    for (; _1!=e1; ++_1, ++_0, ++_2) {
        *o++ = ((*_1                                  )*four +
                (*(_1-1) + *(_1+1) + *_0     + *_2    )*two  +
                (*(_0-1) + *(_0+1) + *(_2-1) + *(_2+1))
                ) * _1_16;
    }
}

// MARK: HammingFilter<3D>
//
template <class In, class Out>
void UTL::__4_::HammingFilter<3>::_filtrate_dim1(In i, In e, Out o) const noexcept
{
    for (; i != e; ++i) {
        _filtrate_dim2((i  )->begin(), (i  )->  end(),
                       (i-1)->begin(), (i+1)->begin(),
                       (o++)->begin());
    }
}
template <class In, class Out>
void UTL::__4_::HammingFilter<3>::_filtrate_dim2(In _1, In e1, In _0, In _2, Out _o) const noexcept
{
    [[maybe_unused]] typedef typename std::iterator_traits<In >::value_type::const_iterator _I;
    [[maybe_unused]] typedef typename std::iterator_traits<Out>::value_type::iterator _O;
    for (; _1 != e1; ++_1, ++_0, ++_2) {
        _filtrate_dim3((_1  )->begin(), (_1  )->  end(),
                       (_1-1)->begin(), (_1+1)->begin(),
                       (_0-1)->begin(), (_2-1)->begin(),
                       (_0  )->begin(), (_2  )->begin(),
                       (_0+1)->begin(), (_2+1)->begin(),
                       (_o++)->begin());
    }
}
template <class In, class Out>
void UTL::__4_::HammingFilter<3>::_filtrate_dim3(In _11, In e11,
                                                 In _10, In _12,
                                                 In _00, In _20,
                                                 In _01, In _21,
                                                 In _02, In _22,
                                                 Out _oo) const noexcept
{
    const typename std::iterator_traits<In>::value_type _1_64(1/64.), two(2.), four(4.), eight(8.);
    for (; _11 != e11; ++_11, ++_10, ++_12, ++_00, ++_20, ++_01, ++_21, ++_02, ++_22) {
        *_oo++ = ((*_11)*eight +
                  (*(_11-1) + *(_11+1) + *_10 + *_12 + *_01 + *_21) * four +
                  (*(_10-1) + *(_10+1) + *(_12-1) + *(_12+1) + *(_01-1) + *(_01+1) + *(_21-1) + *(_21+1) + *_00 + *_02 + *_20 + *_22) * two +
                  (*(_00-1) + *(_00+1) + *(_02-1) + *(_02+1) + *(_20-1) + *(_20+1) + *(_22-1) + *(_22+1))
                  ) * _1_64;
    }
}

#endif /* UTLHammingFilter_hh */
