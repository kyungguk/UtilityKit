//
//  UTLShellDistribution.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLShellDistribution.h"

// MARK: Version 3
//
#include <utility>
#include <cmath>
#include <stdexcept>

#pragma mark class implementation
UTL::__3_::ShellDistribution::ShellDistribution(value_type vth, value_type vs, bool should_normalize_cdf)
: Distribution(), _icdf(), _vth(vth), _vs(vs) {
    if (_vth <= 0/* || _vs < 0*/) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }

    // sampler:
    sampler_type sampler = this->_sampler();

    // sample cdf:
    value_type const xs = _vs / _vth, x_max = 5;
    sampler_type::point_list_type
    cdf_pts = sampler([this](value_type const vOvth)->value_type {
        return this->_helper_cdf(vOvth) - this->_helper_cdf(0);
    }, xs > x_max ? xs-x_max : 0, x_max + xs, 1);

    // optional normalization:
    if (should_normalize_cdf) {
        // normalize cdf:
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        sampler_type::point_list_type::iterator
        first = cdf_pts.begin(), last = cdf_pts.end();
        for (; first != last; ++first) {
            first->second = (first->second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    sampler_type::point_list_type::iterator
    first = cdf_pts.begin(), last = cdf_pts.end();
    for (; first != last; ++first) {
        std::swap(first->second, first->first);
    }
    _icdf = interp_type(cdf_pts.begin(), cdf_pts.end());
}

UTL::__3_::ShellDistribution::ShellDistribution(ShellDistribution const &o) noexcept
: Distribution(), _icdf(o._icdf), _vth(o._vth), _vs(o._vs) {
}
auto UTL::__3_::ShellDistribution::operator=(ShellDistribution const &o) noexcept
-> ShellDistribution &{
    if (this != &o) {
        _icdf = o._icdf;
        _vth = o._vth;
        _vs = o._vs;
    }
    return *this;
}
UTL::__3_::ShellDistribution::ShellDistribution(ShellDistribution &&o) noexcept
: Distribution(), _icdf(std::move(o._icdf)), _vth(std::move(o._vth)), _vs(std::move(o._vs)) {
}
auto UTL::__3_::ShellDistribution::operator=(ShellDistribution &&o) noexcept
-> ShellDistribution &{
    if (this != &o) {
        _icdf = std::move(o._icdf);
        _vth = std::move(o._vth);
        _vs = std::move(o._vs);
    }
    return *this;
}

auto UTL::__3_::ShellDistribution::A() const noexcept
-> value_type {
    value_type const xs = _vs / _vth;
    return M_2_SQRTPI*xs*std::exp(-xs*xs) + (2*xs*xs + 1)*std::erfc(-xs);
}

auto UTL::__3_::ShellDistribution::mean() const noexcept
-> value_type {
    value_type const xs = _vs / _vth;
    return (M_2_SQRTPI*(xs*xs + 1)*std::exp(-xs*xs) + xs*(2*xs*xs + 3)*std::erfc(-xs)) * _vth/this->A();
}
auto UTL::__3_::ShellDistribution::variance() const noexcept
-> value_type {
    value_type const xs = _vs / _vth;
    value_type var = (M_2_SQRTPI*xs*(xs*xs + 2.5)*std::exp(-xs*xs) + (2*xs*xs*xs*xs + 6*xs*xs + 1.5)*std::erfc(-xs)) * _vth*_vth/this->A();
    return var -= this->mean()*this->mean();
}

auto UTL::__3_::ShellDistribution::pdf(value_type v) const
-> value_type {
    value_type const x = v / _vth;
    return v*v*this->_helper_pdf(x) / (_vth*_vth*_vth);
}
auto UTL::__3_::ShellDistribution::cdf(value_type v) const
-> value_type {
    value_type const x = v / _vth;
    return this->_helper_cdf(x) - this->_helper_cdf(0);
}
auto UTL::__3_::ShellDistribution::icdf(value_type cdf) const
-> value_type {
    return _icdf(cdf)*_vth;
}

auto UTL::__3_::ShellDistribution::_helper_pdf(value_type x) const noexcept
-> value_type {
    // note that x is not multiplied!
    x -= _vs / _vth;
    return std::exp(-x*x) * 2*M_2_SQRTPI/this->A();
}
auto UTL::__3_::ShellDistribution::_helper_cdf(value_type x) const noexcept
-> value_type {
    // note that this is indefinite integral of pdf!
    value_type const xs = _vs / _vth;
    x -= xs;
    return -(M_2_SQRTPI*(x + 2*xs)*std::exp(-x*x) - (2*xs*xs + 1)*std::erf(x)) / this->A();
}

auto UTL::__3_::ShellDistribution::_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.maxRecursion = 20;
        sampler.accuracyGoal = 7;
        sampler.initialPoints = 100;
        sampler.yScaleAbsoluteTolerance = 1e-10;
    }
    return sampler;
}


// MARK:- Version 4
//
#include <UtilityKit/AuxiliaryKit.h>
#include <functional>
#include <stdexcept>
#include <utility>
#include <string>
#include <cmath>

UTL::__4_::ShellDistribution::ShellDistribution(ShellDistribution const &o)
: Distribution(), _vth(o._vth), _vs(o._vs), _icdf(o._icdf) {
}
UTL::__4_::ShellDistribution::ShellDistribution(ShellDistribution &&o)
: Distribution(), _vth(o._vth), _vs(o._vs), _icdf() {
    _icdf = std::move(o._icdf);
}
auto UTL::__4_::ShellDistribution::operator=(ShellDistribution const &o)
-> ShellDistribution &{
    if (this != &o) {
        ShellDistribution{o}.swap(*this);
    }
    return *this;
}
auto UTL::__4_::ShellDistribution::operator=(ShellDistribution &&o)
-> ShellDistribution &{
    if (this != &o) {
        ShellDistribution{std::move(o)}.swap(*this);
    }
    return *this;
}

void UTL::__4_::ShellDistribution::swap(ShellDistribution &o)
{
    _icdf.swap(o._icdf);
    std::swap(_vth, o._vth);
    std::swap(_vs, o._vs);
}

auto UTL::__4_::ShellDistribution::mean() const noexcept
-> value_type {
    value_type const b = _vs/_vth, b2 = b*b;
    return (M_2_SQRTPI*(b2 + 1)*std::exp(-b2) + b*(2*b2 + 3)*std::erfc(-b))/A(b) * _vth;
}
auto UTL::__4_::ShellDistribution::variance() const noexcept
-> value_type {
    value_type const b = _vs/_vth, b2 = b*b;
    value_type var = (M_2_SQRTPI*b*(b2 + 2.5)*std::exp(-b2) + (2*b2*b2 + 6*b2 + 1.5)*std::erfc(-b))/A(b) * _vth*_vth;
    return var -= pow<2>(mean());
}

auto UTL::__4_::ShellDistribution::pdf(value_type const v) const noexcept
-> value_type {
    value_type const x = v/_vth, b = _vs/_vth;
    return shell_pdf(x, b) * x*x/_vth;
}
auto UTL::__4_::ShellDistribution::cdf(value_type const v) const noexcept
-> value_type {
    value_type const x = v/_vth, b = _vs/_vth;
    return shell_cdf(x, b) - shell_cdf(0, b);
}
auto UTL::__4_::ShellDistribution::icdf(value_type const cdf) const
-> value_type {
    try {
        return _vth*_icdf(cdf)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

auto UTL::__4_::ShellDistribution::default_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.set_max_recursion(20);
        sampler.set_accuracy_goal(7);
        sampler.set_initial_points(150);
        sampler.set_y_scale_absolute_tolerance(1e-10);
    }
    return sampler;
}

UTL::__4_::ShellDistribution::ShellDistribution(value_type const vth, value_type const vs)
: Distribution(), _vth(vth), _vs(vs), _icdf() {
    if (_vth <= 0/* || _vs < 0*/) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative vth");
    }

    // sampler:
    sampler_type const sampler = default_sampler();

    // sample cdf:
    value_type const b = _vs/_vth, x_max = 6;
    sampler_type::point_list_type
    cdf_pts = sampler(std::bind<value_type>(&shell_cdf, std::placeholders::_1, b), b > x_max ? b - x_max : 0, x_max + b, 1);

    // re-normalization:
    {
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        for (auto &pt : cdf_pts) {
            pt.second = (pt.second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // inverse cdf:
    for (auto &pt : cdf_pts) {
        std::swap(pt.first, pt.second);
    }
    this->_icdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
}

auto UTL::__4_::ShellDistribution::shell_pdf(value_type const x/*v/vth*/, value_type const b/*vs/vth*/) noexcept
-> value_type {
    // note that x is not multiplied!
    return std::exp(-pow<2>(x - b))*2*M_2_SQRTPI/A(b);
}
auto UTL::__4_::ShellDistribution::shell_cdf(value_type const x/*v/vth*/, value_type const b/*vs/vth*/) noexcept
-> value_type {
    // note that this is indefinite integral of pdf!
    return (2*b*b + 1)*std::erf(x - b)/A(b) - .5*((x - b) + 2*b)*shell_pdf(x, b);
}
auto UTL::__4_::ShellDistribution::A(value_type const b/*vs/vth*/) noexcept
-> value_type {
    return M_2_SQRTPI*b*std::exp(-b*b) + (2*b*b + 1)*std::erfc(-b);
}
