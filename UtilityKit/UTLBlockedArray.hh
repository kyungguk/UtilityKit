//
//  UTLBlockedArray.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/23/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLBlockedArray_hh
#define UTLBlockedArray_hh

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <stdexcept>
#include <ostream>
#include <string>

// MARK: BlockedArray::iterator
//
template <class Type, long BlockSize, class OverlayT>
class UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::iterator {
    friend BlockedArray<Type, BlockSize, OverlayT>;
    using _Super = BlockedArray<Type, BlockSize, OverlayT>;
    using _ItPair = std::pair<
    typename _Super::bucket_type::iterator,
    typename _Super::subarray_type::iterator
    >;
    _ItPair _it{nullptr, nullptr};
    iterator(typename _ItPair::first_type _1, typename _ItPair::second_type _2) noexcept : _it(_1, _2) {}

public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type        = Type;
    using reference         = Type&;
    using pointer           = Type*;
    using difference_type   = long;

    ~iterator() = default;
    explicit iterator() = default;
    iterator(iterator const&) noexcept = default;
    iterator& operator=(iterator const&) noexcept = default;

    reference operator*() const noexcept { return *_it.second; }

    iterator& operator++() noexcept {
        if (_it.first->end() == ++_it.second) {
            _it.second = (++_it.first)->begin();
        }
        return *this;
    }
    iterator operator++(int) noexcept { iterator retval = *this; ++(*this); return retval; }

    iterator& operator--() noexcept {
        if (_it.first->begin() == _it.second--) {
            _it.second = (--_it.first)->end();
            --_it.second;
        }
        return *this;
    }
    iterator operator--(int) noexcept { iterator retval = *this; --(*this); return retval; }

    void swap(iterator& o) noexcept { std::swap(_it, o._it); }

    bool operator==(iterator other) const noexcept { return _it == other._it; }
    bool operator!=(iterator other) const noexcept { return _it != other._it; }
};

// MARK: BlockedArray::const_iterator
//
template <class Type, long BlockSize, class OverlayT>
class UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::const_iterator {
    friend BlockedArray<Type, BlockSize, OverlayT>;
    using _Super = BlockedArray<Type, BlockSize, OverlayT>;
    using _ItPair = std::pair<
    typename _Super::bucket_type::const_iterator,
    typename _Super::subarray_type::const_iterator
    >;
    _ItPair _it{nullptr, nullptr};
    const_iterator(typename _ItPair::first_type _1, typename _ItPair::second_type _2) noexcept : _it(_1, _2) {}

public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type        = Type const;
    using reference         = Type const&;
    using pointer           = Type const*;
    using difference_type   = long;

    ~const_iterator() = default;
    explicit const_iterator() = default;
    const_iterator(const_iterator const&) noexcept = default;
    const_iterator& operator=(const_iterator const&) noexcept = default;
    const_iterator(iterator const& o) noexcept : _it(o._it) {}
    const_iterator& operator=(iterator const& o) noexcept { _it = o._it; return *this; }

    reference operator*() const noexcept { return *_it.second; }

    const_iterator& operator++() noexcept {
        if (_it.first->end() == ++_it.second) {
            _it.second = (++_it.first)->begin();
        }
        return *this;
    }
    const_iterator operator++(int) noexcept { const_iterator retval = *this; ++(*this); return retval; }

    const_iterator& operator--() noexcept {
        if (_it.first->begin() == _it.second--) {
            _it.second = (--_it.first)->end();
            --_it.second;
        }
        return *this;
    }
    const_iterator operator--(int) noexcept { const_iterator retval = *this; --(*this); return retval; }

    void swap(const_iterator& o) noexcept { std::swap(_it, o._it); }

    bool operator==(const_iterator other) const noexcept { return _it == other._it; }
    bool operator!=(const_iterator other) const noexcept { return _it != other._it; }
    bool operator==(iterator other) const noexcept { return _it.first == other._it.first && _it.second == other._it.second; }
    bool operator!=(iterator other) const noexcept { return !(*this == other); }
};

// MARK: BlockedArray
//
template <class Type, long BlockSize, class OverlayT>
UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::BlockedArray(size_type sz)
: BlockedArray() {
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    while (sz >= block_size()) { // note that an empty subarray is appended when sz % BlockSize == 0
        _bucket.emplace_back(block_size());
        sz -= block_size();
    }
    _bucket.emplace_back(sz);
}
template <class Type, long BlockSize, class OverlayT>
UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::BlockedArray(size_type const sz, value_type const& x)
: BlockedArray() {
    if (sz < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid size argument");
    }
    while (sz >= block_size()) { // note that an empty subarray is appended when sz % BlockSize == 0
        _bucket.emplace_back(block_size(), x);
        sz -= block_size();
    }
    _bucket.emplace_back(sz, x);
}

template <class Type, long BlockSize, class OverlayT>
template <class ForwardIt, typename std::enable_if<std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<ForwardIt>::iterator_category>::value, long>::type>
UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::BlockedArray(ForwardIt first, ForwardIt last)
: BlockedArray() {
    while (first != last) {
        this->emplace_back(*first++);
    }
}

template <class Type, long BlockSize, class OverlayT>
auto UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::operator=(BlockedArray const &other)
-> BlockedArray& {
    if (this != &other) {
        BlockedArray{other}.swap(*this);
    }
    return *this;
}
template <class Type, long BlockSize, class OverlayT>
auto UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::operator=(BlockedArray &&other) noexcept(noexcept(BlockedArray(std::move(other))))
-> BlockedArray& {
    if (this != &other) {
        BlockedArray{std::move(other)}.swap(*this);
    }
    return *this;
}

template <class Type, long BlockSize, class OverlayT>
bool UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::empty() const noexcept
{
    if (_bucket.empty()) return true;
    for (subarray_type const& sub : _bucket) {
        if (sub.empty()) return true;
    }
    return false;
}
template <class Type, long BlockSize, class OverlayT>
auto UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::size() const noexcept
-> size_type {
    size_type sz{0};
    for (subarray_type const& sub : _bucket) {
        sz += sub.size();
    }
    return sz;
}

template <class Type, long BlockSize, class OverlayT>
void UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::squeeze()
{
    // helper
    static void (*const squeeze)(typename bucket_type::iterator, typename bucket_type::iterator&) = [](typename bucket_type::iterator first, typename bucket_type::iterator& last) {
        while (first != last) {
            while (first->size() < block_size()) {
                while (last->empty()) { // skip empty subarrays
                    if (first == --last) return;
                }
                first->emplace_back(std::move(last->back()));
                last->pop_back();
            }
            ++first;
        }
    };
    if (_bucket.size() <= size_type{1}) return; // no op when there is only 1 subarray
    typename bucket_type::iterator first = _bucket.begin();
    typename bucket_type::iterator last = _bucket.end();
    squeeze(first, --last); // --last > first
    // erase trailing
    _bucket.erase(++last, _bucket.end());
}

template <class Type, long BlockSize, class OverlayT>
auto UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::back() noexcept
-> reference {
    typename bucket_type::reverse_iterator first = _bucket.rbegin();
    typename bucket_type::const_reverse_iterator const last = _bucket.rend();
    for (; first != last; ++first) {
        if (!first->empty()) break;
    }
    return first->back();
}
template <class Type, long BlockSize, class OverlayT>
auto UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::back() const noexcept
-> const_reference {
    typename bucket_type::const_reverse_iterator first = _bucket.rbegin();
    typename bucket_type::const_reverse_iterator const last = _bucket.rend();
    for (; first != last; ++first) {
        if (!first->empty()) break;
    }
    return first->back();
}

template <class Type, long BlockSize, class OverlayT>
template <class UnaryFunc>
void UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::for_each(UnaryFunc&& f) noexcept(noexcept(f(std::declval<value_type>())))
{
    for (subarray_type& sub : _bucket) {
        for (reference x : sub) {
            std::forward<UnaryFunc>(f)(x);
        }
    }
}
template <class Type, long BlockSize, class OverlayT>
template <class UnaryFunc>
void UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::for_each(UnaryFunc&& f) const noexcept(noexcept(f(std::declval<value_type const>())))
{
    for (subarray_type const& sub : _bucket) {
        for (const_reference x : sub) {
            std::forward<UnaryFunc>(f)(x);
        }
    }
}

template <class Type, long BlockSize, class OverlayT>
template <class... Args>
auto UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::emplace_back(Args&&... args)
-> reference {
    if (_bucket.empty() || _bucket.back().size() == block_size()) {
        _bucket.emplace_back();
    }
    _bucket.back().emplace_back(std::forward<Args>(args)...);
}

template <class Type, long BlockSize, class OverlayT>
void UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::pop_back() noexcept
{
    if (_bucket.empty()) return;
    typename bucket_type::iterator first = _bucket.begin(), last = _bucket.end();
    while ((--last)->empty() && first != last) ; // skip empty subarrays
    last->pop_back();
    _bucket.erase(++last, _bucket.end());
}

template <class Type, long BlockSize, class OverlayT>
template <class Container, class Predicate/*bool(position_type)*/>
auto UTL::__4_::BlockedArray<Type, BlockSize, OverlayT>::evict_if(Container& bucket, Predicate&& pred)
-> size_type {
    size_type n{0};
    for (subarray_type& sub : _bucket) {
        n += sub.evict_if(bucket, pred);
    }
    return n;
}

// MARK: std::swap
//
namespace std {
    template <class Type, long BlockSize, class OverlayT>
    inline void swap(UTL::__4_::BlockedArray<Type, BlockSize, OverlayT> &a, UTL::__4_::BlockedArray<Type, BlockSize, OverlayT> &b) noexcept(noexcept(a.swap(b))) {
        a.swap(b);
    }
}

// MARK: Bufferred Output Stream
//
template <class _CharT, class _Traits, class Type, long BlockSize, class OverlayT>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::BlockedArray<Type, BlockSize, OverlayT> const& a) {
    std::basic_ostringstream<_CharT, _Traits> ss; {
        ss.flags(os.flags());
        ss.imbue(os.getloc());
        ss.precision(os.precision());
    }
    a.for_each([&ss](Type const& x) { ss << x << ", "; });
    auto&& buf = ss.str();
    if (!buf.empty()) {
        buf = buf.substr(0, buf.size() - 2);
    }
    return os << "{" << buf << "}";
}

#endif /* UTLBlockedArray_hh */
