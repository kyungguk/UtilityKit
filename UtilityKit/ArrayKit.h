//
//  ArrayKit.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/5/15.
//
//

#ifndef UtilityKit__ArrayKit_h
#define UtilityKit__ArrayKit_h


#if defined(__cplusplus)

// version 3
#include <UtilityKit/PaddedArray-Aggregate.h>
#include <UtilityKit/PaddedArray-Dynamic.h>
#include <UtilityKit/CappedVector.hpp>
#include <UtilityKit/FixedSizedNumericVector.hpp>

// version 4
#include <UtilityKit/UTLArray.h>
#include <UtilityKit/UTLPaddedArray.h>
#include <UtilityKit/UTLArrayND.h>
#include <UtilityKit/UTLDynamicArray.h>
#include <UtilityKit/UTLStaticArray.h>
#include <UtilityKit/UTLBlockedArray.h>

#endif


#endif
