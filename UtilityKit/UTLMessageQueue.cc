//
//  UTLMessageQueue.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/25/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLMessageQueue.h"

// MARK:- Version 4
//
#include <UtilityKit/UTLThread.h>
#include <UtilityKit/UTLBlockQueue.h>
#include <UtilityKit/UTLLock.h>
#include <UtilityKit/UTLSema.h>
#include <cstddef>
#include <unordered_map>
#include <stdexcept>
#include <memory>
#include <string>
#include <vector>
#include <atomic>

struct UTL::__4_::Messenger::_Envelop {
    long to;
    long from;
    constexpr explicit _Envelop(long const _to, long const _from) noexcept : to(_to), from(_from) {}
};

class UTL::__4_::Messenger::_MessageQueue {
    std::unordered_map<long, BlockQueue<SpinLock>> _qs;
    SpinLock _lk;
public:
    BlockQueue<SpinLock>& operator[](_Envelop const &envelop) {
        LockG<decltype(_lk)> _(_lk);
        return _qs[envelop.from];
    }
};

struct UTL::__4_::Messenger::_BarrierToken {
    std::atomic_long counter;
    Sema<SpinLock> sem;
    explicit _BarrierToken(long const size) : counter(size), sem(0) {}
};

UTL::__4_::Messenger::Request::~Request()
{
    if (_ref_cnt && !--*_ref_cnt) {
        delete _ref_cnt;
        delete _sem;
    }
}

UTL::__4_::Messenger::Request::Request()
: _ref_cnt(new std::atomic_long(1)), _sem(new Sema<SpinLock>(0)) {
}

UTL::__4_::Messenger::Request::Request(Request const &o)
: _ref_cnt((static_cast<void>(++*o._ref_cnt), o._ref_cnt)), _sem(o._sem) {
}
UTL::__4_::Messenger::Request::Request(Request&& o)
: _ref_cnt(o._ref_cnt), _sem(o._sem) {
    o._ref_cnt = nullptr;
    o._sem = nullptr;
}

UTL::__4_::Messenger::Messenger(long const size, long const rank, _MessageQueue *const msgQs, _BarrierToken *const barTok)
: _size(size), _rank(rank), _msgQs(msgQs), _barTok(barTok) {
#if defined(DEBUG)
    if (size <= 0 || rank < 0 || rank >= size || msgQs == nullptr || barTok == nullptr) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
#endif
}

void UTL::__4_::Messenger::barrier() const
{
    // assumes that it is called by the thread that executes the main
    if (!--_barTok->counter) { // last thread entered
        _barTok->counter = _size;
        for (long i = 0; i < _size; ++i) {
            _barTok->sem.post();
        }
    }
    _barTok->sem.wait();
}

void UTL::__4_::Messenger::_start(long const size, std::function<void(Messenger const &)> const &main)
{
    if (size <= 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - size argument should be a positive number");
    }
    std::unique_ptr<_MessageQueue[]> const msgQs(new _MessageQueue[static_cast<std::size_t>(size)]);
    _BarrierToken barTok{size};
    std::vector<Thread> pool;
    for (long rank = 1; rank < size; ++rank) {
        pool.emplace_back(main, Messenger{size, rank, msgQs.get(), &barTok});
    }
    main(Messenger{size, 0, msgQs.get(), &barTok});
    for (Thread &th : pool) {
        th.join();
    }
}
