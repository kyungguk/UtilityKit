//
//  UTLTrapzIntegrator.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLTrapzIntegrator_h
#define UTLTrapzIntegrator_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <type_traits>
#include <utility>
#include <tuple> // std::tie
#include <cmath>
#include <complex> // This should be preceed the definition, otherwise std::abs are not specialized for std::complex.

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    // FIXME: Implementation of qsimp is erroneous.
    /**
     @brief Trapezoidal integration algorithm.
     */
    template <class X, class Y = X>
    struct TrapzIntegrator {
        static_assert(std::is_floating_point<X>::value, "The abscissa type should be of a floating point type");

        unsigned accuracyGoal{7}; //!< Accuracy tolerence which is 10^-accuracyGoal.
        unsigned maxRecursion{20}; //!< Maximum number of bisection.
        unsigned minRecursion{5}; //!< Minimum number of bisection.

        /**
         @brief Trapezoidal integration.
         @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
         @param a Lower limit.
         @param b Upper limit.
         @param norm A function that returns a norm of the updated value, which is used for convergence test. The function signature is X(Y).
         @return A pair of integrated value and number of recursions.
         If not converged, the returned number of recursions are the same as maxRecursion.
         */
        template <class F/*Y(X)*/, class Norm/*X(Y)*/>
        std::pair<Y, unsigned> qtrap(F f, X const a, X const b, Norm norm) const {
            X const tolerance = std::pow(X{10}, -X(accuracyGoal));
            Y s;
            unsigned n, i;
            std::tie(s, n) = _trapzd(f, a, b);
            for (i = 1; i < minRecursion; ++i) {
                std::tie(s, n) = _trapzd(f, a, b, s, n);
            }
            for (; i <= maxRecursion; ++i) {
                Y const sold = s;
                std::tie(s, n) = _trapzd(f, a, b, s, n);

                if (norm(sold - s) < norm(sold)*tolerance) {
                    return {s, n};
                }
            }

            // not converged
            return {s, n};
        }
        /**
         @brief Trapezoidal integration with simpson's rule.
         @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
         @param a Lower limit.
         @param b Upper limit.
         @param norm A function that returns a norm of the updated value, which is used for convergence test. The function signature is X(Y).
         @return A pair of integrated value and number of recursions.
         If not converged, the returned number of recursions are the same as maxRecursion.
         */
        template <class F/*Y(X)*/, class Norm/*X(Y)*/>
        std::pair<Y, unsigned> qsimp(F f, X const a, X const b, Norm norm) const {
            X const tolerance = std::pow(X{10}, -X(accuracyGoal));
            Y st;
            unsigned n, i;
            std::tie(st, n) = _trapzd(f, a, b);
            Y s = st;
            for (i = 1; i < minRecursion; ++i) {
                std::tie(st, n) = _trapzd(f, a, b, s, n);
                s = (X(4.)*st - s)/X(3.);
            }
            for (; i <= maxRecursion; ++i) {
                Y const sold = s;
                std::tie(st, n) = _trapzd(f, a, b, s, n);
                s = (X(4.)*st - s)/X(3.);

                if (norm(sold - s) < norm(sold)*tolerance) {
                    return {s, n};
                }
            }

            // not converged
            return {s, n};
        }

        /**
         @brief Trapezoidal integration.
         @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
         @param a Lower limit.
         @param b Upper limit.
         @return A pair of integrated value and number of recursions.
         If not converged, the returned number of recursions are the same as maxRecursion.
         */
        template <class F/*Y(X)*/>
        std::pair<Y, unsigned> qtrap(F f, X const a, X const b) const {
            return qtrap<F const&>(f, a, b, &std::abs<X>);
        }
        /**
         @brief Trapezoidal integration with simpson's rule.
         @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
         @param a Lower limit.
         @param b Upper limit.
         @return A pair of integrated value and number of recursions.
         If not converged, the returned number of recursions are the same as maxRecursion.
         */
        template <class F/*Y(X)*/>
        std::pair<Y, unsigned> qsimp(F f, X const a, X const b) const {
            return qsimp<F const&>(f, a, b, &std::abs<X>);
        }

        /**
         @brief Shorthand for qsimp(f, a, b).
         */
        template <class F/*Y(X)*/>
        std::pair<Y, unsigned> operator()(F f, X const a, X const b) const { return qsimp<F const&>(f, a, b); }

        /**
         @brief Shorthand for qsimp(f, a, b, test).
         */
        template <class F/*Y(X)*/, class Norm/*X(Y)*/>
        std::pair<Y, unsigned> operator()(F f, X const a, X const b, Norm norm) const { return qsimp<F const&, Norm const&>(f, a, b, norm); }

        /**
         @brief Simple trapezoidal sum.
         @param x_first The beginning of the abscissa iterator. The value type should be convertible to X.
         @param x_last The end of the abscissa iterator.
         @param y_first The beginning of the ordinate iterator. The value type should be convertible to Y.
         @return Integrated value of type Y.
         */
        template <class XIt, class YIt>
        static Y trapzd(XIt x_first, XIt x_last, YIt y_first) {
            Y sum(X(0.));
            if (x_first != x_last) {
                XIt x1 = x_first, x0 = x1++;
                YIt y1 = y_first, y0 = y1++;
                for (; x1 != x_last; x0 = x1++, y0 = y1++) {
                    sum += _trapzd(*x0, *y0, *x1, *y1);
                }
            }
            return sum;
        }

    private:
        static Y _trapzd(X const x0, Y const &y0, X const x1, Y const &y1) {
            return X(.5)*(x1 - x0)*(y0 + y1);
        }
        template <class F>
        static std::pair<Y, unsigned> _trapzd(F const& f, X const a, X const b) {
            return {_trapzd(a, f(a), b, f(b)), 0};
        }
        template <class F>
        static std::pair<Y, unsigned> _trapzd(F const& f, X const a, X const b, Y const& sold, unsigned const _n) {
            unsigned const n = 01 << _n;
            Y sumf(X(0.));
            X const dx = (b - a)/X(n);
            for (unsigned i = 0; i < n; ++i) {
                X const x = a + (X(i) + X(.5))*dx;
                sumf += f(x);
            }

            return {X(.5)*(sold + dx*sumf), _n+1};
        }
    } UTILITYKIT_DEPRECATED_ATTRIBUTE;

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <type_traits>
#include <iterator>
#include <utility>
#include <cmath>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class X = double>
    class TrapzIntegrator;

    /**
     @brief Trapezoidal integration algorithm.
     */
    template <class X>
    class TrapzIntegrator {
        static_assert(std::is_floating_point<X>::value, "the template parameter should be a floating point type");
    public:
        // types
        //
        using abscissa_type = X;

    private: // member variables
        abscissa_type _tolerance;
        unsigned _accuracy_goal;
        unsigned _max_recursion;
        unsigned _min_recursion;

    public:
        // constructor
        //
        explicit TrapzIntegrator() noexcept {
            set_accuracy_goal(7);
            set_max_recursion(20);
            set_min_recursion(5);
        }

        // properties
        //
        /**
         @brief Accuracy goal.
         @discussion Tolerance is 10^-accuracy_goal.
         */
        unsigned accuracy_goal() const noexcept { return _accuracy_goal; }
        void set_accuracy_goal(unsigned const accuracy_goal) noexcept {
            _accuracy_goal = accuracy_goal;
            _tolerance = std::pow(abscissa_type{10}, -abscissa_type(accuracy_goal));
        }
        /**
         @brief Maximum number of bisection.
         */
        unsigned max_recursion() const noexcept { return _max_recursion; }
        void set_max_recursion(unsigned const max_recursion) noexcept { _max_recursion = max_recursion; }
        /**
         @brief Minimum number of bisection.
         */
        unsigned min_recursion() const noexcept { return _min_recursion; }
        void set_min_recursion(unsigned const min_recursion) noexcept { _min_recursion = min_recursion; }

        // integrate
        //
        /**
         @brief Trapezoidal integration.
         @discussion Given `int_prev' is the trapz. sum in the previous iteration, and `int_cur' is the trapz. sum in the current iteration,
         the integral is considered to be converged if `norm(int_prev + int_cur) < 2*tolerance' or `norm(int_prev - int_cur)/int_prev < tolerance'.
         @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
         @param a Lower limit.
         @param b Upper limit.
         @param norm A function that returns a norm of the updated value, which is used for convergence test. The function signature is X(Y).
         @return A pair of integrated value and number of recursions.
         If not converged, the returned number of recursions are greater than max_recursion.
         */
        template <class F/*Y(X)*/, class Norm/*X(Y)*/>
        inline auto qtrap(F&& f, abscissa_type const a, abscissa_type const b, Norm&& norm) const -> std::pair<typename std::decay<decltype(f(a))>::type, unsigned>;

        /**
         @brief Trapezoidal integration with simpson's rule.
         @discussion Given `int_prev' is the trapz. sum in the previous iteration, and `int_cur' is the trapz. sum in the current iteration,
         the integral is considered to be converged if `norm(int_prev + int_cur) < 2*tolerance' or `norm(int_prev - int_cur)/int_prev < tolerance'.
         @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
         @param a Lower limit.
         @param b Upper limit.
         @param norm A function that returns a norm of the updated value, which is used for convergence test. The function signature is X(Y).
         @return A pair of integrated value and number of recursions.
         If not converged, the returned number of recursions are greater than max_recursion.
         */
        template <class F/*Y(X)*/, class Norm/*X(Y)*/>
        inline auto qsimp(F&& f, abscissa_type const a, abscissa_type const b, Norm&& norm) const -> std::pair<typename std::decay<decltype(f(a))>::type, unsigned>;

        /**
         @brief Trapezoidal integration.
         @discussion Given `int_prev' is the trapz. sum in the previous iteration, and `int_cur' is the trapz. sum in the current iteration,
         the integral is considered to be converged if `norm(int_prev + int_cur) < 2*tolerance' or `norm(int_prev - int_cur)/int_prev < tolerance'.
         @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
         @param a Lower limit.
         @param b Upper limit.
         @return A pair of integrated value and number of recursions.
         If not converged, the returned number of recursions are greater than max_recursion.
         */
        template <class F/*Y(X)*/>
        auto qtrap(F&& f, abscissa_type const a, abscissa_type const b) const -> std::pair<typename std::decay<decltype(f(a))>::type, unsigned> {
            return qtrap(std::forward<F>(f), a, b, &_abs);
        }
        /**
         @brief Trapezoidal integration with simpson's rule.
         @discussion Given `int_prev' is the trapz. sum in the previous iteration, and `int_cur' is the trapz. sum in the current iteration,
         the integral is considered to be converged if `norm(int_prev + int_cur) < 2*tolerance' or `norm(int_prev - int_cur)/int_prev < tolerance'.
         @param f A function of type Y(X) to integrate. It should return a finite number convertible to Y.
         @param a Lower limit.
         @param b Upper limit.
         @return A pair of integrated value and number of recursions.
         If not converged, the returned number of recursions are greater than max_recursion.
         */
        template <class F/*Y(X)*/>
        auto qsimp(F&& f, abscissa_type const a, abscissa_type const b) const -> std::pair<typename std::decay<decltype(f(a))>::type, unsigned> {
            return qsimp(std::forward<F>(f), a, b, &_abs);
        }

        /**
         @brief Shorthand for qsimp(f, a, b).
         */
        template <class F/*Y(X)*/>
        auto operator()(F&& f, abscissa_type const a, abscissa_type const b) const -> std::pair<typename std::decay<decltype(f(a))>::type, unsigned> {
            return qsimp(std::forward<F>(f), a, b);
        }

        /**
         @brief Shorthand for qsimp(f, a, b, test).
         */
        template <class F/*Y(X)*/, class Norm/*X(Y)*/>
        auto operator()(F&& f, abscissa_type const a, abscissa_type const b, Norm&& norm) const -> std::pair<typename std::decay<decltype(f(a))>::type, unsigned> {
            return qsimp(std::forward<F>(f), a, b, std::forward<Norm>(norm));
        }

        /**
         @brief Simple trapezoidal sum.
         @param x_first The beginning of the abscissa iterator. The value type should be convertible to X.
         @param x_last The end of the abscissa iterator.
         @param y_first The beginning of the ordinate iterator. The value type should be convertible to Y.
         @return Integrated value of type Y.
         */
        template <class XIt, class YIt>
        inline static typename std::iterator_traits<YIt>::value_type trapzd(XIt x_first, XIt x_last, YIt y_first);

    private:
        static abscissa_type _abs(abscissa_type const x) noexcept {
            return std::abs(x);
        }
        template <class Y>
        static Y _trapzd(std::pair<abscissa_type, abscissa_type> const &x, std::pair<Y, Y> const &y) {
            constexpr abscissa_type _1_2 = abscissa_type{1}/2;
            return _1_2*(std::get<1>(x) - std::get<0>(x))*(std::get<1>(y) + std::get<0>(y));
        }
        template <class F>
        static auto _trapzd(F& f, abscissa_type const a, abscissa_type const b) -> std::pair<typename std::decay<decltype(f(a))>::type, unsigned> {
            return {_trapzd(std::make_pair(a, b), std::make_pair(f(a), f(b))), 1};
        }
        template <class F, class Y>
        static auto _trapzd(F& f, abscissa_type const a, abscissa_type const b, Y const& sold, unsigned const n) -> std::pair<Y, unsigned> {
            constexpr abscissa_type _1_2 = abscissa_type{1}/2;
            Y sumf{};
            abscissa_type const dx = (b - a)/n;
            for (unsigned i = 0; i < n; ++i) {
                sumf += f(a + (i + _1_2)*dx);
            }
            return {_1_2*(sold + dx*sumf), n*2};
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLTrapzIntegrator.hh>

#endif /* UTLTrapzIntegrator_h */
