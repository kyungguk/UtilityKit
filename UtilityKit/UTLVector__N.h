//
//  UTLVector__N.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/27/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLVector__N_h
#define UTLVector__N_h

#include <UtilityKit/UTLVector__0.h>

// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK: Vector<N>
    //
    /**
     A vector of length N.
     */
    template <class Type, long Size>
    struct Vector {
        static_assert(Size > 0, "invalid vector size");

        // types:
        using value_type             = Type;
        using reference              = Type&;
        using const_reference        = Type const&;
        using pointer                = Type*;
        using const_pointer          = Type const*;
        using iterator               = pointer;
        using const_iterator         = const_pointer;
        using reverse_iterator       = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;
        using size_type              = long;
        using difference_type        = long;
        using size_vector_type       = Vector<size_type, 1L>;
        using comparison_result_type = Vector<typename _comparison_result<Type>::type, Size>;
        using innermost_element_type = typename _Innermost<Type>::type; // the innermost element type if Type is a vector (whose value_type may be vector as well)

    private:
        using _Array = std::array<Type, Size>;
        _Array _a{};

    public:
        // capacity:
        static constexpr size_type size() noexcept { return size_type{Size}; }
        static constexpr size_type max_size() noexcept { return size(); }
        static constexpr size_type rank() noexcept { return size_vector_type::size(); }
        static constexpr size_vector_type dims() noexcept { return {size()}; }
        static constexpr size_vector_type max_dims() noexcept { return dims(); }

        // constructors:
        constexpr explicit Vector() noexcept(noexcept(_Array{})) {}
        explicit Vector(Type const& v) noexcept(noexcept(Vector{}) && noexcept(_a.fill(v))) : Vector() { _a.fill(v); }
        /**
         @brief Construct with parameter pack arguments.
         @discussion It first default-initializes the elements (as if calling Vector{}), and then copy-assigns them with the corresponding arguments (as if each argument is copy-assigned to the corresponding element).
         The number of arguments can be less than size(), in which case, the elements that do not have the corresponding arguments are left default-initialized.
         @note This overload only participates when the number of arguments are greater than 1 and less than or equal to size().
         */
        template <class... Args, typename std::enable_if<(sizeof...(Args) > 1 && sizeof...(Args) <= Vector::size()), long>::type = 0L>
        Vector(Args&&... args) : Vector() { _unpack(_a.data(), std::forward<Args>(args)...); }

        // copy/move:
        Vector(Vector const& o) noexcept(std::is_nothrow_copy_constructible<_Array>::value) : _a(o._a) {}
        inline Vector& operator=(Vector const& o) noexcept(std::is_nothrow_copy_assignable<_Array>::value);
        Vector(Vector&& o) noexcept(std::is_nothrow_move_constructible<_Array>::value) : _a(std::move(o._a)) {}
        inline Vector& operator=(Vector&& o) noexcept(std::is_nothrow_move_assignable<_Array>::value);
        template <class U, typename std::enable_if<std::is_convertible<U, Type>::value, long>::type = 0L>
        explicit Vector(Vector<U, size()> const& o) : Vector() { _elem_conversion(o); }
        template <class U>
        auto operator=(Vector<U, size()> const& o) -> typename std::enable_if<std::is_convertible<U, Type>::value, Vector>::type &{ return *this = Vector{o}; }

        // modifiers:
        void fill(Type const& v) noexcept(noexcept(_a.fill(v))) { _a.fill(v); }
        void swap(Vector& o) noexcept(noexcept(std::swap(_a, o._a))) { std::swap(_a, o._a); }

        // cast:
        operator std::array<Type, Vector::size()> const&() const& noexcept { return _a; }
        operator std::array<Type, Vector::size()>      &()      & noexcept { return _a; }

        // element access:
        reference       back ()       noexcept { return _a.back(); }
        const_reference back () const noexcept { return _a.back(); }
        reference       front()       noexcept { return _a.front(); }
        const_reference front() const noexcept { return _a.front(); }

        pointer       data()       noexcept { return _a.data(); }
        const_pointer data() const noexcept { return _a.data(); }

#if defined(DEBUG)
        reference       operator[](size_type const i)       { return this->at(i); }
        const_reference operator[](size_type const i) const { return this->at(i); }
#else
        reference       operator[](size_type const i)       noexcept { return data()[i]; }
        const_reference operator[](size_type const i) const noexcept { return data()[i]; }
#endif

        inline reference       at(size_type const i);
        inline const_reference at(size_type const i) const;

        // iterators:
        iterator        begin()       noexcept { return       iterator{data()}; }
        const_iterator  begin() const noexcept { return const_iterator{data()}; }
        const_iterator cbegin() const noexcept { return begin(); }
        iterator        end  ()       noexcept { return       iterator{data() + Vector::size()}; }
        const_iterator  end  () const noexcept { return const_iterator{data() + Vector::size()}; }
        const_iterator cend  () const noexcept { return end(); }

        reverse_iterator        rbegin()       noexcept { return       reverse_iterator{end()}; }
        const_reverse_iterator  rbegin() const noexcept { return const_reverse_iterator{end()}; }
        const_reverse_iterator crbegin() const noexcept { return rbegin(); }
        reverse_iterator        rend  ()       noexcept { return       reverse_iterator{begin()}; }
        const_reverse_iterator  rend  () const noexcept { return const_reverse_iterator{begin()}; }
        const_reverse_iterator crend  () const noexcept { return rend(); }

        // half vectors:
        using lo_vector_type = Vector<Type, Vector::size()/2 + Vector::size()%2>;
        lo_vector_type      & lo()       noexcept { return reinterpret_cast<lo_vector_type      *>(this)[0]; }
        lo_vector_type const& lo() const noexcept { return reinterpret_cast<lo_vector_type const*>(this)[0]; }
        using hi_vector_type = Vector<Type, Vector::size()/2>;
        hi_vector_type      & hi()       noexcept { return *reinterpret_cast<hi_vector_type      *>(&lo() + 1); }
        hi_vector_type const& hi() const noexcept { return *reinterpret_cast<hi_vector_type const*>(&lo() + 1); }

        // transformations:
        // ** take const **
        template <long N> typename std::enable_if<(N > 0), Vector<Type, N>
        >::type const &take() const noexcept { static_assert(N <= Vector::size(), "cannot take more than already have");
            return *reinterpret_cast<Vector<Type, N> const*>(this);
        }
        template <long M> typename std::enable_if<(M < 0), Vector<Type,-M>
        >::type const &take() const noexcept { static_assert(M >=-Vector::size(), "cannot take more than already have");
            return *(reinterpret_cast<Vector<Type,-M> const*>(this + 1) - 1);
        }
        // ** take **
        template <long N> typename std::enable_if<(N > 0), Vector<Type, N>
        >::type &take() noexcept { static_assert(N <= Vector::size(), "cannot take more than already have");
            return *reinterpret_cast<Vector<Type, N> *>(this);
        }
        template <long M> typename std::enable_if<(M < 0), Vector<Type,-M>
        >::type &take() noexcept { static_assert(M >=-Vector::size(), "cannot take more than already have");
            return *(reinterpret_cast<Vector<Type,-M> *>(this + 1) - 1);
        }
        // ** drop const **
        template <long N> typename std::enable_if<(N >= 0), Vector<Type, Vector::size() - N>
        >::type const &drop() const noexcept { static_assert(N < Vector::size(), "cannot drop all");
            return this->template take<N - Vector::size()>();
        }
        template <long M> typename std::enable_if<(M < 0), Vector<Type, Vector::size() + M>
        >::type const &drop() const noexcept { static_assert(M >-Vector::size(), "cannot drop all");
            return this->template take<M + Vector::size()>();
        }
        // ** drop **
        template <long N> typename std::enable_if<(N >= 0), Vector<Type, Vector::size() - N>
        >::type &drop() noexcept { static_assert(N < Vector::size(), "cannot drop all");
            return this->template take<N - Vector::size()>();
        }
        template <long M> typename std::enable_if<(M < 0), Vector<Type, Vector::size() + M>
        >::type &drop() noexcept { static_assert(M >-Vector::size(), "cannot drop all");
            return this->template take<M + Vector::size()>();
        }

        Vector<Type, Vector::size() - 1>       &most()       noexcept { return drop<-1>(); }
        Vector<Type, Vector::size() - 1> const &most() const noexcept { return drop<-1>(); }
        Vector<Type, Vector::size() - 1>       &rest()       noexcept { return drop<+1>(); }
        Vector<Type, Vector::size() - 1> const &rest() const noexcept { return drop<+1>(); }

        Vector<Type, Vector::size() + 1>  append(Type const& x) const { decltype(append(x)) v{}; v.most() = *this; v.back () = x; return v; }
        Vector<Type, Vector::size() + 1> prepend(Type const& x) const { decltype(append(x)) v{}; v.rest() = *this; v.front() = x; return v; }

        // reductions:
        inline Type reduce_plus   () const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Type reduce_prod   () const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Type reduce_bit_and() const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Type reduce_bit_or () const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Type reduce_bit_xor() const noexcept(std::is_arithmetic<innermost_element_type>::value);

        // compound arithmetic:
        inline Vector &operator+=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator+=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator-=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator-=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator*=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator*=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator/=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator/=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // compound modulus:
        inline Vector &operator%=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator%=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // compound bit operators:
        inline Vector &operator&=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator&=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator|=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator|=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator^=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator^=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator<<=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator<<=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator>>=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline Vector &operator>>=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value);
        // unary:
        Vector const &operator+() const noexcept { return *this; }
        Vector        operator-() const noexcept(std::is_arithmetic<innermost_element_type>::value) {
            Vector v{*this};
            for (Type &x : v) x = -x;
            return v;
        }
        // comparison (element-wise):
        inline comparison_result_type operator==(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator!=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator<=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator>=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator<(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);
        inline comparison_result_type operator>(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value);

    private:
        // parameter pack constructor helper:
        static void _unpack(pointer) noexcept {}
        template <class First, class... Rest>
        static void _unpack(pointer p, First&& first, Rest&&... rest) {
            *p = std::forward<First>(first);
            _unpack(++p, std::forward<Rest>(rest)...);
        }
        template <class U>
        void _elem_conversion(Vector<U, 1L> const &v) {
            std::get<size() - 1>(_a) = v.front();
        }
        template <class U, long N>
        void _elem_conversion(Vector<U, N> const &v) {
            std::get<size() - N>(_a) = v.front();
            _elem_conversion(v.rest());
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLVector__N.hh>

#endif /* UTLVector__N_h */
