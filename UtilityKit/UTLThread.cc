//
//  UTLThread.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/23/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLThread.h"
#include "UTLPrinto.h"

// MARK: Version 3
//
#include <stdexcept>
#include <string>
#include <iostream>
#include <memory>
#include <type_traits> // for std::swap
#include <unistd.h> // for sysconf

namespace {
    typedef std::function<void(void)> Block;

    void *invoke(void *_data) {
        std::unique_ptr<Block> block(static_cast<Block *>(_data));

        try {
            (*block)();
        } catch (...) {
            printo(std::cerr, __PRETTY_FUNCTION__, " - block threw an exception", '\n');
            std::terminate();
        }

        //pthread_exit(nullptr);
        return nullptr;
    }

    unsigned processor_count;
    void init_processor_count() {
        long i = sysconf(_SC_NPROCESSORS_ONLN);
        if (-1 == i) {
            printo(std::cerr, __PRETTY_FUNCTION__, " - sysconf(_SC_NPROCESSORS_ONLN) returned error", '\n');
            std::terminate();
        }

        // succeeded
        processor_count = unsigned(i);
    }
}

unsigned UTL::__3_::Thread::hardware_concurrency() noexcept
{
    static pthread_once_t once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, &::init_processor_count)) {
        printo(std::cerr, __PRETTY_FUNCTION__, " - pthread_once(...) returned error", '\n');
        std::terminate();
    }

    return ::processor_count;
}

UTL::__3_::Thread::~Thread()
{
    if (joinable()) {
        printo(std::cerr, __PRETTY_FUNCTION__, " - joinable", '\n');
        std::terminate();
    }
}

UTL::__3_::Thread::Thread() noexcept
: __t_(0) {
}
UTL::__3_::Thread::Thread(Thread&& o) noexcept
: __t_(o.__t_) {
    o.__t_ = 0;
}
UTL::__3_::Thread::Thread(::Block const _b)
: __t_(0) {
    // Thread attribute
    //
    pthread_attr_t attr;
    if (pthread_attr_init(&attr) || pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_attr_init(...) or pthread_attr_setdetachstate(..., PTHREAD_CREATE_JOINABLE) returned error");
    }

    // Create pthread
    //
    ::Block *block = new ::Block(_b);
    auto const failed = pthread_create(&__t_, &attr, &::invoke, block);
    if (pthread_attr_destroy(&attr)) {
        printo(std::cerr, __PRETTY_FUNCTION__, " - pthread_attr_destroy(...) returned error", '\n');
    }
    if (failed) { // fail-safe
        delete block;
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_create(...) returned error");
    }
}

auto UTL::__3_::Thread::operator=(Thread &&o) noexcept
-> Thread &{
    if (joinable()) {
        std::terminate();
    } else {
        __t_ = o.__t_;
        o.__t_ = 0;
    }
    return *this;
}
void UTL::__3_::Thread::swap(Thread &o) noexcept
{
    std::swap(__t_, o.__t_);
}

bool UTL::__3_::Thread::joinable() const noexcept
{
    return 0 != __t_;
}
void UTL::__3_::Thread::join()
{
    if (!joinable()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - not joinable");
    }
    if (pthread_equal(pthread_self(), __t_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - calling thread is equal to this");
    }
    if (pthread_join(__t_, nullptr)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_join(...) returned error");
    }

    // joined
    __t_ = 0;
}
void UTL::__3_::Thread::detach()
{
    if (!joinable()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - not joinable");
    }

    if (pthread_detach(__t_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_detach(...) returned error");
    }

    // detached
    __t_ = 0;
}


// MARK:- Version 4
//
#include <stdexcept>
#include <string>
#include <iostream>
#include <memory>
#include <utility> // for std::swap
#include <unistd.h> // for sysconf
#include <sched.h> // for sched_yield()

unsigned UTL::__4_::Thread::hardware_concurrency() noexcept
{
    static long processor_count;
    static pthread_once_t once = PTHREAD_ONCE_INIT;
    if (pthread_once(&once, [](void)->void {
        processor_count = sysconf(_SC_NPROCESSORS_ONLN);
        if (-1 == processor_count) { // error
            printo(std::cerr, __PRETTY_FUNCTION__, " - sysconf(_SC_NPROCESSORS_ONLN) returned error", '\n');
            std::terminate();
        }
    })) {
        printo(std::cerr, __PRETTY_FUNCTION__, " - pthread_once(...) returned error", '\n');
        std::terminate();
    }

    return static_cast<unsigned>(processor_count);
}

int UTL::__4_::Thread::yield(void) noexcept
{
    return sched_yield();
}

UTL::__4_::Thread::~Thread()
{
    if (joinable()) {
        printo(std::cerr, __PRETTY_FUNCTION__, " - joinable", '\n');
        std::terminate();
    }
}

UTL::__4_::Thread::Thread() noexcept
: __t_(0) {
}
UTL::__4_::Thread::Thread(Thread&& o) noexcept
: __t_(o.__t_) {
    o.__t_ = 0;
}
auto UTL::__4_::Thread::_invoke(std::function<void(void)>&& b)
-> native_handle_type {
    native_handle_type th;
    // Thread attribute
    //
    pthread_attr_t attr;
    if (pthread_attr_init(&attr) || pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_attr_init(...) or pthread_attr_setdetachstate(..., PTHREAD_CREATE_JOINABLE) returned error");
    }

    // Create pthread
    //
    static char const *except_st = __PRETTY_FUNCTION__;
    using Block = std::function<void(void)>;
    std::unique_ptr<Block> block{new Block(std::move(b))};
    auto const failed = pthread_create(&th, &attr, [](void *data)->void* {
        try {
            std::unique_ptr<Block> block{static_cast<Block *>(data)};
            (*block)();
        } catch (std::exception &e) {
            printo(std::cerr, except_st, "__block - ", e.what(), '\n');
            std::terminate();
        } catch (...) {
            printo(std::cerr, except_st, "__block - an unknown exception", '\n');
            std::terminate();
        }
        return nullptr;
    }, block.get());
    if (failed) {
        // memory for block will be released on throw
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_create(...) returned error");
    } else {
        // on success, relinquish ownership of block object
        block.release();
    }
    if (pthread_attr_destroy(&attr)) {
        printo(std::cerr, __PRETTY_FUNCTION__, " - pthread_attr_destroy(...) returned error", '\n');
    }
    return th;
}

auto UTL::__4_::Thread::operator=(Thread &&o) noexcept
-> Thread &{
    if (joinable()) {
        std::terminate();
    } else {
        __t_ = o.__t_;
        o.__t_ = 0;
    }
    return *this;
}
void UTL::__4_::Thread::swap(Thread &o) noexcept
{
    std::swap(__t_, o.__t_);
}

bool UTL::__4_::Thread::joinable() const noexcept
{
    return 0 != __t_;
}
void UTL::__4_::Thread::join()
{
    if (!joinable()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - not joinable");
    }
    if (pthread_equal(pthread_self(), __t_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - calling thread is equal to this");
    }
    if (pthread_join(__t_, nullptr)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_join(...) returned error");
    }

    // joined
    __t_ = 0;
}
void UTL::__4_::Thread::detach()
{
    if (!joinable()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - not joinable");
    }

    if (pthread_detach(__t_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_detach(...) returned error");
    }

    // detached
    __t_ = 0;
}
