//
//  UTLBitReversedRandomReal.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLBitReversedRandomReal.h"

// MARK: Version 3
//
#include <stdexcept>
#include <utility>

void UTL::__3_::BitReversedRandomReal::swap(BitReversedRandomReal &o) noexcept
{
    __base_.swap(o.__base_);
    __seq_.swap(o.__seq_);
}
UTL::__3_::BitReversedRandomReal::BitReversedRandomReal(BitReversedRandomReal &&o) noexcept
: RandomReal(), __base_(), __seq_() {
    this->swap(o);
}
auto UTL::__3_::BitReversedRandomReal::operator=(BitReversedRandomReal &&o) noexcept
-> BitReversedRandomReal &{
    if (this != &o) this->swap(o);
        return *this;
}

UTL::__3_::BitReversedRandomReal::BitReversedRandomReal(size_type base, size_type sequence)
: RandomReal(), __base_(), __seq_() {
    if (base < 2) throw std::invalid_argument(__PRETTY_FUNCTION__);
    __base_.reset(new size_type(base));
    __seq_.reset(new size_type(sequence));
}

auto UTL::__3_::BitReversedRandomReal::_revers(size_type seq, size_type base) noexcept
-> value_type {
    // Note that seq can run out of number when it reach Int_Max
    double power = 1, bit_reversed = 0;
    while (seq > 0) {
        auto iquotient = seq / base;
        bit_reversed += (seq%base) * (power/=base);
        seq = iquotient;
    }
    return bit_reversed;
}


// MARK:- Version 4
//
#include <stdexcept>
#include <utility>

void UTL::__4_::BitReversedRandomReal::swap(BitReversedRandomReal &o) noexcept
{
    __base_.swap(o.__base_);
    __seq_.swap(o.__seq_);
}
UTL::__4_::BitReversedRandomReal::BitReversedRandomReal(BitReversedRandomReal &&o) noexcept
: RandomReal(), __base_(std::move(o.__base_)), __seq_(std::move(o.__seq_)) {
}
auto UTL::__4_::BitReversedRandomReal::operator=(BitReversedRandomReal &&o) noexcept
-> BitReversedRandomReal &{
    if (this != &o) {
        BitReversedRandomReal{std::move(o)}.swap(*this);
    }
    return *this;
}

UTL::__4_::BitReversedRandomReal::BitReversedRandomReal(size_type base, size_type sequence)
: RandomReal(), __base_(), __seq_() {
    if (base < 2) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }
    __base_.reset(new size_type(base));
    __seq_.reset(new size_type(sequence));
}

auto UTL::__4_::BitReversedRandomReal::_revers(size_type seq, size_type base) noexcept
-> value_type {
    // note that seq can run out of number when it reach Int_Max
    //
    double power = 1, bit_reversed = 0;
    while (seq > 0) {
        size_type const iquotient = seq / base;
        bit_reversed += (seq % base) * (power /= base);
        seq = iquotient;
    }
    return bit_reversed;
}
