//
//  UTLSineAlphaPitchAngleDistribution.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLSineAlphaPitchAngleDistribution.h"

// MARK: Version 3
//
#include "UTLCustomDistribution.h"
#include <utility>
#include <cmath>
#include <stdexcept>

#pragma mark class implementation
UTL::__3_::SineAlphaPitchAngleDistribution::SineAlphaPitchAngleDistribution(value_type n, bool should_normalize_pdf)
: Distribution(), _dist(), _n(n) {
    if (_n < 0) {
        throw std::invalid_argument(__PRETTY_FUNCTION__);
    }

    _dist.reset( new CustomDistribution(0, M_PI, [this](value_type const alpha)->value_type {
        return this->pdf(alpha);
    }, should_normalize_pdf) );
}

UTL::__3_::SineAlphaPitchAngleDistribution::SineAlphaPitchAngleDistribution(SineAlphaPitchAngleDistribution const &o) noexcept
: Distribution(), _dist(new CustomDistribution(*o._dist)), _n(o._n) {
}
auto UTL::__3_::SineAlphaPitchAngleDistribution::operator=(SineAlphaPitchAngleDistribution const &o) noexcept
-> SineAlphaPitchAngleDistribution &{
    if (this != &o) {
        _dist.reset(new CustomDistribution(*o._dist));
        _n = o._n;
    }
    return *this;
}
UTL::__3_::SineAlphaPitchAngleDistribution::SineAlphaPitchAngleDistribution(SineAlphaPitchAngleDistribution &&o) noexcept
: Distribution(), _dist(std::move(o._dist)), _n(std::move(o._n)) {
}
auto UTL::__3_::SineAlphaPitchAngleDistribution::operator=(SineAlphaPitchAngleDistribution &&o) noexcept
-> SineAlphaPitchAngleDistribution &{
    if (this != &o) {
        _dist = std::move(o._dist);
        _n = std::move(o._n);
    }
    return *this;
}

auto UTL::__3_::SineAlphaPitchAngleDistribution::A() const noexcept
-> value_type {
    return std::tgamma(1 + this->n_2())/std::tgamma(1.5 + this->n_2()) / M_2_SQRTPI;
}

auto UTL::__3_::SineAlphaPitchAngleDistribution::mean() const noexcept
-> value_type {
    return _dist->mean();
}
auto UTL::__3_::SineAlphaPitchAngleDistribution::variance() const noexcept
-> value_type {
    return _dist->variance();
}

auto UTL::__3_::SineAlphaPitchAngleDistribution::pdf(value_type alpha) const
-> value_type {
    return std::abs(std::sin(alpha)) < 1e-10 ? 0. : std::pow(std::sin(alpha), _n + 1) * 0.5/this->A();
}
auto UTL::__3_::SineAlphaPitchAngleDistribution::cdf(value_type alpha) const
-> value_type {
    return _dist->cdf(alpha);
}
auto UTL::__3_::SineAlphaPitchAngleDistribution::icdf(value_type cdf) const
-> value_type {
    return _dist->icdf(cdf);
}


// MARK:- Version 4
//
#include <UtilityKit/AuxiliaryKit.h>
#include <functional>
#include <stdexcept>
#include <utility>
#include <string>
#include <cmath>

UTL::__4_::SineAlphaPitchAngleDistribution::SineAlphaPitchAngleDistribution(SineAlphaPitchAngleDistribution const &o)
: Distribution(), _n(o._n), _variance(o._variance), _cdf(o._cdf), _icdf(o._icdf) {
}
UTL::__4_::SineAlphaPitchAngleDistribution::SineAlphaPitchAngleDistribution(SineAlphaPitchAngleDistribution &&o)
: Distribution(), _n(o._n), _variance(o._variance), _cdf(), _icdf() {
    _cdf = std::move(o._cdf);
    _icdf = std::move(o._icdf);
}
auto UTL::__4_::SineAlphaPitchAngleDistribution::operator=(SineAlphaPitchAngleDistribution const &o)
-> SineAlphaPitchAngleDistribution &{
    if (this != &o) {
        SineAlphaPitchAngleDistribution{o}.swap(*this);
    }
    return *this;
}
auto UTL::__4_::SineAlphaPitchAngleDistribution::operator=(SineAlphaPitchAngleDistribution &&o)
-> SineAlphaPitchAngleDistribution &{
    if (this != &o) {
        SineAlphaPitchAngleDistribution{std::move(o)}.swap(*this);
    }
    return *this;
}

void UTL::__4_::SineAlphaPitchAngleDistribution::swap(SineAlphaPitchAngleDistribution &o)
{
    _cdf.swap(o._cdf);
    _icdf.swap(o._icdf);
    std::swap(_n, o._n);
    std::swap(_variance, o._variance);
}

auto UTL::__4_::SineAlphaPitchAngleDistribution::mean() const noexcept
-> value_type {
    return M_PI_2;
}
auto UTL::__4_::SineAlphaPitchAngleDistribution::variance() const noexcept
-> value_type {
    return _variance;
}

auto UTL::__4_::SineAlphaPitchAngleDistribution::pdf(value_type const alpha) const
-> value_type {
    if (alpha < 0 || alpha > M_PI) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - alpha not within 0 and pi");
    }
    value_type const sina = std::sin(alpha);
    return std::pow(sina, _n + 1) / (2*A(_n));
}
auto UTL::__4_::SineAlphaPitchAngleDistribution::cdf(value_type const alpha) const
-> value_type {
    try {
        return _cdf(alpha)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}
auto UTL::__4_::SineAlphaPitchAngleDistribution::icdf(value_type const cdf) const
-> value_type {
    try {
        return _icdf(cdf)();
    } catch (std::exception const &e) {
        throw std::out_of_range(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
    }
}

auto UTL::__4_::SineAlphaPitchAngleDistribution::default_integrator() noexcept
-> integrator_type {
    integrator_type integrator; {
        integrator.set_accuracy_goal(7);
        integrator.set_max_recursion(20);
        integrator.set_min_recursion(5);
    }
    return integrator;
}
auto UTL::__4_::SineAlphaPitchAngleDistribution::default_sampler() noexcept
-> sampler_type {
    sampler_type sampler; {
        sampler.set_max_recursion(20);
        sampler.set_accuracy_goal(7);
        sampler.set_initial_points(100);
        sampler.set_y_scale_absolute_tolerance(1e-10);
    }
    return sampler;
}

UTL::__4_::SineAlphaPitchAngleDistribution::SineAlphaPitchAngleDistribution(value_type const n)
: Distribution(), _n(n), _variance(), _cdf(), _icdf() {
    if (_n < 0) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative anisotropy index");
    }
    constexpr value_type a_min = 0, a_90 = M_PI_2, a_180 = M_PI;
    auto pdf = std::bind<value_type>(&Distribution::pdf, this, std::placeholders::_1);

    // helpers:
    integrator_type const integrator = default_integrator();
    sampler_type const sampler = default_sampler();

    // variance:
    _variance = integrator([&pdf](value_type const alpha)->value_type {
        return alpha*alpha * pdf(alpha);
    }, a_min, a_180*(1 - 1e-10)/*prevent alpha from being > pi*/).first;
    _variance -= pow<2>(mean());

    // sample cdf:
    sampler_type::point_list_type
    cdf_pts = sampler([&pdf, &integrator](value_type const alpha)->value_type {
        return integrator(pdf, a_min, alpha).first;
    }, a_min, a_90, 1); // left-half domain
    { // stitch right-half domain
        sampler_type::point_list_type right_half = cdf_pts;
        right_half.reverse();
        value_type const cdf180 = 2*right_half.front().second; // cdf at 180deg
        right_half.pop_front(); // remove duplicate element at 90deg
        for (sampler_type::point_type &pt : right_half) { // reflection about 90deg
            pt.first = a_180 - pt.first;
            pt.second = cdf180 - pt.second; // !! this assumes cdf0 == 0 !!
        }
        right_half.splice_after(right_half.before_begin(), std::move(cdf_pts));
        cdf_pts = std::move(right_half);
    }

    // re-normalization:
    {
        cdf_pts.front().first = a_min; // make sure 0 <= alpha <= pi
        value_type const cdf_min = cdf_pts.front().second;
        cdf_pts.reverse();
        cdf_pts.front().first = a_180; // make sure 0 <= alpha <= pi
        value_type const cdf_max = cdf_pts.front().second;
        value_type const denom = cdf_max - cdf_min;
        for (auto &pt : cdf_pts) {
            pt.second = (pt.second - cdf_min) / denom;
        }
        cdf_pts.reverse();
    }

    // save cdf:
    this->_cdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();

    // inverse cdf:
    for (auto &pt : cdf_pts) {
        std::swap(pt.first, pt.second);
    }
    this->_icdf = spline_coefficient_type{cdf_pts.begin(), cdf_pts.end()}.interpolator();
}

auto UTL::__4_::SineAlphaPitchAngleDistribution::A(value_type const n) noexcept
-> value_type {
    value_type const n_2 = n/2;
    return std::tgamma(1 + n_2)/std::tgamma(1.5 + n_2) / M_2_SQRTPI;
}
