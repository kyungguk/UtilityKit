//
//  SIMDVector.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#ifndef SIMDVector_h
#define SIMDVector_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <UtilityKit/SIMDVectorTraits.h>
#include <type_traits>
#include <algorithm>
#include <sstream>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
namespace SIMD {

/**
 @brief Abstraction of the vector extension.
 */
template <class _Tp, long _Size>
struct Vector;

#pragma mark - Vector Expression
template <class T, class E>
struct _VE {
    static_assert(std::is_arithmetic<T>::value, "The template type should be an arithmetic type");
    typedef T            value_type;
    typedef unsigned long size_type;

    value_type operator[](size_type i) const noexcept { return static_cast<E const&>(*this)[i]; }

    operator E      &()       noexcept { return static_cast<      E&>(*this); }
    operator E const&() const noexcept { return static_cast<const E&>(*this); }
};

#pragma mark Vector<2>
/**
 @brief Two-element vector specialization.
 */
template <class _Tp>
struct Vector<_Tp, 2> : public _VE<_Tp, Vector<_Tp, 2> > {
    // capacity:
    static constexpr long size() noexcept { return 2; }

    // types:
    typedef _Tp value_type;
    typedef unsigned long size_type;
    typedef typename __make_vector<value_type, size()>::type native_vector_type;
    typedef typename __comparison_result<value_type, size()>::type comparison_result_type;
    typedef typename __shuffle_mask<value_type, size()>::type shuffle_mask_type;

    // simd vector:
    native_vector_type __v_; // treat it as if private

    // subscripts:
    value_type operator[](size_type __i) const noexcept { return __v_[__i]; }

    // initialization:
#if !defined(VECTOR_KIT_NO_SIMD)
    Vector(const native_vector_type& v) noexcept : __v_(v) {}
    Vector(value_type x, value_type y) noexcept { __v_ = {x, y}; }
#else
    Vector(value_type x, value_type y) noexcept { __v_[0] = x, __v_[1] = y; }
#endif
    explicit Vector(value_type x = value_type()) noexcept : Vector(x, x) {}
    explicit Vector(value_type const (&a)[size()]) noexcept : Vector(a[0], a[1]) {}

    // copy:
    Vector(const Vector&) noexcept = default;
    Vector& operator=(const Vector&) noexcept = default;
    template <class E>
    Vector(_VE<value_type, E> const& e) noexcept : Vector(e[0], e[1]) {}
    template <class E>
    Vector& operator=(_VE<value_type, E> const& e) noexcept {
        return *this = Vector(e);
    }

    // compound assignments:
#if !defined(VECTOR_KIT_NO_SIMD)
    // arithematic:
    Vector& operator +=(const Vector& v) noexcept { return __v_ += v.__v_, *this; }
    Vector& operator -=(const Vector& v) noexcept { return __v_ -= v.__v_, *this; }
    Vector& operator *=(const Vector& v) noexcept { return __v_ *= v.__v_, *this; }
    Vector& operator /=(const Vector& v) noexcept { return __v_ /= v.__v_, *this; }
    // modulus:
    Vector& operator %=(const Vector& v) noexcept { return __v_ %= v.__v_, *this; }
    // bitwise:
    Vector& operator &=(const Vector& v) noexcept { return __v_ &= v.__v_, *this; }
    Vector& operator |=(const Vector& v) noexcept { return __v_ |= v.__v_, *this; }
    Vector& operator ^=(const Vector& v) noexcept { return __v_ ^= v.__v_, *this; }
    Vector& operator<<=(const Vector& v) noexcept { return __v_<<= v.__v_, *this; }
    Vector& operator>>=(const Vector& v) noexcept { return __v_>>= v.__v_, *this; }
#else
    // arithematic:
    Vector& operator +=(const Vector& v) noexcept { return __v_[0] += v.__v_[0], __v_[1] += v.__v_[1], *this; }
    Vector& operator -=(const Vector& v) noexcept { return __v_[0] -= v.__v_[0], __v_[1] -= v.__v_[1], *this; }
    Vector& operator *=(const Vector& v) noexcept { return __v_[0] *= v.__v_[0], __v_[1] *= v.__v_[1], *this; }
    Vector& operator /=(const Vector& v) noexcept { return __v_[0] /= v.__v_[0], __v_[1] /= v.__v_[1], *this; }
    // modulus:
    Vector& operator %=(const Vector& v) noexcept { return __v_[0] %= v.__v_[0], __v_[1] %= v.__v_[1], *this; }
    // bitwise:
    Vector& operator &=(const Vector& v) noexcept { return __v_[0] &= v.__v_[0], __v_[1] &= v.__v_[1], *this; }
    Vector& operator |=(const Vector& v) noexcept { return __v_[0] |= v.__v_[0], __v_[1] |= v.__v_[1], *this; }
    Vector& operator ^=(const Vector& v) noexcept { return __v_[0] ^= v.__v_[0], __v_[1] ^= v.__v_[1], *this; }
    Vector& operator<<=(const Vector& v) noexcept { return __v_[0]<<= v.__v_[0], __v_[1]<<= v.__v_[1], *this; }
    Vector& operator>>=(const Vector& v) noexcept { return __v_[0]>>= v.__v_[0], __v_[1]>>= v.__v_[1], *this; }
#endif
};

#pragma mark Vector<4>
/**
 @brief Four-element vector specialization.
*/
template <class _Tp>
struct Vector<_Tp, 4> : public _VE<_Tp, Vector<_Tp, 4> > {
    // capacity:
    static constexpr long size() noexcept { return 4; }

    // types:
    typedef _Tp value_type;
    typedef unsigned long size_type;
    typedef typename __make_vector<value_type, size()>::type native_vector_type;
    typedef typename __comparison_result<_Tp, size()>::type comparison_result_type;
    typedef typename __shuffle_mask<_Tp, size()>::type shuffle_mask_type;

    // simd vector:
    native_vector_type __v_; // treat it as if private

    // subscripts:
    value_type operator[](size_type __i) const noexcept { return __v_[__i]; }

    // initialization:
#if !defined(VECTOR_KIT_NO_SIMD)
    Vector(const native_vector_type& v) noexcept : __v_(v) {}
    Vector(value_type x, value_type y, value_type z, value_type w) noexcept { __v_ = {x, y, z, w}; }
#else
    Vector(value_type x, value_type y, value_type z, value_type w) noexcept {
        __v_[0] = x, __v_[1] = y, __v_[2] = z, __v_[3] = w;
    }
#endif
    explicit Vector(value_type x = value_type()) noexcept : Vector(x, x, x, x) {}
    explicit Vector(value_type const (&a)[size()]) noexcept : Vector(a[0], a[1], a[2], a[3]) {}

    // copy:
    Vector(const Vector&) noexcept = default;
    Vector& operator=(const Vector&) noexcept = default;
    template <class E>
    Vector(_VE<value_type, E> const& e) noexcept : Vector(e[0], e[1], e[2], e[3]) {}
    template <class E>
    Vector& operator=(_VE<value_type, E> const& e) noexcept {
        return *this = Vector(e);
    }

    // compound assignments:
#if !defined(VECTOR_KIT_NO_SIMD)
    // arithematic:
    Vector& operator +=(const Vector& v) noexcept { return __v_ += v.__v_, *this; }
    Vector& operator -=(const Vector& v) noexcept { return __v_ -= v.__v_, *this; }
    Vector& operator *=(const Vector& v) noexcept { return __v_ *= v.__v_, *this; }
    Vector& operator /=(const Vector& v) noexcept { return __v_ /= v.__v_, *this; }
    // modulus:
    Vector& operator %=(const Vector& v) noexcept { return __v_ %= v.__v_, *this; }
    // bitwise:
    Vector& operator &=(const Vector& v) noexcept { return __v_ &= v.__v_, *this; }
    Vector& operator |=(const Vector& v) noexcept { return __v_ |= v.__v_, *this; }
    Vector& operator ^=(const Vector& v) noexcept { return __v_ ^= v.__v_, *this; }
    Vector& operator<<=(const Vector& v) noexcept { return __v_<<= v.__v_, *this; }
    Vector& operator>>=(const Vector& v) noexcept { return __v_>>= v.__v_, *this; }
#else
    // arithematic:
    Vector& operator +=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] += v.__v_[i]; return *this; }
    Vector& operator -=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] -= v.__v_[i]; return *this; }
    Vector& operator *=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] *= v.__v_[i]; return *this; }
    Vector& operator /=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] /= v.__v_[i]; return *this; }
    // modulus:
    Vector& operator %=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] %= v.__v_[i]; return *this; }
    // bitwise:
    Vector& operator &=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] &= v.__v_[i]; return *this; }
    Vector& operator |=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] |= v.__v_[i]; return *this; }
    Vector& operator ^=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] ^= v.__v_[i]; return *this; }
    Vector& operator<<=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i]<<= v.__v_[i]; return *this; }
    Vector& operator>>=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i]>>= v.__v_[i]; return *this; }
#endif
};

// generic:
#pragma mark Vector<8>
template <class _Tp>
struct Vector<_Tp, 8> : public _VE<_Tp, Vector<_Tp, 8> > {
    // capacity:
    static constexpr long size() noexcept { return 8; }

    // types:
    typedef _Tp value_type;
    typedef unsigned long size_type;
    typedef typename __make_vector<value_type, size()>::type native_vector_type;
    typedef typename __comparison_result<_Tp, size()>::type comparison_result_type;
    typedef typename __shuffle_mask<_Tp, size()>::type shuffle_mask_type;

    // simd vector:
    native_vector_type __v_; // treat it as if private

    // subscripts:
    value_type operator[](size_type __i) const noexcept { return __v_[__i]; }

    // initialization:
#if !defined(VECTOR_KIT_NO_SIMD)
    Vector(const native_vector_type& v) noexcept : __v_(v) {}
    Vector(value_type x, value_type y, value_type z, value_type w,
           value_type a, value_type b, value_type c, value_type d) noexcept {
        __v_ = {x, y, z, w, a, b, c, d};
    }
#else
    Vector(value_type x, value_type y, value_type z, value_type w,
           value_type a, value_type b, value_type c, value_type d) noexcept {
        __v_[0] = x, __v_[1] = y, __v_[2] = z, __v_[3] = w, __v_[4] = a, __v_[5] = b, __v_[6] = c, __v_[7] = d;
    }
#endif
    explicit Vector(value_type x = value_type()) noexcept : Vector(x, x, x, x, x, x, x, x) {}
    explicit Vector(value_type const (&a)[size()]) noexcept : Vector(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]) {}

    // copy:
    Vector(const Vector&) noexcept = default;
    Vector& operator=(const Vector&) noexcept = default;
    template <class E>
    Vector(_VE<value_type, E> const& e) noexcept : Vector(e[0], e[1], e[2], e[3], e[4], e[5], e[6], e[7]) {}
    template <class E>
    Vector& operator=(_VE<value_type, E> const& e) noexcept {
        return *this = Vector(e);
    }

    // compound assignments:
#if !defined(VECTOR_KIT_NO_SIMD)
    // arithematic:
    Vector& operator +=(const Vector& v) noexcept { return __v_ += v.__v_, *this; }
    Vector& operator -=(const Vector& v) noexcept { return __v_ -= v.__v_, *this; }
    Vector& operator *=(const Vector& v) noexcept { return __v_ *= v.__v_, *this; }
    Vector& operator /=(const Vector& v) noexcept { return __v_ /= v.__v_, *this; }
    // modulus:
    Vector& operator %=(const Vector& v) noexcept { return __v_ %= v.__v_, *this; }
    // bitwise:
    Vector& operator &=(const Vector& v) noexcept { return __v_ &= v.__v_, *this; }
    Vector& operator |=(const Vector& v) noexcept { return __v_ |= v.__v_, *this; }
    Vector& operator ^=(const Vector& v) noexcept { return __v_ ^= v.__v_, *this; }
    Vector& operator<<=(const Vector& v) noexcept { return __v_<<= v.__v_, *this; }
    Vector& operator>>=(const Vector& v) noexcept { return __v_>>= v.__v_, *this; }
#else
    // arithematic:
    Vector& operator +=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] += v.__v_[i]; return *this; }
    Vector& operator -=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] -= v.__v_[i]; return *this; }
    Vector& operator *=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] *= v.__v_[i]; return *this; }
    Vector& operator /=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] /= v.__v_[i]; return *this; }
    // modulus:
    Vector& operator %=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] %= v.__v_[i]; return *this; }
    // bitwise:
    Vector& operator &=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] &= v.__v_[i]; return *this; }
    Vector& operator |=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] |= v.__v_[i]; return *this; }
    Vector& operator ^=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i] ^= v.__v_[i]; return *this; }
    Vector& operator<<=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i]<<= v.__v_[i]; return *this; }
    Vector& operator>>=(const Vector& v) noexcept { for (size_type i = 0; i < size(); ++i) __v_[i]>>= v.__v_[i]; return *this; }
#endif
};
                        
// output stream:
template<class _CharT, class _Traits, class _Tp, long _Size>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& __os, Vector<_Tp, _Size> const& __v) {
    std::basic_ostringstream<_CharT, _Traits> __s; {
        __s.flags(__os.flags());
        __s.imbue(__os.getloc());
        __s.precision(__os.precision());
    }
    unsigned long i = 0;
    { __s << "(" << __v[i++]; while (i < _Size) __s << ", " << __v[i++]; __s << ")"; }
    return __os << __s.str();
}
                        
} // namespace SIMD
} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* SIMDVector_h */
