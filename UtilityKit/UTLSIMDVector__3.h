//
//  UTLSIMDVector__3.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/28/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMDVector__3_h
#define UTLSIMDVector__3_h

#include <UtilityKit/UTLSIMDVector__0.h>

// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK: SIMDVector
    //
    /**
     @brief Specialization for 3 elements.
     @discussion Storage for 4 elements are reserved, so vector operations are done with a 4-element SIMDVector.
     this->size() returns 3, but this->max_size() returns 4.
     Likewise, this->dims() returns {3}, but this->max_dims() returns {4}.
     */
    template <class Type>
    struct alignas(SIMDVector<Type, 4L>) SIMDVector<Type, 3L> : public Vector<Type, 3L> {
        static_assert(std::is_arithmetic<Type>::value && !std::is_same<Type, bool>::value, "Type should be one of arithematic types excluding bool");

        // types:
        using vector_type            = Vector<Type, 3L>;
        using value_type             = typename vector_type::value_type;
        using reference              = typename vector_type::reference;
        using const_reference        = typename vector_type::const_reference;
        using pointer                = typename vector_type::pointer;
        using const_pointer          = typename vector_type::const_pointer;
        using iterator               = typename vector_type::iterator;
        using const_iterator         = typename vector_type::const_iterator;
        using reverse_iterator       = typename vector_type::reverse_iterator;
        using const_reverse_iterator = typename vector_type::const_reverse_iterator;
        using size_type              = typename vector_type::size_type;
        using difference_type        = typename vector_type::difference_type;
        using size_vector_type       = typename vector_type::size_vector_type;
        using comparison_result_type = SIMDVector<typename _comparison_result<Type>::type, vector_type::size()>;

        // capacity:
        static constexpr size_type max_size() noexcept { return size_type{4}; }
        static constexpr size_vector_type max_dims() noexcept { return {max_size()}; }

        // constructors:
        constexpr explicit SIMDVector() noexcept : vector_type() {}
        explicit SIMDVector(Type const& a) noexcept : vector_type(a) {}
        explicit SIMDVector(vector_type const& v) noexcept : vector_type(v) {}
        explicit SIMDVector(Type const& x, Type const& y, Type const& z) noexcept : vector_type(x, y, z) {}

        // copy/move:
        constexpr SIMDVector(SIMDVector const& o) noexcept : vector_type(o) {}
        SIMDVector& operator=(SIMDVector const& o) noexcept { **this = *o; return *this; }
        SIMDVector& operator=(vector_type const& v) noexcept { vector_type::operator=(v); return *this; }
        SIMDVector& operator=(Type const& a) noexcept { vector_type::fill(a); return *this; }
        template <class U, typename std::enable_if<std::is_convertible<U, Type>::value, long>::type = 0L>
        explicit SIMDVector(Vector<U, vector_type::size()> const& o) noexcept : vector_type(o) {}
        template <class U>
        auto operator=(Vector<U, vector_type::size()> const& o) noexcept -> typename std::enable_if<std::is_convertible<U, Type>::value, SIMDVector>::type &{ return *this = SIMDVector{o}; }

        // compound arithmetic:
        inline SIMDVector &operator+=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator+=(vector_type const &rhs) noexcept { return *this += SIMDVector{rhs}; }
        inline SIMDVector &operator+=(Type const &rhs) noexcept { return *this += SIMDVector{rhs}; }
        inline SIMDVector &operator-=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator-=(vector_type const &rhs) noexcept { return *this -= SIMDVector{rhs}; }
        inline SIMDVector &operator-=(Type const &rhs) noexcept { return *this -= SIMDVector{rhs}; }
        inline SIMDVector &operator*=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator*=(vector_type const &rhs) noexcept { return *this *= SIMDVector{rhs}; }
        inline SIMDVector &operator*=(Type const &rhs) noexcept { return *this *= SIMDVector{rhs}; }
        inline SIMDVector &operator/=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator/=(vector_type const &rhs) noexcept { return *this /= SIMDVector{rhs}; }
        inline SIMDVector &operator/=(Type const &rhs) noexcept { return *this /= SIMDVector{rhs}; }
        // compound modulus:
        inline SIMDVector &operator%=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator%=(vector_type const &rhs) noexcept { return *this %= SIMDVector{rhs}; }
        inline SIMDVector &operator%=(Type const &rhs) noexcept { return *this %= SIMDVector{rhs}; }
        // compound bit operators:
        inline SIMDVector &operator&=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator&=(vector_type const &rhs) noexcept { return *this &= SIMDVector{rhs}; }
        inline SIMDVector &operator&=(Type const &rhs) noexcept { return *this &= SIMDVector{rhs}; }
        inline SIMDVector &operator|=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator|=(vector_type const &rhs) noexcept { return *this |= SIMDVector{rhs}; }
        inline SIMDVector &operator|=(Type const &rhs) noexcept { return *this |= SIMDVector{rhs}; }
        inline SIMDVector &operator^=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator^=(vector_type const &rhs) noexcept { return *this ^= SIMDVector{rhs}; }
        inline SIMDVector &operator^=(Type const &rhs) noexcept { return *this ^= SIMDVector{rhs}; }
        inline SIMDVector &operator<<=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator<<=(vector_type const &rhs) noexcept { return *this <<= SIMDVector{rhs}; }
        inline SIMDVector &operator<<=(Type const &rhs) noexcept { return *this <<= SIMDVector{rhs}; }
        inline SIMDVector &operator>>=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator>>=(vector_type const &rhs) noexcept { return *this >>= SIMDVector{rhs}; }
        inline SIMDVector &operator>>=(Type const &rhs) noexcept { return *this >>= SIMDVector{rhs}; }
        // unary:
        SIMDVector const &operator+() const noexcept { return *this; }
        SIMDVector        operator-() const noexcept { SIMDVector m{}; return m -= *this; }
        // comparison:
        // If true, all bits are set (i.e., -1)
        inline comparison_result_type operator==(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator!=(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator<=(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator>=(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator<(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator>(SIMDVector const &rhs) const noexcept;

    private:
        constexpr static Type one{1};

        Type _dummy{one}; // should be filled with 1 to avoid division/modulus by 0 and others

        using simd_type = typename MakeSIMD<Type, max_size()>::type;
        simd_type      & operator*()      & noexcept { return *reinterpret_cast<simd_type      *>(this); }
        simd_type const& operator*() const& noexcept { return *reinterpret_cast<simd_type const*>(this); }
        template <class T, long S> friend struct SIMDVector;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLSIMDVector__3.hh>

#endif /* UTLSIMDVector__3_h */
