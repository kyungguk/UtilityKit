//
//  UTLTrapzIntegrator.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/22/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLTrapzIntegrator_hh
#define UTLTrapzIntegrator_hh

// MARK:- Version 4
//
#include <tuple> // std::tie
#include <cmath>

template <class X>
template <class F/*Y(X)*/, class Norm/*X(Y)*/>
auto UTL::__4_::TrapzIntegrator<X>::qtrap(F&& f, X const a, X const b, Norm&& norm) const
-> std::pair<typename std::decay<decltype(f(a))>::type, unsigned> {
    using result_type = typename std::decay<decltype(f(a))>::type;
    result_type s;
    unsigned n, i;
    { // init
        std::tie(s, n) = _trapzd(f, a, b);
    }
    for (i = 1; i < _min_recursion; ++i) { // min iterations
        std::tie(s, n) = _trapzd(f, a, b, s, n);
    }
    for (; i <= _max_recursion; ++i) { // conversion test
        result_type const sold = s;
        std::tie(s, n) = _trapzd(f, a, b, s, n);
        //
        if (norm(sold + s) < 2*_tolerance || norm(sold - s) < norm(sold)*_tolerance) {
            return {s, i};
        }
    }

    // not converged
    return {s, i};
}

template <class X>
template <class F/*Y(X)*/, class Norm/*X(Y)*/>
auto UTL::__4_::TrapzIntegrator<X>::qsimp(F&& f, X const a, X const b, Norm&& norm) const
-> std::pair<typename std::decay<decltype(f(a))>::type, unsigned> {
    using result_type = typename std::decay<decltype(f(a))>::type;
    constexpr X _4_3 = X{4}/3, m1_3 = -X{1}/3; // this should be scalar because result_type can be complex for which multiplication is not element-wise
    result_type st, s;
    unsigned n, i;
    { // init
        std::tie(st, n) = _trapzd(f, a, b);
    }
    for (i = 1; i < _min_recursion; ++i) { // min iterations
        std::tie(st, n) = _trapzd(f, a, b, st, n);
    }
    for (s = st; i <= _max_recursion; ++i) { // conversion test
        result_type const sold{s};
        s = m1_3*st;
        std::tie(st, n) = _trapzd(f, a, b, st, n);
        s += _4_3*st;
        //
        if (norm(sold + s) < 2*_tolerance || norm(sold - s) < norm(sold)*_tolerance) {
            return {s, i};
        }
    }

    // not converged
    return {s, i};
}

template <class X>
template <class XIt, class YIt>
typename std::iterator_traits<YIt>::value_type UTL::__4_::TrapzIntegrator<X>::trapzd(XIt x_first, XIt x_last, YIt y_first)
{
    static_assert(std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<XIt>::iterator_category>::value, "XIt should be forward iterator");
    static_assert(std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<YIt>::iterator_category>::value, "YIt should be forward iterator");
    using result_type = typename std::iterator_traits<YIt>::value_type;
    result_type sum{};
    if (x_first != x_last) {
        XIt x1 = x_first, x0 = x1++;
        YIt y1 = y_first, y0 = y1++;
        for (; x1 != x_last; x0 = x1++, y0 = y1++) {
            sum += _trapzd(std::make_pair(*x0, *x1), std::make_pair(*y0, *y1));
        }
    }
    return sum;
}

#endif /* UTLTrapzIntegrator_hh */
