//
//  UTLNewtonRootFinder.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLNewtonRootFinder_h
#define UTLNewtonRootFinder_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLOptional.h>
#include <utility>
#include <algorithm>
#include <cmath>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Newton's root finder.
     @discussion x_n+1 = x_n - m*f(x_n)/f'(x_n), where m is the multiplicity.
     The template parameter F_dF should take a value of type double and return f(x_n)/f'(x_n) convertible to double type.
     */
    template <class F_dF>
    struct NewtonRootFinder {
        using value_type = double;
        using optional_type = Optional<value_type>;

    private:
        F_dF _f_df;
    public:
        value_type reltol{1e-5}; //!< Relative tolerance.
        value_type abstol{1e-10}; //!< Absolute tolerance.
        unsigned max_iterations{5}; //!< Maximum number of iterations.
        unsigned multiplicity{1}; //!< Multiplicity; default is 1.

        /**
         @brief Construct with a function to solve.
         @param f An instance of type F_dF representing f(x)/(df/dx), where f(x) is zero at the root (it should be copy-constructable).
         */
        explicit NewtonRootFinder(F_dF f) : _f_df(f) {}

        /**
         @brief Find a root of `f(x) = 0` whose derivative is known.
         @discussion std::isfinite is checked against the function values, so the function object can return NaN to indicate evaluation failure.

         The StepMonitor should be a callable object with signiture `bool(unsigned, value_type)`, where the two arguments are the current iteration count and the root estimate, respectively.
         Returning false upon call exit indicates immediate termination.

         @param x0 An initial guess.
         @return A root wrapped around the optional if found any.
         An empty optional is returned when the convergence failed within max_iterations, when the root is not finite, or when the step monitor returns false.
         */
        template <class StepMonitor>
        optional_type operator()(value_type x0, StepMonitor&& monitor) const;

        /**
         @brief Find a root without monitoring iteration steps.
         */
        optional_type operator()(value_type x0) const {
            return (*this)(x0, &_null_monitor);
        }

    private:
        static bool _null_monitor(decltype(max_iterations) const&, value_type const&) noexcept {
            return true;
        }
    };

    // MARK: Out-of-line implementations
    template <class F_dF>
    template <class StepMonitor>
    auto NewtonRootFinder<F_dF>::operator()(value_type x0, StepMonitor&& monitor) const
    ->optional_type {
        if (!std::isfinite(x0)) return {};

        value_type x1 = x0;
        for (decltype(max_iterations) i = 0; i < max_iterations && monitor(i, x1); ++i) {
            x1 = x0 - multiplicity*_f_df(x0);
            value_type const h = x1 - x0;
            if (!std::isfinite(h)) {
                return {};
            } else if (std::abs(h) < abstol || std::abs(h) < reltol*std::max(std::abs(x1), std::abs(x0))) {
                return x1;
            } else {
                x0 = x1;
            }
        }

        return {};
    }

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLOptional.h>
#include <type_traits>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class RealType>
    struct NewtonRootFinder;

    /**
     @brief Newton's root finder.
     @discussion x_n+1 = x_n - m*f(x_n)/f'(x_n), where m is the multiplicity.
     */
    template <>
    struct NewtonRootFinder<double> {
        using value_type = double;
        using optional_type = Optional<value_type>;

    public:
        value_type reltol{1e-5}; //!< Relative tolerance.
        value_type abstol{1e-10}; //!< Absolute tolerance.
        unsigned max_iterations{5}; //!< Maximum number of iterations.
        unsigned multiplicity{1}; //!< Multiplicity; default is 1.

        /**
         @brief Find a real root of a function, f(x), whose derivative, df(x), is known.
         @discussion std::isfinite is checked against the function values, so the function object can return NaN to indicate evaluation failure.

         The StepMonitor should be a callable object with signiture `bool(unsigned, value_type)`, where the two arguments are the current iteration count and the root estimate, respectively.
         Returning false indicates an immediate termination.

         @param f_over_df A function of signature, `value_type(value_type)`, that returns f(x)/df(x).
         @param x0 An initial guess.
         @param monitor A step monitor callback of signature, `bool(unsigned, value_type)`.

         @return An optional wrapping the root, if found any.
         An empty optional is returned when the convergence failed within max_iterations, when the root is not finite, or when the step monitor returns false.
         */
        template <class F_dF, class StepMonitor>
        inline optional_type operator()(F_dF&& f_over_df, value_type x0, StepMonitor&& monitor) const;

        /**
         @brief Find a root without monitoring iteration steps.
         */
        template <class F_dF>
        optional_type operator()(F_dF&& f_df, value_type const x0) const {
            return (*this)(std::forward<F_dF>(f_df), x0, &null_monitor);
        }

    private:
        static bool null_monitor(unsigned, value_type) noexcept { return true; }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLNewtonRootFinder.hh>

#endif /* UTLNewtonRootFinder_h */
