//
//  UTLArray.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/10/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArray_h
#define UTLArray_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <iterator>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief A wrapper for contiguous sequence of things.
     */
    template <class Type>
    class Array { // TODO: Maybe Sequence be more appropriate?
    public:
        // types:
        using value_type             = Type;
        using reference              = Type&;
        using const_reference        = Type const&;
        using pointer                = Type*;
        using const_pointer          = Type const*;
        using iterator               = pointer;
        using const_iterator         = const_pointer;
        using reverse_iterator       = std::reverse_iterator<iterator>;
        using const_reverse_iterator = std::reverse_iterator<const_iterator>;
        using size_type              = long;
        using difference_type        = long;

    protected:
        pointer _beg{nullptr};
        pointer _end{nullptr};

        explicit Array() noexcept {}
        Array(Array const& o) = delete;
        Array(Array&& o) noexcept : Array(o._beg, o._end) { o._beg = o._end = nullptr; }

    public:
        // destructor:
        virtual ~Array() { _beg = _end = nullptr; }

        /**
         @brief Wraps a contiguous memory block defined by [first, last).
         */
        Array(pointer first, pointer last) noexcept : _beg(first), _end(last) {}

        /**
         @brief Wraps a contiguous memory block of length n starting at p.
         */
        Array(pointer p, size_type n) noexcept : Array(p, p + n) {}

        /**
         @brief Element-wise copy assignment.
         @exception When *this and other are not of the same size or when element copy throws.
         */
        Array& operator=(Array const& o);
        /**
         @brief Element-wise move assignment.
         @exception When *this and other are not of the same size or when element move throws.
         */
        Array& operator=(Array&& o);

        // capacity:
        bool     empty() const noexcept { return _beg == _end; }
        size_type size() const noexcept { return size_type{_end - _beg}; }

        // iterators:
        iterator        begin()       noexcept { return iterator{_beg}; }
        const_iterator  begin() const noexcept { return const_iterator{_beg}; }
        const_iterator cbegin() const noexcept { return this->begin(); }
        iterator          end()       noexcept { return iterator{_end}; }
        const_iterator    end() const noexcept { return const_iterator{_end}; }
        const_iterator   cend() const noexcept { return this->end(); }

        reverse_iterator        rbegin()       noexcept { return reverse_iterator{this->end()}; }
        const_reverse_iterator  rbegin() const noexcept { return const_reverse_iterator{this->end()}; }
        const_reverse_iterator crbegin() const noexcept { return this->rbegin(); }
        reverse_iterator          rend()       noexcept { return reverse_iterator{this->begin()}; }
        const_reverse_iterator    rend() const noexcept { return const_reverse_iterator{this->begin()}; }
        const_reverse_iterator   crend() const noexcept { return this->rend(); }

        // element access:
        virtual Type      & at(size_type const i)      ;
        virtual Type const& at(size_type const i) const;

#if defined(DEBUG)
        Type      & operator[](size_type const i)       { return this->at(i); }
        Type const& operator[](size_type const i) const { return this->at(i); }
#else
        Type      & operator[](size_type const i)       noexcept { return *(_beg + i); }
        Type const& operator[](size_type const i) const noexcept { return *(_beg + i); }
#endif

        Type      & front()       noexcept { return *_beg; }
        Type const& front() const noexcept { return *_beg; }
        Type      &  back()       noexcept { return *(_end - 1); }
        Type const&  back() const noexcept { return *(_end - 1); }

        // modifier:
        /**
         @brief Group those that satisfy the predicate to the right side of the array.
         @discussion The order is not preserved.
         @return Iterator to the first element of the right side group (those that satisfy the predicate).
         */
        template <class Predicate/*bool(const_reference)*/>
        inline iterator shuffle_if(Predicate&& pred) noexcept(noexcept(std::swap(*_beg, *_end)));
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLArray.hh>

#endif /* UTLArray_h */
