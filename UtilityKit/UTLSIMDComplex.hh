//
//  UTLSIMDComplex.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMDComplex_hh
#define UTLSIMDComplex_hh

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <utility>
#include <ostream>
#include <cmath>

namespace std {
    // MARK: std::swap
    //
    template <class Type>
    inline void swap(UTL::__4_::SIMDComplex<Type> &a, UTL::__4_::SIMDComplex<Type> &b) noexcept {
        a.swap(b);
    }

    // MARK: 26.3.7 values:
    //
    template <class T> inline
    T real(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return z.real();
    }
    template <class T> inline
    T imag(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return z.imag();
    }

    template <class T> inline
    T abs(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        UTL::__4_::SIMDVector<T, 2> v{z.real(), z.imag()};
        v *= v;
        return std::sqrt(v.x += v.y);
    }
    template <class T> inline
    T arg(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return arg(static_cast<std::complex<T> const&>(z));
    }

    template <class T> inline
    UTL::__4_::SIMDComplex<T> conj(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::conj(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> proj(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::proj(static_cast<std::complex<T> const&>(z))};
    }

    // MARK: 26.3.8 transcendentals:
    //
    template <class T> inline
    UTL::__4_::SIMDComplex<T> sin(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::sin(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> cos(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::cos(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> tan(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::tan(static_cast<std::complex<T> const&>(z))};
    }

    template <class T> inline
    UTL::__4_::SIMDComplex<T> acos(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::acos(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> asin(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::asin(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> atan(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::atan(static_cast<std::complex<T> const&>(z))};
    }

    template <class T> inline
    UTL::__4_::SIMDComplex<T> sinh(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::sinh(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> cosh(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::cosh(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> tanh(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::tanh(static_cast<std::complex<T> const&>(z))};
    }

    template <class T> inline
    UTL::__4_::SIMDComplex<T> acosh(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::acosh(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> asinh(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::asinh(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> atanh(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::atanh(static_cast<std::complex<T> const&>(z))};
    }

    template <class T> inline
    UTL::__4_::SIMDComplex<T> exp(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::exp(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> log(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::log(static_cast<std::complex<T> const&>(z))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> log10(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::log10(static_cast<std::complex<T> const&>(z))};
    }

    template <class T> inline
    UTL::__4_::SIMDComplex<T> pow(const UTL::__4_::SIMDComplex<T>& z, const UTL::__4_::SIMDComplex<T>& n) noexcept {
        return {std::pow(static_cast<std::complex<T> const&>(z), static_cast<std::complex<T> const&>(n))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> pow(const UTL::__4_::SIMDComplex<T>& z, const T& n) noexcept {
        return {std::pow(static_cast<std::complex<T> const&>(z), n)};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> pow(const T& x, const UTL::__4_::SIMDComplex<T>& n) noexcept {
        return {std::pow(x, static_cast<std::complex<T> const&>(n))};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> pow(const UTL::__4_::SIMDComplex<T>& z, const std::complex<T>& n) noexcept {
        return {std::pow(static_cast<std::complex<T> const&>(z), n)};
    }
    template <class T> inline
    UTL::__4_::SIMDComplex<T> pow(const std::complex<T>& x, const UTL::__4_::SIMDComplex<T>& n) noexcept {
        return {std::pow(x, static_cast<std::complex<T> const&>(n))};
    }

    template <class T> inline
    UTL::__4_::SIMDComplex<T> sqrt(const UTL::__4_::SIMDComplex<T>& z) noexcept {
        return {std::sqrt(static_cast<std::complex<T> const&>(z))};
    }
}

// MARK: Non-buffered Output Stream
//
template <class _CharT, class _Traits, class T>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::SIMDComplex<T> const& z) {
    return os << static_cast<std::complex<T> const&>(z);
}

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK: 26.3.6 operators:
    //
    template <class T> inline
    const SIMDComplex<T>& operator+(const SIMDComplex<T>& z) noexcept {
        return z;
    }
    template <class T> inline
    SIMDComplex<T> operator-(const SIMDComplex<T>& z) noexcept {
        return SIMDComplex<T>{-z.real(), -z.imag()};
    }
    
    template <class T> inline
    SIMDComplex<T> operator+(SIMDComplex<T> a, const SIMDComplex<T>& b) noexcept {
        return a += b;
    }
    template <class T> inline
    SIMDComplex<T> operator+(SIMDComplex<T> a, const T& b) noexcept {
        return a += b;
    }
    template <class T> inline
    SIMDComplex<T> operator+(const T& a, SIMDComplex<T> b) noexcept {
        return b += a;
    }
    template <class T> inline
    SIMDComplex<T> operator+(SIMDComplex<T> a, const std::complex<T>& b) noexcept {
        return a += b;
    }
    template <class T> inline
    SIMDComplex<T> operator+(const std::complex<T>& a, SIMDComplex<T> b) noexcept {
        return b += a;
    }
    
    template <class T> inline
    SIMDComplex<T> operator-(SIMDComplex<T> a, const SIMDComplex<T>& b) noexcept {
        return a -= b;
    }
    template <class T> inline
    SIMDComplex<T> operator-(SIMDComplex<T> a, const T& b) noexcept {
        return a -= b;
    }
    template <class T> inline
    SIMDComplex<T> operator-(const T& a, const SIMDComplex<T>& b) noexcept {
        return SIMDComplex<T>{a} -= b;
    }
    template <class T> inline
    SIMDComplex<T> operator-(SIMDComplex<T> a, const std::complex<T>& b) noexcept {
        return a -= b;
    }
    template <class T> inline
    SIMDComplex<T> operator-(const std::complex<T>& a, const SIMDComplex<T>& b) noexcept {
        return SIMDComplex<T>{a} -= b;
    }
    
    template <class T> inline
    SIMDComplex<T> operator*(SIMDComplex<T> a, const SIMDComplex<T>& b) noexcept {
        return a *= b;
    }
    template <class T> inline
    SIMDComplex<T> operator*(SIMDComplex<T> a, const T& b) noexcept {
        return a *= b;
    }
    template <class T> inline
    SIMDComplex<T> operator*(const T& a, SIMDComplex<T> b) noexcept {
        return b *= a;
    }
    template <class T> inline
    SIMDComplex<T> operator*(SIMDComplex<T> a, const std::complex<T>& b) noexcept {
        return a *= b;
    }
    template <class T> inline
    SIMDComplex<T> operator*(const std::complex<T>& a, SIMDComplex<T> b) noexcept {
        return b *= a;
    }
    
    template <class T> inline
    SIMDComplex<T> operator/(SIMDComplex<T> a, const SIMDComplex<T>& b) noexcept {
        return a /= b;
    }
    template <class T> inline
    SIMDComplex<T> operator/(SIMDComplex<T> a, const T& b) noexcept {
        return a /= b;
    }
    template <class T> inline
    SIMDComplex<T> operator/(const T& a, const SIMDComplex<T>& b) noexcept {
        return SIMDComplex<T>{a} /= b;
    }
    template <class T> inline
    SIMDComplex<T> operator/(SIMDComplex<T> a, const std::complex<T>& b) noexcept {
        return a /= b;
    }
    template <class T> inline
    SIMDComplex<T> operator/(const std::complex<T>& a, const SIMDComplex<T>& b) noexcept {
        return SIMDComplex<T>{a} /= b;
    }
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLSIMDComplex_hh */
