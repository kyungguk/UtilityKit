//
//  UTLSema.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/24/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLSema.h"
#include "UTLPrinto.h"

// MARK: Version 4
//
#include <stdexcept>
#include <string>

// MARK:- Sema<Mutex>
//
UTL::__4_::Sema<UTL::__4_::Mutex>::~Sema<UTL::__4_::Mutex>()
{
}
UTL::__4_::Sema<UTL::__4_::Mutex>::Sema(critical_type const crit)
: _cv(), _mx(), _crit() {
    _crit = crit >= 0 ? crit : throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative value");
}

void UTL::__4_::Sema<UTL::__4_::Mutex>::wait()
{
    LockG<Mutex> _{_mx};
    while (!_crit) {
        _cv.wait(_mx);
    }
    --_crit;
}

void UTL::__4_::Sema<UTL::__4_::Mutex>::post()
{
    LockG<Mutex> _{_mx};
    if (!_crit++) {
        _cv.notify_all();
    }
}


// MARK:- Sema<SpinLock>
//
UTL::__4_::Sema<UTL::__4_::SpinLock>::~Sema<UTL::__4_::SpinLock>()
{
}

UTL::__4_::Sema<UTL::__4_::SpinLock>::Sema(critical_type const crit)
: _crit(), _consumer_q(), _key() {
    _crit = crit >= 0 ? crit : throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - negative value");
}

void UTL::__4_::Sema<UTL::__4_::SpinLock>::wait()
{
    LockG<SpinLock> _{_key};
    while (!_crit) {
        SpinLock consumer{SpinLock::Locked};
        _consumer_q.push(&consumer);
        _key.unlock();
        consumer.lock(true);
        _key.lock(); // so that access to _crit is lock guarded
    }
    --_crit;
}

void UTL::__4_::Sema<UTL::__4_::SpinLock>::post()
{
    LockG<SpinLock> _{_key};
    if (!_crit++ && !_consumer_q.empty()) {
        _consumer_q.front()->unlock();
        _consumer_q.pop();
    }
}
