//
//  PaddedArray-Allocator.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/21/15.
//
//

#ifndef ArrayKit__PaddedArray_Allocator_h
#define ArrayKit__PaddedArray_Allocator_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//
#include <memory>
#include <utility>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
inline namespace Dynamic {

    /**
     @brief On-demain storage allocator.
     @discussion If the storage is given, *this do not perform alloc/dealloc and construct/destory tasks.
     If the storage is not given, calls are relayed to std::allocator.
     */
    template <class _Tp>
    class OndemandAllocator : private std::allocator<_Tp> {
        typedef std::allocator<_Tp> __base;
        typename __base::pointer __p_;
    public:
        typedef typename __base::size_type       size_type;
        typedef typename __base::difference_type difference_type;
        typedef typename __base::pointer         pointer;
        typedef typename __base::const_pointer   const_pointer;
        typedef typename __base::reference       reference;
        typedef typename __base::const_reference const_reference;
        typedef typename __base::value_type      value_type;

        // construct:
        explicit OndemandAllocator() noexcept : __base(), __p_( ) {}
        OndemandAllocator(pointer p) noexcept : __base(), __p_(p) {}

        // copy:
        OndemandAllocator(OndemandAllocator const&) noexcept = default;
        OndemandAllocator& operator=(OndemandAllocator const&) noexcept = default;

        // move:
        OndemandAllocator(OndemandAllocator&& a) noexcept : __p_(a.__p_) { a.__p_ = nullptr; }
        OndemandAllocator& operator=(OndemandAllocator&& a) noexcept { return std::swap(__p_, a.__p_), *this; }

        // alloc:
        pointer allocate(size_type n) { return __p_ ? __p_ : __base::allocate(n); }
        void deallocate(pointer p, size_type n) { if (!__p_) __base::deallocate(p, n); }
        template<class... Args>
        void construct(pointer p, Args&&... args) { if (!__p_) __base::construct(p, std::forward<Args>(args)...); }
        void destroy(pointer p) { if (!__p_) __base::destroy(p); }

        // capacity:
        using __base::max_size;

        // comparison:
        template <class A1, class A2> friend
        bool operator==(const OndemandAllocator<A1>& lhs, const OndemandAllocator<A2>& rhs);
        template <class A1, class A2> friend
        bool operator!=(const OndemandAllocator<A1>& lhs, const OndemandAllocator<A2>& rhs);
    };

    template <class A1, class A2> inline
    bool operator==(const OndemandAllocator<A1>& lhs, const OndemandAllocator<A2>& rhs) { return lhs.__p_ == rhs.__p_; }
    template <class A1, class A2> inline
    bool operator!=(const OndemandAllocator<A1>& lhs, const OndemandAllocator<A2>& rhs) { return lhs.__p_ != rhs.__p_; }

} // namespace Dynamic
} // namespace __3_
UTILITYKIT_END_NAMESPACE

#pragma mark - std::swap
namespace std {
    template <class _Tp> inline
    void swap(UTILITYKIT_NAMESPACE::__3_::Dynamic::OndemandAllocator<_Tp>& x,
              UTILITYKIT_NAMESPACE::__3_::Dynamic::OndemandAllocator<_Tp>& y) noexcept {
        x = std::move(y);
    }
}

#endif /* ArrayKit__PaddedArray_Allocator_h */
