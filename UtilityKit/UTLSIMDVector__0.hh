//
//  UTLSIMDVector__0.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/28/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMDVector__0_hh
#define UTLSIMDVector__0_hh

// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK:- Operator Overloadings
    //
    // MARK: Binary +
    template <class T, long S> inline
    SIMDVector<T, S> operator+(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a += b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator+(SIMDVector<T, S> a, T const& s) noexcept {
        return a += s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator+(T const& s, SIMDVector<T, S> a) noexcept {
        return a += s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator+(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
        return a += b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator+(Vector<T, S> const& b, SIMDVector<T, S> a) noexcept {
        return a += b;
    }
    // MARK: Binary -
    template <class T, long S> inline
    SIMDVector<T, S> operator-(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a -= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator-(SIMDVector<T, S> a, T const& s) noexcept {
        return a -= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator-(T const& s, SIMDVector<T, S> const& b) noexcept {
        return decltype(s - b){s} -= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator-(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
        return a -= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator-(Vector<T, S> const& v, SIMDVector<T, S> const& b) noexcept {
        return decltype(v - b){v} -= b;
    }
    // MARK: Binary *
    template <class T, long S> inline
    SIMDVector<T, S> operator*(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a *= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator*(SIMDVector<T, S> a, T const& s) noexcept {
        return a *= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator*(T const& s, SIMDVector<T, S> a) noexcept {
        return a *= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator*(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
        return a *= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator*(Vector<T, S> const& b, SIMDVector<T, S> a) noexcept {
        return a *= b;
    }
    // MARK: Binary /
    template <class T, long S> inline
    SIMDVector<T, S> operator/(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a /= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator/(SIMDVector<T, S> a, T const& s) noexcept {
        return a /= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator/(T const& s, SIMDVector<T, S> const& b) noexcept {
        return decltype(s / b){s} /= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator/(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
        return a /= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator/(Vector<T, S> const& v, SIMDVector<T, S> const& b) noexcept {
        return decltype(v / b){v} /= b;
    }
    // MARK: Binary %
    template <class T, long S> inline
    SIMDVector<T, S> operator%(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a %= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator%(SIMDVector<T, S> a, T const& s) noexcept {
        return a %= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator%(T const& s, SIMDVector<T, S> const& b) noexcept {
        return decltype(s % b){s} %= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator%(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
        return a %= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator%(Vector<T, S> const& v, SIMDVector<T, S> const& b) noexcept {
        return decltype(v % b){v} %= b;
    }
    // MARK: Binary &
    template <class T, long S> inline
    SIMDVector<T, S> operator&(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a &= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator&(SIMDVector<T, S> a, T const& s) noexcept {
        return a &= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator&(T const& s, SIMDVector<T, S> a) noexcept {
        return a &= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator&(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
        return a &= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator&(Vector<T, S> const& b, SIMDVector<T, S> a) noexcept {
        return a &= b;
    }
    // MARK: Binary |
    template <class T, long S> inline
    SIMDVector<T, S> operator|(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a |= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator|(SIMDVector<T, S> a, T const& s) noexcept {
        return a |= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator|(T const& s, SIMDVector<T, S> a) noexcept {
        return a |= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator|(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
        return a |= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator|(Vector<T, S> const& b, SIMDVector<T, S> a) noexcept {
        return a |= b;
    }
    // MARK: Binary ^
    template <class T, long S> inline
    SIMDVector<T, S> operator^(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a ^= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator^(SIMDVector<T, S> a, T const& s) noexcept {
        return a ^= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator^(T const& s, SIMDVector<T, S> a) noexcept {
        return a ^= s;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator^(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
        return a ^= b;
    }
    template <class T, long S> inline
    SIMDVector<T, S> operator^(Vector<T, S> const& b, SIMDVector<T, S> a) noexcept {
        return a ^= b;
    }
    // MARK: Binary <<
    template <class T, long S> inline
    SIMDVector<T, S> operator<<(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
        return a <<= b;
        }
        template <class T, long S> inline
        SIMDVector<T, S> operator<<(SIMDVector<T, S> a, T const& s) noexcept {
            return a <<= s;
        }
        template <class T, long S> inline
        SIMDVector<T, S> operator<<(T const& s, SIMDVector<T, S> const& b) noexcept {
            return decltype(s << b){s} <<= b;
        }
        template <class T, long S> inline
        SIMDVector<T, S> operator<<(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
            return a <<= b;
        }
        template <class T, long S> inline
        SIMDVector<T, S> operator<<(Vector<T, S> const& v, SIMDVector<T, S> const& b) noexcept {
            return decltype(v << b){v} <<= b;
        }
        // MARK: Binary >>
        template <class T, long S> inline
        SIMDVector<T, S> operator>>(SIMDVector<T, S> a, SIMDVector<T, S> const& b) noexcept {
            return a >>= b;
        }
        template <class T, long S> inline
        SIMDVector<T, S> operator>>(SIMDVector<T, S> a, T const& s) noexcept {
            return a >>= s;
        }
        template <class T, long S> inline
        SIMDVector<T, S> operator>>(T const& s, SIMDVector<T, S> const& b) noexcept {
            return decltype(s >> b){s} >>= b;
        }
        template <class T, long S> inline
        SIMDVector<T, S> operator>>(SIMDVector<T, S> a, Vector<T, S> const& b) noexcept {
            return a >>= b;
        }
        template <class T, long S> inline
        SIMDVector<T, S> operator>>(Vector<T, S> const& v, SIMDVector<T, S> const& b) noexcept {
            return decltype(v >> b){v} >>= b;
        }
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLSIMDVector__0_hh */
