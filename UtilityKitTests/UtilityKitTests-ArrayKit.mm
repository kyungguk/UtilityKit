//
//  UtilityKitTests-ArrayKit.m
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#import <XCTest/XCTest.h>

// MARK: Version 3
//
#import <UtilityKit/UTLPrinto.h>
#import <UtilityKit/ArrayKit.h>
#import <array>
#import <vector>
#import <iostream>
#import <string>
#import <memory>

@interface UtilityKitTests_ArrayKit__3_ : XCTestCase

@end

@implementation UtilityKitTests_ArrayKit__3_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPaddedArray {
    using UTL::__3_::Aggregate::PaddedArray;

    // zero paddings:
    @autoreleasepool {
        constexpr long sz = 3, pad = 0;
        PaddedArray<int, sz, pad> a{{1, 2, 3}};
        PaddedArray<int, sz, pad> const& const_a = a;
        printo(std::cout, a, '\n');

        XCTAssert(sz == const_a.size() && pad == const_a.pad_size() && sz+2*pad == const_a.max_size(),
                  @"a.size() = %ld, a.pad_size() = %ld, a.max_size() = %ld", const_a.size(), const_a.pad_size(), const_a.max_size());
        XCTAssert(const_a.data()+pad == const_a.begin() && const_a.data()+pad+sz == const_a.end(),
                  @"Iterators incorrect.");
        for (int i = -pad; i < sz+pad; ++i) {
            XCTAssert(i+1 == const_a[i],
                      @"a[%d] = %d", i, const_a[i]);
        }
        XCTAssert(1 == const_a.front() && 3 == const_a.back(),
                  @"a.front() = %d, a.back() = %d", const_a.front(), const_a.back());
        try {
            const_a.at(-2);
            XCTAssert(false, @"at(...) should have thrown an out-of-range exception.");
        } catch (std::exception&) { }

        decltype(a) b;
        b.fill(10);
        std::swap(a, b);
        for (auto const& x : const_a) {
            XCTAssert(10 == x,
                      @"x = %d", x);
        }

        XCTAssert(const_a != b, @"");
        XCTAssert(const_a > b, @"");
        a[0] = 0;
        XCTAssert(!(const_a > b), @"");
        a.fill(0); b.fill(0);
        XCTAssert(a == b, @"");
    }

    // non-zero paddings:
    @autoreleasepool {
        constexpr long sz = 3, pad = 1;
        PaddedArray<int, sz, pad> a;
        for (int i = -pad; i < sz+pad; ++i) { a[i] = i; }
        PaddedArray<int, sz, pad> const& const_a = a;
        printo(std::cout, a, '\n');

        XCTAssert(sz == const_a.size() && pad == const_a.pad_size() && sz+2*pad == const_a.max_size(),
                  @"a.size() = %ld, a.pad_size() = %ld, a.max_size() = %ld", const_a.size(), const_a.pad_size(), const_a.max_size());
        XCTAssert(const_a.data()+pad == const_a.begin() && const_a.data()+pad+sz == const_a.end(),
                  @"Iterators incorrect.");
        for (int i = -pad; i < sz+pad; ++i) {
            XCTAssert(i == const_a[i],
                      @"a[%d] = %d", i, const_a[i]);
        }
        XCTAssert(0 == const_a.front() && sz-1 == const_a.back(),
                  @"a.front() = %d, a.back() = %d", const_a.front(), const_a.back());
        try {
            auto x = const_a.at(-pad);
            XCTAssert(-pad == x, @"at(-%ld) = %d", -pad, x);
        } catch (std::exception&) {
            XCTAssert(false, @"at(...) should not have thrown an exception.");
        }

        for (auto& x : a) { x = 10; }
        XCTAssert(-pad == *const_a.data() && 10 == const_a.front() && 10 == const_a.back() && sz == *const_a.end(),
                  @"*a.data() = %d, a.front() = %d, a.back() = %d, *a.end() = %d", *a.data(), a.front(), a.back(), *a.end());

        a.fill(10);
        decltype(a) b = a;
        XCTAssert(a == b, @"");
        a[-1] = 5;
        XCTAssert(const_a != b && const_a < b, @"");
        b = a; a[sz] = 20;
        XCTAssert(const_a > b, @"");

        using std_array = std::array<decltype(a)::value_type, a.max_size()>;
        std_array const& c = *(std_array const*)(&const_a);
        for (int i = -pad, j = 0; i < sz+pad; ++i, ++j) {
            a[i] = j;
            XCTAssert(j == c[unsigned(j)],
                      @"a[%d] = %d, c[%d] = %d", i, const_a[i], j, c[unsigned(j)]);
        }
    }
}

- (void)testArray_ND {
    using UTL::__3_::Aggregate::Array_3D;

    @autoreleasepool {
        constexpr long pad{1}, sz[3]{2, 3, 4};
        Array_3D<double, pad, sz[0], sz[1], sz[2]> a3;
        std::fill_n(a3.data()->data()->data(), sizeof(a3)/sizeof(double), 0.);
        long len{1};
        for (int i = 0; i < 3; ++i) len *= 2*pad + sz[i];
        XCTAssert(len == sizeof(a3)/sizeof(double),
                  @"%ld == %lu", len, sizeof(a3));

        len = 0;
        for (auto& x: a3) {
            for (auto& x: x) {
                for (auto& x: x) {
                    x = len++;
                }
            }
        }
        printo(std::cout, a3, '\n');

        auto xp = a3.front().front().begin();
        for (int i = 0; i < sz[2]; ++i, ++xp) {
            XCTAssert(double(i) == *xp, @"%d == %f", i, *xp);
        }
        auto rp = a3.back().back().rbegin();
        for (int i = 0; i < sz[2]; ++i, ++rp) {
            XCTAssert(double(--len) == *rp, @"%ld == %f", len, *rp);
            //printf("%ld\n", len);
        }
    }
}

- (void)testCappedVector__3_ {
    using UTL::__3_::CappedVector;

    @autoreleasepool {
        constexpr long cap{5};
        typedef CappedVector<double, cap> Vector;
        Vector v;
        XCTAssert(0 == v.size() && cap == v.capacity() && cap == v.max_size(),
                  @"v.size() = %lu, v.capacity() = %lu, v.max_size() = %lu", v.size(), v.capacity(), v.max_size());

        try {
            //v = Vector(cap + 1);
            //v = Vector({1, 2, 3, 4, 5, 6});
            v = {1, 2, 3, 4, 5, 6};
            XCTAssert(false, @"An exception should have been thrown.");
        } catch (std::exception&) {
        }
        try {
            constexpr long sz{3};
            v = Vector(sz);
            XCTAssert(sz == v.size(), @"");
            v = Vector(sz, 10);
            XCTAssert(sz == v.size(), @"");
            for (auto const& x : v) XCTAssert(10. == x, @"x = %f", x);
            printo(std::cout, v, '\n');

            double arr[cap]{3, 3, 3, 3, 3};
            v = Vector(arr, arr+cap);
            XCTAssert(cap == v.size(), @"");
            for (auto const& x : v) XCTAssert(*arr == x, @"x = %f", x);

            v = {1., 1.};
            XCTAssert(2 == v.size() && 1. == v.front() && v.front() == v.back(), @"");
        } catch (std::exception& e) {
            XCTAssert(false, @"%s", e.what());
        }

        Vector u;
        std::swap(v, u);
        XCTAssert(0 == v.size() && 2 == u.size() && u.front() == u.back() && 1. == u.front(), @"");
        u.clear();
        XCTAssert(0 == u.size(), @"");

        v.push_back(1.);
        v.push_back(2.);
        v.front() = v.back();
        v.pop_back();
        XCTAssert(2. == v[0] && 1 == v.size(), @"");
        try {
            v.at(1) = 1.;
            XCTAssert(false, @"An exception should have been thrown.");
        } catch (std::exception&) {}

        int push_back_count;
        try {
            v.clear();
            for (push_back_count = 0; push_back_count <= cap; ++push_back_count) v.push_back(1.);
            XCTAssert(false, @"An exception should have been thrown.");
        } catch (std::exception&) {
            XCTAssert(cap == push_back_count, @"push_back_count = %d", push_back_count);
        }

        u.clear();
        u.resize(cap, 10.);
        XCTAssert(cap == u.size(), @"");
        for (auto const& x : u) XCTAssert(10. == x, @"x = %f", x);
        u.resize(0);
        XCTAssert(0 == u.size() && 10. == *u.data(), @"");
        try {
            u.front() = 0.;
            XCTAssert(false, @"An exception should have been thrown.");
        } catch (std::exception&) {}
    }

    @autoreleasepool {
        struct S {
            std::string s;
            ~S() { printo(std::cout, __PRETTY_FUNCTION__, '\n'); }
        };

        typedef CappedVector<S, 2> Vector;
        std::unique_ptr<Vector> vp(new Vector(2));
        vp->front().s = vp->back().s = "test";
        for (auto const& s : *vp) printo(std::cout, s.s, '\n');
    }
}

- (void)testPaddedArray_Dynamic {
    using UTL::__3_::Dynamic::PaddedArray;

    @autoreleasepool {
        constexpr long pad = 0;
        typedef PaddedArray<double, pad> PA;
        constexpr long sz = 2;
        PA const& a = PA(sz);
        printo(std::cout, a, '\n');

        XCTAssert(!!a && sz == a.size() && pad == a.pad_size() && sz+2*pad == a.max_size(),
                  @"a.size() = %ld, a.pad_size() = %ld, a.max_size() = %ld", a.size(), a.pad_size(), a.max_size());
        XCTAssert(a.data()+pad == a.begin() && a.data()+pad+sz == a.end(),
                  @"Iterators incorrect.");

        PA&& b = PA(sz);
        printo(std::cout, a, '\n');
        XCTAssert(a == b, @"");

        PA c(std::move(b));
        XCTAssert(!b && a == c, @"");

        b = PA(sz, 1.0);
        XCTAssert(!!b && 1.0 == b[0], @"");

        c = PA(sz, 10.0);
        if (sz) {
            XCTAssert(c.begin() != c.end() && c.rbegin() != c.rend(), @"");
            XCTAssert(10.0 == c.front() && 10.0 == c.back(), @"");
        } else {
            XCTAssert(c.begin() == c.end() && c.rbegin() == c.rend(), @"");
        }
    }

    @autoreleasepool {
        constexpr long pad = 1;
        typedef PaddedArray<double, pad> PA;
        constexpr long sz = 2;
        PA const& a = PA({2., 3.});
        printo(std::cout, a, '\n');

        XCTAssert(sz == a.size() && pad == a.pad_size() && sz+2*pad == a.max_size(),
                  @"a.size() = %ld, a.pad_size() = %ld, a.max_size() = %ld", a.size(), a.pad_size(), a.max_size());
        XCTAssert(a.data()+pad == a.begin() && a.data()+pad+sz == a.end(),
                  @"Iterators incorrect.");

        PA b(sz);
        b = a;
        //std::cout << b << std::endl;
        XCTAssert(a == b, @"");

        PA c(a);
        //std::cout << c << std::endl;
        XCTAssert(c == a, @"");

        PA d(std::move(c));
        XCTAssert(!c && d == a, @"");

        d = {3., 2.};
        XCTAssert(d > a, @"");

        b = std::move(d);
        XCTAssert(b > a && a == d, @"");

        b.fill(1.);
        *b.data() = *a.data();
        XCTAssert(a > b, @"");

        XCTAssert(b.front() == 1. && b.back() == 1., @"");
        XCTAssert(b.front() == b.at(0) && b.back() == b.at(long(b.size())-1), @"");

        try {
            a.at(-pad-1);
            XCTAssert(false, @"an exception should have thrown.");
        } catch (std::exception &e) {
            XCTAssert(true, @"");
        }

        XCTAssert(a.begin()-pad == a.data(), @"");
    }
}

- (void)testPaddedArray_Dynamic_2D {
    using UTL::__3_::Dynamic::PaddedArray;

    constexpr long dims[2]{2, 3};
    constexpr long pad{1};
    unsigned nelems = 1;
    for (auto n : dims) nelems *= n + 2*pad;
    XCTAssert(20 == nelems, @"nelems = %d", nelems);

    std::unique_ptr<double, void (*)(void *)> buffer((double *)malloc(sizeof(double)*nelems*2), &free);

    struct Alloc : std::allocator<double> {
        pointer p;
        pointer allocate(size_type) { return p; }
        void deallocate(pointer, size_type) {}
    };
    typedef PaddedArray<PaddedArray<double, pad/*, Alloc*/>, pad> _2D;
    typedef _2D::value_type _1D;

    _2D mat1(dims[0]), mat2(dims[0]);
    @autoreleasepool {
        Alloc a;
        a.p = buffer.get();
        for (int i = -pad; i < dims[0]+pad; ++i, a.p += dims[1] + 2*pad) {
            mat1.at(i) = _1D(dims[1], double(i), a.p);
            mat1.at(i) = PaddedArray<double, pad>(dims[1], double(i));
            //            mat1[i].fill(double(i));
        }
        for (int i = -pad; i < dims[0]+pad; ++i, a.p += dims[1] + 2*pad) {
            mat2.at(i) = _1D(dims[1], a.p);
            mat2[i].fill(0);
        }
    }
    XCTAssert(buffer.get() == mat1.data()->data()
              && buffer.get()+nelems == mat2.data()->data()
              && buffer.get()+nelems*2 == (mat2.end()+pad-1)->end()+pad
              , @"");
    printo(std::cout, mat1, '\n');
    XCTAssert(mat1 != mat2 && mat1 < mat2, @"");

    mat1 = std::move(mat2);
    printo(std::cout, mat2, '\n');
    XCTAssert(mat1.front().begin() != mat2.front().begin(), @"");
    for (int i = -pad; i < dims[0]+pad; ++i) {
        for (int j = -pad; j < dims[1]+pad; ++j) {
            XCTAssert(double() == mat1.at(i).at(j),
                      @"mat1[%d][%d] = %f", i, j, mat1[i][j]);
        }
    }
}

- (void)testPaddedArray_Dynamic_3D {
    using UTL::__3_::Dynamic::PaddedArray;

    constexpr long dims[3]{2, 3, 4};
    unsigned nelems = 1;
    for (auto n : dims) nelems *= n;

    std::unique_ptr<double, void (*)(void *)> buffer((double *)malloc(sizeof(double)*nelems*2), &free);

    struct Alloc : std::allocator<double> {
        pointer p;
        pointer allocate(size_type) { return p; }
        void deallocate(pointer, size_type) {}
    };

    typedef PaddedArray<double, 0/*, Alloc*/> _1D;
    typedef PaddedArray<_1D, 0> _2D;
    typedef PaddedArray<_2D, 0> _3D;

    _3D mat1(dims[0]), mat2(dims[0]);
    @autoreleasepool {
        Alloc a;
        a.p = buffer.get();
        for (int i = 0; i < dims[0]; ++i) {
            mat1.at(i) = _2D(dims[1]);
            for (int j = 0; j < dims[1]; ++j, a.p += dims[2]) {
                mat1.at(i).at(j) = _1D(dims[2], double(i*dims[0] + j), a.p);
                mat1[i][j].fill(double(i*dims[0] + j));
            }
        }
        for (int i = 0; i < dims[0]; ++i) {
            mat2[i] = _2D(dims[1]);
            for (int j = 0; j < dims[1]; ++j, a.p += dims[2]) {
                mat2.at(i).at(j) = _1D(dims[2], a.p);
                mat2[i][j].fill(0.);
            }
        }
    }
    XCTAssert(buffer.get() == mat1.front().front().begin()
              && buffer.get()+nelems == mat2.front().front().begin()
              && buffer.get()+nelems*2 == mat2.back().back().end()
              , @"");
    printo(std::cout, mat1, '\n');
    XCTAssert(mat1 != mat2 && mat1 > mat2, @"");

    mat1 = std::move(mat2);
    printo(std::cout, mat2, '\n');
    XCTAssert(mat1.front().front().begin() != mat2.front().front().begin(), @"");
    for (int i = 0; i < dims[0]; ++i) {
        for (int j = 0; j < dims[1]; ++j) {
            for (int k = 0; k < dims[2]; ++k) {
                XCTAssert(double() == mat1.at(i).at(j).at(k),
                          @"mat1[%d][%d][%d] = %f", i, j, k, mat1[i][j][k]);
            }
        }
    }
}

- (void)testPaddedArray_Dynamic_Copy {
    using UTL::__3_::Dynamic::PaddedArray;

    typedef PaddedArray<PaddedArray<double, 2>, 1> _A1;
    typedef PaddedArray<PaddedArray<double, 1>, 1> _A2;

    _A1 a1(1, _A1::value_type(1, 3.));
    _A2 a2(a1.size(), _A2::value_type(a1.front().size()));
    a2 = a1;
    for (auto const& a : a2) {
        for (auto x : a) {
            XCTAssert(a1.front().front() == x, @"a2[][] = %f", x);
        }
    }
    
    a1.fill(_A1::value_type(a1.front().size()));
    a1 = a2;
    for (auto const& a : a1) {
        XCTAssert(0. == a.pad_front() && a.front() == a2.front().front(), @"a[-2] = %f, a[0] = %f", a.pad_front(), a.front());
    }
}

- (void)testNumericVector_1 {
    using UTL::__3_::Numeric::Vector;

    typedef double _T;
    const long sz(1);
    const double one(1);
    Vector<_T, sz> v(one), v2(0);
    XCTAssert(sizeof(v) == sz*sizeof(_T), @"");
    XCTAssert(sz == v.size() && one == v.x, @"");
    XCTAssert(one == v.front() && one == v.back() && one == v[0], @"");
    XCTAssert(&v.x == v.begin() && v.end() == v.data()+1, @"");

    v2.swap(v);
    XCTAssert(one == v2[0], @"");
    v2.fill(0);
    XCTAssert(0 == v[0] && 0 == v2[0], @"");

    XCTAssert(std::get<0>(const_cast<Vector<_T, sz> const&>(v2)) == v2[0], @"");

    constexpr Vector<_T, sz> v3{3};
    constexpr _T x = std::get<0>(v3);
    XCTAssert(x == v3[0], @"");
}

- (void)testNumericVector_2 {
    using UTL::__3_::Numeric::Vector;

    typedef double _T;
    const long sz(2);
    const double one(1), two(2);
    Vector<_T, sz> v(one, two), v2(0);
    XCTAssert(sizeof(v) == sz*sizeof(_T), @"");
    XCTAssert(sz == v.size() && one == v.x && two == v.y, @"");
    XCTAssert(one == v.front() && one == v[0] && two == v.back() && two == v[sz-1], @"");
    XCTAssert(&v.x == v.begin() && v.end() == v.data()+sz, @"");

    v2.swap(v);
    XCTAssert(one == v2[0] && two == v2[1], @"");
    v2.fill(0);
    for (unsigned i = 0; i < v.size(); ++i) {
        XCTAssert(0 == v[i] && 0 == v2[i], @"");
        v2[i] = i;
    }

    XCTAssert(std::get<0>(v2) == v2[0], @"");
    XCTAssert(std::get<1>(v2) == v2[1], @"");
}

- (void)testNumericVector_3 {
    using UTL::__3_::Numeric::Vector;

    typedef double _T;
    const long sz(3);
    const double one(1), two(2), thr(3);
    Vector<_T, sz> v(one, two, thr), v2(0);
    XCTAssert(sizeof(v) == sz*sizeof(_T), @"");
    XCTAssert(sz == v.size() && one == v.x && two == v.y && thr == v.z, @"");
    XCTAssert(one == v.front() && one == v[0] && two == v[1] && thr == v[2] && v[sz-1] == v.back(), @"");
    XCTAssert(&v.x == v.begin() && v.end() == v.data()+sz, @"");

    v2.swap(v);
    XCTAssert(one == v2[0] && two == v2[1] && thr == v2[2], @"");
    v2.fill(0);
    for (unsigned i = 0; i < v.size(); ++i) {
        XCTAssert(0 == v[i] && 0 == v2[i], @"");
        v2[i] = i;
    }

    XCTAssert(std::get<0>(v2) == v2[0], @"");
    XCTAssert(std::get<1>(v2) == v2[1], @"");
    XCTAssert(std::get<2>(v2) == v2[2], @"");
}

- (void)testNumericVector_4 {
    using UTL::__3_::Numeric::Vector;

    typedef double _T;
    const long sz(4);
    const double one(1), two(2), thr(3), four(4);
    Vector<_T, sz> v(one, two, thr, four), v2(0);
    XCTAssert(sizeof(v) == sz*sizeof(_T), @"");
    XCTAssert(sz == v.size() && one == v.x && two == v.y && thr == v.z && four == v.u, @"");
    XCTAssert(one == v.front() && one == v[0] && two == v[1] && thr == v[2] && four == v[3] && v[sz-1] == v.back(), @"");
    XCTAssert(&v.x == v.begin() && v.end() == v.data()+sz, @"");

    v2.swap(v);
    XCTAssert(one == v2[0] && two == v2[1] && thr == v2[2] && four == v2[3], @"");
    v2.fill(0);
    for (unsigned i = 0; i < v.size(); ++i) {
        XCTAssert(0 == v[i] && 0 == v2[i], @"");
        v2[i] = i;
    }

    XCTAssert(std::get<0>(v2) == v2[0], @"");
    XCTAssert(std::get<1>(v2) == v2[1], @"");
    XCTAssert(std::get<2>(v2) == v2[2], @"");
    XCTAssert(std::get<3>(v2) == v2[3], @"");
}

- (void)testNumericVector_5 {
    using UTL::__3_::Numeric::Vector;

    typedef double _T;
    const long sz(5);
    const double one(1), two(2), thr(3), four(4), five(5);
    Vector<_T, sz> v(one, two, thr, four, five), v2(0);
    XCTAssert(sizeof(v) == sz*sizeof(_T), @"");
    XCTAssert(sz == v.size() && one == v.x && two == v.y && thr == v.z && four == v.u && five == v.v, @"");
    XCTAssert(one == v.front() && one == v[0] && two == v[1] && thr == v[2] && four == v[3] && five == v[4] && v[sz-1] == v.back(), @"");
    XCTAssert(&v.x == v.begin() && v.end() == v.data()+sz, @"");

    v2.swap(v);
    XCTAssert(one == v2[0] && two == v2[1] && thr == v2[2] && four == v2[3] && five == v2[4], @"");
    v2.fill(0);
    for (unsigned i = 0; i < v.size(); ++i) {
        XCTAssert(0 == v[i] && 0 == v2[i], @"");
        v2[i] = i;
    }

    XCTAssert(std::get<0>(v2) == v2[0], @"");
    XCTAssert(std::get<1>(v2) == v2[1], @"");
    XCTAssert(std::get<2>(v2) == v2[2], @"");
    XCTAssert(std::get<3>(v2) == v2[3], @"");
    XCTAssert(std::get<4>(v2) == v2[4], @"");
}

- (void)testNumericVector_6 {
    using UTL::__3_::Numeric::Vector;

    typedef double _T;
    const long sz(6);
    const double one(1), two(2), thr(3), four(4), five(5), six(6);
    Vector<_T, sz> v(one, two, thr, four, five, six), v2(0);
    XCTAssert(sizeof(v) == sz*sizeof(_T), @"");
    XCTAssert(sz == v.size() && one == v.x && two == v.y && thr == v.z && four == v.u && five == v.v && six == v.w, @"");
    XCTAssert(one == v.front() && one == v[0] && two == v[1] && thr == v[2] && four == v[3] && five == v[4] && six == v[5] && v[sz-1] == v.back(), @"");
    XCTAssert(&v.x == v.begin() && v.end() == v.data()+sz, @"");

    v2.swap(v);
    XCTAssert(one == v2[0] && two == v2[1] && thr == v2[2] && four == v2[3] && five == v2[4] && six == v2[5], @"");
    v2.fill(0);
    for (unsigned i = 0; i < v.size(); ++i) {
        XCTAssert(0 == v[i] && 0 == v2[i], @"");
        v2[i] = i;
    }

    XCTAssert(std::get<0>(v2) == v2[0], @"");
    XCTAssert(std::get<1>(v2) == v2[1], @"");
    XCTAssert(std::get<2>(v2) == v2[2], @"");
    XCTAssert(std::get<3>(v2) == v2[3], @"");
    XCTAssert(std::get<4>(v2) == v2[4], @"");
    XCTAssert(std::get<5>(v2) == v2[5], @"");
}

- (void)testNumericVector_N {
    using UTL::__3_::Numeric::Vector;

    typedef double _T;
    const long sz(10);
    constexpr _T x = 3;
    Vector<_T, sz> v(x), v2(0);
    XCTAssert(sizeof(v) == sz*sizeof(_T), @"");
    XCTAssert(sz == v.size() && v.front() == v.back() && x == v.front() && x == std::get<0>(v) && x == std::get<sz-1>(v), @"");
    XCTAssert(v.data() == v.begin() && v.end() == v.data()+sz, @"");

    v2.swap(v);
    XCTAssert(x == v2[0] && x == std::get<sz-1>(v2), @"");
    v2.fill(0);
    for (unsigned i = 0; i < v.size(); ++i) {
        XCTAssert(0 == v[i] && 0 == v2[i], @"");
        v2[i] = i;
    }

    XCTAssert(std::get<0>(v2) == v2[0], @"");
    XCTAssert(std::get<sz-1>(v2) == v2[sz-1], @"");
}

- (void)testNumericVector_arithematic {
    using UTL::__3_::Numeric::Vector;

    typedef double T;
    const unsigned sz = 30;
    Vector<T, sz> a, b;
    for (unsigned i = 0; i < sz; ++i) {
        b[i] = i + 2;
    }
    Vector<T, sz> const c(3);

    // compound - vector:
    a = b;
    a += c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i], @"");
    }
    a = b;
    a -= c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i], @"");
    }
    a = b;
    a *= c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i], @"");
    }
    a = b;
    a /= c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i], @"");
    }
    // compound - scalar:
    a = b;
    a += std::get<0>(c);
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i], @"");
    }
    a = b;
    a -= std::get<0>(c);
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i], @"");
    }
    a = b;
    a *= std::get<0>(c);
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i], @"");
    }
    a = b;
    a /= std::get<0>(c);
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i], @"");
    }

    // binary - expressions:
    a = b + c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i], @"");
    }
    a = b - c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i], @"");
    }
    a = b * c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i], @"");
    }
    a = b / c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i], @"");
    }

    // binary - with a scalar:
    decltype(a) d;
    a = b + std::get<0>(c);
    d = std::get<0>(c) + b;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] == a[i] && c[i] + b[i] == d[i], @"");
    }
    a = b - std::get<0>(c);
    d = std::get<0>(c) - b;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] - c[i] == a[i] && c[i] - b[i] == d[i], @"");
    }
    a = b * std::get<0>(c);
    d = std::get<0>(c) * b;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] * c[i] == a[i] && c[i] * b[i] == d[i], @"");
    }
    a = b / std::get<0>(c);
    d = std::get<0>(c) / b;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] / c[i] == a[i] && c[i] / b[i] == d[i], @"");
    }

    // compound - expressions:
    d.fill(10);
    a = d;
    a += b + c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(d[i] + (b[i] + c[i]) == a[i], @"");
    }
    a = d;
    a -= b + c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(d[i] - (b[i] + c[i]) == a[i], @"");
    }
    a = d;
    a *= b + c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(d[i] * (b[i] + c[i]) == a[i], @"");
    }
    a = d;
    a /= b + c;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(d[i] / (b[i] + c[i]) == a[i], @"");
    }

    // mixed:
    T s = .1;
    a = b + c - s/d;
    for (unsigned i = 0; i < sz; ++i) {
        XCTAssert(b[i] + c[i] - s/d[i] == a[i], @"");
    }
}

@end
