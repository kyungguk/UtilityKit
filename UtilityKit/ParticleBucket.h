//
//  ParticleBucket.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/17/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#ifndef ParticleBucket_h
#define ParticleBucket_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
// Originated from HybridKit Version 3.
//
#include <UtilityKit/ArrayKit.h>
#include <sstream>
#include <utility>
#include <vector>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    // MARK: Particle<ND>
    template <long _ND>
    struct Particle final {
        static constexpr long ND = _ND;

        typedef double value_type;
        typedef Numeric::Vector<value_type, 3> velocity_type;
        typedef Numeric::Vector<value_type, ND> position_type;

        velocity_type vel;
        position_type pos;

        // constructors:
        constexpr explicit Particle() noexcept = default;
        constexpr Particle(velocity_type const& v, position_type const& x) noexcept : vel(v), pos(x) {}
    };

    // MARK: ParticleBucket<ND>
    template <long _ND>
    class ParticleBucket final : private std::vector<Particle<_ND> > {
        typedef std::vector<Particle<_ND> > __base;

    public:
        static constexpr long ND = _ND;
        typedef typename __base::value_type particle_type;
        typedef typename particle_type::position_type position_type;
        typedef typename particle_type::velocity_type velocity_type;
        typedef typename particle_type::value_type value_type;
        // inherited:
        typedef typename __base::size_type size_type;
        typedef typename __base::difference_type difference_type;
        typedef typename __base::reference reference;
        typedef typename __base::const_reference const_reference;
        typedef typename __base::pointer pointer;
        typedef typename __base::const_pointer const_pointer;
        typedef typename __base::iterator iterator;
        typedef typename __base::const_iterator const_iterator;
        typedef typename __base::reverse_iterator reverse_iterator;
        typedef typename __base::const_reverse_iterator const_reverse_iterator;

        explicit ParticleBucket() = default;

    public:
        // element access
        //
        using __base::at;
        using __base::operator[];
        using __base::front;
        using __base::back;

        // iterators
        //
        using __base::begin;
        using __base::end;
        using __base::rbegin;
        using __base::rend;
        using __base::data;

        // capacity
        //
        using __base::empty;
        using __base::size;
        using __base::capacity;
        using __base::reserve;

        // modifiers
        //
        //void swap(ParticleBucket& o) { __base::swap(o); }
        using __base::clear;
        using __base::shrink_to_fit;
        using __base::resize;

        using __base::push_back;
        void push_back(velocity_type const& vel, position_type const& pos) {
            __base::push_back({vel, pos});
        }
        template <class It>
        void insert_back(It first, It last) { __base::insert(__base::end(), first, last); }
        particle_type pop_back() {
            particle_type&& back = std::move(__base::back());
            __base::pop_back();
            return std::move(back);
        }

        template <class OutputIt, class _Pred/*bool(position_type)*/>
        OutputIt evict_if(OutputIt bucket_first, OutputIt bucket_last, _Pred pred) {
            size_type Np = __base::size(), i = 0;
            while (i < Np && bucket_first != bucket_last) {
                reference ptl = __base::operator[](i);
                if ( pred(static_cast<const_reference>(ptl).pos) ) {
                    *bucket_first++ = ptl;
                    ptl = pop_back();
                    --Np;
                } else { ++i; }
            }
            return bucket_first;
        }
        template <class _Pred/*bool(position_type)*/> // cumulative
        void evict_if(ParticleBucket &bucket, _Pred pred) {
            size_type Np = __base::size(), i = 0;
            while (i < Np) {
                reference ptl = __base::operator[](i);
                if ( pred(static_cast<const_reference>(ptl).pos) ) {
                    bucket.push_back( ptl );
                    ptl = pop_back();
                    --Np;
                } else { ++i; }
            }
        }

        ParticleBucket& operator<<(ParticleBucket const &src) {
            insert_back(src.begin(), src.end());
            return *this;
        }
        ParticleBucket& operator<<(Expression::Pair<ParticleBucket const &, position_type> src_L) {
            ParticleBucket const &src = src_L._1();
            position_type const L = src_L._2();

            // 1. resize dst
            //
            size_type d_i = __base::size(), s_i;
            __base::resize(d_i + src.size());

            // 2. copy over
            //
            size_type const n = __base::size();
            for (s_i = 0; d_i < n; ++d_i, ++s_i) {
                particle_type const &ptl = src[s_i];
                __base::operator[](d_i) = {ptl.vel, ptl.pos + L};
            }
            return *this;
        }

        // addition/subtraction on position
        //
        ParticleBucket &operator+=(position_type const &pos) {
            iterator first = __base::begin(), last = __base::end();
            while (first != last) {
                first++->pos += pos;
            }
            return *this;
        }
        ParticleBucket &operator-=(position_type const &pos) {
            iterator first = __base::begin(), last = __base::end();
            while (first != last) {
                first++->pos -= pos;
            }
            return *this;
        }
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE

// MARK: Particle position plus/minus
template <long ND> inline UTILITYKIT_NAMESPACE::__3_::Expression::Pair<
UTILITYKIT_NAMESPACE::__3_::ParticleBucket<ND> const &, typename
UTILITYKIT_NAMESPACE::__3_::ParticleBucket<ND>::position_type
> operator+(UTILITYKIT_NAMESPACE::__3_::ParticleBucket<ND> const &bucket, typename
            UTILITYKIT_NAMESPACE::__3_::ParticleBucket<ND>::position_type const &L) {
    return {bucket,  L};
}
template <long ND> inline UTILITYKIT_NAMESPACE::__3_::Expression::Pair<
UTILITYKIT_NAMESPACE::__3_::ParticleBucket<ND> const &, typename
UTILITYKIT_NAMESPACE::__3_::ParticleBucket<ND>::position_type
> operator-(UTILITYKIT_NAMESPACE::__3_::ParticleBucket<ND> const &bucket, typename
            UTILITYKIT_NAMESPACE::__3_::ParticleBucket<ND>::position_type const &L) {
    return {bucket, -L};
}

// MARK: Output Stream
template<class _CharT, class _Traits, long _ND>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& __os, UTILITYKIT_NAMESPACE::__3_::Particle<_ND> const& ptl)
{
    std::basic_ostringstream<_CharT, _Traits> __s; {
        __s.flags(__os.flags());
        __s.imbue(__os.getloc());
        __s.precision(__os.precision());
    }
    unsigned long i = 0;
    __s << "{" << ptl.vel.x << ", " << ptl.vel.y << ", " << ptl.vel.z << ", ";
    __s << ptl.pos[i++]; while (i < _ND) __s << ", " << ptl.pos[i++]; __s << "}";
    return __os << __s.str();
}

template<class _CharT, class _Traits, long _ND>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& __os, UTILITYKIT_NAMESPACE::__3_::ParticleBucket<_ND> const& ptls)
{
    // nonbuffered
    auto first = ptls.begin(), last = ptls.end();
    __os << "{";
    if (first != last) {
        __os << *first++; while (first != last) { __os << ", " << *first++; }
    }
    return __os << "}";
}

#endif /* ParticleBucket_h */
