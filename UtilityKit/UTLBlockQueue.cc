//
//  UTLBlockQueue.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/24/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLBlockQueue.h"
#include "UTLPrinto.h"

// MARK: Version 4
//
#include <utility>

// MARK:- BlockQueue<Mutex>
//
UTL::__4_::BlockQueue<UTL::__4_::Mutex>::~BlockQueue<UTL::__4_::Mutex>()
{
}
UTL::__4_::BlockQueue<UTL::__4_::Mutex>::BlockQueue()
: _q(), _cv(), _mx() {
}

void UTL::__4_::BlockQueue<UTL::__4_::Mutex>::enqueue(_Block &&block)
{
    // enqueue
    _mx.lock();
    bool const should_notify = _q.empty();
    _q.push(std::move(block));
    _mx.unlock();
    // notify
    if (should_notify) {
        _cv.notify_all();
    }
}

auto UTL::__4_::BlockQueue<UTL::__4_::Mutex>::dequeue()
-> _Block {
    LockG<Mutex> _{_mx};
    while (_q.empty()) {
        _cv.wait(_mx);
    }
    _Block block = std::move(_q.front());
    _q.pop();
    return block;
}
auto UTL::__4_::BlockQueue<UTL::__4_::Mutex>::try_dequeue()
-> Optional<_Block> {
    LockG<Mutex> _{_mx};
    if (_q.empty()) {
        return {};
    }
    Optional<_Block> block = std::move(_q.front());
    _q.pop();
    return block;
}

// MARK:- BlockQueue<SpinLock>
//
UTL::__4_::BlockQueue<UTL::__4_::SpinLock>::~BlockQueue<UTL::__4_::SpinLock>()
{
}
UTL::__4_::BlockQueue<UTL::__4_::SpinLock>::BlockQueue()
: _block_q(), _consumer_q(), _key() {
}

void UTL::__4_::BlockQueue<UTL::__4_::SpinLock>::enqueue(_Block &&block)
{
    LockG<SpinLock> _{_key};
    // enqueue
    bool const should_notify = _block_q.empty();
    _block_q.push(std::move(block));
    // notify one
    if (should_notify && !_consumer_q.empty()) {
        _consumer_q.front()->unlock();
        _consumer_q.pop();
    }
}

auto UTL::__4_::BlockQueue<UTL::__4_::SpinLock>::dequeue()
-> _Block {
    // If the block queue is not empty, then skip waiting for a job to be enqueued.
    // If the block queue is empty, the caller should wait for a job to be enqueued.
    LockG<SpinLock> _{_key};
    while (_block_q.empty()) {
        SpinLock consumer{SpinLock::Locked};
        _consumer_q.push(&consumer);
        _key.unlock();
        consumer.lock(true);
        _key.lock(); // so that access to _crit is lock guarded
    }
    _Block block = std::move(_block_q.front());
    _block_q.pop();
    return block;
}
auto UTL::__4_::BlockQueue<UTL::__4_::SpinLock>::try_dequeue()
-> Optional<_Block> {
    // If the block queue is empty, simply return null optional.
    LockG<SpinLock> _{_key};
    if (_block_q.empty()) {
        return {};
    }
    Optional<_Block> block = std::move(_block_q.front());
    _block_q.pop();
    return block;
}
