//
//  UTLGaussLegendreIntegrator.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/22/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLGaussLegendreIntegrator_hh
#define UTLGaussLegendreIntegrator_hh

// MARK:- Version 4
//
#include <stdexcept>
#include <string>
#include <cmath>

template <long n>
UTL::__4_::GaussLegendreIntegrator<double, n>::GaussLegendreIntegrator()
{
    for (unsigned long i = 0, m = (n + 1)/2; i < m; ++i) {
        // iterative root finding by Newton's method, which will be the abscissa
        // proceed with double for accuracy
        //
        constexpr abscissa_type eps = 1e-14, one = 1, two = 2;
        abscissa_type z = std::cos(M_PI*(i + .75)/(n + .5)); // initial guess
        abscissa_type pp{};
        unsigned iter;
        constexpr unsigned max_iterations = 10;
        for (iter = 0; iter < max_iterations; ++iter) {
            abscissa_type p1 = 1, p2 = 0;
            for (unsigned long j = 0; j < n; ++j) { // recurrence relation to valuate Legendre polynomial
                abscissa_type const
                p3 = p2;
                p2 = p1;
                p1 = ((two*j + 1)*z*p2 - j*p3)/(j + one);
            }
            pp = n*(z*p1 - p2)/(z*z - 1); // first derivative of Legendre polynomial
            abscissa_type const
            z1 = z;
            z = z1 - p1/pp;
            if (std::abs(z - z1) < eps) break;
        }
        if (iter >= max_iterations) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - finding roots of Legendre polynomial failed");
        } else {
            _x[i] = abscissa_type{-z}; // lower half
            _x[n-1-i] = -_x[i]; // upper half; roots are symmetric
            _w[i] = abscissa_type{2./((1 - z*z)*pp*pp)}; // note that `xl' in NR here is 1
            _w[n-1-i] = _w[i]; // symmetric
        }
    }
}

template <long n>
template <class F, class Y>
auto UTL::__4_::GaussLegendreIntegrator<double, n>::operator()(F&& f, abscissa_type const a, abscissa_type const b, Y&& y_init) const noexcept
-> typename std::decay<decltype(f(a))>::type {
    using result_type = typename std::decay<decltype(f(a))>::type;
    result_type s{std::forward<Y>(y_init)};
    abscissa_type const xr = (b - a)/2;
    for (unsigned long i = 0; i < n; ++i) {
        abscissa_type const t = _x[i];
        abscissa_type const w = _w[i];
        abscissa_type const x = (t + 1)*xr + a;
        result_type y = f(x);
        s += (y *= w*xr);
    }
    return s;
}

#endif /* UTLGaussLegendreIntegrator_hh */
