//
//  UTL_splat_tuple.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 12/21/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTL_splat_tuple_h
#define UTL_splat_tuple_h

#include <UtilityKit/UtilityKit-config.h>
#include <type_traits>
#include <utility>
#include <tuple>

UTILITYKIT_BEGIN_NAMESPACE
namespace {
    // index holder and splat tuple elements
    template <long... Is>
    struct _Indices {
        template <class Callable, class Tuple>
        void splat_tuple(Callable&& f, Tuple&& args) const {
            std::forward<Callable>(f)(std::forward<typename std::tuple_element<Is, Tuple>::type>(std::get<Is>(args))...);
        }
        template <class Ret, class Callable, class Tuple>
        Ret splat_tuple(Callable&& f, Tuple&& args) const {
            return std::forward<Callable>(f)(std::forward<typename std::tuple_element<Is, Tuple>::type>(std::get<Is>(args))...);
        }
    };

    // create indices:
    // N is the number of the remaining arguments, and Is... is indices pack.
    template <long N, long... Is> struct _make_indices {
        static_assert(N >= 0, "N is negative");
        using type = typename _make_indices<N-1, N-1, Is...>::type;
    };
    template <        long... Is> struct _make_indices<0L, Is...> { using type = _Indices<Is...>; }; // terminator

    // splat_tuple
    template <class Callable, class Tuple>
    inline void splat_tuple(Callable&& f, Tuple&& args) {
        constexpr typename _make_indices<std::tuple_size<Tuple>::value>::type indices{};
        indices.splat_tuple(std::forward<Callable>(f), std::forward<Tuple>(args));
    }
    template <class Ret, class Callable, class Tuple>
    inline Ret splat_tuple(Callable&& f, Tuple&& args) {
        constexpr typename _make_indices<std::tuple_size<Tuple>::value>::type indices{};
        return indices.template splat_tuple<Ret>(std::forward<Callable>(f), std::forward<Tuple>(args));
    }
}
UTILITYKIT_END_NAMESPACE

#endif /* UTL_splat_tuple_h */
