//
//  UTLCustomDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLCustomDistribution_h
#define UTLCustomDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>
#include <type_traits>
#include <utility>
#include <stdexcept>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    class CustomDistribution final : public Distribution {
        using interp_type = MonotoneCubicInterpolation<value_type>;

        interp_type _pdf, _cdf, _icdf;
        value_type _min, _max;
        value_type _mean, _var;

    public:
        // constructors
        //
        /**
         @brief Initialize with the probability distribution function (PDF) defined between `min` and `max`.
         @discussion Unless `should_normalize_pdf` is set, the PDF should be normalized, so that integral of the PDF between `min` and `max` is 1.

         If the PDF function object is passed by a lvalue reference, it must be alive until the initialization finished.
         If the PDF function object is passed by a rvalue reference, the type must be move-constructible.

         @param min Minimum abscissa.
         @param max Maximum abscissa.
         @param pdf A callable function object of the PDF with one argument of `vaule_type`.
         */
        template<class PDF>
        explicit CustomDistribution(value_type min, value_type max, PDF&& pdf, bool should_normalize_pdf = false);

        /**
         @brief Similar to the previous version, but when the cumulative distribution function (CDF) is also known.
         @discussion The PDF should be normalized, so that integral of the PDF between `min` and `max` is 1.

         If the CDF function object is passed by a lvalue reference, it must be alive until the initialization finished.
         If the CDF function object is passed by a rvalue reference, the type must be move-constructible.

         @param min Minimum abscissa.
         @param max Maximum abscissa.
         @param pdf A callable function object of the PDF with one argument of `vaule_type`.
         @param cdf A callable function object of the CDF with one argument of `vaule_type`.
         */
        template<class PDF, class CDF>
        explicit CustomDistribution(value_type min, value_type max, PDF&& pdf, CDF&& cdf);

        // copy/move
        //
        CustomDistribution(CustomDistribution const &) noexcept;
        CustomDistribution &operator=(CustomDistribution const &) noexcept;
        CustomDistribution(CustomDistribution &&) noexcept;
        CustomDistribution &operator=(CustomDistribution &&) noexcept;

        // properties
        //
        value_type min() const noexcept { return _min; }
        value_type max() const noexcept { return _max; }

        // inherited
        //
        value_type mean() const noexcept override { return _mean; }
        value_type variance() const noexcept override { return _var; }

        /**
         @brief Probability distribution function.
         @discussion Given `x`, `pdf` is interpolated from the table. So the result will be (slightly) different from the `pdf` with which `*this` is initialized.
         @param x min <= x <= max.
         */
        value_type pdf(value_type x) const override;
        /**
         @brief Cumulative distribution function.
         @discussion cdf(min) = 0 and cdf(max) = 1.
         @param x min <= x <= max.
         */
        value_type cdf(value_type x) const override;
        /**
         @brief Inverse cumulative distribution function.
         @discussion cdf^-1 (0) = min and cdf^-1 (1) = max.
         @param cdf 0 <= cdf <= 1.
         */
        value_type icdf(value_type cdf) const override;

    private: // helpers:
        using sampler_type = AdaptiveSampling1D<value_type>;
        static sampler_type _sampler() noexcept;
    };


#pragma mark template implementations:
    template<class PDF>
    CustomDistribution::CustomDistribution(value_type min, value_type max, PDF&& _pdf, bool should_normalize_pdf)
    : Distribution(), _pdf(), _cdf(), _icdf(), _min(min), _max(max), _mean(), _var() {
        // sanity check:
        if (min >= max) { throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - min >= max"); }

        // safeguard: If _PDF is rvalue ref, move construction occur.
        PDF pdf = std::forward<PDF>(_pdf);

        // moments:
        value_type mom_1st = trapz::qsimp([&pdf](value_type x)->value_type {
            return x * pdf(x);
        }, std::make_pair(min, max), 4).first;
        value_type mom_2nd = trapz::qsimp([&pdf](value_type x)->value_type {
            return x*x * pdf(x);
        }, std::make_pair(min, max), 4).first;

        // sampler:
        sampler_type sampler = this->_sampler();

        // pdf:
        sampler_type::point_list_type
        pdf_pts = sampler(pdf, min, max, 1);

        // cdf:
        sampler_type::point_list_type
        cdf_pts = sampler([&pdf, min](value_type x)->value_type {
            return trapz::qsimp(pdf, std::make_pair(min, x), 4).first;
        }, min, max, 1);

        // optional normalization:
        if (should_normalize_pdf) {
            // normalize cdf:
            cdf_pts.reverse();
            value_type const denom = cdf_pts.front().second;
            sampler_type::point_list_type::iterator
            first = cdf_pts.begin(), last = cdf_pts.end();
            for (; first != last; ++first) { first->second /= denom; }
            cdf_pts.reverse();
            // normalize pdf:
            first = pdf_pts.begin(); last = pdf_pts.end();
            for (; first != last; ++first) { first->second /= denom; }
            // moments:
            mom_1st /= denom;
            mom_2nd /= denom;
        }

        // save:
        this->_pdf = interp_type(pdf_pts.begin(), pdf_pts.end());
        this->_cdf = interp_type(cdf_pts.begin(), cdf_pts.end());
        _mean = mom_1st;
        _var  = mom_2nd - _mean*_mean;

        // inverse cdf:
        {
            sampler_type::point_list_type::iterator
            first = cdf_pts.begin(), last = cdf_pts.end();
            for (; first != last; ++first) { std::swap(first->second, first->first); }
        }
        this->_icdf = interp_type(cdf_pts.begin(), cdf_pts.end());
    }

    template<class PDF, class CDF>
    CustomDistribution::CustomDistribution(value_type min, value_type max, PDF&& _pdf, CDF&& _cdf)
    : Distribution(), _pdf(), _cdf(), _icdf(), _min(min), _max(max), _mean(), _var() {
        // sanity check:
        if (min >= max) { throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - min >= max"); }

        // safeguard: If _PDF and/or _CDF are rvalue ref, move construction occur.
        PDF pdf = std::forward<PDF>(_pdf);
        CDF cdf = std::forward<CDF>(_cdf);

        // mean & variance:
        _mean = trapz::qsimp([&pdf](value_type x)->value_type {
            return x * pdf(x);
        }, std::make_pair(min, max), 4).first;
        _var  = trapz::qsimp([&pdf](value_type x)->value_type {
            return x*x * pdf(x);
        }, std::make_pair(min, max), 4).first - _mean*_mean;

        // sampler:
        sampler_type sampler = this->_sampler();

        // pdf:
        sampler_type::point_list_type
        pdf_pts = sampler(pdf, min, max, 1);
        this->_pdf = interp_type(pdf_pts.begin(), pdf_pts.end());

        // cdf:
        sampler_type::point_list_type
        cdf_pts = sampler(cdf, min, max, 1);
        this->_cdf = interp_type(cdf_pts.begin(), cdf_pts.end());

        // inverse cdf:
        {
            sampler_type::point_list_type::iterator
            first = cdf_pts.begin(), last = cdf_pts.end();
            for (; first != last; ++first) { std::swap(first->second, first->first); }
        }
        this->_icdf = interp_type(cdf_pts.begin(), cdf_pts.end());
    }

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>
#include <type_traits>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    class CustomDistribution final : public Distribution {
        using spline_coefficient_type = MonotoneCubicCoefficient<value_type>;
        using interpolator_type = spline_coefficient_type::spline_interpolator_type;
        using integrator_type = TrapzIntegrator<value_type>;
        using sampler_type = AdaptiveSampling1D<value_type>;

        value_type _min, _max;
        value_type _mean, _var;
        interpolator_type _pdf, _cdf, _icdf;

    public:
        // constructors
        //
        /**
         @brief Initialize with the probability distribution function (PDF) defined between `min` and `max`.
         @discussion Unless `should_normalize_pdf` is set, the PDF should be normalized, so that integral of the PDF between `min` and `max` is 1.
         @param min Minimum abscissa.
         @param max Maximum abscissa.
         @param pdf A callable function object of the PDF with signature `value_type(value_type)'.
         @param should_normalize_pdf Flag to indicate to normalize PDF (default is false).
         */
        template <class PDF>
        explicit CustomDistribution(value_type const min, value_type const max, PDF &&pdf, bool should_normalize_pdf = false);

        /**
         @brief Similar to the previous version, but when the cumulative distribution function (CDF) is also known.
         @discussion The PDF should be normalized, so that integral of the PDF between `min` and `max` is 1.
         @param min Minimum abscissa.
         @param max Maximum abscissa.
         @param pdf A callable function object of the PDF with signature `value_type(value_type)'.
         @param cdf A callable function object of the CDF with signature `value_type(value_type)'.
         */
        template <class PDF, class CDF, typename std::enable_if<!std::is_fundamental<CDF>::value, long>::type = 0L> // necessary to prevent to inadvertently call the other version
        explicit CustomDistribution(value_type const min, value_type const max, PDF &&pdf, CDF &&cdf);

        // copy/move
        //
        CustomDistribution(CustomDistribution const &);
        CustomDistribution &operator=(CustomDistribution const &);
        CustomDistribution(CustomDistribution &&);
        CustomDistribution &operator=(CustomDistribution &&);

        // swap
        //
        void swap(CustomDistribution &o);

        // properties
        //
        value_type min() const noexcept { return _min; }
        value_type max() const noexcept { return _max; }

        // inherited
        //
        value_type mean() const noexcept override { return _mean; }
        value_type variance() const noexcept override { return _var; }

        /**
         @brief Probability distribution function.
         @discussion Given `x`, `pdf` is interpolated from the table.
         So the result will be (slightly) different from the `pdf` with which `*this` is initialized.
         @param x min <= x <= max.
         @exception When `x' lies outside the range, [min(), max()].
         */
        value_type pdf(value_type const x) const override;
        /**
         @brief Cumulative distribution function.
         @discussion cdf(min) = 0 and cdf(max) = 1.
         @param x min <= x <= max.
         @exception When `x' lies outside the range, [min(), max()].
         */
        value_type cdf(value_type const x) const override;
        /**
         @brief Inverse cumulative distribution function.
         @discussion cdf^-1 (0) = min and cdf^-1 (1) = max.
         @param cdf 0 <= cdf <= 1.
         @exception When `cdf' lies outside the range, [0, 1].
         */
        value_type icdf(value_type const cdf) const override;

    private: // helpers:
        static integrator_type default_integrator() noexcept;
        static sampler_type default_sampler() noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLCustomDistribution.hh>

#endif /* UTLCustomDistribution_h */
