//
//  UTLVector__0.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/27/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLVector__0_hh
#define UTLVector__0_hh

// MARK:- Version 4
//
#include <tuple>
#include <sstream>
#include <ostream>

namespace std {
    // MARK: std::swap
    //
    template <class Type, long Size>
    inline void swap(UTL::__4_::Vector<Type, Size> &a, UTL::__4_::Vector<Type, Size> &b) noexcept(noexcept(a.swap(b))) {
        a.swap(b);
    }

    // MARK: tuple support
    //
    template <unsigned long I, class Type, long Size>
    constexpr Type& get(UTL::__4_::Vector<Type, Size>& v) noexcept {
        static_assert(I >= 0 && I < Size, "index out-of-bound");
        return std::get<I>(static_cast<std::array<Type, Size>&>(v));
    }
    template <unsigned long I, class Type, long Size>
    constexpr Type const& get(UTL::__4_::Vector<Type, Size> const& v) noexcept {
        static_assert(I >= 0 && I < Size, "index out-of-bound");
        return std::get<I>(static_cast<std::array<Type, Size> const&>(v));
    }

    template <class T, std::size_t N>
    class tuple_size<UTL::__4_::Vector<T, N>> : public integral_constant<std::size_t, N> {};

    template <std::size_t I, class T, std::size_t N>
    struct tuple_element<I, UTL::__4_::Vector<T, N>> { using type = T; };
}

// MARK: Buffered Output Stream
//
template <class _CharT, class _Traits, class T, long Size>
std::basic_ostream<_CharT, _Traits> &operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::Vector<T, Size> const& v) {
    std::basic_ostringstream<_CharT, _Traits> ss; {
        ss.flags(os.flags());
        ss.imbue(os.getloc());
        ss.precision(os.precision());
    }
    auto it = v.begin(), end = v.end();
    ss << "{" << *it++;
    while (it != end) ss << ", " << *it++;
    ss << "}";
    return os << ss.str();
}

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK:- Operator Overloadings
    //
    // MARK: Binary +
    template <class T, long S> inline
    Vector<T, S> operator+(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a += b)) {
        return a += b;
    }
    template <class T, long S> inline
    Vector<T, S> operator+(Vector<T, S> a, T const& s) noexcept(noexcept(a += s)) {
        return a += s;
    }
    template <class T, long S> inline
    Vector<T, S> operator+(T const& s, Vector<T, S> a) noexcept(noexcept(a += s)) {
        return a += s;
    }
    // MARK: Binary -
    template <class T, long S> inline
    Vector<T, S> operator-(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a -= b)) {
        return a -= b;
    }
    template <class T, long S> inline
    Vector<T, S> operator-(Vector<T, S> a, T const& s) noexcept(noexcept(a -= s)) {
        return a -= s;
    }
    template <class T, long S> inline
    Vector<T, S> operator-(T const& s, Vector<T, S> const& b) {
        return decltype(s - b){s} -= b;
    }
    // MARK: Binary *
    template <class T, long S> inline
    Vector<T, S> operator*(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a *= b)) {
        return a *= b;
    }
    template <class T, long S> inline
    Vector<T, S> operator*(Vector<T, S> a, T const& s) noexcept(noexcept(a *= s)) {
        return a *= s;
    }
    template <class T, long S> inline
    Vector<T, S> operator*(T const& s, Vector<T, S> a) noexcept(noexcept(a *= s)) {
        return a *= s;
    }
    // MARK: Binary /
    template <class T, long S> inline
    Vector<T, S> operator/(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a /= b)) {
        return a /= b;
    }
    template <class T, long S> inline
    Vector<T, S> operator/(Vector<T, S> a, T const& s) noexcept(noexcept(a /= s)) {
        return a /= s;
    }
    template <class T, long S> inline
    Vector<T, S> operator/(T const& s, Vector<T, S> const& b) noexcept(noexcept(b / s)) {
        return decltype(s / b){s} /= b;
    }
    // MARK: Binary %
    template <class T, long S> inline
    Vector<T, S> operator%(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a %= b)) {
        return a %= b;
    }
    template <class T, long S> inline
    Vector<T, S> operator%(Vector<T, S> a, T const& s) noexcept(noexcept(a %= s)) {
        return a %= s;
    }
    template <class T, long S> inline
    Vector<T, S> operator%(T const& s, Vector<T, S> const& b) noexcept(noexcept(b % s)) {
        return decltype(s % b){s} %= b;
    }
    // MARK: Binary &
    template <class T, long S> inline
    Vector<T, S> operator&(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a &= b)) {
        return a &= b;
    }
    template <class T, long S> inline
    Vector<T, S> operator&(Vector<T, S> a, T const& s) noexcept(noexcept(a &= s)) {
        return a &= s;
    }
    template <class T, long S> inline
    Vector<T, S> operator&(T const& s, Vector<T, S> a) noexcept(noexcept(a &= s)) {
        return a &= s;
    }
    // MARK: Binary |
    template <class T, long S> inline
    Vector<T, S> operator|(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a |= b)) {
        return a |= b;
    }
    template <class T, long S> inline
    Vector<T, S> operator|(Vector<T, S> a, T const& s) noexcept(noexcept(a |= s)) {
        return a |= s;
    }
    template <class T, long S> inline
    Vector<T, S> operator|(T const& s, Vector<T, S> a) noexcept(noexcept(a |= s)) {
        return a |= s;
    }
    // MARK: Binary ^
    template <class T, long S> inline
    Vector<T, S> operator^(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a ^= b)) {
        return a ^= b;
    }
    template <class T, long S> inline
    Vector<T, S> operator^(Vector<T, S> a, T const& s) noexcept(noexcept(a ^= s)) {
        return a ^= s;
    }
    template <class T, long S> inline
    Vector<T, S> operator^(T const& s, Vector<T, S> a) noexcept(noexcept(a ^= s)) {
        return a ^= s;
    }
    // MARK: Binary <<
    template <class T, long S> inline
    Vector<T, S> operator<<(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a <<= b)) {
        return a <<= b;
        }
        template <class T, long S> inline
        Vector<T, S> operator<<(Vector<T, S> a, T const& s) noexcept(noexcept(a <<= s)) {
            return a <<= s;
        }
        template <class T, long S> inline
        Vector<T, S> operator<<(T const& s, Vector<T, S> const& b) noexcept(noexcept(b << s)) {
            return decltype(s << b){s} <<= b;
        }
        // MARK: Binary >>
        template <class T, long S> inline
        Vector<T, S> operator>>(Vector<T, S> a, Vector<T, S> const& b) noexcept(noexcept(a >>= b)) {
            return a >>= b;
        }
        template <class T, long S> inline
        Vector<T, S> operator>>(Vector<T, S> a, T const& s) noexcept(noexcept(a >>= s)) {
            return a >>= s;
        }
        template <class T, long S> inline
        Vector<T, S> operator>>(T const& s, Vector<T, S> const& b) noexcept(noexcept(b >> s)) {
            return decltype(s >> b){s} >>= b;
        }
} // namespace __4_
UTILITYKIT_END_NAMESPACE
    
#endif /* UTLVector__0_hh */
