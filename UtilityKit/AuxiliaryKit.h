//
//  AuxiliaryKit.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/30/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef AuxiliaryKit_h
#define AuxiliaryKit_h


#if defined(__cplusplus)

#include <UtilityKit/UTLOptional.h>
#include <UtilityKit/UTLAny.h>
#include <UtilityKit/UTL_is_aligned.h>
#include <UtilityKit/UTL_power_of_2.h>
#include <UtilityKit/UTL_splat_tuple.h>
#include <UtilityKit/UTL_pow.h>
#include <UtilityKit/UTLPrinto.h> // At the end

#endif


#endif /* AuxiliaryKit_h */
