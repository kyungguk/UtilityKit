//
//  UTLSIMDVector__N.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/28/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSIMDVector__N_h
#define UTLSIMDVector__N_h

#include <UtilityKit/UTLSIMDVector__0.h>

// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    // MARK: SIMDVector
    //
    /**
     @brief Wrapper for SIMD vector.
     */
    template <class Type, long Size>
    struct alignas(typename MakeSIMD<Type, Size>::type) SIMDVector : public Vector<Type, Size> {
        static_assert(std::is_arithmetic<Type>::value && !std::is_same<Type, bool>::value, "Type should be one of arithematic types excluding bool");
        static_assert(Size > 1, "vector size should be greater than 1");
        static_assert(!(Size & (Size - 1)), "vector size should be a power of 2");
        static_assert(Size <= 02000, "vector size can be no greater than 8192");

        // types:
        using vector_type            = Vector<Type, Size>;
        using value_type             = typename vector_type::value_type;
        using reference              = typename vector_type::reference;
        using const_reference        = typename vector_type::const_reference;
        using pointer                = typename vector_type::pointer;
        using const_pointer          = typename vector_type::const_pointer;
        using iterator               = typename vector_type::iterator;
        using const_iterator         = typename vector_type::const_iterator;
        using reverse_iterator       = typename vector_type::reverse_iterator;
        using const_reverse_iterator = typename vector_type::const_reverse_iterator;
        using size_type              = typename vector_type::size_type;
        using difference_type        = typename vector_type::difference_type;
        using size_vector_type       = typename vector_type::size_vector_type;
        using comparison_result_type = SIMDVector<typename _comparison_result<Type>::type, vector_type::size()>;

        // constructors:
        constexpr explicit SIMDVector() noexcept : vector_type() {}
        explicit SIMDVector(Type const& a) noexcept : vector_type(a) {}
        explicit SIMDVector(vector_type const& v) noexcept : vector_type(v) {}
        template <class... Args, typename std::enable_if<sizeof...(Args) == Size, long>::type = 0L> // explicit qualifier to prevent conflict with operator=
        explicit SIMDVector(Args&&... args) noexcept : vector_type(std::forward<Args>(args)...) {}

        // copy/move:
        constexpr SIMDVector(SIMDVector const& o) noexcept : vector_type(o) {}
        SIMDVector& operator=(SIMDVector const& o) noexcept { **this = *o; return *this; }
        SIMDVector& operator=(vector_type const& v) noexcept { vector_type::operator=(v); return *this; }
        SIMDVector& operator=(Type const& a) noexcept { vector_type::fill(a); return *this; }
        template <class U, typename std::enable_if<std::is_convertible<U, Type>::value, long>::type = 0L>
        explicit SIMDVector(Vector<U, vector_type::size()> const& o) noexcept : vector_type(o) {}
        template <class U>
        auto operator=(Vector<U, vector_type::size()> const& o) noexcept -> typename std::enable_if<std::is_convertible<U, Type>::value, SIMDVector>::type &{ return *this = SIMDVector{o}; }

        // half vectors:
        using half_vector_type = typename std::conditional<vector_type::size() == 2, Vector<Type, vector_type::size()/2>, SIMDVector<Type, vector_type::size()/2>>::type;
        half_vector_type      & lo()       noexcept { return reinterpret_cast<half_vector_type      *>(this)[0]; }
        half_vector_type const& lo() const noexcept { return reinterpret_cast<half_vector_type const*>(this)[0]; }

        half_vector_type      & hi()       noexcept { return reinterpret_cast<half_vector_type      *>(this)[1]; }
        half_vector_type const& hi() const noexcept { return reinterpret_cast<half_vector_type const*>(this)[1]; }

        // reductions:
        inline Type reduce_plus   () const noexcept;
        inline Type reduce_prod   () const noexcept;
        inline Type reduce_bit_and() const noexcept;
        inline Type reduce_bit_or () const noexcept;
        inline Type reduce_bit_xor() const noexcept;

        // compound arithmetic:
        inline SIMDVector &operator+=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator+=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator+=(Type const &rhs) noexcept { return *this += SIMDVector{rhs}; }
        inline SIMDVector &operator-=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator-=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator-=(Type const &rhs) noexcept { return *this -= SIMDVector{rhs}; }
        inline SIMDVector &operator*=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator*=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator*=(Type const &rhs) noexcept { return *this *= SIMDVector{rhs}; }
        inline SIMDVector &operator/=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator/=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator/=(Type const &rhs) noexcept { return *this /= SIMDVector{rhs}; }
        // compound modulus:
        inline SIMDVector &operator%=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator%=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator%=(Type const &rhs) noexcept { return *this %= SIMDVector{rhs}; }
        // compound bit operators:
        inline SIMDVector &operator&=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator&=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator&=(Type const &rhs) noexcept { return *this &= SIMDVector{rhs}; }
        inline SIMDVector &operator|=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator|=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator|=(Type const &rhs) noexcept { return *this |= SIMDVector{rhs}; }
        inline SIMDVector &operator^=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator^=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator^=(Type const &rhs) noexcept { return *this ^= SIMDVector{rhs}; }
        inline SIMDVector &operator<<=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator<<=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator<<=(Type const &rhs) noexcept { return *this <<= SIMDVector{rhs}; }
        inline SIMDVector &operator>>=(SIMDVector const &rhs) noexcept;
        inline SIMDVector &operator>>=(vector_type const &rhs) noexcept;
        inline SIMDVector &operator>>=(Type const &rhs) noexcept { return *this >>= SIMDVector{rhs}; }
        // unary:
        SIMDVector const &operator+() const noexcept { return *this; }
        SIMDVector        operator-() const noexcept { SIMDVector m{}; return m -= *this; }
        // comparison:
        // If true, all bits are set (i.e., -1)
        inline comparison_result_type operator==(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator!=(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator<=(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator>=(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator<(SIMDVector const &rhs) const noexcept;
        inline comparison_result_type operator>(SIMDVector const &rhs) const noexcept;

    private:
        using simd_type = typename MakeSIMD<Type, Size>::type;
        simd_type      & operator*()      & noexcept { return *reinterpret_cast<simd_type      *>(this); }
        simd_type const& operator*() const& noexcept { return *reinterpret_cast<simd_type const*>(this); }
        template <class T, long S> friend struct SIMDVector;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLSIMDVector__N.hh>

#endif /* UTLSIMDVector__N_h */
