(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     21739,        656]
NotebookOptionsPosition[     20451,        610]
NotebookOutlinePosition[     20811,        626]
CellTagsIndexPosition[     20768,        623]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{"Clear", "[", "\"\<`*\>\"", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetDirectory", "[", "\"\<~/Downloads\>\"", "]"}], ";"}]}], "Input"],

Cell[CellGroupData[{

Cell["Plasmapause", "Section"],

Cell[BoxData[{
 RowBox[{"Clear", "[", 
  RowBox[{
  "Nc", ",", "Nx", ",", "Np", ",", "NBoundary", ",", "Lpp", ",", "dLpp"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"particles", "=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"particles", ",", "factor"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"<<", "\"\<PlasmapauseDensityDistribution.m\>\""}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", 
       RowBox[{"{", 
        RowBox[{"q1", ",", "q2"}], "}"}], "*)"}], "\[IndentingNewLine]", 
      "particles"}]}], "\[IndentingNewLine]", "]"}]}], ";"}]}], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"Dimensions", "[", "particles", "]"}], ",", "Np"}], "}"}]], "Input"],

Cell[BoxData[{
 RowBox[{"Clear", "[", "plasmapauseDensity", "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"plasmapauseDensity", "[", "L_Real", "]"}], ":=", 
  RowBox[{"1", "-", 
   RowBox[{
    RowBox[{"Divide", "[", 
     RowBox[{
      RowBox[{"Subtract", "@@", "NBoundary"}], ",", 
      RowBox[{"Plus", "@@", "NBoundary"}]}], "]"}], 
    RowBox[{"Tanh", "[", 
     RowBox[{"4.56", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"L", "-", "Lpp"}], ")"}], "/", "dLpp"}]}], 
     "]"}]}]}]}]}], "Input"],

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{"plasmapauseDensity", "[", "L", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"L", ",", 
     RowBox[{
      RowBox[{"-", ".5"}], "Nx"}], ",", 
     RowBox[{".5", "Nx"}]}], "}"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"With", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"q1q2", "=", 
      RowBox[{"Transpose", "[", "particles", "]"}]}], ",", 
     RowBox[{"bspecs", "=", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
          RowBox[{
           RowBox[{"-", ".5"}], "Nx"}], ",", 
          RowBox[{
           RowBox[{"+", ".5"}], "Nx"}], ",", "1"}], "}"}], ",", 
        RowBox[{"{", "2", "}"}]}], "]"}]}]}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"q1s", ",", "nPP"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"q1s", "=", 
       RowBox[{"Range", "@@", 
        RowBox[{"bspecs", "[", 
         RowBox[{"[", "1", "]"}], "]"}]}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"nPP", "=", 
       RowBox[{
        RowBox[{"Thread", "[", 
         RowBox[{"plasmapauseDensity", "[", "q1s", "]"}], "]"}], "/", 
        RowBox[{"NIntegrate", "[", 
         RowBox[{
          RowBox[{"plasmapauseDensity", "[", "L", "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"L", ",", 
            RowBox[{
             RowBox[{"-", ".5"}], "Nx"}], ",", 
            RowBox[{".5", "Nx"}]}], "}"}]}], "]"}]}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Show", "[", 
         RowBox[{
          RowBox[{"Histogram", "[", 
           RowBox[{
            RowBox[{"q1q2", "[", 
             RowBox[{"[", "1", "]"}], "]"}], ",", 
            RowBox[{"bspecs", "[", 
             RowBox[{"[", "1", "]"}], "]"}], ",", "\"\<PDF\>\""}], "]"}], ",", 
          RowBox[{"ListPlot", "[", 
           RowBox[{
            RowBox[{"Thread", "[", 
             RowBox[{"{", 
              RowBox[{"q1s", ",", "nPP"}], "}"}], "]"}], ",", 
            RowBox[{"Joined", "\[Rule]", "True"}]}], "]"}], ",", 
          RowBox[{"ImageSize", "\[Rule]", "Medium"}]}], "]"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{"Show", "[", 
         RowBox[{
          RowBox[{"Histogram", "[", 
           RowBox[{
            RowBox[{"q1q2", "[", 
             RowBox[{"[", "2", "]"}], "]"}], ",", 
            RowBox[{"bspecs", "[", 
             RowBox[{"[", "2", "]"}], "]"}], ",", "\"\<PDF\>\""}], "]"}], ",", 
          RowBox[{"Graphics", "[", 
           RowBox[{"{", 
            RowBox[{"Line", "[", 
             RowBox[{"Thread", "[", 
              RowBox[{"{", 
               RowBox[{
                RowBox[{"Take", "[", 
                 RowBox[{
                  RowBox[{"bspecs", "[", 
                   RowBox[{"[", "2", "]"}], "]"}], ",", "2"}], "]"}], ",", 
                RowBox[{"Divide", "[", 
                 RowBox[{
                  RowBox[{"-", "1"}], ",", 
                  RowBox[{"Subtract", "@@", 
                   RowBox[{"Take", "[", 
                    RowBox[{
                    RowBox[{"bspecs", "[", 
                    RowBox[{"[", "2", "]"}], "]"}], ",", "2"}], "]"}]}]}], 
                 "]"}]}], "}"}], "]"}], "]"}], "}"}], "]"}], ",", 
          RowBox[{"ImageSize", "\[Rule]", "Medium"}]}], "]"}]}], 
       "\[IndentingNewLine]", "}"}]}]}], "\[IndentingNewLine]", "]"}]}], 
  "\[IndentingNewLine]", "]"}]], "Input"]
}, Closed]],

Cell[CellGroupData[{

Cell["BiMaxwellian", "Section"],

Cell[BoxData[{
 RowBox[{"Clear", "[", 
  RowBox[{"Np", ",", "beta1", ",", "T2OT1", ",", "vd"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"particles", "=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", "particles", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"<<", "\"\<BiMaxwellianDistribution.m\>\""}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", 
       RowBox[{"{", 
        RowBox[{"v1", ",", "v2"}], "}"}], "*)"}], "\[IndentingNewLine]", 
      "particles"}]}], "\[IndentingNewLine]", "]"}]}], ";"}]}], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"Dimensions", "[", "particles", "]"}], ",", "Np"}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{"With", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"v3", "=", 
      RowBox[{
       RowBox[{"Transpose", "[", "particles", "]"}], "-", 
       RowBox[{"{", 
        RowBox[{"vd", ",", "0"}], "}"}]}]}], ",", 
     RowBox[{"\[Beta]", "=", "beta1"}], ",", 
     RowBox[{"T2OT1", "=", "T2OT1"}], ",", 
     RowBox[{"vmax", "=", "3.5"}], ",", 
     RowBox[{"dv", "=", ".2"}]}], "}"}], ",", 
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Show", "[", 
      RowBox[{
       RowBox[{"Histogram", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"v3", "[", 
           RowBox[{"[", "1", "]"}], "]"}], "/", 
          SqrtBox["\[Beta]"]}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"-", "vmax"}], ",", "vmax", ",", "dv"}], "}"}], ",", 
         "\"\<PDF\>\""}], "]"}], ",", 
       RowBox[{"Plot", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"PDF", "[", 
           RowBox[{"NormalDistribution", "[", 
            RowBox[{"0", ",", 
             RowBox[{"1", "/", 
              SqrtBox["2"]}]}], "]"}], "]"}], "[", "v", "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"v", ",", 
           RowBox[{"-", "vmax"}], ",", "vmax"}], "}"}], ",", 
         RowBox[{"PlotRange", "\[Rule]", "All"}]}], "]"}]}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Show", "[", 
      RowBox[{
       RowBox[{"Histogram", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"v3", "[", 
           RowBox[{"[", "2", "]"}], "]"}], "/", 
          SqrtBox[
           RowBox[{"\[Beta]", " ", "T2OT1"}]]}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "vmax", ",", 
           RowBox[{"dv", "/", "2"}]}], "}"}], ",", "\"\<PDF\>\""}], "]"}], 
       ",", 
       RowBox[{"Plot", "[", 
        RowBox[{
         RowBox[{"Divide", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"Exp", "[", 
             RowBox[{"-", 
              RowBox[{"v", "^", "2"}]}], "]"}], "v"}], ",", 
           RowBox[{"1", "/", "2"}]}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"v", ",", "0", ",", "vmax"}], "}"}], ",", 
         RowBox[{"PlotRange", "\[Rule]", "All"}]}], "]"}]}], "]"}]}], 
    "\[IndentingNewLine]", "}"}]}], "]"}]], "Input"]
}, Closed]],

Cell[CellGroupData[{

Cell["MaxwellianRing", "Section"],

Cell[BoxData[{
 RowBox[{"Clear", "[", 
  RowBox[{"Np", ",", "beta1", ",", "vr"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"particles", "=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", "particles", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"<<", "\"\<MaxwellianRingDistribution.m\>\""}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", 
       RowBox[{"{", 
        RowBox[{"v1", ",", "v2"}], "}"}], "*)"}], "\[IndentingNewLine]", 
      "particles"}]}], "\[IndentingNewLine]", "]"}]}], ";"}]}], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"Dimensions", "[", "particles", "]"}], ",", "Np"}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{"With", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"vv", "=", 
      RowBox[{
       RowBox[{"Transpose", "[", "particles", "]"}], "/", 
       RowBox[{"Sqrt", "[", "beta1", "]"}]}]}], ",", 
     RowBox[{"vr", "=", 
      RowBox[{"vr", "/", 
       RowBox[{"Sqrt", "[", "beta1", "]"}]}]}], ",", 
     RowBox[{"vmax", "=", "3.5"}], ",", 
     RowBox[{"dv", "=", ".2"}], ",", 
     RowBox[{"pdf", "=", 
      RowBox[{"PDF", "[", 
       RowBox[{"NormalDistribution", "[", 
        RowBox[{"0", ",", 
         RowBox[{"1", "/", 
          SqrtBox["2"]}]}], "]"}], "]"}]}]}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"v1", ",", "v2"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{"{", 
        RowBox[{"v1", ",", "v2"}], "}"}], "=", "vv"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Show", "[", 
         RowBox[{
          RowBox[{"Histogram", "[", 
           RowBox[{"v1", ",", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"-", "vmax"}], ",", "vmax", ",", "dv"}], "}"}], ",", 
            "\"\<PDF\>\""}], "]"}], ",", 
          RowBox[{"Plot", "[", 
           RowBox[{
            RowBox[{"pdf", "[", "v", "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"v", ",", 
              RowBox[{"-", "vmax"}], ",", "vmax"}], "}"}], ",", 
            RowBox[{"PlotRange", "\[Rule]", "All"}]}], "]"}]}], "]"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{"Show", "[", 
         RowBox[{
          RowBox[{"Histogram", "[", 
           RowBox[{
            RowBox[{"v2", "-", "vr"}], ",", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"-", 
               RowBox[{"If", "[", 
                RowBox[{
                 RowBox[{"vr", "<", "vmax"}], ",", "vr", ",", "vmax"}], 
                "]"}]}], ",", "vmax", ",", "dv"}], "}"}], ",", 
            "\"\<PDF\>\""}], "]"}], ",", 
          RowBox[{"Plot", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"Divide", "[", 
              RowBox[{
               RowBox[{"Exp", "[", 
                RowBox[{"-", 
                 RowBox[{"v", "^", "2"}]}], "]"}], ",", 
               RowBox[{".5", 
                RowBox[{"(", 
                 RowBox[{
                  RowBox[{"Exp", "[", 
                   RowBox[{"-", 
                    RowBox[{"vr", "^", "2"}]}], "]"}], "+", 
                  RowBox[{
                   SqrtBox["\[Pi]"], "vr", 
                   RowBox[{"(", 
                    RowBox[{"1", "+", 
                    RowBox[{"Erf", "[", "vr", "]"}]}], ")"}]}]}], ")"}]}]}], 
              "]"}], 
             RowBox[{"(", 
              RowBox[{"v", "+", "vr"}], ")"}]}], ",", 
            RowBox[{"{", 
             RowBox[{"v", ",", 
              RowBox[{"-", 
               RowBox[{"If", "[", 
                RowBox[{
                 RowBox[{"vr", "<", "vmax"}], ",", "vr", ",", "vmax"}], 
                "]"}]}], ",", "vmax"}], "}"}], ",", 
            RowBox[{"PlotRange", "\[Rule]", "All"}]}], "]"}]}], "]"}]}], 
       "\[IndentingNewLine]", "}"}]}]}], "\[IndentingNewLine]", "]"}]}], 
  "\[IndentingNewLine]", "]"}]], "Input"]
}, Closed]],

Cell[CellGroupData[{

Cell["PartialShell", "Section"],

Cell[BoxData[{
 RowBox[{"Clear", "[", 
  RowBox[{"Np", ",", "beta", ",", "vs", ",", "aniIdx"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"particles", "=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", "particles", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"<<", "\"\<PartialShellDistribution.m\>\""}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", 
       RowBox[{"{", 
        RowBox[{"v", ",", "\[Alpha]"}], "}"}], "*)"}], "\[IndentingNewLine]", 
      "particles"}]}], "\[IndentingNewLine]", "]"}]}], ";"}]}], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"Dimensions", "[", "particles", "]"}], ",", "Np"}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{"With", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"vv", "=", 
      RowBox[{
       RowBox[{"Transpose", "[", "particles", "]"}], "/", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Sqrt", "[", "beta", "]"}], ",", "1"}], "}"}]}]}], ",", 
     RowBox[{"vs", "=", 
      RowBox[{"vs", "/", 
       RowBox[{"Sqrt", "[", "beta", "]"}]}]}], ",", 
     RowBox[{"n", "=", "aniIdx"}], ",", 
     RowBox[{"vmax", "=", "3.5"}], ",", 
     RowBox[{"dv", "=", ".2"}]}], "}"}], ",", "\[IndentingNewLine]", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"v", ",", "\[Alpha]"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{"{", 
        RowBox[{"v", ",", "\[Alpha]"}], "}"}], "=", "vv"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"Show", "[", 
         RowBox[{
          RowBox[{"Histogram", "[", 
           RowBox[{
            RowBox[{"v", "-", "vs"}], ",", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"-", "vmax"}], ",", "vmax", ",", "dv"}], "}"}], ",", 
            "\"\<PDF\>\""}], "]"}], ",", 
          RowBox[{"Plot", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"Divide", "[", 
              RowBox[{
               RowBox[{"Exp", "[", 
                RowBox[{"-", 
                 RowBox[{"v", "^", "2"}]}], "]"}], ",", 
               RowBox[{".25", 
                RowBox[{"(", 
                 RowBox[{
                  RowBox[{"2", 
                   RowBox[{"Exp", "[", 
                    RowBox[{"-", 
                    RowBox[{"vs", "^", "2"}]}], "]"}], "vs"}], "+", 
                  RowBox[{
                   SqrtBox["\[Pi]"], 
                   RowBox[{"(", 
                    RowBox[{"1", "+", 
                    RowBox[{"2", 
                    RowBox[{"vs", "^", "2"}]}]}], ")"}], 
                   RowBox[{"(", 
                    RowBox[{"1", "+", 
                    RowBox[{"Erf", "[", "vs", "]"}]}], ")"}]}]}], ")"}]}]}], 
              "]"}], 
             RowBox[{
              RowBox[{"(", 
               RowBox[{"v", "+", "vs"}], ")"}], "^", "2"}]}], ",", 
            RowBox[{"{", 
             RowBox[{"v", ",", 
              RowBox[{"-", "vmax"}], ",", "vmax"}], "}"}], ",", 
            RowBox[{"PlotRange", "\[Rule]", "All"}]}], "]"}]}], "]"}], ",", 
        "\[IndentingNewLine]", 
        RowBox[{"Show", "[", 
         RowBox[{
          RowBox[{"Histogram", "[", 
           RowBox[{
            RowBox[{"\[Alpha]", "/", "Degree"}], ",", 
            RowBox[{"{", 
             RowBox[{"0", ",", "180", ",", "6"}], "}"}], ",", "\"\<PDF\>\""}],
            "]"}], ",", 
          RowBox[{"Plot", "[", 
           RowBox[{
            RowBox[{"Divide", "[", 
             RowBox[{
              RowBox[{
               RowBox[{"Power", "[", 
                RowBox[{
                 RowBox[{"Sin", "[", 
                  RowBox[{"x", " ", "Degree"}], "]"}], ",", 
                 RowBox[{"n", "+", "1"}]}], "]"}], 
               RowBox[{"\[Pi]", "/", "180"}]}], ",", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{
                 RowBox[{"Sqrt", "[", "Pi", "]"}], 
                 RowBox[{"Gamma", "[", 
                  RowBox[{"1", "+", 
                   RowBox[{"n", "/", "2"}]}], "]"}]}], ")"}], "/", 
               RowBox[{"Gamma", "[", 
                RowBox[{
                 RowBox[{"(", 
                  RowBox[{"3", "+", "n"}], ")"}], "/", "2"}], "]"}]}]}], 
             "]"}], ",", 
            RowBox[{"{", 
             RowBox[{"x", ",", "0", ",", "180"}], "}"}], ",", 
            RowBox[{"PlotRange", "\[Rule]", "All"}]}], "]"}]}], "]"}]}], 
       "\[IndentingNewLine]", "}"}]}]}], "\[IndentingNewLine]", "]"}]}], 
  "\[IndentingNewLine]", "]"}]], "Input"]
}, Closed]],

Cell[CellGroupData[{

Cell["SubtractedMaxwellian", "Section"],

Cell[BoxData[{
 RowBox[{"Clear", "[", 
  RowBox[{"Np", ",", "beta1", ",", "T2OT1", ",", "n2On1"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"particles", "=", 
   RowBox[{"Block", "[", 
    RowBox[{
     RowBox[{"{", "particles", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"<<", "\"\<SubtractedMaxwellianDistribution.m\>\""}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", 
       RowBox[{"{", "v", "}"}], "*)"}], "\[IndentingNewLine]", 
      "particles"}]}], "\[IndentingNewLine]", "]"}]}], ";"}]}], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"Dimensions", "[", "particles", "]"}], ",", "Np"}], "}"}]], "Input"],

Cell[BoxData[
 RowBox[{"With", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"vv", "=", 
      RowBox[{
       RowBox[{"Transpose", "[", "particles", "]"}], "/", 
       RowBox[{"Sqrt", "[", "beta1", "]"}]}]}], ",", 
     RowBox[{"T2OT1", "=", "T2OT1"}], ",", 
     RowBox[{"n2On1", "=", "n2On1"}], ",", 
     RowBox[{"vmax", "=", "4"}], ",", 
     RowBox[{"dv", "=", ".2"}], ",", 
     RowBox[{"pdf", "=", 
      RowBox[{"Function", "[", 
       RowBox[{"x", ",", 
        RowBox[{"Divide", "[", 
         RowBox[{
          RowBox[{
           RowBox[{"Exp", "[", 
            RowBox[{"-", 
             RowBox[{"x", "^", "2"}]}], "]"}], 
           RowBox[{"x", "^", "2"}]}], ",", 
          RowBox[{
           SqrtBox["\[Pi]"], "/", "4"}]}], "]"}]}], "]"}]}]}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"v", ",", "n1", ",", "n2"}], "}"}], ",", "\[IndentingNewLine]", 
     
     RowBox[{
      RowBox[{
       RowBox[{"{", "v", "}"}], "=", "vv"}], ";", "\[IndentingNewLine]", 
      RowBox[{"n1", "=", 
       RowBox[{"Divide", "[", 
        RowBox[{"1", ",", 
         RowBox[{"1", "-", "n2On1"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"n2", "=", 
       RowBox[{"n1", " ", "n2On1"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"Show", "[", 
       RowBox[{
        RowBox[{"Histogram", "[", 
         RowBox[{"v", ",", 
          RowBox[{"{", 
           RowBox[{"0", ",", "vmax", ",", 
            RowBox[{"dv", "/", "2"}]}], "}"}], ",", "\"\<PDF\>\""}], "]"}], 
        ",", 
        RowBox[{"Plot", "[", 
         RowBox[{
          RowBox[{"Subtract", "[", 
           RowBox[{
            RowBox[{"n1", " ", 
             RowBox[{"pdf", "[", "x", "]"}]}], ",", 
            RowBox[{"n2", " ", 
             RowBox[{
              RowBox[{"pdf", "[", 
               RowBox[{"x", "/", 
                RowBox[{"Sqrt", "[", "T2OT1", "]"}]}], "]"}], "/", 
              RowBox[{"Sqrt", "[", "T2OT1", "]"}]}]}]}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"x", ",", "0", ",", "vmax"}], "}"}], ",", 
          RowBox[{"PlotRange", "\[Rule]", "All"}]}], "]"}]}], "]"}]}]}], 
    "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", "]"}]], "Input"]
}, Open  ]]
},
WindowSize->{1258, 1192},
WindowMargins->{{Automatic, 77}, {Automatic, 31}},
FrontEndVersion->"10.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (December 10, \
2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 171, 3, 46, "Input"],
Cell[CellGroupData[{
Cell[754, 27, 30, 0, 64, "Section"],
Cell[787, 29, 640, 17, 114, "Input"],
Cell[1430, 48, 119, 3, 28, "Input"],
Cell[1552, 53, 515, 15, 46, "Input"],
Cell[2070, 70, 259, 8, 28, "Input"],
Cell[2332, 80, 3279, 87, 199, "Input"]
}, Closed]],
Cell[CellGroupData[{
Cell[5648, 172, 31, 0, 50, "Section"],
Cell[5682, 174, 573, 15, 114, "Input"],
Cell[6258, 191, 119, 3, 28, "Input"],
Cell[6380, 196, 2268, 66, 126, "Input"]
}, Closed]],
Cell[CellGroupData[{
Cell[8685, 267, 33, 0, 50, "Section"],
Cell[8721, 269, 558, 14, 114, "Input"],
Cell[9282, 285, 119, 3, 28, "Input"],
Cell[9404, 290, 3322, 92, 270, "Input"]
}, Closed]],
Cell[CellGroupData[{
Cell[12763, 387, 31, 0, 50, "Section"],
Cell[12797, 389, 578, 15, 114, "Input"],
Cell[13378, 406, 119, 3, 28, "Input"],
Cell[13500, 411, 3904, 105, 286, "Input"]
}, Closed]],
Cell[CellGroupData[{
Cell[17441, 521, 39, 0, 50, "Section"],
Cell[17483, 523, 553, 14, 114, "Input"],
Cell[18039, 539, 119, 3, 28, "Input"],
Cell[18161, 544, 2274, 63, 170, "Input"]
}, Open  ]]
}
]
*)

