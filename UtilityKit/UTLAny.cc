//
//  UTLAny.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/30/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLAny.h"
#include "UTLAny.hh"
#include "UTLPrinto.h"

// MARK:- Version 4
//

UTL::__4_::Any::~Any()
{
}

UTL::__4_::Any::Any(Any const& o)
: Any(o._ptr->any()) { // move constructor should be triggered
}
UTL::__4_::Any::Any(Any&& o) noexcept
: _ptr(std::move(o._ptr)) {
}

auto UTL::__4_::Any::operator=(Any const& rhs)
-> Any& {
    if (this != &rhs) {
        Any(rhs).swap(*this);
    }
    return *this;
}
auto UTL::__4_::Any::operator=(Any&& rhs) noexcept
-> Any& {
    if (this != &rhs) {
        Any(std::move(rhs)).swap(*this);
    }
    return *this;
}

void UTL::__4_::Any::reset() noexcept
{
    _ptr.reset();
}

const std::type_info& UTL::__4_::Any::type() const noexcept
{
    if (*this) return _ptr->type();
    else return typeid(void);
}
