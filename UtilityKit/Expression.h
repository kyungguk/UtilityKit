//
//  Expression.h
//  ParticleInCell
//
//  Created by KYUNGGUK MIN on 11/20/15.
//
//

#ifndef Expression_h
#define Expression_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
namespace Expression {

    /**
     @brief Pair expression.
     */
    template <class X, class Y>
    struct Pair {
        inline X _1() const noexcept { return x; }
        inline Y _2() const noexcept { return y; }

        constexpr Pair(X _x, Y _y) noexcept : x(_x), y(_y) {}
    protected:
        X x;
        Y y;
    };

} // namespace Expression
} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* Expression_h */
