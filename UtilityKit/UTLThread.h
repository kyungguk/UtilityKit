//
//  UTLThread.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/23/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLThread_h
#define UTLThread_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <functional>
#include <pthread.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief C++ wrapper for a single pthread execution.
     @discussion It is a replacement of std::thread. Some compilers do not seem to provide this implementation.
     */
    class Thread {
        pthread_t __t_;

        // copy construct/assignment deleted:
        Thread(Thread const&) = delete;
        Thread& operator=(Thread const&) = delete;
    public:
        typedef pthread_t native_handle_type;

        /**
         @brief Returns the number of processors.
         @discussion Terminate on internal error.
         */
        static unsigned hardware_concurrency() noexcept;

        /**
         @brief Destructor.
         @discussion Terminates if `*this' is joinable.
         */
        ~Thread();

        /**
         @brief Create an empty thread whose state is invalid.
         */
        explicit Thread() noexcept;
        /**
         @brief Move constructor.
         */
        Thread(Thread&&) noexcept;
        /**
         @brief Create a single thread of execution and call the passed code block.
         @discussion The Block object should be copy-constructable and callable with no argument.
         The Block object is copied before thread execution and released before thread exit.
         The return value will be ignored.

         On successful construction, the block object is called on the created thread and `*this' becomes valid.

         If the thread creation fails, std::runtime_error exception is returned.
         If the block object throws an exception, std::terminate() is called on the thread.
         @param b A callable function object or a function pointer with no argument.
         */
        explicit Thread(std::function<void(void)> const b);

        /**
         @brief Move assignment.
         @discussion If *this still has an associated running thread (i.e. this->joinable() == true), call std::terminate(). Otherwise, assigns the state of other to *this and sets other to a default constructed state.
         After this call, this->native_handle() is equal to the value of other.native_handle() prior to the call, and other no longer represents a thread of execution.
         */
        Thread& operator=(Thread&&) noexcept;
        /**
         @brief Swap content of the receiver with other Thread object.
         */
        void swap(Thread&) noexcept;

        /**
         @brief Test whether the thread is valid and joinable.
         */
        bool joinable() const noexcept;
        /**
         @brief Wait until the thread execution exits.
         */
        void join();
        /**
         @brief Detach the thread execution.
         @discussion After this call, joinable() returns false.
         */
        void detach();

        /**
         @brief Return native handle of the type `pthread_t' associated with `*this'.
         */
        native_handle_type native_handle() noexcept { return __t_; }
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE

// MARK: std::swap
namespace std {
    inline void swap(UTILITYKIT_NAMESPACE::__3_::Thread& x, UTILITYKIT_NAMESPACE::__3_::Thread& y) noexcept(noexcept(x.swap(y))) {
        x.swap(y);
    }
}


// MARK:- Version 4
//
#include <functional>
#include <pthread.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {

    /**
     @brief C++ wrapper for a single pthread execution.
     @discussion It is a replacement of std::thread. Some compilers do not seem to provide this implementation.
     */
    class Thread {
        pthread_t __t_;

        // copy construct/assignment deleted:
        Thread(Thread const&) = delete;
        Thread& operator=(Thread const&) = delete;
    public:
        using native_handle_type = pthread_t;

        /**
         @brief Returns the number of processors.
         @discussion Terminate on internal error.
         */
        static unsigned hardware_concurrency() noexcept;

        /**
         @brief Causes the calling thread to relinquish the CPU (immediately calls `sched_yield()').
         @discussion sched_yield() causes the calling thread to relinquish the CPU.
         The thread is moved to the end of the queue for its static priority and a new thread gets to run.
         If the calling thread is the only thread in the highest priority list at that time, it will continue to run after a call to sched_yield().
         @note Strategic calls to sched_yield() can improve performance by giving other threads or processes a chance to run when (heavily) contended resources (e.g., mutexes) have been released by the caller.
         Avoid calling sched_yield() unnecessarily or inappropriately (e.g., when resources needed by other schedulable threads are still held by the caller), since doing so will result in unnecessary context switches, which will degrade system performance.
         @return On success, sched_yield() returns 0. On error, -1 is returned, and errno is set appropriately.
         */
        static int yield(void) noexcept;

        /**
         @brief Destructor.
         @discussion Terminates if `*this' is joinable.
         */
        ~Thread();

        /**
         @brief Create an empty thread whose state is invalid.
         */
        explicit Thread() noexcept;
        /**
         @brief Move constructor.
         */
        Thread(Thread&&) noexcept;
        /**
         @brief Create a single thread of execution and call the passed code block.
         @discussion The Callable object and parameter pack arguments are copied/moved before thread creation.
         The return value of the Callable object will be ignored.

         On successful construction, the Callable object is called with the passed arguments on the created thread and `*this' becomes valid.

         If the thread creation fails, std::runtime_error exception is returned.
         If the Callable object throws an exception, std::terminate() is called on the thread in which it has been called.
         @param f A callable function object or a function pointer with zero or more arguments.
         @param args Any number of arguments that should be passed to f.
         */
        template <class Callable, class... Args>
        explicit Thread(Callable&& f, Args&&... args) : Thread() { __t_ = _invoke(std::bind(decay_copy(std::forward<Callable>(f)), decay_copy(std::forward<Args>(args))...)); }

        /**
         @brief Move assignment.
         @discussion If *this still has an associated running thread (i.e. this->joinable() == true), call std::terminate(). Otherwise, assigns the state of other to *this and sets other to a default constructed state.
         After this call, this->native_handle() is equal to the value of other.native_handle() prior to the call, and other no longer represents a thread of execution.
         */
        Thread& operator=(Thread&&) noexcept;
        /**
         @brief Swap content of the receiver with other Thread object.
         */
        void swap(Thread&) noexcept;

        /**
         @brief Test whether the thread is valid and joinable.
         */
        bool joinable() const noexcept;
        /**
         @brief Wait until the thread execution exits.
         */
        void join();
        /**
         @brief Detach the thread execution.
         @discussion After this call, joinable() returns false.
         */
        void detach();

        /**
         @brief Return native handle of the type `pthread_t' associated with `*this'.
         */
        native_handle_type native_handle() noexcept { return __t_; }

    private:
        template <class T>
        static typename std::decay<T>::type decay_copy(T&& v) { return std::forward<T>(v); }
        static native_handle_type _invoke(std::function<void(void)>&& b);
    };

} // namespace __4_
UTILITYKIT_END_NAMESPACE

// MARK: std::swap
namespace std {
    inline void swap(UTILITYKIT_NAMESPACE::__4_::Thread& x, UTILITYKIT_NAMESPACE::__4_::Thread& y) noexcept(noexcept(x.swap(y))) {
        x.swap(y);
    }
}

#endif /* UTLThread_h */
