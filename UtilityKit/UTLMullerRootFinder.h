//
//  UTLMullerRootFinder.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLMullerRootFinder_h
#define UTLMullerRootFinder_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLOptional.h>
#include <utility>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <complex>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Muller's complex root finder.
     @discussion It find root of `F(x) = 0` where the template parameter F should take a value of type std::complex<double> at its sole argument and return a value convertible to std::complex<double> type.
     */
    template <class F>
    struct MullerRootFinder {
        using value_type = std::complex<double>;
        using optional_type = Optional<value_type>;

    private:
        F _f;
    public:
        double reltol{1e-5}; //!< Relative tolerance.
        double abstol{1e-10}; //!< Absolute tolerance.
        unsigned max_iterations{5}; //!< Maximum number of iterations.

        /**
         @brief Construct with a function to solve.
         @param f An instance of type F representing `f(x)` which is zero at the root (it should be copy-constructable).
         The argument of `f` is guaranteed to be a finite number.
         */
        explicit MullerRootFinder(F const& f) : _f(f) {}

        /**
         @brief Find a root of `f(x) = 0` using three dissimilar initial guesses.
         @discussion std::isfinite is checked against the function values, so the function object can return NaN to indicate evaluation failure.

         The StepMonitor should be a callable object with signiture `bool(unsigned, value_type)`, where the two arguments are the current iteration count and the root estimate, respectively.
         Returning false upon call exit indicates immediate termination.

         @return A root wrapped around the optional if found any.
         An empty optional is returned when the convergence failed within max_iterations, when the root is not finite, or when the step monitor returns false.
         */
        template <class StepMonitor>
        optional_type operator()(value_type x0, value_type x1, value_type x2, StepMonitor&& monitor) const;

        /**
         @brief Find a root without monitoring iteration steps.
         */
        optional_type operator()(value_type x0, value_type x1, value_type x2) const {
            return (*this)(x0, x1, x2, &_null_monitor);
        }

    private:
        static inline bool isfinite(value_type const &x) noexcept {
            return std::isfinite(x.real()) && std::isfinite(x.imag());
        }
        static inline double abs_max(value_type const &x0, value_type const &x1, value_type const &x2) noexcept {
            double const elems[3]{std::abs(x0), std::abs(x1), std::abs(x2)};
            return *std::max_element(std::begin(elems), std::end(elems));
        }
        static bool _null_monitor(decltype(max_iterations) const&, value_type const&) noexcept {
            return true;
        }
    };

    // MARK: Out-of-line implementations
    template <class F>
    template <class StepMonitor>
    auto MullerRootFinder<F>::operator()(value_type x0, value_type x1, value_type x2, StepMonitor&& monitor) const
    ->optional_type {
        F const& f = _f;

        // check for initial guesses
        //
        if (!(isfinite( x0) && isfinite( x1) && isfinite( x2)) ) return {};
        value_type fx0 = f(x0), fx1 = f(x1), fx2 = f(x2);
        if (!(isfinite(fx0) && isfinite(fx1) && isfinite(fx2)) ) return {};

        // iteration
        //
        constexpr value_type two(2.);
        for (decltype(max_iterations) i = 0; i < max_iterations && monitor(i, x2); ++i) {
            auto const h1 = x1 - x0;
            auto const h2 = x2 - x1;
            auto const h3 = x2 - x0;
            auto const d1 = (fx1 - fx0)/h1;
            auto const d2 = (fx2 - fx1)/h2;
            auto const d3 = (fx2 - fx0)/h3;
            auto const b = d3 + d2 - d1;
            auto const _2sqrtFx2d = two * std::sqrt(fx2*(fx2*h1 + fx0*h2 - fx1*h3) / (h1*h2*h3));
            auto const D = std::sqrt((b+_2sqrtFx2d) * (b-_2sqrtFx2d));
            value_type const E = (std::abs(b-D) > std::abs(b+D) ? b-D : b+D);

            auto const h = -two * fx2/E;
            if (!isfinite(h)) {
                return {};
            } else if (/*std::abs(h) < abstol || */std::abs(h) < reltol*abs_max(x0, x1, x2)) {
                return x2 + h;
            } else {
                x0 = x1; fx0 = fx1;
                x1 = x2; fx1 = fx2;
                x2 = x2 + h, fx2 = f(x2);
            }
        }

        return {};
    }

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLOptional.h>
#include <type_traits>
#include <complex>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class RealType>
    struct MullerRootFinder;

    /**
     @brief Muller's complex root finder.
     */
    template <>
    struct MullerRootFinder<double> {
        using real_type = double;
        using complex_type = std::complex<real_type>;
        using optional_type = Optional<complex_type>;

    public:
        real_type reltol{1e-5}; //!< Relative tolerance.
        real_type abstol{1e-10}; //!< Absolute tolerance.
        unsigned max_iterations{5}; //!< Maximum number of iterations.

        /**
         @brief Find a complex root of a function, f(z), using three dissimilar initial guesses.
         @discussion std::isfinite is checked against the function values, so the function object can return NaN to indicate evaluation failure.

         The StepMonitor should be a callable object with signiture `bool(unsigned, complex_type)`, where the two arguments are the current iteration count and the root estimate, respectively.
         Returning false indicates an immediate termination.

         @param f A function of signature, `complex_type(complex_type)`, whose root is to be found.
         @param x0 First initial guess.
         @param x1 Second initial guess.
         @param x2 Third initial guess.
         @param monitor A step monitor callback of signature, `bool(unsigned, complex_type)`.

         @return An optional wrapping the root, if found any.
         An empty optional is returned when the convergence failed within max_iterations, when the root is not finite, or when the step monitor returns false.
         */
        template <class F, class StepMonitor>
        inline optional_type operator()(F&& f, complex_type x0, complex_type x1, complex_type x2, StepMonitor&& monitor) const;

        /**
         @brief Find a root without monitoring iteration steps.
         */
        template <class F>
        optional_type operator()(F&& f, complex_type const &x0, complex_type const &x1, complex_type const &x2) const {
            return (*this)(std::forward<F>(f), x0, x1, x2, &null_monitor);
        }

    private:
        static bool null_monitor(unsigned, complex_type) noexcept { return true; }

        static inline bool isfinite(complex_type const &x) noexcept;
        static inline real_type abs_max(complex_type const &x0, complex_type const &x1, complex_type const &x2) noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLMullerRootFinder.hh>

#endif /* UTLMullerRootFinder_h */
