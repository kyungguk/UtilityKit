//
//  UTLVector__1.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 2/27/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLVector__1_hh
#define UTLVector__1_hh

// MARK:- Version 4
//

// MARK: Assignment
//
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator=(Vector const& o) noexcept(std::is_nothrow_copy_assignable<Type>::value) -> Vector &{
    if (this != &o) {
        this->x = o.x;
    }
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator=(Vector&& o) noexcept(std::is_nothrow_move_assignable<Type>::value) -> Vector &{
    if (this != &o) {
        this->x = std::move(o.x);
    }
    return *this;
}

// MARK: at
//
template <class Type>
Type       &UTL::__4_::Vector<Type, 1L>::at(size_type const i) {
    if (i >= 0 && i < size()) return data()[i];
    throw std::out_of_range(__PRETTY_FUNCTION__);
}
template <class Type>
Type const &UTL::__4_::Vector<Type, 1L>::at(size_type const i) const {
    if (i >= 0 && i < size()) return data()[i];
    throw std::out_of_range(__PRETTY_FUNCTION__);
}

// MARK: Compound Arithmetic
//
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator+=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x += rhs.x;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator+=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x += rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator-=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x -= rhs.x;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator-=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x -= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator*=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x *= rhs.x;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator*=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x *= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator/=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x /= rhs.x;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator/=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x /= rhs;
    return *this;
}

// MARK: Compound Modulus
//
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator%=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x %= rhs.x;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator%=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x %= rhs;
    return *this;
}

// MARK: Compound Bit Operators
//
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator&=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x &= rhs.x;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator&=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x &= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator|=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x |= rhs.x;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator|=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x |= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator^=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x ^= rhs.x;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator^=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x ^= rhs;
    return *this;
}
template <class Type>
auto UTL::__4_::Vector<Type, 1L>::operator<<=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
    this->x <<= rhs.x;
    return *this;
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator<<=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
        this->x <<= rhs;
        return *this;
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator>>=(Vector const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
        this->x >>= rhs.x;
        return *this;
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator>>=(Type   const &rhs) noexcept(std::is_arithmetic<innermost_element_type>::value) -> Vector &{
        this->x >>= rhs;
        return *this;
    }

    // MARK: Comparison
    //
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator==(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x == rhs.x
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator!=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x != rhs.x
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator<=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x <= rhs.x
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator>=(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x >= rhs.x
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator<(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x < rhs.x
        };
    }
    template <class Type>
    auto UTL::__4_::Vector<Type, 1L>::operator>(Vector const &rhs) const noexcept(std::is_arithmetic<innermost_element_type>::value) -> comparison_result_type {
        return comparison_result_type{
            this->x > rhs.x
        };
    }

#endif /* UTLVector__1_hh */
