//
//  UTLShellDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLShellDistribution_h
#define UTLShellDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Shell speed distribution.
     @discussion Isotropic shell speed.

     pdf = v^2*exp(-(v-vs)^2/vth^2) * 4/(√pi vth^3*A), where
     A = 2/√pi vs/vth exp(-vs^2/vth^2) + (2 vs^2/vth^2 + 1) erfc(-vs/vth).
     Integrate[pdf(v), v] = -(2/√pi ((v - vs)/vth + 2 vs/vth) exp(-(v-vs)^2/vth^2) - (2 vs^2/vth^2 + 1) erf((v - vs)/vth)) / A.
     mean = (2/√pi (vs^2/vth^2 + 1) exp(-vs^2/vth^2) + vs/vth (2 vs^2/vth^2 + 3) erfc(-vs/vth)) vth/A.
     variance = (2/√pi vs/vth (vs^2/vth^2 + 5/2) exp(-vs^2/vth^2) + (2 vs^4/vth^4 + 6 vs^2/vth^2 + 3/2) erfc(-vs/vth)) vth^2/A - mean^2.
     */
    class ShellDistribution final : public Distribution {
        using interp_type = MonotoneCubicInterpolation<value_type>;

        interp_type _icdf;
        value_type _vth;
        value_type _vs;

    public:
        // constructors
        //
        /**
         Construct shell distribution with the thermal speed.
         @param vth Thermal speed.
         @param vs Shell speed.
         @param should_normalize_cdf If true, normalize the sampled cdf points. Default is true.
         */
        explicit ShellDistribution(value_type vth, value_type vs, bool should_normalize_cdf = true);

        // copy/move
        //
        ShellDistribution(ShellDistribution const &) noexcept;
        ShellDistribution &operator=(ShellDistribution const &) noexcept;
        ShellDistribution(ShellDistribution &&) noexcept;
        ShellDistribution &operator=(ShellDistribution &&) noexcept;

        // properties
        //
        /**
         Thermal speed (note the definition).
         */
        value_type vth() const noexcept { return _vth; }
        /**
         Shell speed.
         */
        value_type vs() const noexcept { return _vs; }
        /**
         Normalization constant.
         */
        value_type A() const noexcept;

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type v) const override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type v) const override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type cdf) const override;

    private: // helpers:
        value_type _helper_pdf(value_type x) const noexcept;
        value_type _helper_cdf(value_type x) const noexcept;

        using sampler_type = AdaptiveSampling1D<value_type>;
        static sampler_type _sampler() noexcept;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Shell speed distribution.
     @discussion Isotropic shell speed.

     pdf = v^2*exp(-(v-vs)^2/vth^2) * 4/(√pi vth^3*A), where
     A = 2/√pi vs/vth exp(-vs^2/vth^2) + (2 vs^2/vth^2 + 1) erfc(-vs/vth).
     Integrate[pdf(v), v] = -(2/√pi ((v - vs)/vth + 2 vs/vth) exp(-(v-vs)^2/vth^2) - (2 vs^2/vth^2 + 1) erf((v - vs)/vth)) / A.
     mean = (2/√pi (vs^2/vth^2 + 1) exp(-vs^2/vth^2) + vs/vth (2 vs^2/vth^2 + 3) erfc(-vs/vth)) vth/A.
     variance = (2/√pi vs/vth (vs^2/vth^2 + 5/2) exp(-vs^2/vth^2) + (2 vs^4/vth^4 + 6 vs^2/vth^2 + 3/2) erfc(-vs/vth)) vth^2/A - mean^2.
     */
    class ShellDistribution final : public Distribution {
        using spline_coefficient_type = MonotoneCubicCoefficient<value_type>;
        using interpolator_type = spline_coefficient_type::spline_interpolator_type;
        using sampler_type = AdaptiveSampling1D<value_type>;

        value_type _vth;
        value_type _vs;
        interpolator_type _icdf;

    public:
        // constructors
        //
        /**
         Construct shell distribution with the thermal speed.
         @param vth Thermal speed.
         @param vs Shell speed (default is 0).
         */
        explicit ShellDistribution(value_type const vth, value_type const vs = 0);

        // copy/move
        //
        ShellDistribution(ShellDistribution const &);
        ShellDistribution &operator=(ShellDistribution const &);
        ShellDistribution(ShellDistribution &&);
        ShellDistribution &operator=(ShellDistribution &&);

        // swap
        //
        void swap(ShellDistribution &o);

        // properties
        //
        /**
         Thermal speed (note the definition).
         */
        value_type vth() const noexcept { return _vth; }
        /**
         Shell speed.
         */
        value_type vs() const noexcept { return _vs; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type const v) const noexcept override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type const v) const noexcept override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type const cdf) const override;

    private: // helpers:
        static sampler_type default_sampler() noexcept;

        static inline value_type shell_pdf(value_type const x/*v/vth*/, value_type const b/*vs/vth*/) noexcept;
        static inline value_type shell_cdf(value_type const x/*v/vth*/, value_type const b/*vs/vth*/) noexcept;
        static inline value_type A(value_type const b/*vs/vth*/) noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLShellDistribution_h */
