//
//  UTLRandomVariate.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLRandomVariate_h
#define UTLRandomVariate_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    class RandomReal;
    class Distribution;

    /**
     @brief Generate random variates from a distribution.
     */
    class RandomVariate {
        RandomReal *_rng;
        Distribution const *_dist;

    public:
        using value_type = double;
        using size_type = unsigned long;

        /**
         @brief Construct object with invalid state.
         */
        explicit RandomVariate() noexcept = default;

        /**
         @brief Initialize with a random number generator and a distribution.
         @discussion `*this` does not take the ownership.
         */
        RandomVariate(RandomReal *rng, Distribution const *dist) noexcept;

        /**
         @brief Generate a random variate.
         @discussion Can be used with `std::generate`.
         */
        value_type operator()() const;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    class RandomReal;
    class Distribution;

    /**
     @brief Generate random variates from a distribution.
     */
    class RandomVariate {
        RandomReal *_rng{nullptr};
        Distribution const *_dist{nullptr};

    public:
        using value_type = double;

        // move
        //
        RandomVariate(RandomVariate &&) noexcept = default;
        RandomVariate &operator=(RandomVariate &&) noexcept = default;

        /**
         @brief Construct object with invalid state.
         */
        explicit RandomVariate() noexcept = default;

        /**
         @brief Initialize with a random number generator and a distribution.
         @discussion `*this` does not take the ownership.
         */
        RandomVariate(RandomReal *rng, Distribution const *dist) noexcept;

        /**
         @brief Generate a random variate.
         @discussion Can be used with `std::generate`.
         */
        value_type operator()() const;

        /**
         @brief Returns true if the state is valid.
         */
        explicit operator bool() const noexcept { return _rng && _dist; }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLRandomVariate_h */
