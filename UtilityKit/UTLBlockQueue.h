//
//  UTLBlockQueue.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/24/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLBlockQueue_h
#define UTLBlockQueue_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLCondV.h>
#include <functional>
#include <utility>
#include <queue>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
    /**
     @brief FIFO queue of blocks.
     */
    template <typename Lock>
    class BlockQueue {
        typedef Lock lock_type;
        typedef std::function<void(void)> block_type;

        lock_type _mx;
        CondV<lock_type> _cv;
        std::queue<block_type> _q;

    public:
        explicit BlockQueue() : _mx(), _cv(), _q() {}

        /**
         @brief Enqueue a block.
         */
        void enqueue(block_type const block);

        /**
         @brief Dequeue a block.
         @discussion Call is blocked if the queue is empty.
         */
        auto dequeue() -> block_type;

        /**
         @brief Dequeue a block if the queue is not empty.
         @discussion The first element is set to true if a block (the second element) is returned.
         */
        auto try_dequeue() -> std::pair<bool, block_type>;
    };
    template <typename Lock>
    void BlockQueue<Lock>::enqueue(const block_type block)
    {
        bool was_empty;
        {
            LockG<lock_type> l(_mx);
            was_empty = _q.empty();
            _q.push(block);
        }
        if (was_empty) {
            _cv.notify_all();
        }
    }
    template <typename Lock>
    auto BlockQueue<Lock>::dequeue()
    -> block_type {
        LockG<lock_type> l(_mx);
        while (_q.empty()) {
            _cv.wait(_mx);
        }
        block_type b = _q.front();
        return _q.pop(), b;
    }
    template <typename Lock>
    auto BlockQueue<Lock>::try_dequeue()
    -> std::pair<bool, block_type> {
        LockG<lock_type> l(_mx);
        if (_q.empty()) {
            return std::make_pair(false, block_type());
        } else {
            auto b = std::make_pair(true, _q.front());
            return _q.pop(), b;
        }
    }
} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLLock.h>
#include <UtilityKit/UTLCondV.h>
#include <UtilityKit/UTLOptional.h>
#include <functional>
#include <queue>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class Lock>
    class BlockQueue;

    /**
     @brief FIFO queue of blocks (or tasks wrapped around lambda expression).
     @discussion Implementation based on condition variable.
     */
    template <>
    class BlockQueue<Mutex> {
        using _Block = std::function<void(void)>;

        std::queue<_Block> _q;
        CondV _cv;
        Mutex _mx;

        BlockQueue(BlockQueue const&) = delete;
        BlockQueue& operator=(BlockQueue const&) = delete;
    public:
        ~BlockQueue();
        explicit BlockQueue();

        /**
         @brief Enqueue a block.
         @discussion Ownership is transferred to the queue.
         */
        void enqueue(std::function<void(void)> const &block) { enqueue(std::function<void(void)>{block}); }
        void enqueue(std::function<void(void)> &&block);

        /**
         @brief Dequeue a block.
         @discussion Call is blocked if the queue is empty.
         */
        std::function<void(void)> dequeue();

        /**
         @brief Dequeue a block if any.
         @discussion The first element is set to true if a block (the second element) is returned.
         */
        Optional<std::function<void(void)>> try_dequeue();
    };

    /**
     @brief FIFO queue of blocks (or tasks wrapped around lambda expression).
     @discussion Implementation based on spin lock.
     */
    template <>
    class BlockQueue<SpinLock> {
        using _Block = std::function<void(void)>;

        std::queue<_Block> _block_q;
        std::queue<SpinLock *> _consumer_q;
        SpinLock _key;

        BlockQueue(BlockQueue const&) = delete;
        BlockQueue& operator=(BlockQueue const&) = delete;
    public:
        ~BlockQueue();
        explicit BlockQueue();

        /**
         @brief Enqueue a block.
         @discussion Ownership of the blick is transferred to the queue.
         */
        void enqueue(std::function<void(void)> const &block) { enqueue(std::function<void ()>{block}); }
        void enqueue(std::function<void(void)> &&block);

        /**
         @brief Dequeue a block.
         @discussion Calling thread is blocked if the queue is empty.
         Ownership of the block is transferred to the caller.
         */
        std::function<void(void)> dequeue();

        /**
         @brief Dequeue a block if any.
         @discussion Calling thread is not blocked even if the queue is empty, in which case, null optional is returned.
         */
        Optional<std::function<void(void)>> try_dequeue();
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLBlockQueue_h */
