//
//  SIMDVectorTraits.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 7/16/16.
//  Copyright © 2016 KYUNGGUK MIN. All rights reserved.
//

#ifndef SIMDVectorTraits_h
#define SIMDVectorTraits_h

#include <UtilityKit/UtilityKit-config.h>

// MARK: Version 3
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {
namespace SIMD {

// for test:
//#if !defined(VECTOR_KIT_NO_SIMD)
//#define VECTOR_KIT_NO_SIMD
//#endif

// check for simd:
#ifndef __has_attribute      // Optional of course.
#define __has_attribute(x) 0 // Compatibility with non-clang compilers.
#endif

#if !defined(VECTOR_KIT_NO_SIMD) && !__has_attribute( vector_size )
#define VECTOR_KIT_NO_SIMD
#endif

#if !defined(VECTOR_KIT_NO_SIMD)
#define __VECTOR_KIT_VECTOR_TYPEDEF(base_type, sizeof, length, alias) typedef base_type alias __attribute__ (( vector_size ((sizeof)*(length)) ))
#else
#define __VECTOR_KIT_VECTOR_TYPEDEF(base_type, sizeof, length, alias) typedef base_type alias [length]
#endif


// make vector types:
template<class _Tp, long _Size>
struct __make_vector;

#define __template__make_vector(__type, __sizeof) \
template<> struct __make_vector<__type,   02> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof,   02, type); }; \
template<> struct __make_vector<__type,   04> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof,   04, type); }; \
template<> struct __make_vector<__type,  010> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof,  010, type); }; \
template<> struct __make_vector<__type,  020> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof,  020, type); }; \
template<> struct __make_vector<__type,  040> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof,  040, type); }; \
template<> struct __make_vector<__type, 0100> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof, 0100, type); }; \
template<> struct __make_vector<__type, 0200> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof, 0200, type); }; \
template<> struct __make_vector<__type, 0400> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof, 0400, type); }; \
template<> struct __make_vector<__type,01000> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof,01000, type); }; \
template<> struct __make_vector<__type,02000> { __VECTOR_KIT_VECTOR_TYPEDEF(__type, __sizeof,02000, type); }

// integral types:
__template__make_vector(char              , 1);
__template__make_vector(unsigned char     , 1);

__template__make_vector(short             , 2);
__template__make_vector(unsigned short    , 2);

__template__make_vector(int               , 4);
__template__make_vector(unsigned int      , 4);

__template__make_vector(long              , 8);
__template__make_vector(unsigned long     , 8);

__template__make_vector(long long         , 8);
__template__make_vector(unsigned long long, 8);

// floating-point types:
__template__make_vector(float             , 4);
__template__make_vector(double            , 8);

#undef __template__make_vector


// vector forward declaration:
template <class _Tp, long _Size>
struct Vector;

// comparison result type:
template<class _Tp> struct __base_comparison_result;
template<         > struct __base_comparison_result<         char     > { typedef char      type; };
template<         > struct __base_comparison_result<         short    > { typedef short     type; };
template<         > struct __base_comparison_result<         int      > { typedef int       type; };
template<         > struct __base_comparison_result<         long     > { typedef long      type; };
template<         > struct __base_comparison_result<         long long> { typedef long long type; };
template<         > struct __base_comparison_result<unsigned char     > { typedef char      type; };
template<         > struct __base_comparison_result<unsigned short    > { typedef short     type; };
template<         > struct __base_comparison_result<unsigned int      > { typedef int       type; };
template<         > struct __base_comparison_result<unsigned long     > { typedef long      type; };
template<         > struct __base_comparison_result<unsigned long long> { typedef long long type; };
template<         > struct __base_comparison_result<         float    > { typedef int       type; };
template<         > struct __base_comparison_result<         double   > { typedef long      type; };
template<class _Tp, long _Size>
struct __comparison_result {
    // signed-integral type with the same width as _Tp
    typedef typename __base_comparison_result<_Tp>::type __base_comparison_result_type;
    // vector type of the signed-integral type with the same width
    typedef Vector<__base_comparison_result_type, _Size> type;
};

// shuffling mask type:
template<class _Tp> struct __base_shuffle_mask : __base_comparison_result<_Tp> {};
template<class _Tp, long _Size>
struct __shuffle_mask {
    // signed-integral type with the same width as _Tp
    typedef typename __base_shuffle_mask<_Tp>::type __base_shuffle_mask_type;
    // vector type of the integral type with the same width
    typedef Vector<__base_shuffle_mask_type, _Size> type;
};

} // namespace SIMD
} // namespace __3_
UTILITYKIT_END_NAMESPACE

#endif /* SIMDVectorTraits_h */
