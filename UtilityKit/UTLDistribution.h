//
//  UTLDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLDistribution_h
#define UTLDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Abstract for a general distribution.
     */
    class Distribution {
        Distribution(Distribution const&) = delete;
        Distribution &operator=(Distribution const&) = delete;

    protected:
        explicit Distribution() noexcept = default;
    public:
        virtual ~Distribution() = default;

        // types
        //
        using value_type = double; //!< real value type.
        using size_type = unsigned long; //!< integer type.

        // properties
        //
        virtual value_type mean() const noexcept = 0; //!< Mean.
        virtual value_type variance() const noexcept = 0; //!< Variance.

        // statistics
        //
        /**
         @brief Probability distribution function.
         @discussion Given `x`, `pdf` is interpolated from the table. So the result will be (slightly) different from the `pdf` with which `*this` is initialized.
         @param x min <= x <= max.
         */
        virtual value_type pdf(value_type x) const = 0;
        /**
         @brief Cumulative distribution function.
         @discussion cdf(min) = 0 and cdf(max) = 1.
         @param x min <= x <= max.
         */
        virtual value_type cdf(value_type x) const = 0;
        /**
         @brief Inverse cumulative distribution function.
         @discussion cdf^-1 (0) = min and cdf^-1 (1) = max.
         @param cdf 0 <= cdf <= 1.
         */
        virtual value_type icdf(value_type cdf) const = 0;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Abstract for a general distribution.
     */
    class Distribution {
        Distribution(Distribution const&) = delete;
        Distribution &operator=(Distribution const&) = delete;

    protected:
        explicit Distribution() noexcept = default;
    public:
        virtual ~Distribution() = default;

        // types
        //
        using value_type = double; //!< real value type.

        // properties
        //
        virtual value_type mean() const noexcept = 0; //!< Mean.
        virtual value_type variance() const noexcept = 0; //!< Variance.

        // statistics
        //
        /**
         @brief Probability distribution function.
         @discussion Given `x`, `pdf` is interpolated from the table. So the result will be (slightly) different from the `pdf` with which `*this` is initialized.
         @param x min <= x <= max.
         */
        virtual value_type pdf(value_type x) const = 0;
        /**
         @brief Cumulative distribution function.
         @discussion cdf(min) = 0 and cdf(max) = 1.
         @param x min <= x <= max.
         */
        virtual value_type cdf(value_type x) const = 0;
        /**
         @brief Inverse cumulative distribution function.
         @discussion cdf^-1 (0) = min and cdf^-1 (1) = max.
         @param cdf 0 <= cdf <= 1.
         */
        virtual value_type icdf(value_type cdf) const = 0;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLDistribution_h */
