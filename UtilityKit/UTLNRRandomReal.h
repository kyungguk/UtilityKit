//
//  UTLNRRandomReal.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLNRRandomReal_h
#define UTLNRRandomReal_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLRandomReal.h>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief From "Numerical Recipe," Press, 2007, Chapter 7.1.
     */
    class NRRandomReal final : public RandomReal {
        std::unique_ptr<size_type> __u_, __v_, __w_;

    public:
        // move/swap
        //
        void swap(NRRandomReal &) noexcept;
        NRRandomReal(NRRandomReal &&) noexcept;
        NRRandomReal &operator=(NRRandomReal &&) noexcept;

        /**
         @brief Default constructor.
         @discussion The state is invalid (i.e., bool(*this) == false).
         */
        explicit NRRandomReal() noexcept = default;
        /**
         @brief Initialize with any integer seed number.
         */
        explicit NRRandomReal(size_type seed) noexcept;

        /**
         @brief State.
         @discussion If true, the state is valid and can be used.
         */
        operator bool() const noexcept { return bool(__u_); }

        /**
         @brief Returns a random real variate uniformly distributed between 0 and 1.
         */
        value_type operator()() noexcept override { return _variate_real(); }
    private:
        value_type _variate_real() noexcept { return 5.42101086242752217e-20 * _variate_int(); }
        size_type _variate_int() noexcept;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLRandomReal.h>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief From "Numerical Recipe," Press, 2007, Chapter 7.1.
     */
    class NRRandomReal final : public RandomReal {
        std::unique_ptr<size_type> __u_, __v_, __w_;

    public:
        // move/swap
        //
        void swap(NRRandomReal &) noexcept;
        NRRandomReal(NRRandomReal &&) noexcept;
        NRRandomReal &operator=(NRRandomReal &&) noexcept;

        /**
         @brief Default constructor.
         @discussion The state is invalid (i.e., bool(*this) == false).
         */
        explicit NRRandomReal() noexcept {}
        /**
         @brief Initialize with any integer seed number.
         */
        explicit NRRandomReal(size_type seed) noexcept;

        /**
         @brief State.
         @discussion If true, the state is valid and can be used.
         */
        explicit operator bool() const noexcept { return bool(__u_); }

        /**
         @brief Returns a random real variate uniformly distributed between 0 and 1.
         */
        value_type operator()() noexcept override { return _variate_real(); }
    private:
        value_type _variate_real() noexcept { return 5.42101086242752217e-20 * _variate_int(); }
        size_type _variate_int() noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLNRRandomReal_h */
