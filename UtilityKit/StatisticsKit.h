//
//  StatisticsKit.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/2/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef StatisticsKit_h
#define StatisticsKit_h

#if defined(__cplusplus)

#include <UtilityKit/RandomRealKit.h>
#include <UtilityKit/DistributionKit.h>
#include <UtilityKit/UTLRandomVariate.h>

#endif


#endif /* StatisticsKit_h */
