//
//  UTLShapeFunction.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/1/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLShapeFunction_h
#define UTLShapeFunction_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <cmath>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    /**
     @brief Particle shape function and neighboring grid weighting sheme.
     @discussion It assumes that the location is normalized, so the integral part is equal to the grid index.
     */
    template<typename T, long Order> struct Shape;

#pragma mark 1st Order Specialization
    template <typename _T>
    struct Shape<_T, 1> {
        typedef _T value_type;
        typedef long index_type;

        value_type w[2];
        index_type i[2];

        void operator()(const value_type &x) noexcept {
            constexpr index_type ione(1);
            constexpr value_type fone(1);

            i[0] = i[1] = __floor(x); // i0 = floor(x), where x = x/dx
            i[1] += ione;
            w[1] = x - value_type(i[0]); // w1=x-i0, i becomes i1
            w[0] = fone - w[1]; // at i=i0, w=w0=1 - (x-i0)
        }
    private:
        index_type __floor(value_type const& x) const noexcept { return index_type(std::floor(x)); }
    };

#pragma mark 2nd Order Specialization
    template <typename _T>
    struct Shape<_T, 2> {
        typedef _T value_type;
        typedef long index_type;

        index_type i[3];
        value_type w[3];

        void operator()(value_type x) noexcept {
            constexpr index_type ione(1);
            constexpr value_type fone(1), half(0.5), _3_4(0.75);

            i[0] = i[1] = i[2] = __round(x);
            i[0] -= ione, i[2] += ione; // i1 = round(x), where x = x/dx
            // i=i1:
            x = value_type(i[1]) - x; // (i-x)
            w[1] = _3_4 - (x*x); // i=i1, w=w1=3/4 - (x-i)^2
            // i=i0:
            x += half; // (i-x) + 1/2
            w[0] = half * (x*x); // i=i0, w=w0=1/2 * (1/2 - (x-i))^2
            // i=i2:
            w[2] = fone - (w[0]+w[1]);
        }
    private:
        index_type __round(value_type const& x) const noexcept { return index_type(std::round(x)); }
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTL_pow.h>

#include <limits>
#include <array>
#include <cmath>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Particle shape function and neighboring grid weighting sheme.
     @discussion It assumes that the location is normalized, so the integral part is equal to the grid index.
     */
    template <class T, long Order> class Shape;

    // MARK: Shape<T, 1L>
    //
    template <class T>
    class Shape<T, 1L> {
        static constexpr long _Order = 1L;

    public:
        using value_type = T;
        using index_type = long;

        static constexpr index_type order() noexcept { return index_type{_Order}; }

        explicit Shape() noexcept { _w.fill(std::numeric_limits<T>::quiet_NaN()), _i.fill(0); }
        explicit Shape(value_type const x) noexcept { (*this)(x); }
        Shape& operator=(value_type const x) noexcept { return (*this)(x), *this; }

        inline void operator()(const value_type x) noexcept;

        // access weight
        template <long I>
        value_type const& w() const noexcept { static_assert(I >= 0 && I <= order(), "index out of range"); return std::get<I>(_w); }
        // access index
        template <long I>
        index_type const& i() const noexcept { static_assert(I >= 0 && I <= order(), "index out of range"); return std::get<I>(_i); }

    private:
        std::array<value_type, _Order + 1> _w;
        std::array<index_type, _Order + 1> _i;

        index_type floor(value_type const x) noexcept { return static_cast<index_type>(std::floor(x)); }
    };

    // MARK: Shape<T, 2L>
    //
    template <class T>
    class Shape<T, 2L> {
        static constexpr long _Order = 2L;

    public:
        using value_type = T;
        using index_type = long;

        static constexpr index_type order() noexcept { return index_type{_Order}; }

        explicit Shape() noexcept { _w.fill(std::numeric_limits<T>::quiet_NaN()), _i.fill(0); }
        explicit Shape(value_type const x) noexcept { (*this)(x); }
        Shape& operator=(value_type const x) noexcept { return (*this)(x), *this; }

        inline void operator()(value_type x) noexcept;

        // access weight
        template <long I>
        value_type const& w() const noexcept { static_assert(I >= 0 && I <= order(), "index out of range"); return std::get<I>(_w); }
        // access index
        template <long I>
        index_type const& i() const noexcept { static_assert(I >= 0 && I <= order(), "index out of range"); return std::get<I>(_i); }

    private:
        std::array<value_type, _Order + 1> _w;
        std::array<index_type, _Order + 1> _i;

        index_type round(value_type const x) noexcept { return static_cast<index_type>(std::round(x)); }
    };

    // MARK: Shape<T, 3L>
    //
    /**
     @brief 3rd order shape function.
     @discussion W(x) =
     *                  (2 + x)^3/6           for -2 <= x < -1
     *                  (4 - 6x^2 - 3x^3)/6   for -1 <= x < 0
     *                  (4 - 6x^2 + 3x^3)/6   for 0 <= x < 1
     *                  (2 - x)^3/6           for 1 <= x < 2
     *                  0                     otherwise
     */
    template <>
    class Shape<double, 3L> {
        static constexpr long _Order = 3L;

    public:
        using value_type = double;
        using index_type = long;

        static constexpr index_type order() noexcept { return index_type{_Order}; }

        explicit Shape() noexcept { _w.fill(std::numeric_limits<value_type>::quiet_NaN()); }
        explicit Shape(value_type const x) noexcept { (*this)(x); }
        Shape& operator=(value_type const x) noexcept { (*this)(x); return *this; }

        inline void operator()(value_type x) noexcept;

        // access weight
        template <long I>
        value_type w() const noexcept {
            return std::get<I>(_w);
            static_assert(I >= 0 && I <= order(), "index out of range");
        }
        // access index
        template <long I>
        index_type i() const noexcept {
            return _m1 + I;
            static_assert(I >= 0 && I <= order(), "index out of range");
        }

    private:
        std::array<value_type, _Order + 1> _w; // [-1, 0, 1, 2]
        index_type _m1{};

        index_type ceil(value_type const x) noexcept { return static_cast<index_type>(std::ceil(x)); }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLShapeFunction.hh>

#endif /* UTLShapeFunction_h */
