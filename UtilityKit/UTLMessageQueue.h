//
//  UTLMessageQueue.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/25/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLMessageQueue_h
#define UTLMessageQueue_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLSema.h>
#include <functional>
#include <utility>
#include <atomic>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief MPI-like inter-thread message communication.
     */
    class Messenger {
        struct _Envelop;
        class _MessageQueue;
        struct _BarrierToken;

        long _size;
        long _rank;
        _MessageQueue *_msgQs;
        _BarrierToken *_barTok;

    public:
        class Request;

        ~Messenger() = default;
        Messenger(Messenger const &) noexcept = default;
        Messenger& operator=(Messenger const &) noexcept = default;

        // message communication interfaces
        // auto send(to, message_type) -> Request // immediate send
        // auto recv(from) -> message_type
        void barrier() const;

        // create worker threads, if necessary, and call main
        // it is assumed that inside main, no other threads are invoked
        static void start(long const size, std::function<void(Messenger const &)> const &main) { _start(size, main); }

    private:
        explicit Messenger(long const size, long const rank, _MessageQueue *const msgQs, _BarrierToken *const barTok);
        static void _start(long const size, std::function<void(Messenger const &)> const &main);
    };

    class Messenger::Request {
        friend Messenger;

        std::atomic_long *_ref_cnt;
        Sema<SpinLock> *_sem;

        explicit Request();
        Request& operator=(Request const&) = delete;

        void post(long const n) const;
        void post() const { post(1); }

    public:
        ~Request();
        Request(Request const&);
        Request(Request&&);

        void wait() const;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLMessageQueue_h */
