//
//  UTLCondV.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/23/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLCondV.h"
#include "UTLPrinto.h"

// MARK: Version 3
//
#include <stdexcept>
#include <string>
#include <iostream>
#include <sys/time.h>

// MARK: CondV<Mutex>
//
UTL::__3_::CondV<UTL::__3_::Mutex>::~CondV<UTL::__3_::Mutex>()
try {
    if (pthread_cond_destroy(&__c_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_destroy(...) returned error");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    std::terminate();
}

UTL::__3_::CondV<UTL::__3_::Mutex>::CondV()
try {
    if (pthread_cond_init(&__c_, nullptr)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_init(...) returned error");
    }
} catch (std::exception&) {
    throw;
}

// notifications:
void UTL::__3_::CondV<UTL::__3_::Mutex>::notify_one() noexcept
try {
    if (pthread_cond_signal(&__c_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_signal(...) returned error");
    }
} catch (std::exception& e) {
    printo(std::cerr, e.what(), '\n');
    //throw;
}
void UTL::__3_::CondV<UTL::__3_::Mutex>::notify_all() noexcept
try {
    if (pthread_cond_broadcast(&__c_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_broadcast(...) returned error");
    }
} catch (std::exception& e) {
    printo(std::cerr, e.what(), '\n');
    //throw;
}

// waiting:
void UTL::__3_::CondV<UTL::__3_::Mutex>::wait(lock_type &__mx)
{
    if (pthread_cond_wait(&__c_, __mx.native_handle())) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_wait(...) returned error");
    }
}

auto UTL::__3_::CondV<UTL::__3_::Mutex>::wait_for(lock_type& __mx, unsigned __dur_in_sec)
-> timed_status {
    // calculate abstime in future
    struct timespec ts{0, 0};
    {
        struct timeval tv;
        if (gettimeofday(&tv, nullptr)) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "gettimeofday(...) returned error");
        }
        ts.tv_sec = tv.tv_sec + __dur_in_sec;
    }

    // timed wait
    int ret;
    if ( (ret = pthread_cond_timedwait(&__c_, __mx.native_handle(), &ts)) ) {
        return ETIMEDOUT == ret ? timeout : throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_cond_timedwait(...) returned error");
    }
    return no_timeout;
}

// MARK: CondV<SpinLock>
//
UTL::__3_::CondV<UTL::__3_::SpinLock>::~CondV<UTL::__3_::SpinLock>()
try {
    LockG<lock_type> l(_master_key);
    if (!_wait_list.empty()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "there are threads still waiting");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    //std::terminate();
}

UTL::__3_::CondV<UTL::__3_::SpinLock>::CondV()
: _wait_list(), _master_key() {
}

// notifications:
void UTL::__3_::CondV<UTL::__3_::SpinLock>::notify_one() noexcept
{
    LockG<lock_type> l(_master_key);
    if (!_wait_list.empty()) {
        _wait_list.back()->unlock();
        _wait_list.pop_back();
    }
}
void UTL::__3_::CondV<UTL::__3_::SpinLock>::notify_all() noexcept
{
    LockG<lock_type> l(_master_key);
    for (lock_type *lock : _wait_list) {
        lock->unlock();
    }
    _wait_list.clear();
}

// waiting:
void UTL::__3_::CondV<UTL::__3_::SpinLock>::wait(lock_type &mx)
{
    lock_type wait(SpinLock::Locked);
    {
        LockG<lock_type> _(_master_key); // first, acquire master lock
        mx.unlock(); // then, release mutex
        _wait_list.push_back(&wait); // be wait-listed
        // release master key
    }
    wait.lock(); // acquire wait lock
    mx.lock(); // acquire mutex
}


// MARK:- Version 4
//
#include <stdexcept>
#include <string>
#include <iostream>

UTL::__4_::CondV::~CondV()
try {
    if (pthread_cond_destroy(&__c_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_cond_destroy(...) returned error");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    std::terminate();
}

UTL::__4_::CondV::CondV()
try {
    if (pthread_cond_init(&__c_, nullptr)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_cond_init(...) returned error");
    }
} catch (std::exception&) {
    throw;
}

auto UTL::__4_::CondV::wait_until(lock_type& mx, std::chrono::system_clock::time_point const &future)
-> timed_status {
    using Clock = std::chrono::system_clock;
    struct timespec ts{Clock::to_time_t(future), 0};
    ts.tv_sec *= ts.tv_sec > 0;
    int ret;
    if ( (ret = pthread_cond_timedwait(&__c_, mx.native_handle(), &ts)) ) {
        return ETIMEDOUT == ret ? timeout : throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_cond_timedwait(...) returned error");
    }
    return no_timeout;
}
