//
//  UTLCubicSplineCoefficient.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/15/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLCubicSplineCoefficient_hh
#define UTLCubicSplineCoefficient_hh

#include <stdexcept>

// MARK:- UTL::__4_::CubicSplineCoefficient<T>
//
template <class T>
void UTL::__4_::CubicSplineCoefficient<T>::_construct_coefs(typename Base::coefficient_table_type &table)
{
    constexpr typename Base::value_type zero{0}, one{1}, two{2}, three{3}, _1_3 = one/three, _1_6 = one/(two*three);
    if (table.size() < Base::spline_order()) {
        throw std::invalid_argument(__PRETTY_FUNCTION__); // at least three entries are needed
    }

    // get y_j'' using a natural boundary condition
    // in coefficient array, {y_j, M(j,j), M(j,j+1), b_j}
    // upon solving tridiag, {y_j, _, _, y_j''}
    //
    {
        // solve tridiag
        typename Base::coefficient_table_type tmp_table(table.size() - 2);
        for (typename Base::coefficient_table_type::size_type i = 0, n = tmp_table.size(); i < n; ++i) {
            typename Base::coefficient_type &c = tmp_table[i].second;
            typename Base::value_type const y0 = std::get<0>(table[i+0].second), y1 = std::get<0>(table[i+1].second), y2 = std::get<0>(table[i+2].second);
            typename Base::value_type const d0 = std::get<1>(table[i+0].second), d1 = std::get<1>(table[i+1].second);
            std::get<3>(c) = (y2 - y1)/d1 - (y1 - y0)/d0;
            std::get<0>(c) = d0*_1_6;
            std::get<1>(c) = (d1 + d0)*_1_3;
            std::get<2>(c) = d1*_1_6;
        }
        _solve_tridiag(tmp_table);
        // copy back results
        for (typename Base::coefficient_table_type::size_type i = 0, n = tmp_table.size(); i < n; ++i) {
            std::get<3>(table[i+1].second) = std::get<3>(tmp_table[i].second);
        }
        // put boundary y_j'':
        std::get<3>(table.front().second) = zero;
        std::get<3>(table.back().second) = zero;
    }

    // get coefficients
    // note that the first element is already filled.
    //
    {
        typename Base::coefficient_table_type::iterator _1 = table.begin(), _0 = _1++;
        for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
            typename Base::coefficient_type &c0 = _0->second, &c1 = _1->second;
            typename Base::value_type const dx = _1->first - _0->first;
#if 1 // normalized; t = (x - x_j)/(x_j + 1-x_j)
            typename Base::value_type const dx2 = dx * dx;
            std::get<1>(c0) = (c1.front() - c0.front()) - dx2*(two*c0.back() + c1.back())*_1_6;
            std::get<2>(c0) = dx2*c0.back() / two;
            std::get<3>(c0) = dx2*(c1.back() - c0.back())*_1_6;
#else // non-normalized; t = (x - x_j)
            std::get<1>(c0) = (c1.front() - c0.front())/dx - dx*(two*c0.back() + c1.back())*_1_6;
            std::get<2>(c0) = c0.back() / two;
            std::get<3>(c0) = (c1.back() - c0.back())/dx*_1_6;
#endif
        }
        // note that the last entry is not filled except c[0].
    }
}

template <class T>
void UTL::__4_::CubicSplineCoefficient<T>::_solve_tridiag(typename Base::coefficient_table_type &table)
{
    if (table.empty()) return;

    // decomposition and forward substitution:
    // in in-place operation, {y_j, M(j,j), M(j,j+1), b_j} -> {y_j, gam, M(j,j+1), y_j''}
    //
    constexpr typename Base::value_type zero{0};
    {
        typename Base::value_type bet, gam;
        typename Base::coefficient_table_type::iterator _1 = table.begin(), _0 = _1++;
        std::get<3>(_0->second) /= (bet = std::get<1>(_0->second));
        for (auto const last = table.end(); _1 != last; ++_0, ++_1) {
            gam = std::get<2>(_0->second) / bet;
            bet = std::get<1>(_1->second) - std::get<0>(_1->second)*gam;
            std::get<1>(_1->second) = gam;
            if (zero == bet) {
                throw std::runtime_error(__PRETTY_FUNCTION__);
            }
            std::get<3>(_1->second) = (std::get<3>(_1->second) - std::get<0>(_1->second)*std::get<3>(_0->second)) / bet;
        }
    }

    // Backsubstitution:
    {
        typename Base::coefficient_table_type::reverse_iterator _1 = table.rbegin(), _0 = _1++;
        for (auto const last = table.rend(); _1 != last; ++_0, ++_1) {
            std::get<3>(_1->second) -= std::get<1>(_0->second) * std::get<3>(_0->second);
        }
    }
}

#endif /* UTLCubicSplineCoefficient_hh */
