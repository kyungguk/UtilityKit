//
//  UTLLock.cc
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 9/23/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#include "UTLLock.h"
#include "UTLPrinto.h"

// MARK: Version 3
//
#include <stdexcept>
#include <string>
#include <iostream>
#include <sched.h> // for sched_yield()

// MARK: Mutex
//
UTL::__3_::Mutex::~Mutex()
try {
    if (pthread_mutex_destroy(&__m_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_mutex_destroy(...) returned error");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    std::terminate();
}

UTL::__3_::Mutex::Mutex() try {
    if (pthread_mutex_init(&__m_, nullptr)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_mutex_init(...) returned error");
    }
} catch (std::exception&) {
    throw;
}

void UTL::__3_::Mutex::lock()
{
    if (pthread_mutex_lock(&__m_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_mutex_lock(...) returned error");
    }
}
bool UTL::__3_::Mutex::try_lock() noexcept
{
    return 0 == pthread_mutex_trylock(&__m_); // lock was aquired
}
void UTL::__3_::Mutex::unlock() noexcept
try {
    if (pthread_mutex_unlock(&__m_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + "pthread_mutex_unlock(...) returned error");
    }
} catch (std::exception& e) {
    printo(std::cerr, e.what(), '\n');
    //throw;
}

// MARK: SpinLock
//
void UTL::__3_::SpinLock::lock() noexcept
{
    while (__s_.test_and_set(std::memory_order_acquire)) {
        //sched_yield();
        continue;
    }
}
bool UTL::__3_::SpinLock::try_lock() noexcept
{
    return !__s_.test_and_set(std::memory_order_acquire);
}
void UTL::__3_::SpinLock::unlock() noexcept
{
    __s_.clear(std::memory_order_release);
}


// MARK:- Version 4
//
#include <UtilityKit/UTLThread.h>
#include <stdexcept>
#include <string>
#include <iostream>

// MARK: Mutex
//
UTL::__4_::Mutex::~Mutex()
try {
    if (pthread_mutex_destroy(&__m_)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_mutex_destroy(...) returned error; perhaps the state is locked");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), '\n');
    std::terminate();
}

UTL::__4_::Mutex::Mutex() try {
    if (pthread_mutex_init(&__m_, nullptr)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - pthread_mutex_init(...) returned error");
    }
} catch (std::exception&) {
    throw;
}

// MARK: SpinLock
//
void UTL::__4_::SpinLock::lock(bool const yield) noexcept
{
    while (__s_.test_and_set(std::memory_order_acquire)) {
        if (yield) Thread::yield();
    }
}
