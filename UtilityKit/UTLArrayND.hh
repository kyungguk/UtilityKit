//
//  UTLArrayND.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 10/19/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayND_hh
#define UTLArrayND_hh

#include <utility>
#include <sstream>

// MARK: std::swap
//
namespace std {
    template <class Type, long ND, long PadSize>
    inline void swap(UTL::__4_::ArrayND<Type, ND, PadSize> &a, UTL::__4_::ArrayND<Type, ND, PadSize> &b) noexcept(noexcept(a.swap(b))) {
        a.swap(b);
    }
    template <class Type, long ND, long PadSize>
    inline void swap(UTL::__4_::ArraySliceND<Type, ND, PadSize> &a, UTL::__4_::ArraySliceND<Type, ND, PadSize> &b) noexcept(noexcept(a.swap(b))) {
        a.swap(b);
    }
}

// MARK: ArrayND Output Stream
//
template <class _CharT, class _Traits, class Type, long ND, long PadSize>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::ArrayND<Type, ND, PadSize> const& a) {
    if (!a) {
        return os << "(null)";
    }
    std::basic_ostringstream<_CharT, _Traits> ss; {
        ss.flags(os.flags());
        ss.imbue(os.getloc());
        ss.precision(os.precision());
    }
    auto it = a.pad_begin(), end = a.begin();
    // left paddings
    ss << "{" << *it++;
    while (it != end) ss << ", " << *it++;
    ss << "|";
    // contents
    end = a.end();
    if (it != end) {
        ss << *it++;
        while (it != end) ss << ", " << *it++;
    }
    // right paddings
    end = a.pad_end();
    ss << "|" << *it++;
    while (it != end) ss << ", " << *it++;
    ss << "}";
    return os << ss.str();
}
template <class _CharT, class _Traits, class Type, long ND>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::ArrayND<Type, ND, 0L> const& a) {
    if (a.empty()) {
        return os << "{}";
    }
    std::basic_ostringstream<_CharT, _Traits> ss; {
        ss.flags(os.flags());
        ss.imbue(os.getloc());
        ss.precision(os.precision());
    }
    auto it = a.begin(), end = a.end();
    ss << "{" << *it++;
    while (it != end) ss << ", " << *it++;
    ss << "}";
    return os << ss.str();
}

// MARK: ArraySliceND Output Stream
// TODO: Specialization is needed for performance.
//
template <class _CharT, class _Traits, class Type, long ND, long PadSize>
std::basic_ostream<_CharT, _Traits>&
operator<<(std::basic_ostream<_CharT, _Traits>& os, UTL::__4_::ArraySliceND<Type, ND, PadSize> const& a) {
    return os << UTL::__4_::ArrayND<Type, ND, PadSize>{a};
}

#endif /* UTLArrayND_hh */
