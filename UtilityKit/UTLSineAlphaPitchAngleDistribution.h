//
//  UTLSineAlphaPitchAngleDistribution.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 11/3/17.
//  Copyright © 2017 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLSineAlphaPitchAngleDistribution_h
#define UTLSineAlphaPitchAngleDistribution_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 3
//
#include <UtilityKit/UTLDistribution.h>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 3
inline
#endif
namespace __3_ {

    class CustomDistribution;

    /**
     Sine^n type pitch angle distribution, where n >= 0.

     pdf = sin(alpha)^(n+1) / (2 A), where
     A = √pi/2 gamma(1+n/2)/gamma(1.5 + n/2).
     */
    class SineAlphaPitchAngleDistribution final : public Distribution {
        using dist_ptr = std::unique_ptr<CustomDistribution>;

        dist_ptr _dist;
        value_type _n;

    public:
        // constructors
        //
        /**
         Construct sin^n(alpha) distribution with index n >= 0.
         @param n Anisotropy index.
         @param should_normalize_pdf If true, normalize the sampled cdf points. Default is false.
         */
        explicit SineAlphaPitchAngleDistribution(value_type n, bool should_normalize_pdf = false);

        // copy/move
        //
        SineAlphaPitchAngleDistribution(SineAlphaPitchAngleDistribution const &) noexcept;
        SineAlphaPitchAngleDistribution &operator=(SineAlphaPitchAngleDistribution const &) noexcept;
        SineAlphaPitchAngleDistribution(SineAlphaPitchAngleDistribution &&) noexcept;
        SineAlphaPitchAngleDistribution &operator=(SineAlphaPitchAngleDistribution &&) noexcept;

        // properties
        //
        /**
         Anisotropy index in sin^n(alpha).
         */
        value_type n() const noexcept { return _n; }
        /**
         n/2.
         */
        value_type n_2() const noexcept { return _n * 0.5; }
        /**
         Normalization constant.
         */
        value_type A() const noexcept;

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type alpha) const override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type alpha) const override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type cdf) const override;
    };

} // namespace __3_
UTILITYKIT_END_NAMESPACE


// MARK:- Version 4
//
#include <UtilityKit/UTLDistribution.h>
#include <UtilityKit/AlgorithmKit.h>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    /**
     @brief Sine pitch angle distribution.
     @discussion sin(α)^n pitch angle distribution, where n >= 0.

     pdf = sin(α)^(n+1) / (2 A), where
     A(n) = √pi/2 gamma(1+n/2)/gamma(1.5 + n/2).
     mean = pi/2.
     variance = Integrate[pdf(α) α^2, {α, 0, pi}] - mean^2.
     */
    class SineAlphaPitchAngleDistribution final : public Distribution {
        using spline_coefficient_type = MonotoneCubicCoefficient<value_type>;
        using interpolator_type = spline_coefficient_type::spline_interpolator_type;
        using integrator_type = TrapzIntegrator<value_type>;
        using sampler_type = AdaptiveSampling1D<value_type>;

        value_type _n;
        value_type _variance;
        interpolator_type _cdf;
        interpolator_type _icdf;

    public:
        // constructors
        //
        /**
         Construct sin(α)^n distribution with index n >= 0.
         @param n Anisotropy index (non-negative).
         */
        explicit SineAlphaPitchAngleDistribution(value_type const n);

        // copy/move
        //
        SineAlphaPitchAngleDistribution(SineAlphaPitchAngleDistribution const &);
        SineAlphaPitchAngleDistribution &operator=(SineAlphaPitchAngleDistribution const &);
        SineAlphaPitchAngleDistribution(SineAlphaPitchAngleDistribution &&);
        SineAlphaPitchAngleDistribution &operator=(SineAlphaPitchAngleDistribution &&);

        // swap
        //
        void swap(SineAlphaPitchAngleDistribution &o);

        // properties
        //
        /**
         Anisotropy index.
         */
        value_type n() const noexcept { return _n; }

        // inherited
        //
        value_type mean() const noexcept override;
        value_type variance() const noexcept override;

        /**
         Probability distribution function.
         */
        value_type pdf(value_type const alpha) const override;
        /**
         Cumulative distribution function.
         */
        value_type cdf(value_type const alpha) const override;
        /**
         Inverse cumulative distribution function.
         */
        value_type icdf(value_type const cdf) const override;

    private: // helpers:
        static integrator_type default_integrator() noexcept;
        static sampler_type default_sampler() noexcept;

        static inline value_type A(value_type const n) noexcept;
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE

#endif /* UTLSineAlphaPitchAngleDistribution_h */
