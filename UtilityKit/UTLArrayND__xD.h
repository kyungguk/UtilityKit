//
//  UTLArrayND__xD.h
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 8/25/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayND__xD_h
#define UTLArrayND__xD_h

#include <UtilityKit/UtilityKit-config.h>

// MARK:- Version 4
//
#include <UtilityKit/UTLArrayNDLeafWalker.h>
#include <UtilityKit/UTLPaddedArray.h>
#include <UtilityKit/UTLVector.h>
#include <type_traits>
#include <algorithm>
#include <iterator>
#include <utility>
#include <memory>

UTILITYKIT_BEGIN_NAMESPACE
#if defined(UTILITYKIT_INLINE_VERSION) && UTILITYKIT_INLINE_VERSION == 4
inline
#endif
namespace __4_ {
    template <class Type, long ND, long PadSize>
    class ArrayND;
    template <class Type, long ND, long PadSize>
    class ArraySliceND;

    // MARK: Array<ND>
    //
    /**
     @brief Multi-dimensional array of homogeneous type.
     */
    template <class Type, long ND, long PadSize>
    class ArrayND final : private PaddedArray<ArrayND<Type, ND - 1, PadSize>, PadSize> {
        using _Super = PaddedArray<ArrayND<Type, ND - 1, PadSize>, PadSize>;
        static_assert(ND > 1, "invalid ND");
        static_assert(PadSize >= 0, "invalid PadSize");

        class _Flat : public Array<Type> {
        public:
            explicit _Flat() noexcept {}
            using Array<Type>::Array;
            void swap(_Flat &o) noexcept { // storage swap
                std::swap(this->_beg, o._beg);
                std::swap(this->_end, o._end);
            }
        } _flat{};
        bool _is_subarray{};

    public:
        // types:
        using value_type             = Type;
        using iterator               = typename _Super::iterator;
        using const_iterator         = typename _Super::const_iterator;
        using reverse_iterator       = typename _Super::reverse_iterator;
        using const_reverse_iterator = typename _Super::const_reverse_iterator;
        using size_type              = typename _Super::size_type;
        using difference_type        = typename _Super::difference_type;
        using size_vector_type       = Vector<size_type, ND>;
        using subarray_type          = typename _Super::value_type;

        using leaf_iterator                 = _ArrayNDLeafWalker<ArrayND>;
        using const_leaf_iterator           = _ArrayNDLeafWalker<ArrayND const>;
        using reverse_leaf_iterator         = std::reverse_iterator<leaf_iterator>;
        using const_reverse_leaf_iterator   = std::reverse_iterator<const_leaf_iterator>;

        // destructor:
        ~ArrayND() {}

        // constructors:
        /**
         @brief Construct an array whose state is invalid.
         @discussion No storage allocation occurs.
         */
        explicit ArrayND() noexcept {}

        //@{
        /**
         @brief Construct an array of the given dimension sizes (not including paddings) optionally with default value.
         */
        explicit ArrayND(size_vector_type const& dims);
        explicit ArrayND(size_vector_type const& dims, Type const& fill);
        //@}

        //@{
        /**
         @brief Copy/move construct from another array of the same type.
         @discussion When move constructing, other should not be an element of a higher rank array.

         Moving swaps the internal storages including flags, while copying is equivalent to creating a new array with the contents of other.
         */
        ArrayND(ArrayND&& other) : ArrayND() { swap(other); }
        ArrayND(ArrayND const& other) : ArrayND() {
            if (other) ArrayND{PaddedArray<Type, 0>{other.flat_array().begin(), other.flat_array().end()}.storage(), other.dims()}._swap(*this);
        }
        //@}

        /**
         @brief Copy construct from an array slice.
         */
        explicit ArrayND(ArraySliceND<Type, ND, PadSize> const &slice) : ArrayND() {
            if (slice) ArrayND{PaddedArray<Type, 0>{slice.leaf_pad_begin(), slice.leaf_pad_end()}.storage(), slice.dims()}._swap(*this);
        }

        // assignment:
        //@{
        /**
         @brief Element-wise copy/move assignment.
         @discussion Be careful when copy assigning array returned from a function, which will trigger move assignment.
         */
        ArrayND& operator=(ArrayND const& other) { _Super::operator=(other); return *this; }
        ArrayND& operator=(ArrayND&& other) { _Super::operator=(std::move(other)); return *this; }
        ArrayND& operator=(ArraySliceND<Type, ND, PadSize> const &slice);
        ArrayND& operator=(ArraySliceND<Type, ND, PadSize> &&slice);
        //@}

        // observers:
        /**
         @brief Check if the status of *this is valid.
         @discussion The validity is defined by whether the underlying storage is allocated or not.
         A default-constructed array is invalid, so is an array with reduce_prod(max_dims()) == 0.
         */
        explicit operator bool() const noexcept { return _Super::operator bool() && _Super::front(); }

        /**
         @brief Returns true if *this is an element of a higher rank array.
         @discussion If true, storage move semantic does not make sense and doing so will throw an exception.
         */
        bool is_subarray() const noexcept { return _is_subarray; }

        // capacity:
        //@{
        /**
         Properties of the leading dimension.
         */
        using _Super::empty;
        using _Super::size;
        using _Super::pad_size;
        using _Super::max_size;
        //@}

        /**
         @brief Array rank.
         */
        static constexpr size_type rank() noexcept { return size_vector_type::size(); }

        /**
         @brief Dimension sizes (excluding paddings).
         */
        size_vector_type dims() const noexcept {
            return _Super::operator bool() ? _Super::front().dims().prepend(_Super::size()) : size_vector_type{};
        }

        /**
         @brief Maximum dimension sizes (i.e., dims() + pad_size()).
         */
        size_vector_type max_dims() const noexcept {
            return _Super::operator bool() ? _Super::front().max_dims().prepend(_Super::max_size()) : size_vector_type{};
        }

        /**
         @brief Size at a given dimension.
         */
        template <long I>
        inline size_type size() const noexcept;

        /**
         @brief Max size at a given dimension.
         */
        template <long I>
        inline size_type max_size() const noexcept;

        // modifiers:
        void fill(Type const& x) noexcept(std::is_nothrow_copy_assignable<Type>::value) {
            if (*this) std::fill(flat_array().begin(), flat_array().end(), x);
        }

        /**
         @brief Storage swap.
         @discussion Both *this and other should not be an element of higher rank array.
         @exception When *this and/or other is an element of higher rank array.
         */
        inline void swap(ArrayND& other);

        // iterators:
        using _Super::begin;
        using _Super::cbegin;
        using _Super::end;
        using _Super::cend;

        using _Super::rbegin;
        using _Super::crbegin;
        using _Super::rend;
        using _Super::crend;

        using _Super::pad_begin;
        using _Super::pad_cbegin;
        using _Super::pad_end;
        using _Super::pad_cend;

        using _Super::pad_rbegin;
        using _Super::pad_crbegin;
        using _Super::pad_rend;
        using _Super::pad_crend;

        //@{
        /**
         @brief Walk through the elements in the slice (excluding paddings).
         */
        leaf_iterator        leaf_begin()       noexcept { return leaf_iterator{this, false, std::false_type{}}; }
        const_leaf_iterator  leaf_begin() const noexcept { return const_leaf_iterator{this, false, std::false_type{}}; }
        const_leaf_iterator leaf_cbegin() const noexcept { return leaf_begin(); }
        leaf_iterator          leaf_end()       noexcept { return leaf_iterator{this, false, std::true_type{}}; }
        const_leaf_iterator    leaf_end() const noexcept { return const_leaf_iterator{this, false, std::true_type{}}; }
        const_leaf_iterator   leaf_cend() const noexcept { return leaf_end(); }

        reverse_leaf_iterator        leaf_rbegin()       noexcept { return reverse_leaf_iterator{leaf_end()}; }
        const_reverse_leaf_iterator  leaf_rbegin() const noexcept { return const_reverse_leaf_iterator{leaf_end()}; }
        const_reverse_leaf_iterator leaf_crbegin() const noexcept { return leaf_rbegin(); }
        reverse_leaf_iterator          leaf_rend()       noexcept { return reverse_leaf_iterator{leaf_begin()}; }
        const_reverse_leaf_iterator    leaf_rend() const noexcept { return const_reverse_leaf_iterator{leaf_begin()}; }
        const_reverse_leaf_iterator   leaf_crend() const noexcept { return leaf_rend(); }
        //@}

        //@{
        /**
         @brief Walk through the elements in the slice (including paddings).
         */
        leaf_iterator        leaf_pad_begin()       noexcept { return leaf_iterator{this, true, std::false_type{}}; }
        const_leaf_iterator  leaf_pad_begin() const noexcept { return const_leaf_iterator{this, true, std::false_type{}}; }
        const_leaf_iterator leaf_pad_cbegin() const noexcept { return leaf_pad_begin(); }
        leaf_iterator          leaf_pad_end()       noexcept { return leaf_iterator{this, true, std::true_type{}}; }
        const_leaf_iterator    leaf_pad_end() const noexcept { return const_leaf_iterator{this, true, std::true_type{}}; }
        const_leaf_iterator   leaf_pad_cend() const noexcept { return leaf_pad_end(); }

        reverse_leaf_iterator        leaf_pad_rbegin()       noexcept { return reverse_leaf_iterator{leaf_pad_end()}; }
        const_reverse_leaf_iterator  leaf_pad_rbegin() const noexcept { return const_reverse_leaf_iterator{leaf_pad_end()}; }
        const_reverse_leaf_iterator leaf_pad_crbegin() const noexcept { return leaf_pad_rbegin(); }
        reverse_leaf_iterator          leaf_pad_rend()       noexcept { return reverse_leaf_iterator{leaf_pad_begin()}; }
        const_reverse_leaf_iterator    leaf_pad_rend() const noexcept { return const_reverse_leaf_iterator{leaf_pad_begin()}; }
        const_reverse_leaf_iterator   leaf_pad_crend() const noexcept { return leaf_pad_rend(); }
        //@}

        // element access:
        using _Super::at;
        using _Super::operator[];
        using _Super::front;
        using _Super::back;
        using _Super::pad_front;
        using _Super::pad_back;

        //@{
        /**
         @brief Index path subscript.
         */
        Type       &operator[](size_vector_type const &ipath)       noexcept(noexcept(std::declval<_Super>()[0])) { return _Super::operator[](ipath.front())[ipath.rest()]; }
        Type const &operator[](size_vector_type const &ipath) const noexcept(noexcept(std::declval<_Super>()[0])) { return _Super::operator[](ipath.front())[ipath.rest()]; }

        Type       &at(size_vector_type const &ipath)       { return _Super::at(ipath.front()).at(ipath.rest()); }
        Type const &at(size_vector_type const &ipath) const { return _Super::at(ipath.front()).at(ipath.rest()); }
        //@}

        /**
         @brief Returns reference to the flattened array of *this.
         @discussion If this->dims() is {N0, N1, ...}, flat_array() will contain (N0+2*pad_size()) * (N1+2*pad_size()) * ... elements.

         It contains no elements if *this is invalid.
         */
        Array<Type>       &flat_array()       noexcept { return _flat; }
        Array<Type> const &flat_array() const noexcept { return _flat; }

        //@{
        /**
         @brief Returns the first element of the underlying storage.
         @discussion This returns nullptr if bool(*this) is false.
         */
        Type       *data()       noexcept { return _flat.begin(); }
        Type const *data() const noexcept { return _flat.begin(); }
        //@}

        /**
         @brief Returns the pointer to the storage of the underlying array and its maximum extents.
         @discussion Used for slicing.
         */
        std::pair<Type *, size_vector_type> storage() noexcept { return std::make_pair(data(), max_dims()); }

    private:
        // constructor funnel point
        //
        explicit ArrayND(_Super &&other) noexcept : _Super(std::move(other)) {
            if (_Super::operator bool()) {
                _Flat{_Super::pad_begin()->data(), reduce_prod(max_dims())}.swap(_flat);
            }
        }
        explicit ArrayND(std::shared_ptr<Type> const &storage, size_vector_type const& dims) : ArrayND() {
            // preconditions:
            // 1. dims are valid (non-negative)
            // 2. storage can be nullptr if dims == 0 AND pad_size == 0; otherwise storage should have enough room for prod(dims) elements
            //
            _Super A(dims.front());
            if (A) {
                // populate subarrays
                //
                Type* data = storage.get();
                size_type const stride = reduce_prod(dims.rest() + 2*A.pad_size()); // data == nullptr is equivalent to stride == 0
                iterator first = A.pad_begin(), last = A.pad_end();
                for (; first != last; ++first, data += stride) {
                    if (storage) {
                        subarray_type{nullptr, std::shared_ptr<Type>{storage, data}, dims.rest()}._swap(*first);
                    } else {
                        subarray_type{nullptr, std::shared_ptr<Type>{             }, dims.rest()}._swap(*first);
                    }
                }
            }
            ArrayND{std::move(A)}._swap(*this);
        }

        friend ArrayND<Type, ND + 1, PadSize>; // grants access to enclosing ArrayND with rank() + 1
        explicit ArrayND(decltype(nullptr), std::shared_ptr<Type> &&storage, size_vector_type const& dims) : ArrayND(storage, dims) {
            // only called by enclosing ArrayND with rank() + 1
            // preconditions:
            // 1. dims are valid (non-negative)
            // 2. storage can be nullptr if dims == 0 AND pad_size == 0; otherwise storage should have enough room for prod(dims) elements
            //
            _is_subarray = true;
        }

        void _swap(ArrayND& other) noexcept {
            _Super::swap(other);
            _flat.swap(other._flat);
            std::swap(_is_subarray, other._is_subarray);
        }
    };
} // namespace __4_
UTILITYKIT_END_NAMESPACE


// MARK:- Implementation Header
//
#include <UtilityKit/UTLArrayND__xD.hh>

#endif /* UTLArrayND__xD_h */
