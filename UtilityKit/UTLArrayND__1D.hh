//
//  UTLArrayND__1D.hh
//  UtilityKit
//
//  Created by KYUNGGUK MIN on 8/25/18.
//  Copyright © 2018 KYUNGGUK MIN. All rights reserved.
//

#ifndef UTLArrayND__1D_hh
#define UTLArrayND__1D_hh

#include <algorithm>
#include <stdexcept>
#include <string>

// MARK:- UTL::__4_::Array<1D>
//
template <class Type, long PadSize>
auto UTL::__4_::ArrayND<Type, 1L, PadSize>::operator=(ArraySliceND<Type, 1L, PadSize> const &o)
-> ArrayND &{
    {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (o.size() != _Super::size()) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array dimension sizes inconsistency");
        }
        std::copy(o.leaf_pad_begin(), o.leaf_pad_end(), _Super::pad_begin());
    }
    return *this;
}
template <class Type, long PadSize>
auto UTL::__4_::ArrayND<Type, 1L, PadSize>::operator=(ArraySliceND<Type, 1L, PadSize> &&o)
-> ArrayND &{
    {
        if (!*this || !o) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - either or both arguments are not valid");
        } else if (o.size() != _Super::size()) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array dimension sizes inconsistency");
        }
        std::move(o.leaf_pad_begin(), o.leaf_pad_end(), _Super::pad_begin());
    }
    return *this;
}

template <class Type, long PadSize>
void UTL::__4_::ArrayND<Type, 1L, PadSize>::swap(ArrayND& other)
{
    if (is_subarray() || other.is_subarray()) {
        throw std::logic_error(std::string(__PRETTY_FUNCTION__) + " - both *this and other should not be an element of a higher rank array");
    }
    _swap(other);
}

#endif /* UTLArrayND__1D_hh */
